#include "IFemurImplant.h"
#include "FemoralPart.h"
#include "FemoralAxes.h"
#include "OutlineProfile.h"
#include "OutlineProfileMgrModel.h"
#include "FemoralCuts.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "TotalMainWindow.h"
#include "../KApp/MainWindow.h"
#include "../KAppTotal/EntityEnums.h"
#include "../KAppTotal/FakeXmlReader.h"
#include "../KAppTotal/Utilities.h"

//////////////////////////////////////////////////////////////////////
// This is a class to interface between femoral implant design module 
// and other down-stream modules. "Down-stream" modules need to ensure
// the femoral implant design is complete (may call 
// IFemurImplant::IsFemurImplantComplete() ) before retrieving any 
// design data from the femoral implant design module. 
// NOTE - The functions in this class never returns NULL pointers.
// If any unexpected data occur, the software application will be
// terminated immediately, which is necessary to ensure no unexpected 
// data is ever used in implant design. 
//////////////////////////////////////////////////////////////////////

IFemurImplant::IFemurImplant()
{

}

IFemurImplant::~IFemurImplant()
{

}
//
void IFemurImplant::CloseImplantImmediately(QString& errorIFunctionName)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc )
	{
		pDoc->AppendLog( QString("IFemurImplant::CloseImplantWithErrorMessages(), errorIFunctionName = ") + errorIFunctionName );
		// When error out here, we do not want to pop up error messages for femoral implant data modification.
		// Force pDoc data modification to be false.
		pDoc->SetModified(false);
	}

	QString msg;
	if ( !IFemurImplant::IsFemurImplantComplete() )// Check whether the femoral implant design is not complete
		msg = QString("Software encounters unexpected errors.\nFemoral implant design is not complete.\nSoftware will be terminated.");
	else if ( errorIFunctionName.isEmpty() )
		msg = QString("Software encounters unexpected errors.\nSoftware will be terminated.");
	else
		msg = QString("Software encounters unexpected errors\nwhen retrieving data from %0.\nSoftware will be terminated.").arg(errorIFunctionName);
	
	mainWindow->CloseImplantImmediately(msg);

}

// Femoral Part
void IFemurImplant::FemoralPart_GetSurfaces
(
	bool copy, 
	IwBSplineSurface*& mainSurface, 
	IwBSplineSurface*& leftSideSurface, 
	IwBSplineSurface*& rightSideSurface
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralPart_GetSurfaces"));

	CFemoralPart* pFemoralPart = pDoc->GetFemur();
	if (pFemoralPart==NULL)
		CloseImplantImmediately(QString("FemoralPart_GetSurfaces"));

	IwBSplineSurface *mSurf=NULL, *lSurf=NULL, *rSurf=NULL;

	mSurf = pFemoralPart->GetSinglePatchSurface();
	
	pFemoralPart->GetSideSurfaces(lSurf, rSurf);

	if ( mSurf==NULL || lSurf==NULL || rSurf==NULL )
		CloseImplantImmediately(QString("FemoralPart_GetSurfaces"));

	if ( copy )
	{
		IwSurface *mSurf_copy, *lSurf_copy, *rSurf_copy;
		mSurf->Copy(pDoc->GetIwContext(), mSurf_copy);
		lSurf->Copy(pDoc->GetIwContext(), lSurf_copy);
		rSurf->Copy(pDoc->GetIwContext(), rSurf_copy);
		mainSurface = (IwBSplineSurface*)mSurf_copy;
		leftSideSurface = (IwBSplineSurface*)lSurf_copy;
		rightSideSurface = (IwBSplineSurface*)rSurf_copy;
	}
	else
	{
		mainSurface = mSurf;
		leftSideSurface = lSurf;
		rightSideSurface = rSurf;
	}

	if ( mainSurface==NULL || leftSideSurface==NULL || rightSideSurface==NULL )
		CloseImplantImmediately(QString("FemoralPart_GetSurfaces"));

}

// Femoral Axes
void IFemurImplant::FemoralAxes_GetFeaturePoints
(
	IwPoint3d& notchProximalPoint, 
	IwPoint3d& leftEpiCondylarPoint, 
	IwPoint3d& rightEpiCondylarPoint, 
	double& antCutFlexionAngle, 
	double& antCutObliqueAngle, 
	double& femoralAxesFlexionAngle
)
{
	// un-reasonable initial values.
	antCutFlexionAngle = 999;
	antCutObliqueAngle = 999;
	femoralAxesFlexionAngle = 999;

	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetFeaturePoints"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetFeaturePoints"));

	IwPoint3d hipPoint;
	IwPoint3d antCutPlaneCenter, antCutPlaneRotationCenter;
	double femoralAxesYRotAngle, femoralAxesZRotAngle;
	pFemoralAxes->GetParams(hipPoint, notchProximalPoint, leftEpiCondylarPoint, rightEpiCondylarPoint, antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, antCutPlaneCenter, antCutPlaneRotationCenter, femoralAxesYRotAngle, femoralAxesZRotAngle);

	if ( !notchProximalPoint.IsInitialized() || !leftEpiCondylarPoint.IsInitialized() || !rightEpiCondylarPoint.IsInitialized() ||
		 antCutFlexionAngle == 999 || antCutObliqueAngle == 999 || femoralAxesFlexionAngle == 999 )
		 CloseImplantImmediately(QString("FemoralAxes_GetFeaturePoints"));

}

IwAxis2Placement IFemurImplant::FemoralAxes_GetMechanicalAxes()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetMechanicalAxes"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetMechanicalAxes"));

	IwAxis2Placement mechanicalAxes = IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1));
	pFemoralAxes->GetMechanicalAxes(mechanicalAxes);

	if ( mechanicalAxes == IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1)) )
		CloseImplantImmediately(QString("FemoralAxes_GetMechanicalAxes"));

	return mechanicalAxes;
}

IwAxis2Placement IFemurImplant::FemoralAxes_GetWhiteSideLineAxes()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetWhiteSideLineAxes"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetWhiteSideLineAxes"));

	IwAxis2Placement whiteSideLineAxes = IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1));
	pFemoralAxes->GetWhiteSideLineAxes(whiteSideLineAxes);

	if ( whiteSideLineAxes == IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1)) )
		CloseImplantImmediately(QString("FemoralAxes_GetWhiteSideLineAxes"));

	return whiteSideLineAxes;
}

IwAxis2Placement IFemurImplant::FemoralAxes_GetSweepAxes()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetSweepAxes"));;

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetSweepAxes"));

	IwAxis2Placement sweepAxes = IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1));
	pFemoralAxes->GetSweepAxes(sweepAxes);

	if ( sweepAxes == IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1)) )
		CloseImplantImmediately(QString("FemoralAxes_GetSweepAxes"));

	return sweepAxes;
}

double IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetFemurEpiCondylarSize"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetFemurEpiCondylarSize"));

	double size = -999;
	size = pFemoralAxes->GetEpiCondyleSize();

	if ( size == -999 )
		CloseImplantImmediately(QString("FemoralAxes_GetFemurEpiCondylarSize"));

	return size;
}

double IFemurImplant::FemoralAxes_GetFemurEpiCondylarSizeRatio()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetFemurEpiCondylarSizeRatio"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetFemurEpiCondylarSizeRatio"));

	double ratio = -999;
	ratio = pFemoralAxes->GetEpiCondyleSize()/76.0;// "76.0" is a golden standard size.

	if ( ratio == -999 )
		CloseImplantImmediately(QString("FemoralAxes_GetFemurEpiCondylarSizeRatio"));

	return ratio;
}

void IFemurImplant::FemoralAxes_GetAntCutPlaneInfo
(
	IwPoint3d& antCutPlaneCenter, 
	IwVector3d& antCutPlaneOrientation
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetAntCutPlaneInfo"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetAntCutPlaneInfo"));

	pFemoralAxes->GetAntCutPlaneInfo(antCutPlaneCenter, antCutPlaneOrientation);

	if ( !antCutPlaneCenter.IsInitialized() || !antCutPlaneOrientation.IsInitialized() )
		CloseImplantImmediately(QString("FemoralAxes_GetAntCutPlaneInfo"));

	return;
}

void IFemurImplant::FemoralAxes_GetMechAxis3Planes
(
	double refSizeRatio,				// I: reference size, percentage of epi-condylar size
	IwBSplineSurface*& coronalPlane,	// O: mech coronalPlane
	IwBSplineSurface*& sagittalPlane,	// O: mech sagittalPlane
	IwBSplineSurface*& distalPlane		// O: mech distalPlane
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetMechAxis3Planes"));

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		CloseImplantImmediately(QString("FemoralAxes_GetMechAxis3Planes"));

	double refSize;
	refSize = refSizeRatio*pFemoralAxes->GetEpiCondyleSize();

	IwAxis2Placement mechanicalAxes = IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1));
	pFemoralAxes->GetMechanicalAxes(mechanicalAxes);
	if ( mechanicalAxes == IwAxis2Placement(IwPoint3d(999,999,999), IwVector3d(1,1,1), IwVector3d(-1,-1,-1)) )
		CloseImplantImmediately(QString("FemoralAxes_GetMechAxis3Planes"));

	// Create 3 planes
	IwPoint3d origin = mechanicalAxes.GetOrigin();
	IwPoint3d xPoint = origin + refSize*mechanicalAxes.GetXAxis();
	IwPoint3d yPoint = origin + refSize*mechanicalAxes.GetYAxis();
	IwPoint3d zPoint = origin + refSize*mechanicalAxes.GetZAxis();
	IwPoint3d xyPoint = origin + refSize*mechanicalAxes.GetXAxis() + refSize*mechanicalAxes.GetYAxis();
	IwPoint3d yzPoint = origin + refSize*mechanicalAxes.GetYAxis() + refSize*mechanicalAxes.GetZAxis();
	IwPoint3d zxPoint = origin + refSize*mechanicalAxes.GetZAxis() + refSize*mechanicalAxes.GetXAxis();
	IwBSplineSurface *xyDistalPlane = NULL, *yzSagittalPlane = NULL, *zxCoronalPlane = NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), origin, xPoint, yPoint, xyPoint, xyDistalPlane);
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), origin, yPoint, zPoint, yzPoint, yzSagittalPlane);
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), origin, zPoint, xPoint, zxPoint, zxCoronalPlane);

	if (xyDistalPlane==NULL || yzSagittalPlane==NULL || zxCoronalPlane==NULL)
		CloseImplantImmediately(QString("FemoralAxes_GetMechAxis3Planes"));

	//
	coronalPlane = zxCoronalPlane;
	sagittalPlane = yzSagittalPlane;
	distalPlane = xyDistalPlane;

	return;
}

IwPoint3d GetIwPoint3d(const QString& strPoint)
{
	IwPoint3d point;
	QString str = strPoint;
	if ( !str.isEmpty() )
	{
		QStringList sl = str.split(",");
		if( sl.size() == 3 )
		{
			point = IwPoint3d( sl[0].toFloat(), sl[1].toFloat(), sl[2].toFloat());
		}
	}

	return point;
}

map<QString, IwPoint3d> IFemurImplant::FemoralAxesInp_PointsDataFromStrykerLayout()
{
	IwTArray<IwPoint3d> arrOfInputPoints;
	
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "FemAxes" + QDir::separator() + "AxesInfo.txt";

	QString					sLine;
	QFile					File( qstrCutInfoFilePath );
	QTextStream				in( &File );

	bool bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	IwTArray<QString> strListOfPoints;

	map<QString, IwPoint3d> pointMap;

	for(int idx = 1; idx <= 4; idx++)
	{
		QString pointName = in.readLine();

		sLine = in.readLine();
		IwPoint3d point = GetIwPoint3d(sLine);

		pointMap[pointName] = point*1000;
	}

	return pointMap;
}

void IFemurImplant::FemoralAxes_AntCenterPointAndAntRotCenterPoint(IwPoint3d &antPlaneCenter, IwPoint3d &antPlaneRotationCenter)
{
	CTotalDoc * pDoc = CTotalDoc::GetTotalDoc();

	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "Implant" + QDir::separator() + "ImplantInfo.txt";

	QString					sLine;
	QFile					File( qstrCutInfoFilePath );
	QTextStream				in( &File );

	bool bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	sLine = in.readLine();
	sLine = in.readLine();
	antPlaneRotationCenter = 1000*GetIwPoint3d(sLine);

	sLine = in.readLine();
	sLine = in.readLine();
	antPlaneCenter = 1000*GetIwPoint3d(sLine);
}

void IFemurImplant::PegPointsFromStrykerFemLayout(IwPoint3d &posPegPosition, IwPoint3d &negPegPosition)
{
	IwTArray<IwPoint3d> arrOfInputPoints;
	
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString strPegPoinsInfoFilePath = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "PegPoints" + QDir::separator() + "PegPointsInfo.txt";

	QString					sLine;
	QFile					File( strPegPoinsInfoFilePath );
	QTextStream				in( &File );

	bool bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	IwTArray<QString> strListOfPoints;

	map<QString, IwPoint3d> pointMap;

	for(int idx = 1; idx <= 2; idx++)
	{
		QString pointName = in.readLine();

		sLine = in.readLine();
		IwPoint3d point = GetIwPoint3d(sLine);

		pointMap[pointName] = point*1000;
	}

	File.close();

	CTotalDoc * pDoc = CTotalDoc::GetTotalDoc();

	QString sideInfo;
	CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);

	if (implantSide == IMPLANT_SIDE_LEFT)
	{
		posPegPosition = pointMap["Medial Peg Point"];
		negPegPosition = pointMap["Lateral Peg Point"];
	}
	else
	{
		negPegPosition = pointMap["Medial Peg Point"];
		posPegPosition = pointMap["Lateral Peg Point"];
	}
}

//JCurve Inputs By Erik
IwBSplineCurve * IFemurImplant::JCurvesInp_GetJCurve(int index)
{
	QString qstrJCurveFileName = "";

	if(index == 0) qstrJCurveFileName = "Medial_JCurve.IGS";
	else if(index == 1) qstrJCurveFileName = "Lateral_JCurve.IGS";
	else if(index == 2) qstrJCurveFileName = "Trochlea_JCurve.IGS";

	//CTotalDoc * pDoc = CTotalDoc::GetTotalDoc();
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");

	QString qstrJCurveLoc = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "JCurves" + QDir::separator() + qstrJCurveFileName;

	IwTArray<IwCurve*> arrOfCurves = ReadCurvesFromIges(qstrJCurveLoc);

	return arrOfCurves.GetSize() == 1 ? (IwBSplineCurve*)arrOfCurves[0] : NULL;
}

void IFemurImplant::JCurvesInp_GetJCurves(IwTArray<IwCurve*>& curves)
{
	IwTArray<IwCurve *> arrOfCurves;

	IwBSplineCurve * pJCurve = JCurvesInp_GetJCurve(0);
	arrOfCurves.Add(pJCurve);

	pJCurve = JCurvesInp_GetJCurve(1);
	arrOfCurves.Add(pJCurve);

	pJCurve = JCurvesInp_GetJCurve(2);
	arrOfCurves.Add(pJCurve);

	curves = arrOfCurves;
}

IwPoint3d IFemurImplant::JCurvesInp_GetWeightBearingMLCenterPoint()
{
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrPoinDataLoc = qstrFilePath + QDir::separator() + "JCurves" + QDir::separator() + "Points_Data.xml";

	CFakeXmlReader xmlReader = CFakeXmlReader( qstrPoinDataLoc );

	IwPoint3d weightBearingMLCenter;
	xmlReader.GetIwPoint3d("WeightBearingMLCenter", weightBearingMLCenter);

	return weightBearingMLCenter;
}

IwPoint3d IFemurImplant::JCurvesInp_GetMedialJPoint()
{
	//QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	//QString qstrPoinDataLoc = qstrFilePath + QDir::separator() + "JCurves" + QDir::separator() + "Points_Data.xml";

	//CFakeXmlReader xmlReader = CFakeXmlReader( qstrPoinDataLoc );

	//IwPoint3d medialJPoint;
	//xmlReader.GetIwPoint3d("MedialJPoint", medialJPoint);

	IwBSplineCurve * pMedialJCurve = JCurvesInp_GetJCurve(0);

	IwPoint3d endPoint1, endPoint2;
	pMedialJCurve->GetEnds(endPoint1, endPoint2);
	
	IwPoint3d medialJPoint = endPoint1.y < endPoint2.y ? endPoint1 : endPoint2;

	return medialJPoint;
}

// Outline Profile
IwBSplineSurface* IFemurImplant::OutlineProfile_GetSketchSurface
(
	bool bCopy
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchSurface"));

	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchSurface"));

	IwBSplineSurface* sketchSurface= NULL;
	pOutlineProfile->GetSketchSurface(sketchSurface);
	if ( sketchSurface == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchSurface"));

	if ( bCopy )
	{
		IwSurface* sketchSurface_copy= NULL;
		sketchSurface->Copy(pDoc->GetIwContext(), sketchSurface_copy);
		sketchSurface = (IwBSplineSurface*)sketchSurface_copy;
	}

	if ( sketchSurface == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchSurface"));

	return sketchSurface;
}

IwBSplineCurve* IFemurImplant::OutlineProfile_GetSketchCurve
(
	bool bCopy,
	IwBSplineCurve** uvCurve
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchCurve"));

	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchCurve"));

	IwBSplineCurve *sketchCurve = NULL, *sketchUVCurve = NULL;
	pOutlineProfile->GetCurveOnSketchSurface(sketchCurve, sketchUVCurve, false);
	if ( sketchCurve == NULL || sketchUVCurve == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchCurve"));

	if ( bCopy )
	{
		IwCurve *sketchCurve_copy = NULL;
		sketchCurve->Copy(pDoc->GetIwContext(), sketchCurve_copy);
		sketchCurve = (IwBSplineCurve*)sketchCurve_copy;
	}
	if ( sketchCurve == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetSketchCurve"));

	if ( uvCurve != NULL )
	{
		if ( bCopy )
		{
			IwCurve *sketchUVCurve_copy = NULL;
			sketchUVCurve->Copy(pDoc->GetIwContext(), sketchUVCurve_copy);
			sketchUVCurve = (IwBSplineCurve*)sketchUVCurve_copy;
		}
		if ( sketchUVCurve == NULL )
			CloseImplantImmediately(QString("OutlineProfile_GetSketchCurve"));

		*uvCurve = sketchUVCurve;
	}

	return sketchCurve;
}

void IFemurImplant::OutlineProfile_GetFeaturePoints
(
	IwTArray<IwPoint3d>& featurePointsUV,	// Please dig in for details
	IwTArray<int>& featurePointsInfo		// Please dig in for details
)
{
	featurePointsUV.RemoveAll();
	featurePointsInfo.RemoveAll();

	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetFeaturePoints"));

	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetFeaturePoints"));

	IwTArray<IwPoint3d> featurePointsRemap;
	pOutlineProfile->GetOutlineProfileFeaturePoints(featurePointsUV, featurePointsInfo, featurePointsRemap);

	if ( featurePointsUV.GetSize() == 0 || featurePointsUV.GetSize() != featurePointsInfo.GetSize() )
		CloseImplantImmediately(QString("OutlineProfile_GetFeaturePoints"));

	return;
}

///////////////////////////////////////////////////////////////////
// This is a service function.
int IFemurImplant::OutlineProfile_InsertAFeaturePointByOrder
(
	IwTArray<IwPoint3d>& featurePointsUV,	// I/O:
	IwTArray<int>& featurePointsInfo,		// I/O:
	IwPoint3d insertPoint					// I:
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return -1;

	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		return -1;
	// Create a fake remap array
	IwTArray<IwPoint3d>	remap;
	unsigned nSize = featurePointsUV.GetSize();
	for (unsigned i=0; i<nSize; i++)
		remap.Add(IwPoint3d(0,0,-1));

	int index = OutlineProfileMgrModel::InsertAFeaturePointByOrder(featurePointsUV, featurePointsInfo, remap, insertPoint);

	return index;
}

///////////////////////////////////////////////////////////////////
// This is a service function. The return could be NULL.
IwBSplineCurve* IFemurImplant::OutlineProfile_CreateUVCurveOnSketchSurface
(
	IwTArray<IwPoint3d>& featurePointsUV,	// I:
	IwTArray<int>& featurePointsInfo,		// I:
	IwBSplineCurve** xyzCurve				// O: 
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return NULL;

	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		return NULL;

	IwBSplineCurve *crvOnSurf, *uvCurve=NULL;
	pOutlineProfile->CreateCurveOnSketchSurface(featurePointsUV, featurePointsInfo, false, crvOnSurf, uvCurve);

	if ( xyzCurve != NULL )
	{
		*xyzCurve = crvOnSurf;
	}

	return uvCurve;
}

IwPoint3d IFemurImplant::OutlineProfile_GetWeightBearingMLCenter
(
	IwTArray<IwPoint3d>& MedialProfilePoints,	// O: medial side profile points at 0, 45, and 90 degrees
	IwTArray<IwPoint3d>& LateralProfilePoints	// O: lateral side profile points at 0, 45, and 90 degrees
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetWeightBearingMLCenter"));

	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		CloseImplantImmediately(QString("OutlineProfile_GetWeightBearingMLCenter"));

	IwPoint3d weightBearingCenter = pOutlineProfile->DetermineOutlineProfileWeightBearingMLCenter(&MedialProfilePoints, &LateralProfilePoints);

	if ( !weightBearingCenter.IsInitialized() )
		CloseImplantImmediately(QString("OutlineProfile_GetWeightBearingMLCenter"));

	return weightBearingCenter;
}

// Femoral Cuts
void IFemurImplant::FemoralCuts_GetCutsInfo
(
	IwTArray<IwPoint3d>& cutPoints,			// Please dig in for details
	IwTArray<IwVector3d>& cutOrientations	// Please dig in for details
)
{
	//QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	//QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "FemCut" + QDir::separator() + "CutInfo.xml";

	//CFakeXmlReader xmlReader = CFakeXmlReader( qstrCutInfoFilePath );

	//IwPoint3d pnt;

	//xmlReader.GetIwPoint3d("PosPostCutPoint", pnt);
	//cutPoints.Add(pnt);// 0th index

	//xmlReader.GetIwPoint3d("NegPostCutPoint", pnt);
	//cutPoints.Add(pnt);// 1th index

	//cutPoints.Add(IwPoint3d());
	//cutPoints.Add(IwPoint3d());
	//cutPoints.Add(IwPoint3d());
	//cutPoints.Add(IwPoint3d());
	////
	//xmlReader.GetIwPoint3d("MedialDistalCutPoint", pnt);
	//cutPoints.Add(pnt);// 6th index

	//xmlReader.GetIwPoint3d("LateralDistalCutPoint", pnt);
	//cutPoints.Add(pnt);//7th index

	//cutPoints.Add(IwPoint3d());
	//cutPoints.Add(IwPoint3d());
	//cutPoints.Add(IwPoint3d());

	//xmlReader.GetIwPoint3d("PosPostCutOrientation", pnt);
	//cutOrientations.Add(pnt);

	//xmlReader.GetIwPoint3d("NegPostCutOrientation", pnt);
	//cutOrientations.Add(pnt);

	//cutOrientations.Add(IwPoint3d());
	//cutOrientations.Add(IwPoint3d());
	//cutOrientations.Add(IwPoint3d());
	//cutOrientations.Add(IwPoint3d());
	//cutOrientations.Add(IwPoint3d());
	//cutOrientations.Add(IwPoint3d());
	//cutOrientations.Add(IwPoint3d());

	//xmlReader.GetIwPoint3d("AnteriorCutOrientation", pnt);
	//cutOrientations.Add(pnt);

	//////cutOrientations.Add(IwPoint3d());//
	//cutOrientations.Add(IwPoint3d());

	//return;

	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetCutsInfo"));

	CFemoralCuts* pFemoralCuts = pDoc->GetFemoralCuts();
	if ( pFemoralCuts == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetCutsInfo"));

	pFemoralCuts->GetCutsInfo(cutPoints, cutOrientations);

	if ( cutPoints.GetSize() == 0 || cutPoints.GetSize() != cutOrientations.GetSize() )
		CloseImplantImmediately(QString("FemoralCuts_GetCutsInfo"));

}

void IFemurImplant::FemoralCuts_GetCutsFeaturePoints
(
	IwTArray<IwPoint3d>& cutFeaturePoints,	// Please dig in for details
	IwTArray<IwPoint3d>& antOuterPoints		// Please dig in for details
)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetCutsFeaturePoints"));

	CFemoralCuts* pFemoralCuts = pDoc->GetFemoralCuts();
	if ( pFemoralCuts == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetCutsFeaturePoints"));

	pFemoralCuts->GetFeaturePoints(cutFeaturePoints, &antOuterPoints);

	if ( cutFeaturePoints.GetSize() == 0 )
		CloseImplantImmediately(QString("FemoralCuts_GetCutsFeaturePoints"));


	//cutFeaturePoints.RemoveAll();

	//cutFeaturePoints = FemoralCutsInp_GetCutsFeaturePoints();
}

int LessThan(IwPoint3d pnt1, IwPoint3d pnt2, void * addData = NULL)
{
	return pnt1.x < pnt2.x ? 1 : 0;
}

std::map<QString, IwPoint3d> IFemurImplant::GetStrykerImplantFeaturePointsMap()
{
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "Implant" + QDir::separator() + "ImplantInfo.txt";

	QString					sLine;
	QFile					File(qstrCutInfoFilePath);
	QTextStream				in(&File);

	bool bOK = File.open(QIODevice::ReadOnly | QIODevice::Text);

	IwTArray<QString> strListOfPoints;

	map<QString, IwPoint3d> pointMap;

	for (int idx = 1; idx <= 14; idx++)
	{
		QString pointName = in.readLine();

		sLine = in.readLine();
		IwPoint3d point = GetIwPoint3d(sLine);

		pointMap[pointName] = point * 1000;
	}

	File.close();

	return pointMap;
}

IwTArray<IwPoint3d> IFemurImplant::ImplantInp_GetCutsFeaturePointInfo()
{
	CTotalDoc * pDoc = CTotalDoc::GetTotalDoc();

	map<QString, IwPoint3d> pointMap = GetStrykerImplantFeaturePointsMap();

	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();

	IwAxis2Placement mechAxes;
	pFemoralAxes->GetMechanicalAxes(mechAxes);

	IwPoint3d medialPostPeekPoint, lateralPostPeekPoint;

	IwPoint3d midPoint = (pointMap["Medial Posterior Chamfer - Posterior Point"] + pointMap["Medial Posterior Chamfer - Posterior Notch Point"])/2;

	double dist = midPoint.DistanceBetween(pointMap["Medial Posterior Chamfer - Posterior Point"]);

	medialPostPeekPoint = midPoint + dist*mechAxes.GetZAxis();

	midPoint = (pointMap["Lateral Posterior Chamfer - Posterior Point"] + pointMap["Lateral Posterior Chamfer - Posterior Notch Point"])/2;

	dist = midPoint.DistanceBetween(pointMap["Lateral Posterior Chamfer - Posterior Point"]);

	lateralPostPeekPoint = midPoint + dist*mechAxes.GetZAxis();

	pointMap["Medial Posterior Peek Point"] = medialPostPeekPoint;
	pointMap["Lateral Posterior Peek Point"] = lateralPostPeekPoint;

	IwPoint3d medialAntDistalPoint, lateralAntDistalPoint;
	medialAntDistalPoint = pointMap["Medial Anterior Chamfer - Distal Point"];
	lateralAntDistalPoint = pointMap["Lateral Anterior Chamfer - Distal Point"];

	IwBSplineCurve * pLine = NULL;
	IwBSplineCurve::CreateLineSegment(pDoc->GetIwContext(), 3, medialAntDistalPoint, lateralAntDistalPoint, pLine);

	IwPoint3d medialDstlPostChamfNotchPoint, lateralDstlPostChamfNotchPoint;
	medialDstlPostChamfNotchPoint = pointMap["Medial Distal - Posterior Chamfer Notch Point"];
	lateralDstlPostChamfNotchPoint = pointMap["Lateral Distal - Posterior Chamfer Notch Point"];

	IwPoint3d medialAntDistalNotchPoint, lateralAntDistalNotchPoint;
	DropPointOnCurve(pLine, medialDstlPostChamfNotchPoint, &medialAntDistalNotchPoint);
	DropPointOnCurve(pLine, lateralDstlPostChamfNotchPoint, &lateralAntDistalNotchPoint);

	pointMap["Medial Anterior Chamfer - Distal Notch Point"] = medialAntDistalNotchPoint;
	pointMap["Lateral Anterior Chamfer - Distal Notch Point"] = lateralAntDistalNotchPoint;

	QString arrOfPointNames[] = {"Medial Posterior Peek Point",                "Lateral Posterior Peek Point",
	                             "Medial Posterior Chamfer - Posterior Point", "Medial Posterior Chamfer - Posterior Notch Point", "Lateral Posterior Chamfer - Posterior Notch Point", "Lateral Posterior Chamfer - Posterior Point",
	                             "Medial Distal - Posterior Chamfer Point",    "Medial Distal - Posterior Chamfer Notch Point",    "Lateral Distal - Posterior Chamfer Notch Point",    "Lateral Distal - Posterior Chamfer Point",
	                           	 "Medial Anterior Chamfer - Distal Point",     "Medial Anterior Chamfer - Distal Notch Point",     "Lateral Anterior Chamfer - Distal Notch Point",     "Lateral Anterior Chamfer - Distal Point",
	                           	 "Medial Anterior - Anterior Chamfer Point",   "Lateral Anterior - Anterior Chamfer Point"};

	QString sideInfo;
	CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);

	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		for(int idx = 0; idx < 16; idx++)
		{
			if(arrOfPointNames[idx].contains("Medial") == true)
			{
				arrOfPointNames[idx].replace("Medial", "Lateral");
				continue;
			}

			if(arrOfPointNames[idx].contains("Lateral") == true)
			{
				arrOfPointNames[idx].replace("Lateral", "Medial");
				continue;
			}
		}
	}

	IwTArray<IwPoint3d> arrOfCutFeaturePoints;

	for(int idx = 0; idx < 16; idx++)
	{
		QString strPointName = arrOfPointNames[idx];
		IwPoint3d point = pointMap[strPointName];

		arrOfCutFeaturePoints.Add(point);
	}

	//refinment wrt anterior side
	IwBSplineSurface* femurSurface = pDoc->GetFemur()->GetSinglePatchSurface();

	IwPoint3d posIntPoint, negIntPoint;
	IntersectSurfaceByLine(femurSurface, arrOfCutFeaturePoints[14] + 50*mechAxes.GetXAxis(), -1*mechAxes.GetXAxis(), posIntPoint);
	IntersectSurfaceByLine(femurSurface, arrOfCutFeaturePoints[15] - 50*mechAxes.GetXAxis(), 1*mechAxes.GetXAxis(), negIntPoint);

	double posShift, negShift;

	IwVector3d xAxis = mechAxes.GetXAxis();
	posShift = posIntPoint.Dot(xAxis) - arrOfCutFeaturePoints[14].Dot(xAxis);
	negShift = negIntPoint.Dot(xAxis) - arrOfCutFeaturePoints[15].Dot(xAxis);

	arrOfCutFeaturePoints[0] += xAxis*posShift;
	arrOfCutFeaturePoints[1] += xAxis*negShift;

	for(int idx = 2; idx < 14; idx++)
	{
		double shift = idx%4 == 2 || idx%4 == 3 ? posShift : negShift;

		arrOfCutFeaturePoints[idx] += xAxis*(shift/2);
	}

	arrOfCutFeaturePoints[14] += xAxis*posShift;
	arrOfCutFeaturePoints[15] += xAxis*negShift;

	return arrOfCutFeaturePoints;
}

IwBrep* IFemurImplant::GetStrykerFemCutBrep()
{
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "FemCut" + QDir::separator() + "CutFemur.igs";

	IwBrep* pFemCutBrep = ReadIGES(qstrCutInfoFilePath);

	return pFemCutBrep;
}

IwVector3d IFemurImplant::PosteriorFaceNormalVector()
{
	IwBrep* pFemCutBrep = GetStrykerFemCutBrep();

	IwTArray<IwFace*> arrOfFaces;
	pFemCutBrep->GetFaces(arrOfFaces);

	CTotalDoc * pDoc = CTotalDoc::GetTotalDoc();

	CFemoralAxes * pFemroalAxes = pDoc->GetFemoralAxes();

	IwAxis2Placement mechAxes;
	pFemroalAxes->GetMechanicalAxes(mechAxes);

	IwVector3d yAxes = mechAxes.GetYAxis();

	double minVal = 1000000;
	IwVector3d retNormal;
	for(int idx = 0; idx < arrOfFaces.GetSize(); idx++)
	{
		IwFace * pFace = arrOfFaces[idx];
		IwSurface * pSurf = pFace->GetSurface();

		IwPoint3d pnt, normal;
		if(pSurf->IsPlanar(0.001, &pnt, &normal))
		{
			double dot = normal.Dot(yAxes);

			if(dot < minVal)
			{
				retNormal = normal;
				minVal = dot;
			}
		}
	}

	return retNormal;
}

std::map<QString, IwPoint3d> IFemurImplant::GetStrykerFemLayoutCutPointsMap()
{
	map<QString, IwPoint3d> pointMap;

	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "Surfaces" + QDir::separator() + "FemCut" + QDir::separator() + "CutPointInfo.txt";

	QString					sLine;
	QFile					File(qstrCutInfoFilePath);
	QTextStream				in(&File);

	bool bOK = File.open(QIODevice::ReadOnly | QIODevice::Text);

	IwTArray<QString> strListOfPoints;

	for (int idx = 1; idx <= 8; idx++)
	{
		QString pointName = in.readLine();

		sLine = in.readLine();
		IwPoint3d point = GetIwPoint3d(sLine);

		pointMap[pointName] = point * 1000;
	}

	File.close();

	return pointMap;
}

void IFemurImplant::FemoralCutsInp_GetCutsInfo(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations)
{
	cutPoints.RemoveAll();
	cutOrientations.RemoveAll();

	map<QString, IwPoint3d> pointMap = GetStrykerFemLayoutCutPointsMap();

	CTotalDoc * pDoc = CTotalDoc::GetTotalDoc();

	CFemoralAxes * pFemroalAxes = pDoc->GetFemoralAxes();

	IwAxis2Placement mechAxes;
	pFemroalAxes->GetMechanicalAxes(mechAxes);

	map<QString, IwPoint3d> orientaionMap;
	IwVector3d normalVect = -1*PosteriorFaceNormalVector();//mechAxes.GetYAxis();
	orientaionMap["Medial Posterior Cut Face Point"] = -1*normalVect;//"Lateral Posterior Cut Face Point"
	orientaionMap["Lateral Posterior Cut Face Point"] = -1*normalVect;//

	orientaionMap["Anterior Cut Face Point"] = normalVect;//

	IwTArray<IwPoint3d> arrOfCutFeaturePoints = ImplantInp_GetCutsFeaturePointInfo();

	IwPoint3d point1, point2, point3;

	IwPoint3d antPlaneCenter, antPlaneRotCenter;
	FemoralAxes_AntCenterPointAndAntRotCenterPoint(antPlaneCenter, antPlaneRotCenter);

	point1 = antPlaneCenter;
	point2 = arrOfCutFeaturePoints[15];
	point3 = arrOfCutFeaturePoints[14];

	normalVect = (point1-point3)*(point2-point3);
	normalVect.Unitize();
	orientaionMap["Anterior Cut Face Point"] = -1*normalVect;

	point1 = arrOfCutFeaturePoints[14];
	point2 = arrOfCutFeaturePoints[15];
	point3 = arrOfCutFeaturePoints[10];

	normalVect = (point1-point3)*(point2-point3);
	normalVect.Unitize();
	orientaionMap["Anterior Chamfer Cut Face Point"] = -1*normalVect;

	point1 = arrOfCutFeaturePoints[10];
	point2 = arrOfCutFeaturePoints[11];
	point3 = arrOfCutFeaturePoints[6];

	normalVect = (point1-point3)*(point2-point3);
	normalVect.Unitize();

	orientaionMap["Medial Distal Cut Face Point"] = -1*normalVect;
	orientaionMap["Lateral Distal Cut Face Point"] = -1*normalVect;

    point1 = arrOfCutFeaturePoints[6];
	point2 = arrOfCutFeaturePoints[7];
	point3 = arrOfCutFeaturePoints[2];

	normalVect = (point1-point3)*(point2-point3);
	normalVect.Unitize();

	orientaionMap["Medial Posterior Chamfer Cut Face Point"] = -1*normalVect;
	orientaionMap["Lateral Posterior Chamfer Cut Face Point"] = -1*normalVect;

	//Cut Points
	IwPoint3d medialPostPoint = pointMap["Medial Posterior Cut Face Point"];
	IwPoint3d lateralPostPoint = pointMap["Lateral Posterior Cut Face Point"];

	IwPoint3d medialChamferPoint = pointMap["Medial Posterior Chamfer Cut Face Point"];
	IwPoint3d lateralChamferPoint = pointMap["Lateral Posterior Chamfer Cut Face Point"];

	IwPoint3d medialDistalPoint = pointMap["Medial Distal Cut Face Point"];
	IwPoint3d lateralDistalPoint = pointMap["Lateral Distal Cut Face Point"];

	IwPoint3d antChamferPoint = pointMap["Anterior Chamfer Cut Face Point"];
	IwPoint3d antPoint = pointMap["Anterior Cut Face Point"];

	//IwPoint3d stepCutPoint = avgPoint;//pointMap["Step Cut Point"];

	//Cut Orientations
	IwPoint3d medialPostOriention = -1*orientaionMap["Medial Posterior Cut Face Point"];
	IwPoint3d lateralPostOriention = -1*orientaionMap["Lateral Posterior Cut Face Point"];

	IwPoint3d medialChamferOriention = -1*orientaionMap["Medial Posterior Chamfer Cut Face Point"];
	IwPoint3d lateralChamferOriention = -1*orientaionMap["Lateral Posterior Chamfer Cut Face Point"];

	IwPoint3d medialDistalOriention = -1*orientaionMap["Medial Distal Cut Face Point"];
	IwPoint3d lateralDistalOriention = -1*orientaionMap["Lateral Distal Cut Face Point"];

	IwPoint3d antChamferOriention = -1*orientaionMap["Anterior Chamfer Cut Face Point"];
	IwPoint3d antOriention = -1*orientaionMap["Anterior Cut Face Point"];

	IwPoint3d stepCutOriention = -1*orientaionMap["Medial Distal Cut Face Point"];//orientaionMap["Step Cut Oriention"];

	QString sideInfo;
	CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);

	if (implantSide == IMPLANT_SIDE_LEFT)
	{
		//Cut points
		cutPoints.Add(medialPostPoint);
		cutPoints.Add(lateralPostPoint);

		cutPoints.Add(medialChamferPoint);
		cutPoints.Add(lateralChamferPoint);

		cutPoints.Add(medialChamferPoint);//Repetetion
		cutPoints.Add(lateralChamferPoint);

		cutPoints.Add(medialDistalPoint);
		cutPoints.Add(lateralDistalPoint);

		cutPoints.Add(antChamferPoint);
		cutPoints.Add(antPoint);

		cutPoints.Add(arrOfCutFeaturePoints[11]);

		//Cut Orientations
		cutOrientations.Add(medialPostOriention);
		cutOrientations.Add(lateralPostOriention);

		cutOrientations.Add(medialChamferOriention);
		cutOrientations.Add(lateralChamferOriention);

		cutOrientations.Add(medialChamferOriention);//Repetetion
		cutOrientations.Add(lateralChamferOriention);

		cutOrientations.Add(medialDistalOriention);
		cutOrientations.Add(lateralDistalOriention);

		cutOrientations.Add(antChamferOriention);
		cutOrientations.Add(antOriention);

		cutOrientations.Add(stepCutOriention);
	}
	else
	{
		//Cut Points
		cutPoints.Add(lateralPostPoint);
		cutPoints.Add(medialPostPoint);

		cutPoints.Add(lateralChamferPoint);
		cutPoints.Add(medialChamferPoint);

		cutPoints.Add(lateralChamferPoint);//Repetetion
		cutPoints.Add(medialChamferPoint);

		cutPoints.Add(lateralDistalPoint);
		cutPoints.Add(medialDistalPoint);

		cutPoints.Add(antChamferPoint);
		cutPoints.Add(antPoint);

		cutPoints.Add(arrOfCutFeaturePoints[12]);

		//Cut Orientations
		cutOrientations.Add(lateralPostOriention);
		cutOrientations.Add(medialPostOriention);

		cutOrientations.Add(lateralChamferOriention);
		cutOrientations.Add(medialChamferOriention);

		cutOrientations.Add(lateralChamferOriention);//Repetetion
		cutOrientations.Add(medialChamferOriention);

		cutOrientations.Add(lateralDistalOriention);
		cutOrientations.Add(medialDistalOriention);

		cutOrientations.Add(antChamferOriention);
		cutOrientations.Add(antOriention);

		cutOrientations.Add(stepCutOriention);

	}

}

void IFemurImplant::FemoralCuts_GetCondylarCutWidths(double& posWidth, double& negWidth)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetCondylarCutWidths"));

	CFemoralCuts* pFemoralCuts = pDoc->GetFemoralCuts();
	if ( pFemoralCuts == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetCondylarCutWidths"));

	// initialize with un-reason values
	posWidth = -999;
	negWidth = -999;
	pFemoralCuts->GetCondylarCutWidths(posWidth, negWidth);

	if ( posWidth == -999 || negWidth == -999 )
		CloseImplantImmediately(QString("FemoralCuts_GetCondylarCutWidths"));
}

IwBrep* IFemurImplant::FemoralCuts_GetIwBrep(bool bCopy)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetIwBrep"));

	CFemoralCuts *pFemoralCuts = pDoc->GetFemoralCuts();
	if ( pFemoralCuts == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetIwBrep"));

	IwBrep *pBrep=NULL, *copyBrep=NULL;
	pBrep = pFemoralCuts->GetIwBrep();

	if ( pBrep == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetIwBrep"));

	if (bCopy)
	{
		copyBrep = new (pDoc->GetIwContext()) IwBrep(*pBrep);
		pBrep = copyBrep;
	}

	if ( pBrep == NULL )
		CloseImplantImmediately(QString("FemoralCuts_GetIwBrep"));

	return pBrep;
}

IwTArray<IwPoint3d> IFemurImplant::FemoralCutsInp_GetCutsFeaturePoints()
{
	QString qstrFilePath = mainWindow->GetImplant()->GetInfo("ImplantDir");
	QString qstrCutInfoFilePath = qstrFilePath + QDir::separator() + "FemCut" + QDir::separator() + "cutFeaturePoints.xml";

	CFakeXmlReader xmlReader = CFakeXmlReader( qstrCutInfoFilePath );

	IwTArray<IwPoint3d> cutFeaturePoints;
	for(int idx = 1; idx <= 21; idx++)
	{
		IwPoint3d cuFeaturetPoint;
		 
		QString strPointIdx = "CutFeatPoint" + QString::number(idx);
		xmlReader.GetIwPoint3d(strPointIdx, cuFeaturetPoint);

		cutFeaturePoints.Add(cuFeaturetPoint);
	}

	cutFeaturePoints[6] = (cutFeaturePoints[2] + cutFeaturePoints[10])/2;
	cutFeaturePoints[7] = (cutFeaturePoints[3] + cutFeaturePoints[11])/2;
	cutFeaturePoints[8] = (cutFeaturePoints[4] + cutFeaturePoints[12])/2;
	cutFeaturePoints[9] = (cutFeaturePoints[5] + cutFeaturePoints[13])/2;

	//cutFeaturePoints[6].z =- 1.5;
	//cutFeaturePoints[7].z =- 1.5;
	//cutFeaturePoints[8].z =- 1.5;
	//cutFeaturePoints[9].z =- 1.5;


	return cutFeaturePoints;
}

///////////////////////////////////////////////////////////////////
// This is a service function.
// Display/Hide an entity
void IFemurImplant::DisplayHideEntity(CEntRole entRole, CDisplayMode dispMode)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return;

	CEntity*	pEntity = pDoc->GetEntity( entRole );
	if ( pEntity != NULL )
	{
		pEntity->SetDisplayMode(dispMode);
		if ( dispMode == DISP_MODE_HIDDEN || dispMode == DISP_MODE_NONE )
			pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		else
			pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
	}
		
}

///////////////////////////////////////////////////////////////////
// This is a service function. 
void IFemurImplant::HideAllFemurImplantEntities()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return;

	pDoc->HideAllFemurImplantEntities();
		
}

///////////////////////////////////////////////////////////////////
// This is a service function. 
// Femur Implant Info
bool IFemurImplant::IsFemurImplantComplete()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return false;

	bool isComplete = pDoc->IsFemoralImplantDesignComplete();

	return isComplete;
}

///////////////////////////////////////////////////////////////////
// This is a service function. 
bool IFemurImplant::IsFemurImplantWithValidAPMLSizes()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return false;

	bool validSize = pDoc->IsFemoralImplantDesignWithValidAPMLSizes();

	return validSize;
}

///////////////////////////////////////////////////////////////////
// This is a service function. 
void IFemurImplant::Review_StartReviewFemur()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return;

	pDoc->GetView()->GetMainWindow()->OnFemoralImplantReview();

}

void IFemurImplant::Review_DisplayReviewCommentDialog(CEntRole eRole)
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		CloseImplantImmediately(QString("Review_DisplayReviewCommentDialog"));
	CTotalView* pView = pDoc->GetView();
	if ( pView == NULL )
		CloseImplantImmediately(QString("Review_DisplayReviewCommentDialog"));

	pView->DisplayReviewCommentDialog(eRole);
}
