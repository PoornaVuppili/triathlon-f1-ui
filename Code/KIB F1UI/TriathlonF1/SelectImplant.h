#ifndef SELECTIMPLANT_H
#define SELECTIMPLANT_H

#include <QDialog>
#include <QDir>
#include <QMessageBox>
#include <QComboBox>
#include <QCheckBox>
#include <qdom.h>
#include <vector>

// using namespace std;
namespace Ui {class SelectImplantClass;};

class SelectImplant : public QDialog
{
    Q_OBJECT

public:
    SelectImplant(QWidget *parent/* = 0*/);
    ~SelectImplant();

    QString GetBasePath()        const { return basePath; }
    QString GetImplantName()     const { return implantName; }
    QString GetImplantFolder()   const { return caseFolderName; }
    QString GetImplantType()     const { return implantType; }
    QString GetKneeType()        const { return kneeType; }
    QString GetCompartmentType() const { return compartmentType; }
    QString GetCountry()         const { return country; }
    QString GetZone()            const { return zone; }

    QString GetImgDir()         const { return imgDir; }
    QString GetSegmentDir()     const { return segDir; }
    QString GetSurfaceDir()     const { return surfDir; }
    QString GetSTLDir()         const { return stlDir; }
    QString GetSolidWorksDir()  const { return sldworksDir; }

    // Set the current implant name
    void SetImplantName(QString& name);

    // Whether the (single) knee series is a reformat
    bool IsKneeSeriesReformat() { return useReformat; }

    // Get the femural implant file
    QString GetFemuralImplantFile();

protected:
    void PopulateComboBox();
    void UpdateUI();
    void UpdateButton();
    bool WarnSpacingThickness();

    // Update the property file
    bool UpdatePropertyFile();

private: // methods

	void ClearCaseList();
    QString GetSpacingThicknessString(const QString& name);

	void UpdateBasePathAllThingsConsidered();

private: // data
    Ui::SelectImplantClass* ui;

    QString basePath, implantName, caseFolderName, imgDir, segDir, surfDir, stlDir, sldworksDir;
    QString kneeCor, kneeSag, kneeAxi, hipAxi, ankleAxi;
    QString implantType, kneeType, compartmentType;
    QString country, zone;

    bool modified;
    bool useReformat;
	bool isUserProcessingaCase;

    QVector<QComboBox*> vecCombo;
    QVector<QCheckBox*> vecCheck;
    // for erad scan x,y spacing warning
    QHash<QString,bool> nonSquarePixSpacing, flipPixSpacing, eradScan;
    // for slice spacing and thickness warning
    QHash<QString,float> sliceSpacing, sliceThickness;

	private slots:
    void on_pushButtonBrowse_clicked();
    void on_pushButtonNewSerial_clicked();
    void on_LineEdit_editingFinished();
    void on_pushButtonOpenReformat_clicked();
    void on_pushButtonReformat_clicked();
    void on_pushButtonImport_clicked();
    void on_comboImplantList_indexChanged(int);
    void on_pushButtonCancel_clicked();
    void on_pushButtonOK_clicked();
	void on_allOptStateChanged(int state);

	void onAdjustSize();
};

#endif // SELECTIMPLANT_H
