#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "SelectedEntity.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

///////////////////////////////////////////////////////////////////////////////////////////
// Methods for CSelectedEntity
///////////////////////////////////////////////////////////////////////////////////////////
CSelectedEntity::CSelectedEntity()
{
	point = IwPoint3d();
	curve = NULL;
	edge = NULL;
	face = NULL;
	closestPoint = IwPoint3d();
}

CSelectedEntity::~CSelectedEntity()
{

}

///////////////////////////////////////////////////////////////
// offsetVec to offset the displayed point toward the vector.
// Do NOT use offsetVec when display within a display list.
// Only use it when realtime display the entity.
///////////////////////////////////////////////////////////////
void CSelectedEntity::Display(IwVector3d offsetVec, double onePixelSize)
{
	if (IsEmpty()) return;

	if (point != IwPoint3d())
	{
		glBegin(GL_POINTS);
			glVertex3d( point.x+offsetVec.x, point.y+offsetVec.y, point.z+offsetVec.z );
		glEnd();
	}
	else if (curve != NULL)
	{
		HighLightCurve(curve);
	}
	else if (edge != NULL)
	{
		HighLightEdge(edge);
	}
	else if (face != NULL)
	{
		HighLightFace(face, onePixelSize);
	}
}

void CSelectedEntity::HighLightCurve(IwCurve* curve)
{
	if (curve==NULL) return;

	IwPoint3d	pt;
	IwPoint3d	sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	IwExtent1d sIvl = curve->GetNaturalInterval();

	double	dChordHeightTol = 0.75;
	double	dAngleTolInDegrees = 8.0;

	IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

	sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

	int nPoints = sPnts.GetSize();

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
	{
		pt = sPnts.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();
}

void CSelectedEntity::HighLightEdge(IwEdge* eg)
{
	if ( eg == NULL) return;

	IwPoint3d	pt;
	IwPoint3d	sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	IwExtent1d sIvl = eg->GetInterval();

	double	dChordHeightTol = 0.75;
	double	dAngleTolInDegrees = 8.0;

	IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

	IwCurve* curve = eg->GetCurve();
	sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

	int nPoints = sPnts.GetSize();

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
	{
		pt = sPnts.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();
}

void CSelectedEntity::HighLightFaceEdge(IwEdgeuse* edgeUse, double onePixelSize)
{
	if ( onePixelSize < 0.000001 )
		onePixelSize = 0.000001;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	int displayNo = 0;

	IwCurve* curve = edgeUse->GetEdge()->GetCurve();
	IwExtent1d domEdge = edgeUse->GetEdge()->GetInterval();
	double length = 0;
	curve->Length(domEdge, 0.01, length);
	displayNo = (int)ceil(length/(50*onePixelSize));
	if (displayNo > 50) displayNo = 50;
	double delta = domEdge.GetLength()/(displayNo+1);

	IwPoint3d edgePnt, edgePlusPnt;
	IwVector3d binormal, offVec;

	
	for (double param = domEdge.GetMin()+0.5*delta; param < (domEdge.GetMax()-0.25*delta); param+=delta)
	{
		edgeUse->EvaluateBinormal(param, FALSE, edgePnt, binormal);
		edgePlusPnt = edgePnt + 5*onePixelSize*binormal;

		glBegin( GL_LINES );
			glVertex3d( edgePnt.x, edgePnt.y, edgePnt.z );
			glVertex3d( edgePlusPnt.x, edgePlusPnt.y, edgePlusPnt.z );
		glEnd();
	}
}

void CSelectedEntity::HighLightFace(IwFace* fc, double onePixelSize)
{
	if (fc == NULL) return;

	IwTArray<IwEdge*> edgeList;
	IwEdge* eg;
	fc->GetEdges(edgeList);

	// Highlight edges
	for (unsigned i=0; i<edgeList.GetSize(); i++)
	{
		eg = edgeList.GetAt(i);
		HighLightEdge(eg);
	}

	// Highlight face edges
	IwTArray<IwLoop*> loops;
	face->GetLoops(loops);
	IwLoop* firstLoop = loops.GetAt(0);
	IwLoopuse *pLU1, *pLU2;
	firstLoop->GetLoopuses(pLU1,pLU2);
	IwTArray<IwEdgeuse*> edgeUses;
	pLU1->GetEdgeuses(edgeUses);
	for (unsigned i=0; i< edgeUses.GetSize(); i++)
	{
		IwEdgeuse* edgeuse = edgeUses.GetAt(i);
		HighLightFaceEdge(edgeuse, onePixelSize);
	}

}

void CSelectedEntity::DisplayClosestPoint()
{
	if (IsEmpty()) return;

	if (closestPoint != IwPoint3d())
	{
		glBegin(GL_POINTS);
			glVertex3d( closestPoint.x, closestPoint.y, closestPoint.z );
		glEnd();
	}
}

void CSelectedEntity::DisplayHitPoint()
{
	if (IsEmpty()) return;

	if (hitPoint != IwPoint3d())
	{
		glBegin(GL_POINTS);
			glVertex3d( hitPoint.x, hitPoint.y, hitPoint.z );
		glEnd();
	}
}
bool CSelectedEntity::IsEmpty()
{
	bool empty = true;

	if ( point != IwPoint3d() ||
		 curve != NULL ||
		 edge != NULL ||
		 face != NULL )
	{
		empty = false;
	}

	return empty;
}

bool CSelectedEntity::IsClosestPointValid()
{
	bool valid = true;
	if (closestPoint == IwPoint3d())
		valid = false;

	return valid;
}

void CSelectedEntity::Empty()
{
	point = IwPoint3d();
	curve = NULL;
	edge = NULL;
	face = NULL;
	closestPoint = IwPoint3d();
}

void CSelectedEntity::AddPoint(IwPoint3d& pnt)
{
	point = pnt;
	curve = NULL;
	edge = NULL;
	face = NULL;
	closestPoint = IwPoint3d();
}

void CSelectedEntity::AddCurve
(
	IwCurve*& crv,
	IwPoint3d& hitPnt, 
	double hitPntU
)
{
	point = IwPoint3d();
	curve = crv;
	edge = NULL;
	face = NULL;
	hitPoint = hitPnt;
	hitPointUV = IwPoint2d(hitPntU, 0);
	closestPoint = IwPoint3d();
}

void CSelectedEntity::AddEdge
(
	IwEdge*& eg,
	IwPoint3d& hitPnt, 
	double hitPntU
)
{
	point = IwPoint3d();
	curve = NULL;
	edge = eg;
	face = NULL;
	hitPoint = hitPnt;
	hitPointUV = IwPoint2d(hitPntU, 0);
	closestPoint = IwPoint3d();
}

void CSelectedEntity::AddFace
(
	IwFace*& fc,
	IwPoint3d& hitPnt, 
	IwPoint2d& hitPntUV
)
{
	point = IwPoint3d();
	curve = NULL;
	edge = NULL;
	face = fc;
	hitPoint = hitPnt;
	hitPointUV = hitPntUV;
	closestPoint = IwPoint3d();
}

double CSelectedEntity::DistanceBetween(CSelectedEntity& other, bool* isMinDist, bool localResult)
{
	IwContext& iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();

	bool localIsMinDist = true;

	double dist = -1000;
	IwCurve *thisCrv = NULL, *otherCrv = NULL;
	IwExtent1d thisIntval, otherIntval;

	// condese this curve and edge
	if (this->curve != NULL)
	{
		thisCrv = this->curve;
		thisIntval = thisCrv->GetNaturalInterval();
	}
	else if (this->edge != NULL)
	{
		thisCrv = this->edge->GetCurve();
		thisIntval = this->edge->GetInterval();
	}

	// condese this curve and edge
	if (other.curve != NULL)
	{
		otherCrv = other.curve;
		otherIntval = otherCrv->GetNaturalInterval();
	}
	else if (other.edge != NULL)
	{
		otherCrv = other.edge->GetCurve();
		otherIntval = other.edge->GetInterval();
	}

	IwSolutionArray sols;
	IwSolution sol;
	IwPoint3d pnt, pnt2;

	if (this->point.IsInitialized())
	{
		// point - point
		if (other.point.IsInitialized())
		{
			// update the closest point
			closestPoint = this->point;
			other.closestPoint = other.point;
			// update distance
			dist = this->point.DistanceBetween(other.point);
		}
		// point - curve
		else if ( otherCrv != NULL )
		{
			if (otherCrv->IsLinear())
			{
				IwPoint3d stPnt, edPnt;
				otherCrv->GetEnds(stPnt, edPnt);
				IwVector3d vec = edPnt - stPnt;
				vec.Unitize();
				IwVector3d vec2 = this->point - stPnt;
				IwVector3d cross = vec2*vec;
				dist = cross.Length();
				// determine closest points
				this->closestPoint = this->point;
				other.closestPoint = stPnt + vec.Dot(vec2)*vec;
			}
			else
			{
				otherCrv->GlobalPointSolve(otherIntval, IW_SO_MINIMIZE, this->point, 0.0001, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() > 0)
				{
					sol = sols[0];
					otherCrv->EvaluatePoint(sol.m_vStart[0], pnt);
					// update the closest point
					closestPoint = this->point;
					other.closestPoint = pnt;
					// update distance
					dist = sol.m_vStart.m_dSolutionValue;
				}
			}
		}
		// point - face
		else if ( other.face != NULL )
		{
			IwPoint3d facePnt, faceNormal;
			if (other.face->GetSurface()->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
			{
				IwVector3d vec = this->point - facePnt;
				dist = fabs(vec.Dot(faceNormal));
				// determine the closest point
				closestPoint = this->point;
				other.closestPoint = this->point.ProjectPointToPlane(facePnt, faceNormal);
			}
			else
			{
				IwBrep* otherFaceBrep = new (iwContext) IwBrep();
				IwBrep* otherBrep = other.face->GetBrep();
				IwTArray<IwFace*> faces;
				faces.Add(other.face);
				otherBrep->CopyFaces(faces, otherFaceBrep);
				IwTopologySolver::BrepPointSolve(otherFaceBrep, this->point, IW_SO_MINIMIZE, IW_SR_ALL, 0.00001, IW_BIG_DOUBLE, NULL, sols);
				if (sols.GetSize() > 0)
				{
					IwSurface *otherSurf = other.face->GetSurface();
					double minDist = 10000000;
					for (unsigned i=0; i<sols.GetSize(); i++)
					{
						sol = sols[i];
						if (sol.m_vStart.m_dSolutionValue < minDist)
						{
							IwObject* pObject = (IwObject*)sol.m_apObjects[0];
							if (pObject->IsKindOf(IwEdge_TYPE))
							{
								double edgeParam = sol.m_vStart.m_adParameters[0];
								IwEdge* myEdge = (IwEdge*)pObject;
								IwCurve* myCrv = myEdge->GetCurve();
								myCrv->EvaluatePoint(edgeParam, pnt);
							}
							else if (pObject->IsKindOf(IwFace_TYPE))
							{
								IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
								otherSurf->EvaluatePoint(faceUV, pnt);
							}
							else if (pObject->IsKindOf(IwVertex_TYPE))
							{
								IwVertex* vertex = (IwVertex*)pObject;
								pnt = vertex->GetPoint();
							}
							other.closestPoint = pnt;
							minDist = sol.m_vStart.m_dSolutionValue;
						}
					}
					// update this point closest point
					closestPoint = this->point;
					// update distance
					dist = minDist;
				}
				IwObjDelete delBrep(otherFaceBrep);
			}
		}
	}
	else if ( thisCrv != NULL )
	{
		// curve - point
		if (other.point.IsInitialized())
		{
			if (thisCrv->IsLinear())
			{
				IwPoint3d stPnt, edPnt;
				thisCrv->GetEnds(stPnt, edPnt);
				IwVector3d vec = edPnt - stPnt;
				vec.Unitize();
				IwVector3d vec2 = other.point - stPnt;
				IwVector3d cross = vec2*vec;
				dist = cross.Length();
				// determine closest points
				this->closestPoint = stPnt + vec.Dot(vec2)*vec;
				other.closestPoint = other.point;
			}
			else
			{
				thisCrv->GlobalPointSolve(thisIntval, IW_SO_MINIMIZE, other.point, 0.0001, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() > 0)
				{
					sol = sols[0];
					thisCrv->EvaluatePoint(sol.m_vStart[0], pnt);
					// update the closest point
					closestPoint = pnt;
					other.closestPoint = other.point;
					// update distance
					dist = sol.m_vStart.m_dSolutionValue;
				}
			}
		}
		// curve - curve
		else if ( otherCrv != NULL )
		{
			IwBoolean foundAnswer;
			thisCrv->LocalCurveSolve(thisIntval, *otherCrv, otherIntval, IW_SO_MINIMIZE, 0.0001, NULL, NULL, this->hitPointUV.x, other.hitPointUV.x, foundAnswer, sol);
			if (localResult && foundAnswer)
			{
				thisCrv->EvaluatePoint(sol.m_vStart[0], pnt);
				otherCrv->EvaluatePoint(sol.m_vStart[1], pnt2);
				// update the closest point
				closestPoint = pnt;
				other.closestPoint = pnt2;
				// update distance
				dist = sol.m_vStart.m_dSolutionValue;
				// the local solver only return a stable solution. It is not necessary min or max.
				// We need to test whether it is a min/max distance.
				IwPoint3d pntTest;
				double tolDist = 0.0001;
				thisCrv->EvaluatePoint(sol.m_vStart[0]+0.001, pntTest);
				otherCrv->LocalPointSolve(otherIntval, IW_SO_MINIMIZE, pntTest, &tolDist, NULL, NULL, sol.m_vStart[1], foundAnswer, sol);
				if (foundAnswer)
				{
					if ( dist > sol.m_vStart.m_dSolutionValue )
						localIsMinDist = false;
				}
			}
			else if (thisCrv->IsLinear() && otherCrv->IsLinear())
			{
				IwPoint3d thisStPnt, thisEdPnt, otherStPnt, otherEdPnt;
				thisCrv->GetEnds(thisStPnt, thisEdPnt);
				otherCrv->GetEnds(otherStPnt, otherEdPnt);
				IwVector3d thisVec = thisEdPnt - thisStPnt;
				IwVector3d otherVec = otherEdPnt - otherStPnt;
				if (thisVec.IsParallelTo(otherVec, 0.25))
				{
					IwVector3d vec = otherStPnt - thisStPnt;
					thisVec.Unitize();
					IwVector3d cross = thisVec*vec;
					dist = cross.Length();
				}
			}
			else
			{
				thisCrv->GlobalCurveSolve(thisIntval, *otherCrv, otherIntval, IW_SO_MINIMIZE, 0.0001, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() > 0)
				{
					sol = sols.GetAt(0);
					thisCrv->EvaluatePoint(sol.m_vStart[0], pnt);
					otherCrv->EvaluatePoint(sol.m_vStart[1], pnt2);
					// update the closest point
					closestPoint = pnt;
					other.closestPoint = pnt2;
					// update distance
					dist = sol.m_vStart.m_dSolutionValue;
				}
			}
		}
		// curve - face
		else if (other.face != NULL)
		{
		///////// Local Solution //////////////////////////////////////////////////////
			IwSurface* otherSurface = other.face->GetSurface();
			IwExtent2d otherUVDomain = other.face->GetUVDomain();
			IwBoolean foundAnswer;
			otherSurface->LocalCurveSolve(otherUVDomain, *thisCrv, thisIntval,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, other.hitPointUV, this->hitPointUV.x, foundAnswer, sol);
			if (localResult && foundAnswer)
			{
				thisCrv->EvaluatePoint(sol.m_vStart[0], pnt);
				IwVector2d paramUV(sol.m_vStart[1], sol.m_vStart[2]);
				otherSurface->EvaluatePoint(paramUV, pnt2);
				// update the closest point
				closestPoint = pnt;
				other.closestPoint = pnt2;
				// update distance
				dist = sol.m_vStart.m_dSolutionValue;
				// The local solver returns a stable solution. It is not necessary min or max.
				// We need to test it.
				IwPoint3d pntTest;
				thisCrv->EvaluatePoint(sol.m_vStart[0]+0.001, pntTest);
				otherSurface->LocalPointSolve(otherUVDomain, IW_SO_MINIMIZE, pntTest, other.hitPointUV, foundAnswer, sol);
				if (foundAnswer)
				{
					if ( dist > sol.m_vStart.m_dSolutionValue )
						localIsMinDist = false;
				}
			}
		////////Global Solution////////////////////////////////////////////////////////
			else
			{
				IwBrep* otherFaceBrep = new (iwContext) IwBrep();
				IwBrep* otherBrep = other.face->GetBrep();
				IwTArray<IwFace*> faces;
				faces.Add(other.face);
				otherBrep->CopyFaces(faces, otherFaceBrep);
				IwTopologySolver::BrepCurveSolve(otherFaceBrep, *thisCrv, thisIntval, IW_SO_MINIMIZE, IW_SR_ALL, 0.00001, IW_BIG_DOUBLE, NULL, sols);
				if (sols.GetSize() > 0)
				{
					IwSurface *otherSurf = other.face->GetSurface();
					double minDist = 10000000;
					for (unsigned i=0; i<sols.GetSize(); i++)
					{
						sol = sols[i];
						if (sol.m_vStart.m_dSolutionValue < minDist)
						{
							double crvParam = sol.m_vStart.m_adParameters[0];
							thisCrv->EvaluatePoint(crvParam, pnt);
							closestPoint = pnt;
							// the closest point may be on edge or surface
							IwObject *pObject = (IwObject*)sol.m_apObjects[1];
							if (pObject->IsKindOf(IwEdge_TYPE))
							{
								double edgeParam = sol.m_vStart.m_adParameters[1];
								IwEdge* myEdge = (IwEdge*)pObject;
								IwCurve* myCrv = myEdge->GetCurve();
								myCrv->EvaluatePoint(edgeParam, pnt2);
							}
							else if (pObject->IsKindOf(IwFace_TYPE))
							{
								IwVector2d faceUV(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
								otherSurf->EvaluatePoint(faceUV, pnt2);
							}
							else if (pObject->IsKindOf(IwVertex_TYPE))
							{
								IwVertex* vertex = (IwVertex*)pObject;
								pnt2 = vertex->GetPoint();
							}
							other.closestPoint = pnt2;
							minDist = sol.m_vStart.m_dSolutionValue;
						}
					}
					// update distance
					dist = minDist;
				}
				IwObjDelete delBrep(otherFaceBrep);
			}
		}
	}
	else if ( this->face != NULL )
	{
		// face - point
		if (other.point.IsInitialized())
		{
			IwPoint3d facePnt, faceNormal;
			if (this->face->GetSurface()->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
			{
				IwVector3d vec = other.point - facePnt;
				dist = fabs(vec.Dot(faceNormal));
				// determine the closest point
				other.closestPoint = other.point;
				this->closestPoint = other.point.ProjectPointToPlane(facePnt, faceNormal);
			}
			else
			{
				IwBrep* thisFaceBrep = new (iwContext) IwBrep();
				IwBrep* thisBrep = this->face->GetBrep();
				IwTArray<IwFace*> faces;
				faces.Add(this->face);
				thisBrep->CopyFaces(faces, thisFaceBrep);
				IwTopologySolver::BrepPointSolve(thisFaceBrep, other.point, IW_SO_MINIMIZE, IW_SR_ALL, 0.00001, IW_BIG_DOUBLE, NULL, sols);
				if (sols.GetSize() > 0)
				{
					IwSurface *thisSurf = this->face->GetSurface();
					double minDist = 10000000;
					for (unsigned i=0; i<sols.GetSize(); i++)
					{
						sol = sols[i];
						if (sol.m_vStart.m_dSolutionValue < minDist)
						{
							IwObject* pObject = (IwObject*)sol.m_apObjects[0];
							if (pObject->IsKindOf(IwEdge_TYPE))
							{
								double edgeParam = sol.m_vStart.m_adParameters[0];
								IwEdge* myEdge = (IwEdge*)pObject;
								IwCurve* myCrv = myEdge->GetCurve();
								myCrv->EvaluatePoint(edgeParam, pnt);
							}
							else if (pObject->IsKindOf(IwFace_TYPE))
							{
								IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
								thisSurf->EvaluatePoint(faceUV, pnt);
							}
							else if (pObject->IsKindOf(IwVertex_TYPE))
							{
								IwVertex* vertex = (IwVertex*)pObject;
								pnt = vertex->GetPoint();
							}
							this->closestPoint = pnt;
							minDist = sol.m_vStart.m_dSolutionValue;
						}
					}
					// update this point closest point
					other.closestPoint = other.point;
					// update distance
					dist = minDist;
				}
				IwObjDelete delBrep(thisFaceBrep);
			}
		}
		// face - curve
		else if (otherCrv != NULL)
		{
			////////// Local Solution //////////////////////////////////////
			IwSurface* thisSurface = this->face->GetSurface();
			IwExtent2d thisUVDomain = this->face->GetUVDomain();
			IwBoolean foundAnswer;
			thisSurface->LocalCurveSolve(thisUVDomain, *otherCrv, otherIntval,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, this->hitPointUV, other.hitPointUV.x, foundAnswer, sol);
			if (localResult && foundAnswer)
			{
				otherCrv->EvaluatePoint(sol.m_vStart[0], pnt);
				IwVector2d paramUV(sol.m_vStart[1], sol.m_vStart[2]);
				thisSurface->EvaluatePoint(paramUV, pnt2);
				// update the closest point
				other.closestPoint = pnt;
				this->closestPoint = pnt2;
				// update distance
				dist = sol.m_vStart.m_dSolutionValue;
				// The local solver returns a stable solution. It is not necessary min or max.
				// We need to test it.
				IwPoint3d pntTest;
				otherCrv->EvaluatePoint(sol.m_vStart[0]+0.001, pntTest);
				thisSurface->LocalPointSolve(thisUVDomain, IW_SO_MINIMIZE, pntTest, this->hitPointUV, foundAnswer, sol);
				if (foundAnswer)
				{
					if ( dist > sol.m_vStart.m_dSolutionValue )
						localIsMinDist = false;
				}
			}
			////////// Global Solution /////////////////////////////////////
			else
			{
				IwBrep* thisFaceBrep = new (iwContext) IwBrep();
				IwBrep* thisBrep = this->face->GetBrep();
				IwTArray<IwFace*> faces;
				faces.Add(this->face);
				thisBrep->CopyFaces(faces, thisFaceBrep);
				IwTopologySolver::BrepCurveSolve(thisFaceBrep, *otherCrv, otherIntval, IW_SO_MINIMIZE, IW_SR_ALL, 0.00001, IW_BIG_DOUBLE, NULL, sols);
				if (sols.GetSize() > 0)
				{
					IwSurface *thisSurf = this->face->GetSurface();
					double minDist = 10000000;
					for (unsigned i=0; i<sols.GetSize(); i++)
					{
						sol = sols[i];
						if (sol.m_vStart.m_dSolutionValue < minDist)
						{
							double crvParam = sol.m_vStart.m_adParameters[0];
							otherCrv->EvaluatePoint(crvParam, pnt);
							other.closestPoint = pnt;
							// the closest point may be on edge or face
							IwObject *pObject = (IwObject*)sol.m_apObjects[1];
							if (pObject->IsKindOf(IwEdge_TYPE))
							{
								double edgeParam = sol.m_vStart.m_adParameters[1];
								IwEdge* myEdge = (IwEdge*)pObject;
								IwCurve* myCrv = myEdge->GetCurve();
								myCrv->EvaluatePoint(edgeParam, pnt2);
							}
							else if (pObject->IsKindOf(IwFace_TYPE))
							{
								IwVector2d faceUV(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
								thisSurf->EvaluatePoint(faceUV, pnt2);
							}
							else if (pObject->IsKindOf(IwVertex_TYPE))
							{
								IwVertex* vertex = (IwVertex*)pObject;
								pnt2 = vertex->GetPoint();
							}
							this->closestPoint = pnt2;
							minDist = sol.m_vStart.m_dSolutionValue;
						}
					}
					// update distance
					dist = minDist;
				}
				IwObjDelete delBrep(thisFaceBrep);
			}
		}
		// face - face
		else if ( other.face != NULL )
		{
			/////////// Local Solution /////////////////////////////////////////////
			IwSurface* thisSurface = this->face->GetSurface();
			IwExtent2d thisUVDomain = this->face->GetUVDomain();
			IwPoint3d thisPnt, thisNormal;
			IwBoolean thisIsPlanar = thisSurface->IsPlanar(0.01, &thisPnt, &thisNormal, 0);
			IwSurface* otherSurface = other.face->GetSurface();
			IwExtent2d otherUVDomain = other.face->GetUVDomain();
			IwPoint3d otherPnt, otherNormal;
			IwBoolean otherIsPlanar = otherSurface->IsPlanar(0.01, &otherPnt, &otherNormal, 0);

			if ( thisIsPlanar && otherIsPlanar && thisNormal.IsParallelTo(otherNormal, 0.01) )
			{
				IwVector3d tempVec = thisPnt - otherPnt;
				dist = fabs(tempVec.Dot(thisNormal));
			}
			else
			{
				IwBoolean foundAnswer;
				thisSurface->LocalSurfaceSolve(thisUVDomain, *otherSurface, otherUVDomain,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, this->hitPointUV, other.hitPointUV, foundAnswer, sol);
				if (localResult && foundAnswer)
				{
					IwVector2d paramUV(sol.m_vStart[0], sol.m_vStart[1]);
					thisSurface->EvaluatePoint(paramUV, pnt);
					IwVector2d otherParamUV(sol.m_vStart[2], sol.m_vStart[3]);
					otherSurface->EvaluatePoint(otherParamUV, pnt2);
					// update the closest point
					this->closestPoint = pnt;
					other.closestPoint = pnt2;
					// update distance
					dist = sol.m_vStart.m_dSolutionValue;
					// The local solver returns a stable solution. It is not necessary min or max.
					// We need to test it.
					IwPoint3d pntTest;
					thisSurface->EvaluatePoint(paramUV+IwVector2d(0.001, 0.001), pntTest);
					otherSurface->LocalPointSolve(otherUVDomain, IW_SO_MINIMIZE, pntTest, other.hitPointUV, foundAnswer, sol);
					if (foundAnswer)
					{
						if ( dist > sol.m_vStart.m_dSolutionValue )
							localIsMinDist = false;
					}
				}
				/////////// Global Solution /////////////////////////////////////////////
				else
				{
					IwBrep* thisFaceBrep = new (iwContext) IwBrep();
					IwBrep* thisBrep = this->face->GetBrep();
					IwTArray<IwFace*> thisFaces;
					thisFaces.Add(this->face);
					thisBrep->CopyFaces(thisFaces, thisFaceBrep);
					IwBrep* otherFaceBrep = new (iwContext) IwBrep();
					IwBrep* otherBrep = other.face->GetBrep();
					IwTArray<IwFace*> otherFaces;
					otherFaces.Add(other.face);
					otherBrep->CopyFaces(otherFaces, otherFaceBrep);
					IwTopologySolver::BrepBrepSolve(thisFaceBrep, otherFaceBrep, IW_SO_MINIMIZE, IW_SR_ALL, 0.00001, IW_BIG_DOUBLE, NULL, sols);
					if (sols.GetSize() > 0)
					{
						IwSurface *thisSurf = this->face->GetSurface();
						IwSurface *otherSurf = other.face->GetSurface();
						double minDist = 10000000;
						for (unsigned i=0; i<sols.GetSize(); i++)
						{
							sol = sols[i];
							if (sol.m_vStart.m_dSolutionValue < minDist)
							{
								int next_adParamIndex = 0;;
								// the closest point may be on edge or face
								IwObject *pObject0 = (IwObject*)sol.m_apObjects[0];
								if (pObject0->IsKindOf(IwEdge_TYPE))
								{
									double edgeParam = sol.m_vStart.m_adParameters[0];
									IwEdge* myEdge = (IwEdge*)pObject0;
									IwCurve* myCrv = myEdge->GetCurve();
									myCrv->EvaluatePoint(edgeParam, pnt);
									next_adParamIndex = 1;
								}
								else if (pObject0->IsKindOf(IwFace_TYPE))
								{
									IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
									thisSurf->EvaluatePoint(faceUV, pnt);
									next_adParamIndex = 2;
								}
								else if (pObject0->IsKindOf(IwVertex_TYPE))
								{
									IwVertex* vertex = (IwVertex*)pObject0;
									pnt = vertex->GetPoint();
									next_adParamIndex = 0;
								}
								this->closestPoint = pnt; // pObject0 may be "this" object, but we do not care here.

								// handle object1
								IwObject *pObject1 = (IwObject*)sol.m_apObjects[1];
								if (pObject1->IsKindOf(IwEdge_TYPE))
								{
									double edgeParam = sol.m_vStart.m_adParameters[next_adParamIndex];
									IwEdge* myEdge = (IwEdge*)pObject1;
									IwCurve* myCrv = myEdge->GetCurve();
									myCrv->EvaluatePoint(edgeParam, pnt2);
								}
								else if (pObject1->IsKindOf(IwFace_TYPE))
								{
									IwVector2d faceUV(sol.m_vStart.m_adParameters[next_adParamIndex], sol.m_vStart.m_adParameters[next_adParamIndex+1]);
									otherSurf->EvaluatePoint(faceUV, pnt2);
								}
								else if (pObject1->IsKindOf(IwVertex_TYPE))
								{
									IwVertex* vertex = (IwVertex*)pObject1;
									pnt2 = vertex->GetPoint();
								}
								other.closestPoint = pnt2;
								minDist = sol.m_vStart.m_dSolutionValue;
							}
						}
						// update distance
						dist = minDist;
					}
				IwObjDelete delBrep(thisFaceBrep);
				IwObjDelete delBrep1(otherFaceBrep);
				}
			}
		}
	}
	
	if (dist == 10000000) // no solution
		dist = -1.0;

	if ( isMinDist != NULL )
		*isMinDist = localIsMinDist;

	return dist;
}

IwVector3d CSelectedEntity::VectorBetween
(
	CSelectedEntity& other,		// I:
	IwPoint3d& thisPoint,		// O:
	IwPoint3d& otherPoint		// O:
)
{
	IwVector3d vecBetween = IwVector3d(0,0,0);

	// Determine the distance first to get the closest points info;
	this->DistanceBetween(other);

	if (this->IsClosestPointValid() && other.IsClosestPointValid())
	{
		thisPoint = this->point;
		otherPoint = other.point;
		vecBetween = otherPoint - thisPoint;
	}

	return vecBetween;
}

bool CSelectedEntity::IsIdentical(CSelectedEntity& other)
{
	bool same = false;

	if (this->IsEmpty() && other.IsEmpty() ) return true;

	if ( this->point == other.point &&
		 this->curve == other.curve &&
		 this->edge == other.edge &&
		 this->face == other.face
		)
	{
		same = true;
	}

	return same;
}

IwCurve* CSelectedEntity::GetCurve(bool curveOnly)
{
	if (curve) 
		return curve;

	if ( !curveOnly && edge)
	{
		IwCurve* crv = edge->GetCurve();
		return crv;
	}

	return NULL;
}

IwEdge* CSelectedEntity::GetEdge()
{
	if (edge) 
		return edge;

	return NULL;
}

IwFace* CSelectedEntity::GetFace()
{
	if (face) 
		return face;

	return NULL;
}

IwSurface* CSelectedEntity::GetSurface()
{
	if (face)
	{
		IwSurface* surface = face->GetSurface();
		return surface;
	}

	return NULL;
}
