#include "IFemurImplantPS.h"
#include "TotalDocPS.h"
#include "TotalViewPS.h"

IFemurImplantPS::IFemurImplantPS()
{

}

IFemurImplantPS::~IFemurImplantPS()
{

}

bool IFemurImplantPS::MotionPatternsExtendToolbar()
{
	bool bExtendToolbar = false;

	return bExtendToolbar;
}

void IFemurImplantPS::CloseImplantImmediately(QString& errorIFunctionName)
{
	KTotalDocPS* pDoc = KTotalDocPS::GetTotalDocPS();
	if ( pDoc )
	{
		pDoc->AppendLog( QString("IFemurImplantPS::CloseImplantWithErrorMessages(), errorIFunctionName = ") + errorIFunctionName );
		// When error out here, we do not want to pop up error messages for femoral implant data modification.
		// Force pDoc data modification to be false.
		pDoc->SetModified(false);
	}

	QString msg;
	msg = QString("Software encounters unexpected errors\nwhen retrieving data from %0.\nSoftware will be terminated.").arg(errorIFunctionName);

	mainWindow->CloseImplantImmediately(msg);

}

///////////////////////////////////////////////////////////////////
// This is a service function. 
bool IFemurImplantPS::IsFemoralSidePSFeaturesComplete()
{
	KTotalDocPS* pDoc = KTotalDocPS::GetTotalDocPS();
	if ( pDoc == NULL )
		return false;

	bool isComplete = pDoc->IsFemoralSidePSFeaturesDesignComplete();

	return isComplete;
}