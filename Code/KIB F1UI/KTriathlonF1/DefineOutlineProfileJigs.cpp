#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "DefineOutlineProfileJigs.h"
#include "OutlineProfileJigs.h"
#include "CartilageSurface.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "FemoralPart.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "UndoRedoCmdJigs.h"
#include "AdvancedControlJigs.h"
#include "..\KApp\SwitchBoard.h"

#include "OutlineProfile.h"
#include "FemoralAxes.h"

CDefineOutlineProfileJigs::CDefineOutlineProfileJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
    EnableUndoStack(true);

	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW );

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();
	
	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->StartTimeLog("DefineOutlineProfileJigs");

	m_sClassName = "CDefineOutlineProfileJigs";

	m_toolbar = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;
	m_actReset = NULL;
	m_actAdd = NULL;
	m_actDel = NULL;
	m_actValidate = NULL;
	m_oldInnerSurfaceJigsUpToDate = false;
	m_oldOuterSurfaceJigsUpToDate = false;
	m_predefinedViewIndex = 0;

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Outline Profile");
		m_actDefineOutlineProfileJigs = m_toolbar->addAction("Outline");
		SetActionAsTitle( m_actDefineOutlineProfileJigs );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );		
		m_actAdd = m_toolbar->addAction(QIcon( ":/KTriathlonF1/Resources/Plus.png" ),QString(""));
		m_actAdd->setToolTip("Add a control point");
		m_actAdd->setShortcut(QKeySequence("Ctrl+P"));
		EnableAction( m_actAdd, true );
		m_actDel = m_toolbar->addAction(QIcon( ":/KTriathlonF1/Resources/Minus.png" ),QString(""));
		m_actDel->setToolTip("Delete a control point");
		m_actDel->setShortcut(QKeySequence("Alt+M"));
		EnableAction( m_actDel, true );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		m_actCancel = m_toolbar->addAction("Cancel");
		EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAdd, SIGNAL( triggered() ), this, SLOT( OnAdd() ) );
		m_pMainWindow->connect( m_actDel, SIGNAL( triggered() ), this, SLOT( OnDel() ) );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );
	}
	else if (m_manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Outline Profile");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );

		m_actDefineOutlineProfileJigs = m_toolbar->addAction("Outline");
		SetActionAsTitle( m_actDefineOutlineProfileJigs );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		m_pMainWindow->connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		m_pMainWindow->connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		m_pMainWindow->connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	m_leftButtonDown = false;
	m_bMouseMiddleDown = false;
	m_addDeleteMode = 0;
	m_sweepAxis = IFemurImplant::FemoralAxes_GetSweepAxes();// for display purpose

	// Get the runtime data
	COutlineProfileJigs*			outlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( outlineProfileJigs == NULL )
	{
		outlineProfileJigs = new COutlineProfileJigs( m_pDoc );
		m_pDoc->AddEntity( outlineProfileJigs, true );
		// Reset is the way to construct the outline profile.
		Reset(activateUI);
	}
	else
	{
		// We want to know both InnerSurfaceJigs & OuterSurfaceJigs are up-to-date or not when entering into this step.
		CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
		if ( pInnerSurfaceJigs )
			m_oldInnerSurfaceJigsUpToDate = pInnerSurfaceJigs->GetUpToDateStatus();
		COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
		if ( pOuterSurfaceJigs )
			m_oldOuterSurfaceJigsUpToDate = pOuterSurfaceJigs->GetUpToDateStatus();

		// retrieve data from outlineProfileJigs
		IwTArray<IwPoint3d> totalSearchList;
		outlineProfileJigs->GetOutlineProfileFeaturePoints(m_oldFeaturePoints, m_oldFeaturePointsInfo);
		outlineProfileJigs->GetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo);
		outlineProfileJigs->ConvertUVToXYZ(m_currFeaturePoints, m_currFeaturePointsXYZ);
		m_currFeaturePointsXYZ.RemoveLast();// Note, the last one is never selectable.
		totalSearchList.Append(m_currFeaturePointsXYZ);

		outlineProfileJigs->GetDroppingCenters(m_oldPosDroppingCenter, m_oldNegDroppingCenter);
		outlineProfileJigs->GetDroppingCenters(m_currPosDroppingCenter, m_currNegDroppingCenter);
		totalSearchList.Add(m_currPosDroppingCenter);
		totalSearchList.Add(m_currNegDroppingCenter);

		outlineProfileJigs->GetOutlineProfileTabPoints(m_oldTabPoints);
		outlineProfileJigs->GetOutlineProfileTabEndPoints(m_oldTabEndPoints);
		outlineProfileJigs->GetOutlineProfileTabPoints(m_currTabPoints);
		outlineProfileJigs->GetOutlineProfileTabEndPoints(m_currTabEndPoints);
		totalSearchList.Append(m_currTabPoints);
		totalSearchList.Append(m_currTabEndPoints);

		// By calling Set functions to update m_outlineProfileProjectedCurve for display purpose.
		if ( !outlineProfileJigs->GetUpToDateStatus() )
		{
			outlineProfileJigs->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo);
			outlineProfileJigs->SetOutlineProfileTabPoints(m_currTabPoints);
			outlineProfileJigs->SetOutlineProfileTabEndPoints(m_currTabEndPoints);
		}

		outlineProfileJigs->GetWindowCutAngles(m_oldWindowCutAngles);
		outlineProfileJigs->GetWindowCutAngles(m_currWindowCutAngles, &m_currWindowCutTips, NULL, NULL);
		DetermineWindowCutDistancesToProfile(m_pDoc, m_windowCutDistancesToProfile);
		totalSearchList.Append(m_currWindowCutTips);

		SetMouseMoveSearchingList(totalSearchList);

		outlineProfileJigs->GetOutlineProfileReferencePoints(m_referencePoints);

		OnValidate();
	}


	// Display the viewing normal region
	if ( activateUI )
	{
		// display sketch surface
		outlineProfileJigs->SetDisplaySketchSurface(true);
		// Calculate sketch surface normal and viewing normal
		CalculateSketchSurfaceNormalInfo();
		DetermineViewingNormalRegion();
	}

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

	m_pView->Redraw();
}

CDefineOutlineProfileJigs::~CDefineOutlineProfileJigs()
{
	COutlineProfileJigs*			outlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( outlineProfileJigs != NULL )
	{
		int previoisDesignTime = outlineProfileJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		outlineProfileJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}

	SwitchBoard::removeSignalSender(this);
}

bool CDefineOutlineProfileJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	bool continuousResponse = true;
    m_posStart = cursor;
	m_leftButtonDown = true;

	IwPoint3d caughtPnt;
	int caughtIndex;
	if ( GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
	{
		continuousResponse = false;
	}

	if (m_manActType == MAN_ACT_TYPE_REVIEW) 
	{
		m_leftButtonDown = false;// no response to MouseUp if under review
	}

	ReDraw();

	return continuousResponse;
}

bool CDefineOutlineProfileJigs::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_bMouseMiddleDown = true;
	m_leftButtonDown = false;
	ReDraw();

	return true;
}

bool CDefineOutlineProfileJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{
	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	if (m_manActType == MAN_ACT_TYPE_REVIEW) // Remember if under review, m_leftButtonDown is always false.
	{
		// if the clicked-down is the same as the button-up point,
		// change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}
	else if ( m_leftButtonDown ) 
	{
		IwPoint3d caughtPoint;
		int caughtIndex;
		bool gotCaughtPoint = GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);
		COutlineProfileJigs* outlineProfileJigs = m_pDoc->GetOutlineProfileJigs(); 
		bool gotSketchSurf=false;
		IwBSplineSurface* skchSurf=NULL;
		if (outlineProfileJigs)
			gotSketchSurf = outlineProfileJigs->GetSketchSurface(skchSurf);

		//// Update the mouse move caught point /////////////////
		if ( m_addDeleteMode == 0 && gotCaughtPoint)
		{
			m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::MouseUp(), m_addDeleteMode = move, caughtIndex: %1, caughtPoint: %2 %3 %4").arg(caughtIndex).arg(caughtPoint.x).arg(caughtPoint.y).arg(caughtPoint.z) );

			// if the clicked-down is the same as the button-up point, do nothing
			// **** NOTE **** DO NOT CHANGE THIS CONDITION UNLESS YOU SEE THE MouseDoubleClickLeft().
			if (m_posStart == cursor)
			{
				m_leftButtonDown = false;
				return false;
			}

			CDefineOutlineProfileJigsUndoData prevUndoData, postUndoData;
			// Get the undo data before any changes
			GetDefineOutlineProfileJigsUndoData(prevUndoData);
			// if caught the outline profile feature points
			int featPntSize = (int)m_currFeaturePointsXYZ.GetSize();
			IwVector3d viewVec = m_pView->GetViewingVector();
			IwPoint3d backWardPnt = caughtPoint - 200*viewVec; // move the caught point closer to the viewer, in case no intersection with the sketch surface
			if (caughtIndex < featPntSize && gotSketchSurf)
			{
				IwPoint3d interPnt;
				double uvPnt[2];
				bool gotIntersectPnt = IntersectSurfaceByLine((IwSurface*)skchSurf, backWardPnt, viewVec, interPnt, uvPnt);
				if (gotIntersectPnt)
				{
					IwPoint3d prevUVPnt = m_currFeaturePoints.GetAt(caughtIndex);
					// update the UVPnt
					IwPoint3d interUVPnt = IwPoint3d(uvPnt[0], uvPnt[1], 0);
					// Check interUVPnt is a valid position (not between any arcs)
					if ( IsAValidPosition(interUVPnt, m_currFeaturePointsInfo.GetAt(caughtIndex)) )
					{
						m_currFeaturePoints.SetAt(caughtIndex, interUVPnt);
						// if it is an arc center point
						if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 1 || m_currFeaturePointsInfo.GetAt(caughtIndex) == 2 ||
							 m_currFeaturePointsInfo.GetAt(caughtIndex) == 11 || m_currFeaturePointsInfo.GetAt(caughtIndex) == 12 )
						{
							if ( QApplication::keyboardModifiers() == Qt::ControlModifier ) // control key is down
							{
								IwVector3d deltaVec = interUVPnt - prevUVPnt;
								IwPoint3d arcSatrt = m_currFeaturePoints.GetAt(caughtIndex-1);
								m_currFeaturePoints.SetAt(caughtIndex-1, arcSatrt+deltaVec);
								IwPoint3d arcEnd = m_currFeaturePoints.GetAt(caughtIndex+1);
								m_currFeaturePoints.SetAt(caughtIndex+1, arcEnd+deltaVec);
							}
						}
						else if ( caughtIndex == 0 && QApplication::keyboardModifiers() == Qt::ControlModifier )// if it is the anterior seam point + ctrl
						{
							IwVector3d deltaVec = interUVPnt - prevUVPnt;
							IwPoint3d pnt;
							// positive side arc
							pnt = m_currFeaturePoints.GetAt(caughtIndex+1);// arc start point
							m_currFeaturePoints.SetAt(caughtIndex+1, pnt+deltaVec);
							pnt = m_currFeaturePoints.GetAt(caughtIndex+2);// arc center point
							m_currFeaturePoints.SetAt(caughtIndex+2, pnt+deltaVec);
							pnt = m_currFeaturePoints.GetAt(caughtIndex+3);// arc end point
							m_currFeaturePoints.SetAt(caughtIndex+3, pnt+deltaVec);
							// positive transition point
							pnt = m_currFeaturePoints.GetAt(caughtIndex+4);// transition point
							m_currFeaturePoints.SetAt(caughtIndex+4, pnt+0.75*deltaVec);

							// negative side arc
							unsigned nsize = m_currFeaturePointsInfo.GetSize();
							pnt = m_currFeaturePoints.GetAt(nsize-4);// arc start point
							m_currFeaturePoints.SetAt(nsize-4, pnt+deltaVec);
							pnt = m_currFeaturePoints.GetAt(nsize-3);// arc center point
							m_currFeaturePoints.SetAt(nsize-3, pnt+deltaVec);
							pnt = m_currFeaturePoints.GetAt(nsize-2);// arc end point
							m_currFeaturePoints.SetAt(nsize-2, pnt+deltaVec);
							// negative transition point
							pnt = m_currFeaturePoints.GetAt(nsize-5);// transition point
							m_currFeaturePoints.SetAt(nsize-5, pnt+0.75*deltaVec);
						}
					}
				}
			}
			else if ( caughtIndex == featPntSize || caughtIndex == (featPntSize+1))// dropping centers
			{
				IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
				if ( caughtIndex == featPntSize ) // m_currPosDroppingCenter
				{
					m_currPosDroppingCenter = caughtPoint.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());// always on xy plane
				}
				else if ( caughtIndex == (featPntSize+1) ) // m_currNegDroppingCenter
				{
					m_currNegDroppingCenter = caughtPoint.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());// always on xy plane
				}
			}
			else if (caughtIndex < (featPntSize+2+6) ) // tab points
			{
				int thisTabIndex = caughtIndex - (featPntSize+2);
				IwPoint3d originalTabPoint = m_currTabPoints.GetAt(thisTabIndex);

				bool moved = UpdateTabPointForMouseUpEvent(caughtPoint, caughtIndex, viewVec, 2.0);
				// If anterior tab, reset its counter tab end point by calling UpdateTabPointForMouseUpEvent()
				if (thisTabIndex == 0 || thisTabIndex == 1 )
				{
					int counterTabIndex = (thisTabIndex == 0) ? 1 : 0;
					int counterCaughtIndex = counterTabIndex + (featPntSize+2);
					UpdateTabPointForMouseUpEvent(m_currTabPoints.GetAt(counterTabIndex), counterCaughtIndex, viewVec, 2.0);
				}

				Qt::KeyboardModifiers keyModifier = QApplication::keyboardModifiers();
				if ( keyModifier == Qt::ControlModifier && moved ) // the control key is down, also move its counter part point.
				{
					int itsCounterIndex;
					if (thisTabIndex %2 == 0) 
						itsCounterIndex = thisTabIndex + 1; // 0/2/4 -> 1/3/5
					else
						itsCounterIndex = thisTabIndex - 1; // 1/3/5 -> 0/2/4
					IwPoint3d originalTabCounterPoint = m_currTabPoints.GetAt(itsCounterIndex);
					double originalWidth = originalTabCounterPoint.DistanceBetween(originalTabPoint);
					IwPoint3d itsCounterCaughtPoint = m_currTabPoints.GetAt(itsCounterIndex) + (caughtPoint - originalTabPoint);
					int itsCounterCaughtIndex = itsCounterIndex + (featPntSize+2);
					// Update counter point
					UpdateTabCounterPointForMouseUpEvent(itsCounterCaughtPoint, itsCounterCaughtIndex, viewVec, 20.0, m_currTabPoints.GetAt(thisTabIndex), originalWidth);
				}
			}
			else if (caughtIndex < (featPntSize+2+6+6) )// tab end points
			{
				CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
				if ( pInnerSurfaceJigs )
				{
					IwBrep* innerSurfaceJigsBrep = pInnerSurfaceJigs->GetIwBrep();
					if (innerSurfaceJigsBrep)
					{
						// The tab end point is always on the same iso-curve as the tab point,
						// except the anterior tab anterior end (the first tab index)
						int tabEndIndex = caughtIndex - (featPntSize+2+6);
						IwTArray<IwFace*> faces;
						innerSurfaceJigsBrep->GetFaces(faces);
						IwBSplineSurface* innerSurf = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
						if ( tabEndIndex == 0 || tabEndIndex == 1 ) // anterior tab end
						{
							IwPoint3d surfPnt;
							double thisUV[2];
							IntersectSurfaceByLine(innerSurf, caughtPoint, viewVec, surfPnt, thisUV); 
							m_currTabEndPoints.SetAt(tabEndIndex, surfPnt);
						}
						else // the other tab ends
						{
							// Get iso-curve of the corresponding tab point
							IwPoint3d surfPnt, crvPnt, linePnt;
							double crvParam, lineParam;
							IwPoint2d uvParam;
							IwBSplineCurve* isoCurve=NULL;
							DistFromPointToSurface(m_currTabPoints.GetAt(tabEndIndex), innerSurf, surfPnt, uvParam);
							innerSurf->CreateIsoParametricCurve(m_pDoc->GetIwContext(),  IW_SP_U, uvParam.x, 0.1, isoCurve);
							// Create a viewing line
							IwLine* viewingLine=NULL;
							IwLine::CreateCanonical(m_pDoc->GetIwContext(), caughtPoint, viewVec, viewingLine);
							double dist = DistFromCurveToCurve(isoCurve, viewingLine, crvPnt, crvParam, linePnt, lineParam); 
							if (dist != -1)// Got the answer
								m_currTabEndPoints.SetAt(tabEndIndex, crvPnt);
							if (isoCurve)
								IwObjDelete(isoCurve);
							if (viewingLine)
								IwObjDelete(viewingLine);
						}
					}
				}
			}
			else // window cut tips
			{
				// determine the window cut angles
				IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
				IwTArray<double> windowCutAngles;
				IwPoint3d posCoreHoleCenter, negCoreHoleCenter;
				outlineProfileJigs->GetWindowCutAngles(windowCutAngles, NULL, &posCoreHoleCenter, &negCoreHoleCenter);
				IwPoint3d projPnt;
				projPnt = caughtPoint.ProjectPointToPlane(posCoreHoleCenter, wslAxes.GetZAxis());
				IwVector3d tempVec;
				double angleRadian;
				if ( caughtIndex == (featPntSize+2+6+6) )// positive F1 window cut tip
				{
					IwPoint3d projPnt = caughtPoint.ProjectPointToPlane(posCoreHoleCenter, wslAxes.GetZAxis());
					tempVec = projPnt - posCoreHoleCenter;
					tempVec.AngleBetween(wslAxes.GetYAxis(), angleRadian);
					if ( tempVec.Dot(wslAxes.GetXAxis()) > 0 )
						angleRadian = -angleRadian;
					double posWindowCutAngle = floor(angleRadian*180.0/IW_PI+0.5);// round to nearest integer
					m_currWindowCutAngles.SetAt(0,posWindowCutAngle);
				}
				else if ( caughtIndex == (featPntSize+2+6+6+1) )// negative F1 window cut tip
				{
					IwPoint3d projPnt = caughtPoint.ProjectPointToPlane(negCoreHoleCenter, wslAxes.GetZAxis());
					tempVec = projPnt - negCoreHoleCenter;
					tempVec.AngleBetween(wslAxes.GetYAxis(), angleRadian);
					if ( tempVec.Dot(wslAxes.GetXAxis()) < 0 )
						angleRadian = -angleRadian;
					double negWindowCutAngle = floor(angleRadian*180.0/IW_PI+0.5);// round to nearest integer
					m_currWindowCutAngles.SetAt(1,negWindowCutAngle);
				}
				else if ( caughtIndex == (featPntSize+2+6+6+2) )// positive F2 window cut tip
				{
					IwPoint3d projPnt = caughtPoint.ProjectPointToPlane(posCoreHoleCenter, wslAxes.GetZAxis());
					tempVec = projPnt - posCoreHoleCenter;
					tempVec.AngleBetween(wslAxes.GetYAxis(), angleRadian);
					if ( !positiveSideIsLateral )// positive side is medial
						angleRadian = angleRadian - 23.4/180.0*IW_PI; // why 23.4? a weird start part
					double posWindowCutAngleF2 = floor(angleRadian*180.0/IW_PI+0.5);// round to nearest integer
					m_currWindowCutAngles.SetAt(2,posWindowCutAngleF2);
				}
				else if ( caughtIndex == (featPntSize+2+6+6+3) )// negative F2 window cut tip
				{
					IwPoint3d projPnt = caughtPoint.ProjectPointToPlane(negCoreHoleCenter, wslAxes.GetZAxis());
					tempVec = projPnt - negCoreHoleCenter;
					tempVec.AngleBetween(wslAxes.GetYAxis(), angleRadian);
					if ( positiveSideIsLateral )// negative side is medial
						angleRadian = angleRadian - 23.4/180.0*IW_PI; // why 23.4? a weird start part
					double posWindowCutAngleF2 = floor(angleRadian*180.0/IW_PI+0.5);// round to nearest integer
					m_currWindowCutAngles.SetAt(3,posWindowCutAngleF2);
				}
			}
			// Get the undo data after changes
			GetDefineOutlineProfileJigsUndoData(postUndoData);

			QUndoCommand *defineOutlineProfileJigsCmd = new DefineOutlineProfileJigsCmd( this, prevUndoData, postUndoData);
			undoStack->push( defineOutlineProfileJigsCmd );
		}
		//// Update the adding point /////////////////
		else if (m_addDeleteMode == 1)
		{
			m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::MouseUp(), m_addDeleteMode = add, cursor: %1 %2").arg(cursor.x()).arg(cursor.y()) );

			QApplication::restoreOverrideCursor();
			m_addDeleteMode = 0;
			EnableAction( m_actDel, true );
			EnableAction( m_actAdd, true );
			//
			IwPoint3d curPnt;
			m_pView->UVtoXYZ(cursor, curPnt);
			IwVector3d viewVec = m_pView->GetViewingVector();
			IwPoint3d backWardPnt = curPnt - 200*viewVec; // move the caught point close to the viewer, in case no intersection with the sketch surface
			IwPoint3d interPnt;
			double uvPnt[2];
			bool gotIntersectPnt = IntersectSurfaceByLine((IwSurface*)skchSurf, backWardPnt, viewVec, interPnt, uvPnt);
			if (gotIntersectPnt && outlineProfileJigs)
			{
				CDefineOutlineProfileJigsUndoData prevUndoData, postUndoData;
				// Get the undo data before any changes
				GetDefineOutlineProfileJigsUndoData(prevUndoData);

				// update the change
				IwPoint3d interUVPnt = IwPoint3d(uvPnt[0], uvPnt[1], 0);
				int index = IFemurImplant::OutlineProfile_InsertAFeaturePointByOrder(m_currFeaturePoints, m_currFeaturePointsInfo, interUVPnt);
				if (index>=0)
				{
					// Get the undo data after changes
					GetDefineOutlineProfileJigsUndoData(postUndoData);
					QUndoCommand *defineOutlineProfileJigsCmd = new DefineOutlineProfileJigsCmd( this, prevUndoData, postUndoData);
					undoStack->push( defineOutlineProfileJigsCmd );
				}
			}
		}
		//// Update the deleting point /////////////////
		else if (m_addDeleteMode == 2)
		{
			m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::MouseUp(), m_addDeleteMode = delete, cursor: %1 %2").arg(cursor.x()).arg(cursor.y()) );

			QApplication::restoreOverrideCursor();
			m_addDeleteMode = 0; // none
			EnableAction( m_actDel, true );

			if (gotCaughtPoint && outlineProfileJigs)
			{
				// Check whether the caught point is delete-able
				// only interpolation points can be deleted. But the first point can NOT be detelted. 
				if ( caughtIndex != 0 && m_currFeaturePointsInfo.GetAt(caughtIndex) == 0 ) 
				{
					CDefineOutlineProfileJigsUndoData prevUndoData, postUndoData;
					// Get the undo data before any changes
					GetDefineOutlineProfileJigsUndoData(prevUndoData);

					// update the change
					m_currFeaturePoints.RemoveAt(caughtIndex);
					m_currFeaturePointsInfo.RemoveAt(caughtIndex);

					// Get the undo data after changes
					GetDefineOutlineProfileJigsUndoData(postUndoData);
					QUndoCommand *defineOutlineProfileJigsCmd = new DefineOutlineProfileJigsCmd( this, prevUndoData, postUndoData);
					undoStack->push( defineOutlineProfileJigsCmd );
				}
			}
		}

	}

	m_leftButtonDown = false;
	m_bMouseMiddleDown = false;
	ReDraw();

	return true;
}

bool CDefineOutlineProfileJigs::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	bool continuousResponse = true;

	if (m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( m_addDeleteMode == 0 && GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			MoveCaughtPoint(cursor);
			continuousResponse = false;
		}
        m_posLast = cursor;
	}
	else
	{
		// call the parent's MouseMove();
		CManager::MouseMove(cursor, keyModifier);
		IwPoint3d caughtPnt;
		int caughtIndex;
		bool bCaught = GetMouseMoveCaughtPoint(caughtPnt, caughtIndex);
		if ( !caughtPnt.IsInitialized() )// The tab end points could be un-initialized.
		{
			CManager::RemoveMouseMoveCaughtPoint();
			bCaught = false;
		}
		// If there is any caught point, we need to check whether it is within viewing normal region.
		if ( bCaught )
		{
			int featPntSize = (int)m_currFeaturePointsXYZ.GetSize();
			IwVector3d viewVec = m_pView->GetViewingVector();
			if ( caughtIndex < featPntSize )// outline profile feature points
			{
				IwPoint3d featUVPnt = m_currFeaturePoints.GetAt(caughtIndex);
				IwPoint3d xyzPnt;
				IwVector3d normal;
				m_pDoc->GetOutlineProfileJigs()->ConvertUVToXYZ(featUVPnt, xyzPnt, normal);
				if ( viewVec.Dot(normal) > -0.93969262) // > 20 degrees
				{
					RemoveMouseMoveCaughtPoint();
				}
			}
			else if ( caughtIndex < featPntSize+2+6 )// dropping centers & tab points
			{
				if ( !viewVec.IsParallelTo(m_sweepAxis.GetZAxis(), 1.0 ) )
				{
					RemoveMouseMoveCaughtPoint();
				}
			}
			else if ( caughtIndex >= (featPntSize+2+6+6) && caughtIndex <= (featPntSize+2+6+6+3) ) // 4 window cut tips
			{
				RemoveMouseMoveCaughtPoint();
			}
			else
			{
				// tab end points
			}
		}
	}

	ReDraw();

	return continuousResponse;
}

bool CDefineOutlineProfileJigs::MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp )
{
	ReDraw();

	return true;
}

////////////////////////////////////////////////////////////////////
// This function will update tab point and its teb end point
bool CDefineOutlineProfileJigs::UpdateTabPointForMouseUpEvent
(
	IwPoint3d caughtPoint, 
	int caughtIndex, 
	IwVector3d viewVec,
	double maxDeviation
)
{
	bool updated = false;
	COutlineProfileJigs* outlineProfileJigs = m_pDoc->GetOutlineProfileJigs(); 
	IwTArray<IwPoint3d> projectedCurvePoints;
	outlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedCurvePoints);
	int featPntSize = (int)m_currFeaturePointsXYZ.GetSize();
	// search for the closest point
	IwPoint3d minPnt;
	int cIndex;
	double dist = DistFromPointToIwTArray(caughtPoint, projectedCurvePoints, &viewVec, minPnt, &cIndex);
	if ( dist < maxDeviation )// Do nothing if mouse release point is too far away outline profile
	{
		int tabIndex = caughtIndex - (featPntSize+2);
		// update tab point
		// minPnt is the closest on projectedCurvePoints (points), rather than the closest on curve.
		// Determine the closest on the curve.
		// project projectedCurvePoints(cIndex), projectedCurvePoints(cIndex+1), and caughtPoint onto the same plane
		IwPoint3d p0 = projectedCurvePoints.GetAt(cIndex);
		IwPoint3d p1 = projectedCurvePoints.GetAt(cIndex+1).ProjectPointToPlane(p0, viewVec);
		IwPoint3d p2 = caughtPoint.ProjectPointToPlane(p0, viewVec);
		IwVector3d vec01 = p1 - p0;
		vec01.Unitize();
		IwVector3d vec02 = p2 - p0;
		minPnt = p0 + vec01.Dot(vec02)*vec01;
		m_currTabPoints.SetAt(tabIndex, minPnt);
		updated = true;
		// Update tab end point.
		// The tab end point should always be 1~3mm pass the split curve as the default result.
		CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
		if (pInnerSurfaceJigs)
		{
			IwBrep* innerSurfaceJigsBrep = pInnerSurfaceJigs->GetIwBrep();
			if (innerSurfaceJigsBrep)
			{
				IwTArray<IwFace*> faces;
				innerSurfaceJigsBrep->GetFaces(faces);
				IwBSplineSurface* innerSurf = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
				if ( innerSurf )
				{
					IwTArray<IwPoint3d> defaultTabEndPoints = CDefineOutlineProfileJigs::DetermineTabEndLocations(m_pDoc, &m_currTabPoints);
					m_currTabEndPoints.SetAt(tabIndex, defaultTabEndPoints.GetAt(tabIndex));
				}
			}
		}
	}

	return updated;
}

//////////////////////////////////////////////////////////////////////
// If users use Ctrl+ to move a tab point, its counter point also needs
// to be moved accordingly.
//////////////////////////////////////////////////////////////////////
bool CDefineOutlineProfileJigs::UpdateTabCounterPointForMouseUpEvent
(
	IwPoint3d counterPoint,		// I: the reference counter tab point. However, its final location will be re-calcuated.
	int counterIndex,			// I: the counter tab index
	IwVector3d viewVec,			// I: view vector
	double maxDeviation,		// I:
	IwPoint3d mainCaughtPoint,	// I: the tab point that users grab 
	double expectedDist			// I: the expected distance between mainCaughtPoint and counterPoint
)
{
	COutlineProfileJigs* outlineProfileJigs = m_pDoc->GetOutlineProfileJigs(); 
	IwTArray<IwPoint3d> projectedCurvePoints;
	outlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedCurvePoints);
	// search for the closest point
	IwPoint3d minPnt;
	int cIndex;
	double dist, length;
	for (int iter=0; iter<5; iter++)
	{
		dist = DistFromPointToIwTArray(counterPoint, projectedCurvePoints, &viewVec, minPnt, &cIndex);
		if ( dist < maxDeviation )// Do nothing if mouse release point is too far away outline profile
		{
			IwVector3d tempVec = minPnt - mainCaughtPoint;
			length = tempVec.Length();
			tempVec.Unitize();
			counterPoint = mainCaughtPoint + expectedDist*tempVec;
if (0)
{
ShowPoint(m_pDoc, counterPoint, magenta);
ShowPoint(m_pDoc, mainCaughtPoint, red);
}
		}
	}

	//
	bool updated = UpdateTabPointForMouseUpEvent(counterPoint, counterIndex, viewVec, maxDeviation);

	return updated;
}

void CDefineOutlineProfileJigs::Display(Viewport* vp)
{		
	glDisable( GL_LIGHTING );

	glDrawBuffer( GL_BACK );

	IwVector3d viewVec = m_pView->GetViewingVector();

	DisplayOutlineFeaturePoints(-39*viewVec);//PSV

	DisplayReferencePoints(-40*viewVec);//PSV

	DisplayTabNCenters(-40*viewVec);//PSV

	DisplayWindowCutTips(-40*viewVec);

	if ( !viewVec.IsParallelTo(m_viewingNormal, 1.0) )
		DetermineViewingNormalRegion();

	DisplayViewingNormalRegion();

	glLineWidth( 1.0 );
	glEnable( GL_LIGHTING );


	if (m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			glDisable( GL_LIGHTING );
			glPointSize(7);
			glColor3ub( 255, 255, 255 );

			DisplayCaughtPoint(-50*viewVec);

			glPointSize(1);
			glEnable( GL_LIGHTING );
		}

	}
	else
	{
		glDisable( GL_LIGHTING );
		glPointSize(6);
		glColor3ub( 255, 255, 0 );

		DisplayCaughtPoint(-50*viewVec);

		IwPoint3d caughtPoint;
		int caughtIndex;
		if ( GetMouseMoveCaughtPoint(caughtPoint, caughtIndex) )
		{
			// Display 3 arc points
			if ( caughtIndex < (int)m_currFeaturePointsInfo.GetSize() )
			{
				unsigned nSize = m_currFeaturePointsInfo.GetSize();
				IwPoint3d sPnt, cPnt, ePnt;
				if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 1 ||
					 m_currFeaturePointsInfo.GetAt(caughtIndex) == 2 ||
					 m_currFeaturePointsInfo.GetAt(caughtIndex) == 11 ||
					 m_currFeaturePointsInfo.GetAt(caughtIndex) == 12 )
				{	// the caught point is arc center, display its start/end points
					sPnt = m_mouseMoveSearchingList.GetAt(caughtIndex-1);
					sPnt = sPnt -50*viewVec; // move closer to the viewer
					cPnt = m_mouseMoveSearchingList.GetAt(caughtIndex);
					cPnt = cPnt -50*viewVec; // move closer to the viewer
					ePnt = m_mouseMoveSearchingList.GetAt(caughtIndex+1);
					ePnt = ePnt -50*viewVec; // move closer to the viewer
					glBegin( GL_POINTS );
						glVertex3d( sPnt.x, sPnt.y, sPnt.z );
						glVertex3d( ePnt.x, ePnt.y, ePnt.z );
					glEnd();
				}
				else if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 3 )
				{	// the caught point is arc start point, display its center/end points
					sPnt = m_mouseMoveSearchingList.GetAt(caughtIndex);
					sPnt = sPnt -50*viewVec; // move closer to the viewer
					cPnt = m_mouseMoveSearchingList.GetAt(caughtIndex+1);
					cPnt = cPnt -50*viewVec; // move closer to the viewer
					ePnt = m_mouseMoveSearchingList.GetAt(caughtIndex+2);
					ePnt = ePnt -50*viewVec; // move closer to the viewer
					glBegin( GL_POINTS );
						glVertex3d( cPnt.x, cPnt.y, cPnt.z );
						glVertex3d( ePnt.x, ePnt.y, ePnt.z );
					glEnd();
				}
				else if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 4 )
				{	// the caught point is arc end point, display its start/center points
					sPnt = m_mouseMoveSearchingList.GetAt(caughtIndex-2);
					sPnt = sPnt -50*viewVec; // move closer to the viewer
					cPnt = m_mouseMoveSearchingList.GetAt(caughtIndex-1);
					cPnt = cPnt -50*viewVec; // move closer to the viewer
					ePnt = m_mouseMoveSearchingList.GetAt(caughtIndex);
					ePnt = ePnt -50*viewVec; // move closer to the viewer
					glBegin( GL_POINTS );
						glVertex3d( sPnt.x, sPnt.y, sPnt.z );
						glVertex3d( cPnt.x, cPnt.y, cPnt.z );
					glEnd();
				}
				// Display auxiliary lines if hover both posterior arcs
				if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 1 ||
					 m_currFeaturePointsInfo.GetAt(caughtIndex) == 3 ||
					 m_currFeaturePointsInfo.GetAt(caughtIndex) == 4 )
				{
					if ( caughtIndex > 5 && caughtIndex < nSize-5 ) // it is a posterior arc control point
					{
						if (m_pDoc->GetAdvancedControlJigs()->GetOutlineSquareOffPosteriorTip())
						{
							IwPoint3d viewingVector = m_pView->GetViewingVector();
							sPnt = sPnt.ProjectPointToPlane(cPnt,viewingVector); 
							ePnt = ePnt.ProjectPointToPlane(cPnt,viewingVector); 
							// Determine the tangent vectors and tangent points
							IwVector3d arcNormal = -viewingVector;
							IwVector3d sTangent = arcNormal*(sPnt-cPnt);
							sTangent.Unitize();
							IwPoint3d sTangentPoint = sPnt + 2.5*sTangent;
							IwVector3d eTangent = -arcNormal*(ePnt-cPnt);
							eTangent.Unitize();
							IwPoint3d eTangentPoint = ePnt + 2.5*eTangent;
							glLineWidth( 2.0 );
							glBegin( GL_LINES );
								glVertex3d( cPnt.x, cPnt.y, cPnt.z );
								glVertex3d( sPnt.x, sPnt.y, sPnt.z );
								glVertex3d( cPnt.x, cPnt.y, cPnt.z );
								glVertex3d( ePnt.x, ePnt.y, ePnt.z );
								glVertex3d( sPnt.x, sPnt.y, sPnt.z );
								glVertex3d( sTangentPoint.x, sTangentPoint.y, sTangentPoint.z );
								glVertex3d( ePnt.x, ePnt.y, ePnt.z );
								glVertex3d( eTangentPoint.x, eTangentPoint.y, eTangentPoint.z );
							glEnd();
						}
					}
				}
					 
			}

			//// display tab widths
			glDisable(GL_DEPTH_TEST);
			glColor3ub( 0, 0, 0 );
			int featPntSize = (int)m_currFeaturePointsXYZ.GetSize();
			if ( caughtIndex > (featPntSize+1) && caughtIndex < (featPntSize+8) )// 2~7 for tab points
			{
				IwPoint3d pnt, vec;
				if ( caughtIndex == (featPntSize+2) || caughtIndex == (featPntSize+4) || caughtIndex == (featPntSize+6) ) // tab points
				{
					pnt = m_currTabPoints.GetAt(caughtIndex-(featPntSize+2));
					vec = m_currTabPoints.GetAt(caughtIndex+1-(featPntSize+2)) - pnt;
				}
				else if ( caughtIndex == (featPntSize+3) || caughtIndex == (featPntSize+5) || caughtIndex == (featPntSize+7) ) // tab points
				{
					pnt = m_currTabPoints.GetAt(caughtIndex-(featPntSize+2));
					vec = m_currTabPoints.GetAt(caughtIndex-1-(featPntSize+2)) - pnt;
				}
				//vec = vec.ProjectToPlane(viewVec);
				double dist = vec.Length();
				dist = floor(100*dist+0.5)/100.0;
				QString str, totalStr;
				str.setNum(dist, 'f', 2);
				totalStr = QString("Width: ") + str;
				QFont font( "Tahoma", 12);
				font.setBold(true);
				pnt -= 20*viewVec;
				GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
			}
			// display tab end depth
			if ( caughtIndex > (featPntSize+7) && caughtIndex < (featPntSize+14) )// 8~13 for tab end points
			{
				IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
				IwPoint3d pnt0, pnt1, vec;
				pnt0 = m_currTabEndPoints.GetAt(caughtIndex-(featPntSize+8));
				pnt1 = m_currTabPoints.GetAt(caughtIndex-(featPntSize+8));
				vec = pnt0 - pnt1;
				double dist = vec.Dot(wslAxes.GetZAxis());
				dist = floor(100*dist+0.5)/100.0;
				QString str, totalStr;
				str.setNum(dist, 'f', 2);
				totalStr = QString("Depth: ") + str;
				QFont font( "Tahoma", 12);
				font.setBold(true);
				pnt0 -= 20*viewVec;
				GetViewWidget()->renderText(pnt0.x, pnt0.y, pnt0.z, totalStr, font);
			}

			glEnable(GL_DEPTH_TEST);

			glFlush();
		}

		glPointSize(1);
		glEnable( GL_LIGHTING );
	}

	return;
}

void CDefineOutlineProfileJigs::DisplayOutlineFeaturePoints(IwVector3d offset)
{
	if ( m_currFeaturePointsXYZ.GetSize() == 0 )
		return;

	IwPoint3d pnt;//m_currFeaturePoints
	//IwPoint3d currFeaturePointUV;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	for (unsigned i=0; i<(m_currFeaturePointsInfo.GetSize()-1); i++)
	{
		pnt = m_currFeaturePointsXYZ.GetAt(i) + offset;
		//currFeaturePointUV = m_currFeaturePoints.GetAt(i);//PSV

		if (i==0 || i== (m_currFeaturePointsInfo.GetSize()-1)) // seam point
		{
			glColor3ub(0, 0, 0);
			glPointSize(8);
		}
		else if (m_currFeaturePointsInfo.GetAt(i) == 0) // interpolation points
		{
			{
				glColor3ub(0, 0, 255);
				glPointSize(6);
			}
		}
		else if (m_currFeaturePointsInfo.GetAt(i) == 1 || // arc center points
				 m_currFeaturePointsInfo.GetAt(i) == 2)
		{
			glColor3ub( 98, 0, 255 );
			glPointSize(6);
		}
		else if (m_currFeaturePointsInfo.GetAt(i) == 3 || // arc start points
			m_currFeaturePointsInfo.GetAt(i) == 4)	    // arc end points
		{
			glColor3ub( 128, 0, 255 );
			glPointSize(6);
		}

		glBegin( GL_POINTS );

			//glColor3ub( 255, 0, 0 );

			glVertex3d( pnt.x, pnt.y, pnt.z );
			//glVertex3d( currFeaturePointUV.x, currFeaturePointUV.y, currFeaturePointUV.z );//PSV

		glEnd();
	}

	//PSV Origin
	//glColor3ub( 255, 0, 0);
	//glVertex3d(0, 0, 0);

	glPointSize(1);

}

void CDefineOutlineProfileJigs::DisplayReferencePoints(IwVector3d offset)
{
	if ( m_referencePoints.GetSize() < 2 )
		return;

	IwVector3d viewVec = m_pView->GetViewingVector();

	IwPoint3d pnt;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	glColor3ub(255, 0, 255);

	glBegin( GL_POINTS );

	// See DetermineOutlineProfileJigs() for the m_referencePoints sequence.
	// if InnerSurfaceJigs exists, do not display reference points 3 & 4 (posterior tip reference points)
	// because posterior profile has been automatically squared-off.
	// However, InnerSurfaceJigs not exists, do not display reference points 8 & 9 (square-off reference points)
	// because posterior profile has not been squared-off yet.
	bool innerSurfaceJigsExist = m_pDoc->GetInnerSurfaceJigs() != NULL; 
	unsigned nSize = m_referencePoints.GetSize();
	for (unsigned i=0; i<nSize; i++)
	{
		if ( innerSurfaceJigsExist )
		{
			if ( i == 3 || i == 4  )
				continue;
		}
		else // not exists
		{
			if ( i == 8 || i == 9 )
				continue;
		}
		pnt = m_referencePoints.GetAt(i) + offset;

		glVertex3d( pnt.x, pnt.y, pnt.z );

	}

	glEnd();

	// display shifted refPoints 0 & 1
	if ( viewVec.IsParallelTo(m_sweepAxis.GetYAxis(), 25.0 ) )
	{
		IwPoint3d pnt0 = m_referencePoints.GetAt(0) + offset;
		IwPoint3d pnt1 = m_referencePoints.GetAt(1) + offset;
		glPointSize(2);
		glBegin( GL_POINTS );
		for (int i=1; i<5; i++)// i=1~4
		{
			pnt = pnt0 + 1.5*i*m_sweepAxis.GetXAxis();
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = pnt0 - 1.5*i*m_sweepAxis.GetXAxis();
			glVertex3d( pnt.x, pnt.y, pnt.z );
			//
			pnt = pnt1 + 1.5*i*m_sweepAxis.GetXAxis();
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = pnt1 - 1.5*i*m_sweepAxis.GetXAxis();
			glVertex3d( pnt.x, pnt.y, pnt.z );

		}
		glEnd();
	}

	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();

	IwPoint3d posRefLinePnt1, posRefLinePnt2, negRefLinePnt1, negRefLinePnt2;
	pOutlineProfileJigs->GetRefLinePoints(posRefLinePnt1, posRefLinePnt2, negRefLinePnt1, negRefLinePnt2);

	posRefLinePnt1 += offset;
	posRefLinePnt2 += offset;
	negRefLinePnt1 += offset;
	negRefLinePnt2 += offset;

	glLineWidth( 4.0 );
	glColor3ub( 255, 255, 0);
	glBegin( GL_LINES );
	   glVertex3d( posRefLinePnt1.x, posRefLinePnt1.y, posRefLinePnt1.z );
	   glVertex3d( posRefLinePnt2.x, posRefLinePnt2.y, posRefLinePnt2.z );
	glEnd();

	glBegin( GL_LINES );
	   glVertex3d( negRefLinePnt1.x, negRefLinePnt1.y, negRefLinePnt1.z );
	   glVertex3d( negRefLinePnt2.x, negRefLinePnt2.y, negRefLinePnt2.z );
	glEnd();

	glPointSize(1);

}

void CDefineOutlineProfileJigs::DisplayTabNCenters(IwVector3d offset)
{
	if ( m_currTabPoints.GetSize() == 0 || !m_currPosDroppingCenter.IsInitialized() )
		return;

	IwVector3d viewVec = m_pView->GetViewingVector();

	IwPoint3d medialDroppingCenter, lateralDroppingCenter;

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		medialDroppingCenter = m_currNegDroppingCenter;
		lateralDroppingCenter = m_currPosDroppingCenter;
	}
	else
	{
		positiveSideIsLateral = false;
		medialDroppingCenter = m_currPosDroppingCenter;
		lateralDroppingCenter = m_currNegDroppingCenter;
	}

	IwPoint3d pnt;
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glLineWidth( 6.0 );
	glPointSize(6);

	IwVector3d tempVec;
	IwPoint3d outerPnt;

	glColor3ub(0, 64, 0);

	glBegin( GL_POINTS );
	for (unsigned i=0; i<m_currTabEndPoints.GetSize(); i++)
	{
		pnt = m_currTabEndPoints.GetAt(i);
		glVertex3d( pnt.x, pnt.y, pnt.z );
	}
	glEnd();


	if ( !viewVec.IsParallelTo(m_sweepAxis.GetZAxis(), 1.0 ) )
	{
		glLineWidth( 1.0 );
		glPointSize(1);
		return;
	}

	for (unsigned i=0; i<m_currTabPoints.GetSize(); i++)
	{
		double maxTabWidth = m_pDoc->GetVarTableValue("JIGS TAB MAX WIDTH");
		double minTabWidth = m_pDoc->GetVarTableValue("JIGS TAB MIN WIDTH");
		glColor3ub(0, 64, 0);
		if ( i == 2 || i == 3 ) // mid tab in lateral side
		{
			double length = m_currTabPoints.GetAt(2).DistanceBetween(m_currTabPoints.GetAt(3));
			length = floor(100*length+0.5)/100.0;
			if (length < minTabWidth || length > maxTabWidth)
				glColor3ub(255, 0, 0);
			pnt = m_currTabPoints.GetAt(i);
			tempVec = pnt - lateralDroppingCenter;
			tempVec.Unitize();
			pnt = pnt + offset;
			outerPnt = pnt + 2.0*tempVec;
		}
		else if ( i == 0 || i == 1 ) // ant tab  in medial side
		{
			double length = m_currTabPoints.GetAt(0).DistanceBetween(m_currTabPoints.GetAt(1));
			length = floor(100*length+0.5)/100.0;
			if (length < minTabWidth || length > maxTabWidth)
				glColor3ub(255, 0, 0);
			pnt = m_currTabPoints.GetAt(i);
			tempVec = pnt - medialDroppingCenter;
			tempVec.Unitize();
			pnt = pnt + offset;
			outerPnt = pnt + 2.0*tempVec;
		}		
		else if ( i == 4 || i == 5 )// peg tab in medial side
		{
			double length = m_currTabPoints.GetAt(4).DistanceBetween(m_currTabPoints.GetAt(5));
			length = floor(100*length+0.5)/100.0;
			if (length < minTabWidth || length > maxTabWidth)
				glColor3ub(255, 0, 0);
			pnt = m_currTabPoints.GetAt(i);
			tempVec = pnt - medialDroppingCenter;
			tempVec.Unitize();
			pnt = pnt + offset;
			outerPnt = pnt + 2.0*tempVec;
		}
		glBegin( GL_LINE_STRIP );
			glVertex3d( pnt.x, pnt.y, pnt.z );
			glVertex3d( outerPnt.x, outerPnt.y, outerPnt.z );
		glEnd();

	}

	glPointSize( 8.0 );
	glColor3ub(152, 0, 0);
	glBegin( GL_POINTS );
		pnt = medialDroppingCenter + offset;
		glVertex3d( pnt.x, pnt.y, pnt.z );
	glEnd();

	glColor3ub(0, 0, 152);
	glBegin( GL_POINTS );
		pnt = lateralDroppingCenter + offset;
		glVertex3d( pnt.x, pnt.y, pnt.z );
	glEnd();

	glLineWidth( 1.0 );
	glPointSize(1);

}

void CDefineOutlineProfileJigs::DisplayWindowCutTips(IwVector3d offset)
{
	if ( m_currWindowCutTips.GetSize() != 4 )
		return;

	IwVector3d viewVec = m_pView->GetViewingVector();

	if ( !viewVec.IsParallelTo(m_sweepAxis.GetZAxis(), 1.0 ) )
	{
		glLineWidth( 1.0 );
		glPointSize(1);
		return;
	}

	IwPoint3d pnt;
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	IwVector3d tempVec;
	IwPoint3d outerPnt;

	double windowCutMinDistance = m_pDoc->GetVarTableValue("JIGS WINDOW CUT MIN DISTANCE TO PROFILE");
	glBegin( GL_POINTS );
		for ( unsigned i=0; i<m_currWindowCutTips.GetSize(); i++)
		{
			glColor3ub(0, 127, 0);
			if ( m_windowCutDistancesToProfile.GetAt(i) > 0.0 && 
				 m_windowCutDistancesToProfile.GetAt(i) < windowCutMinDistance ) // "-1" has a special meaning.
				glColor3ub(255, 0, 0);
			pnt = m_currWindowCutTips.GetAt(i) + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}
	glEnd();

	glPointSize(1);

	// Display the minimum distance from window cut to outline profile jig
	if ( !m_leftButtonDown )
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			int featPntSize = (int)m_currFeaturePointsXYZ.GetSize();
			if ( caughtIndex >= (featPntSize+2+6+6) && caughtIndex <= (featPntSize+2+6+6+3) ) // 4 window cut tips
			{
				int distIndex = caughtIndex - (featPntSize+2+6+6);
				double dist = m_windowCutDistancesToProfile.GetAt(distIndex);
				dist = floor(100*dist+0.5)/100.0;
				if ( dist > 0 && windowCutMinDistance) // "-1" has a special meaning, only display if windowCutMinDistance > 0
				{
					QString str;
					str.setNum(dist, 'f', 2);
					QString totalStr = QString("Min dist: ") + str;
					pnt = m_currWindowCutTips.GetAt(distIndex) + offset;
					QFont font( "Tahoma", 12);
					font.setBold(true);
					glColor3ub(0, 0, 0);
					GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
				}
			}
		}
	}

}

void CDefineOutlineProfileJigs::DisplayViewingNormalRegion()
{
	// Display viewing normal region
	glLineWidth( 2.0 );
	glColor3d( 1.0, 1.0, 0.0 );
	if (m_viewingNormalRegion.GetSize() > 0)
	{
		glBegin( GL_LINE_LOOP );
			glVertex3d(m_viewingNormalRegion.GetAt(0).x, m_viewingNormalRegion.GetAt(0).y, m_viewingNormalRegion.GetAt(0).z);
			glVertex3d(m_viewingNormalRegion.GetAt(1).x, m_viewingNormalRegion.GetAt(1).y, m_viewingNormalRegion.GetAt(1).z);
			glVertex3d(m_viewingNormalRegion.GetAt(2).x, m_viewingNormalRegion.GetAt(2).y, m_viewingNormalRegion.GetAt(2).z);
			glVertex3d(m_viewingNormalRegion.GetAt(3).x, m_viewingNormalRegion.GetAt(3).y, m_viewingNormalRegion.GetAt(3).z);
		glEnd();
	}
	// Display viewing normal region
	glLineWidth( 1.0 );
	glColor3d( 1.0, 0.75, 0.0 );
	if (m_viewingNormalRegion.GetSize() > 0)
	{
		glBegin( GL_LINE_LOOP );
			glVertex3d(m_manipulatingNormalRegion.GetAt(0).x, m_manipulatingNormalRegion.GetAt(0).y, m_manipulatingNormalRegion.GetAt(0).z);
			glVertex3d(m_manipulatingNormalRegion.GetAt(1).x, m_manipulatingNormalRegion.GetAt(1).y, m_manipulatingNormalRegion.GetAt(1).z);
			glVertex3d(m_manipulatingNormalRegion.GetAt(2).x, m_manipulatingNormalRegion.GetAt(2).y, m_manipulatingNormalRegion.GetAt(2).z);
			glVertex3d(m_manipulatingNormalRegion.GetAt(3).x, m_manipulatingNormalRegion.GetAt(3).y, m_manipulatingNormalRegion.GetAt(3).z);
		glEnd();
	}
	glFlush();

}

void CDefineOutlineProfileJigs::CreateOutlineProfileJigsObject(CTotalDoc* pDoc)
{

	// Create OutlineProfile object
	if ( pDoc->GetOutlineProfileJigs() == NULL )
	{
		COutlineProfileJigs* pOutlineProfileJigs = new COutlineProfileJigs(pDoc);
		pDoc->AddEntity(pOutlineProfileJigs, true);
	}

	return;
}

////////////////////////////////////////////////////////////////////
// This function is to reset the outline profile.
////////////////////////////////////////////////////////////////////
void CDefineOutlineProfileJigs::OnReset()
{
	int button = QMessageBox::warning( NULL, 
									QString("Outline Profile Reset!"), 
									QString("Outline Profile will be reset."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return;

	Reset(true);
}

//#include "../KUtility/SMLibHelpers.h"
void CDefineOutlineProfileJigs::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::Reset()") );

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	QApplication::setOverrideCursor( Qt::WaitCursor );

	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();

	// Create its own sketch surface
	double sizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSizeRatio();
	IwAxis2Placement sweepAxis = IFemurImplant::FemoralAxes_GetSweepAxes();
	bool bCopy = false;
	IwBSplineSurface* sketchSurf = IFemurImplant::OutlineProfile_GetSketchSurface(bCopy);

	if(0)
	{
		ShowSurface(m_pDoc, sketchSurf, "sketchSurf_1", yellow);//PSV
	}

	// Extend anteriorly 
	double extensionDist = 1.0;
	CFemoralPart* osteoSurface = m_pDoc->GetOsteophyteSurface();
	if ( osteoSurface != NULL )
	{
		IwBSplineSurface* osteoSurf = osteoSurface->GetSinglePatchSurface();
		//ShowSurface(m_pDoc, osteoSurf, "osteoSurf_1", cyan);//PSV
		IwPoint3d farAntPoint = sweepAxis.GetOrigin() + 1000*sweepAxis.GetZAxis();
		IwPoint3d osteoEdgePoint, sketchSurfEdgePoint;
		IwPoint2d param2d;
		DistFromPointToSurface(farAntPoint, osteoSurf, osteoEdgePoint, param2d);
		DistFromPointToSurface(farAntPoint, sketchSurf, sketchSurfEdgePoint, param2d);
		IwVector3d tempVec = osteoEdgePoint - sketchSurfEdgePoint;
		double dotValue = tempVec.Dot(sweepAxis.GetZAxis());
		if ( dotValue > 1.0 )
			extensionDist = 1.1*dotValue;// make it a bit longer
	}

	IwBSplineSurface* sketchSurfExtended;
	sketchSurf->CreateExtendedSurface(m_pDoc->GetIwContext(), IW_SP_VMIN, extensionDist,  IW_CT_G1, sketchSurfExtended);
	double offsetDist = 2.5*sizeRatio;
	IwTArray<IwSurface*> offSurfs;
	sketchSurfExtended->CreateOffsetSurface(m_pDoc->GetIwContext(), offsetDist, 0.01, offSurfs);// The parameters of both surfaces will be synchronized.
	IwBSplineSurface* sketchSurfJigs = (IwBSplineSurface*)offSurfs.GetAt(0);
	pOutlineProfileJigs->SetSketchSurface(sketchSurfJigs);
	//ShowSurface(m_pDoc, sketchSurfJigs, "sketchSurfJigs_1", cyan);//PSV

	//IwBrep* skchBrep = new (m_pDoc->GetIwContext()) IwBrep();
	//IwFace* pFace;
	//skchBrep->CreateFaceFromSurface(sketchSurfJigs, sketchSurfJigs->GetNaturalUVDomain(), pFace);
	//WriteIges("C:\\PS\\Ops\\m_sketchSurface.iges", skchBrep);

	IwTArray<IwSurface*> arrOfSketchSurfs;
	arrOfSketchSurfs.Add(sketchSurfJigs);
	//WriteSurfaces(arrOfSketchSurfs, "C:\\PS\\Ops\\m_sketchSurface.surf");

	// Create default outline profile Jigs
	DetermineOutlineProfileJigs();
	pOutlineProfileJigs->SetOutlineProfileReferencePoints(m_referencePoints);
	pOutlineProfileJigs->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo);
	pOutlineProfileJigs->GetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo);// To ensure the arc ending points are consistent in m_currFeaturePoints
	IwBSplineCurve * pOutlineProfileBSC = NULL;
	pOutlineProfileBSC = pOutlineProfileJigs->GetUVCurveOnSketchSurface();

	// Determine the dropping centers. Dropping centers need to be determined only when reset.
	DetermineDroppingCenters();
	pOutlineProfileJigs->SetDroppingCenters(m_currPosDroppingCenter, m_currNegDroppingCenter);
	// Determine default tab locations
	DetermineTabLocations();// only m_currTabPoints are determined.
	pOutlineProfileJigs->SetOutlineProfileTabPoints(m_currTabPoints);
	m_currTabEndPoints = CDefineOutlineProfileJigs::DetermineTabEndLocations(m_pDoc);// only m_currTabEndPoints are determined.
	pOutlineProfileJigs->SetOutlineProfileTabEndPoints(m_currTabEndPoints);

	// Reset window cut angle
	m_currWindowCutAngles.RemoveAll();
	for (int i=0; i<4; i++)
		m_currWindowCutAngles.Add(10.0);// default angles
	RefineWindowCutAngles();
	pOutlineProfileJigs->SetWindowCutAngles(m_currWindowCutAngles);

	// update search list
	// for feature points
	IwTArray<IwPoint3d>  totalSearchList;
	pOutlineProfileJigs->ConvertUVToXYZ(m_currFeaturePoints, m_currFeaturePointsXYZ);

	IwBSplineSurface * pSurf1 = NULL;
	pOutlineProfileJigs->GetSketchSurface(pSurf1);
	//ShowSurface(m_pDoc, pSurf1, "pSurf1_GetSketchSurface", green);//PSV

	m_currFeaturePointsXYZ.RemoveLast();// Note, the last one is never selectable.

	totalSearchList.Append(m_currFeaturePointsXYZ);
	// for dropping centers
	totalSearchList.Add(m_currPosDroppingCenter);
	totalSearchList.Add(m_currNegDroppingCenter);
	// for tab points
	totalSearchList.Append(m_currTabPoints);
	totalSearchList.Append(m_currTabEndPoints);
	// for window tips
	pOutlineProfileJigs->GetWindowCutAngles(m_currWindowCutAngles, &m_currWindowCutTips, NULL, NULL);
	DetermineWindowCutDistancesToProfile(m_pDoc, m_windowCutDistancesToProfile);
	totalSearchList.Append(m_currWindowCutTips);

	SetMouseMoveSearchingList(totalSearchList);

	QApplication::restoreOverrideCursor();

	// Change the status sign of itself
	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE_JIGS, m_validateStatus, m_validateMessage);
	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTLINE_PROFILE_JIGS, false, false); // therefore, outline profile Jigs become out of date

	m_pView->Redraw();
}

void CDefineOutlineProfileJigs::OnAdd()
{
	m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::OnAdd()") );

	QApplication::setOverrideCursor( Qt::PointingHandCursor );
	m_addDeleteMode = 1; // 0: none, 1:add, 2:delete
	EnableAction( m_actAdd, false );

	m_pView->Redraw();

}

void CDefineOutlineProfileJigs::OnDel()
{
	m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::OnDel()") );

	QApplication::setOverrideCursor( Qt::PointingHandCursor );
	m_addDeleteMode = 2; // 0: none, 1:add, 2:delete
	EnableAction( m_actDel, false );

	m_pView->Redraw();

}

void CDefineOutlineProfileJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::OnAccept()") );

	COutlineProfileJigs* outlineProfileJigs = m_pDoc->GetOutlineProfileJigs();

	// Set reference points to outlineProfileJigs
	outlineProfileJigs->SetOutlineProfileReferencePoints(m_referencePoints);

	// Outline profile jigs is up-to-date or not
	// Need to check IsMemberDataChanged()
	if ( outlineProfileJigs )
	{
		if ( !outlineProfileJigs->GetUpToDateStatus() || 
			  outlineProfileJigs->IsMemberDataChanged(m_oldFeaturePoints, m_oldFeaturePointsInfo, m_oldTabPoints, m_oldTabEndPoints, m_oldPosDroppingCenter, m_oldNegDroppingCenter, m_oldWindowCutAngles, false) )
			m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTLINE_PROFILE_JIGS, true, true);
	}

	// Hide sketch surface
	outlineProfileJigs->SetDisplaySketchSurface(false);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	if ( outlineProfileJigs )
	{
		if ( m_validateStatus == VALIDATE_STATUS_NOT_VALIDATE ) // use runtime m_validateStatus, rather than outlineProfileJigs->GetValidateStatus()
		{
			OnValidate();
		}
	}

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CDefineOutlineProfileJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CDefineOutlineProfileJigs::OnCancel()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("DefineOutlineProfileJigs");
	
	COutlineProfileJigs* outlineProfileJigs = m_pDoc->GetOutlineProfileJigs();

	if (outlineProfileJigs)
	{
		if ( m_oldFeaturePoints.GetSize() > 0 )// set the old data bact to outlineProfileJigs.
		{
			outlineProfileJigs->SetOutlineProfileFeaturePoints(m_oldFeaturePoints, m_oldFeaturePointsInfo);
			outlineProfileJigs->SetDroppingCenters(m_oldPosDroppingCenter, m_oldNegDroppingCenter);
			outlineProfileJigs->SetOutlineProfileTabPoints(m_oldTabPoints);
			outlineProfileJigs->SetOutlineProfileTabEndPoints(m_oldTabEndPoints);
			outlineProfileJigs->SetWindowCutAngles(m_oldWindowCutAngles);
		}
	}

	// Hide sketch surface
	outlineProfileJigs->SetDisplaySketchSurface(false);

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	ReDraw();

}

void CDefineOutlineProfileJigs::DetermineViewingNormalRegion()
{
	m_viewingNormalRegion.RemoveAll();
	m_manipulatingNormalRegion.RemoveAll();

	IwVector3d towardEyesVector = -m_pView->GetViewingVector();
	towardEyesVector.Unitize();
	m_viewingNormal = towardEyesVector;

	double dot;
	int firstPos = -1;
	int lastPos = -1;
	for (unsigned i=0; i<m_outlineSurfaceNormal.GetSize(); i++)
	{
		dot = towardEyesVector.Dot(m_outlineSurfaceNormal.GetAt(i));
		if (dot > 0.994522) //6 degrees
		{
			if (firstPos == -1)
				firstPos = i;
			lastPos = i;
		}
	}

	if (firstPos < lastPos)
	{
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(firstPos));
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(firstPos));
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(lastPos));// "Max" follows the direction
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(lastPos));// "Min"
	}

	firstPos = -1;
	lastPos = -1;
	for (unsigned i=0; i<m_outlineSurfaceNormal.GetSize(); i++)
	{
		dot = towardEyesVector.Dot(m_outlineSurfaceNormal.GetAt(i));
		if (dot > 0.93969262) //20 degrees
		{
			if (firstPos == -1)
				firstPos = i;
			lastPos = i;
		}
	}

	if (firstPos < lastPos)
	{
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(firstPos));
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(firstPos));
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(lastPos));// "Max" follows the direction
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(lastPos));// "Min"
	}
}

void CDefineOutlineProfileJigs::GetDefineOutlineProfileJigsUndoData(CDefineOutlineProfileJigsUndoData& undoData)
{
	undoData.featurePoints.RemoveAll();
	undoData.featurePointsInfo.RemoveAll();
	undoData.tabPoints.RemoveAll();
	undoData.tabEndPoints.RemoveAll();
	undoData.windowCutAngles.RemoveAll();

	undoData.featurePoints.Append(m_currFeaturePoints);
	undoData.featurePointsInfo.Append(m_currFeaturePointsInfo);
	undoData.posDroppingCenter = m_currPosDroppingCenter;
	undoData.negDroppingCenter = m_currNegDroppingCenter;
	undoData.tabPoints.Append(m_currTabPoints);
	undoData.tabEndPoints.Append(m_currTabEndPoints);
	undoData.windowCutAngles.Append(m_currWindowCutAngles);
}

void CDefineOutlineProfileJigs::SetDefineOutlineProfileJigsUndoData(CDefineOutlineProfileJigsUndoData& undoData)
{
	m_currFeaturePoints.RemoveAll();
	m_currFeaturePointsInfo.RemoveAll();
	m_currTabPoints.RemoveAll();
	m_currTabEndPoints.RemoveAll();
	m_currWindowCutAngles.RemoveAll();

	m_currFeaturePoints.Append(undoData.featurePoints);
	m_currFeaturePointsInfo.Append(undoData.featurePointsInfo);
	m_currPosDroppingCenter = undoData.posDroppingCenter;
	m_currNegDroppingCenter = undoData.negDroppingCenter;
	m_currTabPoints.Append(undoData.tabPoints);
	m_currTabEndPoints.Append(undoData.tabEndPoints);
	m_currWindowCutAngles.Append(undoData.windowCutAngles);

	// Also set the m_currFeaturePoints to the SetMouseMoveSearchingList() for mouse searching;
	// convert feature points from UV domain to xyz space
	COutlineProfileJigs*		outlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	IwBSplineSurface* surf = NULL;
	if (outlineProfileJigs)
	{
		// We need to call SetOutlineProfileFeaturePoints() and GetOutlineProfileFeaturePoints()
		// such that all the m_currFeaturePoints are in the correct positions, especially for
		// the arc end points.
		IwTArray<IwPoint3d> totalSearchList;
		outlineProfileJigs->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo);
		outlineProfileJigs->GetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo);// To ensure the arc ending points are consistent in m_currFeaturePoints
		outlineProfileJigs->ConvertUVToXYZ(m_currFeaturePoints, m_currFeaturePointsXYZ);
		m_currFeaturePointsXYZ.RemoveLast();// Note, the last one is never selectable.
		totalSearchList.Append(m_currFeaturePointsXYZ);

		outlineProfileJigs->SetDroppingCenters(m_currPosDroppingCenter, m_currNegDroppingCenter);
		totalSearchList.Add(m_currPosDroppingCenter);
		totalSearchList.Add(m_currNegDroppingCenter);

		// Need to update m_currTabPoints
		IwTArray<IwPoint3d> projectedCurvePoints;
		outlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedCurvePoints);
		IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
		IwVector3d zVec = wslAxes.GetZAxis();
		IwPoint3d minPnt;
		int cIndex;
		for (unsigned i=0; i<m_currTabPoints.GetSize(); i++)
		{
			DistFromPointToIwTArray(m_currTabPoints.GetAt(i), projectedCurvePoints, &zVec, minPnt, &cIndex);
			// project projectedCurvePoints(cIndex), projectedCurvePoints(cIndex+1), and m_currTabPoints.GetAt(i) onto the same plane
			IwPoint3d p0 = projectedCurvePoints.GetAt(cIndex);
			IwPoint3d p1 = projectedCurvePoints.GetAt(cIndex+1).ProjectPointToPlane(p0, zVec);
			IwPoint3d p2 = m_currTabPoints.GetAt(i).ProjectPointToPlane(p0, zVec);
			IwVector3d vec01 = p1 - p0;
			vec01.Unitize();
			IwVector3d vec02 = p2 - p0;
			minPnt = p0 + vec01.Dot(vec02)*vec01;
			m_currTabPoints.SetAt(i, minPnt);
		}
		outlineProfileJigs->SetOutlineProfileTabPoints(m_currTabPoints);
		outlineProfileJigs->SetOutlineProfileTabEndPoints(m_currTabEndPoints);
		totalSearchList.Append(m_currTabPoints);
		totalSearchList.Append(m_currTabEndPoints);

		// window cut
		outlineProfileJigs->SetWindowCutAngles(m_currWindowCutAngles);
		outlineProfileJigs->GetWindowCutAngles(m_currWindowCutAngles, &m_currWindowCutTips, NULL, NULL);// to update tips
		DetermineWindowCutDistancesToProfile(m_pDoc, m_windowCutDistancesToProfile);
		totalSearchList.Append(m_currWindowCutTips);

		SetMouseMoveSearchingList(totalSearchList);

	}

	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE_JIGS, m_validateStatus, m_validateMessage);

	m_pView->Redraw();
}

bool CDefineOutlineProfileJigs::ConvertToArc
(
	int index,									// I:
	IwTArray<IwPoint3d>& featurePoints,			// I/O: 
	IwTArray<int>& featurePointsInfo,			// I/O:
	double minRadius							// I:
)
{
	// Not implement yet.
	return true;
}

bool CDefineOutlineProfileJigs::ConvertToPoint
(
	int index,							// I:
	IwTArray<IwPoint3d>& featurePoints,	// I/O: 
	IwTArray<int>& featurePointsInfo	// I/O:
)
{
	// Not implement yet.
	return true;
}

void CDefineOutlineProfileJigs::CalculateSketchSurfaceNormalInfo()
{
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;

	IwBSplineSurface* skchSurface=NULL;
	pOutlineProfileJigs->GetSketchSurface(skchSurface);
	if (skchSurface == NULL) return;
	// the sketch surface normal and points are repeatedly referred
	// when determine the viewing normal region. 
	// Here we need to pre-determine the surface normal and points
	// to save CPU time.
	m_outlineSurfaceNormal.RemoveAll();
	m_outlineSurfacePointsMinU.RemoveAll();
	m_outlineSurfacePointsMaxU.RemoveAll();
	IwExtent2d surfDomain = skchSurface->GetNaturalUVDomain();
	int nNum = 400, nNumPlusOne = nNum+1;
	double minV = surfDomain.GetMin().y;
	double maxV = surfDomain.GetMax().y;
	double deltaT = (maxV - minV)/nNum;
	double tV = minV;
	double tMidU = 0.5*(surfDomain.GetMin().x+surfDomain.GetMax().x);
	double tMinU = surfDomain.GetMin().x;
	double tMaxU = surfDomain.GetMax().x;
	IwPoint3d pointMinU, pointMaxU;
	IwVector3d normal;
	for (int i=0; i<nNumPlusOne; i++)
	{
		skchSurface->EvaluateNormal(IwVector2d(tMidU, tV), FALSE, FALSE, normal);
		normal.Unitize();
		m_outlineSurfaceNormal.Add(normal);
		skchSurface->EvaluatePoint(IwVector2d(tMinU, tV), pointMinU);
		m_outlineSurfacePointsMinU.Add(pointMinU);
		skchSurface->EvaluatePoint(IwVector2d(tMaxU, tV), pointMaxU);
		m_outlineSurfacePointsMaxU.Add(pointMaxU);
		tV += deltaT;
	}
}

////////////////////////////////////////////////////////////////////
void CDefineOutlineProfileJigs::DetermineOutlineProfileJigs()
{
	m_currFeaturePoints.RemoveAll();
	m_currFeaturePointsInfo.RemoveAll();
	m_currFeaturePointsXYZ.RemoveAll();
	m_referencePoints.RemoveAll();

	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;
	IwBSplineSurface* sketchSurfaceJigs;
	pOutlineProfileJigs->GetSketchSurface(sketchSurfaceJigs);
	if ( sketchSurfaceJigs == NULL )
		return;

	// Get cartilage surface
	CCartilageSurface *pCartilageSurface = m_pDoc->GetCartilageSurface();
	if ( pCartilageSurface == NULL )
		return;
	IwBSplineSurface* cartiMainSurface = pCartilageSurface->GetSinglePatchSurface();
	if (cartiMainSurface==NULL)
		return;
	//
	double distanceToCutEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");

	// Get cuts info
	IwTArray<IwPoint3d> cutPoints, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);
	IwTArray<IwPoint3d> cutFeaturePoints, antOuterPoints;
	IFemurImplant::FemoralCuts_GetCutsFeaturePoints(cutFeaturePoints, antOuterPoints);

	// Get J curves
	bool bCopy = true;
	//IwTArray<IwCurve*> Jcurves;
	//IFemurImplant::JCurves_GetJCurves(bCopy, Jcurves);

	// Get femoral axes
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSizeRatio();

	// Get outline profile uv curve
	bCopy = true;
	IwBSplineCurve*sketch3dCurve=NULL, *sketchUVCurve=NULL;
	sketch3dCurve = IFemurImplant::OutlineProfile_GetSketchCurve(bCopy, &sketchUVCurve);

	//ShowCurve(m_pDoc, sketchUVCurve, lightYellow);

	// Fillet implant edge as reference curve
	IwBSplineCurve* filletEdgeCurve = NULL;//GetOutlineProfileReferenceCurve();

	if(0)
	{
		IwTArray<IwCurve*> arrOfCurves;
		arrOfCurves.Add(filletEdgeCurve);
		//ReadCurves("C:\\PS\\Ops\\filletEdgeCurve.crv", arrOfCurves);
		filletEdgeCurve = (IwBSplineCurve*)arrOfCurves[0];
	}
	
	if(1)
	{
		COutlineProfile * pOutlineProfile = m_pDoc->GetOutlineProfile();
		filletEdgeCurve = pOutlineProfile->GetOutlineProfileXYZCurve();
	}

	//ShowCurve(m_pDoc, filletEdgeCurve, cyan);

	// Get condylar cut widths
	double posCondylarCutWidth, negCondylarCutWidth;
	IFemurImplant::FemoralCuts_GetCondylarCutWidths(posCondylarCutWidth, negCondylarCutWidth);
	posCondylarCutWidth += 2.0*distanceToCutEdge;// compensate by 2*distanceToCutEdge
	negCondylarCutWidth += 2.0*distanceToCutEdge;// compensate by 2*distanceToCutEdge

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	//////////////////////////////////////////////////////////////////////////
	// Determine the 2 reference points to indicate anterior profile width
	double antProfileWidth = cutFeaturePoints.GetAt(18).DistanceBetween(cutFeaturePoints.GetAt(19)) + 2.0*distanceToCutEdge;// 
	IwPoint3d posAntProfileRefPoint, negAntProfileRefPoint;
	double posPercent, negPercent;
	if ( positiveSideIsLateral )
	{
		posPercent = 0.20;
		negPercent = 0.22;
	}
	else
	{
		posPercent = 0.22;
		negPercent = 0.20;
	}
	posAntProfileRefPoint = cutFeaturePoints.GetAt(18) + wslAxes.GetXAxis() - posPercent*antProfileWidth*wslAxes.GetXAxis();// cutFeaturePoints.GetAt(18) + wslAxes.GetXAxis() is the vertex point
	negAntProfileRefPoint = cutFeaturePoints.GetAt(19) - wslAxes.GetXAxis() + negPercent*antProfileWidth*wslAxes.GetXAxis();// cutFeaturePoints.GetAt(19) - wslAxes.GetXAxis() is the vertex point
	double antReferenceWidth = posAntProfileRefPoint.DistanceBetween(negAntProfileRefPoint);

	////////////////////////////////////////////////////////////////////////////
	// Determine the anterior tip/seam point, which is in the middle plane of 
	// both posAntProfileRefPoint & negAntProfileRefPoint 
	// and away from distal lateral cut with "F3APThickness" distance
	IwPoint3d antTipPoint;
	double F3APThickness = m_pDoc->GetVarTableValue("JIGS F3 SKETCH AP WIDTH") + 2.0; // 2.0 as allowance for projection later
	IwPoint3d lateralDistalCutPnt;
	double lateralNotchShift;
	if ( positiveSideIsLateral )
	{
		lateralDistalCutPnt = cutPoints.GetAt(6);
		lateralNotchShift = 1.0;
	}
	else
	{
		lateralDistalCutPnt = cutPoints.GetAt(7);
		lateralNotchShift = -1.0;
	}
	IwPoint3d midRefPoint = 0.5*(posAntProfileRefPoint+negAntProfileRefPoint);
	IwVector3d tempVec = midRefPoint - lateralDistalCutPnt;
	double distToRefPntDist = tempVec.Dot(wslAxes.GetZAxis());// The distance between the middle point to lateral distal cut
	antTipPoint = midRefPoint + (F3APThickness-distToRefPntDist)*wslAxes.GetZAxis();
	// project to sketch surface
	IwPoint3d antSeamRefPoint;
	IwPoint2d uvParam;
	DistFromPointToSurface(antTipPoint, sketchSurfaceJigs, antSeamRefPoint, uvParam);
	IwPoint3d antTipPointUV = IwPoint3d(uvParam.x, uvParam.y, 0.0);
if (0)
ShowPoint(m_pDoc, antSeamRefPoint, red);

	// Determine the positive side anterior arc
	// We are working on UV domain, rather XYZ space.
	// The sketch surface is parameterized with arc length. 
	// The distance in UV space is quite close to the actual 3D distance.
	double rad20deg = 20.0/180.0*IW_PI;
	double antArcRadius = 5.0;
	double distToShift = 0.2*((F3APThickness-distToRefPntDist) - antArcRadius); // this distance will make the profile pass the pos/negAntProfileRefPoint
	if ( distToShift < 0 )
		distToShift = 0;
	IwPoint3d posAntArcUVStart, posAntArcUVCenter, posAntArcUVEnd;
	posAntArcUVStart = antTipPointUV + (0.5*antReferenceWidth-distToShift-antArcRadius)*IwVector3d(1,0,0);
	posAntArcUVCenter = antTipPointUV + (0.5*antReferenceWidth-distToShift-antArcRadius)*IwVector3d(1,0,0) + antArcRadius*IwVector3d(0,1,0);
	posAntArcUVEnd = posAntArcUVCenter + antArcRadius*cos(rad20deg)*IwVector3d(1,0,0) - antArcRadius*sin(rad20deg)*IwVector3d(0,1,0);

	// Determine the negative side anterior arc
	// We are working on UV domain.
	IwPoint3d negAntArcUVStart, negAntArcUVCenter, negAntArcUVEnd;
	negAntArcUVEnd = antTipPointUV - (0.5*antReferenceWidth-distToShift-antArcRadius)*IwVector3d(1,0,0);
	negAntArcUVCenter = antTipPointUV - (0.5*antReferenceWidth-distToShift-antArcRadius)*IwVector3d(1,0,0) + antArcRadius*IwVector3d(0,1,0);
	negAntArcUVStart = negAntArcUVCenter - antArcRadius*cos(rad20deg)*IwVector3d(1,0,0) - antArcRadius*sin(rad20deg)*IwVector3d(0,1,0);

	// Determine positive side posterior tip reference point
	// Intersect posterior cut with cartilage 
	IwPoint3d posPostCutPnt = cutPoints.GetAt(0);


	// Project onto condylar center plane
	posPostCutPnt = posPostCutPnt.ProjectPointToPlane(0.5*(cutFeaturePoints.GetAt(6)+cutFeaturePoints.GetAt(7)), wslAxes.GetXAxis());

	// Intersect with cartilage
	IwPoint3d cPnt;
	IntersectSurfaceByLine(cartiMainSurface, posPostCutPnt-50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), cPnt);

	// Then project onto sketchSurfaceJigs
	IwPoint3d posPosteriorTipRefPoint;
	DistFromPointToSurface(cPnt, sketchSurfaceJigs, posPosteriorTipRefPoint, uvParam);
if (0)
ShowPoint(m_pDoc, posPosteriorTipRefPoint, red);

	// Determine the positive side posterior arc
	double rad5deg = 5.0/180.0*IW_PI; 
	double rad10deg = 10.0/180.0*IW_PI; 
	IwPoint3d posPosteriorTipPntUV = IwPoint3d(uvParam.x,uvParam.y,0.0);
	double posPosteriorArcRadius = 0.4*posCondylarCutWidth;
	IwPoint3d posPosteriorArcUVCenter, posPosteriorArcUVStart, posPosteriorArcUVEnd;
	posPosteriorArcUVCenter = posPosteriorTipPntUV - posPosteriorArcRadius*sin(rad5deg)*IwVector3d(1,0,0) - posPosteriorArcRadius*cos(rad5deg)*IwVector3d(0,1,0);
	posPosteriorArcUVEnd = posPosteriorArcUVCenter - posPosteriorArcRadius*cos(rad10deg)*IwVector3d(1,0,0) + posPosteriorArcRadius*sin(rad10deg)*IwVector3d(0,1,0);
	// Need to determine the vector for start point, which make positive jig profile follow the implant profile
	IwPoint3d testPnt0 = posPosteriorArcUVCenter + posPosteriorArcRadius*IwVector3d(1,0,0);
	IwPoint3d testPnt1 = posPosteriorArcUVCenter + posPosteriorArcRadius*IwVector3d(1,0,0) + 0.5*posPosteriorArcRadius*IwVector3d(0,1,0);
	double param;
	IwVector3d tempNormal;
	IwPoint3d cPnt0, cPnt1;
	DistFromPointToCurve(testPnt0, sketchUVCurve, cPnt0, param);
	DistFromPointToCurve(testPnt1, sketchUVCurve, cPnt1, param);
	IwVector3d alongVec = cPnt1 - cPnt0;
	alongVec.Unitize();
	IwVector3d StartPointOutwardVec = alongVec*IwVector3d(0,0,1);

	// Now determine the start point
	posPosteriorArcUVStart = posPosteriorArcUVCenter + posPosteriorArcRadius*StartPointOutwardVec;
	// Make the positive side profile direction and position follow the implant fillet edge
	IwPoint3d posPosteriorArcStartOnFilletEdge;
	pOutlineProfileJigs->ConvertUVToXYZ(posPosteriorArcUVStart, posPosteriorArcStartOnFilletEdge, tempNormal);// to xyz
	DistFromPointToCurve(posPosteriorArcStartOnFilletEdge, filletEdgeCurve, posPosteriorArcStartOnFilletEdge, param); // to fillet edge
	DistFromPointToSurface(posPosteriorArcStartOnFilletEdge, sketchSurfaceJigs, cPnt, uvParam);
	IwPoint3d posPosteriorArcUVStartOnFilletEdge = IwPoint3d(uvParam.x, uvParam.y, 0);
	IwVector3d vectorToMove = posPosteriorArcUVStartOnFilletEdge - posPosteriorArcUVStart; 
	posPosteriorArcUVCenter = posPosteriorArcUVCenter + vectorToMove;
	posPosteriorArcUVStart = posPosteriorArcUVStart + vectorToMove;
	posPosteriorArcUVEnd = posPosteriorArcUVEnd + vectorToMove;

	// Determine reference point for square-off profile
	IwPoint3d posSquareOffRefUVPnt = posPosteriorArcUVStart + 0.5*posPosteriorArcRadius*alongVec;
	IwPoint3d posSquareOffRefPnt;
	pOutlineProfileJigs->ConvertUVToXYZ(posSquareOffRefUVPnt, posSquareOffRefPnt, tempNormal);
	DistFromPointToCurve(posSquareOffRefPnt, filletEdgeCurve, posSquareOffRefPnt, param);
	DistFromPointToSurface(posSquareOffRefPnt, sketchSurfaceJigs, posSquareOffRefPnt, uvParam);

	// Determine negative side posterior reference point
	// Intersect posterior cut with cartilage 
	IwPoint3d negPostCutPnt = cutPoints.GetAt(1);

	// Project onto condylar center plane
	negPostCutPnt = negPostCutPnt.ProjectPointToPlane(0.5*(cutFeaturePoints.GetAt(8)+cutFeaturePoints.GetAt(9)), wslAxes.GetXAxis());//temp change

	// Intersect with cartilage
	IntersectSurfaceByLine(cartiMainSurface, negPostCutPnt-50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), cPnt);

	// Then project onto sketchSurfaceJigs
	IwPoint3d negPosteriorTipRefPoint;
	DistFromPointToSurface(cPnt, sketchSurfaceJigs, negPosteriorTipRefPoint, uvParam);
if (0)
ShowPoint(m_pDoc, negPosteriorTipRefPoint, red);

	// Determine the negative side posterior arc
	IwPoint3d negPosteriorTipPntUV = IwPoint3d(uvParam.x,uvParam.y,0.0);
	double negPosteriorArcRadius = 0.4*negCondylarCutWidth;
	IwPoint3d negPosteriorArcUVCenter, negPosteriorArcUVStart, negPosteriorArcUVEnd;
	negPosteriorArcUVCenter = negPosteriorTipPntUV + negPosteriorArcRadius*sin(rad5deg)*IwVector3d(1,0,0) - negPosteriorArcRadius*cos(rad5deg)*IwVector3d(0,1,0);
	negPosteriorArcUVStart = negPosteriorArcUVCenter + negPosteriorArcRadius*cos(rad10deg)*IwVector3d(1,0,0) + negPosteriorArcRadius*sin(rad10deg)*IwVector3d(0,1,0);
	// Need to determine the vector for end point, which make negative jig profile follow the implant profile
	testPnt0 = negPosteriorArcUVCenter - negPosteriorArcRadius*IwVector3d(1,0,0);
	testPnt1 = negPosteriorArcUVCenter - negPosteriorArcRadius*IwVector3d(1,0,0) + 0.5*negPosteriorArcRadius*IwVector3d(0,1,0);
	DistFromPointToCurve(testPnt0, sketchUVCurve, cPnt0, param);
	DistFromPointToCurve(testPnt1, sketchUVCurve, cPnt1, param);
	alongVec = cPnt1 - cPnt0;
	alongVec.Unitize();
	IwVector3d EndPointOutwardVec = -alongVec*IwVector3d(0,0,1);//

	// Now determine the end point
	negPosteriorArcUVEnd = negPosteriorArcUVCenter + negPosteriorArcRadius*EndPointOutwardVec;
	// Make the negative side profile direction and position follow the implant fillet edge
	IwPoint3d negPosteriorArcEndOnFilletEdge;
	pOutlineProfileJigs->ConvertUVToXYZ(negPosteriorArcUVEnd, negPosteriorArcEndOnFilletEdge, tempNormal);// to xyz
	//ShowPoint(m_pDoc, negPosteriorArcEndOnFilletEdge, red);

	DistFromPointToCurve(negPosteriorArcEndOnFilletEdge, filletEdgeCurve, negPosteriorArcEndOnFilletEdge, param); // to fillet edge
	//ShowPoint(m_pDoc, negPosteriorArcEndOnFilletEdge, green);

	DistFromPointToSurface(negPosteriorArcEndOnFilletEdge, sketchSurfaceJigs, cPnt, uvParam);
	//ShowPoint(m_pDoc, cPnt, blue);

	IwPoint3d negPosteriorArcUVEndOnFilletEdge = IwPoint3d(uvParam.x, uvParam.y, 0);;
	vectorToMove = negPosteriorArcUVEndOnFilletEdge - negPosteriorArcUVEnd; 
	negPosteriorArcUVCenter = negPosteriorArcUVCenter + vectorToMove;
	negPosteriorArcUVStart = negPosteriorArcUVStart + vectorToMove;
	negPosteriorArcUVEnd = negPosteriorArcUVEnd + vectorToMove;

	// Determine reference point for square-off profile
	IwPoint3d negSquareOffRefUVPnt = negPosteriorArcUVEnd + 0.5*negPosteriorArcRadius*alongVec;
	IwPoint3d negSquareOffRefPnt;
	pOutlineProfileJigs->ConvertUVToXYZ(negSquareOffRefUVPnt, negSquareOffRefPnt, tempNormal);
	DistFromPointToCurve(negSquareOffRefPnt, filletEdgeCurve, negSquareOffRefPnt, param);
	DistFromPointToSurface(negSquareOffRefPnt, sketchSurfaceJigs, negSquareOffRefPnt, uvParam);

	// Determine the notch arc
	double rad15deg = 20.0/180.0*IW_PI;// actually we use "20" here.
	double notchArcRadius = 0.80*0.5*(posPosteriorArcRadius+negPosteriorArcRadius);// 80% of averaged posterior radii
	DistFromPointToSurface(m_pDoc->GetFemoralAxes()->GetRefProximalPoint()/*wslAxes.GetOrigin()*/, sketchSurfaceJigs, cPnt, uvParam);
	IwPoint3d notchArcUVCenter, notchArcUVStart, notchArcUVEnd;
	notchArcUVCenter = IwPoint3d(uvParam.x, uvParam.y, 0.0);
	notchArcUVCenter = notchArcUVCenter +lateralNotchShift*IwVector3d(1,0,0) + 0.75*notchArcRadius*IwVector3d(0,1,0);// move laterally 1.0mm & posteriorly 75% of notch radius
	notchArcUVStart = notchArcUVCenter + notchArcRadius*cos(rad15deg)*IwVector3d(1,0,0) - notchArcRadius*sin(rad15deg)*IwVector3d(0,1,0);
	notchArcUVEnd = notchArcUVCenter - notchArcRadius*cos(rad15deg)*IwVector3d(1,0,0) - notchArcRadius*sin(rad15deg)*IwVector3d(0,1,0);

	// Determine the points on positive side distal cut plane
	IwPoint2d uvParam105, uvParam14;
	IwPoint3d point105, point14, sketchPoint105, sketchPoint14;
	double param105, param14;
	DistFromPointToCurve(0.5*cutFeaturePoints.GetAt(10)+0.5*cutFeaturePoints.GetAt(14), filletEdgeCurve, point105, param105);
	DistFromPointToCurve(cutFeaturePoints.GetAt(14), filletEdgeCurve, point14, param14);
	DistFromPointToSurface(point105, sketchSurfaceJigs, sketchPoint105, uvParam105);
	DistFromPointToSurface(point14, sketchSurfaceJigs, sketchPoint14, uvParam14);
	IwPoint3d pointUV105 = IwPoint3d(uvParam105.x,uvParam105.y, 0.0);
	IwPoint3d pointUV14 = IwPoint3d(uvParam14.x,uvParam14.y, 0.0) - 1.0*IwVector3d(1,0,0);
	// The reference point105
	IwPoint3d posRefPoint105 = sketchPoint105;

	// Determine the points on negative side distal cut plane
	IwPoint2d uvParam135, uvParam17;
	IwPoint3d point135, point17, sketchPoint135, sketchPoint17;
	double param135, param17;
	DistFromPointToCurve(0.5*cutFeaturePoints.GetAt(13)+0.5*cutFeaturePoints.GetAt(17), filletEdgeCurve, point135, param135);
	DistFromPointToCurve(cutFeaturePoints.GetAt(17), filletEdgeCurve, point17, param17);
	DistFromPointToSurface(point135, sketchSurfaceJigs, sketchPoint135, uvParam135);
	DistFromPointToSurface(point17, sketchSurfaceJigs, sketchPoint17, uvParam17);
	IwPoint3d pointUV135 = IwPoint3d(uvParam135.x,uvParam135.y, 0.0);
	IwPoint3d pointUV17 = IwPoint3d(uvParam17.x,uvParam17.y, 0.0) + 1.0*IwVector3d(1,0,0);
	// Determine the reference point135
	IwPoint3d negRefPoint135 = sketchPoint135;;

	// Determine the point between pointUV14 && posAntArcUVEnd
	// Determine the point between pointUV17 && negAntArcUVStart
	IwPoint3d posTransitUV, negTransitUV;
	if (m_pDoc->IsPositiveSideLateral())
	{
		posTransitUV = 0.5*(pointUV14+posAntArcUVEnd) - 2.0*refSizeRatio*IwVector3d(1,0,0);
		negTransitUV = 0.5*(pointUV17+negAntArcUVStart) + 4.0*refSizeRatio*IwVector3d(1,0,0);
	}
	else
	{
		posTransitUV = 0.5*(pointUV14+posAntArcUVEnd) - 4.0*refSizeRatio*IwVector3d(1,0,0);
		negTransitUV = 0.5*(pointUV17+negAntArcUVStart) + 2.0*refSizeRatio*IwVector3d(1,0,0);
	}

	///////////////////////////////////////////////////////
	// The posterior arcs and notch arc need to be refined.
	// Positive condylar width near notch
	IwVector3d tempVec0 = pointUV14 - pointUV105;
	tempVec0.Unitize();
	IwVector3d tempVec1 = notchArcUVStart - pointUV105;
	double posCondylarWidth = (tempVec0*tempVec1).Length();
	// Negative condylar width near notch
	tempVec0 = pointUV17 - pointUV135;
	tempVec0.Unitize();
	tempVec1 = notchArcUVEnd - pointUV135;
	double negCondylarWidth = (tempVec0*tempVec1).Length();
	//
	double notchCenterPositiveAdjustment = posCondylarWidth-2*posPosteriorArcRadius;
	double notchCenterNegativeAdjustment = negCondylarWidth-2*negPosteriorArcRadius;
	double notchRadiusAdjustment = 0.25*notchCenterPositiveAdjustment + 0.25*notchCenterNegativeAdjustment;
	double notchCenterShift = 0.5*notchCenterPositiveAdjustment - 0.5*notchCenterNegativeAdjustment;
	// Adjust notch arc radius
	notchArcRadius += notchRadiusAdjustment;
	tempVec0 = notchArcUVStart - notchArcUVCenter;
	tempVec0.Unitize();
	notchArcUVStart = notchArcUVCenter + notchArcRadius*tempVec0;
	tempVec0 = notchArcUVEnd - notchArcUVCenter;
	tempVec0.Unitize();
	notchArcUVEnd = notchArcUVCenter + notchArcRadius*tempVec0;
	// Shift notch arc ML direction
	notchArcUVCenter += IwVector3d(notchCenterShift,0,0);
	notchArcUVStart += IwVector3d(notchCenterShift,0,0);
	notchArcUVEnd += IwVector3d(notchCenterShift,0,0);
	//
	IwPoint3d notchArcCenterXYZ;
	IwVector3d normal;
	pOutlineProfileJigs->ConvertUVToXYZ(notchArcUVCenter, notchArcCenterXYZ, normal);

	// Determine the notch arc reference 
	double notchRefDist = 4.0;
	IwPoint3d notchArcPeakRefPoint;
	IntersectSurfaceByLine(sketchSurfaceJigs, m_pDoc->GetFemoralAxes()->GetRefProximalPoint()/*wslAxes.GetOrigin()*/ + notchRefDist*wslAxes.GetYAxis(), wslAxes.GetZAxis(), notchArcPeakRefPoint);
	// Project to sagittal plane at notchArcCenterXYZ
	notchArcPeakRefPoint = notchArcPeakRefPoint.ProjectPointToPlane(notchArcCenterXYZ, wslAxes.GetXAxis());
	// Determine notchArcPeakRefPnt on uv domain
	DistFromPointToSurface(notchArcPeakRefPoint, sketchSurfaceJigs, cPnt, uvParam);
	//
	IwPoint3d notchArcPeak = notchArcUVCenter - notchArcRadius*IwVector3d(0,1,0);// move anteriorly with notch radius
	// How much to move
	tempVec = uvParam - notchArcPeak;
	// Move the notch arc to contact the notchArcPeakRefPnt
	notchArcUVCenter += tempVec;
	notchArcUVStart += tempVec;
	notchArcUVEnd += tempVec;

	// Determine positive side condylar angle by posPosteriorArcUVEnd and notchArcUVStart
	tempVec0 = notchArcUVStart - posPosteriorArcUVEnd;
	double posCondylarAngleRad;
	tempVec0.AngleBetween(IwVector3d(0,-1,0),posCondylarAngleRad);
	// update posPosteriorArcUVEnd and notchArcUVStart
	posPosteriorArcUVEnd = posPosteriorArcUVCenter - posPosteriorArcRadius*cos(posCondylarAngleRad)*IwVector3d(1,0,0) + posPosteriorArcRadius*sin(posCondylarAngleRad)*IwVector3d(0,1,0);
	notchArcUVStart = notchArcUVCenter + notchArcRadius*cos(posCondylarAngleRad)*IwVector3d(1,0,0) - notchArcRadius*sin(posCondylarAngleRad)*IwVector3d(0,1,0);
	// Side product - posInnerSquareOffRefPnt
	IwPoint3d posInnerSquareOffRefPnt;
	tempVec = posPosteriorArcUVEnd - posPosteriorArcUVCenter;
	tempVec.Unitize();
	IwVector3d towardNotchVec = IwVector3d(0,0,1)*tempVec;
	IwPoint3d posInnerSquareOffRefUVPnt = posPosteriorArcUVEnd + 1.0*towardNotchVec; // "1.0" to prevent from overlapping with arc end point
	pOutlineProfileJigs->ConvertUVToXYZ(posInnerSquareOffRefUVPnt, posInnerSquareOffRefPnt, normal);

	// Determine positive side condylar angle by negPosteriorArcUVStart and notchArcUVEnd
	tempVec0 = notchArcUVEnd - negPosteriorArcUVStart;
	double negCondylarAngleRad;
	tempVec0.AngleBetween(IwVector3d(0,-1,0),negCondylarAngleRad);
	// update negPosteriorArcUVStart and notchArcUVEnd
	negPosteriorArcUVStart = negPosteriorArcUVCenter + negPosteriorArcRadius*cos(negCondylarAngleRad)*IwVector3d(1,0,0) + negPosteriorArcRadius*sin(negCondylarAngleRad)*IwVector3d(0,1,0);
	notchArcUVEnd = notchArcUVCenter - notchArcRadius*cos(rad15deg)*IwVector3d(1,0,0) - notchArcRadius*sin(negCondylarAngleRad)*IwVector3d(0,1,0);
	// Side product - negInnerSquareOffRefPnt
	IwPoint3d negInnerSquareOffRefPnt;
	tempVec = negPosteriorArcUVStart - negPosteriorArcUVCenter;
	tempVec.Unitize();
	towardNotchVec = tempVec*IwVector3d(0,0,1);
	IwPoint3d negInnerSquareOffRefUVPnt = negPosteriorArcUVStart + 1.0*towardNotchVec; // "1.0" to prevent from overlapping with arc end point
	pOutlineProfileJigs->ConvertUVToXYZ(negInnerSquareOffRefUVPnt, negInnerSquareOffRefPnt, normal);

	// Refine pointUV105 & pointUV135 to get a better shape.
	IwVector3d tempCross;
	tempVec0 = pointUV14 - posPosteriorArcUVStart;
	tempVec0.Unitize();
	tempVec1 = IwPoint3d(uvParam105.x,uvParam105.y,0)  - posPosteriorArcUVStart;
	tempCross = tempVec0*tempVec1;
	double dev105 = tempCross.Length();
	if ( dev105 < 0.75 ) // move outer a bit always get a better result
		dev105 = 0.75;
	else if ( dev105 > 2.5 ) // But if move outer too much is not good
		dev105 = dev105 - 0.75;
	pointUV105 = 0.5*(pointUV14 + posPosteriorArcUVStart) + dev105*IwVector3d(1,0,0);
	//
	tempVec0 = pointUV17 - negPosteriorArcUVEnd;
	tempVec0.Unitize();
	tempVec1 = IwPoint3d(uvParam135.x,uvParam135.y,0)  - negPosteriorArcUVEnd;
	tempCross = tempVec0*tempVec1;
	double dev135 = tempCross.Length();
	if ( dev135 < 0.75 ) // move outer a bit always get a better result
		dev135 = 0.75;
	else if ( dev135 > 2.5 ) // But if move outer too much is not good
		dev135 = dev135 - 0.75;
	pointUV135 = 0.5*(pointUV17 + negPosteriorArcUVEnd) - dev135*IwVector3d(1,0,0); 

	///////////////////////////////////////////////////////
	// Add to m_currFeaturePoints one by one
	// antTipPointUV
	m_currFeaturePoints.Add(antTipPointUV);
	m_currFeaturePointsInfo.Add(0);
	// positive ant arc
	m_currFeaturePoints.Add(posAntArcUVStart);
	m_currFeaturePointsInfo.Add(3);
	m_currFeaturePoints.Add(posAntArcUVCenter);
	m_currFeaturePointsInfo.Add(1);
	m_currFeaturePoints.Add(posAntArcUVEnd);
	m_currFeaturePointsInfo.Add(4);
	// posTransitUV
	m_currFeaturePoints.Add(posTransitUV);
	m_currFeaturePointsInfo.Add(0);
	// pointUV14 && pointUV105
	m_currFeaturePoints.Add(pointUV14);
	m_currFeaturePointsInfo.Add(0);
	m_currFeaturePoints.Add(pointUV105);
	m_currFeaturePointsInfo.Add(0);
	// positive posterior arc
	m_currFeaturePoints.Add(posPosteriorArcUVStart);
	m_currFeaturePointsInfo.Add(3);
	m_currFeaturePoints.Add(posPosteriorArcUVCenter);
	m_currFeaturePointsInfo.Add(1);
	m_currFeaturePoints.Add(posPosteriorArcUVEnd);
	m_currFeaturePointsInfo.Add(4);
	// notch arc
	m_currFeaturePoints.Add(notchArcUVStart);
	m_currFeaturePointsInfo.Add(3);
	m_currFeaturePoints.Add(notchArcUVCenter);
	m_currFeaturePointsInfo.Add(2);
	m_currFeaturePoints.Add(notchArcUVEnd);
	m_currFeaturePointsInfo.Add(4);
	// negative posterior arc
	m_currFeaturePoints.Add(negPosteriorArcUVStart);
	m_currFeaturePointsInfo.Add(3);
	m_currFeaturePoints.Add(negPosteriorArcUVCenter);
	m_currFeaturePointsInfo.Add(1);
	m_currFeaturePoints.Add(negPosteriorArcUVEnd);
	m_currFeaturePointsInfo.Add(4);
	// pointUV135 && pointUV17
	m_currFeaturePoints.Add(pointUV135);
	m_currFeaturePointsInfo.Add(0);
	m_currFeaturePoints.Add(pointUV17);
	m_currFeaturePointsInfo.Add(0);
	// negative transit point
	m_currFeaturePoints.Add(negTransitUV);
	m_currFeaturePointsInfo.Add(0);
	// negative ant arc
	m_currFeaturePoints.Add(negAntArcUVStart);
	m_currFeaturePointsInfo.Add(3);
	m_currFeaturePoints.Add(negAntArcUVCenter);
	m_currFeaturePointsInfo.Add(1);
	m_currFeaturePoints.Add(negAntArcUVEnd);
	m_currFeaturePointsInfo.Add(4);
	// seam point (antTipPointUV)
	m_currFeaturePoints.Add(antTipPointUV);
	m_currFeaturePointsInfo.Add(0);


	// Reference points. Not the sequence
	m_referencePoints.Add(posAntProfileRefPoint); //[0]
	m_referencePoints.Add(negAntProfileRefPoint); //[1]
	m_referencePoints.Add(antSeamRefPoint);//[2] ant seam point
	m_referencePoints.Add(posPosteriorTipRefPoint);//[3] positive side posterior reference point
	m_referencePoints.Add(negPosteriorTipRefPoint);//[4] negative side posterior reference point
	m_referencePoints.Add(posRefPoint105);//[5] the reference point105
	m_referencePoints.Add(negRefPoint135);//[6] reference point135
	m_referencePoints.Add(notchArcPeakRefPoint);//[7] notch arc reference point
	m_referencePoints.Add(posSquareOffRefPnt); // [8] positive side 2nd posterior reference point
	m_referencePoints.Add(negSquareOffRefPnt); // [9] negative side 2nd posterior reference point
	m_referencePoints.Add(posInnerSquareOffRefPnt); // [10] positive inner side posterior reference point
	m_referencePoints.Add(negInnerSquareOffRefPnt); // [11] negative inner side reference point

	//IntersectSurfaceByLine(cartiMainSurface, cutFeaturePoints[2] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), cPnt);
	//DistFromPointToSurface(cPnt, sketchSurfaceJigs, m_posReflinePnt1, uvParam);

	//IntersectSurfaceByLine(cartiMainSurface, cutFeaturePoints[3] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), cPnt);
	//DistFromPointToSurface(cPnt, sketchSurfaceJigs, m_posReflinePnt2, uvParam);

	//IntersectSurfaceByLine(cartiMainSurface, cutFeaturePoints[4] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), cPnt);
	//DistFromPointToSurface(cPnt, sketchSurfaceJigs, m_negReflinePnt1, uvParam);

	//IntersectSurfaceByLine(cartiMainSurface, cutFeaturePoints[5] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), cPnt);
	//DistFromPointToSurface(cPnt, sketchSurfaceJigs, m_negReflinePnt2, uvParam);


	IwPoint3d posReflinePnt1, posReflinePnt2, negReflinePnt1, negReflinePnt2;
	IntersectSurfaceByLine(sketchSurfaceJigs, cutFeaturePoints[2] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), posReflinePnt1);
	IntersectSurfaceByLine(sketchSurfaceJigs, cutFeaturePoints[3] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), posReflinePnt2);
	IntersectSurfaceByLine(sketchSurfaceJigs, cutFeaturePoints[4] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), negReflinePnt1);
	IntersectSurfaceByLine(sketchSurfaceJigs, cutFeaturePoints[5] - 50*wslAxes.GetZAxis(), wslAxes.GetZAxis(), negReflinePnt2);
	
	pOutlineProfileJigs->SetRefLinePoints(posReflinePnt1, posReflinePnt2, negReflinePnt1, negReflinePnt2);

	if ( sketch3dCurve )
		IwObjDelete(sketch3dCurve);
}

void CDefineOutlineProfileJigs::DetermineDroppingCenters()
{
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;

	IwTArray<IwPoint3d> outlineProfileProjectedCurve;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(outlineProfileProjectedCurve);
	if ( outlineProfileProjectedCurve.GetSize() == 0 )
		return;

	// Get osteophyte surface, cartilage surface
	CCartilageSurface* pCartiSurface = m_pDoc->GetCartilageSurface();
	if ( pCartiSurface == NULL )
		return;
	IwBSplineSurface* cartiSurf = pCartiSurface->GetSinglePatchSurface(true);

	CFemoralPart* osteoSurface = m_pDoc->GetOsteophyteSurface();
	if ( osteoSurface == NULL )
		return;
	IwBSplineSurface* osteoSurf = osteoSurface->GetSinglePatchSurface();

	IwTArray<IwSurface*> surfaces;
	surfaces.Add(cartiSurf);
	surfaces.Add(osteoSurf);

	// Get femoral axes
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	double refSize = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize();

	// Get femoral cuts feature points
	IwTArray<IwPoint3d> cutFeaturePoints, antOuterPoints;
	IFemurImplant::FemoralCuts_GetCutsFeaturePoints(cutFeaturePoints, antOuterPoints);

	// Get peg position
	IwPoint3d posPegPosition, negPegPosition;

	pOutlineProfileJigs->EstimatePegPositions(posPegPosition, negPegPosition);

	// 
	IwPoint3d antChamPnt = cutFeaturePoints.GetAt(18);


	// Cycle through projectedCurvePoints and determine the closest point on the (antChamPnt, z) plane
	IwPoint3d pnt, posAntPnt, negAntPnt;
	int posAntIndex, negAntIndex;
	IwVector3d tempVec;
	int nSize = (int)(0.5*outlineProfileProjectedCurve.GetSize());
	double dist, minDist=HUGE_DOUBLE;
	for (int i=0; i<nSize; i++)
	{
		tempVec = outlineProfileProjectedCurve.GetAt(i) - antChamPnt;
		dist = fabs(tempVec.Dot(wslAxes.GetZAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			posAntPnt = outlineProfileProjectedCurve.GetAt(i);
			posAntIndex = i;
		}
	}
	minDist=HUGE_DOUBLE;
	for (unsigned i=nSize; i<outlineProfileProjectedCurve.GetSize(); i++)
	{
		tempVec = outlineProfileProjectedCurve.GetAt(i) - antChamPnt;
		dist = fabs(tempVec.Dot(wslAxes.GetZAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			negAntPnt = outlineProfileProjectedCurve.GetAt(i);
			negAntIndex = i;
		}
	}
if (0)
{
	ShowPoint(m_pDoc, posAntPnt, red);
	ShowPoint(m_pDoc, negAntPnt, blue);
}

	// Cycle through projectedCurvePoints and determine the closest point on the (origin, y) plane
	IwPoint3d posMidPnt, negMidPnt;
	int posMidIndex, negMidIndex;
	nSize = (int)outlineProfileProjectedCurve.GetSize();
	minDist=HUGE_DOUBLE;
	for (int i=0; i<0.4*nSize; i++)
	{
		tempVec = outlineProfileProjectedCurve.GetAt(i) - wslAxes.GetOrigin();
		dist = fabs(tempVec.Dot(wslAxes.GetYAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			posMidPnt = outlineProfileProjectedCurve.GetAt(i);
			posMidIndex = i;
		}
	}
	minDist=HUGE_DOUBLE;
	int startIndex = (int)(0.6*nSize);
	for (unsigned i=startIndex; i<outlineProfileProjectedCurve.GetSize(); i++)
	{
		tempVec = outlineProfileProjectedCurve.GetAt(i) - wslAxes.GetOrigin();
		dist = fabs(tempVec.Dot(wslAxes.GetYAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			negMidPnt = outlineProfileProjectedCurve.GetAt(i);
			negMidIndex = i;
		}
	}
if (0)
{
	ShowPoint(m_pDoc, posMidPnt, red);
	ShowPoint(m_pDoc, negMidPnt, blue);
}

	// Determine the starting point on positive condylar tip arc, and
	// the ending point on negative condylar tip arc
	IwPoint3d posPostPnt, negPostPnt;
	IwPoint3d awayPnt;
	IwVector3d normal;
	IwPoint3d intPnt;
	IwBSplineSurface* sketchSurf;
	pOutlineProfileJigs->GetSketchSurface(sketchSurf);
	IwTArray<IwPoint3d> featurePoints;
	IwTArray<int> featurePointsInfo;
	pOutlineProfileJigs->GetOutlineProfileFeaturePoints(featurePoints, featurePointsInfo);
	int encounteredNo = 0;
	for (unsigned i=0; i<featurePointsInfo.GetSize(); i++)
	{
		if ( featurePointsInfo.GetAt(i) == 1 )
		{
			if ( encounteredNo == 1 )// second not collapsible CCW arc center
			{
				IwPoint2d uv = IwPoint2d(featurePoints.GetAt(i-1).x, featurePoints.GetAt(i-1).y);// starting point
				sketchSurf->EvaluatePoint(uv, pnt);
				sketchSurf->EvaluateNormal(uv, FALSE, FALSE, normal);
				awayPnt = pnt + 30*normal;
				bool bInt = IntersectSurfacesByLine(surfaces, awayPnt, normal, intPnt);
				if ( bInt )
				{
						posPostPnt = intPnt;
				}
			}
			encounteredNo++;
		}
	}
	encounteredNo = 0;
	for (unsigned i=0; i<featurePointsInfo.GetSize(); i++)
	{
		if ( featurePointsInfo.GetAt(i) == 1 )
		{
			if ( encounteredNo == 2 )// third not collapsible CCW arc center
			{
				IwPoint2d uv = IwPoint2d(featurePoints.GetAt(i+1).x, featurePoints.GetAt(i+1).y);// ending point
				sketchSurf->EvaluatePoint(uv, pnt);
				sketchSurf->EvaluateNormal(uv, FALSE, FALSE, normal);
				awayPnt = pnt + 30*normal;
				bool bInt = IntersectSurfacesByLine(surfaces, awayPnt, normal, intPnt);
				if ( bInt )
				{
					negPostPnt = intPnt;
				}
			}
			encounteredNo++;
		}
	}
if (0)
{
ShowPoint(m_pDoc, posPostPnt, red);
ShowPoint(m_pDoc, negPostPnt, blue);
}

	// Determine the tab dropping centers
	IwPoint3d posAntProjPnt = posAntPnt.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());
	IwPoint3d posMidProjPnt = posMidPnt.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());
	IwPoint3d posPostProjPnt = posPostPnt.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());
	m_currPosDroppingCenter = ComputeArcCenter(posAntProjPnt, posMidProjPnt, posPostProjPnt);
	// m_posDroppingCenter should be in negative side of femur
	tempVec = m_currPosDroppingCenter - (wslAxes.GetOrigin()-0.75*refSize*wslAxes.GetXAxis());
	if ( tempVec.Dot(wslAxes.GetXAxis()) > 0 )
	{
		m_currPosDroppingCenter = m_currPosDroppingCenter.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		m_currPosDroppingCenter = m_currPosDroppingCenter - 1.0*refSize*wslAxes.GetXAxis();
	}
	// Should not be higher than positive y-axis
	tempVec = m_currPosDroppingCenter - wslAxes.GetOrigin();
	if ( tempVec.Dot(wslAxes.GetYAxis()) > 0 )
	{
		m_currPosDroppingCenter = m_currPosDroppingCenter.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetYAxis());
	}
	// Should not be too posterior (too low)
	tempVec = m_currPosDroppingCenter - wslAxes.GetOrigin();
	if ( tempVec.Dot(wslAxes.GetYAxis()) < -1.0*refSize )
	{
		m_currPosDroppingCenter = m_currPosDroppingCenter.ProjectPointToPlane(wslAxes.GetOrigin()-1.0*refSize*wslAxes.GetYAxis(), wslAxes.GetYAxis());
	}
	// m_posDroppingCenter should not be too far away posMidProjPnt
	if ( m_currPosDroppingCenter.DistanceBetween(posMidProjPnt) > 1.0*refSize )
	{
		tempVec = m_currPosDroppingCenter - posMidProjPnt;
		tempVec.Unitize();
		m_currPosDroppingCenter = posMidProjPnt + 1.0*refSize*tempVec;
	}

	IwPoint3d negAntProjPnt = negAntPnt.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());
	IwPoint3d negMidProjPnt = negMidPnt.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());
	IwPoint3d negPostProjPnt = negPostPnt.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetZAxis());
	m_currNegDroppingCenter = ComputeArcCenter(negAntProjPnt, negMidProjPnt, negPostProjPnt);
	// m_negDroppingCenter should be in positive side of femur
	tempVec = m_currNegDroppingCenter - (wslAxes.GetOrigin()+0.75*refSize*wslAxes.GetXAxis());
	if ( tempVec.Dot(wslAxes.GetXAxis()) < 0 )
	{
		m_currNegDroppingCenter = m_currNegDroppingCenter.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		m_currNegDroppingCenter = m_currNegDroppingCenter + 1.0*refSize*wslAxes.GetXAxis();
	}
	// Should not be higher than positive y-axis
	tempVec = m_currNegDroppingCenter - wslAxes.GetOrigin();
	if ( tempVec.Dot(wslAxes.GetYAxis()) > 0 )
	{
		m_currNegDroppingCenter = m_currNegDroppingCenter.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetYAxis());
	}
	// Should not be too posterior (too low)
	tempVec = m_currNegDroppingCenter - wslAxes.GetOrigin();
	if ( tempVec.Dot(wslAxes.GetYAxis()) < -1.0*refSize )
	{
		m_currNegDroppingCenter = m_currNegDroppingCenter.ProjectPointToPlane(wslAxes.GetOrigin()-1.0*refSize*wslAxes.GetYAxis(), wslAxes.GetYAxis());
	}
	// m_negDroppingCenter should not be too far away negMidProjPnt
	if ( m_currNegDroppingCenter.DistanceBetween(negMidProjPnt) > 1.0*refSize )
	{
		IwVector3d tempVec = m_currNegDroppingCenter - negMidProjPnt;
		tempVec.Unitize();
		m_currNegDroppingCenter = negMidProjPnt + 1.0*refSize*tempVec;
	}

if (0)
{
ShowPoint(m_pDoc, m_currPosDroppingCenter, red);
ShowPoint(m_pDoc, m_currNegDroppingCenter, blue);
}

}

////////////////////////////////////////////////////////////////////////////////
// This function can only be called when DetermineOutlineProfileJigs() is done. 
void CDefineOutlineProfileJigs::DetermineTabLocations()
{
	// Get outlineProfileJigs projected curve points
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;

	IwTArray<IwPoint3d> projectedCurvePoints;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedCurvePoints);
	if ( projectedCurvePoints.GetSize() == 0 )
		return;

	// Get femoral axes
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;

	// Get femoral cuts feature points
	IwTArray<IwPoint3d> cutFeaturePoints, antOuterPoints;
	IFemurImplant::FemoralCuts_GetCutsFeaturePoints(cutFeaturePoints, antOuterPoints);

	// Get peg position
	IwPoint3d posPegPosition, negPegPosition;
	pOutlineProfileJigs->EstimatePegPositions(posPegPosition, negPegPosition);

	// 
	IwPoint3d antChamPnt = cutFeaturePoints.GetAt(18);
	ShowPoint(m_pDoc, antChamPnt, yellow);//PSV

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Cycle through projectedCurvePoints and determine the closest point on the (antChamPnt, z) plane
	IwPoint3d pnt, posAntPnt, negAntPnt;
	int posAntIndex, negAntIndex;
	IwVector3d tempVec;
	int nSize = (int)(0.5*projectedCurvePoints.GetSize());
	double dist, minDist=HUGE_DOUBLE;
	for (int i=0; i<nSize; i++)
	{
		tempVec = projectedCurvePoints.GetAt(i) - antChamPnt;
		dist = fabs(tempVec.Dot(wslAxes.GetYAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			posAntPnt = projectedCurvePoints.GetAt(i);
			posAntIndex = i;
		}
	}
	// Move anteriorly 5 points
	if ( posAntIndex-5 > 0 )
		posAntPnt = projectedCurvePoints.GetAt(posAntIndex-5);

	// Negative side
	minDist=HUGE_DOUBLE;
	for (unsigned i=nSize; i<projectedCurvePoints.GetSize(); i++)
	{
		tempVec = projectedCurvePoints.GetAt(i) - antChamPnt;
		dist = fabs(tempVec.Dot(wslAxes.GetYAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			negAntPnt = projectedCurvePoints.GetAt(i);
			negAntIndex = i;
		}
	}
	// Move anteriorly 5 points
	if ( negAntIndex+5 < (int)projectedCurvePoints.GetSize() )
		negAntPnt = projectedCurvePoints.GetAt(negAntIndex+5);

if (0)//PSV
{
	ShowPoint(m_pDoc, posAntPnt, orange);
	ShowPoint(m_pDoc, negAntPnt, gray);
}

	// Cycle through projectedCurvePoints and determine the closest point on the (pegPosition, y) plane
	IwPoint3d posPegPnt, negPegPnt;
	int posPegIndex, negPegIndex;
	nSize = (int)projectedCurvePoints.GetSize();
	minDist=HUGE_DOUBLE;
	for (int i=0; i<0.4*nSize; i++)
	{
		tempVec = projectedCurvePoints.GetAt(i) - posPegPosition;
		dist = fabs(tempVec.Dot(wslAxes.GetYAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			posPegPnt = projectedCurvePoints.GetAt(i);
			posPegIndex = i;
		}
	}
	// Move anteriorly 2 points
	posPegIndex -= 2;
	posPegPnt = projectedCurvePoints.GetAt(posPegIndex);

	// Negative side
	minDist=HUGE_DOUBLE;
	int startIndex = (int)(0.6*nSize);
	for (unsigned i=startIndex; i<projectedCurvePoints.GetSize(); i++)
	{
		tempVec = projectedCurvePoints.GetAt(i) - negPegPosition;
		dist = fabs(tempVec.Dot(wslAxes.GetYAxis()));
		if ( dist < minDist )
		{
			minDist = dist;
			negPegPnt = projectedCurvePoints.GetAt(i);
			negPegIndex = i;
		}
	}
	// Move anteriorly 2 points
	negPegIndex += 2;
	negPegPnt = projectedCurvePoints.GetAt(negPegIndex);

if (0)//PSV
{
	ShowPoint(m_pDoc, posPegPnt, orange);
	ShowPoint(m_pDoc, negPegPnt, gray);
}

	///////////////////////////////////////////////////////////////////////////////////
	// Medial anterior tab is from antPnt to a point away from 15.0mm (tab width 5~20mm)
	double maxTabWidth = m_pDoc->GetVarTableValue("JIGS TAB MAX WIDTH");
	double minTabWidth = m_pDoc->GetVarTableValue("JIGS TAB MIN WIDTH");
	IwPoint3d medialAntTab0, medialAntTab1;
	unsigned medialAntTabIndex1;
	double ref15 = 15.0; // target at 15.0 regardless size
	if ( ref15 > maxTabWidth )
		ref15 = maxTabWidth;
	else if (ref15 < (minTabWidth+3.0) ) // "3.0" as allowance
		ref15 = 5.0 + 3.0;
	if ( positiveSideIsLateral )
	{
		medialAntTab0 = negAntPnt;
		for (unsigned i=negAntIndex; i>0; i--)
		{
			tempVec = projectedCurvePoints.GetAt(i) - negAntPnt;
			//tempVec = tempVec.ProjectToPlane(wslAxes.GetZAxis());
			dist = tempVec.Length();
			if ( dist > ref15 )
			{
				// Actually, we want the previous one 
				medialAntTabIndex1 = i+1;
				medialAntTab1 = projectedCurvePoints.GetAt(medialAntTabIndex1);
				break;
			}
		}
	}
	else
	{
		medialAntTab0 = posAntPnt;
		for (unsigned i=posAntIndex; i<projectedCurvePoints.GetSize(); i++)
		{
			tempVec = projectedCurvePoints.GetAt(i) - posAntPnt;
			//tempVec = tempVec.ProjectToPlane(wslAxes.GetZAxis());
			dist = tempVec.Length();
			if ( dist > ref15 )
			{
				// Actually, we want the previous one 
				medialAntTabIndex1 = i-1;
				medialAntTab1 = projectedCurvePoints.GetAt(medialAntTabIndex1);
				break;
			}
		}
	}
if (0)//PSV
{
ShowPoint(m_pDoc, medialAntTab0, red);
ShowPoint(m_pDoc, medialAntTab1, magenta);
}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Medial peg tab is defined by two points centered by pegPnt and away 15.0mm (tab width 5~20mm)
	double ref75 = 7.5; // target at 7.5x2=15.0 regardless size
	if ( ref75 > maxTabWidth/2.0 ) 
		ref75 = maxTabWidth/2.0;
	else if (ref75 < (minTabWidth/2.0+1.5) ) // "1.5" as allowance
		ref75 = (minTabWidth/2.0+1.5);

	IwPoint3d medialPegTab0, medialPegTab1;
	unsigned medialPegTabIndex0, medialPegTabIndex1;
	if ( positiveSideIsLateral )
	{
		for (unsigned i=negPegIndex; i>0; i--)
		{
			tempVec = projectedCurvePoints.GetAt(i) - negPegPnt;
			//tempVec = tempVec.ProjectToPlane(wslAxes.GetZAxis());
			dist = tempVec.Length();
			if ( dist > ref75 )
			{
				// Actually we want the previous one
				medialPegTabIndex1 = i+1;
				medialPegTab1 = projectedCurvePoints.GetAt(medialPegTabIndex1);
				medialPegTabIndex0 = 2*negPegIndex-i-1;
				medialPegTab0 = projectedCurvePoints.GetAt(medialPegTabIndex0);
				break;
			}
		}
	}
	else
	{
		for (unsigned i=posPegIndex; i<projectedCurvePoints.GetSize(); i++)
		{
			tempVec = projectedCurvePoints.GetAt(i) - posPegPnt;
			//tempVec = tempVec.ProjectToPlane(wslAxes.GetZAxis());
			dist = tempVec.Length();
			if ( dist > ref75 )
			{
				medialPegTabIndex1 = i-1;
				medialPegTab1 = projectedCurvePoints.GetAt(medialPegTabIndex1);
				medialPegTabIndex0 = 2*posPegIndex-i+1;
				medialPegTab0 = projectedCurvePoints.GetAt(medialPegTabIndex0);
				break;
			}
		}
	}
if (0)
{
ShowPoint(m_pDoc, medialPegTab0, red);
ShowPoint(m_pDoc, medialPegTab1, magenta);
}
	// We need to check whether both medial tabs are too close
	if ( medialAntTab1.DistanceBetween(medialPegTab0) < 6.0 )
	{
		int neededIndex = (int) ((6.0 - medialAntTab1.DistanceBetween(medialPegTab0))/2.0) + 1;
		if ( positiveSideIsLateral )
			neededIndex = -1.0*neededIndex;
		medialAntTab1 = projectedCurvePoints.GetAt(medialAntTabIndex1-neededIndex);
		medialPegTab0 = projectedCurvePoints.GetAt(medialPegTabIndex0+neededIndex);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// Lateral mid tab is defined by two points whose lower (posterior) point is pegPnt and away 15mm (tab width 5~20mm)
	IwPoint3d lateralMidTab0, lateralMidTab1;
	int lateralMidTabIndex0, lateralMidTabIndex1;
	if ( positiveSideIsLateral )
	{
		for (unsigned i=posPegIndex; i>0; i--)
		{
			tempVec = projectedCurvePoints.GetAt(i) - posPegPnt;
			//tempVec = tempVec.ProjectToPlane(wslAxes.GetZAxis());
			dist = tempVec.Length();
			if ( dist > ref15 )
			{
				// actually we want the previous one
				lateralMidTabIndex0 = i+1;
				lateralMidTab0 = projectedCurvePoints.GetAt(lateralMidTabIndex0);
				lateralMidTabIndex1 = posPegIndex;
				lateralMidTab1 = projectedCurvePoints.GetAt(lateralMidTabIndex1);
				break;
			}
		}
	}
	else
	{
		for (unsigned i=negPegIndex; i<projectedCurvePoints.GetSize(); i++)
		{
			tempVec = projectedCurvePoints.GetAt(i) - negPegPnt;
			//tempVec = tempVec.ProjectToPlane(wslAxes.GetZAxis());
			dist = tempVec.Length();
			if ( dist > ref15 )
			{
				// actually we want the previous one
				lateralMidTabIndex0 = i-1;
				lateralMidTab0 = projectedCurvePoints.GetAt(lateralMidTabIndex0);
				lateralMidTabIndex1 = negPegIndex;
				lateralMidTab1 = projectedCurvePoints.GetAt(lateralMidTabIndex1);
				break;
			}
		}
	}
if (0)
{
ShowPoint(m_pDoc, lateralMidTab0, red);
ShowPoint(m_pDoc, lateralMidTab1, magenta);
}

	// Make tab width = ref15 as possible
	tempVec = medialAntTab1 - medialAntTab0;
	double length = tempVec.Length();
	tempVec.Unitize();
	medialAntTab1 = medialAntTab0 + ref15*tempVec;
	// 
	tempVec = medialPegTab1 - medialPegTab0;
	length = tempVec.Length();
	tempVec.Unitize();
	medialPegTab1 = medialPegTab0 + ref15*tempVec;
	// 
	tempVec = lateralMidTab1 - lateralMidTab0;
	length = tempVec.Length();
	tempVec.Unitize();
	lateralMidTab1 = lateralMidTab0 + ref15*tempVec;

	// Update m_currTabPoints and set to pOutlineProfileJigs
	m_currTabPoints.RemoveAll();
	m_currTabPoints.Add(medialAntTab0);
	m_currTabPoints.Add(medialAntTab1);
	m_currTabPoints.Add(lateralMidTab0);
	m_currTabPoints.Add(lateralMidTab1);
	m_currTabPoints.Add(medialPegTab0);
	m_currTabPoints.Add(medialPegTab1);
}

////////////////////////////////////////////////////
// If there is no outline profile jigs, the output 
// tabEndPoints are not initialized points.
////////////////////////////////////////////////////
IwTArray<IwPoint3d> CDefineOutlineProfileJigs::DetermineTabEndLocations
(
	CTotalDoc* pDoc,						// I:
	IwTArray<IwPoint3d>* oTabPoints			// I: optional input for tab points
)
{
	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();

	// Determine m_currTabEndPoints if InnerSurfaceJigs exists.
	IwTArray<IwPoint3d> tabEndPoints;

	IwTArray<IwPoint3d> tabPoints;
	if ( oTabPoints && oTabPoints->GetSize()>0 ) // if caller provides the tab points
	{
		tabPoints.Append(*oTabPoints);
	}
	else // if caller does not provide, get them from pOutlineProfileJigs
	{
		pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
	}
	unsigned nSize = tabPoints.GetSize();
	if ( nSize == 0 )
		return tabEndPoints;
	for (unsigned i=0; i<nSize; i++)
		tabEndPoints.Add(IwPoint3d());// uninitialized yet.

	// If no inner surface jigs object, return.
	CInnerSurfaceJigs* pInnerSurfaceJigs = pDoc->GetInnerSurfaceJigs();
	if ( pInnerSurfaceJigs == NULL )
		return tabEndPoints;
	// Note: however, InnerSurfaceJigs calls this function during its initialization.
	// pInnerSurfaceJigs object do exist, but the surface may not created yet.
	// innerBSurface serves as optional to project the tab end points onto it.
	IwBSplineSurface *innerBSurface = NULL;
	IwBrep *innerSurfBrep = pInnerSurfaceJigs->GetIwBrep();
	if ( innerSurfBrep != NULL )
	{
		IwTArray<IwFace*> faces;
		innerSurfBrep->GetFaces(faces);
		if ( faces.GetSize() > 0 )
		{
			innerBSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
		}
	}
	//
	IwTArray<IwPoint3d> cutPnts, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPnts, cutOrientations);
	IwPoint3d posDropCenter, negDropCenter;
	IwPoint3d dropCenterForMedialSide, dropCenterForLateralSide;
	pOutlineProfileJigs->GetDroppingCenters(posDropCenter, negDropCenter);
	IwPoint3d medialDistalCutPoint, lateralDistalCutPoint;
	// Get side info
	bool positiveSideIsLateral;
	if (pDoc->IsPositiveSideLateral())
	{
		positiveSideIsLateral = true;
		dropCenterForLateralSide = posDropCenter;
		dropCenterForMedialSide = negDropCenter;
		lateralDistalCutPoint = cutPnts.GetAt(6);
		medialDistalCutPoint = cutPnts.GetAt(7);
	}
	else
	{
		positiveSideIsLateral = false;
		dropCenterForLateralSide = negDropCenter;
		dropCenterForMedialSide = posDropCenter;
		lateralDistalCutPoint = cutPnts.GetAt(7);
		medialDistalCutPoint = cutPnts.GetAt(6);
	}

	//
	CFemoralPart* pOsteo = pDoc->GetOsteophyteSurface();
	if ( pOsteo == NULL )
		return tabEndPoints;

	// Get 3 osteophyte surfaces
	IwBSplineSurface* mainSurf = pOsteo->GetSinglePatchSurface();
	IwBSplineSurface *leftSurf, *rightSurf;
	pOsteo->GetSideSurfaces(leftSurf, rightSurf);
	if (mainSurf==NULL)
		return tabEndPoints;
	IwTArray<IwCurve*> silCurves = pOsteo->GetIwCurves();// silhouette curves

	// The dorpping direction, should be the same as CMakeInnerSurfaceJigs::DetermineContactSurface()
	IwAxis2Placement mechAxes = IFemurImplant::FemoralAxes_GetMechanicalAxes();
	IwAxis2Placement rotMat, rotatedAxes;
	double rotRadian = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceApproachDegree()/180.0*IW_PI;
	rotMat.RotateAboutAxis(rotRadian, IwVector3d(1,0,0));
	rotMat.TransformAxis2Placement(mechAxes, rotatedAxes);
	IwVector3d zAxis = rotatedAxes.GetZAxis();
	IwVector3d upVecUnit;
	upVecUnit = zAxis;

	///////////////////////////////////////////////////////////////
	// The default tab length is 10.0*refSizeRatio down.
	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSizeRatio();
	double tabLength = 10.0;
	IwPoint3d pnt00, pnt01, pnt11, pnt10;
	IwTArray<IwPoint3d> intPnts, zHeightValues;
	IwTArray<IwPoint2d>	intUVParams;
	IwTArray<double> intParams;
	double zHeightValue;
	double tabPassingDistance = 2.0; // tab end passing the split curve this distance
	
	//////////////////////////////////////////////////////////////////
	// For medial anterior tab
	///////////////////////////
	// Tab Point 0
	double tabLength0 = tabLength;
	IwVector3d outVecUnit0 = tabPoints.GetAt(0) - dropCenterForMedialSide;
	outVecUnit0 = outVecUnit0.ProjectToPlane(mechAxes.GetZAxis());
	outVecUnit0.Unitize();
	// Create a plane
	pnt00 = tabPoints.GetAt(0) - 2.5*mechAxes.GetYAxis(); // always move posteriorly a bit to make the tab end taper-shape.
	pnt01 = pnt00 + 3*tabLength*outVecUnit0;
	pnt10 = pnt00 + tabLength*upVecUnit;
	pnt11 = pnt00 + 3*tabLength*outVecUnit0 + tabLength*upVecUnit;
	IwBSplineSurface *biPlane0=NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), pnt00, pnt01, pnt10, pnt11, biPlane0);
if (0)
ShowSurface(pDoc, biPlane0, "biPlane0", blue);
	// intersect silCurves with this plane
	IntersectSurfaceByCurves(biPlane0, silCurves, intPnts, intUVParams, intParams);
	// Determine the one with the lowest femoral z value
	if ( intPnts.GetSize() > 0 )
	{
		zHeightValues.RemoveAll();
		for(unsigned i=0; i<intPnts.GetSize(); i++)
		{
			zHeightValue = (intPnts.GetAt(i) - pnt00).Dot(upVecUnit);
			zHeightValues.Add(IwPoint3d(zHeightValue, i, 0));// Note, 3 elements
		}
		SortIwTArray(zHeightValues);
		// Now the lowest z is in 0th element. Its index is in "y".
		IwPoint3d silPnt = intPnts.GetAt((int)(zHeightValues.GetAt(0).y));
		IwPoint3d tabEndPnt = silPnt + tabPassingDistance*upVecUnit;
		tabEndPoints.SetAt(0, tabEndPnt);
if(0)
ShowPoint(pDoc, tabEndPnt, red);
		// update tabLength0
		tabLength0 = (tabEndPnt - pnt00).Dot(upVecUnit);
	}
	///////////////////////////
	// Tab Point 1
	double tabLength1 = tabLength;
	IwVector3d outVecUnit1 = tabPoints.GetAt(1) - dropCenterForMedialSide;
	outVecUnit1 = outVecUnit1.ProjectToPlane(mechAxes.GetZAxis());
	outVecUnit1.Unitize();
	// Create a plane
	pnt00 = tabPoints.GetAt(1);
	pnt01 = pnt00 + 3*tabLength*outVecUnit1;
	pnt10 = pnt00 + tabLength*upVecUnit;
	pnt11 = pnt00 + 3*tabLength*outVecUnit1 + tabLength*upVecUnit;
	IwBSplineSurface *biPlane1=NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), pnt00, pnt01, pnt10, pnt11, biPlane1);
if (0)
ShowSurface(pDoc, biPlane1, "biPlane1", blue);
	// intersect silCurves with this plane
	IntersectSurfaceByCurves(biPlane1, silCurves, intPnts, intUVParams, intParams);
	// Determine the one with the lowest femoral z value
	if ( intPnts.GetSize() > 0 )
	{
		zHeightValues.RemoveAll();
		for(unsigned i=0; i<intPnts.GetSize(); i++)
		{
			zHeightValue = (intPnts.GetAt(i) - pnt00).Dot(upVecUnit);
			zHeightValues.Add(IwPoint3d(zHeightValue, i, 0));// Note, 3 elements
		}
		SortIwTArray(zHeightValues);
		// Now the lowest z is in 0th element. Its index is in "y".
		IwPoint3d silPnt = intPnts.GetAt((int)(zHeightValues.GetAt(0).y));
		IwPoint3d tabEndPnt = silPnt + tabPassingDistance*upVecUnit;
		tabEndPoints.SetAt(1, tabEndPnt);
if(0)
ShowPoint(pDoc, tabEndPnt, red);
		// update tabLength1
		tabLength1 = (tabEndPnt - pnt00).Dot(upVecUnit);
	}

	// Need to check whether Tab End Point 0 & Tab End Point 1 have similar lengths
	if (tabEndPoints.GetAt(0).IsInitialized() && tabEndPoints.GetAt(1).IsInitialized())// both have been determined
	{
		double bigDeviation = 3.0;
		if ( (tabLength0 - tabLength1) > bigDeviation )// Tab End Point 0 is too deep toward shaft (hip)
		{
			IwPoint3d tabEndPoint0 = tabEndPoints.GetAt(0);
			IwPoint3d tabEndPoint1 = tabEndPoints.GetAt(1);
			tabEndPoints.SetAt(0, tabEndPoint0-0.75*upVecUnit); // move a bit toward ankle
			tabEndPoints.SetAt(1, tabEndPoint1+0.75*upVecUnit); // move a bit toward hip
		}
		else if ( (tabLength1 - tabLength0) > bigDeviation )// Tab End Point 1 is too deep toward shaft (hip)
		{
			IwPoint3d tabEndPoint0 = tabEndPoints.GetAt(0);
			IwPoint3d tabEndPoint1 = tabEndPoints.GetAt(1);
			tabEndPoints.SetAt(0, tabEndPoint0+0.75*upVecUnit); // move a bit toward hip
			tabEndPoints.SetAt(1, tabEndPoint1-0.75*upVecUnit); // move a bit toward ankle
		}
	}

	////////////////
	// Now check whether plane0 intersects with silhouette curves or not.
	// If not, use tabLength1 as the tab length down.
	if ( !tabEndPoints.GetAt(0).IsInitialized() )// Not intersect with any silhouette curves
	{
		IwPoint3d tabEndPntOutside = tabPoints.GetAt(0) + tabLength1*upVecUnit + tabLength1*outVecUnit0;
		IwPoint3d tabEndPnt;
		bool gotIt = IntersectSurfaceByLine(mainSurf, tabEndPntOutside, outVecUnit0, tabEndPnt);
		if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength1 )// Not get the intersection point, or the point is not right.
		{
			if ( positiveSideIsLateral )
				gotIt = IntersectSurfaceByLine(leftSurf, tabEndPntOutside, outVecUnit0, tabEndPnt);
			else
				gotIt = IntersectSurfaceByLine(rightSurf, tabEndPntOutside, outVecUnit0, tabEndPnt);
			if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength1 )// Not get the intersection point, or the point is not right.
			{
				tabEndPnt = tabPoints.GetAt(0) + tabLength1*upVecUnit + 0.33*tabLength1*outVecUnit0;// just assign a point
			}
		}
		tabEndPoints.SetAt(0, tabEndPnt);
if (0)
ShowPoint(pDoc, tabEndPnt, red);
	}
	////////////////
	// Now check whether plane1 intersects with silhouette curves or not.
	// If not, use tabLength0 as the tab length down.
	if ( !tabEndPoints.GetAt(1).IsInitialized() )// Not intersect with any silhouette curves
	{
		IwPoint3d tabEndPntOutside = tabPoints.GetAt(1) + tabLength0*upVecUnit + tabLength0*outVecUnit1;
		IwPoint3d tabEndPnt;
		bool gotIt = IntersectSurfaceByLine(mainSurf, tabEndPntOutside, outVecUnit1, tabEndPnt);
		if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength0 )// Not get the intersection point, or the point is not right.
		{
			if ( positiveSideIsLateral )
				gotIt = IntersectSurfaceByLine(leftSurf, tabEndPntOutside, outVecUnit1, tabEndPnt);
			else
				gotIt = IntersectSurfaceByLine(rightSurf, tabEndPntOutside, outVecUnit1, tabEndPnt);
			if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength0 )// Not get the intersection point, or the point is not right.
			{
				tabEndPnt = tabPoints.GetAt(1) + tabLength0*upVecUnit + 0.33*tabLength0*outVecUnit1;// just assign a point
			}
		}
		tabEndPoints.SetAt(1, tabEndPnt);
if (0)
ShowPoint(pDoc, tabEndPnt, red);
	}

	//////////////////////////////////////////////////////////////////
	// For lateral middle tab
	///////////////////////////
	// Tab Point 2
	double tabLength2 = tabLength;
	IwVector3d outVecUnit2 = tabPoints.GetAt(2) - dropCenterForLateralSide;
	outVecUnit2 = outVecUnit2.ProjectToPlane(mechAxes.GetZAxis());
	outVecUnit2.Unitize();
	// Create a plane
	pnt00 = tabPoints.GetAt(2);
	pnt01 = pnt00 + 3*tabLength*outVecUnit2;
	pnt10 = pnt00 + tabLength*upVecUnit;
	pnt11 = pnt00 + 3*tabLength*outVecUnit2 + tabLength*upVecUnit;
	IwBSplineSurface *biPlane2=NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), pnt00, pnt01, pnt10, pnt11, biPlane2);
if (0)
ShowSurface(pDoc, biPlane2, "biPlane2", blue);
	// intersect silCurves with this plane
	IntersectSurfaceByCurves(biPlane2, silCurves, intPnts, intUVParams, intParams);
	// Determine the one with the lowest femoral z value
	if ( intPnts.GetSize() > 0 )
	{
		zHeightValues.RemoveAll();
		for(unsigned i=0; i<intPnts.GetSize(); i++)
		{
			zHeightValue = (intPnts.GetAt(i) - pnt00).Dot(upVecUnit);
			zHeightValues.Add(IwPoint3d(zHeightValue, i, 0));// Note, 3 elements
		}
		SortIwTArray(zHeightValues);
		// Now the lowest z is in 0th element. Its index is in "y".
		IwPoint3d silPnt = intPnts.GetAt((int)(zHeightValues.GetAt(0).y));
		IwPoint3d tabEndPnt = silPnt + tabPassingDistance*upVecUnit;
		tabEndPoints.SetAt(2, tabEndPnt);
if(0)
ShowPoint(pDoc, tabEndPnt, red);
		// update tabLength2
		tabLength2 = (tabEndPnt - pnt00).Dot(upVecUnit);
	}
	///////////////////////////
	// Tab Point 3
	double tabLength3 = tabLength;
	IwVector3d outVecUnit3 = tabPoints.GetAt(3) - dropCenterForLateralSide;
	outVecUnit3 = outVecUnit3.ProjectToPlane(mechAxes.GetZAxis());
	outVecUnit3.Unitize();
	// Create a plane
	pnt00 = tabPoints.GetAt(3);
	pnt01 = pnt00 + 3*tabLength*outVecUnit3;
	pnt10 = pnt00 + tabLength*upVecUnit;
	pnt11 = pnt00 + 3*tabLength*outVecUnit3 + tabLength*upVecUnit;
	IwBSplineSurface *biPlane3=NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), pnt00, pnt01, pnt10, pnt11, biPlane3);
if (0)
ShowSurface(pDoc, biPlane3, "biPlane3", blue);
	// intersect silCurves with this plane
	IntersectSurfaceByCurves(biPlane3, silCurves, intPnts, intUVParams, intParams);
	// Determine the one with the lowest femoral z value
	if ( intPnts.GetSize() > 0 )
	{
		zHeightValues.RemoveAll();
		for(unsigned i=0; i<intPnts.GetSize(); i++)
		{
			zHeightValue = (intPnts.GetAt(i) - pnt00).Dot(upVecUnit);
			zHeightValues.Add(IwPoint3d(zHeightValue, i, 0));// Note, 3 elements
		}
		SortIwTArray(zHeightValues);
		// Now the lowest z is in 0th element. Its index is in "y".
		IwPoint3d silPnt = intPnts.GetAt((int)(zHeightValues.GetAt(0).y));
		IwPoint3d tabEndPnt = silPnt + tabPassingDistance*upVecUnit;
		tabEndPoints.SetAt(3, tabEndPnt);
if(0)
ShowPoint(pDoc, tabEndPnt, red);
		// update tabLength1
		tabLength3 = (tabEndPnt - pnt00).Dot(upVecUnit);
	}

	// Need to check whether Tab End Point 2 & Tab End Point 3 have similar lengths
	if (tabEndPoints.GetAt(2).IsInitialized() && tabEndPoints.GetAt(3).IsInitialized())// both have been determined
	{
		double bigDeviation = 3.0;
		if ( (tabLength2 - tabLength3) > bigDeviation )// Tab End Point 2 is too deep toward shaft (hip)
		{
			IwPoint3d tabEndPoint2 = tabEndPoints.GetAt(2);
			IwPoint3d tabEndPoint3 = tabEndPoints.GetAt(3);
			tabEndPoints.SetAt(2, tabEndPoint2-0.75*upVecUnit); // move a bit toward ankle
			tabEndPoints.SetAt(3, tabEndPoint3+0.75*upVecUnit); // move a bit toward hip
		}
		else if ( (tabLength3 - tabLength2) > bigDeviation )// Tab End Point 3 is too deep toward shaft (hip)
		{
			IwPoint3d tabEndPoint2 = tabEndPoints.GetAt(2);
			IwPoint3d tabEndPoint3 = tabEndPoints.GetAt(3);
			tabEndPoints.SetAt(2, tabEndPoint2+0.75*upVecUnit); // move a bit toward hip
			tabEndPoints.SetAt(3, tabEndPoint3-0.75*upVecUnit); // move a bit toward ankle
		}
	}

	////////////////
	// Now check whether plane2 intersects with silhouette curves or not.
	// If not, use tabLength3 as the tab length down.
	if ( !tabEndPoints.GetAt(2).IsInitialized() )// Not intersect with any silhouette curves
	{
		IwPoint3d tabEndPntOutside = tabPoints.GetAt(2) + tabLength3*upVecUnit + tabLength3*outVecUnit2;
		IwPoint3d tabEndPnt;
		bool gotIt = IntersectSurfaceByLine(mainSurf, tabEndPntOutside, outVecUnit2, tabEndPnt);
		if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength3 )// Not get the intersection point, or the point is not right.
		{
			if ( positiveSideIsLateral )
				gotIt = IntersectSurfaceByLine(rightSurf, tabEndPntOutside, outVecUnit2, tabEndPnt);
			else
				gotIt = IntersectSurfaceByLine(leftSurf, tabEndPntOutside, outVecUnit2, tabEndPnt);
			if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength3 )// Not get the intersection point, or the point is not right.
			{
				tabEndPnt = tabPoints.GetAt(2) + tabLength3*upVecUnit + 0.33*tabLength3*outVecUnit2;// just assign a point
			}
		}
		tabEndPoints.SetAt(2, tabEndPnt);
if (0)
ShowPoint(pDoc, tabEndPnt, red);
	}
	////////////////
	// Now check whether plane3 intersects with silhouette curves or not.
	// If not, use tabLength2 as the tab length down.
	if ( !tabEndPoints.GetAt(3).IsInitialized() )// Not intersect with any silhouette curves
	{
		IwPoint3d tabEndPntOutside = tabPoints.GetAt(3) + tabLength2*upVecUnit + tabLength2*outVecUnit3;
		IwPoint3d tabEndPnt;
		bool gotIt = IntersectSurfaceByLine(mainSurf, tabEndPntOutside, outVecUnit3, tabEndPnt);
		if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength2 )// Not get the intersection point, or the point is not right.
		{
			if ( positiveSideIsLateral )
				gotIt = IntersectSurfaceByLine(rightSurf, tabEndPntOutside, outVecUnit3, tabEndPnt);
			else
				gotIt = IntersectSurfaceByLine(leftSurf, tabEndPntOutside, outVecUnit3, tabEndPnt);
			if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength2 )// Not get the intersection point, or the point is not right.
			{
				tabEndPnt = tabPoints.GetAt(3) + tabLength2*upVecUnit + 0.33*tabLength2*outVecUnit3;// just assign a point
			}
		}
		tabEndPoints.SetAt(3, tabEndPnt);
if (0)
ShowPoint(pDoc, tabEndPnt, red);
	}
	// Additional check whether tabEndPoints are passing the distal cut plane with tabPassingDistance
	if ( pDoc->GetVarTableValue("JIGS TAB LENGTH REFER TO DISTAL CUTS") == 1 )
	{
		IwVector3d lateralDistalCutPointOffset = lateralDistalCutPoint + tabPassingDistance*mechAxes.GetZAxis();
		// for point 2
		IwPoint3d tabEndPnt = tabEndPoints.GetAt(2);
		IwVector3d tempVector = tabEndPnt - lateralDistalCutPointOffset;
		double distToMove = tempVector.Dot(upVecUnit);
		if ( distToMove < 0 ) // tabEndPnt is in the negative z side of lateralDistalCutPointOffset
		{
			tabEndPnt = tabEndPnt - distToMove*upVecUnit;
			tabEndPoints.SetAt(2, tabEndPnt);
		}
		// for point 3
		tabEndPnt = tabEndPoints.GetAt(3);
		tempVector = tabEndPnt - lateralDistalCutPointOffset;
		distToMove = tempVector.Dot(upVecUnit);
		if ( tempVector.Dot(upVecUnit) < 0 )// tabEndPnt is in the negative z side of lateralDistalCutPointOffset
		{
			tabEndPnt = tabEndPnt - distToMove*upVecUnit;
			tabEndPoints.SetAt(3, tabEndPnt);
		}
	}

	//////////////////////////////////////////////////////////////////
	// For medial peg tab
	///////////////////////////
	// Tab Point 4
	double tabLength4 = tabLength;
	IwVector3d outVecUnit4 = tabPoints.GetAt(4) - dropCenterForMedialSide;
	outVecUnit4 = outVecUnit4.ProjectToPlane(mechAxes.GetZAxis());
	outVecUnit4.Unitize();
	// Create a plane
	pnt00 = tabPoints.GetAt(4);
	pnt01 = pnt00 + 3*tabLength*outVecUnit4;
	pnt10 = pnt00 + tabLength*upVecUnit;
	pnt11 = pnt00 + 3*tabLength*outVecUnit4 + tabLength*upVecUnit;
	IwBSplineSurface *biPlane4=NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), pnt00, pnt01, pnt10, pnt11, biPlane4);
if (0)
ShowSurface(pDoc, biPlane4, "biPlane4", blue);
	// intersect silCurves with this plane
	IntersectSurfaceByCurves(biPlane4, silCurves, intPnts, intUVParams, intParams);
	// Determine the one with the lowest femoral z value
	if ( intPnts.GetSize() > 0 )
	{
		zHeightValues.RemoveAll();
		for(unsigned i=0; i<intPnts.GetSize(); i++)
		{
			zHeightValue = (intPnts.GetAt(i) - pnt00).Dot(upVecUnit);
			zHeightValues.Add(IwPoint3d(zHeightValue, i, 0));// Note, 3 elements
		}
		SortIwTArray(zHeightValues);
		// Now the lowest z is in 0th element. Its index is in "y".
		IwPoint3d silPnt = intPnts.GetAt((int)(zHeightValues.GetAt(0).y));
		IwPoint3d tabEndPnt = silPnt + tabPassingDistance*upVecUnit;
		tabEndPoints.SetAt(4, tabEndPnt);
if(0)
ShowPoint(pDoc, tabEndPnt, red);
		// update tabLength4
		tabLength4 = (tabEndPnt - pnt00).Dot(upVecUnit);
	}
	///////////////////////////
	// Tab Point 5
	double tabLength5 = tabLength;
	IwVector3d outVecUnit5 = tabPoints.GetAt(5) - dropCenterForMedialSide;
	outVecUnit5 = outVecUnit5.ProjectToPlane(mechAxes.GetZAxis());
	outVecUnit5.Unitize();
	// Create a plane
	pnt00 = tabPoints.GetAt(5);
	pnt01 = pnt00 + 3*tabLength*outVecUnit5;
	pnt10 = pnt00 + tabLength*upVecUnit;
	pnt11 = pnt00 + 3*tabLength*outVecUnit5 + tabLength*upVecUnit;
	IwBSplineSurface *biPlane5=NULL;
	IwBSplineSurface::CreateBilinearSurface(pDoc->GetIwContext(), pnt00, pnt01, pnt10, pnt11, biPlane5);
if (0)
ShowSurface(pDoc, biPlane5, "biPlane5", blue);
	// intersect silCurves with this plane
	IntersectSurfaceByCurves(biPlane5, silCurves, intPnts, intUVParams, intParams);
	// Determine the one with the lowest femoral z value
	if ( intPnts.GetSize() > 0 )
	{
		zHeightValues.RemoveAll();
		for(unsigned i=0; i<intPnts.GetSize(); i++)
		{
			zHeightValue = (intPnts.GetAt(i) - pnt00).Dot(upVecUnit);
			zHeightValues.Add(IwPoint3d(zHeightValue, i, 0));// Note, 3 elements
		}
		SortIwTArray(zHeightValues);
		// Now the lowest z is in 0th element. Its index is in "y".
		IwPoint3d silPnt = intPnts.GetAt((int)(zHeightValues.GetAt(0).y));
		IwPoint3d tabEndPnt = silPnt + tabPassingDistance*upVecUnit;
		tabEndPoints.SetAt(5, tabEndPnt);
if(0)
ShowPoint(pDoc, tabEndPnt, red);
		// update tabLength1
		tabLength5 = (tabEndPnt - pnt00).Dot(upVecUnit);
	}

	// Need to check whether Tab End Point 4 & Tab End Point 5 have similar lengths
	if (tabEndPoints.GetAt(4).IsInitialized() && tabEndPoints.GetAt(5).IsInitialized())// both have been determined
	{
		double bigDeviation = 3.0;
		if ( (tabLength4 - tabLength5) > bigDeviation )// Tab End Point 4 is too deep toward shaft (hip)
		{
			IwPoint3d tabEndPoint4 = tabEndPoints.GetAt(4);
			IwPoint3d tabEndPoint5 = tabEndPoints.GetAt(5);
			tabEndPoints.SetAt(4, tabEndPoint4-0.75*upVecUnit); // move a bit toward ankle
			tabEndPoints.SetAt(5, tabEndPoint5+0.75*upVecUnit); // move a bit toward hip
		}
		else if ( (tabLength5 - tabLength4) > bigDeviation )// Tab End Point 5 is too deep toward shaft (hip)
		{
			IwPoint3d tabEndPoint4 = tabEndPoints.GetAt(4);
			IwPoint3d tabEndPoint5 = tabEndPoints.GetAt(5);
			tabEndPoints.SetAt(4, tabEndPoint4+0.75*upVecUnit); // move a bit toward hip
			tabEndPoints.SetAt(5, tabEndPoint5-0.75*upVecUnit); // move a bit toward ankle
		}
	}

	////////////////
	// Now check whether plane4 intersects with silhouette curves or not.
	// If not, use tabLength5 as the tab length down.
	if ( !tabEndPoints.GetAt(4).IsInitialized() )// Not intersect with any silhouette curves
	{
		IwPoint3d tabEndPntOutside = tabPoints.GetAt(4) + tabLength5*upVecUnit + tabLength5*outVecUnit4;
		IwPoint3d tabEndPnt;
		bool gotIt = IntersectSurfaceByLine(mainSurf, tabEndPntOutside, outVecUnit4, tabEndPnt);
		if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength5 )// Not get the intersection point, or the point is not right.
		{
			if ( positiveSideIsLateral )
				gotIt = IntersectSurfaceByLine(leftSurf, tabEndPntOutside, outVecUnit4, tabEndPnt);
			else
				gotIt = IntersectSurfaceByLine(rightSurf, tabEndPntOutside, outVecUnit4, tabEndPnt);
			if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength5 )// Not get the intersection point, or the point is not right.
			{
				tabEndPnt = tabPoints.GetAt(4) + tabLength5*upVecUnit + 0.33*tabLength5*outVecUnit4;// just assign a point
			}
		}
		tabEndPoints.SetAt(4, tabEndPnt);
if (0)
ShowPoint(pDoc, tabEndPnt, red);
	}
	////////////////
	// Now check whether plane5 intersects with silhouette curves or not.
	// If not, use tabLength4 as the tab length down.
	if ( !tabEndPoints.GetAt(5).IsInitialized() )// Not intersect with any silhouette curves
	{
		IwPoint3d tabEndPntOutside = tabPoints.GetAt(5) + tabLength4*upVecUnit + tabLength4*outVecUnit5;
		IwPoint3d tabEndPnt;
		bool gotIt = IntersectSurfaceByLine(mainSurf, tabEndPntOutside, outVecUnit5, tabEndPnt);
		if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength4 )// Not get the intersection point, or the point is not right.
		{
			if ( positiveSideIsLateral )
				gotIt = IntersectSurfaceByLine(leftSurf, tabEndPntOutside, outVecUnit5, tabEndPnt);
			else
				gotIt = IntersectSurfaceByLine(rightSurf, tabEndPntOutside, outVecUnit5, tabEndPnt);
			if ( !gotIt || tabEndPntOutside.DistanceBetween(tabEndPnt) > tabLength4 )// Not get the intersection point, or the point is not right.
			{
				tabEndPnt = tabPoints.GetAt(5) + tabLength4*upVecUnit + 0.33*tabLength4*outVecUnit5;// just assign a point
			}
		}
		tabEndPoints.SetAt(5, tabEndPnt);
if (0)
ShowPoint(pDoc, tabEndPnt, red);
	}
	// Additional check whether tabEndPoints are passing the distal cut plane with tabPassingDistance
	if ( pDoc->GetVarTableValue("JIGS TAB LENGTH REFER TO DISTAL CUTS") == 1 )
	{
		IwVector3d medialDistalCutPointOffset = medialDistalCutPoint + tabPassingDistance*mechAxes.GetZAxis();
		// for point 4
		IwPoint3d tabEndPnt = tabEndPoints.GetAt(4);
		IwVector3d tempVector = tabEndPnt - medialDistalCutPointOffset;
		double distToMove = tempVector.Dot(upVecUnit);
		if ( distToMove < 0 ) // tabEndPnt is in the negative z side of medialDistalCutPointOffset
		{
			tabEndPnt = tabEndPnt - distToMove*upVecUnit;
			tabEndPoints.SetAt(4, tabEndPnt);
		}
		// for point 5
		tabEndPnt = tabEndPoints.GetAt(5);
		tempVector = tabEndPnt - medialDistalCutPointOffset;
		distToMove = tempVector.Dot(upVecUnit);
		if ( distToMove < 0 ) // tabEndPnt is in the negative z side of medialDistalCutPointOffset
		{
			tabEndPnt = tabEndPnt - distToMove*upVecUnit;
			tabEndPoints.SetAt(5, tabEndPnt);
		}
	}

	// Finally, project all tabEndPoints onto inner surface jigs
	if ( innerBSurface != NULL )
	{
		IwPoint3d pnt, cPnt;
		IwPoint2d param;
		for (unsigned i=0; i<tabEndPoints.GetSize(); i++)
		{
			pnt = tabEndPoints.GetAt(i);
			DistFromPointToSurface(pnt, innerBSurface, cPnt, param); 
			tabEndPoints.SetAt(i, cPnt);
		}
	}

	if ( biPlane0 )
		IwObjDelete(biPlane0);
	if ( biPlane1 )
		IwObjDelete(biPlane1);
	if ( biPlane2 )
		IwObjDelete(biPlane2);
	if ( biPlane3 )
		IwObjDelete(biPlane3);
	if ( biPlane4 )
		IwObjDelete(biPlane4);
	if ( biPlane5 )
		IwObjDelete(biPlane5);
	

	return tabEndPoints;
}

////////////////////////////////////////////////////////////////////////
// This function uses the existing window cut angles to refine
// the needed window cut angles.
////////////////////////////////////////////////////////////////////////
void CDefineOutlineProfileJigs::RefineWindowCutAngles()
{
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;
	// Get projectedOutlineProfilePoints
	IwTArray<IwPoint3d> projectedOutlineProfilePoints;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedOutlineProfilePoints);
	if ( projectedOutlineProfilePoints.GetSize() == 0 )
		return;

	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	// Determine tab point indexes m_currTabPoints
	if ( m_currTabPoints.GetSize() == 0 )
		return;

	IwPoint3d cPnt;
	int tabIndex0, tabIndex1, tabIndex2, tabIndex3, tabIndex4, tabIndex5;
	DistFromPointToIwTArray(m_currTabPoints.GetAt(0), projectedOutlineProfilePoints, NULL, cPnt, &tabIndex0);
	DistFromPointToIwTArray(m_currTabPoints.GetAt(1), projectedOutlineProfilePoints, NULL, cPnt, &tabIndex1);
	if ( tabIndex0 > tabIndex1 )
		swap(tabIndex0, tabIndex1);
	DistFromPointToIwTArray(m_currTabPoints.GetAt(2), projectedOutlineProfilePoints, NULL, cPnt, &tabIndex2);
	DistFromPointToIwTArray(m_currTabPoints.GetAt(3), projectedOutlineProfilePoints, NULL, cPnt, &tabIndex3);
	if ( tabIndex2 > tabIndex3 )
		swap(tabIndex2, tabIndex3);
	DistFromPointToIwTArray(m_currTabPoints.GetAt(4), projectedOutlineProfilePoints, NULL, cPnt, &tabIndex4);
	DistFromPointToIwTArray(m_currTabPoints.GetAt(5), projectedOutlineProfilePoints, NULL, cPnt, &tabIndex5);
	if ( tabIndex4 > tabIndex5 )
		swap(tabIndex4, tabIndex5);

	// Use the existing ones to estimate the needed ones
	pOutlineProfileJigs->SetWindowCutAngles(m_currWindowCutAngles);
	IwTArray<double> windowCutAngles;
	IwPoint3d posCenterPoint, negCenterPoint;
	IwTArray<IwPoint3d> windowCutTips;
	pOutlineProfileJigs->GetWindowCutAngles(windowCutAngles, &windowCutTips, &posCenterPoint, &negCenterPoint);
	// Determine the positive side arc center (the arc with radius=2.0mm)
	IwPoint3d posWindowCutTip = windowCutTips.GetAt(0);
	posWindowCutTip = posWindowCutTip.ProjectPointToPlane( posCenterPoint, wslAxes.GetZAxis());
	IwVector3d vecUp = posWindowCutTip - posCenterPoint;
	vecUp.Unitize();
	IwAxis2Placement posFrame = IwAxis2Placement(posCenterPoint, vecUp, wslAxes.GetZAxis());
	IwVector3d vecOut = posFrame.GetZAxis();
	IwVector3d posSmallArcCenter = posCenterPoint + 8.691*vecUp + 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
	// search for the closest point
	double distToMove=0;
	IwPoint3d minPnt;
	int index;
	double dist = DistFromPointToIwTArray(posSmallArcCenter, projectedOutlineProfilePoints, &wslAxes.GetZAxis(), minPnt, &index);
	double posWindowCutAngle = m_currWindowCutAngles.GetAt(0);
	// The closest point is within a tab
	if ( (index >= (tabIndex0+1) && index <= (tabIndex1-1)) ||
		 (index >= (tabIndex2+1) && index <= (tabIndex3-1)) || 
		 (index >= (tabIndex4+1) && index <= (tabIndex5-1)) )
	{ 
		// The expected distance is "2.0+1.0". 
		if ( dist < (2.0 + 1.0) )
		{
			distToMove = dist - (2.0 + 1.0);		
			posWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}	
	}
	else
	{
		// The expected distance is "2.0+3.0". "0.25" as allowance
		if ( dist < (2.0 + 3.0 + 0.25) )
		{
			distToMove = dist - (2.0 + 3.0 + 0.25);
			posWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	m_currWindowCutAngles.SetAt(0, posWindowCutAngle);

	// Determine the negative side arc center (the arc with radius=2.0mm)
	IwPoint3d negWindowCutTip = windowCutTips.GetAt(1);
	negWindowCutTip = negWindowCutTip.ProjectPointToPlane( negCenterPoint, wslAxes.GetZAxis());
	vecUp = negWindowCutTip - negCenterPoint;
	vecUp.Unitize();
	IwAxis2Placement negFrame = IwAxis2Placement(negCenterPoint, vecUp, -wslAxes.GetZAxis());
	vecOut = negFrame.GetZAxis();
	IwVector3d negSmallArcCenter = negCenterPoint + 8.691*vecUp + 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
	// search for the closest point
	dist = DistFromPointToIwTArray(negSmallArcCenter, projectedOutlineProfilePoints, &wslAxes.GetZAxis(), minPnt, &index);
	distToMove=0;
	double negWindowCutAngle = m_currWindowCutAngles.GetAt(1);
	// The closest point is within a tab
	if ( (index >= (tabIndex0+1) && index <= (tabIndex1-1)) ||
		 (index >= (tabIndex2+1) && index <= (tabIndex3-1)) || 
		 (index >= (tabIndex4+1) && index <= (tabIndex5-1)) )
	{ 
		// The expected distance is "2.0+1.0". 
		if ( dist < (2.0 + 1.0) )
		{
			distToMove = dist - (2.0 + 1.0);		
			negWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	else
	{
		// The expected distance is "2.0+3.0". "0.25" as allowance
		if ( dist < (2.0 + 3.0 + 0.25) )
		{
			distToMove = dist - (2.0 + 3.0 + 0.25);
			negWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	m_currWindowCutAngles.SetAt(1, negWindowCutAngle);

	//////////////////////////////////////////////////////////////////////////////////////////
	// Run 2nd time to get a better estimated result
	pOutlineProfileJigs->SetWindowCutAngles(m_currWindowCutAngles);
	pOutlineProfileJigs->GetWindowCutAngles(windowCutAngles, &windowCutTips,  &posCenterPoint, &negCenterPoint);// to update tips
	// Determine the positive side arc center (the arc with radius=2.0mm)
	posWindowCutTip = windowCutTips.GetAt(0);
	posWindowCutTip = posWindowCutTip.ProjectPointToPlane( posCenterPoint, wslAxes.GetZAxis());
	vecUp = posWindowCutTip - posCenterPoint;
	vecUp.Unitize();
	posFrame = IwAxis2Placement(posCenterPoint, vecUp, wslAxes.GetZAxis());
	vecOut = posFrame.GetZAxis();
	posSmallArcCenter = posCenterPoint + 8.691*vecUp + 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
	// search for the closest point
	dist = DistFromPointToIwTArray(posSmallArcCenter, projectedOutlineProfilePoints, &wslAxes.GetZAxis(), minPnt, &index);
	distToMove=0;
	posWindowCutAngle = m_currWindowCutAngles.GetAt(0);
	// The closest point is within a tab
	if ( (index >= (tabIndex0+1) && index <= (tabIndex1-1)) ||
		 (index >= (tabIndex2+1) && index <= (tabIndex3-1)) || 
		 (index >= (tabIndex4+1) && index <= (tabIndex5-1)) )
	{ 
		// The expected distance is "2.0+1.0". 
		if ( dist < (2.0 + 1.0) )
		{
			distToMove = dist - (2.0 + 1.0);
			posWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	else
	{
		// The expected distance is "2.0+3.0". "0.25" as allowance
		if ( dist < (2.0 + 3.0 + 0.25) )
		{
			distToMove = dist - (2.0 + 3.0 + 0.25);
			posWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	m_currWindowCutAngles.SetAt(0, posWindowCutAngle);

	// Determine the negative side arc center (the arc with radius=2.0mm)
	negWindowCutTip = windowCutTips.GetAt(1);
	negWindowCutTip = negWindowCutTip.ProjectPointToPlane( negCenterPoint, wslAxes.GetZAxis());
	vecUp = negWindowCutTip - negCenterPoint;
	vecUp.Unitize();
	negFrame = IwAxis2Placement(negCenterPoint, vecUp, -wslAxes.GetZAxis());
	vecOut = negFrame.GetZAxis();
	negSmallArcCenter = negCenterPoint + 8.691*vecUp + 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
	// search for the closest point
	dist = DistFromPointToIwTArray(negSmallArcCenter, projectedOutlineProfilePoints, &wslAxes.GetZAxis(), minPnt, &index);
	distToMove=0;
	negWindowCutAngle = m_currWindowCutAngles.GetAt(1);
	// The closest point is within a tab
	if ( (index >= (tabIndex0+1) && index <= (tabIndex1-1)) ||
		 (index >= (tabIndex2+1) && index <= (tabIndex3-1)) || 
		 (index >= (tabIndex4+1) && index <= (tabIndex5-1)) )
	{ 
		// The expected distance is "2.0+1.0".
		if ( dist < (2.0 + 1.0) )
		{
			distToMove = dist - (2.0 + 1.0);
			negWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	else
	{
		// The expected distance is "2.0+3.0". "0.25" as allowance
		if ( dist < (2.0 + 3.0 + 0.25) )
		{
			distToMove = dist - (2.0 + 3.0 + 0.25);
			negWindowCutAngle -= distToMove/9.25*(180.0/IW_PI); // The windowe cut shape is fixed. "9.25" is the fixed distance from center point to small arc center.
		}
	}
	m_currWindowCutAngles.SetAt(1, negWindowCutAngle);

	// To be continued [2] [3]

	return;
}

bool CDefineOutlineProfileJigs::DetermineWindowCutDistancesToProfile
(
	CTotalDoc* pDoc,								// I:
	IwTArray<double>& windowCutDistancesToProfile	// O:
)
{
	windowCutDistancesToProfile.RemoveAll();

	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return false;
	// Get projectedOutlineProfilePoints
	IwTArray<IwPoint3d> projectedOutlineProfilePoints;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedOutlineProfilePoints);
	if ( projectedOutlineProfilePoints.GetSize() == 0 )
		return false;
	// Get tab points
	IwTArray<IwPoint3d> tabPoints;
	pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
	// Get Axis
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();

	bool positiveSideIsLateral = pDoc->IsPositiveSideLateral();

	// We only determine the distance to the open profile, rather than the profile enclosed by tabs
	// Determine the tab indexes
	IwPoint3d minPnt0, minPnt1;
	int index;
	IwTArray<unsigned> tabIndexes;
	for (unsigned i=0; i<tabPoints.GetSize(); i++)
	{
		DistFromPointToIwTArray(tabPoints.GetAt(i), projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt0, &index);
		tabIndexes.Add(index);
	}
	unsigned tabIndex0 = tabIndexes.GetAt(0) < tabIndexes.GetAt(1) ? tabIndexes.GetAt(0) : tabIndexes.GetAt(1);
	unsigned tabIndex1 = tabIndexes.GetAt(0) < tabIndexes.GetAt(1) ? tabIndexes.GetAt(1) : tabIndexes.GetAt(0);
	unsigned tabIndex2 = tabIndexes.GetAt(2) < tabIndexes.GetAt(3) ? tabIndexes.GetAt(2) : tabIndexes.GetAt(3);
	unsigned tabIndex3 = tabIndexes.GetAt(2) < tabIndexes.GetAt(3) ? tabIndexes.GetAt(3) : tabIndexes.GetAt(2);
	unsigned tabIndex4 = tabIndexes.GetAt(4) < tabIndexes.GetAt(5) ? tabIndexes.GetAt(4) : tabIndexes.GetAt(5);
	unsigned tabIndex5 = tabIndexes.GetAt(4) < tabIndexes.GetAt(5) ? tabIndexes.GetAt(5) : tabIndexes.GetAt(4);
	// void the projectedOutlineProfilePoints in between of the tabs by moving them
	// to far away such that they will not be the min distance.
	IwPoint3d farPoint = IwPoint3d(1000000, 1000000, 1000000);
	for (unsigned i=0; i<projectedOutlineProfilePoints.GetSize(); i++) 
	{
		if ( i > (tabIndex0-1) && i < (tabIndex1+1) )// between 1st tab
		{
			projectedOutlineProfilePoints.SetAt(i, farPoint); // these points become very far away.
		}
		else if ( i > (tabIndex2-1) && i < (tabIndex3+1) )// between 2nd tab
		{
			projectedOutlineProfilePoints.SetAt(i, farPoint); // these points become very far away.
		}
		else if ( i > (tabIndex4-1) && i < (tabIndex5+1) )// between 3rd tab
		{
			projectedOutlineProfilePoints.SetAt(i, farPoint); // these points become very far away.
		}
	}

	// Get window cut info
	IwTArray<double> windowCutAngles;
	IwTArray<IwPoint3d> windowCutTips;
	IwPoint3d posCenterPoint, negCenterPoint;
	pOutlineProfileJigs->GetWindowCutAngles(windowCutAngles, &windowCutTips, &posCenterPoint, &negCenterPoint);

	IwVector3d smallArcCenter0, smallArcCenter1;
	IwPoint3d midPnt;
	double minDist, dist0, dist1;
	// Determine the positive side arc center (the arc with radius=2.0mm)
	{
		IwPoint3d posWindowCutTip = windowCutTips.GetAt(0);
		posWindowCutTip = posWindowCutTip.ProjectPointToPlane( posCenterPoint, sweepAxes.GetZAxis());
		IwVector3d vecUp = posWindowCutTip - posCenterPoint;
		vecUp.Unitize();
		IwAxis2Placement posFrame = IwAxis2Placement(posCenterPoint, vecUp, sweepAxes.GetZAxis());
		IwVector3d vecOut = posFrame.GetZAxis();
		// one side smallArcCenter
		smallArcCenter0 = posCenterPoint + 8.691*vecUp + 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist0 = DistFromPointToIwTArray(smallArcCenter0, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt0, &index);
		// the other side smallArcCenter
		smallArcCenter1 = posCenterPoint + 8.691*vecUp - 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist1 = DistFromPointToIwTArray(smallArcCenter1, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt1, &index);
if (0)
{
ShowPoint(pDoc, smallArcCenter0, red);
ShowPoint(pDoc, minPnt0, magenta);
ShowPoint(pDoc, smallArcCenter1, blue);
ShowPoint(pDoc, minPnt1, cyan);
}
		// Min distance
		minDist = min(dist0, dist1);
		minDist -= 2.0;// The window cut profile should be 2.0mm closer.
		windowCutDistancesToProfile.Add(minDist);
	}
	// Determine the negative side arc center (the arc with radius=2.0mm)
	{
		IwPoint3d negWindowCutTip = windowCutTips.GetAt(1);
		negWindowCutTip = negWindowCutTip.ProjectPointToPlane( negCenterPoint, sweepAxes.GetZAxis());
		IwVector3d vecUp = negWindowCutTip - negCenterPoint;
		vecUp.Unitize();
		IwAxis2Placement negFrame = IwAxis2Placement(negCenterPoint, vecUp, -sweepAxes.GetZAxis());
		IwVector3d vecOut = negFrame.GetZAxis();
		// one side smallArcCenter
		smallArcCenter0 = negCenterPoint + 8.691*vecUp + 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist0 = DistFromPointToIwTArray(smallArcCenter0, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt0, &index);
		// the other side smallArcCenter
		smallArcCenter1 = negCenterPoint + 8.691*vecUp - 3.166*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist1 = DistFromPointToIwTArray(smallArcCenter1, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt1, &index);
if (0)
{
ShowPoint(pDoc, smallArcCenter0, red);
ShowPoint(pDoc, minPnt0, magenta);
ShowPoint(pDoc, smallArcCenter1, blue);
ShowPoint(pDoc, minPnt1, cyan);
}
		// Min distance
		minDist = min(dist0, dist1);
		minDist -= 2.0;// The window cut profile should be 2.0mm closer.
		windowCutDistancesToProfile.Add(minDist);
	}

	// window cuts for F2 [2] [3]
	// Determine the positive side F2 window cut 
	if ( positiveSideIsLateral )
	{
		// lateral F2 window cut
		// Note - here we use an arc with radius=2.5mm to approximate the 2 lateral fillets around the corner.
		IwPoint3d posWindowCutTip = windowCutTips.GetAt(2);
		posWindowCutTip = posWindowCutTip.ProjectPointToPlane( posCenterPoint, sweepAxes.GetZAxis());
		IwVector3d vecUp = posWindowCutTip - posCenterPoint;
		vecUp.Unitize();
		IwAxis2Placement posFrame = IwAxis2Placement(posCenterPoint, vecUp, sweepAxes.GetZAxis());
		IwVector3d vecOut = posFrame.GetZAxis();
		// one side smallArcCenter
		smallArcCenter0 = posCenterPoint + 6.203*vecUp + 1.942*vecOut; // The F2 windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist0 = DistFromPointToIwTArray(smallArcCenter0, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt0, &index);
		// the other side smallArcCenter
		smallArcCenter1 = posCenterPoint + 6.203*vecUp - 1.942*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist1 = DistFromPointToIwTArray(smallArcCenter1, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt1, &index);
if (0)
{
ShowPoint(pDoc, smallArcCenter0, red);
ShowPoint(pDoc, minPnt0, magenta);
ShowPoint(pDoc, smallArcCenter1, blue);
ShowPoint(pDoc, minPnt1, cyan);
}
		// Min distance
		minDist = min(dist0, dist1);
		minDist -= 2.866;// The true F2 window cut profile should be 2.866mm closer (worst case).
		windowCutDistancesToProfile.Add(minDist);
	}
	else
	{
		// medial F2 window cut -> do not need to calculate the min distance
		windowCutDistancesToProfile.Add(-1);
	}

	// Determine the negative side F2 window cut 
	if ( !positiveSideIsLateral )
	{
		// lateral F2 window cut
		// Note - here we use an arc with radius=2.5mm to approximate the 2 lateral fillets around the corner.
		IwPoint3d negWindowCutTip = windowCutTips.GetAt(3);
		negWindowCutTip = negWindowCutTip.ProjectPointToPlane( negCenterPoint, sweepAxes.GetZAxis());
		IwVector3d vecUp = negWindowCutTip - negCenterPoint;
		vecUp.Unitize();
		IwAxis2Placement negFrame = IwAxis2Placement(negCenterPoint, vecUp, sweepAxes.GetZAxis());
		IwVector3d vecOut = negFrame.GetZAxis();
		// one side smallArcCenter
		smallArcCenter0 = negCenterPoint + 6.203*vecUp + 1.942*vecOut; // The F2 windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist0 = DistFromPointToIwTArray(smallArcCenter0, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt0, &index);
		// the other side smallArcCenter
		smallArcCenter1 = negCenterPoint + 6.203*vecUp - 1.942*vecOut; // The windowe cut shape is fixed. "8.691" and "3.166" are the related position from center point to the small arc center.
		//Distance from smallArcCenter to profile
		dist1 = DistFromPointToIwTArray(smallArcCenter1, projectedOutlineProfilePoints, &sweepAxes.GetZAxis(), minPnt1, &index);
if (0)
{
ShowPoint(pDoc, smallArcCenter0, red);
ShowPoint(pDoc, minPnt0, magenta);
ShowPoint(pDoc, smallArcCenter1, blue);
ShowPoint(pDoc, minPnt1, cyan);
}
		// Min distance
		minDist = min(dist0, dist1);
		minDist -= 2.866;// The true F2 window cut profile should be 2.866mm closer (worst case).
		windowCutDistancesToProfile.Add(minDist);
	}
	else
	{
		// medial F2 window cut -> do not need to calculate the min distance
		windowCutDistancesToProfile.Add(-1);
	}

	return true;
}

void CDefineOutlineProfileJigs::OnValidate()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_validateStatus = Validate(m_pDoc, m_validateMessage);
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE_JIGS, m_validateStatus, m_validateMessage);
	m_pDoc->GetOutlineProfileJigs()->SetValidateStatus(m_validateStatus, m_validateMessage);
}

///////////////////////////////////////////////////////////////////////
// This function validates users settings.
///////////////////////////////////////////////////////////////////////
CEntValidateStatus CDefineOutlineProfileJigs::Validate
(
	CTotalDoc* pDoc,						// I:
	QString &message						// O: error messages
)
{
	pDoc->AppendLog( QString("CDefineOutlineProfileJigs::Validate()") );

	pDoc->AppendValidationReportJigsEntry("\n");
	pDoc->AppendValidationReportJigsEntry("**** Outline Profile Jig ****");

	// initialize data
	message.clear();
	int vStatus = 0; 

	// Tab length
	if ( pDoc->GetValidateTableStatus("OUTLINE PROFILE JIGS TAB WIDTH STATUS") != -1 )
	{
		// Check tab width here.
		double minTabWidth = pDoc->GetVarTableValue("JIGS TAB MIN WIDTH");
		double maxTabWidth = pDoc->GetVarTableValue("JIGS TAB MAX WIDTH");
		IwTArray<double> tabWidths;
		bool reliable = ValidateTabWidth(pDoc, tabWidths);
		double antTab = floor(100*tabWidths.GetAt(0)+0.5)/100.0;
		double midTab = floor(100*tabWidths.GetAt(1)+0.5)/100.0;
		double pegTab = floor(100*tabWidths.GetAt(2)+0.5)/100.0;

		if ( reliable )
		{
			if ( antTab < minTabWidth || antTab > maxTabWidth ||
				 midTab < minTabWidth || midTab > maxTabWidth ||
				 pegTab < minTabWidth || pegTab > maxTabWidth )
			{
				pDoc->AppendValidateMessage( QString("OUTLINE PROFILE JIGS TAB WIDTH"), vStatus, message );
				pDoc->AppendValidationReportJigsEntry( QString("Fail: Outline profile jig tab widths (%1mm ~ %2mm) are not valid.").arg(minTabWidth).arg(maxTabWidth), false );
			}
			else
			{
				pDoc->AppendValidationReportJigsEntry( QString("Pass: Outline profile jig tab widths (%1mm ~ %2mm) are valid.").arg(minTabWidth).arg(maxTabWidth), true );
			}
			pDoc->AppendValidationReportJigsEntry( QString("    Medial Anterior tab width: %1mm").arg(antTab, 0, 'f', 2) );
			pDoc->AppendValidationReportJigsEntry( QString("    Medial peg tab width: %1mm").arg(pegTab, 0, 'f', 2) );
			pDoc->AppendValidationReportJigsEntry( QString("    Lateral middle tab width: %1mm").arg(midTab, 0, 'f', 2) );
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for outline profile tab width.");		
			vStatus = max(vStatus, 2);
			pDoc->AppendValidationReportJigsEntry( "Fail: Software validation is not reliable. Please proceed manual checking for outline profile tab width.", false );
		}
	}
	// Outline profile location
	if ( pDoc->GetValidateTableStatus("OUTLINE PROFILE JIGS PROFILE LOCATION STATUS") != -1 )
	{
		double maxDeviation = pDoc->GetVarTableValue("JIGS PROFILE LOCATION MAX DEVIATION");
		IwTArray<IwPoint3d> refPoints;
		IwTArray<double> distances;
		bool reliable = ValidateProfileLocation(pDoc, refPoints, distances);

		if ( reliable )
		{
			bool outOfRange = false;
			for (unsigned i=0; i<distances.GetSize(); i++)
			{
				if ( distances.GetAt(i) > maxDeviation )
				{
					outOfRange = true;
					ShowPoint(pDoc, refPoints.GetAt(i), red);
				}
			}
			if ( outOfRange )
			{
				pDoc->AppendValidateMessage( QString("OUTLINE PROFILE JIGS PROFILE LOCATION"), vStatus, message );
			}
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for outline profile location.");		
			vStatus = max(vStatus, 2);
		}
	}
	// Window cut distance to profile
	if ( pDoc->GetValidateTableStatus("OUTLINE PROFILE JIGS WINDOW CUT DISTANCE STATUS") != -1 )
	{
		IwTArray<double> windowCutDistancesToProfile;
		DetermineWindowCutDistancesToProfile(pDoc, windowCutDistancesToProfile);
		double minWindowCutDistance = 100.0;
		for ( unsigned i=0; i<windowCutDistancesToProfile.GetSize(); i++)
		{
			if ( windowCutDistancesToProfile.GetAt(i) > 0 && // "-1" = do not care
				 windowCutDistancesToProfile.GetAt(i) < minWindowCutDistance )
			{
				minWindowCutDistance = windowCutDistancesToProfile.GetAt(i);
			}
		}
		minWindowCutDistance = floor(100.0*minWindowCutDistance+0.5)/100.0;
		if ( minWindowCutDistance == 100.0 )// Not got an answer
		{
			message += QString("Software validation is not reliable. Please proceed manual checking for window cut distance to profile.");		
			vStatus = max(vStatus, 2);
		}
		else 
		{
			double minDistanceAllowed = pDoc->GetVarTableValue("JIGS OUTLINE WINDOW MIN THICKNESS");
			if ( minWindowCutDistance < minDistanceAllowed )
			{
				pDoc->AppendValidateMessage( QString("OUTLINE PROFILE JIGS WINDOW CUT DISTANCE"), vStatus, message );
			}
			pDoc->AppendValidationReportJigsEntry( QString("    Window cut minimum distance to profile: %1mm").arg(minWindowCutDistance));// Just output distance
		}
	}

	//
	if ( vStatus == 0 )
	{
		if ( message.isEmpty() )
			message = QString("All pass.");
		else
			message += QString("\nAll pass.");
	}

	return (CEntValidateStatus)vStatus;
}

void CDefineOutlineProfileJigs::SetValidateIcon(CEntValidateStatus vStatus, QString message)
{
	if ( m_actValidate )
	{
		m_actValidate->setToolTip( message );
		if ( vStatus == VALIDATE_STATUS_RED )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Red.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_YELLOW )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Yellow.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_GREEN )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Green.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			m_actValidate->setToolTip( QString("Not validate.") );
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate.png" ) );
		}
	}
}

bool CDefineOutlineProfileJigs::ValidateTabWidth
(
	CTotalDoc* pDoc,				// I:
	IwTArray<double>& tabWidths		// O: [0]:medial anterior tab, [1]:lateral middle tab, [2]: medial peg tab 
)
{
	tabWidths.RemoveAll();

	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return false;

	IwTArray<IwPoint3d> tabPoints;
	pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
	if ( tabPoints.GetSize() != 6 )
		return false;

	double width;
	for (int i=0; i<3; i++)
	{
		width = tabPoints.GetAt(2*i).DistanceBetween(tabPoints.GetAt(2*i+1));
		tabWidths.Add(width);
	}
	return true;
}

bool CDefineOutlineProfileJigs::ValidateProfileLocation
(
	CTotalDoc* pDoc,				// I:
	IwTArray<IwPoint3d>& points,	// O: the curve point near reference point 
	IwTArray<double>& distances		// O: the distances
)
{
	points.RemoveAll();
	distances.RemoveAll();

	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return false;
	// Get sketch surface
	IwBSplineSurface* sketchSurf=NULL;
	pOutlineProfileJigs->GetSketchSurface(sketchSurf);
	// get reference points
	IwTArray<IwPoint3d> refPoints;
	pOutlineProfileJigs->GetOutlineProfileReferencePoints(refPoints);
	// Get outline profile projected curve points
	IwTArray<IwPoint3d> projectedCurvePoints;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedCurvePoints);

	//
	IwPoint3d refPoint, cPnt, minPnt;
	IwPoint2d param2d;
	IwVector3d normal, vec, vector0, vector1;
	int index;
	double dist;
	for (unsigned i=0; i<refPoints.GetSize(); i++) 
	{
		refPoint = refPoints.GetAt(i);
		DistFromPointToSurface(refPoint, sketchSurf, cPnt, param2d);
		sketchSurf->EvaluateNormal(param2d, FALSE, FALSE, normal);
		DistFromPointToIwTArray(refPoint, projectedCurvePoints, &normal, cPnt, &index);
		// Refine the closest point
		// project projectedCurvePoints(cIndex), projectedCurvePoints(cIndex+1), and refPoint onto the same plane
		IwPoint3d p0 = projectedCurvePoints.GetAt(index);
		IwPoint3d p1 = projectedCurvePoints.GetAt(index+1).ProjectPointToPlane(p0, normal);
		IwPoint3d p2 = refPoint.ProjectPointToPlane(p0, normal);
		IwVector3d vec01 = p1 - p0;
		vec01.Unitize();
		IwVector3d vec02 = p2 - p0;
		minPnt = p0 + vec01.Dot(vec02)*vec01;
		dist = minPnt.DistanceBetween(p2);
		points.Add(minPnt);
		distances.Add(dist);
		//
		if (i==0)
			vector0 = minPnt - p2;
		else if (i==1)
			vector1 = minPnt - p2;
	}
	// However, if inner surface jigs not exists (not square-off yet), do not validate ref points 8 & 9.
	bool innerSurfaceJigsExist = pDoc->GetInnerSurfaceJigs() != NULL; 
	if ( !innerSurfaceJigsExist && distances.GetSize() > 9 )
	{
		distances.SetAt(8, 0);
		distances.SetAt(9, 0);
	}

	// However, profile distance to refPoints 0 & 1 need to be re-validated if both sides of profile have the same shift
	// How? Profile can deviate +-3mm from refPoints 0 & 1. Which means both sides of profile can deviate up to 6mm from the preferred distance.
	// Here calculates the vectors from the profile to both refPoints. Then the half of the vector difference should not be larger than 3mm. 
	vec = vector0 - vector1;
	double delta = vec.Length()/2.0;
	double dist0, dist1;
	dist0 = distances.GetAt(0);
	dist1 = distances.GetAt(1);
	distances.SetAt(0,min(delta,dist0));
	distances.SetAt(1,min(delta,dist1));

	return true;
}

bool CDefineOutlineProfileJigs::OnReviewNext()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_OUTLINE_PROFILE_JIGS, true);
	return true;
}

bool CDefineOutlineProfileJigs::OnReviewBack()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_OUTLINE_PROFILE_JIGS, false);
	return true;
}

bool CDefineOutlineProfileJigs::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;


	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_OUTLINE_PROFILE_JIGS))
    	OnAccept();
	return true;
}

void CDefineOutlineProfileJigs::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 3;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 ) 
	{
		// Display femur
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display femur axis
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Hide osteophyte
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}

		vp->BottomView(false);	
	}
	else if ( viewIndex == 1 ) 
	{
		// Hide femoral axis
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Display osteophyte
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}

		vp->FrontView(false);
	}
	else if ( viewIndex == 2 ) 
	{
		// Hide femur
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}

		vp->BottomView(false);
	}
	m_predefinedViewIndex++;
}

///////////////////////////////////////////////////////////////////////////
// Check the UVPnt is between any arcs or not.
bool CDefineOutlineProfileJigs::IsAValidPosition(IwPoint3d UVPnt, int info)
{
	if ( info != 0 ) // not an interpolation point, just return true. Here does not check non-interpolation points.
		return true;

	IwTArray<IwPoint3d>	featurePoints;
	IwTArray<int> featurePointsInfo;
	featurePoints.Append(m_currFeaturePoints);
	featurePointsInfo.Append(m_currFeaturePointsInfo);
	int index = IFemurImplant::OutlineProfile_InsertAFeaturePointByOrder(featurePoints, featurePointsInfo, UVPnt);
	if (index>=0)
		return true;
	else
		return false;
}
