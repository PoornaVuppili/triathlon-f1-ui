#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"
#include "..\KAppTotal\Entity.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

enum CDefineStylusJigsErrorCodes
{
	DEFINE_STYLUS_JIGS_ERROR_CODES_NONE,
	DEFINE_STYLUS_JIGS_ERROR_CODES_NOT_RELIABLE
};

struct CDefineStylusJigsErrorCodesEntry
{
	CDefineStylusJigsErrorCodes			ErrorCodes;
	QString								ErrorMessages;
};

struct CDefineStylusJigsUndoData
{
	IwTArray<double>			filletRadii;			
	IwPoint3d					stylusDelta;			
};

class TEST_EXPORT_TW CDefineStylusJigs : public CManager
{
    Q_OBJECT

public:
					CDefineStylusJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CDefineStylusJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

	virtual void	Display(Viewport* vp=NULL);
    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    keyPressEvent( QKeyEvent* event, Viewport* vp=NULL );

	static void		CreateStylusJigsObject(CTotalDoc* pDoc);
	void			GetDefineStylusJigsUndoData(CDefineStylusJigsUndoData& undoData);
	void			SetDefineStylusJigsUndoData(CDefineStylusJigsUndoData& undoData);
	void			SetStylusJigsUpToDate(bool flag);
	bool			DoStylus(IwBrep*& stylusJigsBrep, IwPoint3d& stylusControlPosition, IwPoint3d& stylusDelta);
	bool			DoVirtualStylus(IwPoint3d& stylusControlPosition, IwPoint3d& stylusDelta, IwTArray<IwCurve*>& stylusWireFrame);
	bool			DoTabFilleting(IwBrep*& filletBrep, IwTArray<IwEdge*>& filletEdges, IwTArray<double>& filletRadii, bool bSkipAntTab=false);
	bool			DoVirtualFilleting(IwBrep*& stylusJigsBrep, IwTArray<IwEdge*>& filletEdges, IwTArray<double>& filletRadii,IwTArray<IwCurve*>& filletArcs);
	bool			DetermineStylusPositionNLength(IwPoint3d& stylusControlPosition, IwPoint3d& tipPosition, double& deltaLength);
	void			DetermineStylusCtrlPoints(IwTArray<IwPoint3d>& stylusCtrlPoints);
	static CEntValidateStatus Validate(CTotalDoc* pDoc, QString &message);
	static bool		DetermineStylusClearance(CTotalDoc* pDoc, double& stylusContactClearance, double& stylusArmClearance);

private:
	void			Reset(bool activateUI=true);
	void			Update(bool activateUI=true, bool updateStylusPosition=false);
	void			CreateStylusProfiles(bool bPositiveSide, IwPoint3d& stylusControlPosition, IwPoint3d& stylusDelta, IwTArray<IwBSplineCurve*>& stylusProfiles);
	void			SetValidateIcon(CEntValidateStatus vStatus, QString message);
	// for error codes
	void			AddErrorCodes(CDefineStylusJigsErrorCodes errorCode){m_errorCodes.AddUnique(errorCode);};
	void			GetErrorCodeMessage(CDefineStylusJigsErrorCodes& errorCode, QString& errorMsg);
	void			DisplayErrorMessages();

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnUpdate();
	void			OnCancel();
	void			OnValidate();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewBack();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actDefineStylusJigs;
	QAction*					m_actReset;
	QAction*					m_actUpdate;
	QAction*					m_actValidate;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewRework;

	int							m_predefinedViewIndex;
	bool						m_stylusJigsUpToDate;
	bool						m_leftButtonDown;

	IwPoint3d					m_currStylusControlPosition;
	IwPoint3d					m_oldStylusControlPosition;
	IwPoint3d					m_currTipPosition;
	IwPoint3d					m_oldTipPosition;
	IwPoint3d					m_currStylusDelta;	// delta distance along (x-axis,y-axis,z-axis) related to the default location.
	IwPoint3d					m_oldStylusDelta;
	IwTArray<IwPoint3d>			m_stylusCtrlPoints;

	IwTArray<double>			m_currFilletRadii; // the radii for tab concave edges, [0~1]:anterior tab, [2~3]:middle tab, [4~5]:peg tab
	IwTArray<IwPoint3d>			m_currFilletPoints; // fillet points are on the outer surface
	IwTArray<double>			m_oldFilletRadii; // the radii for tab concave edges, [0~1]:anterior tab, [2~3]:middle tab, [4~5]:peg tab
	IwTArray<IwPoint3d>			m_oldFilletPoints; // fillet points are on the outer surface

	double						m_currGapBetweenStylusToOuterSurface; 
	double						m_oldGapBetweenStylusToOuterSurface; 
	double						m_pickedRadius;
	bool						m_keyUpDown;

	// For error messages
	IwTArray<CDefineStylusJigsErrorCodes>	m_errorCodes;
	static CDefineStylusJigsErrorCodesEntry	DefineStylusJigsErrorCodes[];
	//// For Validation ///////////////////////////////////////////////////////////////
	CEntValidateStatus			m_validateStatus;
	QString						m_validateMessage;

};
