#pragma once

#include <QWidget>
#include <QDialog>

class QGridLayout;
class QDoubleSpinBox;
class QSpinBox;
class QCheckBox;
class QLineEdit;
class QButtonGroup;
class QActionGroup;
class QRadioButton;

class CAdvancedControlDlgJigs : public QDialog
	{
	Q_OBJECT

public:
	CAdvancedControlDlgJigs( QWidget* parent, Qt::WindowFlags fl );

	// Auto Steps
	bool	GetAutoAllSteps();
	void	SetAutoAllSteps(bool flag);
	bool	GetAutoDrivenSteps();
	void	SetAutoDrivenSteps(bool flag);

	// Osteophyte Surface

	// Cartilage Surface
	double	GetCartilageThickness();
	void	SetCartilageThickness(double val);
	double	GetTrochlearGrooveAdditionalCartilageThickness();
	void	SetTrochlearGrooveAdditionalCartilageThickness(double val);
	double	GetTrochlearGrooveCartilageTransitionWidth();
	void	SetTrochlearGrooveCartilageTransitionWidth(double val);
	bool	GetCartilageInitializeEdgeOnly();
	void	SetCartilageInitializeEdgeOnly(bool val);
	bool	GetCartilageUseWaterTightSurface();
	void	SetCartilageUseWaterTightSurface(bool val);
	bool	GetCartilageDisplaySmooth();
	void	SetCartilageDisplaySmooth(bool val);

	// Outline Profile
	bool	GetOutlineSquareOffPosteriorTip();
	void	SetOutlineSquareOffPosteriorTip(bool val);
	double	GetOutlineSquareOffCornerRadiusRatio();
	void	SetOutlineSquareOffCornerRadiusRatio(double val);
	double	GetOutlineSquareOffTransitionFactor();
	void	SetOutlineSquareOffTransitionFactor(double val);

	// Inner Surface
	double	GetInnerSurfaceApproachDegree();
	void	SetInnerSurfaceApproachDegree(double val);
	double	GetInnerSurfaceDropOffsetDistance();
	void	SetInnerSurfaceDropOffsetDistance(double val);
	double	GetInnerSurfaceAdditionalDropLength();
	void	SetInnerSurfaceAdditionalDropLength(double val);
	double	GetInnerSurfaceAdditionalPosteriorCoverage();
	void	SetInnerSurfaceAdditionalPosteriorCoverage(double val);
	double	GetInnerSurfaceAnteriorDropDegree();
	void	SetInnerSurfaceAnteriorDropDegree(double val);
	bool	GetInnerSurfaceDisplayAnteriorDropSurface();
	void	SetInnerSurfaceDisplayAnteriorDropSurface(bool val);

	// Outer Surface
	double	GetOuterSurfaceOffsetDistance();
	void	SetOuterSurfaceOffsetDistance(double val);
	double	GetOuterSurfaceTrochlearGrooveAdditionalThickness();
	void	SetOuterSurfaceTrochlearGrooveAdditionalThickness(double val);
	double	GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth();
	void	SetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth(double val);
	bool	GetOuterSurfaceDisplayAnteriorDropSurface();
	void	SetOuterSurfaceDisplayAnteriorDropSurface(bool val);
	double	GetOuterSurfaceDropDegree();
	void	SetOuterSurfaceDropDegree(double val);
	bool	GetOuterSurfaceEnforceSmooth();
	void	SetOuterSurfaceEnforceSmooth(bool val);

	// Side Surface
	double	GetSideSurfaceExtendedWidth();
	void	SetSideSurfaceExtendedWidth(double val);

	// Solid Position Jigs
	int		GetSolidPositionJigsToleranceLevel();
	void	SetSolidPositionJigsToleranceLevel(int val);

	// Stylus Jigs
	bool	GetStylusJigsDisplayVirtualFeaturesOnly();
	void	SetStylusJigsDisplayVirtualFeaturesOnly(bool val);
	bool	GetStylusJigsFullControl();
	void	SetStylusJigsFullControl(bool val);
	double	GetStylusJigsClearance();
	void	SetStylusJigsClearance(double val);

	// Anterior Surface
	double	GetAnteriorSurfaceApproachDegree();
	void	SetAnteriorSurfaceApproachDegree(double val);
	double	GetAnteriorSurfaceAntExtension();
	void	SetAnteriorSurfaceAntExtension(double val);
	double	GetAnteriorSurfaceDropExtension();
	void	SetAnteriorSurfaceDropExtension(double val);

	// Output
	bool	GetOutputOsteophyteSurface();
	void	SetOutputOsteophyteSurface(bool val);
	bool	GetOutputCartilageSurface();
	void	SetOutputCartilageSurface(bool val);
	bool	GetOutputThreeSurfaces();
	void	SetOutputThreeSurfaces(bool val);
	bool	GetOutputAnteriorSurfaceRegardless();
	void	SetOutputAnteriorSurfaceRegardless(bool val);

protected:

private slots:

private:
	QGridLayout *createDataLayout();

private:
	// Auto
	QButtonGroup*	m_buttonGroupAuto;
	QRadioButton*	m_chkAutoAllSteps;
	QRadioButton*	m_chkAutoDrivenSteps;
	QRadioButton*	m_chkAutoNoneSteps;

	// Osteophyte Surface

	// Cartilage Surface
	QDoubleSpinBox* m_spinCartilageThickness;
	QDoubleSpinBox* m_spinTroGroAddCartilageThickness;
	QDoubleSpinBox* m_spinTroGroCartilageTransitionWidth;
	QCheckBox*		m_chkCartilageInitializeEdgeOnly;
	QCheckBox*		m_chkCartilageUseWaterTightSurface;
	QCheckBox*		m_chkCartilageDisplaySmooth;
	// Not time stamp UI

	// Outline Profile
	QCheckBox*		m_chkOutlineSquareOffPosteriorTip;
	QDoubleSpinBox*	m_spinOutlineSquareOffCornerRadiusRatio;
	QDoubleSpinBox*	m_spinOutlineSquareOffTransitionFactor;

	// Inner Surface
	QDoubleSpinBox*	m_spinInnerSurfaceApproachDegree;
	QDoubleSpinBox*	m_spinInnerSurfaceDropOffsetDistance;
	QDoubleSpinBox*	m_spinInnerSurfaceAdditionalDropLength;
	QDoubleSpinBox*	m_spinInnerSurfaceAdditionalPosteriorCoverage;
	QDoubleSpinBox*	m_spinInnerSurfaceAnteriorDropDegree;
	QCheckBox*		m_chkInnerSurfaceDisplayAnteriorDropSurface;

	// Outer Surface
	QDoubleSpinBox*	m_spinOuterSurfaceOffsetDistance;
	QDoubleSpinBox* m_spinOuterSurfaceTroGroAddThickness;
	QDoubleSpinBox* m_spinOuterSurfaceTroGroAddThicknessTransitionWidth;
	QCheckBox*		m_chkOuterSurfaceDisplayAnteriorDropSurface;
	QDoubleSpinBox*	m_spinOuterSurfaceDropDegree;
	QCheckBox*		m_chkOuterSurfaceEnforceSmooth;

	// Side Surface
	QDoubleSpinBox*	m_spinSideSurfaceExtendedWidth;

	// Solid Position Jigs
	QSpinBox*		m_spinSolidPositionJigsToleranceLevel;

	// Stylus Jigs
	QCheckBox*		m_chkStylusJigsDisplayVirtualFeaturesOnly;
	QCheckBox*		m_chkStylusJigsFullControl;
	QDoubleSpinBox*	m_spinStylusJigsClearance;

	// Anterior Surface
	QDoubleSpinBox*	m_spinAnteriorSurfaceApproachDegree;
	QDoubleSpinBox*	m_spinAnteriorSurfaceAntExtension;
	QDoubleSpinBox*	m_spinAnteriorSurfaceDropExtension;

	// Output
	QCheckBox*		m_chkOutputOsteophyteSurface;
	QCheckBox*		m_chkOutputCartilageSurface;
	QCheckBox*		m_chkOutputThreeSurfaces;
	QCheckBox*		m_chkOutputAnteriorSurfaceRegardless;

};
