/****************************************************************************
** Meta object code from reading C++ file 'OutlineProfileMgrUIBase.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../OutlineProfileMgrUIBase.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OutlineProfileMgrUIBase.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OutlineProfileMgrUIBase_t {
    QByteArrayData data[10];
    char stringdata0[102];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OutlineProfileMgrUIBase_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OutlineProfileMgrUIBase_t qt_meta_stringdata_OutlineProfileMgrUIBase = {
    {
QT_MOC_LITERAL(0, 0, 23), // "OutlineProfileMgrUIBase"
QT_MOC_LITERAL(1, 24, 19), // "SwitchToCoordSystem"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 4), // "name"
QT_MOC_LITERAL(4, 50, 11), // "OnDispRuler"
QT_MOC_LITERAL(5, 62, 10), // "OnValidate"
QT_MOC_LITERAL(6, 73, 7), // "OnReset"
QT_MOC_LITERAL(7, 81, 8), // "OnRefine"
QT_MOC_LITERAL(8, 90, 5), // "OnAdd"
QT_MOC_LITERAL(9, 96, 5) // "OnDel"

    },
    "OutlineProfileMgrUIBase\0SwitchToCoordSystem\0"
    "\0name\0OnDispRuler\0OnValidate\0OnReset\0"
    "OnRefine\0OnAdd\0OnDel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OutlineProfileMgrUIBase[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   52,    2, 0x0a /* Public */,
       5,    0,   53,    2, 0x0a /* Public */,
       6,    0,   54,    2, 0x0a /* Public */,
       7,    0,   55,    2, 0x0a /* Public */,
       8,    0,   56,    2, 0x0a /* Public */,
       9,    0,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void OutlineProfileMgrUIBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<OutlineProfileMgrUIBase *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SwitchToCoordSystem((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->OnDispRuler(); break;
        case 2: _t->OnValidate(); break;
        case 3: _t->OnReset(); break;
        case 4: _t->OnRefine(); break;
        case 5: _t->OnAdd(); break;
        case 6: _t->OnDel(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (OutlineProfileMgrUIBase::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OutlineProfileMgrUIBase::SwitchToCoordSystem)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject OutlineProfileMgrUIBase::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_OutlineProfileMgrUIBase.data,
    qt_meta_data_OutlineProfileMgrUIBase,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OutlineProfileMgrUIBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OutlineProfileMgrUIBase::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OutlineProfileMgrUIBase.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int OutlineProfileMgrUIBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void OutlineProfileMgrUIBase::SwitchToCoordSystem(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
