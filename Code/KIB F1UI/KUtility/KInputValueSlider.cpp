#include "KInputValueSlider.h"
#include "ui_KInputValueSlider.h"

KInputValueSlider::KInputValueSlider(QWidget *parent)
    : QDialog(parent), ui(new Ui_InputValueSliderClass())
{
    ui->setupUi(this);

    connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(onSliderChange(int)));
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(onSpinBoxChange(int)));

    initValue = 0;
}

KInputValueSlider::~KInputValueSlider()
{
    ui->horizontalSlider->disconnect();
    ui->spinBox->disconnect();
    delete ui;
}

void KInputValueSlider::SetLabelText(QString& str)
{
    ui->label->setText(str);
}

void KInputValueSlider::SetRange(int min, int max)
{
    ui->horizontalSlider->setRange(min, max);
    ui->spinBox->setRange(min, max);
}

void KInputValueSlider::onSliderChange(int v)
{
    if( v != ui->spinBox->value() )
        ui->spinBox->setValue(v);
    emit valueChanged(v);
}

void KInputValueSlider::onSpinBoxChange(int v)
{
    if( v != ui->horizontalSlider->value() )
        ui->horizontalSlider->setValue(v);
}

void KInputValueSlider::InitValue(int v)
{
    initValue = v;
    SetValue(v);
}

void KInputValueSlider::SetValue(int v)
{
    if( v != ui->horizontalSlider->value() )
        ui->horizontalSlider->setValue(v);
}

int KInputValueSlider::GetValue()
{
    return ui->spinBox->value();
}

void KInputValueSlider::on_pushButtonOK_clicked()
{
    accept();
}

void KInputValueSlider::on_pushButtonCancel_clicked()
{
    if( initValue != GetValue() )
        SetValue(initValue );

    reject();
}
