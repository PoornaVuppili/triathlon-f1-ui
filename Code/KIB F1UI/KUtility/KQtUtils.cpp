#include "KQtUtils.h"

namespace KQtUtils
{
	void QStringToCharArray( const QString& sText, char* out )
	{
		QByteArray			byteArray = sText.toLatin1();
		char*				data = byteArray.data();

		strcpy( out, data );
	}
}