#include "DocLogWriter.h"
#include "DataDoc.h"

#include "..\KApp\Implant.h"
#include "..\KApp\MainWindow.h"

////////////////////////////////////////////////////////////////////////////////////////////
// The way to use CDocLogWriter is as follows:
// When open a part file, call CDocLogWriter::InitializeLogFile()
// When executing application, call CDocLogWriter::AppendLog() to
// record user's actions.
// When close the part file, call CDocLogWriter::InitializeLogFile()
//
// The user's actions will be realtime recorded in TriathlonF1ActionLogCrash.txt.
// When terminate the application, TriathlonF1ActionLogCrash.txt will be copied to
// TriathlonF1ActionLog.txt and TriathlonF1ActionLogCrash.txt will be deleted.
// When crash happens, TriathlonF1ActionLogCrash.txt keeps all information till crash.
// When users restart the application again, TriathlonF1ActionLogCrash.txt 
// will be copied to TriathlonF1ActionLogPreviousCrash.txt.
//////////////////////////////////////////////////////////////////////////////////////////////
CDocLogWriter::CDocLogWriter( )
{
	m_fileDir=QString("");
}


CDocLogWriter::~CDocLogWriter()
	{
	}

QString	CDocLogWriter::GetFileCrashName(bool completePath) 
{
	QString strSolidworksDir = mainWindow->GetImplant()->GetInfo("SolidWorksDir");

	if ( !completePath )
		return QString("TriathlonF1ActionLogCrash.txt");
	else if ( m_fileDir.isEmpty() )
		return QString(strSolidworksDir + QDir::separator() + "TriathlonF1ActionLogCrash.txt");
	else
		return m_fileDir + QString("\\TriathlonF1ActionLogCrash.txt");
}

QString	CDocLogWriter::GetFileLogName() 
{
	QString strSolidworksDir = mainWindow->GetImplant()->GetInfo("SolidWorksDir");

	if ( m_fileDir.isEmpty() )
		return QString(strSolidworksDir + QDir::separator() + "TriathlonF1ActionLog.txt");
	else 
		return m_fileDir + QString("\\TriathlonF1ActionLog.txt");
}

QString	CDocLogWriter::GetFileDir() 
{
	QString strSolidworksDir = mainWindow->GetImplant()->GetInfo("SolidWorksDir");

	if ( m_fileDir.isEmpty() )
		return strSolidworksDir;
	else
		return m_fileDir;
}

void CDocLogWriter::SetFileDir(QString fileDir)
{
	m_fileDir = fileDir;
}

QString	CDocLogWriter::GetFilePreviousCrashName()
	{
	QString			prevCrashName;
	QFile			crashFile( GetFileCrashName() );

	QString strSolidworksDir = mainWindow->GetImplant()->GetInfo("SolidWorksDir");

	if ( !crashFile.exists() )
	{
		if ( m_fileDir.isEmpty() )
			prevCrashName = QString(strSolidworksDir + QDir::separator() + "TriathlonF1ActionLogPreviousCrash.txt");
		else
			prevCrashName = m_fileDir + QString("\\TriathlonF1ActionLogPreviousCrash.txt");
	}
	else
	{
		QTextStream	in( &crashFile );

		QString PatientID;
		if ( crashFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
		{
			QString str;
			while( !in.atEnd() )
			{
				str = in.readLine();
				int existIndex = str.indexOf("PatientID: ", 0, Qt::CaseInsensitive);
				if ( existIndex != -1 )// exist
				{
					str.remove(": ", Qt::CaseInsensitive);
					PatientID = str + " ";
					break;
				}
			}
			crashFile.close();
		}

		QFileInfo fileInfo(crashFile);
		QDateTime lastModified = fileInfo.lastModified();
		if ( m_fileDir.isEmpty() )
			prevCrashName = QString(strSolidworksDir + QDir::separator() + "TriathlonF1ActionLogPreviousCrash ") + PatientID + lastModified.toString("yyyy.MM.dd.hh.mm.ss") + QString(".txt");
		else
			prevCrashName = m_fileDir + QString("\\TriathlonF1ActionLogPreviousCrash ") + PatientID + lastModified.toString("yyyy.MM.dd.hh.mm.ss") + QString(".txt");

	}

	return prevCrashName;
	}


QString	CDocLogWriter::GetFilePreviousLogName()
{
	QString			previousLogName;
	QFile			crashFile( GetFileCrashName() );

	QString strSolidworksDir = mainWindow->GetImplant()->GetInfo("SolidWorksDir");

	if ( !crashFile.exists() )
	{
		if ( m_fileDir.isEmpty() )
			previousLogName = QString(strSolidworksDir + QDir::separator() + "TriathlonF1ActionLog.txt");
		else
			previousLogName = m_fileDir + QString("\\TriathlonF1ActionLog.txt");
	}
	else
	{
		QTextStream	in( &crashFile );

		QString PatientID;
		if ( crashFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
		{
			QString str;
			while( !in.atEnd() )
			{
				str = in.readLine();
				int existIndex = str.indexOf("PatientID: ", 0, Qt::CaseInsensitive);
				if ( existIndex != -1 )// exist
				{
					str.remove(": ", Qt::CaseInsensitive);
					PatientID = str + " ";
					break;
				}
			}
			crashFile.close();
		}

		QFileInfo fileInfo(crashFile);
		QDateTime lastModified = fileInfo.lastModified();
		if ( m_fileDir.isEmpty() )
			previousLogName = QString(strSolidworksDir + QDir::separator() + "TriathlonF1ActionLog ") + PatientID + lastModified.toString("yyyy.MM.dd.hh.mm.ss") + QString(".txt");
		else
			previousLogName = m_fileDir + QString("\\TriathlonF1ActionLog ") + PatientID + lastModified.toString("yyyy.MM.dd.hh.mm.ss") + QString(".txt");

	}

	return previousLogName;
}

void CDocLogWriter::DeleteOldCrashFiles()
	{
	QDir			dir( GetFileDir() );
	if ( !dir.exists() ) return;
	
	QDateTime lastMonth = QDateTime::currentDateTime().addMonths(-1);
	//QDateTime lastMonth = QDateTime::currentDateTime().addSecs(-300);
	QDateTime lastModified;
	QFileInfo fileInfo;
	QString baseName;
	QFileInfoList fileInfoList = dir.entryInfoList();
	int fileNo = fileInfoList.size();
	for (int i=0; i<fileNo; i++)
	{
		fileInfo = fileInfoList.at(i);
		baseName = fileInfo.baseName();
		if ( baseName.left(33) == QString("TriathlonF1ActionLogPreviousCrash") )
		{
			lastModified = fileInfo.lastModified();
			QString lm = lastModified.toString();
			QString lmon = lastMonth.toString();
			if ( lastModified < lastMonth )
			{
				QFile oldFile(fileInfo.filePath ());
				oldFile.remove();
			}
		}

	}
	return;
	}

void CDocLogWriter::AppendLog(const QString& appendLogString, bool newLine)
	{
	bool			bOK;

	QString logName = GetFileCrashName();

	QFile			File( logName );
	QTextStream		out( &File );

	bOK = File.open( QIODevice::Append | QIODevice::Text );

	if( !bOK )
		return;

	if (newLine)
		out << QString("%1\n").arg(appendLogString);
	else
		out << QString("%1 ...... ").arg(appendLogString);

	File.close();

	return;
	}


/////////////////////////////////////////////////////////////////////////////
// When initialize the log file, the backFile will be deteted. The
// system records the log into the crashFile. 
// When normal initialize TriathlonF1 and normal terminate TriathlonF1
// (no crash happens during execution), the crashFile will be 
// deleted when terminate (see next method). When crash happens,
// the crashFile will exist. Users can check the info inside the crashFile.
// If users restart the TriathlonF1 again when crash happens previously, 
// the crashFile will be copy to previousCrashFile. In case users forget to
// check the crashFile info and restart TriathlonF1, they at least 
// still have crash info in previousCrashFile.
//////////////////////////////////////////////////////////////////////////////
void CDocLogWriter::InitializeLogFile(QString patientID)
{
	QFile			crashFile( GetFileCrashName() );

	// Make the folder, if does not exist.
	if ( !crashFile.exists() )
	{
		QString			logFloder = GetFileDir();
		QDir			dir( logFloder );

		if( !dir.exists() )
		{
			bool bOK = dir.mkdir( logFloder );

			if( !bOK )
			{
				return;
			}
		}
	}

	// crash situation (crash previously)
	if (crashFile.exists())
	{
		// Delete previous crash files one month ago or older.
		DeleteOldCrashFiles();
		// copy to the previous crash name
		crashFile.copy( GetFilePreviousCrashName() );
	}

	// Initialize the crash file
	bool bOK = crashFile.open( QIODevice::WriteOnly | QIODevice::Text );

	QTextStream		out( &crashFile );

	// get date & time
	QString			sDate = QDateTime::currentDateTime().toString();
	// Get the user info
	char acUserName[100];
	DWORD nUserName = sizeof(acUserName);
	GetUserName(acUserName, &nUserName);

	out << QString("This log file was created by %1 on %2\n").arg(acUserName).arg(sDate);

	// Patient ID
	if ( !patientID.isEmpty() )
		out << QString("PatientID: %1\n").arg(patientID);

	crashFile.close();

}

void CDocLogWriter::TerminateLogFile()
{
	QFile			crashFile( GetFileCrashName() );

	// Copy the crashFile into logFile and delete the crashFile.
	crashFile.copy(GetFilePreviousLogName());

	crashFile.remove();

	m_fileDir = QString("");
}

