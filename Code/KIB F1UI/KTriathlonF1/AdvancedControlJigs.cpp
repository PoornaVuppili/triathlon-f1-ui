#include "AdvancedControlJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "TotalView.h"

CAdvancedControlJigs::CAdvancedControlJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CEntity( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_ADVANCED_CONTROL );

	m_eDisplayMode = DISP_MODE_NONE;

	m_bModified = false;
	m_bDataModified = false;

	double sizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;

	// Auto
	m_autoAllSteps = false; 
	m_autoDrivenSteps = true; 

	// Osteophyte Surface
	m_osteophyteTimeStampForInner = 0;
	m_osteophyteTimeStampForOuter = 0;

	// Cartilage Surface
	m_cartilageThickness = 3.1;
	m_troGroAddCartilageThickness = 2.0;
	m_troGroCartilageTransitionWidth = 9.5*sizeRatio; // It's proportional to femoral size;
	m_troGroCartilageTransitionWidth = 0.5*ceil(m_troGroCartilageTransitionWidth/0.5);// round up to near 0.5
	m_cartilageInitializeEdgeOnly = false;
	m_cartilageTimeStamp = 0;
	m_cartilageUseWaterTightSurface = false;
	m_cartilageDisplaySmooth = false;

	// Outline Profile
	m_outlineSquareOffPosteriorTip = true;
	m_outlineSquareOffCornerRadiusRatio = 0.35;
	m_outlineSquareOffTransitionFactor = 2.0;

	// Inner Surface
	m_innerSurfaceApproachDegree = 0.0;
	m_innerSurfaceDropOffsetDistance = 0.1;
	m_innerSurfaceAdditionalDropLength = 0.0; 
	m_innerSurfaceAdditionalPosteriorCoverage = 0.0;
	m_innerSurfaceAnteriorDropDegree = 1.0;
	m_innerSurfaceDisplayAnteriorDropSurface = false;

	// Outer Surface
	m_outerSurfaceOffsetDistance = 3.5;
	m_outerSurfaceTroGroAddThickness = 2.5;
	m_outerSurfaceTroGroAddThicknessTransitionWidth = 0.40; // Percentage of the width of the outline profile jigs at each side
	m_outerSurfaceDisplayAnteriorDropSurface = false;
	m_outerSurfaceDropDegree = 0.0;
	m_outerSurfaceEnforceSmooth = true;

	// Side Surface
	m_sideSurfaceExtendedWidth = 3.0;

	// Solid Position Jigs 
	m_solidPositionJigsToleranceLevel = 3;

	// Stylus Jigs
	m_stylusJigsDisplayVirtualFeaturesOnly = false;
	m_stylusJigsFullControl = false;
	m_stylusJigsClearance = m_pDoc->GetVarTableValue("JIGS STYLUS CLEARANCE DISTANCE");

	// Anterior Surface
	m_anteriorSurfaceApproachDegree = 6.0;
	m_anteriorSurfaceAntExtension = 0;
	m_anteriorSurfaceDropExtension = 0;

	// Outputs
	m_outputOsteophyteSurface = false;
	m_outputCartilageSurface = false;
	m_outputThreeSurfaces = false;
	m_outputAnteriorSurfaceRegardless = false;


}

bool CAdvancedControlJigs::Save( const QString& sDir, bool incrementalSave )
{
	QString				sFileName;
	bool				bOK = true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_ac_jigs.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	// Auto
	// Not save m_autoAllSteps
	// Not save m_autoDrivenSteps

	// Osteophyte surface
	// Write m_osteophyteTimeStamp
	File.write( (char*) &m_osteophyteTimeStampForOuter, sizeof( unsigned ) );

	// Cartilage Surface
	// Write m_cartilageThickness
	File.write( (char*) &m_cartilageThickness, sizeof( double ) );
	// Write m_cartilageTransitionWidth. Be retired in iTW6
	double cartilageTransitionWidth=0;
	File.write( (char*) &cartilageTransitionWidth, sizeof( double ) );
	// Write m_troGroAddCartilageThickness
	File.write( (char*) &m_troGroAddCartilageThickness, sizeof( double ) );
	// Write m_troGroCartilageTransitionWidth
	File.write( (char*) &m_troGroCartilageTransitionWidth, sizeof( double ) );
	// Write m_cartilageInitializeEdgeOnly
	// Not write m_cartilageInitializeEdgeOnly
	// Write m_cartilageTimeStamp
	File.write( (char*) &m_cartilageTimeStamp, sizeof( unsigned ) );
	// Write m_cartilageUseWaterTightSurface
	File.write( (char*) &m_cartilageUseWaterTightSurface, sizeof( bool ) );
	// Write m_cartilageDisplaySmooth
	// No need to File.write( (char*) &m_cartilageDisplaySmooth, sizeof( bool ) );
	// Write m_cartilageTransitionWidthInnerRatio. Be retired in iTW6
	double cartilageTransitionWidthInnerRatio=0;
	File.write( (char*) &cartilageTransitionWidthInnerRatio, sizeof( double ) );

	// Outline Profile

	// Inner Surface
	// Write m_innerSurfaceDropOffsetDistance
	File.write( (char*) &m_innerSurfaceDropOffsetDistance, sizeof( double ) );
	// Write m_innerSurfaceDropDegree
	double innerSurfaceDropDegree = 0;// retired from iTW6
	File.write( (char*) &innerSurfaceDropDegree, sizeof( double ) );
	// Write m_innerSurfaceAnteriorDropDegree
	File.write( (char*) &m_innerSurfaceAnteriorDropDegree, sizeof( double ) );
	// Write m_innerSurfaceDisplayAnteriorDropSurface
	// Not File.write( (char*) &m_innerSurfaceDisplayAnteriorDropSurface, sizeof( bool ) );
	// Write m_innerSurfaceDropEdgeDegree
	double innerSurfaceDropEdgeDegree = 0; // retired from iTW6
	File.write( (char*) &innerSurfaceDropEdgeDegree, sizeof( double ) );

	// Outer Surface
	// Write m_outerSurfaceOffsetDistance
	File.write( (char*) &m_outerSurfaceOffsetDistance, sizeof( double ) );
	// Write m_outerSurfaceDisplayAnteriorDropSurface
	// not File.write( (char*) &m_outerSurfaceDisplayAnteriorDropSurface, sizeof( bool ) );
	// Write m_outerSurfaceDropDegree
	File.write( (char*) &m_outerSurfaceDropDegree, sizeof( double ) );
	// Write m_outerSurfaceTroGroAddThickness
	File.write( (char*) &m_outerSurfaceTroGroAddThickness, sizeof( double ) );
	// Write m_outerSurfaceTroGroAddThicknessTransitionWidth
	File.write( (char*) &m_outerSurfaceTroGroAddThicknessTransitionWidth, sizeof( double ) );

	// Side Surface
	// Write m_sideSurfaceExtendedWidth
	File.write( (char*) &m_sideSurfaceExtendedWidth, sizeof( double ) );
	// Write m_sideSurfaceTabFanningDistance
	// m_sideSurfaceTabFanningDistance was retired in iTW6
	double sideSurfaceTabFanningDistance = 0;
	File.write( (char*) &sideSurfaceTabFanningDistance, sizeof( double ) );

	// Solid Position Jigs
	// Write m_solidPositionJigsToleranceLevel
	File.write( (char*) &m_solidPositionJigsToleranceLevel, sizeof( int ) );
	
	// Stylus Position Jigs
	// write m_stylusJigsDisplayVirtualFeaturesOnly
	File.write( (char*) &m_stylusJigsDisplayVirtualFeaturesOnly, sizeof( bool ) );

	// write m_stylusJigsFullControl
	// No need to write m_stylusJigsFullControl

	// Anterior Surface for iTW 5.0.4
	// write m_anteriorSurfaceAntExtension
	File.write( (char*) &m_anteriorSurfaceAntExtension, sizeof( double ) );

	///////////////////////////////////////////////////////////////////////////////
	////NOTE: THE FOLLOWING PARAMETERS WERE ADDED DURING iTW v5.2 DEVELOPMENT//////
	////      BUT iTW v6.0.0~6.0.25 HAD BEEN DEVELOPED BEFORE iTW v5.2       //////
	///////////////////////////////////////////////////////////////////////////////
	//// Parameters for iTW 5.2.1
	//// write m_anteriorSurfaceFineResolution
	//File.write( (char*) &m_anteriorSurfaceFineResolution, sizeof( bool ) );
	//// write m_anteriorSurfaceSmoothAPCliff
	//File.write( (char*) &m_anteriorSurfaceSmoothAPCliff, sizeof( bool ) );

	//// Parameters for iTW 5.2.2
	//// write m_innerSurfaceSmoothAPCliff
	//File.write( (char*) &m_innerSurfaceSmoothAPCliff, sizeof( bool ) );
	//// write m_outerSurfaceSmoothAPCliff
	//File.write( (char*) &m_outerSurfaceSmoothAPCliff, sizeof( bool ) );
	//// No need to write m_anteriorSurfaceSmoothMesh
	//// File.write( (char*) &m_anteriorSurfaceSmoothMesh, sizeof( bool ) );
	//// Osteophyte surface
	//// Write m_osteophyteTimeStampForInner
	//File.write( (char*) &m_osteophyteTimeStampForInner, sizeof( unsigned ) );
	//////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////
	// New parameter for iTW606
	// Write m_innerSurfaceApproachDegree
	File.write( (char*) &m_innerSurfaceApproachDegree, sizeof( double ) );
	// Write m_stylusJigsClearance
	File.write( (char*) &m_stylusJigsClearance, sizeof( double ) );

	////////////////////////////////////////
	// New parameter for iTW609
	// Write m_innerSurfaceAdditionalDropLength
	File.write( (char*) &m_innerSurfaceAdditionalDropLength, sizeof( double ) );

	////////////////////////////////////////
	// New parameter for iTW6012
	// Write m_outerSurfaceEnforceSmooth
	File.write( (char*) &m_outerSurfaceEnforceSmooth, sizeof( bool ) );

	////////////////////////////////////////
	// New parameter for iTW6013
	// Write m_anteriorSurfaceDropExtension
	File.write( (char*) &m_anteriorSurfaceDropExtension, sizeof( double ) );

	////////////////////////////////////////
	// New parameter for iTW6015
	// Write m_innerSurfaceAdditionalPosteriorCoverage
	File.write( (char*) &m_innerSurfaceAdditionalPosteriorCoverage, sizeof( double ) );
	// Write m_outlineSquareOffPosteriorTip
	File.write( (char*) &m_outlineSquareOffPosteriorTip, sizeof( bool ) );
	// Write m_outlineSquareOffCornerRadiusRatio
	File.write( (char*) &m_outlineSquareOffCornerRadiusRatio, sizeof( double ) );
	// Write m_outlineSquareOffTransitionFactor
	File.write( (char*) &m_outlineSquareOffTransitionFactor, sizeof( double ) );

	////////////////////////////////////////
	// New parameter for iTW6026
	// Osteophyte surface
	// Write m_osteophyteTimeStampForInner
	File.write( (char*) &m_osteophyteTimeStampForInner, sizeof( unsigned ) );

	////////////////////////////////////////
	// New parameter for iTW6027
	// Write m_anteriorSurfaceApproachDegree
	File.write( (char*) &m_anteriorSurfaceApproachDegree, sizeof( double ) );

	File.close();

	m_bDataModified = false;

	return true;
}


bool CAdvancedControlJigs::Load( const QString& sDir )
{
	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_ac_jigs.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	// Variables for TriathlonF1 version 5.0.0
	if ( m_pDoc->IsOpeningFileRightVersion(5,0,0) )
	{
		// Auto
		// Not load m_autoAllSteps
		// Not load m_autoDrivenSteps

		// Osteophyte Surface
		// read m_osteophyteTimeStamp
		File.read( (char*) &m_osteophyteTimeStampForOuter, sizeof(unsigned) );

		// Cartilage Surfcae
		// read m_cartilageThickness
		File.read( (char*) &m_cartilageThickness, sizeof(double) );
		// read m_cartilageTransitionWidth. Be retired in iTW6.
		double cartilageTransitionWidth;
		File.read( (char*) &cartilageTransitionWidth, sizeof(double) );
		// read m_troGroAddCartilageThickness
		File.read( (char*) &m_troGroAddCartilageThickness, sizeof(double) );
		// read m_troGroCartilageTransitionWidth
		File.read( (char*) &m_troGroCartilageTransitionWidth, sizeof(double) );
		// read m_cartilageInitializeEdgeOnly
		// Not read m_cartilageInitializeEdgeOnly
		// read m_cartilageTimeStamp
		File.read( (char*) &m_cartilageTimeStamp, sizeof(unsigned) );
		// read m_cartilageUseWaterTightSurface
		File.read( (char*) &m_cartilageUseWaterTightSurface, sizeof(bool) );
		// read m_cartilageDisplaySmooth
		// No need to File.read( (char*) &m_cartilageDisplaySmooth, sizeof(bool) );
		// read m_cartilageTransitionWidthInnerRatio. Be retired in iTW6.
		double cartilageTransitionWidthInnerRatio;
		File.read( (char*) &cartilageTransitionWidthInnerRatio, sizeof(double) );

		// Outline Profile

		// Inner Surface
		// read m_innerSurfaceDropOffsetDistance
		File.read( (char*) &m_innerSurfaceDropOffsetDistance, sizeof(double) );
		// read m_innerSurfaceDropDegree
		double innerSurfaceDropDegree;// Retired from iTW6
		File.read( (char*) &innerSurfaceDropDegree, sizeof(double) );
		// read m_innerSurfaceAnteriorDropDegree
		File.read( (char*) &m_innerSurfaceAnteriorDropDegree, sizeof(double) );
		// read m_innerSurfaceDisplayAnteriorDropSurface
		// not File.read( (char*) &m_innerSurfaceDisplayAnteriorDropSurface, sizeof(bool) );
		// read m_innerSurfaceDropEdgeDegree
		double innerSurfaceDropEdgeDegree;// Retired from iTW6
		File.read( (char*) &innerSurfaceDropEdgeDegree, sizeof(double) );

		// Outer Surface
		// read m_outerSurfaceOffsetDistance
		File.read( (char*) &m_outerSurfaceOffsetDistance, sizeof(double) );
		// read m_outerSurfaceDisplayAnteriorDropSurface
		// not File.read( (char*) &m_outerSurfaceDisplayAnteriorDropSurface, sizeof(bool) );
		// read m_outerSurfaceDropDegree
		File.read( (char*) &m_outerSurfaceDropDegree, sizeof(double) );
		// read m_outerSurfaceTroGroAddThickness
		File.read( (char*) &m_outerSurfaceTroGroAddThickness, sizeof(double) );
		// read m_outerSurfaceTroGroAddThicknessTransitionWidth
		File.read( (char*) &m_outerSurfaceTroGroAddThicknessTransitionWidth, sizeof(double) );

		// Side Surface
		// read m_sideSurfaceExtendedWidth
		File.read( (char*) &m_sideSurfaceExtendedWidth, sizeof(double) );
		// read m_sideSurfaceTabFanningDistance
		// m_sideSurfaceTabFanningDistance was retired in iTW6
		double sideSurfaceTabFanningDistance;
		File.read( (char*) &sideSurfaceTabFanningDistance, sizeof(double) );

		// Solid Position Jigs
		// read m_solidPositionJigsToleranceLevel
		File.read( (char*) &m_solidPositionJigsToleranceLevel, sizeof(int) );

		// Stylus Position Jigs
		// read m_stylusJigsDisplayVirtualFeaturesOnly
		File.read( (char*) &m_stylusJigsDisplayVirtualFeaturesOnly, sizeof(bool) );
		// read m_stylusJigsFullControl
		// No need to read m_stylusJigsFullControl
	}

	if ( m_pDoc->IsOpeningFileRightVersion(5,0,4) )
	{
		// Anterior Surface
		// read m_anteriorSurfaceAntExtension
		File.read( (char*) &m_anteriorSurfaceAntExtension, sizeof(double) );
	}

	//////////////////////////////////////////////////////////////////////
	//// SEE THE COMMENTS IN SAVE FUNCTION.
	//////////////////////////////////////////////////////////////////////
	if ( m_pDoc->IsOpeningFileRightVersion(5,2,1) && !m_pDoc->IsOpeningFileRightVersion(6,0,0) ) // versions [5.2.1~6.0.0) (include 5.2.1, but not include 6.0.0)
	{
		// Anterior Surface
		// read m_anteriorSurfaceFineResolution
		bool anteriorSurfaceFineResolution; // But we do not use anteriorSurfaceFineResolution in iTW 6.0.0 anymore
		File.read( (char*) &anteriorSurfaceFineResolution, sizeof(bool) );
		// read m_anteriorSurfaceSmoothAPCliff
		bool anteriorSurfaceSmoothAPCliff; // But we do not use anteriorSurfaceSmoothAPCliff in iTW 6.0.0 anymore
		File.read( (char*) &anteriorSurfaceSmoothAPCliff, sizeof(bool) );
	}
	//////////////////////////////////////////////////////////////////////
	//// SEE THE COMMENTS IN SAVE FUNCTION.
	//////////////////////////////////////////////////////////////////////
	if ( m_pDoc->IsOpeningFileRightVersion(5,2,2) && !m_pDoc->IsOpeningFileRightVersion(6,0,0) ) // versions [5.2.1~6.0.0) (include 5.2.1, but not include 6.0.0)
	{
		// Inner Surface
		// read m_innerSurfaceSmoothAPCliff
		bool innerSurfaceSmoothAPCliff; // But we do not use innerSurfaceSmoothAPCliff in iTW 6.0.0 anymore
		File.read( (char*) &innerSurfaceSmoothAPCliff, sizeof(bool) );
		// Outer Surface
		// read m_outerSurfaceSmoothAPCliff
		bool outerSurfaceSmoothAPCliff; // But we do not use innerSurfaceSmoothAPCliff in iTW 6.0.0 anymore
		File.read( (char*) &outerSurfaceSmoothAPCliff, sizeof(bool) );
		// Anterior Surface
		// No need to read m_anteriorSurfaceSmoothMesh
		// File.read( (char*) &m_anteriorSurfaceSmoothMesh, sizeof(bool) );
		// Osteophyte Surface
		// read m_osteophyteTimeStampForInner
		File.read( (char*) &m_osteophyteTimeStampForInner, sizeof(unsigned) );
	}

	// Variables for TriathlonF1 version 6.0.6
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,6) )
	{
		// Inner Surface
		// read m_innerSurfaceApproachDegree
		File.read( (char*) &m_innerSurfaceApproachDegree, sizeof(double) );
		// read m_stylusJigsClearance
		File.read( (char*) &m_stylusJigsClearance, sizeof(double) );
	}

	// Variables for TriathlonF1 version 6.0.9
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,9) )
	{
		// Inner Surface
		// read m_innerSurfaceAdditionalDropLength
		File.read( (char*) &m_innerSurfaceAdditionalDropLength, sizeof(double) );
	}

	// Variables for TriathlonF1 version 6.0.12
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,12) )
	{
		// outer Surface
		// read m_outerSurfaceEnforceSmooth
		File.read( (char*) &m_outerSurfaceEnforceSmooth, sizeof(bool) );
	}

	// Variables for TriathlonF1 version 6.0.13
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,13) )
	{
		// outer Surface
		// read m_anteriorSurfaceDropExtension
		File.read( (char*) &m_anteriorSurfaceDropExtension, sizeof(double) );
	}
	// Variables for TriathlonF1 version 6.0.15
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,15) )
	{
		// inner surface
		// read m_innerSurfaceAdditionalPosteriorCoverage
		File.read( (char*) &m_innerSurfaceAdditionalPosteriorCoverage, sizeof(double) );
		// outline profile
		// read m_outlineSquareOffPosteriorTip
		File.read( (char*) &m_outlineSquareOffPosteriorTip, sizeof(bool) );
		// read m_outlineSquareOffCornerRadiusRatio
		File.read( (char*) &m_outlineSquareOffCornerRadiusRatio, sizeof(double) );
		// read m_outlineSquareOffTransitionFactor
		File.read( (char*) &m_outlineSquareOffTransitionFactor, sizeof(double) );
	}
	else
	{
		m_outlineSquareOffPosteriorTip = false;// For the old data, it should be false.
	}
	// Variables for TriathlonF1 version 6.0.26
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,26) )
	{
		// Read m_osteophyteTimeStampForInner
		File.read( (char*) &m_osteophyteTimeStampForInner, sizeof( unsigned ) );
	}
	// Variables for TriathlonF1 version 6.0.27
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,27) )
	{
		// Read m_anteriorSurfaceApproachDegree
		File.read( (char*) &m_anteriorSurfaceApproachDegree, sizeof( double ) );
	}

	m_bDataModified = false;

	File.close();

	return true;
}


void CAdvancedControlJigs::SetModifiedFlag( bool bModified )
{
	m_bModified = bModified;
	if (m_bModified)
	{
		m_bDataModified = true;
		m_pDoc->SetModified(m_bModified);
	}
}

