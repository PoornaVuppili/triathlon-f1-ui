#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW COutlineProfile : public CPart
{
public:
	COutlineProfile( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_OUTLINE_PROFILE, const QString& sFileName="", int nEntId=-1 );
	~COutlineProfile();
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);
	bool			IsMemberDataChanged(IwTArray<IwPoint3d>& featurePoints);
	void			SetSketchSurface(IwBSplineSurface* sketchSurface);
	bool			GetSketchSurface(IwBSplineSurface*& sketchSurface);
	bool			GetSketchSurfaceFromBrep(IwBSplineSurface*& sketchSurface);
	void			SetOutlineProfileFeaturePoints(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, IwTArray<IwPoint3d>& featurePointsRemap);
	void			GetOutlineProfileFeaturePoints(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, IwTArray<IwPoint3d>& featurePointsRemap);
	int				GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(double femoralCutsFeatureIndex);
	bool			CreateCurveOnSketchSurface(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, bool updateFeaturePoints, IwBSplineCurve*& crvOnSurf, IwBSplineCurve*& UVCurve, bool trueCurveOnSurf=false);
	bool			GetCurveOnSketchSurface(IwBSplineCurve*& crvOnSurf, IwBSplineCurve*& UVCurve, bool trueCurveOnSurf=true);
	void			SetDisplayOutlineProfileRuler(bool flag) {m_displayOutlineProfileRuler=flag;};
	void			SetDisplayOutlineProfileBoth(bool flag) {m_displayOutlineProfileBoth=flag;};
	void			DetermineRulerCurves();
	void			DetermineBoxCutCurves();
	IwPoint3d		DetermineOutlineProfileWeightBearingMLCenter(IwTArray<IwPoint3d> *MProfilePoints=NULL, IwTArray<IwPoint3d> *LProfilePoints=NULL);
	void			GetRulerCurves(IwTArray<IwCurve*> &posCurves, IwTArray<IwCurve*> &negCurves);
	bool			ConvertSketchSurfaceUVtoXYZ(IwPoint3d& inputUVPnt, IwPoint3d& outputXYZPnt, IwVector3d& outputXYZNormal);
	bool			ConvertSketchSurfaceUVtoXYZ(IwTArray<IwPoint3d>& inputUVPnts, IwTArray<IwPoint3d>& outputXYZPnts);
	IwBSplineCurve* ConvertUVCurveToXYZCurve(IwBSplineCurve* UVCurve, bool trueCurveOnSurf=true);
	bool			GetNotchArcMiddlePoint(IwPoint3d& middlePnt, IwPoint2d& middleUVPnt);
	bool			GetPosteriorTipPoints(IwPoint3d& posTipPnt, IwPoint3d& negTipPnt);
	bool			GetAnteriorEarFeaturePoints(IwTArray<IwPoint3d>& antFeaturePoints);
	static bool		GetInitialAnteriorEarFeaturePoints(CTotalDoc* pDoc, IwTArray<IwPoint3d>& antFeaturePoints);
	void			SetEstimatedImplantMLSize(double value) {m_estimatedImplantMLSize=value;};
	double			GetEstimatedImplantMLSize();
	double			GetImplantMLSize(IwPoint3d* posPoint=NULL, IwPoint3d* negPoint=NULL);
	void			SetEstimatedNotchOutlineProfileFeatures(IwTArray<IwBSplineCurve*>& curves, IwTArray<IwPoint3d>& points);
	void			GetEstimatedNotchOutlineProfileFeatures(IwTArray<IwBSplineCurve*>& curves, IwTArray<IwPoint3d>& points);
	IwExtent2d		GetSketchSurfaceUVDomain();
	bool			GetCondylarProfileCenters(IwPoint3d& posCenter, IwPoint3d& negCenter);
	bool			GetNonRemapablePointNowIndexes(IwTArray<int>& indexes);
	IwBSplineCurve*	GetOutlineProfileXYZCurve() {return m_outlineProfileXYZCurve;};
	bool			MoveAnteriorProfile(IwPoint3d earIndentation);

	void			Set2DEditing(bool flag)				{	m_b2DEditing = flag;								};
	void			SetEver2DEditing()					{	m_bEver2DEditing = true;							};
	bool			GetEver2DEditing()					{	return m_bEver2DEditing;							};

	IwTArray<IwPoint3d> &Get2DDisplayPoints()			{	return m_outlineProfileXYZCurvePoints2D;			};
	IwTArray<IwPoint3d>	&GetFeaturePointsValidation()	{	return m_outlineProfileFeaturePointsValidation;		};
	bool			GetBoxCutReferencePoints(IwPoint3d& posRefPnt, IwPoint3d& negRefPnt);
	void			SetPostProfileAdjustDistance(double& posAdjustDist, double& negAdjustDist){m_posPostProfileAdjustDist=posAdjustDist;m_negPostProfileAdjustDist=negAdjustDist;};
	void			GetPostProfileAdjustDistance(double& posAdjustDist, double& negAdjustDist){posAdjustDist=m_posPostProfileAdjustDist;negAdjustDist=m_negPostProfileAdjustDist;};
	static bool		IsAsymmetricEarShaped(CTotalDoc* pDoc);

	//// For validation //////////////////////////////
	void			SetOutlineProfileFeaturePointsValidation(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo);
	void			GetOutlineProfileFeaturePointsValidation(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo);

private:
	void			SetOPModifiedFlag( bool bModified );
	void			DisplayOutlineProfileProjectedCurve();
	void			DisplayOutlineProfile();
	void			DisplayOutlineFeaturePoints();
	bool			CreateArcInterpolatingPoints(IwTArray<IwPoint3d>& featurePoints, int posIndex, IwTArray<IwPoint3d>& interPoints, IwPoint3d &arcStPnt, IwPoint3d &arcEdPnt);
	void			DisplayRulerCurves();
	void			DisplayRulerCurvesGLlist();
	void			DisplayBoxCut();
	void			DisplayBoxCutGLlist();

private:
	bool					m_b2DEditing;			// The current edit mode
	bool					m_bEver2DEditing;		// Has 2D edit mode ever been used. For record prupose
	bool					m_outlineProfileModified;// Geometric entities have not been updated yet to the screen. 
	bool					m_bOPDataModified;// Geometric entities have not been saved yet to the file. 
	bool					m_outlineProfileRulerModified;

	IwBSplineSurface*		m_sketchSurface;
	IwBSplineCurve*			m_uvCurve;							// for reference purpose
	IwBSplineCurve*			m_outlineProfileXYZCurve;			// mainly for display purpose
	IwTArray<IwPoint3d>		m_outlineProfileXYZCurvePoints;		// for display purpose
	IwTArray<IwPoint3d>		m_outlineProfileXYZCurvePoints2D;
	bool					m_displayOutlineProfileRuler;
	bool					m_displayOutlineProfileBoth;
	bool					m_displayBoxCut;
	IwTArray<IwPoint3d>		m_outlineProfileFeaturePoints;		// Although they are IwPoint3d points, only (x,y) are used to represent (U,V) on sketch surface
	IwTArray<IwPoint3d>		m_outlineProfileFeaturePointsRemap;	// (,,z) represents its reference point, (x,y,) represents the delta to its reference point.
																// if z=-1, it is a non remap-able point.
																// if z=-1.xxxx, it is a non remap-able point now, but can be converted into remap-able point. 
																// e.g., z=-1.1825 indicates it can be converted into remap-able point with reference point 18.25
																// if z=x.25, x.5, or x.75, it is an edge middle point
																// Only cut edge points and edge middle points have reference points and can be remapped.
	IwTArray<int>			m_outlineProfileFeaturePointsInfo;	// 0: interpolation point
																// 1: arc center point to define an CCW arc
																// 2: arc center point to define an CW arc
																// 3: arc start point
																// 4: arc end point
																// 11: collapsible arc center point to define an CCW arc
																// 12: collapsible arc center point to define an CW arc
																// the sequence in outline is 3-(1/2)-4 to describe an arc.

	long					m_glOutlineProfileCurveList;
	long					m_glOutlineProfileProjectedCurveList;
	long					m_glOutlineProfileRulerCurveList;
	long					m_glOutlineProfileBoxCutList;

	IwTArray<IwCurve*>		m_posRulerCurves;
	IwTArray<IwCurve*>		m_negRulerCurves;
	IwTArray<IwCurve*>		m_boxCutCurves;
	IwPoint3d				m_posBoxCutRefPoint;			// Box cut outer wall ref point;
	IwPoint3d				m_negBoxCutRefPoint;			// Box cut outer wall ref point;
	double					m_posPostProfileAdjustDist;		// The distance to adjust the outline profile (AP direction) to the cut profile such that the implant posterior vertical distance is [1.5 - 2.5].
	double					m_negPostProfileAdjustDist;		// See comments above.

	//// For validation //////////////////////////
	IwTArray<IwPoint3d>		m_outlineProfileFeaturePointsValidation;		// See comments above
	// We do not need the remap for validation
	IwTArray<int>			m_outlineProfileFeaturePointsInfoValidation;	// See comments above
	IwBSplineCurve*			m_outlineProfileXYZCurveValidation;				// mainly for display purpose

	/////////////////////////////////////////////////////////////////////////////////
	// The following data members are available only when run OnInitializeJOCSteps()
	/////////////////////////////////////////////////////////////////////////////////
	double					m_estimatedImplantMLSize;			// the estimated implant ML size
	IwTArray<IwBSplineCurve*>	m_estimatedNotchOutlineProfileCurves;	// the estimated outline profile near notch, 0: medial side, 1: lateral side.
	IwTArray<IwPoint3d>		m_estimatedNotchOutlineProfileFeaturePoints;	// 0: notch profile tip point, 1: medial 9.5/18mm point, 2: lateral 8.5/18mm point.


	IwTArray<IwTArray<IwPoint3d>>		m_arrOfArrOfIntPoints;	// (,,z) represents its reference point, (x,y,) represents the delta to its reference point.

};