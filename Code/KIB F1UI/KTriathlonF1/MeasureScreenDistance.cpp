#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "MeasureScreenDistance.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"

CMeasureScreenDistance::CMeasureScreenDistance( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMeasureScreenDistance";

	m_toolbar = new QToolBar("Measure Distance");

	QLabel* labelResultDistance = new QLabel(tr("Distance:"));
	labelResultDistance->setFont(m_fontBold);
	m_toolbar->addWidget(labelResultDistance);

	m_textMeasuredDistance = new QLineEdit(tr(" "));
	m_textMeasuredDistance->setReadOnly(true);
	m_textMeasuredDistance->setFixedWidth(62);
	m_textMeasuredDistance->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMeasuredDistance);

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	//m_toolbar->setMovable( false );	
	//m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	// Measure Distance here
	m_mouseMiddleDown = false;
	m_mouseLeftDown = false;
	m_mouseDownPoint = IwPoint3d();
	m_mouseUpPoint = IwPoint3d();

}

CMeasureScreenDistance::~CMeasureScreenDistance()
{
}

bool CMeasureScreenDistance::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseLeftDown = true;

	int uvw[3];
	uvw[0] = cursor.x();
	uvw[1] = cursor.y();
	uvw[2] = 0;
	IwPoint3d pnt;
	m_pView->UVWtoXYZ(uvw, pnt);
	m_mouseDownPoint = pnt;

	// Also initialize up point
	m_mouseUpPoint = IwPoint3d();

	m_pView->Redraw();

	return true;
}

bool CMeasureScreenDistance::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseMiddleDown = true;
	m_mouseDownPoint = IwPoint3d();
	m_mouseUpPoint = IwPoint3d();
	
	m_pView->Redraw();

	return true;
}
bool CMeasureScreenDistance::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_mouseLeftDown)
	{
		int uvw[3];
		uvw[0] = cursor.x();
		uvw[1] = cursor.y();
		uvw[2] = 0;
		IwPoint3d pnt;
		m_pView->UVWtoXYZ(uvw, pnt);
		m_mouseUpPoint = pnt;

		// calculate distance
		QString distText;
		double dist = m_mouseDownPoint.DistanceBetween(m_mouseUpPoint);
		distText.setNum(dist, 'f', 3);
		m_textMeasuredDistance->clear();
		m_textMeasuredDistance->setText(distText);

	}
	m_mouseLeftDown = false;
	m_mouseMiddleDown = false;

	m_pView->Redraw();


	return true;
}


bool CMeasureScreenDistance::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	if (m_mouseLeftDown)
	{
		int uvw[3];
		uvw[0] = cursor.x();
		uvw[1] = cursor.y();
		uvw[2] = 0;
		IwPoint3d pnt;
		m_pView->UVWtoXYZ(uvw, pnt);
		IwVector3d viewVec = m_pView->GetViewingVector();
		CBounds3d bounds = ((CTotalView*)m_pView)->GetDocBounds();
		double d = dist( bounds.pt0, bounds.pt1 );
		m_mouseUpPoint = pnt;

	}

	if ( m_mouseMiddleDown )
	{
		m_mouseDownPoint = IwPoint3d();
		m_mouseUpPoint = IwPoint3d();
	}

	m_pView->Redraw();

	return true;
}


bool CMeasureScreenDistance::MouseRight( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseDownPoint = IwPoint3d();
	m_mouseUpPoint = IwPoint3d();

	m_pView->Redraw();
	return true;
}

bool CMeasureScreenDistance::keyPressEvent( QKeyEvent* event, Viewport* viewport )
{
	if ( event->key() == Qt::Key_Escape )
		Reject();

	return false; // false: do not execute subsequent actions.
}

void CMeasureScreenDistance::Display(Viewport* vp)
{		
	glDisable( GL_LIGHTING );

	IwVector3d offset = m_pView->GetLayerOffsetVector(1);
	IwPoint3d pnt;
	glPointSize(5);
	// Display points
	if (m_mouseDownPoint.IsInitialized())
	{
		pnt = m_mouseDownPoint + offset;
		glColor3ub( 255, 255, 0 );
		glBegin( GL_POINTS );
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if (m_mouseUpPoint.IsInitialized())
	{
		pnt = m_mouseUpPoint + offset;
		glColor3ub( 255, 255, 0 );
		glBegin( GL_POINTS );
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	// Display dimension (perpendicular) lines 
	if ( m_mouseDownPoint.IsInitialized() &&
		 m_mouseUpPoint.IsInitialized() &&
		 m_mouseUpPoint != m_mouseDownPoint )
	{
		IwVector3d viewVec = m_pView->GetViewingVector();
		IwVector3d distVec = m_mouseUpPoint - m_mouseDownPoint;
		IwVector3d prepVec = viewVec*distVec;
		prepVec.Unitize();
		// Get the reference size for displaying the dimension lines
		int uv0[2] = {0,0};
		int uv1[2] = {1,0};
		IwPoint3d p0, p1, pVec;
		m_pView->UVtoXYZ(uv0, p0);
		m_pView->UVtoXYZ(uv1, p1);
		pVec = p0 - p1;
		double lineLength = 20*pVec.Length();
		// Determine the dimension line start/end points
		IwPoint3d upStart, upEnd, downStart, downEnd;
		downStart = m_mouseDownPoint + lineLength*prepVec;
		downEnd = m_mouseDownPoint - lineLength*prepVec;
		upStart = m_mouseUpPoint + lineLength*prepVec;
		upEnd = m_mouseUpPoint - lineLength*prepVec;
		glLineWidth( 1.0 );
		glBegin( GL_LINES );
			pnt = m_mouseDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = m_mouseUpPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
		glBegin( GL_LINES );
			pnt = downStart + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = downEnd + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
		glBegin( GL_LINES );
			pnt = upStart + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = upEnd + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();

		// calculate distance
		QString distStr;
		double dist = m_mouseDownPoint.DistanceBetween(m_mouseUpPoint);
		distStr.setNum(dist, 'f', 3);
		glColor3ub( 0, 0, 0 );
		QFont font( "Tahoma", 12);
		font.setBold(true);
		QString totalStr = QString(tr("  ")) + distStr;
		pnt = m_mouseUpPoint + offset;
		GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
	}


	glPointSize(1);
	glEnable( GL_LIGHTING );
}

void CMeasureScreenDistance::OnAccept()
{
	m_bDestroyMe = true;
	m_pView->Redraw();
}
