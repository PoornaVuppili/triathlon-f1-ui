#ifndef SELECTIMPLANTSIMPLE_H
#define SELECTIMPLANTSIMPLE_H

#include <QDialog>
#include <QDir>
#include <QMessageBox>
#include <QComboBox>
#include <QCheckBox>
#include <vector>
using namespace std;
namespace Ui {class SelectImplantSimpleClass;};

class SelectImplantSimple : public QDialog
{
    Q_OBJECT

public:
    SelectImplantSimple(QWidget *parent = 0);
    ~SelectImplantSimple();

    QString GetBasePath()        const { return basePath; }
    QString GetImplantName()     const { return implantName; }
    QString GetImplantFolder()   const { return caseFolderName; }
    QString GetImplantType()     const { return implantType; }
    QString GetKneeType()        const { return kneeType; }
    QString GetCompartmentType() const { return compartmentType; }
    QString GetCountry()         const { return country; }
    QString GetZone()            const { return zone; }
    QString GetCatalogNumber()   const { return catalogNumber; }
    QString GetCatalogNumberRev()const { return catalogNumberRev; }
    QString GetKitEDNumber()     const { return kitEDNumber; }
    QString GetKitEDNumberRev()  const { return kitEDNumberRev; }

    QString GetImgDir()         const { return imgDir; }
    QString GetSegmentDir()     const { return segDir; }
    QString GetSurfaceDir()     const { return surfDir; }
    QString GetSTLDir()         const { return stlDir; }
    QString GetSolidWorksDir()  const { return sldworksDir; }

    // Set the current implant name
    void SetImplantName(QString& name);

protected:
    void PopulateComboBox();

private:
    Ui::SelectImplantSimpleClass* ui;

    QString basePath, implantName, caseFolderName, imgDir, segDir, surfDir, stlDir, sldworksDir;
    QString kneeCor, kneeSag, kneeAxi, hipAxi, ankleAxi;
    QString implantType, kneeType, compartmentType;
    QString country, zone, catalogNumber, catalogNumberRev, kitEDNumber, kitEDNumberRev;

private slots:
    void on_pushButtonBrowse_clicked();
    void on_LineEdit_editingFinished();
    void on_comboImplantList_indexChanged(int);
    void on_pushButtonCancel_clicked();
    void on_pushButtonOpen_clicked();
};

#endif // SELECTIMPLANTSIMPLE_H
