/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../MainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[19];
    char stringdata0[212];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 5), // "About"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 11), // "ContextHelp"
QT_MOC_LITERAL(4, 30, 16), // "ReviewerFeedback"
QT_MOC_LITERAL(5, 47, 21), // "OnDockLocationChanged"
QT_MOC_LITERAL(6, 69, 18), // "Qt::DockWidgetArea"
QT_MOC_LITERAL(7, 88, 4), // "area"
QT_MOC_LITERAL(8, 93, 11), // "OpenImplant"
QT_MOC_LITERAL(9, 105, 11), // "SaveImplant"
QT_MOC_LITERAL(10, 117, 12), // "CloseImplant"
QT_MOC_LITERAL(11, 130, 7), // "confirm"
QT_MOC_LITERAL(12, 138, 13), // "CaptureScreen"
QT_MOC_LITERAL(13, 152, 4), // "Undo"
QT_MOC_LITERAL(14, 157, 4), // "Redo"
QT_MOC_LITERAL(15, 162, 15), // "ControlViewport"
QT_MOC_LITERAL(16, 178, 9), // "SetOption"
QT_MOC_LITERAL(17, 188, 14), // "TriggerMeasure"
QT_MOC_LITERAL(18, 203, 8) // "QAction*"

    },
    "MainWindow\0About\0\0ContextHelp\0"
    "ReviewerFeedback\0OnDockLocationChanged\0"
    "Qt::DockWidgetArea\0area\0OpenImplant\0"
    "SaveImplant\0CloseImplant\0confirm\0"
    "CaptureScreen\0Undo\0Redo\0ControlViewport\0"
    "SetOption\0TriggerMeasure\0QAction*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x09 /* Protected */,
       3,    0,   85,    2, 0x09 /* Protected */,
       4,    0,   86,    2, 0x09 /* Protected */,
       5,    1,   87,    2, 0x09 /* Protected */,
       8,    0,   90,    2, 0x09 /* Protected */,
       9,    0,   91,    2, 0x09 /* Protected */,
      10,    1,   92,    2, 0x09 /* Protected */,
      10,    0,   95,    2, 0x29 /* Protected | MethodCloned */,
      12,    0,   96,    2, 0x09 /* Protected */,
      13,    0,   97,    2, 0x09 /* Protected */,
      14,    0,   98,    2, 0x09 /* Protected */,
      15,    0,   99,    2, 0x09 /* Protected */,
      16,    0,  100,    2, 0x09 /* Protected */,
      17,    1,  101,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 18,    2,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->About(); break;
        case 1: _t->ContextHelp(); break;
        case 2: _t->ReviewerFeedback(); break;
        case 3: _t->OnDockLocationChanged((*reinterpret_cast< Qt::DockWidgetArea(*)>(_a[1]))); break;
        case 4: _t->OpenImplant(); break;
        case 5: _t->SaveImplant(); break;
        case 6: _t->CloseImplant((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->CloseImplant(); break;
        case 8: _t->CaptureScreen(); break;
        case 9: _t->Undo(); break;
        case 10: _t->Redo(); break;
        case 11: _t->ControlViewport(); break;
        case 12: _t->SetOption(); break;
        case 13: _t->TriggerMeasure((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
