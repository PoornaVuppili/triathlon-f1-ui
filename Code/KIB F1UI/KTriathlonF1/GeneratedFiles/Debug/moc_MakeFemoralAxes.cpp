/****************************************************************************
** Meta object code from reading C++ file 'MakeFemoralAxes.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../MakeFemoralAxes.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MakeFemoralAxes.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CMakeFemoralAxes_t {
    QByteArrayData data[11];
    char stringdata0[135];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CMakeFemoralAxes_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CMakeFemoralAxes_t qt_meta_stringdata_CMakeFemoralAxes = {
    {
QT_MOC_LITERAL(0, 0, 16), // "CMakeFemoralAxes"
QT_MOC_LITERAL(1, 17, 19), // "SwitchToCoordSystem"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 4), // "name"
QT_MOC_LITERAL(4, 43, 8), // "OnAccept"
QT_MOC_LITERAL(5, 52, 8), // "OnCancel"
QT_MOC_LITERAL(6, 61, 11), // "OnRotateCCW"
QT_MOC_LITERAL(7, 73, 10), // "OnRotateCW"
QT_MOC_LITERAL(8, 84, 12), // "OnReviewNext"
QT_MOC_LITERAL(9, 97, 14), // "OnReviewRework"
QT_MOC_LITERAL(10, 112, 22) // "OnReviewPredefinedView"

    },
    "CMakeFemoralAxes\0SwitchToCoordSystem\0"
    "\0name\0OnAccept\0OnCancel\0OnRotateCCW\0"
    "OnRotateCW\0OnReviewNext\0OnReviewRework\0"
    "OnReviewPredefinedView"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CMakeFemoralAxes[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   57,    2, 0x08 /* Private */,
       5,    0,   58,    2, 0x08 /* Private */,
       6,    0,   59,    2, 0x08 /* Private */,
       7,    0,   60,    2, 0x08 /* Private */,
       8,    0,   61,    2, 0x08 /* Private */,
       9,    0,   62,    2, 0x08 /* Private */,
      10,    0,   63,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Void,

       0        // eod
};

void CMakeFemoralAxes::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CMakeFemoralAxes *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SwitchToCoordSystem((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->OnAccept(); break;
        case 2: _t->OnCancel(); break;
        case 3: _t->OnRotateCCW(); break;
        case 4: _t->OnRotateCW(); break;
        case 5: { bool _r = _t->OnReviewNext();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->OnReviewRework();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: _t->OnReviewPredefinedView(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CMakeFemoralAxes::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CMakeFemoralAxes::SwitchToCoordSystem)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CMakeFemoralAxes::staticMetaObject = { {
    QMetaObject::SuperData::link<CManager::staticMetaObject>(),
    qt_meta_stringdata_CMakeFemoralAxes.data,
    qt_meta_data_CMakeFemoralAxes,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CMakeFemoralAxes::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CMakeFemoralAxes::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CMakeFemoralAxes.stringdata0))
        return static_cast<void*>(this);
    return CManager::qt_metacast(_clname);
}

int CMakeFemoralAxes::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CManager::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void CMakeFemoralAxes::SwitchToCoordSystem(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
