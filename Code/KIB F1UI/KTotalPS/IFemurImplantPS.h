#pragma once
#include "EntityForTwin.h"
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include <QtGui>

class IFemurImplantPS
{
public:
	IFemurImplantPS();
	~IFemurImplantPS();

	static bool MotionPatternsExtendToolbar();
	static bool IsFemoralSidePSFeaturesComplete();

private:
	static void	CloseImplantImmediately(QString& errorIFunctionName);
	

private:

};