#ifndef KINPUTVALUESLIDER_H
#define KINPUTVALUESLIDER_H

// A dialog for entering a user-adjustable value with a slider. It can be
// used, for example, to enter the slice number.
// Other than the slider, the dialog also contains a label for the slider, a spin box,
// and OK + Cancel buttons. 

#include <QDialog>

class Ui_InputValueSliderClass;

class KInputValueSlider : public QDialog
{
    Q_OBJECT

public:
    KInputValueSlider(QWidget *parent = 0);
    ~KInputValueSlider();

    void SetLabelText(QString& str);
    void SetRange(int min, int max);
    void InitValue(int v);
    void SetValue(int v);
    int GetValue();

protected slots:
    void onSliderChange(int v);
    void onSpinBoxChange(int v);

signals:
    void valueChanged(int);

private:
    Ui_InputValueSliderClass* ui;
    int initValue;

private slots:
    void on_pushButtonCancel_clicked();
    void on_pushButtonOK_clicked();
};

#endif // KINPUTVALUESLIDER_H
