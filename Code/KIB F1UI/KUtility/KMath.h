#ifndef KMATH_H
#define KMATH_H

// Math, vector, matrix, geometry functions
#include "TVector2.h"
#include "TVector3.h"
#include "TBox3.h"
#include "TRigid3.h"
#include "TArc3.h"
#include "TIntersect.h"
#include "TPickRay.h"
#include "TTessTriangle.h"
#include "TPointSet.h"
#include "TPointSet3.h"
#include "TContour.h"
#include "TOctreeTriangle.h"

#endif //KMATH_H
