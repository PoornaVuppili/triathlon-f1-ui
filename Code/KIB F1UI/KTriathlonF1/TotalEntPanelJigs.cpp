#include "TotalEntPanelJigs.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "AdvancedControl.h"
#include "FemoralAxes.h"
#include "SolidPositionJigs.h"
#include "..\KApp\EntityWidget.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\MainWindow.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Utilities.h"
#include <assert.h>
#include "../KTriathlonF1/IFemurImplant.h"

CTotalEntPanelJigs::CTotalEntPanelJigs(QTreeView* view) : CEntPanel(view)
{
}

CTotalEntPanelJigs::~CTotalEntPanelJigs()
{
}

void CTotalEntPanelJigs::Add( int id, const QString& sName, CEntRole eRole )
{
    // create the 'Fem Jigs' root item
    if( items.size()==0 )
    {
        QStandardItem* item = NewItem("Femoral Jigs", false, false);

        QFont font = item->font();
        font.setBold(true);
        font.setItalic(true);
        item->setFont(font);

        items.append( item );

		if ( !entityWidget->IsItemExist(this) )
			entityWidget->AddItem( this );
    }

	CPanelElem		el;

	el.id			= id;
	el.eEntRole		= eRole;
	el.sEntName		= sName;

	m_list.append( el );

	QStandardItem*		itemId = NewItem("", true, false, true);

	CEntity*	pEnt = m_pDoc->GetEntityById( id );

	if ( pEnt )
		itemId->setFlags( Qt::ItemIsEnabled );
	else
		itemId->setFlags( Qt::NoItemFlags );

	QString sEntName = sName;
	if ( pEnt && !pEnt->GetUpToDateStatus())
	{
		sEntName = "*" + sEntName;// Add "*" if entity is out of date
	}

	itemId->setData( sEntName, 0 );
	itemId->setFlags( Qt::ItemIsEnabled );
	itemId->setSelectable(false);

	if (eRole == ENT_ROLE_STYLUS_JIGS ||
		eRole == ENT_ROLE_SOLID_POSITION_JIGS ||
		eRole == ENT_ROLE_OUTLINE_PROFILE_JIGS || 
		eRole == ENT_ROLE_CARTILAGE_SURFACE)
	{
		QFont font = itemId->font();
		font.setBold(true);
		itemId->setFont(font);
		QString message;
		CEntValidateStatus validateStatus = pEnt->GetValidateStatus(message);
		if ( !message.isEmpty() )
			itemId->setToolTip(message);
		if ( validateStatus == VALIDATE_STATUS_RED )// red
			itemId->setForeground( QBrush( QColor(darkRed[0],darkRed[1],darkRed[2]) ) );
		else if ( validateStatus == VALIDATE_STATUS_YELLOW ) // yellow
			itemId->setForeground( QBrush(QColor(darkYellow[0],darkYellow[1],darkYellow[2])) ); 
		else if ( validateStatus == VALIDATE_STATUS_GREEN ) // green
			itemId->setForeground( QBrush(QColor(darkGreen[0],darkGreen[1],darkGreen[2])) );
		else	// black
			itemId->setForeground( QBrush(QColor(black[0],black[1],black[2])) );		
	}

	if(pEnt && pEnt->Has2ndItem())
		items[0]->appendRow( QList<QStandardItem*>() << itemId << pEnt->Get2ndItem());
	else
		items[0]->appendRow( QList<QStandardItem*>() << itemId << NewItem(""));

	if ( pEnt )
	{
		if( pEnt->GetDisplayMode() != DISP_MODE_NONE && pEnt->GetDisplayMode() != DISP_MODE_HIDDEN )
			itemId->setCheckState( Qt::Checked );
		else
			itemId->setCheckState( Qt::Unchecked );
	}

}

void CTotalEntPanelJigs::OnItemClicked(const QModelIndex& index)
{
    if( items[0]->index() == index )
        return; // root node

    // sub node
    int row = index.row();
    assert( row >= 0 && row < m_list.size() );

	ItemClicked( index );
}

void CTotalEntPanelJigs::OnItemRightClicked(const QModelIndex& index)
{
    if( items[0]->index() == index )
        return; // root node

    // sub node
    int row = index.row();
    assert( row >= 0 && row < m_list.size() );

	m_id = m_list[row].id;
	CEntity*	pEntity = m_pDoc->GetEntityById( m_id );
	if ( pEntity == NULL )
		return;

	// the items for femoral CR implant and jigs
	if ( pEntity->GetEntRole() >= ENT_ROLE_JIGS_NONE &&
		 pEntity->GetEntRole() <= ENT_ROLE_JIGS_END )
	{
		DisplayContextMenuJigs( row );
	}
}

void CTotalEntPanelJigs::OnItemDoubleClicked(const QModelIndex& index)
{
}

void CTotalEntPanelJigs::DisplayContextMenuJigs( int row ) 
{
	// initial data
	m_actDisplay = NULL;
	m_actRedefine = NULL;
	m_actRegenerateAStep = NULL;
	m_actRegenerateMultipleSteps = NULL;
	m_actRetessellate = NULL;

	CEntity*	pEntity;

	m_id = m_list[row].id;
	pEntity = m_pDoc->GetEntityById( m_id );

	if ( pEntity == NULL )
		return;

	// skip if the item is not for femoral jigs
	if ( pEntity->GetEntRole() < ENT_ROLE_JIGS_NONE ||
		 pEntity->GetEntRole() > ENT_ROLE_JIGS_END )
		return;

	QMenu		Menu;
	QMenu*		pMenuDisplayMode( &Menu );

	// Check whether any manager is active.
	bool managerExists = false;
	if ( m_pDoc->GetView()->GetManager() || m_pDoc->GetView()->GetGlobalManager() )
		managerExists = true;

	// Use environment variable to activate ExportIGES
	bool enableExportIGES = false;
	char *envExportIGES = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (envExportIGES != NULL && QString("1") == envExportIGES)
	{
		enableExportIGES = true;
	}

	switch( pEntity->GetEntType() )
	{
		case ENT_TYPE_PART:
		case ENT_TYPE_FEMORAL_PART:
		{
			if( pEntity->GetEntRole() == ENT_ROLE_OSTEOPHYTE_SURFACE ||
				pEntity->GetEntRole() == ENT_ROLE_CARTILAGE_SURFACE ||
				pEntity->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_INNER_SURFACE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_OUTER_SURFACE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_SIDE_SURFACE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_SOLID_POSITION_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_STYLUS_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_ANTERIOR_SURFACE_JIGS )
			{
				if ( !managerExists )
				{
					m_actRedefine		= Menu.addAction( "Edit" );
					connect( m_actRedefine,		SIGNAL( triggered() ),	this, SLOT( OnRedefine() ) );
				}
			}
			if( pEntity->GetEntRole() == ENT_ROLE_CARTILAGE_SURFACE ||
				pEntity->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_INNER_SURFACE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_OUTER_SURFACE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_SIDE_SURFACE_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_SOLID_POSITION_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_STYLUS_JIGS ||
				pEntity->GetEntRole() == ENT_ROLE_ANTERIOR_SURFACE_JIGS )
			{
				if ( !managerExists )
				{
					m_actRegenerateAStep	= Menu.addAction( "Regen Itself" );
					connect( m_actRegenerateAStep,		SIGNAL( triggered() ),	this, SLOT( OnRegenerateAStep() ) );

					// if itself is update-to-date or the previous one is out-of-date
					if ( pEntity->GetUpToDateStatus() || !m_pDoc->GetEntity( (CEntRole)(pEntity->GetEntRole()-1) )->GetUpToDateStatus() )
					{
						m_actRegenerateAStep->setEnabled(false);
					}

					m_actRegenerateMultipleSteps	= Menu.addAction( "Regen Through" );
					connect( m_actRegenerateMultipleSteps,		SIGNAL( triggered() ),	this, SLOT( OnRegenerateMultipleSteps() ) );
					// if itself is update-to-date or the previous one is out-of-date
					if ( pEntity->GetUpToDateStatus() ||
						!m_pDoc->GetEntity( (CEntRole)(pEntity->GetEntRole()-1) )->GetUpToDateStatus())
					{
						m_actRegenerateMultipleSteps->setEnabled(false);
					}				
				}
			}

			pMenuDisplayMode	= Menu.addMenu( "Display Mode" );
			m_actDisplay		= pMenuDisplayMode->addAction( "Display" );
			m_actTransparency	= pMenuDisplayMode->addAction( "Transparency" );
			m_actWireframe		= pMenuDisplayMode->addAction( "Wireframe" );

			m_actDisplay->setCheckable( true );
			m_actTransparency->setCheckable( true );
			m_actWireframe->setCheckable( true );

			// Only these 5 entities can display iso curves. (and femur)
			if ( pEntity->GetEntRole() == ENT_ROLE_OSTEOPHYTE_SURFACE ||
				 pEntity->GetEntRole() == ENT_ROLE_CARTILAGE_SURFACE ||
				 pEntity->GetEntRole() == ENT_ROLE_INNER_SURFACE_JIGS ||
				 pEntity->GetEntRole() == ENT_ROLE_OUTER_SURFACE_JIGS ||
				 pEntity->GetEntRole() == ENT_ROLE_ANTERIOR_SURFACE_JIGS )
			{
				m_actDisplayNet			= pMenuDisplayMode->addAction( "Display Net" );
				m_actTransparencyNet	= pMenuDisplayMode->addAction( "Transparency Net" );
				m_actDisplayNet->setCheckable( true );
				m_actTransparencyNet->setCheckable( true );
  				connect( m_actDisplayNet,		SIGNAL( triggered() ),	this, SLOT( OnDisplayNet() ) );
  				connect( m_actTransparencyNet,	SIGNAL( triggered() ),	this, SLOT( OnTransparencyNet() ) );
			}

			m_actHide			= pMenuDisplayMode->addAction( "Hide" );
			m_actHide->setCheckable( true );

			switch( pEntity->GetDisplayMode() )
			{
				case DISP_MODE_DISPLAY:			m_actDisplay->setChecked( true );		break;
				case DISP_MODE_DISPLAY_NET:		m_actDisplayNet->setChecked( true );	break;
				case DISP_MODE_TRANSPARENCY:	m_actTransparency->setChecked( true );	break;
				case DISP_MODE_TRANSPARENCY_NET:m_actTransparencyNet->setChecked( true );break;
				case DISP_MODE_WIREFRAME:		m_actWireframe->setChecked( true );		break;
				case DISP_MODE_HIDDEN:			m_actHide->setChecked( true );			break;
			}

  			connect( m_actDisplay,		SIGNAL( triggered() ),	this, SLOT( OnDisplay() ) );
  			connect( m_actTransparency,	SIGNAL( triggered() ),	this, SLOT( OnTransparency() ) );
  			connect( m_actWireframe,	SIGNAL( triggered() ),	this, SLOT( OnWireframe() ) );
  			connect( m_actHide,			SIGNAL( triggered() ),	this, SLOT( OnHide() ) );

			m_actColor			= Menu.addAction( "Color" );
  			connect( m_actColor,		SIGNAL( triggered() ),	this, SLOT( OnColor() ) );

			if ( !managerExists )
			{
				m_actRetessellate		= Menu.addAction( "Retessellate" );
				connect( m_actRetessellate,		SIGNAL( triggered() ),	this, SLOT( OnRetessellate() ) );
			}

			if (enableExportIGES)
			{
				m_actExportStl		= Menu.addAction( "Export as STL" );
  				connect( m_actExportStl,	SIGNAL( triggered() ),	this, SLOT( OnExportStl() ) );
			}

			if (!managerExists && pEntity->GetEntRole() == ENT_ROLE_JIGS_NONE)
			{
				m_actDelete		= Menu.addAction( "Delete" );
				connect( m_actDelete,		SIGNAL( triggered() ),	this, SLOT( OnDelete() ) );
			}

			break;
		}

	}
  
	// Open, Regenerate depend on the the edit/review mode
	if ( mainWindow->IsReviewMode() )
	{
		// disable all entities belong to jigs design
		if ( m_actRedefine )
			m_actRedefine->setEnabled(false);
		if ( m_actRegenerateAStep )
			m_actRegenerateAStep->setEnabled(false);
		if ( m_actRegenerateMultipleSteps ) 
			m_actRegenerateMultipleSteps->setEnabled(false);
	}

	QPoint		pos = QCursor::pos();

	pos.setY( pos.y() + 10 );

	Menu.exec( pos );
}

void CTotalEntPanelJigs::OnDeleteAllJigs()
{
	m_pDoc->AppendLog( QString("CTotalEntPanelJigs::OnDeleteAllJigs()") );

	int		button;
	button = QMessageBox::warning( NULL, 
									tr("Warning Message!"), 
									tr("All Jigs steps will be deleted.\nDo you want to proceed deletion?"),
									QMessageBox::Yes, QMessageBox::No);	

	if ( button == QMessageBox::No ) return;

	GetTotalDoc()->DeleteEntitiesBelongToJigs();

	m_pDoc->GetView()->Redraw();

}

void CTotalEntPanelJigs::OnRedefine()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	CTotalView*	pView = (CTotalView*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("CTotalEntPanelJigs::OnRedefine(), entity name: %1").arg(pEnt->GetEntName()) );

	switch( eEntRole )
	{
		case ENT_ROLE_OSTEOPHYTE_SURFACE:
		{
			pView->OnImportOsteophyteSurface();
			break;
		}
		case ENT_ROLE_CARTILAGE_SURFACE:
		{
			pView->OnDefineCartilage();
			break;
		}
		case ENT_ROLE_OUTLINE_PROFILE_JIGS:
		{
			pView->OnDefineOutlineProfileJigs();
			break;
		}
		case ENT_ROLE_INNER_SURFACE_JIGS:
		{
			pView->OnMakeInnerSurfaceJigs();
			break;
		}
		case ENT_ROLE_OUTER_SURFACE_JIGS:
		{
			pView->OnMakeOuterSurfaceJigs();
			break;
		}
		case ENT_ROLE_SIDE_SURFACE_JIGS:
		{
			pView->OnMakeSideSurfaceJigs();
			break;
		}
		case ENT_ROLE_SOLID_POSITION_JIGS:
		{
			pView->OnMakeSolidPositionJigs();
			break;
		}
		case ENT_ROLE_STYLUS_JIGS:
		{
			pView->OnDefineStylusJigs();
			break;
		}
		case ENT_ROLE_ANTERIOR_SURFACE_JIGS:
		{
			pView->OnMakeAnteriorSurfaceJigs();
			break;
		}
	}
}

void CTotalEntPanelJigs::OnRegenerateAStep()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	CTotalView*	pView = (CTotalView*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("CTotalEntPanelJigs::OnRegenerateAStep(), entity name: %1").arg(pEnt->GetEntName()) );

	switch( eEntRole )
		{
		case ENT_ROLE_CARTILAGE_SURFACE:
			{
				pView->OnDefineCartilage( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_OUTLINE_PROFILE_JIGS:
			{
				pView->OnDefineOutlineProfileJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_INNER_SURFACE_JIGS:
			{
				pView->OnMakeInnerSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_OUTER_SURFACE_JIGS:
			{
				pView->OnMakeOuterSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_SIDE_SURFACE_JIGS:
			{
				pView->OnMakeSideSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_SOLID_POSITION_JIGS:
			{
				pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_STYLUS_JIGS:
			{
				pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}
		case ENT_ROLE_ANTERIOR_SURFACE_JIGS:
			{
				pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				break;
			}

		}

}

void CTotalEntPanelJigs::OnRegenerateMultipleSteps()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	CTotalView*	pView = (CTotalView*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("CTotalEntPanelJigs::OnRegenerateMultipleSteps(), entity name: %1").arg(pEnt->GetEntName()) );

	bool regenerateOnly = true;

	switch( eEntRole )
		{
		// jigs
		case ENT_ROLE_CARTILAGE_SURFACE:
			{
				pView->OnDefineCartilage( MAN_ACT_TYPE_REGENERATE );
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS ) )
					pView->OnDefineOutlineProfileJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS ) )
					pView->OnMakeInnerSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS ) )
					pView->OnMakeOuterSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS ) )
					pView->OnMakeSideSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS ) )
					pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				// check solid positioning jig exists or not
				if ( GetTotalDoc()->GetSolidPositionJigs() && !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
					break;
				if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
					pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_OUTLINE_PROFILE_JIGS:
			{
				pView->OnDefineOutlineProfileJigs( MAN_ACT_TYPE_REGENERATE );
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS ) )
					pView->OnMakeInnerSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS ) )
					pView->OnMakeOuterSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS ) )
					pView->OnMakeSideSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS ) )
					pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				// check solid positioning jig exists or not
				if ( GetTotalDoc()->GetSolidPositionJigs() && !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
					break;
				if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
					pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_INNER_SURFACE_JIGS:
			{
				pView->OnMakeInnerSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS ) )
					pView->OnMakeOuterSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS ) )
					pView->OnMakeSideSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS ) )
					pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				// check solid positioning jig exists or not
				if ( GetTotalDoc()->GetSolidPositionJigs() && !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
					break;
				if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
					pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_OUTER_SURFACE_JIGS:
			{
				pView->OnMakeOuterSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS ) )
					pView->OnMakeSideSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS ) )
					pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				// check solid positioning jig exists or not
				if ( GetTotalDoc()->GetSolidPositionJigs() && !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
					break;
				if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
					pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_SIDE_SURFACE_JIGS:
			{
				pView->OnMakeSideSurfaceJigs( MAN_ACT_TYPE_REGENERATE );
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS ) )
					pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				// check solid positioning jig exists or not
				if ( GetTotalDoc()->GetSolidPositionJigs() && !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
					break;
				if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
					pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_SOLID_POSITION_JIGS:
			{
				pView->OnMakeSolidPositionJigs( MAN_ACT_TYPE_REGENERATE );
				// check solid positioning jig exists or not
				if ( GetTotalDoc()->GetSolidPositionJigs() && !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
					break;
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
					pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_STYLUS_JIGS:
			{
				pView->OnDefineStylusJigs( MAN_ACT_TYPE_REGENERATE );
				// automatically regenerate next entities
				if ( m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS ) )
					pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		case ENT_ROLE_ANTERIOR_SURFACE_JIGS:
			{
				pView->OnMakeAnteriorSurfaceJigs( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		}

}

void CTotalEntPanelJigs::OnExportIges()
{
	// Output to original CT scanner coordinate system
	if ( !GetTotalDoc()->GetAdvancedControl()->GetOutputNormalized() &&
		 !GetTotalDoc()->GetAdvancedControl()->GetOutputWithTransformation() )
	{
		CEntPanel::OnExportIges();
	}
	else
	{
		QString					sFileTypes = "IGES files (*.igs);;";
		QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
		QString					sFileName, sDir;
		CPart*					pPart;
		QFileDialog				Dlg;

		sDir = m_pDoc->GetFileFolder();

		sFileName = Dlg.getSaveFileName( NULL, "Export as IGES", sDir, sFileTypes, 0, options  );

		if( sFileName.isNull() )
			return;

		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";

		sDir = GetFileDir( sFileName );

		pPart = m_pDoc->GetPartById( m_id );

		m_pDoc->AppendLog( QString("CEntPanel::OnExportIges(), entity name: %1").arg(pPart->GetEntName()) );

		CBrepArray				BrepTrimmedSurfArray;
		char					filename[2048];
		CPoint3d				pt;

		QStringToCharArray( sFileName, filename );

		QApplication::setOverrideCursor( Qt::WaitCursor );

		// Output normalized
		if (GetTotalDoc()->GetAdvancedControl()->GetOutputNormalized())
		{
			if (pPart->GetIwBrep() && GetTotalDoc()->GetFemoralAxes())
			{
				IwAxis2Placement mechAxis, invertMatrix;
				GetTotalDoc()->GetFemoralAxes()->GetMechanicalAxes(mechAxis);
				mechAxis.Invert(invertMatrix);
				double refSize = 76.0/GetTotalDoc()->GetFemoralAxes()->GetEpiCondyleSize();
				IwVector3d scale = IwVector3d(refSize,refSize,refSize);
				IwBrep* brepCopy = new (m_pDoc->GetIwContext()) IwBrep(*pPart->GetIwBrep());// make a copy
				brepCopy->Transform(invertMatrix);// inverse
				brepCopy->Transform(IwAxis2Placement(), &scale); // scale
				BrepTrimmedSurfArray.Add( brepCopy );
				// Regardless the m_pBrep is a solid Brep or a trimmed shell,
				// Save it as trimmed surfaces.
				WriteIges( filename, NULL, NULL, NULL, &BrepTrimmedSurfArray, NULL );

				if ( brepCopy )
					IwObjDelete(brepCopy);
			}
		}
		// Output to Motion Studio transformation coordinates
		else if (GetTotalDoc()->GetAdvancedControl()->GetOutputWithTransformation())
		{
			IwAxis2Placement transfMatrix ;
			IwBrep* brepCopy = new (m_pDoc->GetIwContext()) IwBrep(*pPart->GetIwBrep());// make a copy
			brepCopy->Transform(transfMatrix);
			BrepTrimmedSurfArray.Add( brepCopy );
			// Regardless the m_pBrep is a solid Brep or a trimmed shell,
			// Save it as trimmed surfaces.
			WriteIges( filename, NULL, NULL, NULL, &BrepTrimmedSurfArray, NULL );

			if ( brepCopy )
				IwObjDelete(brepCopy);
		}

		QApplication::restoreOverrideCursor();
	}

}


void CTotalEntPanelJigs::OnDelete()
{

	m_pDoc->AppendLog( QString("CTotalEntPanelJigs::OnDelete()") );

	CEntPanel::OnDelete(); // Will delete from m_EntList

	this->DeleteById( m_id ); // Will remove from entity panel
	
	m_pDoc->GetView()->Redraw();
}