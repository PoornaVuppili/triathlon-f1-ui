#include "GL\glew.h" //glew.h must be included before gl.h
//#include "glew.h"
#include <QApplication>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <exception>
using namespace std;

#include "../KUtility/KApplicationMode.h"
#include "Version.h"
#include "MainWindowPS.h"
#include "..\KApp\SwitchBoard.h"

#include "ImplantTotalPS.h"

#include <Windows.h>
#include <tchar.h>
#include <Strsafe.h>

#include <dbghelp.h>
#include <shellapi.h>
#include <shlobj.h>

DWORD GetExePath(LPTSTR szPath, DWORD nSize)
{
   DWORD nLen = GetModuleFileName(GetModuleHandle(NULL), szPath, nSize);
   // find the last backslash and put a zero after it
   for (int ic = nLen; ic --> 0; )
   {
      if (szPath[ic] == '\\')
      {
         szPath[ic + 1] = _T(0);
         return ic;
      }
   }
   return nLen;
}

#ifdef TESTING_MODE
struct RunningIndicator
{
    RunningIndicator()
        : theFile(INVALID_HANDLE_VALUE)
    {
        // try creating a file '<username>'.0.running
        // in the directory of the executable
        TCHAR filepath[1024] = {};
        DWORD nn = GetExePath(filepath, 1023);

        QString username;
        char *value;
        size_t len;
        if( _dupenv_s( &value, &len, "USERNAME" ) == 0 )
        {
            username = value;
            free(value);
        }

		char acComputerName[100];
		DWORD nComputerName = sizeof(acComputerName);
		GetComputerName(acComputerName, &nComputerName);
		QString computerName = QString(acComputerName);

        // look for files named '<username>.*' in the exepath
        QDir ddd(filepath);
        QStringList filters;
        filters << username + "_*_.running";
        ddd.setNameFilters(filters);

        QStringList names = ddd.entryList();
        int theNumber = 0;
        if (!names.empty())
        {
            // let's open the one past the last one
            QStringList parts = names.back().split('_');
            if (parts.length() > 1)
                theNumber = parts[1].toInt() + 1;
        }

        QString path(QString("%1%2At%3_%4_.running").arg(filepath).arg(username).arg(computerName).arg(theNumber));

        HANDLE hFile = CreateFile( path.toUtf8(),
            GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ,// allow subsequent read
            NULL,           // no security attributes
            CREATE_ALWAYS,  // creating a new file
            0,              // not overlapped index/O
            NULL);

        OVERLAPPED ov;
        ov.Offset = 0;
        ov.OffsetHigh = 0;

        if (hFile != INVALID_HANDLE_VALUE
            && LockFileEx(hFile, LOCKFILE_FAIL_IMMEDIATELY, 0, 0, 100, &ov))
        {
            theName = path;
            theFile = hFile;
        }
    }

    ~RunningIndicator()
    {
        OVERLAPPED ov;
        ov.Offset = 0;
        ov.OffsetHigh = 0;

        if (theFile != INVALID_HANDLE_VALUE
            && QFile::exists(theName)
            && UnlockFileEx(theFile, 0, 0, 100, &ov))
        {
            CloseHandle(theFile);
            DeleteFile(theName.toUtf8());
        }
    }

    QString theName;
    HANDLE theFile;
};
#endif

QString MakeCrashDumpFilename(QString const& casenum, QString const& username, SYSTEMTIME const& now)
{
   TCHAR szFileName[MAX_PATH+1];
   StringCchPrintf(szFileName, MAX_PATH, _T("%s-%s-%04d%02d%02d-%02d%02d%02d"),
      static_cast<const char*>(casenum.toUtf8()),
      static_cast<const char*>(username.toUtf8()),
      now.wYear, now.wMonth, now.wDay,
      now.wHour, now.wMinute, now.wSecond);

   return szFileName;
}

HANDLE GetCrashDumpFile(Implant *pImp)
{
   const TCHAR* const szAppName = _T(PRODUCT_STR);
   const TCHAR* const szVersion = _T(VERSION_STR);

   TCHAR szFileName[MAX_PATH+1];

   SYSTEMTIME now;
   GetLocalTime(&now);

   TCHAR szUserName[MAX_PATH+1];
   DWORD usernameLen = MAX_PATH;
   GetUserName(szUserName, &usernameLen);

   bool usingInCaseFolder = false;

   // first let's see if we have a case currently open
   if (pImp && pImp->HasImplant())
   {
      QString casenum = pImp->GetInfo("ImplantName");
      QString casedir = pImp->GetInfo("ImplantDir");

      StringCchPrintf(szFileName, MAX_PATH, _T("%s"), static_cast<const char*>(casedir.toUtf8()));
      CreateDirectory(szFileName, NULL); // should be no need...

      // try creating in the case folder
      usingInCaseFolder = true;
      StringCchPrintf(szFileName, MAX_PATH, _T("%s\\%s.dmp"),
         static_cast<const char*>(casedir.toUtf8()),
         static_cast<const char*>( MakeCrashDumpFilename(pImp->GetInfo("ImplantName"), szUserName, now).toUtf8() ));
   }
   else // no implant -- put it in the temp folder
   {
      TCHAR szTempPath[MAX_PATH+1];
      DWORD dwBufferSize(MAX_PATH);
      dwBufferSize = GetTempPath(dwBufferSize, szTempPath);

      StringCchPrintf(szFileName, MAX_PATH, _T("%s%s"), szTempPath, szAppName);
      CreateDirectory(szFileName, NULL);

      StringCchPrintf(szFileName, MAX_PATH, _T("%s%s\\%s.dmp"),
         szTempPath, szAppName,
         static_cast<const char*>( MakeCrashDumpFilename("no-implant", szUserName, now).toUtf8() ));
   }

   HANDLE hDumpFile = CreateFile(szFileName, GENERIC_READ|GENERIC_WRITE, 
      FILE_SHARE_WRITE|FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
   return hDumpFile;
}

#pragma comment(lib, "dbghelp")
int GenerateDump(EXCEPTION_POINTERS* pExceptionPointers)
{
   const TCHAR* const szAppName = _T(PRODUCT_STR);
   const TCHAR* const szVersion = _T(VERSION_STR);

   DWORD dwBufferSize = MAX_PATH+1;

   SYSTEMTIME stLocalTime;
   GetLocalTime(&stLocalTime);

   HANDLE hDumpFile = (mainWindow && mainWindow->GetImplant()) ?
      GetCrashDumpFile(mainWindow->GetImplant()) : GetCrashDumpFile(nullptr);

   MINIDUMP_EXCEPTION_INFORMATION ExpParam;
   ExpParam.ThreadId = GetCurrentThreadId();
   ExpParam.ExceptionPointers = pExceptionPointers;
   ExpParam.ClientPointers = TRUE;

   BOOL bMiniDumpSuccessful = MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), 
      hDumpFile, MiniDumpWithDataSegs, &ExpParam, NULL, NULL);

   return EXCEPTION_EXECUTE_HANDLER;// EXCEPTION_CONTINUE_SEARCH;
}

int mainWithCppExceptions(QApplication &a)
{
   try
   {
      return a.exec();
   }
   catch (exception &e)
   {
      QMessageBox::critical(NULL, "Critical Error", QString("Unhandled exception occurred: ") + e.what()
         + "\n" + PRODUCT_STR + " is about to be closed. Please report the error to the development team.");
   }

   return 1;
}

int mainWithCrashDump(QApplication &a)
{
   __try
   {
      return mainWithCppExceptions(a);
   }
   __except(GenerateDump(GetExceptionInformation()))
   {
      ::MessageBox(NULL, "Windows exception occurred.\n" PRODUCT_STR
         " is about to be closed."
         " Please report the error to the development team.", "Windows Exception", MB_OK | MB_ICONERROR);
   }

   return 1;
}

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(KApp);
    Q_INIT_RESOURCE(iTotalWorks);

    bool hasGeom = false;
    bool isReviewMode = false;
	bool skipCT = false;

    int L,T,W,H;    // left/top/width/height of the window

    for(int i=1; i<argc; i++)
    {
        if( _stricmp(argv[i], "-geometry")==0 )
        {
            QStringList plist = QString(argv[i+1]).split(",");
            if( plist.size()==4 )
            {
                L = plist[0].toInt();
                T = plist[1].toInt();
                W = plist[2].toInt();
                H = plist[3].toInt();
                hasGeom = (L>=0 && T>=0 && W>=0 && H>=0);
                ++i;
            }
        }
        else if (_stricmp(argv[i], "-reviewmode") == 0)
        {
           isReviewMode = true;
        }
		else if (_stricmp(argv[i], "-skipCT") == 0)
        {
           skipCT = true;
        }
	}

    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("Stryker");
    QCoreApplication::setApplicationName(PRODUCT_STR); // comes from 'Version.h'
    QCoreApplication::setOrganizationDomain("Stryker.com");

    MainWindowPS w(isReviewMode, skipCT);

    //w.show();

    HDC hDC = wglGetCurrentDC();;
    HGLRC hRC = wglGetCurrentContext();

    wglMakeCurrent(hDC, hRC);

    GLenum glenum = glewInit();

	if (GLEW_OK != glenum)
	{
		QMessageBox::critical(NULL, "Critical Error", 
			QString("Failed to initialize glew.\n Error: " + QString::number((int)glenum) + "\n")
            + w.GetAppName() + " is about to be closed. Please report the error to the development team.");
		return 1;
	}

#ifdef TESTING_MODE
    RunningIndicator indicator;
#endif

    if( hasGeom )
    {
        w.showNormal();
        QRect fRect = w.frameGeometry();
        QRect wRect = w.geometry();
        int dw = fRect.width() - wRect.width();
        int dh = fRect.height() - wRect.height();
        w.move(L,T);
        w.resize(W-dw,H-dh);
    }
    else
        w.showMaximized();
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));

    return mainWithCrashDump(a);
}
