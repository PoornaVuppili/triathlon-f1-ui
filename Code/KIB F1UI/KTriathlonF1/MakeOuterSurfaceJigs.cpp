#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "OuterSurfaceJigs.h"
#include "MakeOuterSurfaceJigs.h"
#include "MakeInnerSurfaceJigs.h"
#include "OutlineProfileJigs.h"
#include "FemoralPart.h"
#include "CartilageSurface.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "AdvancedControlJigs.h"
#include "UndoRedoCmdJigs.h"

CMakeOuterSurfaceJigs::CMakeOuterSurfaceJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
    EnableUndoStack(true);

	bool activateUI = (manActType==MAN_ACT_TYPE_EDIT);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMakeOuterSurfaceJigs";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;
	m_actDispSurface = NULL;
	m_actDispAntDrop = NULL;
	m_actDispOsteo = NULL;
	m_actDispCarti = NULL;

	m_bDispSurface = true;
	m_bDispAntDrop = false;
	m_bDispOsteo = false;
	m_bDispCarti = false;
	m_leftButtonDown = false;

	if ( activateUI )
	{
		m_toolbar = new QToolBar("Outer Surface Jigs");
		m_actMakeOuterSurfaceJigs = m_toolbar->addAction("OuterSurf");
		SetActionAsTitle( m_actMakeOuterSurfaceJigs );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );
		if ( m_pDoc->GetAdvancedControlJigs()->GetOuterSurfaceDisplayAnteriorDropSurface() )
		{
			m_actDispSurface = m_toolbar->addAction("DispSurf");
			EnableAction( m_actDispSurface, true );
			m_pMainWindow->connect( m_actDispSurface, SIGNAL( triggered() ), this, SLOT( OnDispSurface() ) );
			m_actDispAntDrop = m_toolbar->addAction("AntDrop");
			EnableAction( m_actDispAntDrop, true );
			m_pMainWindow->connect( m_actDispAntDrop, SIGNAL( triggered() ), this, SLOT( OnDispAntDrop() ) );
			m_actDispOsteo = m_toolbar->addAction("Osteo");
			EnableAction( m_actDispOsteo, true );
			m_pMainWindow->connect( m_actDispOsteo, SIGNAL( triggered() ), this, SLOT( OnDispOsteo() ) );
			m_actDispCarti = m_toolbar->addAction("Carti");
			EnableAction( m_actDispCarti, true );
			m_pMainWindow->connect( m_actDispCarti, SIGNAL( triggered() ), this, SLOT( OnDispCarti() ) );
		}
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );

	}

	COuterSurfaceJigs* outerSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	if ( outerSurfaceJigs == NULL  ) // if no object
	{
		if ( outerSurfaceJigs == NULL )
		{
			outerSurfaceJigs = new COuterSurfaceJigs( m_pDoc, ENT_ROLE_OUTER_SURFACE_JIGS );
			m_pDoc->AddEntity( outerSurfaceJigs, true );
		}

		// Reset is the way to make inner surface jigs.
		Reset( activateUI );
	}
	else
	{
		// Call reset to update inner surface jigs
		if ( !outerSurfaceJigs->GetUpToDateStatus() )
			Reset( activateUI );
	}

	if ( !activateUI )
		OnAccept();

}

CMakeOuterSurfaceJigs::~CMakeOuterSurfaceJigs()
{
	COuterSurfaceJigs* outerSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	if ( outerSurfaceJigs != NULL )
	{
		int previoisDesignTime = outerSurfaceJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		outerSurfaceJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}

}

void CMakeOuterSurfaceJigs::CreateOuterSurfaceJigsObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_OUTER_SURFACE_JIGS) == NULL )
	{
		COuterSurfaceJigs* pOuterSurfaceJigs = new COuterSurfaceJigs(pDoc, ENT_ROLE_OUTER_SURFACE_JIGS);
		pDoc->AddEntity(pOuterSurfaceJigs, true);
	}

	return;
}

void CMakeOuterSurfaceJigs::OnReset()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////
// Reset is the way to make inner surface, which includes the
// rectanglar bearing surface and 2 dropping surfaces on sides.
//////////////////////////////////////////////////////////////////////
void CMakeOuterSurfaceJigs::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs:Reset()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// If the time stamps here (outer surface jigs) and in osteophyte surface are the same,
	// (users never update the osteophyte surface since last time create osteoOffsetBrep.)
	// the offset surface can be retrieved directly from osteoOffsetBrep (time saving).
	// Otherwise (users had updated the osteophyte surface), we need to create the osteoOffsetBrep here.

	IwTArray<IwSurface*> totalOffsetSurfaces;
	IwBSplineSurface *offsetSurface;

	// get outer surface jigs
	COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	if ( pOuterSurfaceJigs == NULL )
		return;

	// get inner surface jigs
	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if ( pInnerSurfaceJigs == NULL )
		return;

	// get osteophyte surface
	CFemoralPart* pOsteoSurf = m_pDoc->GetOsteophyteSurface();
	if ( pOsteoSurf == NULL )
		return;

	// Get outline profile
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();

	// Get cartilage
	CCartilageSurface* pCartiSurf = m_pDoc->GetCartilageSurface();
	if ( pCartiSurf == NULL )
		return;

	IwBrep* cartiSurfBrep = pCartiSurf->GetIwBrep();
	if ( cartiSurfBrep == NULL )
		return;

	// offset distance
	double offsetDist = m_pDoc->GetAdvancedControlJigs()->GetOuterSurfaceOffsetDistance();

	//////////////////////////////////////////////////////////////
	// create the osteoOffsetBrep
	if ( m_pDoc->GetAdvancedControlJigs()->GetOsteophyteTimeStampForOuter() != pOuterSurfaceJigs->GetOsteophyteTimeStamp() )
	{
		double osteoAdditionalOffset = m_pDoc->GetVarTableValue("JIGS OSTEOPHYTE ADDITIONAL OFFSET DISTANCE");
		IwBSplineSurface* osteoMainSurf = pOsteoSurf->GetSinglePatchSurface();
		IwBSplineSurface* osteoLeftSurf, * osteoRightSurf;
		pOsteoSurf->GetSideSurfaces(osteoLeftSurf, osteoRightSurf);
		osteoMainSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), offsetDist+osteoAdditionalOffset, 0.05, offsetSurface);
		IwBSplineSurface* osteoMainOffsetSurf = offsetSurface;
		totalOffsetSurfaces.Add(osteoMainOffsetSurf);
		osteoLeftSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), offsetDist+osteoAdditionalOffset, 0.05, offsetSurface);
		IwBSplineSurface* osteoLeftOffsetSurf = offsetSurface;
		totalOffsetSurfaces.Add(osteoLeftOffsetSurf);
		osteoRightSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), offsetDist+osteoAdditionalOffset, 0.05, offsetSurface);
		IwBSplineSurface* osteoRightOffsetSurf = offsetSurface;
		totalOffsetSurfaces.Add(osteoRightOffsetSurf);
		// Create an IwBrep
		IwBrep* osteoOffsetBrep = new (m_pDoc->GetIwContext()) IwBrep(); 
		IwFace* face;
		osteoOffsetBrep->CreateFaceFromSurface(osteoMainOffsetSurf, osteoMainOffsetSurf->GetNaturalUVDomain(), face);
		osteoOffsetBrep->CreateFaceFromSurface(osteoLeftOffsetSurf, osteoLeftOffsetSurf->GetNaturalUVDomain(), face);
		osteoOffsetBrep->CreateFaceFromSurface(osteoRightOffsetSurf, osteoRightOffsetSurf->GetNaturalUVDomain(), face);
if (0)
ShowBrep(m_pDoc, osteoOffsetBrep, "osteoOffsetBrep", brown);
		// Set it to pOuterSurfaceJigs
		pOuterSurfaceJigs->SetOffsetOsteophyteBrep(osteoOffsetBrep);
		// Set time stamp
		pOuterSurfaceJigs->SetOsteophyteTimeStamp(m_pDoc->GetAdvancedControlJigs()->GetOsteophyteTimeStampForOuter());
	}
	else // Retrieve from pOuterSurfaceJigs
	{
		IwBrep* osteoOffsetBrep;
		pOuterSurfaceJigs->GetOffsetOsteophyteBrep( osteoOffsetBrep );
if (0)
ShowBrep(m_pDoc, osteoOffsetBrep, "osteoOffsetBrep", brown);
		IwTArray<IwFace*> faces;
		osteoOffsetBrep->GetFaces(faces);
		totalOffsetSurfaces.Add(faces.GetAt(0)->GetSurface());
		totalOffsetSurfaces.Add(faces.GetAt(1)->GetSurface());
		totalOffsetSurfaces.Add(faces.GetAt(2)->GetSurface());
	}

	//////////////////////////////////////////////////////////////////
	// create the cartiOffsetBrep
	if ( m_pDoc->GetAdvancedControlJigs()->GetCartilageTimeStamp() != pOuterSurfaceJigs->GetCartilageTimeStamp() )
	{
		bool bSmooth = !m_pDoc->GetAdvancedControlJigs()->GetCartilageUseWaterTightSurface();
		IwBSplineSurface* cartiMainSurf = pCartiSurf->GetSinglePatchSurface(bSmooth);
		IwBSplineSurface* cartiLeftSurf, * cartiRightSurf;
		pCartiSurf->GetSideSurfaces(cartiLeftSurf, cartiRightSurf);

		cartiMainSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), offsetDist, 0.05, offsetSurface);
		IwBSplineSurface* cartiMainOffsetSurf = offsetSurface;
		totalOffsetSurfaces.Add(cartiMainOffsetSurf);
		cartiLeftSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), offsetDist, 0.05, offsetSurface);
		IwBSplineSurface* cartiLeftOffsetSurf = offsetSurface;
		totalOffsetSurfaces.Add(cartiLeftOffsetSurf);
		cartiRightSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), offsetDist, 0.05, offsetSurface);
		IwBSplineSurface* cartiRightOffsetSurf = offsetSurface;
		totalOffsetSurfaces.Add(cartiRightOffsetSurf);
		// Create an IwBrep
		IwFace* face;
		IwBrep* cartiOffsetBrep = new (m_pDoc->GetIwContext()) IwBrep(); 
		cartiOffsetBrep->CreateFaceFromSurface(cartiMainOffsetSurf, cartiMainOffsetSurf->GetNaturalUVDomain(), face);
		cartiOffsetBrep->CreateFaceFromSurface(cartiLeftOffsetSurf, cartiLeftOffsetSurf->GetNaturalUVDomain(), face);
		cartiOffsetBrep->CreateFaceFromSurface(cartiRightOffsetSurf, cartiRightOffsetSurf->GetNaturalUVDomain(), face);
if (0)
ShowBrep(m_pDoc, cartiOffsetBrep, "cartiOffsetBrep", cPink);
		// Set it to pOuterSurfaceJigs
		pOuterSurfaceJigs->SetOffsetCartilageBrep(cartiOffsetBrep);
		// Set time stamp
		pOuterSurfaceJigs->SetCartilageTimeStamp(m_pDoc->GetAdvancedControlJigs()->GetCartilageTimeStamp());
	}
	else // Retrieve from pOuterSurfaceJigs
	{
		IwBrep* cartiOffsetBrep;
		pOuterSurfaceJigs->GetOffsetCartilageBrep( cartiOffsetBrep );
if (0)
ShowBrep(m_pDoc, cartiOffsetBrep, "cartiOffsetBrep", cPink);
		IwTArray<IwFace*> faces;
		cartiOffsetBrep->GetFaces(faces);
		totalOffsetSurfaces.Add(faces.GetAt(0)->GetSurface());
		totalOffsetSurfaces.Add(faces.GetAt(1)->GetSurface());
		totalOffsetSurfaces.Add(faces.GetAt(2)->GetSurface());
	}

	// Determine anterior dropping surface
	IwBrep* anteriorDroppingBrep=NULL;
	CMakeInnerSurfaceJigs::DetermineAnteriorDroppingSurface(m_pDoc, totalOffsetSurfaces, true, anteriorDroppingBrep);// 6 surfaces as input
	if (anteriorDroppingBrep==NULL)
	{
		pOuterSurfaceJigs->SetIwBrep(NULL);
		pOuterSurfaceJigs->MakeTess();
		pOuterSurfaceJigs->SetModifiedFlag(true);
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTER_SURFACE_JIGS, false, false);
		QApplication::restoreOverrideCursor();
		int		button;
		button = QMessageBox::warning( NULL, 
										tr("Warning Message!"), 
										tr("Outer surface needs a long time to generate.\n"
										   "Do you want to increase the outer surface offset value to avoid this issue?\n"), 
										QMessageBox::Yes, QMessageBox::No );	

		if( button == QMessageBox::Yes )		
			return;
		// If "No", then continue  
		QApplication::setOverrideCursor( Qt::WaitCursor );
		CMakeInnerSurfaceJigs::DetermineAnteriorDroppingSurface(m_pDoc, totalOffsetSurfaces, false, anteriorDroppingBrep);// 6 surfaces as input
	}

if(0)
	ShowBrep(m_pDoc, anteriorDroppingBrep, "anteriorDroppingBrep", green);

	// set it to pOuterSurfaceJigs
	pOuterSurfaceJigs->SetAnteriorDropping(anteriorDroppingBrep);

	// Add anterior dropping surface to totalOffsetSurfaces
	IwTArray<IwFace*> faces;
	anteriorDroppingBrep->GetFaces(faces);
	IwSurface* antDropSurf = faces.GetAt(0)->GetSurface();
	totalOffsetSurfaces.Add(antDropSurf);// now totalOffsetSurfaces has 7 surfaces

	// Get the jump curves
	IwTArray<IwBSplineCurve*> jumpCurves;
	if ( pOutlineProfileJigs )
		pOutlineProfileJigs->GetJumpCurves(jumpCurves);
	if (jumpCurves.GetSize() == 0 )
	{
		jumpCurves.Add(NULL);
		jumpCurves.Add(NULL);
	}

	//// See whether need to enforce to remove spikes
	IwBSplineSurface* referenceSurface = NULL;
	double referenceDistance = 0;
	if ( m_pDoc->GetAdvancedControlJigs()->GetOuterSurfaceEnforceSmooth() )
	{
		IwBrep* innerBrep = pInnerSurfaceJigs->GetIwBrep();
		if ( innerBrep )
		{
			innerBrep->GetFaces(faces);
			if ( faces.GetSize() > 0 )
			{
				referenceSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
				referenceDistance = m_pDoc->GetAdvancedControlJigs()->GetOuterSurfaceOffsetDistance() - 0.5 + 0.05; // The value needs to synchronize with CMakeSolidPositionJigs::Validate(), "0.05" as allowance.
			}
		}
	}

	//// Create outer contact surface
	IwBrep* contactSurfBrep=NULL;
	double dropOffset = m_pDoc->GetAdvancedControlJigs()->GetInnerSurfaceDropOffsetDistance() + 0.1;//  0.1 as allowance
	double troGroAddThickness = m_pDoc->GetAdvancedControlJigs()->GetOuterSurfaceTrochlearGrooveAdditionalThickness();
	double troGroAddThicknessTransitionWidthRatio = m_pDoc->GetAdvancedControlJigs()->GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth();
	IwTArray<IwPoint3d> posVerticalEdgePoints, negVerticalEdgePoints;
	CMakeInnerSurfaceJigs::DetermineContactSurface(m_pDoc, totalOffsetSurfaces, dropOffset, false, false, &troGroAddThickness, &troGroAddThicknessTransitionWidthRatio, &jumpCurves, contactSurfBrep, referenceSurface, referenceDistance ); // 7 surfaces as input
	pOuterSurfaceJigs->SetIwBrep(contactSurfBrep);
	pOuterSurfaceJigs->MakeTess();
	pOuterSurfaceJigs->SetModifiedFlag(true);

	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTER_SURFACE_JIGS, false, false);

	QApplication::restoreOverrideCursor();

	m_pView->Redraw();

}

void CMakeOuterSurfaceJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs::OnAccept()") );

	COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();

	// For outer surface, we also need to check pOuterSurfaceJigs->GetIwBrep() due to the time test when generate anterior dropping surface
	if ( pOuterSurfaceJigs && pOuterSurfaceJigs->GetIwBrep() && !pOuterSurfaceJigs->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTER_SURFACE_JIGS, true, true);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}


void CMakeOuterSurfaceJigs::OnDispSurface()
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs::OnDispSurface()") );

	m_bDispSurface = !m_bDispSurface;

	m_pDoc->GetOuterSurfaceJigs()->SetDispSurface(m_bDispSurface);

	m_pView->Redraw();

}

void CMakeOuterSurfaceJigs::OnDispAntDrop()
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs::OnDispAntDrop()") );

	m_bDispAntDrop = !m_bDispAntDrop;

	m_pDoc->GetOuterSurfaceJigs()->SetDispAntDrop(m_bDispAntDrop);

	m_pView->Redraw();

}
void CMakeOuterSurfaceJigs::OnDispOsteo()
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs::OnDispOsteo()") );

	m_bDispOsteo = !m_bDispOsteo;

	m_pDoc->GetOuterSurfaceJigs()->SetDispOsteo(m_bDispOsteo);

	m_pView->Redraw();

}

void CMakeOuterSurfaceJigs::OnDispCarti()
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs::OnDispCarti()") );

	m_bDispCarti = !m_bDispCarti;

	m_pDoc->GetOuterSurfaceJigs()->SetDispCarti(m_bDispCarti);

	m_pView->Redraw();

}

void CMakeOuterSurfaceJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CMakeOuterSurfaceJigs::OnCancel()") );

	m_bDestroyMe = true;
	m_pView->Redraw();

}

bool CMakeOuterSurfaceJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_leftButtonDown = true;

	ReDraw();

	return true;
}

bool CMakeOuterSurfaceJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{
	m_leftButtonDown = false;

	ReDraw();

	return true;
}

bool CMakeOuterSurfaceJigs::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{

	ReDraw();

	return true;
}

////////////////////////////////////////////////////////////////////////
bool CMakeOuterSurfaceJigs::PickAControlPoint
(
	const QPoint& cursor,			// I:
	ULONG& ctrlPntIndex,	// O:
	IwPoint3d& prevCtrlPnt,	// O:
	IwPoint3d& postCtrlPnt	// O:
)
{
	COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	if ( pOuterSurfaceJigs == NULL )
		return false;

	IwBrep* oBrep = pOuterSurfaceJigs->GetIwBrep();
	if ( oBrep == NULL )
		return false;
	IwTArray<IwFace*> faces;
	IwFace* face;
	oBrep->GetFaces(faces);
	if (faces.GetSize() == 0 )
		return false;
	face = faces.GetAt(0);
	IwBSplineSurface* Surf = (IwBSplineSurface*)face->GetSurface();
	//////////////////////////////////////////////////////////////////////////////
	// cycle through all control points and get the control points on the boundary 
	// Get the knot info 
	ULONG ukCount, vkCount;
	double *uKnots, *vKnots;
	Surf->GetKnotsPointers(ukCount, vkCount, uKnots, vKnots);
	// Get control points
	ULONG ucCount, vcCount;
	double* cPointers;
	Surf->GetControlPointsPointer(ucCount, vcCount, cPointers);
	ULONG cpIndex, nIndex0, nIndex1, nIndex2, nIndex3;
	IwPoint3d cPoint, nPoint0, nPoint1, nPoint2, nPoint3;
	IwPoint3d cpt;
	int uv[2];
	double dist, minDist = HUGE_DOUBLE;
	bool gotAPoint = false;
	for (ULONG ui=1; ui<(ucCount-1); ui++)// skip the first one and last one
	{
		for (ULONG vi=2; vi<(vcCount-2); vi++)// skip the first two and last two. We do not want to modify the ctrl point on boundary.
		{
			cpIndex = 4*(vi+ui*vcCount);
			cPoint = IwPoint3d(cPointers[cpIndex], cPointers[cpIndex+1], cPointers[cpIndex+2]);
			cpt = cPoint;
			m_pDoc->GetView()->XYZtoUV(cpt, uv);
			dist = (cursor.x()-uv[0])*(cursor.x()-uv[0]) + (cursor.y()-uv[1])*(cursor.y()-uv[1]);
			if ( dist < minDist )
			{
				minDist = dist;

				gotAPoint = true;
				ctrlPntIndex = cpIndex;
				prevCtrlPnt = cPoint;
				// here to average its neighbors
				nIndex0 = 4*(vi-1+ui*vcCount);
				nPoint0 = IwPoint3d(cPointers[nIndex0], cPointers[nIndex0+1], cPointers[nIndex0+2]);
				nIndex1 = 4*(vi+1+ui*vcCount);
				nPoint1 = IwPoint3d(cPointers[nIndex1], cPointers[nIndex1+1], cPointers[nIndex1+2]);
				nIndex2 = 4*(vi+(ui-1)*vcCount);
				nPoint2 = IwPoint3d(cPointers[nIndex2], cPointers[nIndex2+1], cPointers[nIndex2+2]);
				nIndex3 = 4*(vi+(ui+1)*vcCount);
				nPoint3 = IwPoint3d(cPointers[nIndex3], cPointers[nIndex3+1], cPointers[nIndex3+2]);
				postCtrlPnt = 0.25*(nPoint0+nPoint1+nPoint2+nPoint3);
			}
		}
	}

	return gotAPoint;
}

void CMakeOuterSurfaceJigs::GetMakeOuterSurfaceJigsUndoData
(
	CMakeOuterSurfaceJigsUndoData& undoData,	// O:
	ULONG& ctrlPntIndex,						// I: 
	IwPoint3d& ctrlPnts							// I:
)
{

	undoData.ctrlPntIndex = ctrlPntIndex;
	undoData.ctrlPnt = ctrlPnts;
}


void CMakeOuterSurfaceJigs::SetMakeOuterSurfaceJigsUndoData
(
	CMakeOuterSurfaceJigsUndoData& undoData	// O:
)
{
	// to update the main surface in cartilageSurface
	{
		COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
		if ( pOuterSurfaceJigs == NULL )
			return;
		IwBrep* oBrep = pOuterSurfaceJigs->GetIwBrep();
		if ( oBrep == NULL )
			return;
		IwTArray<IwFace*> faces;
		IwFace* face;
		oBrep->GetFaces(faces);
		if (faces.GetSize() == 0 )
			return;
		face = faces.GetAt(0);
		IwBSplineSurface* mainSurf = (IwBSplineSurface*)face->GetSurface();
		//
		IwTArray<ULONG> ctrlPntIndexes;
		IwTArray<IwPoint3d> ctrlPnts;
		ctrlPntIndexes.Add(undoData.ctrlPntIndex);
		ctrlPnts.Add(undoData.ctrlPnt);
		// Very time consuming, need to let users know still in processing.
		QApplication::setOverrideCursor( Qt::WaitCursor );
		CCartilageSurface::UpdateSurfacebyCtrlPoints(m_pDoc, mainSurf, ctrlPntIndexes, ctrlPnts);
		pOuterSurfaceJigs->MakeTess();
		pOuterSurfaceJigs->SetModifiedFlag(true);
		QApplication::restoreOverrideCursor();
	}

	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTER_SURFACE_JIGS, false, false); // therefore, cartilage become out of date

	m_pView->Redraw();
}