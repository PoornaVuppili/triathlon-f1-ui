#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <QObject>
#include <QWidget>
#include <QMenu>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QCursor>
#include <QStack>
#include <QDomDocument>
#include <qgl.h>
#include "..\KUtility\TRigid3.h"
#include "..\KUtility\TBox3.h"
#include "..\KUtility\TPickRay.h"
#include "../KUtility/TPickRect.h"
#include "PredefinedViewToolBar.h"
#include "Measure.h"
#pragma warning( disable : 4996 4805 )
#include <iwcore_types.h>
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )

class EntityView;
class Manager;

#include <vector>
using namespace std;

//class GLUquadricObj;
#include <gl/GLU.h>

class Viewport : public QObject
{
    Q_OBJECT

public:
    Viewport();
    ~Viewport();

    // Clear everything
    void ClearContent();

    // Get/set id
    QString GetID() const { return id; }
    void SetID(QString s) { id = s; }

    // Get/Set the OpenGL widget
    QGLWidget* GetGLWidget() { return widget; }
    void SetGLWidget(QGLWidget* w) { widget = w; }

    // Set the quadric object
    void SetQuadricObj(GLUquadricObj* q) { quad = q; }

    // Get the model and projection matrix
    double* GetModelMatrix()        { return modelMatrix;   }
    double* GetProjectionMatrix()   { return projMatrix;    }

    // Get/set the viewport
    void GetViewport(int& minX, int& maxX, int& minY, int& maxY);
    void SetViewport(int minX, int maxX, int minY, int maxY);

    // Get/set the view range
    void GetViewRange(float& left, float& right, float& bottom, float& top, float& ne, float& fa);
    void SetViewRange(float  left, float right,  float  bottom, float  top, float  ne, float  fa);

    // Get/set the viewing bounding box
    void GetBoundingBox(Box3Float& box) { box = boundBox; }
    void SetBoundingBox(const Box3Float& box);

    // Get/set the viewing transformation
    Rigid3Float GetTransform() { return transform; }
    void SetTransform(Rigid3Float& tran);

	// Get/Set the viewing orientation/rotation
    Rotate3Float GetOrientation()			{ return transform.GetRotationMatrix(); }
    void SetOrientation(Rotate3Float& rot);

	void SetAnimateTransform(bool animate)		{	animateTransform = animate; }
	bool GetAnimateTransform()					{	return animateTransform;	}

    // Test if (x,y) is inside the viewport
    bool InsideViewport(int x, int y);

	// Zoom in/out at the specified viewport coordinate
	void Zoom(bool in, int viewX=-1, int viewY=-1);

    // Adjust the viewing to best fit the data
    void BestFit(float expandRatio=0);

    // Draw everything
    virtual void Draw();

	virtual void DrawAfterBuffersSwap();

    // Event processing
    virtual void ProcessMousePressEvent( QMouseEvent * event );
    virtual void ProcessMouseMoveEvent( QMouseEvent * event );
    virtual void ProcessMouseReleaseEvent( QMouseEvent * event );
    virtual void ProcessDoubleClickEvent( QMouseEvent * event );
    virtual void ProcessWheelEvent ( QWheelEvent * event );
    virtual void ProcessKeyPressEvent(QKeyEvent* event);
    virtual void ProcessKeyReleaseEvent(QKeyEvent* ev);
	virtual void ProcessFocusInEvent(QFocusEvent* ev);
	virtual void ProcessFocusOutEvent(QFocusEvent* ev);

    // Whether the viewport is maximized
    bool IsMaximized() const { return maximized; }
    void SetMaximized(bool b);

    // Add/Remove an entity view
    virtual void AddView(EntityView* v);
    void RemoveView(EntityView* v);
    bool IsViewExist(EntityView* v);

    // Add the manager to the top of the manager stack as the active manager.
	void AddManager(Manager* mgr);

	void SetDefaultManager(Manager *);
	void RemoveDefaultManager();

    // Remove the manager if it is at the top of the manager stack.
    void RemoveManager(Manager* mgr);

    // Set the measure mode
    void SetMeasureMode(MeasureMode m);

    // Get the measure result: distance in mm / angle in degree / radius in mm
    // depending on the measurement type. Generate an exception if the measure is invalid.
    double GetMeasure() { return resMeasure.GetResult(); }

    // Build the ray for picking at the cursor point
	PickRayFloat BuildPickRay(const QPoint& cursor);

    // Build the rectangle for picking
    PickRectFloat BuildPickRect(const QPoint& c1, const QPoint& c2) const;

    // For compatibility with TriathlonF1
    QPoint XYZtoUV( const Point3Float& pt );
    Point3Float UVtoXYZ( const QPoint& point );
    void XYZtoUVW( Point3Float& pt, int uvw[3] );
    void UVWtoXYZ( int uvw[3], Point3Float& pt );

    // Get the viewport resolution (corresponding to ?? mm per pixel)
    float GetResolution_MMPerPixel() const { return viewWidth()/viewportWidth(); }
    // Get the viewport resolution (corresponding to ?? pixel per mm)
    float GetResolution_PixelPerMM() const { return viewportWidth()/viewWidth(); }

    // whether 3D viewport
    virtual bool Is3D() { return true; }

    // Toolbar related to predefined views
	QToolBar* SetupPredefinedViewsToolBar();
    int GetNumberOfCoordSystems() const;
    QString GetCurrentCoordSystemName();
    void SetCurCoordSystemName(const QString& name);

	void ReAlignAlongXAxis(bool viewingFromLeftToRight);
	void ReAlignAlongYAxis(bool viewingFromBackToFront);
	void ReAlignAlongZAxis(bool viewingFromBottomToTop);

	// Get the status by filling in 'elem'
    virtual void GetStatus(QDomElement& elem);

    // Set the status based on the content of 'elem'
    virtual void SetStatus(QDomElement elem);

public slots:
    void FrontView(bool autoRotZ180=true);
    void BackView(bool autoRotZ180=true);
    void LeftView(bool autoRotZ180=true);
    void RightView(bool autoRotZ180=true);
    void TopView(bool autoRotZ180=true);
    void BottomView(bool autoRotZ180=true);
    void IsoView();
	void IsoBackView();
    void MedialView(bool autoRotZ180=true);
    void LateralView(bool autoRotZ180=true);

protected:
	void AddParams(QDomElement& elem);
	void ParseParams(QDomElement elem);

    // Adjust the viewing depending on the window aspect ratio
    void AdjustViewAspectRatio();

    // Adjust the viewing depth 
    void AdjustViewDepth();

    // Get the axis-aligned bounding box after the transformation
    void GetBoundTransform(Point3Float& boundTransformMin, Point3Float& boundTransformMax);

    // Update the bounding box of visible entities
    virtual void UpdateBoundingBox();

    // Update the transform based on bounding box and center of rotation
    void UpdateTransform();

    // Update the viewing volume based on the bounding box
    void UpdateViewVolume(float expandRatio=0);

    // Do measurement when the mouse is clicked
    virtual void DoMeasure(const QPoint& cursor);

	// Adjust pick ray depends on shift-key during screen measurement
	PickRayFloat AdjustPickRay(const QPoint& cursor);

    // Draw the entities
    virtual void DrawEntities();

    // Draw the measurement
    virtual void DrawMeasure();

    // Draw the axis at the bottom left corner
    void DrawAxis();

    // Iso-transform the viewport
    void IsoTransform(Rigid3Float& t);

	// Iso-transform the viewport, from the back side
    void IsoTransformBack(Rigid3Float& t);

    // Animate the transformation
    void AnimateTransform();

	inline void RotateX(float degree);
	inline void RotateY(float degree);
	inline void RotateZ(float degree);
	inline void RotateYX(float ydegree, float xdegree);
	inline void Shift(float h, float v);
	void Zoom(float zoomRatio, int viewX=-1, int viewY=-1);

    // Set up the context menu
    virtual void SetupContextMenu();

    // OpenGL widget that the viewport is located
    QGLWidget* widget;

    // entity views
    vector<EntityView*> entityViews;

    // viewport location
    int viewportMinX, viewportMinY, viewportMaxX, viewportMaxY;

    int viewportWidth()  const { return viewportMaxX - viewportMinX; }
    int viewportHeight() const { return viewportMaxY - viewportMinY; }
    int viewportCenX()   const { return (viewportMinX+viewportMaxX)>>1; }
    int viewportCenY()   const { return (viewportMinY+viewportMaxY)>>1; }

    float viewWidth()    const { return viewRight - viewLeft; }
    float viewHeight()   const { return viewTop - viewBottom; }

    // model & projection matrices
    double modelMatrix[16], projMatrix[16];

    // viewing volume
    float viewLeft, viewRight, viewBottom, viewTop, viewNear, viewFar;

    // the (oriented) viewing bounding box (before transformation)
    // - orientated bounding box is used in 2D view for best fit along the scan orientation
    // - for 3D view, use the axis aligned bounding box
    Box3Float boundBox;
    bool emptyBBox;  // whether the default empty bbox

	bool animateTransform;

    // the center of rotation
    Point3Float centerRot;

    // the transformation
    Rigid3Float transform;

    QPoint mouseBegin, mouseLast;
    Qt::KeyboardModifiers keyModifier;

    // the transform animation
    virtual void timerEvent( QTimerEvent * event );

    int timerID;
    Rigid3Float transformTo;
    int timerLimit, timerCount;
    Quaternionf oldQ,newQ;
    Vector3Float oldT, newT;
    float theta;

    // display setting
    bool maximized, showAxis, showAxisRotationRegion;
    QString txtRotation;    // the rotation angle shown on top of cursor when only rotating along X or Y (similar to TriathlonF1)

    // context menu
    QMenu* contextMenu;

	PredefinedViewToolBar *predefinedViews;
    void InitPredefinedViewTransform();
	IwAxis2Placement	previousCoordSystem;

    // measurement
    MeasureMode measureMode;
    vector<MeasureEntity> entyMeasure;
    MeasureResult resMeasure;

	vector<Point3Float> lineAssist;	// assistant lines during screen measurement
	void UpdateAssistLine(const QPoint& cursor);

    // add or replace a measure entity in the measurement buffer
    void AddReplaceMeasure(const MeasureEntity& ent, int bufferSizeBound);

    // quadric object for drawing
    GLUquadricObj* quad;

    // ID
    QString id;

    // Manager stack
	QStack< Manager* > managerStack;
	Manager* defaultManager;

    QCursor measureCursor;

    QAction* actFront;
    QAction* actBack;
    QAction* actLeft;
    QAction* actRight;
    QAction* actTop;
    QAction* actBottom;

signals:
    void maximizeChanged();

protected slots:
    void BestFitView();
    void ToggleMaximize();
	void ToggleShowAxisRotationRegion();
    void ToggleShowAxis();
    void AdjustBBoxDepth();
    void SetCurrentCoordSystem(const CoordSystem& axes);
};

#endif // VIEWPORT_H
