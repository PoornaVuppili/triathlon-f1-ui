#pragma once

#ifndef TEST_EXPORT_TW 
#define TEST_EXPORT_TW __declspec(dllexport)
#endif

///////////////////////////////////////////////////////////////////
// These 3 numbers indicate the data format. They should be kind of 
// synchronized with the correspondents in /TriathlonF1/Resource Files/Version.h 
//#define VERSION_MAJOR_NUM 
//#define VERSION_MINOR_NUM 
//#define VERSION_BUILD_NUM 
//
// Note:
// (VERSION_MAJOR_NUM,VERSION_MINOR_NUM,VERSION_BUILD_NUM) are the 
// application (software) version numbers.
// (ITOTAL_RELEASE,ITOTAL_REVISION,ITOTAL_DATAFILE_VERSION) are the
// TriathlonF1 (CR) and TriathlonF1 PS data file version number. There 
// 3 numbers will be saved to *.tw and *tb header.h during saving. 
// Only one data file version number 
// (ITOTAL_RELEASE,ITOTAL_REVISION,ITOTAL_DATAFILE_VERSION)
// exists across TriathlonF1 (CR) and TriathlonF1 PS at any development time.
// (ITOTAL_RELEASE,ITOTAL_REVISION) should synchronize with TriathlonF1 (CR)
// (VERSION_MAJOR_NUM,VERSION_MINOR_NUM) and set ITOTAL_DATAFILE_VERSION=0
// when TriathlonF1 (CR) major and minor version numbers are updated. 
// TriathlonF1 PS developers should NOT change both numbers. 
// During development, when data file formats are changed (like new parameters
// are introduced) either in TriathlonF1 (CR) or in TriathlonF1 PS or both, 
// developers should increase ITOTAL_DATAFILE_VERSION.
///////////////////////////////////////////////////////////////////

#ifndef ITOTAL_RELEASE
#define ITOTAL_RELEASE 6
#endif

#ifndef ITOTAL_REVISION
#define ITOTAL_REVISION 0
#endif

#ifndef ITOTAL_DATAFILE_VERSION
#define ITOTAL_DATAFILE_VERSION 30
#endif
