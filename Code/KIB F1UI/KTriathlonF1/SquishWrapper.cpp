#include "SquishWrapper.h"
#include "TotalView.h"
#include "TotalDoc.h"
#include "MeasureEntityDistance.h"
#include "MeasureEntityAngle.h"
#include "MeasureCurveRadius.h"
#include "MeasureScreenDistance.h"
#include "MeasureScreenAngle.h"
#include "MeasureScreenRadius.h"

CSquishWrapper* CSquishWrapper::pEntryPoint = NULL;

CSquishWrapper* CSquishWrapper::GetEntryPoint()
{
	return pEntryPoint;
}

void CSquishWrapper::SetEntryPoint(CSquishWrapper* entryPoint)
{
	pEntryPoint = entryPoint;
}

CSquishWrapper::CSquishWrapper()
{
	m_pDoc = NULL;
	m_pView = NULL;
}


CSquishWrapper::~CSquishWrapper()
{

}

void CSquishWrapper::SetFilePath(const QString& filePath)
{
	//m_pDoc->SetFilePath(filePath);
}

void CSquishWrapper::SetFixedWidgetSize()
{
	//m_pDoc->SetFixedWidgetSize();
}

////////////////////////////////////////////////////////////////////////////
// If errors occur, return -1. Otherwise, it returns the numbers of files 
// been modified within the given seconds ago.
////////////////////////////////////////////////////////////////////////////
int CSquishWrapper::FilesLastModified(const QString& fileDir, int secondsAgo)
{
	int modifiedNo = -1;
	QDir			dir( fileDir );
	if ( !dir.exists() ) return modifiedNo;

	QDateTime nowTime = QDateTime::currentDateTime();
	QDateTime modifiedDate, modifiedDateAdd;
	QFileInfo fileInfo;
	QFileInfoList fileInfoList = dir.entryInfoList();

	modifiedNo = 0;
	int fileNo = fileInfoList.size();
	for (int i=0; i<fileNo; i++)
	{
		fileInfo = fileInfoList.at(i);
		modifiedDate = fileInfo.lastModified();
		modifiedDateAdd = modifiedDate.addSecs(secondsAgo);

		if (modifiedDateAdd>nowTime) 
			modifiedNo +=1;
	}

	return modifiedNo;
}

bool CSquishWrapper::FileLastModified(const QString& fileName, int secondsAgo)
{
	
	int length = fileName.length();
	int lastIndex = fileName.lastIndexOf(QDir::separator());
	QString fileDir = fileName.left(lastIndex);
	QString fName = fileName.right(length-lastIndex-1);

	QDir			dir( fileDir );
	if ( !dir.exists() ) return false;

	QDateTime nowTime = QDateTime::currentDateTime();
	QDateTime modifiedDate, modifiedDateAdd;
	QFileInfo fileInfo;
	QFileInfoList fileInfoList = dir.entryInfoList();

	int fileNo = fileInfoList.size();
	for (int i=0; i<fileNo; i++)
	{
		fileInfo = fileInfoList.at(i);
		if (fileInfo.fileName() == fName)
		{
			modifiedDate = fileInfo.lastModified();
			modifiedDateAdd = modifiedDate.addSecs(secondsAgo);

			if (modifiedDateAdd>nowTime) 
				return true;
			else
				return false;
		}
	}

	return false;
}