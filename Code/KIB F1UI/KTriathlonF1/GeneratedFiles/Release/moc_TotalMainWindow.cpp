/****************************************************************************
** Meta object code from reading C++ file 'TotalMainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../TotalMainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TotalMainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CTotalMainWindow_t {
    QByteArrayData data[27];
    char stringdata0[406];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CTotalMainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CTotalMainWindow_t qt_meta_stringdata_CTotalMainWindow = {
    {
QT_MOC_LITERAL(0, 0, 16), // "CTotalMainWindow"
QT_MOC_LITERAL(1, 17, 20), // "DesignStageCommenced"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 7), // "Module*"
QT_MOC_LITERAL(4, 47, 19), // "DesignStageComplete"
QT_MOC_LITERAL(5, 67, 9), // "OnFileNew"
QT_MOC_LITERAL(6, 77, 10), // "OnFileOpen"
QT_MOC_LITERAL(7, 88, 8), // "QString&"
QT_MOC_LITERAL(8, 97, 4), // "elem"
QT_MOC_LITERAL(9, 102, 21), // "OnFileOpenWithOptions"
QT_MOC_LITERAL(10, 124, 7), // "options"
QT_MOC_LITERAL(11, 132, 11), // "OnFileClose"
QT_MOC_LITERAL(12, 144, 10), // "OnFileSave"
QT_MOC_LITERAL(13, 155, 8), // "bWarning"
QT_MOC_LITERAL(14, 164, 20), // "OnFileOpenAsReadOnly"
QT_MOC_LITERAL(15, 185, 15), // "OnFileSaveACopy"
QT_MOC_LITERAL(16, 201, 16), // "OnImportSurfaces"
QT_MOC_LITERAL(17, 218, 21), // "OnEnableMakingActions"
QT_MOC_LITERAL(18, 240, 25), // "OnEnableMakingActionsJigs"
QT_MOC_LITERAL(19, 266, 20), // "OnCleanUpTempDisplay"
QT_MOC_LITERAL(20, 287, 17), // "OnImportCADEntity"
QT_MOC_LITERAL(21, 305, 21), // "OnImportCADEntityJigs"
QT_MOC_LITERAL(22, 327, 22), // "OnFemoralImplantReview"
QT_MOC_LITERAL(23, 350, 19), // "OnFemoralJigsReview"
QT_MOC_LITERAL(24, 370, 13), // "OnSaveNewFile"
QT_MOC_LITERAL(25, 384, 11), // "OnDoNothing"
QT_MOC_LITERAL(26, 396, 9) // "OnBestFit"

    },
    "CTotalMainWindow\0DesignStageCommenced\0"
    "\0Module*\0DesignStageComplete\0OnFileNew\0"
    "OnFileOpen\0QString&\0elem\0OnFileOpenWithOptions\0"
    "options\0OnFileClose\0OnFileSave\0bWarning\0"
    "OnFileOpenAsReadOnly\0OnFileSaveACopy\0"
    "OnImportSurfaces\0OnEnableMakingActions\0"
    "OnEnableMakingActionsJigs\0"
    "OnCleanUpTempDisplay\0OnImportCADEntity\0"
    "OnImportCADEntityJigs\0OnFemoralImplantReview\0"
    "OnFemoralJigsReview\0OnSaveNewFile\0"
    "OnDoNothing\0OnBestFit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CTotalMainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  134,    2, 0x06 /* Public */,
       4,    2,  139,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,  144,    2, 0x0a /* Public */,
       6,    1,  145,    2, 0x0a /* Public */,
       6,    0,  148,    2, 0x2a /* Public | MethodCloned */,
       9,    2,  149,    2, 0x0a /* Public */,
       9,    1,  154,    2, 0x2a /* Public | MethodCloned */,
       9,    0,  157,    2, 0x2a /* Public | MethodCloned */,
      11,    0,  158,    2, 0x0a /* Public */,
      12,    1,  159,    2, 0x0a /* Public */,
      12,    0,  162,    2, 0x2a /* Public | MethodCloned */,
      14,    0,  163,    2, 0x0a /* Public */,
      15,    0,  164,    2, 0x0a /* Public */,
      16,    0,  165,    2, 0x0a /* Public */,
      17,    0,  166,    2, 0x0a /* Public */,
      18,    0,  167,    2, 0x0a /* Public */,
      19,    0,  168,    2, 0x0a /* Public */,
      20,    0,  169,    2, 0x0a /* Public */,
      21,    0,  170,    2, 0x0a /* Public */,
      22,    0,  171,    2, 0x0a /* Public */,
      23,    0,  172,    2, 0x0a /* Public */,
      24,    0,  173,    2, 0x09 /* Protected */,
      25,    0,  174,    2, 0x09 /* Protected */,
      26,    0,  175,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    2,    2,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7, QMetaType::QString,    8,   10,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CTotalMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CTotalMainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->DesignStageCommenced((*reinterpret_cast< Module*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->DesignStageComplete((*reinterpret_cast< Module*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->OnFileNew(); break;
        case 3: _t->OnFileOpen((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->OnFileOpen(); break;
        case 5: _t->OnFileOpenWithOptions((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->OnFileOpenWithOptions((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->OnFileOpenWithOptions(); break;
        case 8: _t->OnFileClose(); break;
        case 9: _t->OnFileSave((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->OnFileSave(); break;
        case 11: _t->OnFileOpenAsReadOnly(); break;
        case 12: _t->OnFileSaveACopy(); break;
        case 13: { bool _r = _t->OnImportSurfaces();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->OnEnableMakingActions(); break;
        case 15: _t->OnEnableMakingActionsJigs(); break;
        case 16: _t->OnCleanUpTempDisplay(); break;
        case 17: _t->OnImportCADEntity(); break;
        case 18: _t->OnImportCADEntityJigs(); break;
        case 19: _t->OnFemoralImplantReview(); break;
        case 20: _t->OnFemoralJigsReview(); break;
        case 21: _t->OnSaveNewFile(); break;
        case 22: _t->OnDoNothing(); break;
        case 23: _t->OnBestFit(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Module* >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Module* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CTotalMainWindow::*)(Module * , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CTotalMainWindow::DesignStageCommenced)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CTotalMainWindow::*)(Module * , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CTotalMainWindow::DesignStageComplete)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CTotalMainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<CMainWindow::staticMetaObject>(),
    qt_meta_stringdata_CTotalMainWindow.data,
    qt_meta_data_CTotalMainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CTotalMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CTotalMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CTotalMainWindow.stringdata0))
        return static_cast<void*>(this);
    return CMainWindow::qt_metacast(_clname);
}

int CTotalMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void CTotalMainWindow::DesignStageCommenced(Module * _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CTotalMainWindow::DesignStageComplete(Module * _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
