#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "MakeFemoralAxes.h"
#include "FemoralAxes.h"
#include "..\KAppTotal\Part.h"
#include "FemoralPart.h"
#include "AdvancedControl.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "UndoRedoCmd.h"
#include "..\KApp\SwitchBoard.h"
#include "..\KApp\MainWindow.h"
#include "IFemurImplant.h"
#include <map>

CMakeFemoralAxes::CMakeFemoralAxes( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
    EnableUndoStack(true);

	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT || manActType == MAN_ACT_TYPE_REVIEW );
	
	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->StartTimeLog("MakeFemoralAxes");

	m_pFemur = m_pDoc->GetFemur();
	m_pFemurBrep = m_pFemur->GetIwBrep();	

	m_pHip = m_pDoc->GetHip();
	m_pHipBrep = m_pHip->GetIwBrep();
	m_hipPoint = IwPoint3d(-111111, -111111, -111111);
	if (m_pHipBrep == NULL)
	{
		IwTArray<IwPoint3d> hipPoints;
		m_pHip->GetPoints(hipPoints);
		if (hipPoints.GetSize() > 0)
			m_hipPoint = hipPoints.GetAt(0);// First one is the hip point
	}

	m_sClassName = "CMakeFemoralAxes";

	m_toolbar = NULL;
	m_actRefine = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;
	m_actValidate = NULL;
	m_actReviewNext = NULL;
	m_actReviewRework = NULL;
	m_actRotateCCW = NULL;
	m_actRotateCW = NULL;
	m_spinAllFlexionAngle = NULL;
	m_spinAntCutFlexionAngle = NULL;
	m_spinAntCutObliqueAngle = NULL;
	m_spinFemoralAxesFlexionAngle = NULL;
	m_spinFemoralAxesYRotAngle = NULL;
	m_spinFemoralAxesZRotAngle = NULL;
	m_predefinedViewIndex = 0;

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Femoral Axes");

		m_actMakeFemoralAxes = m_toolbar->addAction("FemAxes");
		SetActionAsTitle( m_actMakeFemoralAxes );

		if ( m_pDoc->GetAdvancedControl()->GetFemoralAxesEnableRefine() )
		{
			m_actRefine = m_toolbar->addAction("Refine");
			EnableAction( m_actRefine, true );
			connect( m_actRefine, SIGNAL( triggered() ), this, SLOT( OnRefine() ) );
		}

		m_actRotateCW = m_toolbar->addAction(QIcon( ":/KTriathlonF1/Resources/Rotate_CW.png" ),QString(""));
		m_actRotateCW->setToolTip("Rotate femur clockwise");

		m_actRotateCCW = m_toolbar->addAction(QIcon( ":/KTriathlonF1/Resources/Rotate_CCW.png" ),QString(""));
		m_actRotateCCW->setToolTip("Rotate femur counter clockwise");

		if(0)//removed by PSV---
		{
		QLabel* labelTextAllFlexionAngle = new QLabel(QString("Implant F %1:").arg(QChar(0x00B0)));
		m_toolbar->addWidget(labelTextAllFlexionAngle);
		labelTextAllFlexionAngle->setFont( m_fontBold );
		double allFlexionAngleMin = -1.0*m_pDoc->GetVarTableValue("FEMORAL IMPLANT FLEXION MAX ANGLE")/* -(-0.0) degrees */;
		double allFlexionAngleMax = -1.0*m_pDoc->GetVarTableValue("FEMORAL IMPLANT FLEXION MIN ANGLE")/* -(-5.0) degrees */;

		m_spinAllFlexionAngle = new QDoubleSpinBox();
		m_spinAllFlexionAngle->setRange(allFlexionAngleMin, allFlexionAngleMax);// 0 ~ 8 degrees
		m_spinAllFlexionAngle->setSingleStep(1.0);
		m_spinAllFlexionAngle->setEnabled(true);
		m_toolbar->addWidget(m_spinAllFlexionAngle);
		}

		if (0) // We do not use this input in iTW6
		{
			QLabel* labelTextAnteriorCutAngle = new QLabel(tr("Ant F %1:").arg(QChar(0x00B0)));
			m_toolbar->addWidget(labelTextAnteriorCutAngle);
			labelTextAnteriorCutAngle->setFont( m_fontBold );
			double anteriorCutMaxAngle = 85.0;// 85.0 degrees
			double anteriorCutMinAngle = 82.0;// 82.0 degrees
			m_spinAntCutFlexionAngle = new QDoubleSpinBox();
			m_spinAntCutFlexionAngle->setRange(anteriorCutMinAngle, anteriorCutMaxAngle);// 82 ~ 85 degrees
			m_spinAntCutFlexionAngle->setSingleStep(0.5);
			m_spinAntCutFlexionAngle->setEnabled(true);
			m_toolbar->addWidget(m_spinAntCutFlexionAngle);

			connect( m_spinAntCutFlexionAngle, SIGNAL( valueChanged(double) ), this, SLOT( OnAntCutFlexionAngle(double) ) );
		}

		if ( m_pDoc->GetVarTableValue("FEMORAL IMPLANT DISPLAY OBLIQUE ANGLE") == 1 )
		{
			double obliqueRange = m_pDoc->GetVarTableValue("FEMORAL IMPLANT OBLIQUE ANGLE MAX RANGE");
			QLabel* labelTextObliqueAngle = new QLabel(QString("Ant O %1:").arg(QChar(0x00B0)));
			m_toolbar->addWidget(labelTextObliqueAngle);
			labelTextObliqueAngle->setFont( m_fontBold );
			m_spinAntCutObliqueAngle = new QDoubleSpinBox();
			m_spinAntCutObliqueAngle->setRange(-obliqueRange, obliqueRange);
			m_spinAntCutObliqueAngle->setSingleStep(0.5);
			m_spinAntCutObliqueAngle->setEnabled(true);
			m_toolbar->addWidget(m_spinAntCutObliqueAngle); 
			m_pMainWindow->connect( m_spinAntCutObliqueAngle, SIGNAL( valueChanged(double) ), this, SLOT( OnAntCutObliqueAngle(double) ) );
		}

		if ( m_pDoc->GetVarTableValue("FEMORAL IMPLANT DISPLAY ABD ADD ROTATION") == 1 )
		{
			double intExtRange = m_pDoc->GetVarTableValue("FEMORAL IMPLANT ABD ADD ROTATION MAX RANGE");
			QLabel* labelTextCoordinateYAngle = new QLabel(QString("AbdAdd %1:").arg(QChar(0x00B0)));
			m_toolbar->addWidget(labelTextCoordinateYAngle);
			labelTextCoordinateYAngle->setFont( m_fontBold );
			m_spinFemoralAxesYRotAngle = new QDoubleSpinBox();
			m_spinFemoralAxesYRotAngle->setRange(-intExtRange, intExtRange);
			m_spinFemoralAxesYRotAngle->setSingleStep(0.5);
			m_spinFemoralAxesYRotAngle->setEnabled(true);
			m_toolbar->addWidget(m_spinFemoralAxesYRotAngle); 
			m_pMainWindow->connect( m_spinFemoralAxesYRotAngle, SIGNAL( valueChanged(double) ), this, SLOT( OnFemoralAxesYRotAngle(double) ) );
		}

		if ( m_pDoc->GetVarTableValue("FEMORAL IMPLANT DISPLAY INT EXT ROTATION") == 1 )
		{
			double intExtRange = m_pDoc->GetVarTableValue("FEMORAL IMPLANT INT EXT ROTATION MAX RANGE");
			QLabel* labelTextCoordinateZAngle = new QLabel(QString("IntExt %1:").arg(QChar(0x00B0)));
			m_toolbar->addWidget(labelTextCoordinateZAngle);
			labelTextCoordinateZAngle->setFont( m_fontBold );
			m_spinFemoralAxesZRotAngle = new QDoubleSpinBox();
			m_spinFemoralAxesZRotAngle->setRange(-intExtRange, intExtRange);
			m_spinFemoralAxesZRotAngle->setSingleStep(0.5);
			m_spinFemoralAxesZRotAngle->setEnabled(true);
			m_toolbar->addWidget(m_spinFemoralAxesZRotAngle); 
			m_pMainWindow->connect( m_spinFemoralAxesZRotAngle, SIGNAL( valueChanged(double) ), this, SLOT( OnFemoralAxesZRotAngle(double) ) );
		}

		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		m_actCancel = m_toolbar->addAction("Cancel");
		EnableAction( m_actCancel, true );

		connect( m_actRotateCCW, SIGNAL( triggered() ), this, SLOT( OnRotateCCW() ) );
		connect( m_actRotateCW, SIGNAL( triggered() ), this, SLOT( OnRotateCW() ) );
		connect( m_spinAllFlexionAngle, SIGNAL( valueChanged(double) ), this, SLOT( OnAllFlexionAngle(double) ) );
		connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );

	}
	else if (manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Femoral Axes");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, false );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );

		m_actMakeFemoralAxes = m_toolbar->addAction("FemAxes");
		SetActionAsTitle( m_actMakeFemoralAxes );
		connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	// Get the runtime data
	CFemoralAxes*			pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL || /*if no runtime data */
		 manActType == MAN_ACT_TYPE_RESET ) 
	{
		// We want the advanced control be the first entity
		m_pDoc->GetAdvancedControl(); // This function will create advCtrl if not exist;
		//
		pFemoralAxes = new CFemoralAxes( m_pDoc );
		m_pDoc->AddEntity( pFemoralAxes, true );

		m_oldFemuralAxesExist = false;
		
		// Reset is the way to determine the hipPoint/proximalPoint/left/rightCondylarPoints.
		Reset( activateUI );
		// The searching list is updated inside Reset()
	}
	else
	{
		m_oldFemuralAxesExist = true;
		pFemoralAxes->GetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);
		m_validateStatus = pFemoralAxes->GetValidateStatus(m_validateMessage);
		// keep the original data for cancel
		pFemoralAxes->GetParams(m_oldHipPoint, m_oldProximalPoint, m_oldLeftEpiCondylarPoint, m_oldRightEpiCondylarPoint, m_oldAntCutFlexionAngle, m_oldAntCutObliqueAngle, m_oldFemoralAxesFlexionAngle, m_oldAntCutPlaneCenter,  m_oldAntCutPlaneRotationCenter, m_oldFemoralAxesYRotAngle, m_oldFemoralAxesZRotAngle);
		m_oldUpToDateStatus = pFemoralAxes->GetUpToDateStatus();
		m_oldValidateStatus = pFemoralAxes->GetValidateStatus(m_oldValidateMessage);
		// Determine ideal notch point as its reference point
		bool bDeviated, bBiSect;
		m_proximalPointReference = m_proximalPoint;
		RefineFemoralOrigin(m_pDoc, m_proximalPointReference, bDeviated, bBiSect);

		if ( activateUI )// activate UI
		{
			// set validate icon & message
			SetValidateIcon(m_validateStatus, m_validateMessage);
			// set angles
			double maxAntCutAngle = 85.0; // 85 degrees
			m_allFlexionAngle = (maxAntCutAngle-m_antCutFlexionAngle) - 1.0*m_femoralAxesFlexionAngle;// m_femoralAxesFlexionAngle is a negative value
			if ( m_spinAllFlexionAngle )
				m_spinAllFlexionAngle->setValue(m_allFlexionAngle);
			if ( m_spinAntCutFlexionAngle )
				m_spinAntCutFlexionAngle->setValue(m_antCutFlexionAngle);
			if ( m_spinAntCutObliqueAngle )
				m_spinAntCutObliqueAngle->setValue(m_antCutObliqueAngle);
			if ( m_spinFemoralAxesFlexionAngle )
				m_spinFemoralAxesFlexionAngle->setValue(-1.0*m_femoralAxesFlexionAngle);
			if ( m_spinFemoralAxesYRotAngle )
				m_spinFemoralAxesYRotAngle->setValue(m_femoralAxesYRotAngle);
			if ( m_spinFemoralAxesZRotAngle )
				m_spinFemoralAxesZRotAngle->setValue(m_femoralAxesZRotAngle);
		}

		// if need to refine the femoral axes
		if ( manActType == MAN_ACT_TYPE_REFINE )
		{
			//Refine( false );
		}
		if ( activateUI )
		{
			//OnValidate();
		}
		// Set the searching list
		IwTArray<IwPoint3d>	searchList;
		searchList.Add(m_proximalPoint);
		searchList.Add(m_leftEpiCondylarPoint);
		searchList.Add(m_rightEpiCondylarPoint);
		searchList.Add(m_antCutPlaneRotationCenter);
		SetMouseMoveSearchingList(searchList);
	}

	// Get the validation land marks
	if ( pFemoralAxes && m_pDoc->DisplayValidationData() ) // only get the validation points when is in readOnly mode
		pFemoralAxes->GetParamsValidation(m_hipPointValidation, m_proximalPointValidation, m_leftEpiCondylarPointValidation, m_rightEpiCondylarPointValidation,
										  m_antCutFlexionAngleValidation, m_antCutObliqueAngleValidation, m_femoralAxesFlexionAngleValidation, m_antCutPlaneCenterValidation);

	m_leftButtonDown = false;
	m_valueChangedFromUndo = false;

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

	m_pView->Redraw();
}

CMakeFemoralAxes::~CMakeFemoralAxes()
{
	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes != NULL )
	{
		int previoisDesignTime = pFemoralAxes->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		pFemoralAxes->SetDesignTime(previoisDesignTime+thisDesignTime);
	}

	SwitchBoard::removeSignalSender(this);
}

bool CMakeFemoralAxes::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	bool continuousResponse = true;
    m_posStart = cursor;
	m_leftButtonDown = true;

	IwPoint3d caughtPnt;
	int caughtIndex;
	if ( GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
	{
		continuousResponse = false;
	}

	if (m_manActType == MAN_ACT_TYPE_REVIEW) 
	{
		m_leftButtonDown = false;// no response to MouseUp if under review
	}

	m_pView->Redraw();

	return continuousResponse;
}

bool CMakeFemoralAxes::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // Remember if under review, m_leftButtonDown is always false.
	{
		// if the clicked-down is the same as the button-up point, change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}
	else if ( m_leftButtonDown ) 
	{
		IwPoint3d caughtPoint;
		int caughtIndex;
		if ( GetMouseMoveCaughtPoint(caughtPoint, caughtIndex) )
		{
			m_pDoc->AppendLog( QString("CMakeFemoralAxes::MouseUp(), caughtIndex: %1, caughtPoint: %2 %3 %4").arg(caughtIndex).arg(caughtPoint.x).arg(caughtPoint.z).arg(caughtPoint.z) );

			// if the clicked-down is the same as the button-up point
			if (m_posStart == cursor)
			{
				if ( caughtIndex == 3 ) // anterior cut plane center point
				{
					m_pDoc->GetFemoralAxes()->DisplayAntCutPlane();
				}
				m_leftButtonDown = false;
				return false;
			}

			CMakeFemoralAxesUndoData prevUndoData, postUndoData;
			// Get the undo data before any changes
			GetMakeFemoralAxesUndoData(prevUndoData);

			// update m_hipPoint/m_proximalPoint/m_leftEpiCondylarPoint/m_rightEpiCondylarPoint
			IwPoint3d intersectPnt;
			IwVector3d downVector = m_pView->GetViewingVector();
			if (PierceBrep(m_pFemurBrep, (caughtPoint-100*downVector), downVector, intersectPnt))
			{
				if (caughtIndex == 0)
				{
					m_proximalPoint = (IwPoint3d)intersectPnt;
					BisectFemoralOrigin(false);// automatically move notch point to bisect plane.
				}
				else if (caughtIndex == 1)
				{
					m_leftEpiCondylarPoint = (IwPoint3d)intersectPnt;
					BisectFemoralOrigin(false);// automatically move notch point to bisect plane.
				}
				else if (caughtIndex == 2)
				{
					m_rightEpiCondylarPoint = (IwPoint3d)intersectPnt;
					BisectFemoralOrigin(false);// automatically move notch point to bisect plane.
				}
				else if (caughtIndex == 3)// m_antCutPlaneRotationCenter
				{
					IwVector3d moveVec = (IwPoint3d)intersectPnt - m_antCutPlaneRotationCenter;
					// Also move m_antCutPlaneCenter with the same vector
					m_antCutPlaneRotationCenter = (IwPoint3d)intersectPnt;
					m_antCutPlaneCenter = m_antCutPlaneCenter + moveVec;
				}
			}
			
			// Get the undo data after changes
			GetMakeFemoralAxesUndoData(postUndoData);
			QUndoCommand *makeFemoralAxesCmd = new MakeFemoralAxesCmd( this, prevUndoData, postUndoData);
			undoStack->push( makeFemoralAxesCmd );

		}
	}

	m_leftButtonDown = false;

	m_pView->Redraw();

	return true;
}

bool CMakeFemoralAxes::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	bool continuousResponse = true;

	IwPoint3d caughtPnt;
	int caughtIndex;
	bool isPointCaught = GetMouseMoveCaughtPoint(caughtPnt, caughtIndex);

    CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
    pFemoralAxes->EnableCTScanDisplayOnTroclearPlane(isPointCaught && caughtIndex == 0 && pFemoralAxes->Is2ndItemOn());

	if ( m_leftButtonDown )
	{
		if ( isPointCaught )
		{
			MoveCaughtPoint(cursor);
			continuousResponse = false;
		}
        m_posLast = cursor;
	}
	else
	{
		CManager::MouseMove(cursor, keyModifier, vp);
	}

	m_pView->Redraw();

	return continuousResponse;
}

void CMakeFemoralAxes::Display(Viewport* vp)
{		
	glDrawBuffer( GL_BACK );

	IwVector3d viewVec = m_pView->GetViewingVector();

	IwPoint3d caughtPnt;
	int caughtIndex;
	bool isPointCaught = GetMouseMoveCaughtPoint(caughtPnt, caughtIndex);
	
	glDisable( GL_LIGHTING );
	glPointSize(7);
	glColor3ub( 255, 255, 0 );
		
	DisplayCaughtPoint(-50*viewVec);
	if (m_proximalPointReference.IsInitialized())
	{
		glPointSize(8);
		glColor3ub( 255, 0, 255 );
		IwPoint3d pnt = m_proximalPointReference -5*viewVec;
		glBegin( GL_POINTS );
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if ( caughtIndex == 0 ) // proximal point
		DisplayBiSectLine(caughtPnt, -50*viewVec);

	glPointSize(1);
	glEnable( GL_LIGHTING );
	

	// only display ideal land marks when is in readOnly mode
	if ( m_pDoc->DisplayValidationData() ) 
		DisplayValidationLandMarks(-50*viewVec);

}

void CMakeFemoralAxes::DisplayBiSectLine
(
	IwPoint3d caughtPnt, 
	IwVector3d offset
)
{
	CFemoralAxes* pFemAxes = m_pDoc->GetFemoralAxes();
	IwAxis2Placement fAxes;
	pFemAxes->GetSweepAxes(fAxes);

	if ( !fAxes.GetXAxis().IsPerpendicularTo(offset, 1.0) )
		return;

	// determine one pixel size. Do not display bi-sect line if zoom in too much.
	Viewport* vp = GetViewWidget()->GetViewport();
	const float pixelSize = vp->GetResolution_MMPerPixel();
	if ( pixelSize < 0.005 )
		return;

	IwPoint3d hipPnt, proxPnt, leftEpiPnt, rightEpiPnt, antCutPlaneCenter, antCutPlaneRotationCenter;
	double antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, femoralAxesYRotAngle, femoralAxesZRotAngle;
	pFemAxes->GetParams(hipPnt, proxPnt, leftEpiPnt, rightEpiPnt, antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, antCutPlaneCenter, antCutPlaneRotationCenter, femoralAxesYRotAngle, femoralAxesZRotAngle);

	IwPoint3d upVec = offset*fAxes.GetXAxis();
	upVec.Unitize();
	IwPoint3d midPnt = 0.5*(leftEpiPnt+rightEpiPnt) + offset;
	IwVector3d tempVec = caughtPnt - midPnt;
	midPnt = midPnt + tempVec.Dot(upVec)*upVec;// move the midPnt to the caughtPnt but still biSect both epi-condylar points
	IwPoint3d upPnt = midPnt + 15*upVec;
	IwPoint3d downPnt = midPnt - 15*upVec;

	glDisable( GL_LIGHTING );

	glLineWidth( 1.0 );
	glBegin( GL_LINES );
		glColor3ub( 30, 30, 30 );
		glVertex3d( upPnt.x, upPnt.y, upPnt.z );
		glVertex3d( downPnt.x, downPnt.y, downPnt.z );
	glEnd();

	glEnable( GL_LIGHTING );

}

void CMakeFemoralAxes::DisplayValidationLandMarks
(
	IwVector3d offset
)
{

	glDisable( GL_LIGHTING );

	glDrawBuffer( GL_BACK );

	glPointSize(6);

	glColor3ub( 0, 255, 0 );

	IwPoint3d pnt;

	glBegin( GL_POINTS );

	pnt = m_proximalPointValidation + offset; // move closer to the viewer.
	glVertex3d( pnt.x, pnt.y, pnt.z );

	pnt = m_leftEpiCondylarPointValidation + offset; // move closer to the viewer.
	glVertex3d( pnt.x, pnt.y, pnt.z );

	pnt = m_rightEpiCondylarPointValidation + offset; // move closer to the viewer.
	glVertex3d( pnt.x, pnt.y, pnt.z );

	glEnd();

	glFlush();

	glPointSize(1);

	glEnable( GL_LIGHTING );

}


void CMakeFemoralAxes::OnRotateCCW()
{
	m_pDoc->AppendLog( QString("CMakeFemoralAxes::OnRotateCCW()." ));

	CMakeFemoralAxesUndoData prevUndoData, postUndoData;
	// Get the undo data before any changes
	GetMakeFemoralAxesUndoData(prevUndoData);

	// If the value changed is from Undo, do not trigger another undo. 
	if ( m_valueChangedFromUndo )
		return;

	RotateFemur(true);
	BisectFemoralOrigin(false);

	// Get the undo data after changes
	GetMakeFemoralAxesUndoData(postUndoData);
	QUndoCommand *makeFemoralAxesCmd = new MakeFemoralAxesCmd( this, prevUndoData, postUndoData);
	undoStack->push( makeFemoralAxesCmd );

}

void CMakeFemoralAxes::OnRotateCW()
{
	m_pDoc->AppendLog( QString("CMakeFemoralAxes::OnRotateCCW()." ));

	CMakeFemoralAxesUndoData prevUndoData, postUndoData;
	// Get the undo data before any changes
	GetMakeFemoralAxesUndoData(prevUndoData);

	// If the value changed is from Undo, do not trigger another undo. 
	if ( m_valueChangedFromUndo )
		return;

	RotateFemur(false);
	BisectFemoralOrigin(false);

	// Get the undo data after changes
	GetMakeFemoralAxesUndoData(postUndoData);
	QUndoCommand *makeFemoralAxesCmd = new MakeFemoralAxesCmd( this, prevUndoData, postUndoData);
	undoStack->push( makeFemoralAxesCmd );

}

////////////////////////////////////////////////////////////////////////////////////////
// The parameters (0.85, 1.10, 18.5) in this function should be synchronized with RefineFlexionAngle()
void CMakeFemoralAxes::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CMakeFemoralAxes::Reset()") );

	CFemoralAxes*			pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return;

	QApplication::setOverrideCursor( Qt::WaitCursor );
	IwPoint3d hPnt, pPnt, lPnt, rPnt;

	map<QString, IwPoint3d> pointsMap = IFemurImplant::FemoralAxesInp_PointsDataFromStrykerLayout();

	m_hipPoint = pointsMap["HIP Point"];
	m_proximalPoint = pointsMap["Notch Point"];

	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);

	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		m_leftEpiCondylarPoint = pointsMap["Medial Epicondylar Center"];
		m_rightEpiCondylarPoint = pointsMap["Lateral Epicondylar OMP"];
	}
	else
	{
		m_leftEpiCondylarPoint = pointsMap["Lateral Epicondylar OMP"];
		m_rightEpiCondylarPoint = pointsMap["Medial Epicondylar Center"];
	}


	m_antCutFlexionAngle = 85.0;// 85.0 degrees
	m_antCutObliqueAngle = 0.0;
	m_femoralAxesFlexionAngle = -5.0;// -5.0 degrees;
	m_femoralAxesYRotAngle = 0.0;
	m_femoralAxesZRotAngle = 0.0;

	// Set the initial epicondylar points to CFemoralAxes
	// in order to obtain the femoral axes, which is needed for RefineFemoralLandMarks()
	// m_antCutPlaneCenter and m_antCutPlaneCenter are not initialized yet, but here we need to setParams to get the temp femoral axes in order to determine m_antCutPlaneCenter
	pFemoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint,
	                       	m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter,
	                       	m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);

	bool deviated, bisect;
	IwPoint3d proximalRefPoint = m_proximalPoint;
	RefineFemoralOrigin(m_pDoc, proximalRefPoint, deviated, bisect);

	// reset angles
	if ( activateUI )
	{
		double maxAntCutAngle = 85.0; // 85 degrees
		m_allFlexionAngle = (maxAntCutAngle-m_antCutFlexionAngle) - 1.0*m_femoralAxesFlexionAngle;// m_femoralAxesFlexionAngle is a negative value
		if ( m_spinAllFlexionAngle )
			m_spinAllFlexionAngle->setValue(m_allFlexionAngle);
		if ( m_spinAntCutFlexionAngle )
			m_spinAntCutFlexionAngle->setValue(m_antCutFlexionAngle);
		if ( m_spinAntCutObliqueAngle )
			m_spinAntCutObliqueAngle->setValue(m_antCutObliqueAngle);
		if ( m_spinFemoralAxesFlexionAngle )
			m_spinFemoralAxesFlexionAngle->setValue(-1.0*m_femoralAxesFlexionAngle);
		if ( m_spinFemoralAxesYRotAngle )
			m_spinFemoralAxesYRotAngle->setValue(m_femoralAxesYRotAngle);
		if ( m_spinFemoralAxesZRotAngle )
			m_spinFemoralAxesZRotAngle->setValue(m_femoralAxesZRotAngle);
	}

	// Set the searching list
	IwTArray<IwPoint3d>	searchList;
	searchList.Add(m_proximalPoint);
	searchList.Add(m_leftEpiCondylarPoint);
	searchList.Add(m_rightEpiCondylarPoint);
	searchList.Add(m_antCutPlaneRotationCenter);
	SetMouseMoveSearchingList(searchList);

	// Update status sign
	m_pDoc->SetEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false);

	m_pView->Redraw();

	QApplication::restoreOverrideCursor();
}

///////////////////////////////////////////////////////////////////////////
// This function bi-sect the femur. Determines the ideal proximalPoint 
// at the -35 degree view. Then refine it to the high curvature location.
///////////////////////////////////////////////////////////////////////////
bool CMakeFemoralAxes::RefineFemoralOrigin
(
	CTotalDoc* pDoc,			// I:
	IwPoint3d& proximalPoint,	// I/O: proximal point
	bool& bDeviated,			// O: whether the proximalPoint is changed (which is not on the ideal location)
	bool& bBiSect,				// O: whether the proximalPoint is bisect the epi-condylar landmarks
	double tolDist				// I:
)
{
	bool answerIsReliable = false;

	bDeviated = false;
	bBiSect = true;
	double biSectTol = 0.01;

	IwPoint3d newProximalPoint;

	// Get the FemoralAxes info
	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return answerIsReliable;

	IwAxis2Placement mechAxes;
	pFemoralAxes->GetMechanicalAxes(mechAxes);
	double refSizeRatio = pFemoralAxes->GetEpiCondyleSize()/76.0;
	
	CFemoralPart* femPart = pDoc->GetFemur();
	if ( femPart == NULL )
		return answerIsReliable;
	IwBSplineSurface *mainSurf;
	mainSurf = femPart->GetSinglePatchSurface();

	// bi-sect main femur surface 
	IwTArray<IwCurve*> intCurves;
	bool bInt = IntersectSurfaceByPlane(mainSurf, mechAxes.GetOrigin(), mechAxes.GetXAxis(), intCurves);
	if ( !bInt ) 
		return answerIsReliable;

	// farNYZPoint is in the negative y-z side of far point
	IwPoint3d farNYZPoint = proximalPoint - 0.5*100*refSizeRatio*mechAxes.GetYAxis() - 0.866*100*refSizeRatio*mechAxes.GetZAxis();// 30 degree away mechanical axial axis.
	IwPoint3d farDistalPoint = proximalPoint - 100*refSizeRatio*mechAxes.GetZAxis();
	// determine the closest point to the farNYZPoint
	IwPoint3d cPnt, minPnt, distalPnt;
	IwCurve *crv, *minCrv, *distalCrv;
	double param, minParam, distalParam;
	double dist, minDist=1000000, distalDist=1000000;
	for (unsigned i=0; i<intCurves.GetSize(); i++)
	{
		crv = intCurves.GetAt(i);
		dist = DistFromPointToCurve(farNYZPoint, (IwBSplineCurve*)crv, cPnt, param);
		if ( dist > 0 )
		{
			if ( dist < minDist )
			{
				minDist = dist;
				minPnt = cPnt;
				minCrv = crv;
				minParam = param;
			}
		}
		dist = DistFromPointToCurve(farDistalPoint, (IwBSplineCurve*)crv, cPnt, param);
		if ( dist > 0 )
		{
			if ( dist < distalDist )
			{
				distalDist = dist;
				distalPnt = cPnt;
				distalCrv = crv;
				distalParam = param;

			}
		}
	}

	if ( minDist==1000000 )
		return answerIsReliable;

	// But the ideal point (35 degree away) should not be too far away from the most distal point (something wrong)
	if ( distalPnt.DistanceBetween(minPnt) > 12.5*refSizeRatio )
	{
		minCrv = distalCrv;
		minParam = distalParam;
	}

	IwPoint3d hPnt;
	double kValue, k, maxK = -1000000;
	IwExtent1d intval = minCrv->GetNaturalInterval();
	IwVector3d vec, evals[3];
	// determine the values at minParam first.
	minCrv->EvaluateGeometric(minParam, 2, FALSE, evals);
	vec = evals[2];
	kValue = vec.Length();
	// the default proximalPoint point is the closest point
	newProximalPoint = evals[0];

	//// determine high curvature location
	//// Determine the values for +- 5 points
	//for (double p=(minParam-5); p<(minParam+5.01); p+=1.0)
	//{
	//	if ( p < intval.GetMin() ) 
	//		continue;
	//	if ( p > intval.GetMax() )
	//		continue;
	//	minCrv->EvaluateGeometric(p, 2, FALSE, evals);
	//	vec = evals[2];
	//	k = vec.Length();
	//	// here is looking for the significant high curvature point, rather than the highest curvature only.
	//	if ( k > 2.0*kValue ) 
	//	{
	//		if ( k > maxK )
	//		{
	//			maxK = k;
	//			hPnt = evals[0];
	//		}
	//	}
	//}
	//// update proximalPoint if high curvature point exists.
	//if ( maxK > -1000000 )
	//	newProximalPoint = hPnt;

	// check ML distance, always use biSectTol=0.01 as tolerance
	IwVector3d tempVec = newProximalPoint - proximalPoint;
	if ( fabs(tempVec.Dot(mechAxes.GetXAxis())) > biSectTol )
	{
		proximalPoint = newProximalPoint;
		bDeviated = true;
		bBiSect = false;
	}
	// check 3D distance
	if ( newProximalPoint.DistanceBetween(proximalPoint) > tolDist ) 
	{
		proximalPoint = newProximalPoint;
		bDeviated = true;
	}

	answerIsReliable = true;

	pFemoralAxes->SetProximalRefPoint(proximalPoint);

	return answerIsReliable;
}

/////////////////////////////////////////////////////////////////////////////////
// This function force the femoral notch point onto the bi-sect plane.
// This function also determines the reference point for the femoral notch point.
/////////////////////////////////////////////////////////////////////////////////
void CMakeFemoralAxes::BisectFemoralOrigin(bool forceNotchToIdealLocation)
{

	// Get the FemoralAxes info
	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return;
	IwAxis2Placement sweepingAxes;

	CFemoralPart* femPart = m_pDoc->GetFemur();
	if ( femPart == NULL )
		return;
	IwBSplineSurface *mainSurf;
	mainSurf = femPart->GetSinglePatchSurface();

	// Set the new m_proximalPoint to CFemoralAxes
	// in order to update the femoral axes;
	pFemoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);
	// Need to run many times to get a converge solution
	double dist=100;
	IwPoint2d param2d;
	for (unsigned i=0; i<5; i++)
	{
		pFemoralAxes->GetSweepAxes(sweepingAxes);

		// project m_proximalPoint onto YZ plane
		m_proximalPoint = m_proximalPoint.ProjectPointToPlane(sweepingAxes.GetOrigin(), sweepingAxes.GetXAxis());
	
		// update m_proximalPoint
		pFemoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);

		// Determine the closest point on mainSurf
		dist = DistFromPointToSurface(m_proximalPoint, mainSurf, m_proximalPoint, param2d);

		if (dist < 0.005)
			break;
	}

	// In case weird geometry
	if ( dist > 0.1 )
	{
		IwTArray<IwCurve*> intCurves;
		IntersectSurfaceByPlane(mainSurf, sweepingAxes.GetOrigin(), sweepingAxes.GetXAxis(), intCurves);
		DistFromPointToCurves(m_proximalPoint, intCurves, m_proximalPoint);
		pFemoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);
	}

	// Also create the femoral origin reference point 
	bool deviated, bisect;
	if (forceNotchToIdealLocation)
	{
		RefineFemoralOrigin(m_pDoc, m_proximalPoint, deviated, bisect);
		pFemoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);
		m_proximalPointReference = m_proximalPoint;
	}
	else
	{
		RefineFemoralOrigin(m_pDoc, m_proximalPointReference, deviated, bisect);
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////////
// CMakeFemoralAxes object always displays the update-to-date femoral axes and 
// feature points on screen. Therefore, OnAccept only needs to compare the old data
// with the current ones. OnCancel only needs to set back the old data.
////////////////////////////////////////////////////////////////////////////////////
void CMakeFemoralAxes::OnAccept()
{
	m_pDoc->AppendLog( QString("CMakeFemoralAxes::OnAccept()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("MakeFemoralAxes");
	
	//Supply values to a task
	//vector<string> values;
	//values.push_back(QString("%1").arg(1.0).toStdString());
	//values.push_back(QString("%1").arg(2.0).toStdString());

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
	{
		// Make JCOSToolbar visible
		m_pMainWindow->SetJCOSToolbarVisible(true);
	}

	Accept();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CMakeFemoralAxes::Accept(bool activateUI)
{
	m_pDoc->AppendLog( QString("CMakeFemoralAxes::Accept()") );

	// overwrite it
	activateUI = ( m_manActType == MAN_ACT_TYPE_EDIT );

	// Check whether the data is really changed.
	bool modified = false;
	if ( m_oldProximalPoint.IsInitialized() )
	{
		modified = (m_proximalPoint != m_oldProximalPoint) || 
					(m_leftEpiCondylarPoint != m_oldLeftEpiCondylarPoint) || 
					(m_rightEpiCondylarPoint != m_oldRightEpiCondylarPoint ||
					(m_antCutPlaneRotationCenter != m_oldAntCutPlaneRotationCenter) ||
				    !IS_EQ_TOL6(m_antCutFlexionAngle, m_oldAntCutFlexionAngle) ||
				    !IS_EQ_TOL6(m_antCutObliqueAngle, m_oldAntCutObliqueAngle) ||
					!IS_EQ_TOL6(m_femoralAxesFlexionAngle, m_oldFemoralAxesFlexionAngle) ||
					!IS_EQ_TOL6(m_femoralAxesYRotAngle, m_oldFemoralAxesYRotAngle) ||
					!IS_EQ_TOL6(m_femoralAxesZRotAngle, m_oldFemoralAxesZRotAngle) );
	}

	CFemoralAxes* femoralAxes = m_pDoc->GetFemoralAxes();
	if (femoralAxes)
	{
		femoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);
		//OnValidate();
	}

	// When users click accept, the latest data is sent to femoralAxes.
	// Therefore, it should always be up-to-date when accept.
	// The "*" should be deleted when it is modified or "*" is showed.
	// Remember, AdvancedControl may change the data and make "*" show up 
	// but the oldData here is the same as the current data.
	// However, if the femoralAxes is up-to-date, and users just enter this action, do nothing, and exit,
	// it should not update the status sign again.
	if ( !femoralAxes->GetUpToDateStatus() || modified ) 
		m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES);

	// if users modify femoral axes. m_oldFemuralAxesExist
	if ( activateUI && modified && (m_pDoc->GetOutlineProfile() || m_pDoc->GetFemoralCuts()) )
	{
		// Empty viewProfiles
		m_pDoc->GetFemur()->EmptyViewProfiles();

		if (m_pDoc->GetAdvancedControl()->GetAutoNone()) // Delete entities after Femoral Axes
		{
			QMessageBox::warning( NULL, 
								  QString("Femoral Axes!"), 
								  QString("Femoral axes have been modified.\nJCO steps will be deleted."), 
								  QMessageBox::Ok);	
			m_pDoc->DeleteEntitiesAfterFemoralAxes();
		}
		else // Regenerate JOC steps
		{
			QMessageBox::warning( NULL, 
								  QString("Femoral Axes!"), 
								  QString("Femoral axes have been modified.\nJCO steps need to be reinitialized."), 
								  QMessageBox::Ok);	
		
			GetView()->OnAutoInitializeFromFemoralAxes();
		}
	}
	else if ( activateUI && m_pDoc->GetAdvancedControl()->GetAutoMajorSteps() && 
		      ( m_pDoc->GetOutlineProfile()==NULL && m_pDoc->GetFemoralCuts()==NULL) )
	{
		// Empty viewProfiles
		m_pDoc->GetFemur()->EmptyViewProfiles();
		
		GetView()->OnAutoInitializeFromFemoralAxes();
	}
	
	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

}

void CMakeFemoralAxes::OnCancel()
{
	m_pDoc->AppendLog( QString("CMakeFemoralAxes::OnCancel()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("MakeFemoralAxes");

	CFemoralAxes*		pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( !m_oldFemuralAxesExist ) // Delete the newly created one.
	{
		if (pFemoralAxes) // check again, in case
		{
			m_pDoc->DeleteEntityById( pFemoralAxes->GetEntId() );
			m_pMainWindow->EnableMakingActions(false);// disable all,
			m_pMainWindow->EnableMakingActions(true);// then enable again.
		}
	}
	else // Set back to the old ones.
	{
		pFemoralAxes->SetParams(m_oldHipPoint, m_oldProximalPoint, m_oldLeftEpiCondylarPoint, m_oldRightEpiCondylarPoint, m_oldAntCutFlexionAngle, m_oldAntCutObliqueAngle, m_oldFemoralAxesFlexionAngle, m_oldAntCutPlaneCenter, m_oldAntCutPlaneRotationCenter, m_oldFemoralAxesYRotAngle, m_oldFemoralAxesZRotAngle);
		m_pDoc->SetEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, m_oldUpToDateStatus);
		m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_FEMORAL_AXES, m_oldValidateStatus, m_oldValidateMessage);
		pFemoralAxes->SetValidateStatus(m_oldValidateStatus, m_oldValidateMessage);
	}

	// Make JCOSToolbar visible
	if ( m_manActType == MAN_ACT_TYPE_EDIT )
	{
		m_pMainWindow->SetJCOSToolbarVisible(true);
	}

	m_bDestroyMe = true;
	m_pView->Redraw();

}

bool CMakeFemoralAxes::OnReviewNext()
{
	GetView()->OnActivateNextReviewManager(this, ENT_ROLE_FEMORAL_AXES);
	return true;
}

bool CMakeFemoralAxes::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;

	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_FEMORAL_AXES))
    	OnAccept();
	return true;
}

void CMakeFemoralAxes::SetValidateIcon(CEntValidateStatus vStatus, QString message)
{
	if ( m_actValidate )
	{
		//m_actValidate->setToolTip( message );
		if ( vStatus == VALIDATE_STATUS_RED )
		{
			//m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Red.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_YELLOW )
		{
			//m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Yellow.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_GREEN )
		{
			//m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Green.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			//m_actValidate->setToolTip( QString("Not validate.") );
			//m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate.png" ) );
		}
	}
}

void CMakeFemoralAxes::GetMakeFemoralAxesUndoData(CMakeFemoralAxesUndoData& undoData)
{
	undoData.hipPoint = m_hipPoint;
	undoData.proximalPoint = m_proximalPoint;
	undoData.leftEpiCondylarPoint = m_leftEpiCondylarPoint;
	undoData.rightEpiCondylarPoint = m_rightEpiCondylarPoint;
	undoData.allFlexionAngle = m_allFlexionAngle;
	undoData.antCutFlexionAngle = m_antCutFlexionAngle;
	undoData.antCutObliqueAngle = m_antCutObliqueAngle;
	undoData.femoralAxesFlexionAngle = m_femoralAxesFlexionAngle;
	undoData.femoralAxesYRotAngle = m_femoralAxesYRotAngle;
	undoData.femoralAxesZRotAngle = m_femoralAxesZRotAngle;
	undoData.antCutPlaneCenter = m_antCutPlaneCenter;
}

void CMakeFemoralAxes::SetMakeFemoralAxesUndoData(CMakeFemoralAxesUndoData& undoData)
{
	m_hipPoint = undoData.hipPoint;
	m_proximalPoint = undoData.proximalPoint;
	m_leftEpiCondylarPoint = undoData.leftEpiCondylarPoint;
	m_rightEpiCondylarPoint = undoData.rightEpiCondylarPoint;
	m_allFlexionAngle = undoData.allFlexionAngle;
	m_antCutFlexionAngle = undoData.antCutFlexionAngle;
	m_antCutObliqueAngle = undoData.antCutObliqueAngle;
	m_femoralAxesFlexionAngle = undoData.femoralAxesFlexionAngle;
	m_femoralAxesYRotAngle = undoData.femoralAxesYRotAngle;
	m_femoralAxesZRotAngle = undoData.femoralAxesZRotAngle;
	m_antCutPlaneCenter = undoData.antCutPlaneCenter;
	
	if ( m_spinAllFlexionAngle )
	{
		m_valueChangedFromUndo = true;
		m_spinAllFlexionAngle->setValue(m_allFlexionAngle);
		m_valueChangedFromUndo = false;
	}
	if ( m_spinAntCutFlexionAngle )
	{
		m_valueChangedFromUndo = true;
		m_spinAntCutFlexionAngle->setValue(m_antCutFlexionAngle);
		m_valueChangedFromUndo = false;
	}
	if ( m_spinAntCutObliqueAngle )
	{
		m_valueChangedFromUndo = true;
		m_spinAntCutObliqueAngle->setValue(m_antCutObliqueAngle);
		m_valueChangedFromUndo = false;
	}
	if ( m_spinFemoralAxesFlexionAngle )
	{
		m_valueChangedFromUndo = true;
		m_spinFemoralAxesFlexionAngle->setValue(-1.0*m_femoralAxesFlexionAngle);// its negative
		m_valueChangedFromUndo = false;
	}
	if ( m_spinFemoralAxesYRotAngle )
	{
		m_valueChangedFromUndo = true;
		m_spinFemoralAxesYRotAngle->setValue(m_femoralAxesYRotAngle);
		m_valueChangedFromUndo = false;
	}
	if ( m_spinFemoralAxesZRotAngle )
	{
		m_valueChangedFromUndo = true;
		m_spinFemoralAxesZRotAngle->setValue(m_femoralAxesZRotAngle);
		m_valueChangedFromUndo = false;
	}

	// Update to Femoral Axes
	CFemoralAxes*			femoralAxes = m_pDoc->GetFemoralAxes();
	if (femoralAxes)
	{
		femoralAxes->SetParams(m_hipPoint, m_proximalPoint, m_leftEpiCondylarPoint, m_rightEpiCondylarPoint, m_antCutFlexionAngle, m_antCutObliqueAngle, m_femoralAxesFlexionAngle, m_antCutPlaneCenter, m_antCutPlaneRotationCenter, m_femoralAxesYRotAngle, m_femoralAxesZRotAngle);
		// Whenever parameters are changed, the validation status is invalid.
		m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
		m_validateMessage = QString("Not validate.");
		SetValidateIcon(m_validateStatus, m_validateMessage);
		m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_FEMORAL_AXES, m_validateStatus, m_validateMessage);
	}

	// Also update MouseMoveSearchingList
	IwTArray<IwPoint3d> searchList;
	searchList.Add(m_proximalPoint);
	searchList.Add(m_leftEpiCondylarPoint);
	searchList.Add(m_rightEpiCondylarPoint);
	searchList.Add(m_antCutPlaneRotationCenter);

	SetMouseMoveSearchingList(searchList);

	// Update status sign
	m_pDoc->SetEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false);

	m_pView->Redraw();
}

void CMakeFemoralAxes::RotateFemur(bool CCW)
{

	// Get the info
	CFemoralPart* pFemur = m_pDoc->GetFemur();
	if ( pFemur == NULL )
		return;
	IwBrep* femurBrep = pFemur->GetIwBrep();

	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return;

	IwAxis2Placement femAxes;
	pFemoralAxes->GetSweepAxes(femAxes);

	double refSize = pFemoralAxes->GetEpiCondyleSize();

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	double sign;// = CCW ? 1.0 : -1.0;
	double quarterDegreeDist = 0.004363*refSize; // about 0.25 degree moving distance
	IwVector3d vec;
	// 
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		sign = CCW ? -1.0 : 1.0; // how to move the medial (patient's left) m_leftEpiCondylarPoint
		m_leftEpiCondylarPoint += sign*femAxes.GetYAxis();
		// Make m_leftEpiCondylarPoint on the femur surface
		vec = m_rightEpiCondylarPoint - m_leftEpiCondylarPoint;
		vec.Unitize();
		PierceBrep(femurBrep, m_leftEpiCondylarPoint-10*vec, vec, m_leftEpiCondylarPoint);
	}
	else
	{
		positiveSideIsLateral = false;
		sign = CCW ? 1.0 : -1.0; // how to move the medial (patient's right) m_rightEpiCondylarPoint
		m_rightEpiCondylarPoint += sign*femAxes.GetYAxis();
		// Make m_rightEpiCondylarPoint on the femur surface
		vec = m_leftEpiCondylarPoint - m_rightEpiCondylarPoint;
		vec.Unitize();
		PierceBrep(femurBrep, m_rightEpiCondylarPoint-10*vec, vec, m_rightEpiCondylarPoint);
	}
}

void CMakeFemoralAxes::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 4;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 ) // lateral side view
	{
		// Display femur
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		if ( m_pDoc->IsPositiveSideLateral() ) // right knee
			vp->RightView(false);
		else
			vp->LeftView(false);
	}
	else if ( viewIndex == 1 ) // medial side view
	{
		if ( m_pDoc->IsPositiveSideLateral() ) // right knee
			vp->LeftView(false);
		else
			vp->RightView(false);
	}
	else if ( viewIndex == 2) // bottom view
	{
		vp->BottomView(false);
	}
	else if ( viewIndex == 3) // lateral side view
	{
		// Display femur in transparent
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		if ( m_pDoc->IsPositiveSideLateral() ) // right knee
			vp->RightView(false);
		else
			vp->LeftView(false);
	}

	m_predefinedViewIndex++;
}