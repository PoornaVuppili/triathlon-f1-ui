#include "UndoRedoCmdJigs.h"

///////////////////////////////////////////////////////////////////////////
// Define Cartilage
///////////////////////////////////////////////////////////////////////////
DefineCartilageCmd::DefineCartilageCmd
(
	CManager* manager, 
	CDefineCartilageUndoData &prevUndoData, 
	CDefineCartilageUndoData &postUndoData, 
	QUndoCommand *parent)
{
	m_pManager			= manager;
	m_prevUndoData = prevUndoData;
	m_postUndoData = postUndoData;
}
void DefineCartilageCmd::undo()
{
	if (m_pManager->GetClassName() != "CDefineCartilage") return;

	CDefineCartilage* defineCartilage = (CDefineCartilage*) m_pManager;
	defineCartilage->SetDefineCartilageUndoData(m_prevUndoData);
}

void DefineCartilageCmd::redo()
{
	if (m_pManager->GetClassName() != "CDefineCartilage") return;

	CDefineCartilage* defineCartilage = (CDefineCartilage*) m_pManager;
	defineCartilage->SetDefineCartilageUndoData(m_postUndoData);
}

///////////////////////////////////////////////////////////////////////////
// Define Outline Profile
///////////////////////////////////////////////////////////////////////////
DefineOutlineProfileJigsCmd::DefineOutlineProfileJigsCmd
(
	CManager* manager, 
	CDefineOutlineProfileJigsUndoData &prevUndoData, 
	CDefineOutlineProfileJigsUndoData &postUndoData, 
	QUndoCommand *parent)
{
	m_pManager			= manager;
	
	m_prevUndoData = prevUndoData;
	m_postUndoData = postUndoData;
}
void DefineOutlineProfileJigsCmd::undo()
{
	if (m_pManager->GetClassName() != "CDefineOutlineProfileJigs") return;

	CDefineOutlineProfileJigs* defineOutlineProfileJigs = (CDefineOutlineProfileJigs*) m_pManager;
	defineOutlineProfileJigs->SetDefineOutlineProfileJigsUndoData(m_prevUndoData);
	defineOutlineProfileJigs->ReDraw();
}

void DefineOutlineProfileJigsCmd::redo()
{
	if (m_pManager->GetClassName() != "CDefineOutlineProfileJigs") return;

	CDefineOutlineProfileJigs* defineOutlineProfileJigs = (CDefineOutlineProfileJigs*) m_pManager;
	defineOutlineProfileJigs->SetDefineOutlineProfileJigsUndoData(m_postUndoData);
	defineOutlineProfileJigs->ReDraw();
}

///////////////////////////////////////////////////////////////////////////
// Make Outer Surface Jigs
///////////////////////////////////////////////////////////////////////////
MakeOuterSurfaceJigsCmd::MakeOuterSurfaceJigsCmd
(
	CManager* manager, 
	CMakeOuterSurfaceJigsUndoData &prevUndoData, 
	CMakeOuterSurfaceJigsUndoData &postUndoData, 
	QUndoCommand *parent)
{
	m_pManager			= manager;
	m_prevUndoData = prevUndoData;
	m_postUndoData = postUndoData;
}
void MakeOuterSurfaceJigsCmd::undo()
{
	if (m_pManager->GetClassName() != "CMakeOuterSurfaceJigs") return;

	CMakeOuterSurfaceJigs* makeOuterSurfaceJigs = (CMakeOuterSurfaceJigs*) m_pManager;
	makeOuterSurfaceJigs->SetMakeOuterSurfaceJigsUndoData(m_prevUndoData);
}

void MakeOuterSurfaceJigsCmd::redo()
{
	if (m_pManager->GetClassName() != "CMakeOuterSurfaceJigs") return;

	CMakeOuterSurfaceJigs* makeOuterSurfaceJigs = (CMakeOuterSurfaceJigs*) m_pManager;
	makeOuterSurfaceJigs->SetMakeOuterSurfaceJigsUndoData(m_postUndoData);
}

///////////////////////////////////////////////////////////////////////////
// Define Stylus Jigs
///////////////////////////////////////////////////////////////////////////
DefineStylusJigsCmd::DefineStylusJigsCmd
(
	CManager* manager, 
	CDefineStylusJigsUndoData &prevUndoData, 
	CDefineStylusJigsUndoData &postUndoData, 
	QUndoCommand *parent)
{
	m_pManager			= manager;
	
	m_prevUndoData = prevUndoData;
	m_postUndoData = postUndoData;
}
void DefineStylusJigsCmd::undo()
{
	if (m_pManager->GetClassName() != "CDefineStylusJigs") return;

	CDefineStylusJigs* defineStylusJigs = (CDefineStylusJigs*) m_pManager;
	defineStylusJigs->SetDefineStylusJigsUndoData(m_prevUndoData);
	defineStylusJigs->ReDraw();
}

void DefineStylusJigsCmd::redo()
{
	if (m_pManager->GetClassName() != "CDefineStylusJigs") return;

	CDefineStylusJigs* defineStylusJigs = (CDefineStylusJigs*) m_pManager;
	defineStylusJigs->SetDefineStylusJigsUndoData(m_postUndoData);
	defineStylusJigs->ReDraw();
}
