#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "OuterSurfaceJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

COuterSurfaceJigs::COuterSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CInnerSurfaceJigs( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = darkGray;

	m_bOSJigsModified = false;
	m_bOSJigsDataModified = false;

	m_glOsteoIsoCurveList = 0;
	m_glCartiIsoCurveList = 0;

	m_offsetOsteophyteBrep = NULL;
	m_offsetCartilageBrep = NULL;
	m_bDispOsteo = false;
	m_bDispCarti = false;
	m_osteophyteTimeStamp = 1;
	m_cartilageTimeStamp = 1;
}

COuterSurfaceJigs::~COuterSurfaceJigs()
{

}

void COuterSurfaceJigs::Display()
{
	CInnerSurfaceJigs::Display();

	if( m_eDisplayMode == DISP_MODE_DISPLAY ||
		m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
		m_eDisplayMode == DISP_MODE_DISPLAY_NET ||
		m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET ||
		m_eDisplayMode == DISP_MODE_WIREFRAME )
	{
		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY || m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthMask(GL_FALSE);
		}

		// Display anterior drop surface
		glEnable( GL_LIGHTING );

		glEnable( GL_DEPTH_TEST );

		glEnable( GL_POLYGON_OFFSET_FILL );
		glPolygonOffset( 3.0, 100.0 );

		// Only generate and display iso curves for osteo and carti.
		// (Computer can not handle displaying facet. Require too much memory.) 
		if ( m_bDispOsteo )
		{
			glColor4ub( darkBrown[0], darkBrown[1], darkBrown[2], m_pDoc->GetTransparencyFactor());
			if( m_glOsteoIsoCurveList == 0 )
			{
				long glOsteoFaceList, glOsteoEdgeList;
				m_glOsteoIsoCurveList = GenerateIwBrepDisplayList(m_pDoc, m_offsetOsteophyteBrep, true, darkBrown, glOsteoFaceList, glOsteoEdgeList);
			}
			else
			{
				glCallList( m_glOsteoIsoCurveList );
			}
		}
		if ( m_bDispCarti )
		{
			glColor4ub( cPink[0], cPink[1], cPink[2], m_pDoc->GetTransparencyFactor());
			if( m_glCartiIsoCurveList == 0 )
			{
				long glCartiFaceList, glCartiEdgeList;
				m_glCartiIsoCurveList = GenerateIwBrepDisplayList(m_pDoc, m_offsetCartilageBrep, true, cPink, glCartiFaceList, glCartiEdgeList);
			}
			else
			{
				glCallList( m_glCartiIsoCurveList );
			}
		}

		glDisable( GL_POLYGON_OFFSET_FILL );

		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY || m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
		}
	}

	m_bOSJigsModified = false;

}

bool COuterSurfaceJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	bool				bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bOSJigsDataModified ) 
		return bOK;

	QString sFileNameOsteo= sDir + "\\" + m_sFileName + "_os_jigs_osteo.iwp~";

	QFile				FileOsteo( sFileNameOsteo );

	bOK = FileOsteo.open( QIODevice::WriteOnly );

	if ( bOK && m_offsetOsteophyteBrep )
	{
		WriteBrep( m_offsetOsteophyteBrep, sFileNameOsteo );
	}

	FileOsteo.close();

	//////////////////////////////
	QString sFileNameCarti= sDir + "\\" + m_sFileName + "_os_jigs_carti.iwp~";

	QFile				FileCarti( sFileNameCarti );

	bOK = FileCarti.open( QIODevice::WriteOnly );

	if ( bOK && m_offsetCartilageBrep )
	{
		WriteBrep( m_offsetCartilageBrep, sFileNameCarti );
	}

	FileCarti.close();

	//////////////////////////////////////////////
	QString sFileName = sDir + "\\" + m_sFileName + "_os_jigs.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	File.write( (char*) &m_osteophyteTimeStamp, sizeof(unsigned) );
	File.write( (char*) &m_cartilageTimeStamp, sizeof(unsigned) );

	File.close();


	m_bOSJigsDataModified = false;

	return true;
}


bool COuterSurfaceJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);


	// Variables for Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		QString sFileNameOsteo = sDir + "\\" + m_sFileName + "_os_jigs_osteo.iwp";
		QFile				FileOsteo( sFileNameOsteo );
		bool bOK = FileOsteo.open( QIODevice::ReadOnly );
		if( bOK )
			m_offsetOsteophyteBrep = ReadBrep( sFileNameOsteo );
		FileOsteo.close();
		//////////////////////////
		QString sFileNameCarti = sDir + "\\" + m_sFileName + "_os_jigs_carti.iwp";
		QFile				FileCarti( sFileNameCarti );
		bOK = FileCarti.open( QIODevice::ReadOnly );
		if( bOK )
			m_offsetCartilageBrep = ReadBrep( sFileNameCarti );
		FileCarti.close();

		//////////////////////////
		QString sFileName = sDir + "\\" + m_sFileName + "_os_jigs.dat";
		QFile				File( sFileName );
		bOK = File.open( QIODevice::ReadOnly );

		if( !bOK )
			return false;

		File.read( (char*) &m_osteophyteTimeStamp, sizeof( unsigned ) );
		File.read( (char*) &m_cartilageTimeStamp, sizeof( unsigned ) );

		File.close();
	}

	m_bOSJigsDataModified = false;

	return true;
}

void COuterSurfaceJigs::SetOSJigsModifiedFlag( bool bModified )
{
	m_bOSJigsModified = bModified;
	if (m_bOSJigsModified)
	{
		m_bOSJigsDataModified = true;
		m_pDoc->SetModified(m_bOSJigsModified);
	}
}

void COuterSurfaceJigs::SetOffsetOsteophyteBrep
(
	IwBrep*& osteoBrep
)
{
	// delete m_offsetOsteophyteBrep
	if ( m_offsetOsteophyteBrep )
	{
		IwObjDelete(m_offsetOsteophyte);
		m_offsetOsteophyte=NULL;
	}
	if( glIsList( m_glOsteoIsoCurveList ) )
	{
		glDeleteLists( m_glOsteoIsoCurveList, 1 );
		m_glOsteoIsoCurveList = 0;
	}

	m_offsetOsteophyteBrep = osteoBrep;
}

void COuterSurfaceJigs::GetOffsetOsteophyteBrep
(
	IwBrep*& osteoBrep
)
{
	osteoBrep = m_offsetOsteophyteBrep;
}

void COuterSurfaceJigs::SetOffsetCartilageBrep
(
	IwBrep*& cartiBrep
)
{
	// delete m_offsetCartilageBrep
	if ( m_offsetCartilageBrep )
	{
		IwObjDelete(m_offsetCartilage);
		m_offsetCartilage = NULL;
	}
	if( glIsList( m_glCartiIsoCurveList ) )
	{
		glDeleteLists( m_glCartiIsoCurveList, 1 );
		m_glCartiIsoCurveList = 0;
	}
	m_offsetCartilageBrep = cartiBrep;
}

void COuterSurfaceJigs::GetOffsetCartilageBrep
(
	IwBrep*& cartiBrep
)
{
	cartiBrep = m_offsetCartilageBrep;
}
