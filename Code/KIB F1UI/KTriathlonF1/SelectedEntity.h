#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class TEST_EXPORT_TW CSelectedEntity
{
public:
					CSelectedEntity();
					~CSelectedEntity();
	void			Display(IwVector3d offsetVec=IwVector3d(0,0,0), double onePixelSize=0 );
	void			DisplayClosestPoint();
	void			DisplayHitPoint();
	IwPoint3d		GetClosestPoint() {return closestPoint;};
	void			Empty();
	bool			IsEmpty();
	bool			IsClosestPointValid();
	void			AddPoint(IwPoint3d& pnt);
	void			AddCurve(IwCurve*& crv, IwPoint3d& hitPnt, double hitPntU);
	void			AddEdge(IwEdge*& eg, IwPoint3d& hitPnt, double hitPntU);
	void			AddFace(IwFace*& fc, IwPoint3d& hitPnt, IwPoint2d& hitPntUV);
	double			DistanceBetween(CSelectedEntity& other, bool* isMinDist=NULL, bool localResult=true);
	IwVector3d		VectorBetween(CSelectedEntity& other, IwPoint3d& thisPoint, IwPoint3d& otherPoint);
	bool			IsIdentical(CSelectedEntity& other);
	IwPoint3d		GetPoint() {return point;};
	IwCurve*		GetCurve(bool curveOnly=false);
	IwEdge*			GetEdge();
	IwFace*			GetFace();
	IwSurface*		GetSurface();
	IwPoint3d		GetHitPoint() {return hitPoint;};
	IwPoint2d		GetHitPointUV() {return hitPointUV;};

private:
	void			HighLightCurve(IwCurve* curve);
	void			HighLightEdge(IwEdge* eg);
	void			HighLightFaceEdge(IwEdgeuse* edgeUse, double onePixelSize=0.000001);
	void			HighLightFace(IwFace* fc, double onePixelSize=0.000001);

private:

	IwPoint3d		point;
	IwCurve*		curve;
	IwEdge*			edge;
	IwFace*			face;

	IwPoint3d		hitPoint;		// the cursor hit point
	IwPoint2d		hitPointUV;		// the pick point u (for curve/edge) or uv (for face)

	IwPoint3d		closestPoint;
};

