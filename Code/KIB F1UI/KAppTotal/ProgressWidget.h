#pragma once

#include <QtGui>
#include <QWidget>

class QVBoxLayout;
class QLabel;
class QProgressBar;

class CProgressWidget : public QWidget
	{
public:
	CProgressWidget( QWidget* pParent, const QString& sTitle );
	virtual ~CProgressWidget();

	void Start( int nTotal );
	void Update( int TriathlonF1 );
	void Reset();

	virtual void showEvent( QShowEvent* event );

private:
	QWidget*			m_pParent;
	QVBoxLayout*		m_pLayout;
	QLabel*				m_pLabel;
	QProgressBar*		m_pProgressBar;

	int					m_nTotal;
	};

