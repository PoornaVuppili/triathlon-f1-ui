#include "SelectImplant.h"
#include "ui_SelectImplant.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"
#include "..\KUtility\KUtility.h"
#include "..\KUtility\KXmlHelper.h"
#include "..\KUtility\KPropertyFile.h"
#include "..\KUtility\KApplicationMode.h"
#include "..\KUtility\KImplantFolder.h"
#include "..\KUtility\KPropertyFile.h"


#include <QSettings>
#include <QTextStream>
#include <qfiledialog.h>
#include <qfile.h>
#include <qdatetime.h>
#include <qdom.h>
#include <qdebug.h>
#include <algorithm>
#include <qsizepolicy.h>
#include <qtimer.h>
#include <qmessagebox.h>

#define PROF_DESIGN_NODE		"ImplantDesign"
#define PROF_REVIEW_NODE		"ImplantReview"
#define DAYS_TO_DUE_DATE_ALERT	10

int sortByDate(pair<pair<unsigned __int64, unsigned __int64>, string> a,
               pair<pair<unsigned __int64, unsigned __int64>, string> b)
{
    if (b.first.first == a.first.first)
        return b.first.second < a.first.second;
    else
        return a.first.first < b.first.first;
}

SelectImplant::SelectImplant(QWidget *parent)
    : QDialog(parent)
{
    ui = new Ui::SelectImplantClass();
    ui->setupUi(this);
    setWindowFlags(windowFlags() & (~Qt::WindowContextHelpButtonHint));

	QString eksYoonee = mainWindow->GetImplant()->strProdFolder();
    QSettings settings;
    basePath = settings.value("ImplantRootFolder", eksYoonee).toString();

    modified = false;
    useReformat = false;
    isUserProcessingaCase = false;

    on_allOptStateChanged(Qt::Unchecked);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    
	connect(ui->lineEdit, SIGNAL(editingFinished()), this, SLOT(on_LineEdit_editingFinished()));
    connect(ui->comboImplantList, SIGNAL(currentIndexChanged(int)), this, SLOT(on_comboImplantList_indexChanged(int)));

    // Allow developers to select design files 
	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	bool devMode = (underDevelopment != NULL && QString("1") == underDevelopment); // under-development mode

    ui->comboBoxFemurImplant->setVisible(devMode);
    ui->label_7->setVisible(devMode);

    PopulateComboBox();

    if (mainWindow->GetImplant()->isProduction())
    {
    }

}

SelectImplant::~SelectImplant()
{
    kStatus.ShowMessage("");
    delete ui;

    ClearCaseList();
}

void SelectImplant::on_pushButtonBrowse_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Select the implant root folder", ui->lineEdit->text());
    if (path != "")
    {
        ui->lineEdit->setText(path);
        on_LineEdit_editingFinished();
    }
}

void SelectImplant::on_pushButtonNewSerial_clicked()
{
    // create the implant folder and get the implant information
    QList<QPair<QString, QString> > info;
    if (!KCreateImplantFolder(basePath, info, this))
        return;

    // add the software information
    info.append(QPair<QString, QString>("Software", QString(mainWindow->GetAppName()) + "_v" + mainWindow->GetAppVersion()));

    // save the property file
    QString name = info[0].second;
    KUpdateImplantInfoTxt(basePath + QDir::separator() + name + QDir::separator() + name + "_info.txt", info);

    PopulateComboBox();
    SetImplantName(name);

    // import the series
    on_pushButtonImport_clicked();
}

void SelectImplant::on_LineEdit_editingFinished()
{
    QString str = ui->lineEdit->text();
    if (str==basePath)
        return;

    if (!QDir(str).exists())
    {
        ui->lineEdit->setText(basePath);
        QMessageBox::information(this, "Error", "Folder doesn't exist:" + str);
        return;
    }
    // Test version: make sure the root isn't X:/UNI or its subfolders
	QString eksYoonee = mainWindow->GetImplant()->strProdFolder();
    if (QFileInfo(str).canonicalFilePath().startsWith(eksYoonee, Qt::CaseInsensitive))
    {
		char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
        bool isUnderDev = (underDevelopment != NULL && QString("1") == underDevelopment);// under-development mode

       if (isUnderDev)
       {
           ui->lineEdit->setText(basePath);
		   QString reason("This version");
		   		   
		   reason += QString(" cannot set implant root to %1 or its subfolders").arg(eksYoonee);

		   QMessageBox::information(this, "Error", reason);
           return;
       }
    }

    QSettings settings;
    settings.setValue("ImplantRootFolder", str);

    basePath = str;
    PopulateComboBox();
}

void SelectImplant::on_allOptStateChanged(int state)
{
    bool show = state == Qt::Checked;

    setUpdatesEnabled(false);

    //ui->grpBxSwapPixels->setVisible(show);
    //ui->pushButtonOK->setVisible(show);

    ui->gridLayout_2->update();
    QTimer::singleShot(10, this, SLOT(onAdjustSize()));
}

void SelectImplant::onAdjustSize()
{
    adjustSize();
    setUpdatesEnabled(true);
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Fill the combo box with the directory names in the UNIDIR folder
void SelectImplant::PopulateComboBox()
{
    QSettings settings;

    ui->comboImplantList->clear();

    if ( true )
    {
		bool enableSelectFile = false;
        char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
        if (underDevelopment != NULL && QString("1") == underDevelopment)
		{
			enableSelectFile = true;
		}

		UpdateBasePathAllThingsConsidered();

        {
			KWaitCursor wc;
            ui->comboImplantList->addItem("<Select an implant>");

			QStringList implants = KGetSubdirListOf(basePath);// uniDir.entryList(QDir::Dirs, QDir::Name);

            for (QStringList::Iterator it = implants.begin(); it != implants.end(); ++it)
            {	
                if (*it == "." || *it == "..")
                    continue;
                ui->comboImplantList->addItem(*it);
            }
        }
        ui->comboBoxFemurImplant->setVisible(enableSelectFile);
        ui->label_7->setVisible(enableSelectFile);
    }
    else
    {
        
    }
}

void SelectImplant::ClearCaseList()
{

}

void SelectImplant::UpdateBasePathAllThingsConsidered()
{
	QString eksYoonee = mainWindow->GetImplant()->strProdFolder();
    QSettings settings;
    basePath = settings.value("ImplantRootFolder", eksYoonee).toString();

 	bool enableAccessXDir = true;
	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (underDevelopment != NULL && QString("1") == underDevelopment)// under-development mode
	{
		enableAccessXDir = false;
	}

    if (!enableAccessXDir && QFileInfo(basePath).canonicalFilePath().startsWith(eksYoonee, Qt::CaseInsensitive))
	{ // Test version: make sure the root isn't X:/UNI or its subfolders
           basePath = QString("X:/TestData/") + mainWindow->GetAppName();
    }

    ui->lineEdit->setText(basePath);
}

// Set the current implant name
void SelectImplant::SetImplantName(QString& name)
{
    int i = ui->comboImplantList->findText(name);
    ui->comboImplantList->setCurrentIndex(i<0 ? 0 : i);
}

void SelectImplant::UpdateUI()
{
    ui->pushButtonOpenReformat->setEnabled(false);

    nonSquarePixSpacing.clear();
    flipPixSpacing.clear();
    eradScan.clear();
    sliceSpacing.clear();
    sliceThickness.clear();

    int vv = ui->comboImplantList->currentIndex();
    if (ui->comboImplantList->currentIndex()==0 && !isUserProcessingaCase)
        return;

    // parse the property file
    caseFolderName = ui->comboImplantList->currentText();

	implantName = caseFolderName;
	if ( caseFolderName.size() != 7 )
	{
	}

    QDir baseDir(basePath);
    imgDir = baseDir.absoluteFilePath(caseFolderName + "/CT");
    segDir = baseDir.absoluteFilePath(caseFolderName + "/Segmentation");
    surfDir = baseDir.absoluteFilePath(caseFolderName + "/Surfaces");
    stlDir = baseDir.absoluteFilePath(caseFolderName + "/STL");
    sldworksDir = baseDir.absoluteFilePath(caseFolderName + "/SolidWorks");

    if (!QDir(segDir).exists() || !QDir(surfDir).exists() || !QDir(sldworksDir).exists())
    {
        QMessageBox::information(this, "Error", "Invalid implant folder structure");
        return;
    }

    QString pfPath = basePath + QDir::separator() + caseFolderName + QDir::separator() 
        + implantName + "_info.txt";

    KPropertyFile pf(pfPath);
    if (!pf.loadFile())
    {
        QMessageBox::information(this, "Error", "Failed to load the property file:" + pfPath);
        return;
    }

    implantType = "";//pf.getValue("ImplantType");
    kneeType = pf.getValue("Knee");
    compartmentType = pf.getValue("Compartment");
    country = pf.getValue("Country");
	
    // display the implant info
    QString strInfo = implantType;
    if (!kneeType.isEmpty())
        strInfo += kneeType;
    if (!compartmentType.isEmpty())
        strInfo += ", " + compartmentType;
    if (country.isEmpty())
    {
        // for older implant without this information
        zone = "";
    }
    else
    {
        zone = pf.getValue("Zone");
        strInfo += ", " + country + " (" + zone + ")";
    }

    if (strInfo.length() > 0)
    {
        
    }

    KWaitCursor wait;

    // get the list of femur implants under the SolidWorks/Femur folder
    ui->comboBoxFemurImplant->clear();

    QDir femurImplantDir(sldworksDir /*+ QDir::separator() + "Femur"*/);
    if (femurImplantDir.exists())
    {
        QFileInfoList infoList = femurImplantDir.entryInfoList(QStringList() << "*.tw", QDir::Dirs, QDir::Name);
        for (int i=0; i<infoList.size(); i++)
            ui->comboBoxFemurImplant->addItem(infoList[i].fileName().toUtf8());
        // add an empty one to allow selecting none
        if (infoList.size()>0)
            ui->comboBoxFemurImplant->addItem("");
    }
    
    // enable/disable the buttons
    UpdateButton();

    modified = false;
}

QString SelectImplant::GetSpacingThicknessString(const QString& name)
{
    QString s = name + "=";
    auto jt = sliceThickness.find(name);
    if (jt != sliceThickness.end() && jt.key() == name && jt.value() > 0)
        s += QString::number(jt.value());
    s += "|";
    auto it = sliceSpacing.find(name);
    if (it != sliceSpacing.end() && it.key() == name && it.value() > 0)
        s += QString::number(it.value());
    return s+"  ";
}

void SelectImplant::UpdateButton()
{
    ui->pushButtonOpenReformat->setEnabled(true);

    // show the knee series space & thickness on the status bar
    QString str;
    
    kStatus.ShowMessage(str);

    // Allow developers to select design files 
    bool enableSelectFile = false;
    char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
    if (underDevelopment != NULL && QString("1") == underDevelopment)// under-development mode to optionally select with different names
        enableSelectFile = true;

    if (enableSelectFile)
    {
        ui->pushButtonOpenReformat->setEnabled(enableSelectFile);

        ui->comboBoxFemurImplant->setVisible(enableSelectFile);
        ui->label_7->setVisible(enableSelectFile);
    }
}

void SelectImplant::on_comboImplantList_indexChanged(int index)
{
    if (index==-1)
        return;

    //ui->pushButtonImport->setEnabled(index != 0);

    if (modified)
    {
        if (QMessageBox::question(this, "Save", "Content is changed. Do you want to save the changes?",
            QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
            UpdatePropertyFile();
    }

	UpdateUI();
}

// Update the property file
bool SelectImplant::UpdatePropertyFile()
{
    //int ci = ui->comboBoxCor->currentIndex();
    //int si = ui->comboBoxSag->currentIndex();
    //int ai = ui->comboBoxAxi->currentIndex();
    //int hi = ui->comboBoxHip->currentIndex();
    //int ki = ui->comboBoxAnkle->currentIndex();

    //if ((si!=0 && si==ci) ||
    //    (ai!=0 && (ai==ci || ai==si)) ||
    //    (hi!=0 && (hi==ci || hi==si || hi==ai)) ||
    //    (ki!=0 && (ki==ci || ki==si || ki==ai || ki==hi)))
    //{
    //    ui->pushButtonOK->setEnabled(false);
    //    QMessageBox::information(this, "Error", "Select each scan only once.");
    //    return false;
    //}

    //QString pfPath = basePath + QDir::separator() + caseFolderName + QDir::separator() 
    //    + implantName + "_info.txt";

    //KPropertyFile pf(pfPath);
    //if (!pf.loadFile())
    //{
    //    QMessageBox::information(this, "Error", "Failed to load the property file:" + pfPath);
    //    return false;
    //}
    //
    //if (ci > 0)
    //{
    //    kneeCor = ui->comboBoxCor->currentText();
    //    pf.insertValue("CorDir", kneeCor);

    //    if (ui->checkBoxSwapCor->isEnabled() && ui->checkBoxSwapCor->isChecked())
    //        pf.insertValue(kneeCor, "SwapSpacing");
    //    else
    //        pf.removeValue(kneeCor);
    //}
    //else
    //{
    //    kneeCor = "";
    //    pf.removeValue("CorDir");
    //}

    //if (si > 0)
    //{
    //    kneeSag = ui->comboBoxSag->currentText();
    //    pf.insertValue("SagDir", kneeSag);

    //    if (ui->checkBoxSwapSag->isEnabled() && ui->checkBoxSwapSag->isChecked())
    //        pf.insertValue(kneeSag, "SwapSpacing");
    //    else
    //        pf.removeValue(kneeSag);
    //}
    //else
    //{
    //    kneeSag = "";
    //    pf.removeValue("SagDir");
    //}

    //if (ai > 0)
    //{
    //    kneeAxi = ui->comboBoxAxi->currentText();
    //    pf.insertValue("AxiDir", kneeAxi);

    //    if (ui->checkBoxSwapAxi->isEnabled() && ui->checkBoxSwapAxi->isChecked())
    //        pf.insertValue(kneeAxi, "SwapSpacing");
    //    else
    //        pf.removeValue(kneeAxi);
    //}
    //else
    //{
    //    kneeAxi = "";
    //    pf.removeValue("AxiDir");
    //}

    //if (hi > 0)
    //{
    //    hipAxi = ui->comboBoxHip->currentText();
    //    pf.insertValue("HipDir", hipAxi);

    //    if (ui->checkBoxSwapHip->isEnabled() && ui->checkBoxSwapHip->isChecked())
    //        pf.insertValue(hipAxi, "SwapSpacing");
    //    else
    //        pf.removeValue(hipAxi);
    //}
    //else
    //{
    //    hipAxi = "";
    //    pf.removeValue("HipDir");
    //}

    //if (ki > 0)
    //{
    //    ankleAxi = ui->comboBoxAnkle->currentText();
    //    pf.insertValue("AnkleDir", ankleAxi);

    //    if (ui->checkBoxSwapAnkle->isEnabled() && ui->checkBoxSwapAnkle->isChecked())
    //        pf.insertValue(ankleAxi, "SwapSpacing");
    //    else
    //        pf.removeValue(ankleAxi);
    //}
    //else
    //{
    //    ankleAxi = "";
    //    pf.removeValue("AnkleDir");
    //}

    //pf.insertValue("Software", QString(mainWindow->GetAppName()) + "_v" + mainWindow->GetAppVersion());
    //pf.saveFile();
    return true;
}

void SelectImplant::on_pushButtonImport_clicked()
{
    if (modified)
    {
        if (QMessageBox::question(this, "Save", "Content is changed. Do you want to save the changes?",
            QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
        {
            UpdatePropertyFile();
            modified = false;
        }
    }

    // import the series
    QString implantFolder = basePath + QDir::separator() + caseFolderName + QDir::separator();

    UpdateUI();
}

void SelectImplant::on_pushButtonReformat_clicked()
{
    UpdateButton();
}

bool SelectImplant::WarnSpacingThickness()
{
    //QStringList names;
    //int count = 0;

    //QStringList warnSpaceThickList;

    //if (ui->comboBoxCor->currentIndex()>0 && !useReformat)
    //{
    //    QString name = ui->comboBoxCor->currentText();
    //    if (eradScan[name] && nonSquarePixSpacing[name])
    //    {
    //        names.append(name + "(knee)");
    //        ++count; // count how many knee scans has non-square pixel spacing
    //    }

    //    // Check the knee spacing or thickness > 1.5mm
    //    auto it = sliceSpacing.find(name);
    //    auto jt = sliceThickness.find(name);
    //    if (it != sliceSpacing.end()   && it.key() == name && it.value() > 1.5 ||
    //        jt != sliceThickness.end() && jt.key() == name && jt.value() > 1.5)
    //        warnSpaceThickList.append(name);
    //}
    //if (ui->comboBoxSag->currentIndex()>0 && !useReformat)
    //{
    //    QString name = ui->comboBoxSag->currentText();
    //    if (eradScan[name] && nonSquarePixSpacing[name])
    //    {
    //        names.append(name + "(knee)");
    //        ++count; // count how many knee scans has non-square pixel spacing
    //    }

    //    // Check the knee spacing or thickness > 1.5mm
    //    auto it = sliceSpacing.find(name);
    //    auto jt = sliceThickness.find(name);
    //    if (it != sliceSpacing.end()   && it.key() == name && it.value() > 1.5 ||
    //        jt != sliceThickness.end() && jt.key() == name && jt.value() > 1.5)
    //        warnSpaceThickList.append(name);
    //}
    //if (ui->comboBoxAxi->currentIndex()>0)
    //{
    //    QString name = ui->comboBoxAxi->currentText();
    //    if (eradScan[name] && nonSquarePixSpacing[name])
    //    {
    //        names.append(name + "(knee)");
    //        ++count; // count how many knee scans has non-square pixel spacing
    //    }

    //    // Check the knee spacing or thickness > 1.5mm
    //    auto it = sliceSpacing.find(name);
    //    auto jt = sliceThickness.find(name);
    //    if (it != sliceSpacing.end()   && it.key() == name && it.value() > 1.5 ||
    //        jt != sliceThickness.end() && jt.key() == name && jt.value() > 1.5)
    //        warnSpaceThickList.append(name);
    //}
    //if (ui->comboBoxHip->currentIndex()>0)
    //{
    //    QString name = ui->comboBoxHip->currentText();
    //    if (eradScan[name] && nonSquarePixSpacing[name])
    //        names.append(name + "(hip)");
    //}
    //if (ui->comboBoxAnkle->currentIndex()>0)
    //{
    //    QString name = ui->comboBoxAnkle->currentText();
    //    if (eradScan[name] && nonSquarePixSpacing[name])
    //        names.append(name + "(ankle)");
    //}
    //// warn eRAD scans with non-square pixel spacing
    //if (names.count()>0)
    //{
    //    QString str = names.join(", ") + " are eRAD scans with non-square pixel spacing. May need swapping to process correctly.";
    //    if (count>1)
    //        str += " And check the alignment of knee scans.";
    //    QMessageBox::warning(this, "Warning", str);
    //}

    //// warn knee spacing or thickness > 1.5mm
    //if (warnSpaceThickList.count()>0)
    //{
    //    QMessageBox::critical(mainWindow, "Error", QString("Knee series ") + warnSpaceThickList.join(", ") + " slice thickness or spacing is greater than 1.5mm.\n");
    //    return false;
    //}
    return true;
}

// Get the femural implant file
QString SelectImplant::GetFemuralImplantFile()
{
    QString cur = ui->comboBoxFemurImplant->currentText();
		
    return cur.isEmpty() ? "" : sldworksDir + QDir::separator() + cur;
}

void SelectImplant::on_pushButtonOK_clicked()
{
    useReformat = false;

    // generate warning message for 
    //  - eRad scans with non-square pixels
    //  - knee slice thickness or spacing > 1.5mm
    if (!WarnSpacingThickness())
        return;


    if (modified)
    {
        if (!UpdatePropertyFile())
            return;
    }

    accept();
}

void SelectImplant::on_pushButtonCancel_clicked()
{

    reject();
}

void SelectImplant::on_pushButtonOpenReformat_clicked()
{
    useReformat = true;

    // generate warning message for 
    //  - eRad scans with non-square pixels
    //  - knee slice thickness or spacing > 1.5mm
    if (!WarnSpacingThickness())
        return;

    if (modified)
    {
        if (!UpdatePropertyFile())
            return;
    }

	accept();
}
