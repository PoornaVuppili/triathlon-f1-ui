#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "InnerSurfaceJigs.h"
#include "MakeInnerSurfaceJigs.h"
#include "OutlineProfileJigs.h"
#include "FemoralPart.h"
#include "CartilageSurface.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "AdvancedControlJigs.h"
#include "UndoRedoCmdJigs.h"

CMakeInnerSurfaceJigsErrorCodesEntry CMakeInnerSurfaceJigs::MakeInnerSurfaceJigsErrorCodes[] = 
{
	{ MAKE_INNER_SURFACE_JIGS_ERROR_CODES_NONE,	"None."	},
	{ MAKE_INNER_SURFACE_JIGS_ERROR_CODES_FLIP_NORMAL,		"Inner surface jigs is overalpping.\nCheck anterior outline profile is on a smooth area and \ndropping centers are in proper locations."	}
};

CMakeInnerSurfaceJigs::CMakeInnerSurfaceJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
	bool activateUI = (manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMakeInnerSurfaceJigs";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;
	m_actDispSurface = NULL;
	m_actDispAntDrop = NULL;

	m_bDispSurface = true;
	m_bDispAntDrop = false;
	m_predefinedViewIndex = 0;

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Inner Surface Jigs");
		m_actMakeInnerSurfaceJigs = m_toolbar->addAction("InnerSurf");
		SetActionAsTitle( m_actMakeInnerSurfaceJigs );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );
		if ( m_pDoc->GetAdvancedControlJigs()->GetInnerSurfaceDisplayAnteriorDropSurface() )
		{
			m_actDispSurface = m_toolbar->addAction("DispSurf");
			EnableAction( m_actDispSurface, true );
			m_pMainWindow->connect( m_actDispSurface, SIGNAL( triggered() ), this, SLOT( OnDispSurface() ) );
			m_actDispAntDrop = m_toolbar->addAction("AntDrop");
			EnableAction( m_actDispAntDrop, true );
			m_pMainWindow->connect( m_actDispAntDrop, SIGNAL( triggered() ), this, SLOT( OnDispAntDrop() ) );
		}
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );

	}
	else if (manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Inner Surface Jigs");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );
		//m_actReviewValidateImplant = m_toolbar->addAction("E Report");
		//EnableAction( m_actReviewValidateImplant, true );

		m_actMakeInnerSurfaceJigs = m_toolbar->addAction("InnerSurf");
		SetActionAsTitle( m_actMakeInnerSurfaceJigs );
		m_pMainWindow->connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		m_pMainWindow->connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		m_pMainWindow->connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	CInnerSurfaceJigs* innerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if ( innerSurfaceJigs == NULL  ) // if no object
	{
		if ( innerSurfaceJigs == NULL )
		{
			innerSurfaceJigs = new CInnerSurfaceJigs( m_pDoc, ENT_ROLE_INNER_SURFACE_JIGS );
			m_pDoc->AddEntity( innerSurfaceJigs, true );
		}

		// Reset is the way to make inner surface jigs.
		Reset( activateUI );
	}
	else
	{
		// Call reset to update inner surface jigs
		if ( !innerSurfaceJigs->GetUpToDateStatus() )
			Reset( activateUI );
	}

	// If any errors, display error messages.
	DisplayErrorMessages();

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

}

CMakeInnerSurfaceJigs::~CMakeInnerSurfaceJigs()
{
	CInnerSurfaceJigs* innerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if ( innerSurfaceJigs != NULL )
	{
		int previoisDesignTime = innerSurfaceJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		innerSurfaceJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}

}

void CMakeInnerSurfaceJigs::CreateInnerSurfaceJigsObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_INNER_SURFACE_JIGS) == NULL )
	{
		CInnerSurfaceJigs* pInnerSurfaceJigs = new CInnerSurfaceJigs(pDoc, ENT_ROLE_INNER_SURFACE_JIGS);
		pDoc->AddEntity(pInnerSurfaceJigs, true);
	}

	return;
}

void CMakeInnerSurfaceJigs::OnReset()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	Reset();

	// If any errors, display error messages.
	DisplayErrorMessages();

}

//////////////////////////////////////////////////////////////////////
// Reset is the way to make inner surface, which includes the
// rectanglar bearing surface and 2 dropping surfaces on sides.
//////////////////////////////////////////////////////////////////////
void CMakeInnerSurfaceJigs::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CMakeInnerSurfaceJigs:Reset()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Get the anterior dropping surface, and add to totalSurfaces
	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if ( pInnerSurfaceJigs == NULL )
		return;

	// Get osteophyte 3 surfaces
	IwTArray<IwSurface*> totalSurfaces;

	CFemoralPart* pOsteoSurf = m_pDoc->GetOsteophyteSurface();
	if ( pOsteoSurf == NULL )
		return;

	//////////////////////////////////////////////////////////////
	IwBrep* osteoOffsetBrep = NULL;
	// create the osteoOffsetBrep
	if ( m_pDoc->GetAdvancedControlJigs()->GetOsteophyteTimeStampForInner() != pInnerSurfaceJigs->GetOsteophyteTimeStamp() )
	{
		double osteoAdditionalOffset = m_pDoc->GetVarTableValue("JIGS OSTEOPHYTE ADDITIONAL OFFSET DISTANCE");
		IwBSplineSurface* osteoMainSurf = pOsteoSurf->GetSinglePatchSurface();
		IwBSplineSurface* osteoLeftSurf, * osteoRightSurf;
		pOsteoSurf->GetSideSurfaces(osteoLeftSurf, osteoRightSurf);
		IwBSplineSurface* osteoMainOffsetSurf = NULL;
		osteoMainSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), osteoAdditionalOffset, 0.05, osteoMainOffsetSurf);
		totalSurfaces.Add(osteoMainOffsetSurf);
		IwBSplineSurface* osteoLeftOffsetSurf = NULL;
		osteoLeftSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), osteoAdditionalOffset, 0.05, osteoLeftOffsetSurf);
		totalSurfaces.Add(osteoLeftOffsetSurf);
		IwBSplineSurface* osteoRightOffsetSurf = NULL;
		osteoRightSurf->ApproximateOffsetSurface(m_pDoc->GetIwContext(), osteoAdditionalOffset, 0.05, osteoRightOffsetSurf);
		totalSurfaces.Add(osteoRightOffsetSurf);
		// Create an IwBrep
		osteoOffsetBrep = new (m_pDoc->GetIwContext()) IwBrep(); 
		IwFace* face;
		osteoOffsetBrep->CreateFaceFromSurface(osteoMainOffsetSurf, osteoMainOffsetSurf->GetNaturalUVDomain(), face);
		osteoOffsetBrep->CreateFaceFromSurface(osteoLeftOffsetSurf, osteoLeftOffsetSurf->GetNaturalUVDomain(), face);
		osteoOffsetBrep->CreateFaceFromSurface(osteoRightOffsetSurf, osteoRightOffsetSurf->GetNaturalUVDomain(), face);
		// Set it to pOuterSurfaceJigs
		pInnerSurfaceJigs->SetOffsetOsteophyteBrep(osteoOffsetBrep);
		// Set time stamp
		pInnerSurfaceJigs->SetOsteophyteTimeStamp(m_pDoc->GetAdvancedControlJigs()->GetOsteophyteTimeStampForInner());
	}
	else // Retrieve from pOuterSurfaceJigs
	{
		IwTArray<IwFace*> faces;
		bool gotIt = pInnerSurfaceJigs->GetOffsetOsteophyteBrep( osteoOffsetBrep );
		osteoOffsetBrep->GetFaces(faces);
		totalSurfaces.Add(faces.GetAt(0)->GetSurface());
		totalSurfaces.Add(faces.GetAt(1)->GetSurface());
		totalSurfaces.Add(faces.GetAt(2)->GetSurface());
	}
if (0)
	ShowBrep(m_pDoc, osteoOffsetBrep, "osteoOffsetBrep", red);

	// Get cartilage 3 surfaces
	CCartilageSurface* pCartiSurf = m_pDoc->GetCartilageSurface();
	if ( pCartiSurf == NULL )
		return;
	bool bSmooth = !m_pDoc->GetAdvancedControlJigs()->GetCartilageUseWaterTightSurface();
	IwBSplineSurface* cartiMainSurf = pCartiSurf->GetSinglePatchSurface(bSmooth);
	IwBSplineSurface* cartiLeftSurf,* cartiRightSurf;
	pCartiSurf->GetSideSurfaces(cartiLeftSurf, cartiRightSurf);
	totalSurfaces.Add(cartiMainSurf);
	totalSurfaces.Add(cartiLeftSurf);
	totalSurfaces.Add(cartiRightSurf);

	// Determine anterior dropping surface
	IwBrep* anteriorDroppingBrep=NULL;
	DetermineAnteriorDroppingSurface(m_pDoc, totalSurfaces, false, anteriorDroppingBrep);// 6 surfaces
	// Set it to pInnerSurfaceJigs
	pInnerSurfaceJigs->SetAnteriorDropping(anteriorDroppingBrep);
	IwTArray<IwFace*> faces;
	anteriorDroppingBrep->GetFaces(faces);
	IwSurface* antDropSurf = faces.GetAt(0)->GetSurface();
	totalSurfaces.Add(antDropSurf);// 7 surfaces

if (0)//PSV---
{
	ShowSurface(m_pDoc, totalSurfaces[0], "osteoMainOffsetSurf", red);//osteoLeftOffsetSurf
	ShowSurface(m_pDoc, totalSurfaces[1], "osteoLeftOffsetSurf", red);
	ShowSurface(m_pDoc, totalSurfaces[2], "osteoRightOffsetSurf", red);
	ShowSurface(m_pDoc, totalSurfaces[3], "cartiMainSurf", red);
	ShowSurface(m_pDoc, totalSurfaces[4], "cartiLeftSurf", red);
	ShowSurface(m_pDoc, totalSurfaces[5], "cartiRightSurf", red);
	ShowSurface(m_pDoc, totalSurfaces[6], "antDropSurf", red);

}

	// If tabEndPoints are not initialized (have not determined yet),
	// we need to determine tab end points here. Why? The tab points and tab end points 
	// are on the same iso-surves of the inner surface (along the dropping curves).
	// The dropping curve directions can not be finalized 
	// until DefineOutlineProfileJigs manager is exited. 
	// Therefore, it is more reasonable to initialize the tab end points here.
	// Additionally, the creation of the inner surface jigs surface needs the tab end profiles
	// as references to determine 6 surfaces dropping (contact to outer most surface) 
	// or 3 surfaces dropping (contact to osteophyte surface around tab ends).
	IwTArray<IwBSplineCurve*> jumpCurves;
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs )
	{
		IwTArray<IwPoint3d> tabEndPoints;
		pOutlineProfileJigs->GetOutlineProfileTabEndPoints(tabEndPoints);
		if ( !tabEndPoints.GetAt(0).IsInitialized() ) // have not determined yet.
		{
			tabEndPoints = CDefineOutlineProfileJigs::DetermineTabEndLocations(m_pDoc);
			pOutlineProfileJigs->SetOutlineProfileTabEndPoints(tabEndPoints);
		}
		// Get jump curves
		pOutlineProfileJigs->GetJumpCurves(jumpCurves);
		if (jumpCurves.GetSize() == 0 )
		{
			jumpCurves.Add(NULL);
			jumpCurves.Add(NULL);
		}
		// We only determine tab end points, but not determine tab profiles here.
		// The tab profiles should be done when the inner surface jigs is created.
	}

	//// Create inner contact surface
	IwBrep* contactSurfBrep=NULL;
	double dropOffset = m_pDoc->GetAdvancedControlJigs()->GetInnerSurfaceDropOffsetDistance();
	DetermineContactSurface(m_pDoc, totalSurfaces, dropOffset, false, true, NULL, NULL, &jumpCurves, contactSurfBrep); // 7 surfaces as input

	if(0) ShowBrep(m_pDoc, contactSurfBrep, "contactSurfBrep", blue);//PSV---
	
	// Validate inner surface
	bool flipNormal;
	IwTArray<IwPoint3d> flipPoints;
	ValidateContactSurface(m_pDoc, contactSurfBrep, flipNormal, flipPoints); 
	if (flipNormal)
	{
		AddErrorCodes(MAKE_INNER_SURFACE_JIGS_ERROR_CODES_FLIP_NORMAL);
		for (unsigned i=0; i<flipPoints.GetSize(); i++)
			ShowPoint(m_pDoc, flipPoints.GetAt(i), red);
	}
	// Set it to pInnerSurfaceJigs
	pInnerSurfaceJigs->SetIwBrep(contactSurfBrep);
	pInnerSurfaceJigs->MakeTess();
	pInnerSurfaceJigs->SetModifiedFlag(true);
	// update the sign
	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_INNER_SURFACE_JIGS, false, false);

	// Now update tab profiles, and square off posterior tips if needed.
	if ( pOutlineProfileJigs )
	{
		pOutlineProfileJigs->DetermineOutlineProfileJigsTabProfilePoints();//update tab profiles
		pOutlineProfileJigs->DetermineOutlineProfileJigsProjectedCurvePoints();//It will square off posterior tips if needed
	}

	QApplication::restoreOverrideCursor();

	m_pView->Redraw();

}

void CMakeInnerSurfaceJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CMakeInnerSurfaceJigs::OnAccept()") );

	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();

	if ( pInnerSurfaceJigs && !pInnerSurfaceJigs->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_INNER_SURFACE_JIGS, true, true);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}


void CMakeInnerSurfaceJigs::OnDispSurface()
{
	m_pDoc->AppendLog( QString("CMakeInnerSurfaceJigs::OnDispSurface()") );

	m_bDispSurface = !m_bDispSurface;

	m_pDoc->GetInnerSurfaceJigs()->SetDispSurface(m_bDispSurface);

	m_pView->Redraw();

}

void CMakeInnerSurfaceJigs::OnDispAntDrop()
{
	m_pDoc->AppendLog( QString("CMakeInnerSurfaceJigs::OnDispAntDrop()") );

	m_bDispAntDrop = !m_bDispAntDrop;

	m_pDoc->GetInnerSurfaceJigs()->SetDispAntDrop(m_bDispAntDrop);

	m_pView->Redraw();

}

void CMakeInnerSurfaceJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CMakeInnerSurfaceJigs::OnCancel()") );

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

bool CMakeInnerSurfaceJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_posStart = cursor;

	ReDraw();

	return true;
}

bool CMakeInnerSurfaceJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // 
	{
		// if the clicked-down is the same as the button-up point, change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}

	ReDraw();

	return true;
}

bool CMakeInnerSurfaceJigs::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{

	ReDraw();

	return true;
}

//////////////////////////////////////////////////////////////////////////
// This function determines the dropping curves (represented by points) 
// defined by the startPnt, downVec, and outVec.
// The startPnt, downVec, and outVec define a plane. The plane intersects
// both osteophyte and cartilage surfaces. Then start to search along 
// the intersection profiles from the startPnt whoever is outer. 
// The next point can move up or down, but is limited to move out only. 
//////////////////////////////////////////////////////////////////////////
void CMakeInnerSurfaceJigs::DroppingCurve
(
	CTotalDoc* pDoc,				// I:
	IwTArray<IwSurface*>& totalSurfaces, // I: the [0~2] are osteophyte surface, the [3~5] are cartilage surfaces. If [6] exists, it is anterior dropping surface.
	IwPoint3d& startPnt,			// I: 
	IwVector3d& downVec,			// I: Note the magnitude of downVec defines how deep the dropping curve is.
	IwVector3d& outVec,				// I: Note the magnitude of outVec defines how far the dropping curve to go out.
	double offsetDist,				// I: offset distance
	IwTArray<IwPoint3d>& dropPnts,	// O: Note the number of points is controlled.
	IwBSplineCurve** dropCurve		// O:
)
{
	// Dropping Curve only takes 6 surfaces. (No anterior dropping surface);
	IwTArray<IwSurface*> totalSurfaces6;
	totalSurfaces6.Append(totalSurfaces);
	if ( totalSurfaces.GetSize() == 7 )
		totalSurfaces6.RemoveLast();

	// initialize data
	dropPnts.RemoveAll();

	// Intersect both sets of surfaces along outVec
	// Create a plane define by startPnt, downVec, and outVec
	double magDown = downVec.Length();
	double magOut = outVec.Length();

	IwVector3d downVecUnit = downVec;
	downVecUnit.Unitize();
	IwVector3d outVecUnit = outVec;
	outVecUnit.Unitize();
	IwVector3d myDownVecUnit=downVecUnit;

	int missCount = 0;
	double stepForward = 0.5;
	double dist, soFarDist = 0;
	int surfIndex;
	IwPoint2d uv;
	IwPoint3d pnt, intPnt;
	IwVector3d normal;
	double normalStepSize = 0.2;
	int nStep = (int)(magOut/normalStepSize)+1;
	double deltaStepSize = magOut/nStep;
	IwTArray<IwPoint3d> intPoints;
	IwVector3d tempVec;
	IwPoint3d bottomPnt = startPnt+downVec;
	double ratio;
	double myOffsetDist = offsetDist;
	IwTArray<int> dotValues;
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	// first point is the startPnt
	intPoints.Add(startPnt);
	for (soFarDist=deltaStepSize; soFarDist<=magOut; soFarDist+=deltaStepSize)
	{
		if ( missCount > 4 )// if no intersection more than 4 times, skip all.
			break;
		myDownVecUnit = downVecUnit;
		//
		pnt = startPnt + soFarDist*outVecUnit - 30*myDownVecUnit;
		bool bInt = IntersectSurfacesByFireRay(totalSurfaces6, pnt, myDownVecUnit, HUGE_DOUBLE, intPnt, &surfIndex, &uv, &dotValues);
if (0)
{
	if ( bInt )
		ShowPoint(pDoc, intPnt, red);
}
		if ( bInt )
		{
			tempVec = intPnt - bottomPnt;
			if ( tempVec.Dot(myDownVecUnit) > 0 )// lower than bottomPnt (out of range)
			{
				missCount++;
				continue;
			}
			tempVec =  intPoints.GetLast() - intPnt;
			// Offset intersection point with surface normal
			totalSurfaces6.GetAt(surfIndex)->EvaluateNormal(uv, FALSE, FALSE, normal);
			if ( soFarDist < 1.5 )
			{
				myOffsetDist = 0.0;
			}
			else if ( soFarDist < 4.5 )// gradually increase the offset distance
			{
				ratio = (soFarDist-1.5)/3.0;
				myOffsetDist = ratio*offsetDist;
			}
			else
			{
				myOffsetDist = offsetDist;
			}
			intPnt = intPnt + myOffsetDist*normal;
			// However, we need to control the density. Not too far away.
			dist = intPoints.GetLast().DistanceBetween(intPnt);
			if ( dist > 3*normalStepSize )
			{
				IwPoint3d intmediaPnt, sLinePnt;
				int nSize = (int)(dist/normalStepSize);
				sLinePnt = intPoints.GetLast();
				IwVector3d deltaVec = (intPnt - sLinePnt)/((double)nSize);
				for (int j=0; j<nSize; j++)
				{
					intmediaPnt = sLinePnt + (j+1)*deltaVec;
					intPoints.Add(intmediaPnt);
				}
			}
			else
			{
				intPoints.Add(intPnt);
			}
		}
		else
		{
			missCount++;
		}
	}

	// Do we drop to the bottom of the plane
	bottomPnt = intPoints.GetLast().ProjectPointToPlane(startPnt+downVec, myDownVecUnit);
	dist = intPoints.GetLast().DistanceBetween(bottomPnt);
	if ( dist > 3*normalStepSize )
	{
		IwPoint3d intmediaPnt, sLinePnt;
		int nSize = (int)(dist/normalStepSize);
		sLinePnt = intPoints.GetLast();
		double delta = dist/((double)nSize);
		for (int j=0; j<nSize; j++)
		{
			intmediaPnt = sLinePnt + (j+1)*delta*myDownVecUnit;
			intPoints.Add(intmediaPnt);
		}
	}
if(0)
for(unsigned i=0;i<intPoints.GetSize();i++)
ShowPoint(pDoc, intPoints.GetAt(i), red);
	// Create an interpolateion curve
	IwBSplineCurve* myDropCurve = NULL;
	IwBSplineCurve::InterpolatePoints(pDoc->GetIwContext(), intPoints, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, myDropCurve);
	// Sampling into 30 points. Therefore, the output dropPnts always consist fixed number of points 
	if ( myDropCurve )
	{
		IwExtent1d dom = myDropCurve->GetNaturalInterval();
		myDropCurve->EquallySpacedPoints(dom.GetMin(), dom.GetMax(), 29, 0.01, &dropPnts, NULL);
	}

	if ( dropCurve != NULL )
	{
		*dropCurve = myDropCurve;
	}
	else
	{
		IwObjDelete delCrv(myDropCurve);
	}

}

//////////////////////////////////////////////////////////////////////////
// This functions is similar to CMakeInnerSurfaceJigs::DroppingCurve().
// However, the return dropPnts & dropCurve will jump from the outer-most
// totalSurfaces to [0~2] osteophyte surfaces near jumpCurve.
//////////////////////////////////////////////////////////////////////////
void CMakeInnerSurfaceJigs::DroppingCurveWithJump
(
	CTotalDoc* pDoc,				// I:
	IwTArray<IwSurface*>& totalSurfaces, // I: the [0~2] are osteophyte surface, the [3~5] are cartilage surfaces. If [6] exists, it is anterior dropping surface.
	IwPoint3d& startPnt,			// I: 
	IwVector3d& downVec,			// I: Note the magnitude of downVec defines how deep the dropping curve is.
	IwVector3d& outVec,				// I: Note the magnitude of outVec defines how far the dropping curve to go out.
	double offsetDist,				// I: offset distance
	IwBSplineCurve* jumpCurve,		// I: where to jump from all surfaces to osteophyte surfaces
	bool bAccurate,					// I: use interpolation or approximation
	double extendAlongOutVec,		// I: the distance to extend along the out vec at the end of dropping
	IwTArray<IwPoint3d>& dropPnts,	// O: Note the number of points is controlled.
	IwBSplineCurve** dropCurve,		// O:
	IwBSplineSurface* referenceSurface, // I: additional reference to ensure this contact surface is at least referenceDistance to the given referenceSurface
	double referenceDistance			// I: additional reference to ensure this contact surface is at least referenceDistance to the given referenceSurface
)
{
	// Dropping Curve only takes 6 surfaces. (No anterior dropping surface);
	IwTArray<IwSurface*> totalSurfaces6, osteoSurfaces;
	totalSurfaces6.Append(totalSurfaces);
	if ( totalSurfaces.GetSize() == 7 )
		totalSurfaces6.RemoveLast();
	osteoSurfaces.Add(totalSurfaces.GetAt(0));
	osteoSurfaces.Add(totalSurfaces.GetAt(1));
	osteoSurfaces.Add(totalSurfaces.GetAt(2));

	// initialize data
	dropPnts.RemoveAll();

	//////////////////////////////////////////////////////////////////////////////
	// How it works?
	// Define a virtual plane by startPnt, downVec, and outVec.
	// Move along the outVec. At each point (along the outVec), scan the surfaces
	// along the downVec. The outer-most points are the dropping curve.
	// However, a jump point is defined. Before reached the jump point, take 
	// the outer-most point of all 6 surfaces. After passed jump point, take
	// the outer-most point of osteo [0~2] surfaces.

	///////////////////////////////////////////////
	// Determine the jump point.
	IwPoint3d jumpPoint = DetermineJumpPoint(pDoc, totalSurfaces, startPnt, downVec, outVec, offsetDist, jumpCurve);
static int showJumpPoint = 0;
if(showJumpPoint)
ShowPoint(pDoc, jumpPoint, cyan);

	///////////////////////////////////////////////
	double magDown = downVec.Length();
	double magOut = outVec.Length();

	IwVector3d downVecUnit = downVec;
	downVecUnit.Unitize();
	IwVector3d outVecUnit = outVec;
	outVecUnit.Unitize();
	IwVector3d myDownVecUnit=downVecUnit;

	int missCount = 0, missCountOsteo=0, missCountCarti=0;
	double stepForward = 0.5;
	double dist, soFarDist = 0;
	int surfIndex;
	IwPoint2d uv;
	IwPoint3d pnt, intPnt;
	IwVector3d normal;
	double normalStepSize = 0.2;
	int nStep = (int)(magOut/normalStepSize)+1;
	double deltaStepSize = magOut/nStep;
	IwTArray<IwPoint3d> intPoints, intNoSpikePoints, intEvenPoints;
	IwVector3d tempVec;
	IwPoint3d bottomPnt = startPnt+downVec;
	double ratio;
	double myOffsetDist = offsetDist;
	bool use6Surfaces, isSpike;
	bool hitOsteo, hitCarti;
	IwTArray<int> dotValues;
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(-1);
	IwTArray<bool> hitEachSurf;
	// first point is the startPnt
	intPoints.Add(startPnt);
	for (soFarDist=deltaStepSize; soFarDist<=magOut; soFarDist+=deltaStepSize)
	{
		if ( missCount > 4 )// if no intersection more than 4 times, skip all.
			break;
		myDownVecUnit = downVecUnit;
		//
		pnt = startPnt + soFarDist*outVecUnit - 1.5*magDown*myDownVecUnit;
		// if the pnt is inside of the jumpPoint, use totalSurfaces6
		tempVec = (startPnt + soFarDist*outVecUnit) - jumpPoint;
		bool bInt;
		if ( tempVec.Dot(outVecUnit) > 0 )// outer side of jumpPoint, use osteoSurfaces
		{
			bInt = IntersectSurfacesByFireRay(osteoSurfaces, pnt, myDownVecUnit, 2.5*magDown, intPnt, &surfIndex, &uv, &dotValues);
			use6Surfaces = false;
		}
		else // inner side of jumpPoint, use totalSurfaces6
		{
			bInt = IntersectSurfacesByFireRay(totalSurfaces6, pnt, myDownVecUnit, 2.5*magDown, intPnt, &surfIndex, &uv, &dotValues, &hitEachSurf);
			hitOsteo = hitEachSurf.GetAt(0) || hitEachSurf.GetAt(1) || hitEachSurf.GetAt(2);
			hitCarti = hitEachSurf.GetAt(3) || hitEachSurf.GetAt(4) || hitEachSurf.GetAt(5);
			if ( !hitOsteo && missCountOsteo < 8 )
			{
				IwVector3d thirdVecPlus = outVecUnit*myDownVecUnit;
				IwPoint3d pntPlus0 = pnt + 1.0*deltaStepSize*thirdVecPlus;
				IwPoint3d intPntPlus0, intPntPlus1;
				bool bIntPlus0 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus0, myDownVecUnit, 2.5*magDown, intPntPlus0, &surfIndex, &uv, &dotValues, &hitEachSurf);
				bool hitOsteoPlus0 = hitEachSurf.GetAt(0) || hitEachSurf.GetAt(1) || hitEachSurf.GetAt(2);
				IwPoint3d pntPlus1 = pnt - 1.0*deltaStepSize*thirdVecPlus;
				bool bIntPlus1 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus0, myDownVecUnit, 2.5*magDown, intPntPlus1, &surfIndex, &uv, &dotValues, &hitEachSurf);
				bool hitOsteoPlus1 = hitEachSurf.GetAt(0) || hitEachSurf.GetAt(1) || hitEachSurf.GetAt(2);
				if (hitOsteoPlus0 && hitOsteoPlus1)
				{
					intPnt = 0.5*intPntPlus0 + 0.5*intPntPlus1;
					missCountOsteo = 0;
				}
				else
				{
					missCountOsteo++;
				}
			}
			if ( !hitCarti && missCountCarti < 8 )
			{
				IwVector3d thirdVecPlus = outVecUnit*myDownVecUnit;
				IwPoint3d pntPlus0 = pnt + 1.0*deltaStepSize*thirdVecPlus;
				IwPoint3d intPntPlus0, intPntPlus1;
				bool bIntPlus0 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus0, myDownVecUnit, 2.5*magDown, intPntPlus0, &surfIndex, &uv, &dotValues, &hitEachSurf);
				bool hitCartiPlus0 = hitEachSurf.GetAt(3) || hitEachSurf.GetAt(4) || hitEachSurf.GetAt(5);
				IwPoint3d pntPlus1 = pnt - 1.0*deltaStepSize*thirdVecPlus;
				bool bIntPlus1 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus0, myDownVecUnit, 2.5*magDown, intPntPlus1, &surfIndex, &uv, &dotValues, &hitEachSurf);
				bool hitCartiPlus1 = hitEachSurf.GetAt(3) || hitEachSurf.GetAt(4) || hitEachSurf.GetAt(5);
				if (hitCartiPlus0 && hitCartiPlus1) 
				{
					intPnt = 0.5*intPntPlus0 + 0.5*intPntPlus1;
					missCountCarti = 0;
				}
				else
				{
					missCountCarti++;
				}
			}
			use6Surfaces = true;
		}
static int dispIntPnt = 0;
if (dispIntPnt)
{
	if ( bInt )
		ShowPoint(pDoc, intPnt, red);
}
		if ( bInt )
		{
			tempVec = intPnt - bottomPnt;
			if ( tempVec.Dot(myDownVecUnit) > 0 )// lower than bottomPnt (out of range)
			{
				missCount++;
				continue;
			}
			// Offset intersection point with surface normal
			totalSurfaces6.GetAt(surfIndex)->EvaluateNormal(uv, FALSE, FALSE, normal);
			if ( soFarDist < 1.5 )
			{
				myOffsetDist = 0.0;
			}
			else if ( soFarDist < 4.5 )// gradually increase the offset distance
			{
				ratio = (soFarDist-1.5)/3.0;
				myOffsetDist = ratio*offsetDist;
			}
			else
			{
				myOffsetDist = offsetDist;
			}
			intPnt = intPnt + myOffsetDist*normal;
			isSpike = false;
			dist = intPoints.GetLast().DistanceBetween(intPnt);
			tempVec =  intPnt - intPoints.GetLast();
			// If it is jumping down with a big distance, it indicates maybe missing the right answer
			if ( dist > 6*deltaStepSize && tempVec.Dot(myDownVecUnit)>0 ) // <A> down big > 1.2mm
			{
				IwVector3d thirdVec = outVecUnit*myDownVecUnit;
				IwPoint3d pntPlus0 = pnt + 2.5*deltaStepSize*outVecUnit + 1.0*deltaStepSize*thirdVec;// 2.5 steps outside
				IwPoint3d pntPlus1 = pnt + 2.5*deltaStepSize*outVecUnit - 1.0*deltaStepSize*thirdVec;// 2.5 steps outside
				IwPoint3d intPntPlus0, intPntPlus1;
				bool bInt0, bInt1;
				IwPoint3d pntPlus00 = pnt + 3.5*deltaStepSize*outVecUnit + 2.0*deltaStepSize*thirdVec;// 3.5 steps outside
				IwPoint3d pntPlus11 = pnt + 3.5*deltaStepSize*outVecUnit - 2.0*deltaStepSize*thirdVec;// 3.5 steps outside
				if ( !use6Surfaces )
				{
					bInt0 = IntersectSurfacesByFireRay(osteoSurfaces, pntPlus0, myDownVecUnit, HUGE_DOUBLE, intPntPlus0, &surfIndex, &uv, &dotValues, &hitEachSurf);
					bInt1 = IntersectSurfacesByFireRay(osteoSurfaces, pntPlus1, myDownVecUnit, HUGE_DOUBLE, intPntPlus1, &surfIndex, &uv, &dotValues, &hitEachSurf);
					if ( !bInt0 || !bInt1 )// If miss, try pntPlus00 & pntPlus11
					{
						bInt0 = IntersectSurfacesByFireRay(osteoSurfaces, pntPlus00, myDownVecUnit, HUGE_DOUBLE, intPntPlus0, &surfIndex, &uv, &dotValues, &hitEachSurf);
						bInt1 = IntersectSurfacesByFireRay(osteoSurfaces, pntPlus11, myDownVecUnit, HUGE_DOUBLE, intPntPlus1, &surfIndex, &uv, &dotValues, &hitEachSurf);
					}
				}
				else 
				{
					bInt0 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus0, myDownVecUnit, HUGE_DOUBLE, intPntPlus0, &surfIndex, &uv, &dotValues, &hitEachSurf);
					bInt1 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus1, myDownVecUnit, HUGE_DOUBLE, intPntPlus1, &surfIndex, &uv, &dotValues, &hitEachSurf);
					if ( !bInt0 || !bInt1 )// If miss, try pntPlus00 & pntPlus11
					{
						bInt0 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus00, myDownVecUnit, HUGE_DOUBLE, intPntPlus0, &surfIndex, &uv, &dotValues, &hitEachSurf);
						bInt1 = IntersectSurfacesByFireRay(totalSurfaces6, pntPlus11, myDownVecUnit, HUGE_DOUBLE, intPntPlus1, &surfIndex, &uv, &dotValues, &hitEachSurf);
					}
				}
				if ( bInt0 && bInt1 ) 
				{
					IwPoint3d avgPnt = 0.5*(intPntPlus0+intPntPlus1);
					double distPlus = intPoints.GetLast().DistanceBetween(avgPnt);
					double dist2 = avgPnt.DistanceBetween(intPnt);
					tempVec =  avgPnt - intPnt;
					if ( dist2 > 4*deltaStepSize && tempVec.Dot(myDownVecUnit)<0 ) // <B>, up big > 0.8mm
					{
						//The conditions <A> and <B> indicate a spike (up-down-up)
						isSpike = true;
					}
				}
			}
			if ( !isSpike )
			{
				bool addToPool = true;
				// Need to check the distance with the reference surface 
				if ( referenceSurface )
				{
					IwPoint3d cPnt;
					IwPoint2d param2d;
					double checkDist = DistFromPointToSurface(intPnt, referenceSurface, cPnt, param2d);
					if ( checkDist > 0 /*found solution*/ && checkDist < referenceDistance )
					{
static int dispTooClosePnt = 0;
if (dispTooClosePnt)
{
	ShowPoint(pDoc, intPnt, magenta);
}
						addToPool = false;
					}
				}
				if ( addToPool )
				{
					intPoints.Add(intPnt);
				}
			}
		}
		else
		{
			missCount++;
		}
	}

	// Do we drop to the bottom of the plane
	bottomPnt = intPoints.GetLast().ProjectPointToPlane(startPnt+downVec, myDownVecUnit);
	dist = intPoints.GetLast().DistanceBetween(bottomPnt);
	if ( dist > 0.025 )
		intPoints.Add(bottomPnt);
	
	// Remove spike up-down-up-down defined by 4 adjacent points
	if ( intPoints.GetSize() > 3 )
	{
		intNoSpikePoints.Add(intPoints.GetAt(0));
		intNoSpikePoints.Add(intPoints.GetAt(1));
		intNoSpikePoints.Add(intPoints.GetAt(2));
		IwPoint3d p0, p1, p2, p3;
		IwVector3d v0, v1, v2;
		double dotValue0, dotValue1, dotValue2;
		for (unsigned i=3; i<intPoints.GetSize(); i++)
		{
			p0 = intPoints.GetAt(i-3);
			p1 = intPoints.GetAt(i-2);
			p2 = intPoints.GetAt(i-1);
			p3 = intPoints.GetAt(i);
			v0 = p0 - p1;
			v1 = p1 - p2;
			v2 = p2 - p3;
			dotValue0 = v0.Dot(myDownVecUnit);
			dotValue1 = v1.Dot(myDownVecUnit);
			dotValue2 = v2.Dot(myDownVecUnit);
			if (v0.Dot(v1)<0 && v1.Dot(v2)<0 && fabs(dotValue0)>5*normalStepSize && fabs(dotValue1)>5*normalStepSize && fabs(dotValue2)>5*normalStepSize )
			{// spike
static int dispSpike = 0;
if (dispSpike)
ShowPoint(pDoc, intNoSpikePoints.GetLast(), magenta);
				intNoSpikePoints.RemoveLast();
			}
			else if (v2.Length() < 0.001)
			{ // if adjacent points are identical
				intNoSpikePoints.RemoveLast();
			}
			intNoSpikePoints.Add(p3);
		}
	}
	else
	{
		intNoSpikePoints.Append(intPoints);
	}

	// Add intermeidate points, if adjacent distance is too big
	intEvenPoints.RemoveAll();
	for (unsigned i=1; i<intNoSpikePoints.GetSize(); i++)
	{
		dist = intNoSpikePoints.GetAt(i-1).DistanceBetween(intNoSpikePoints.GetAt(i));
		if (dist > 3*normalStepSize )
		{
			IwPoint3d intmediaPnt, sLinePnt;
			int nSize = (int)(dist/normalStepSize);
			sLinePnt = intNoSpikePoints.GetAt(i-1);
			IwVector3d deltaV = (intNoSpikePoints.GetAt(i)-intNoSpikePoints.GetAt(i-1))/nSize;
			for (int j=0; j<nSize; j++)
			{
				intmediaPnt = sLinePnt + j*deltaV;
				intEvenPoints.Add(intmediaPnt);
			}
		}
		else
		{
			intEvenPoints.Add(intNoSpikePoints.GetAt(i-1));
		}
	}
	intEvenPoints.Add(intNoSpikePoints.GetLast());

	// Extend along the out vec with this extendAlongOutVec distance
	if ( !IS_EQ_TOL6( extendAlongOutVec, 0.0 ) )
	{
		double outDelta = extendAlongOutVec/5.0;
		IwPoint3d lastPnt = intEvenPoints.GetLast();
		IwPoint3d lastPntDownOne = 	lastPnt.ProjectPointToPlane(startPnt+(magDown+1.0)*downVecUnit, downVecUnit);

		IwPoint3d extPnt1 = lastPntDownOne + 0.0*outDelta*downVecUnit + (1.0 + 0.0*outDelta)*outVecUnit;
		intEvenPoints.Add(extPnt1);
		IwPoint3d extPnt2 = lastPntDownOne + 0.25*outDelta*downVecUnit + (1.0 + 1.0*outDelta)*outVecUnit;
		intEvenPoints.Add(extPnt2);
		IwPoint3d extPnt3 = lastPntDownOne + 0.5*outDelta*downVecUnit + (1.0 + 2.0*outDelta)*outVecUnit;
		intEvenPoints.Add(extPnt3);
		IwPoint3d extPnt4 = lastPntDownOne + 0.75*outDelta*downVecUnit + (1.0 + 3.0*outDelta)*outVecUnit;
		intEvenPoints.Add(extPnt4);
		IwPoint3d extPnt5 = lastPntDownOne + 1.0*outDelta*downVecUnit + (1.0 + 4.0*outDelta)*outVecUnit;
		intEvenPoints.Add(extPnt5);
	}

	// Create a curve
	IwBSplineCurve* myDropCurve = NULL;
	if ( bAccurate )
	{
		IwBSplineCurve::InterpolatePoints(pDoc->GetIwContext(), intEvenPoints, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, myDropCurve);
	}
	else
	{
		double tol = 0.5;
		IwBSplineCurve::ApproximatePoints(pDoc->GetIwContext(), intEvenPoints, 3, NULL, NULL, FALSE, &tol, myDropCurve);
	}
	// Sampling into 30 points. Therefore, the output dropPnts always consist fixed number of points 
	if ( myDropCurve )
	{
		IwExtent1d dom = myDropCurve->GetNaturalInterval();
		myDropCurve->EquallySpacedPoints(dom.GetMin(), dom.GetMax(), 29, 0.01, &dropPnts, NULL);
	}
	else
	{
		// An backup answer (better than crash).
		// use intPoints.GetAt(0) and bottomPnt to create 30 points.
		IwPoint3d sPnt, mPnt;
		sPnt = intPoints.GetAt(0);
		IwVector3d deltaV = (bottomPnt - intPoints.GetAt(0))/29.0;
		for (int i=0; i<29; i++)
		{
			mPnt = sPnt + i*deltaV;
			dropPnts.Add(mPnt);
		}
		dropPnts.Add(bottomPnt);
	}
static int dispDropPnts = 0;
if (dispDropPnts)
{
	for (unsigned i=0; i<dropPnts.GetSize(); i++)
		ShowPoint(pDoc, dropPnts.GetAt(i), orange);
}

	if ( dropCurve != NULL )
	{
		*dropCurve = myDropCurve;
	}
	else
	{
		IwObjDelete delCrv(myDropCurve);
	}

}

///////////////////////////////////////////////
// Determine the jump point.
// The input jump curve is not an accurate curve for jumping.
// 1. If there exist osteo silhouette curves horizontally up (toward ankle) the jump curve, 
// the jumping point is the outer-most point (along outVec) of osteo surfaces.
// 2. If no osteo silhouette curves horizontally up (toward ankle) the jump curve, 
// the horizontal corresponding point of the jump curve on the osteo surface
// is the tab end point. 3mm up (toward ankle) along the osteo surface is the jumping point.
// (Therefore, the tab will contact the osteo at least 3mm.)
IwPoint3d CMakeInnerSurfaceJigs::DetermineJumpPoint
(
	CTotalDoc* pDoc,					// I:
	IwTArray<IwSurface*>& totalSurfaces,// I: the [0~2] are osteophyte surface, the [3~5] are cartilage surfaces. If [6] exists, it is anterior dropping surface.
	IwPoint3d& startPnt,				// I:
	IwVector3d& downVec,				// I:
	IwVector3d& outVec,					// I:
	double offsetDist,					// I:
	IwBSplineCurve* jumpCurve			// I:
)
{
	IwPoint3d jumpPoint = startPnt + 1.1*outVec;// make default outside of scan range

	if ( jumpCurve == NULL )
		return jumpPoint;

	IwVector3d downVecUnit = downVec;
	downVecUnit.Unitize();
	IwVector3d outVecUnit = outVec;
	outVecUnit.Unitize();

	CFemoralPart* pOsteoSurf = pDoc->GetOsteophyteSurface();
	if ( pOsteoSurf == NULL )
		return jumpPoint;

	IwTArray<IwCurve*> silCurves;
	silCurves = pOsteoSurf->GetIwCurves();
	if ( silCurves.GetSize() == 0 )
		return jumpPoint;

	// osteophyte surfaces
	IwTArray<IwSurface*> osteoSurfaces;
	osteoSurfaces.Add(totalSurfaces.GetAt(0));
	osteoSurfaces.Add(totalSurfaces.GetAt(1));
	osteoSurfaces.Add(totalSurfaces.GetAt(2));

	// Intersect jump curve with a plane defined by (startPnt, downVec, outVec)
	IwVector3d planeNormal = downVec*outVec;
	planeNormal.Unitize();
	IwPoint3d intPnt;
	double param;
	bool bInt = IntersectCurveByPlane(jumpCurve, startPnt, planeNormal, intPnt, &param);
	if ( !bInt )
		return jumpPoint;

	// Need an extra offset to transit from hit osteophyte surfaces to all surfaces
	double smoothnessOffset = 0;// for transition purpose
	double crvLength;
	IwExtent1d crvDom = jumpCurve->GetNaturalInterval();
	jumpCurve->Length(crvDom, 0.01, crvLength);
	double fiveMMParamRatio = 5.5/crvLength;// How much parameter for 8.5mm curve length. Why "8.5"? The jumpCurve is extended 9.5. Therefore, 8.5 is a good transit distance. 
	double paramRatio = (param-crvDom.GetMin())/crvDom.GetLength();
	if ( paramRatio < fiveMMParamRatio )// near beginning of curve
	{
		smoothnessOffset = 7.0*(fiveMMParamRatio-paramRatio)/fiveMMParamRatio;
	}
	else if ( paramRatio > (1.0-fiveMMParamRatio) )// near the end of curve
	{
		smoothnessOffset = 7.0*(paramRatio-(1.0-fiveMMParamRatio))/fiveMMParamRatio;
	}
	
	// Move intPnt down (toward hip) 1mm, then the pnt4mm actually becomes the 3mm point away from tab end.
	// If smoothnessOffset != 0, we are determining the jump point for non-tab locations, like in the anterior or posterior of tabs.
	// The "+smoothnessOffset" will move the search down (toward hip) a bit, in case the jumpCurve is too distal in the anterior or posterior of tabs.
	intPnt += (1.0+smoothnessOffset)*downVecUnit;

	// horizontally scan the osteo surfaces from intPnt up
	IwPoint3d outerBasePoint = intPnt + outVec;
	IwPoint3d outerPoint, hitPnt;
	bool bHit;
	IwPoint3d prevHitPnt, outerMostPoint, pnt4mm;
	double dist, minDelta = 0, minDist = 1000000;
	double crvDist=0;
	for (double delta=0; delta < 25.0; delta+=0.25)
	{
		outerPoint = outerBasePoint - delta*downVecUnit;// "-", therefore gradually move up.
		bHit = IntersectSurfacesByFireRay(osteoSurfaces, outerPoint, -outVecUnit, HUGE_DOUBLE, hitPnt);
		if ( delta > 5.0 && !bHit )
			break; // if not hit, just stop
		if ( bHit )
		{
			dist = outerPoint.DistanceBetween(hitPnt);
			// get the outer most point
			if ( dist < minDist ) 
			{
				minDist = dist;
				minDelta = delta;
				outerMostPoint = hitPnt;
			}
			// calculate the curve distance so far
			if ( prevHitPnt.IsInitialized() )
			{
				crvDist += prevHitPnt.DistanceBetween(hitPnt);
				// record the point that is 4mm away from beginning
				if ( !pnt4mm.IsInitialized() && crvDist > 4.0 )
				{
					pnt4mm = hitPnt;
				}
			}
			prevHitPnt = hitPnt;
		}
	}

	// No outerMostPoint (or the first one is the outer most point -> no split curve in the scan range).
	if ( minDelta == 0 )
	{
		jumpPoint = pnt4mm;// jumpPoint is the point 3mm away from tab end
	}
	else // Have the true outerMostPoint
	{
		jumpPoint = outerMostPoint;
	}
	// Push jumpPoint further out
	jumpPoint = jumpPoint + (smoothnessOffset+offsetDist)*outVecUnit;

	return jumpPoint;
}

////////////////////////////////////////////////////////////////
// This function creates the outermost surface of totalSurfaces
// for the inner surface jig, outer surface jig, and anterior 
// surface jig.
////////////////////////////////////////////////////////////////
void CMakeInnerSurfaceJigs::DetermineContactSurface
(
	CTotalDoc*& pDoc,						// I:
	IwTArray<IwSurface*>& totalSurfaces,	// I: the [0~2] are osteophyte surface, the [3~5] are cartilage surfaces. If [6] exists, it is anterior dropping surface.
	double dropOffset,						// I:
	bool bAnteriorOnly,						// I: only cover anterior portion (for F3 anterior surface creation)
	bool bAccurate,							// I: true: higher accruacy for inner surface, false: low accuracy for outer surface.
	double* troGroAddThickness,				// I: Trochlear grove additional thickness
	double* transitionWidthRatio,			// I: transition width for troGroAddThickness
	IwTArray<IwBSplineCurve*>* jumpCurves,	// I: jump curves for DroppingCurve(). [0] is positive side curve, [1] is negative side curve.
	IwBrep*& contactBrep,					// O:
	IwBSplineSurface* referenceSurface,     // I: additional reference to ensure this contact surface is at least referenceDistance to the given referenceSurface
	double referenceDistance				// I: additional reference to ensure this contact surface is at least referenceDistance to the given referenceSurface
)
{
	contactBrep = NULL;

	// surfaces for the main area projection
	IwTArray<IwSurface*> totalSurfacesMain;
	totalSurfacesMain.Add(totalSurfaces.GetAt(0));
	totalSurfacesMain.Add(totalSurfaces.GetAt(3));
	if ( totalSurfaces.GetSize() == 7 )
		totalSurfacesMain.Add(totalSurfaces.GetAt(6));

	//
	IwAxis2Placement mechAxes = IFemurImplant::FemoralAxes_GetMechanicalAxes();
	// matrix for rotating along x-axis
	IwAxis2Placement rotMat, rotatedAxes;
	double rotRadian = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceApproachDegree()/180.0*IW_PI;
	rotMat.RotateAboutAxis(rotRadian, IwVector3d(1,0,0));
	rotMat.TransformAxis2Placement(mechAxes, rotatedAxes);
	IwVector3d zAxis = rotatedAxes.GetZAxis();
	IwVector3d yAxis = rotatedAxes.GetYAxis();
	IwVector3d xAxis = rotatedAxes.GetXAxis();
	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;

	// Get aiitional drop-down length
	double additionalDropLength = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceAdditionalDropLength();
	double outExtensionDistance = 0.0;

	// Get Cartilage Surface
	CCartilageSurface* pCartilageSurface = pDoc->GetCartilageSurface();
	if ( pCartilageSurface == NULL )
		return;

	// Get outline profile jigs
	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;

	// Get sketch surface
	IwBSplineSurface* sketchSurf = NULL;
	pOutlineProfileJigs->GetSketchSurface(sketchSurf);

	// Get bounding profiles
	IwBSplineCurve *posUVProfile, *negUVProfile, *notchUVProfile;
	if ( bAnteriorOnly )
	{
		double antExtensionDistance = pDoc->GetAdvancedControlJigs()->GetAnteriorSurfaceAntExtension();
		pOutlineProfileJigs->GetAnteriorBoundingProfiles(antExtensionDistance, posUVProfile, negUVProfile, notchUVProfile);
	}
	else
	{
		pOutlineProfileJigs->GetBoundingProfiles(0.5, posUVProfile, negUVProfile, notchUVProfile);
	}

	// Get dropping centers
	IwPoint3d posDropCenter, negDropCenter;
	pOutlineProfileJigs->GetDroppingCenters(posDropCenter, negDropCenter);

	// Get tab points
	IwTArray<IwPoint3d>tabPoints;
	pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
	IwPoint3d medialTabPoint = tabPoints.GetAt(0);
	int medialTabIndex=0;
	double minMedialTabDist=100000;

	// Scan the osteo/carti surfaces 
	int nStep = 101, nStepJ=61, nCenterStepJ=30;
	int nCenterAntShiftJ=0, nCenterNotchShiftJ=0;
	if ( !bAccurate )
	{
		nStep = 51;
		nStepJ = 31;
		nCenterStepJ = 15;
	}

	// if troGroAddThickness exists
	int troGroAddThicknessStep = 0; // Where to terminate applying additional thickness, in terms of steps
	int troGroTranWidthStepJ = 0; // How wide to apply additional thickness, in terms of steps
	if ( troGroAddThickness )
	{
		double transWidthRatio = *transitionWidthRatio;
		// convert transitionWidthRatio to step size
		troGroTranWidthStepJ = (int)(nStepJ*transWidthRatio);
		int maxShiftStep = (int)(nStepJ*(0.5-transWidthRatio)) + 1;

		// Determine troGroAddThicknessStep. 
		// The troGroAddThickness will be applied from anterior to troGroAddThicknessStep 
		// First determine the notch (tip) point in notchUVProfile
		IwExtent2d sketchSurfDom = sketchSurf->GetNaturalUVDomain();
		IwPoint2d topPnt2d = sketchSurfDom.Evaluate(0.5,0.0);
		IwPoint3d notchTip;
		double notchParam;
		DistFromPointToCurve(IwPoint3d(topPnt2d.x,topPnt2d.y,0), notchUVProfile, notchTip, notchParam);
		// Get the end points of posUVProfile, negUVProfile
		IwPoint3d posStartPnt, posEndPnt, negStartPnt, negEndPnt, midStartPnt, midEndPnt;
		posUVProfile->GetEnds(posStartPnt, posEndPnt);
		negUVProfile->GetEnds(negStartPnt, negEndPnt);
		midStartPnt = 0.5*posStartPnt + 0.5*negStartPnt;
		midEndPnt = 0.5*posEndPnt + 0.5*negEndPnt;
		// Let's determine the percentage of notchTip in between of midStartPnt & midEndPnt
		double notchTipRatio = notchTip.DistanceBetween(midStartPnt)/midEndPnt.DistanceBetween(midStartPnt);
		// The troGroAddThickness will be applied from anterior to troGroAddThicknessStep 
		troGroAddThicknessStep = int (notchTipRatio*nStep);

		// Now determine nCenterAntShiftJ=0 and nCenterNotchShiftJ.
		// Why shift? Because the troGroAddThickness should be applied to the trochlear groove, 
		// rather than the center of the position jigs main contact area.
		// Now project the GetTrochlearGrooveXYZCurve onto sketch UV domain (end points only)
		IwBSplineCurve* trochlearGrooveCurve = pCartilageSurface->GetTrochlearGrooveXYZCurve();
		IwPoint3d startTGPnt, endTGPnt;
		trochlearGrooveCurve->GetEnds(startTGPnt, endTGPnt);
		IwPoint3d closestPnt;
		IwPoint2d param2d;
		DistFromPointToSurface(startTGPnt, sketchSurf, closestPnt, param2d);
		IwPoint3d startTGParam3d = IwPoint3d(param2d.x, param2d.y, 0 );
		DistFromPointToSurface(endTGPnt, sketchSurf, closestPnt, param2d);
		IwPoint3d endTGParam3d = IwPoint3d(param2d.x, param2d.y, 0 );
		// Determine the distance to posUVProfile and negUVProfile,
		// then determine nCenterAntShiftJ & nCenterNotchShiftJ
		double param;
		double posAntTGDist = DistFromPointToCurve(startTGParam3d, posUVProfile, closestPnt, param);
		double negAntTGDist = DistFromPointToCurve(startTGParam3d, negUVProfile, closestPnt, param);
		int TGAntStepJ = (int)(nStepJ*negAntTGDist/(posAntTGDist+negAntTGDist));
		nCenterAntShiftJ = TGAntStepJ - nCenterStepJ;
		// do not shift too much
		if ( nCenterAntShiftJ > maxShiftStep )
			 nCenterAntShiftJ = maxShiftStep;
		else if ( nCenterAntShiftJ < -maxShiftStep )
			 nCenterAntShiftJ = -maxShiftStep;
		//
		double posNotchTGDist = DistFromPointToCurve(endTGParam3d, posUVProfile, closestPnt, param);
		double negNotchTGDist = DistFromPointToCurve(endTGParam3d, negUVProfile, closestPnt, param);
		int TGNotchStepJ = (int)(nStepJ*negNotchTGDist/(posNotchTGDist+negNotchTGDist));
		nCenterNotchShiftJ = TGNotchStepJ - nCenterStepJ;
		// do not shift too much
		if ( nCenterNotchShiftJ > maxShiftStep )
			 nCenterNotchShiftJ = maxShiftStep;
		else if ( nCenterNotchShiftJ < -maxShiftStep )
			 nCenterNotchShiftJ = -maxShiftStep;
	}

	IwExtent1d posDom = posUVProfile->GetNaturalInterval();
	IwExtent1d negDom = negUVProfile->GetNaturalInterval();
	double posDeltaParam = posDom.GetLength()/(nStep-1);
	double negDeltaParam = negDom.GetLength()/(nStep-1);
	double posParam, negParam;
	IwPoint2d deltaParamJ, paramJ;
	IwPoint3d posPnt3d, negPnt3d;
	IwPoint2d posPnt2d, negPnt2d;
	IwPoint3d sketchPnt, sketchNormal, hitNormal0, hitNormal1, outerPnt;
	IwVector3d tempVec;
	IwVector2d tempVecJ0, tempVecJ1;
	IwPoint3d posIntPnt3d, negIntPnt3d, tang;
	IwPoint2d posIntPnt2d, negIntPnt2d;
	double intParam;
	IwPoint2d uv;
	bool bInt, bInts, bothHit;
	double maxHitDist=65.0;
	double backwardDist = maxHitDist - 20.0;
	int startNotchGapIndex, endNotchGapIndex;
	IwTArray<IwPoint3d> mainContactPnts, oneScanPnts, oneScanNormals, bridgePnts, posDropPnts, negDropPnts;
	IwPoint3d startBridgePnt, endBridgePnt, midBridgePnt;
	IwVector3d startBridgeVec, endBridgeVec;
	IwTArray<IwPoint3d> fairPnts, fairVectors;
	int bridgeSpan;
	IwExtent1d bridgeDom;
	IwTArray<int> notchGapIndexes;
	IwTArray<IwVector3d> notchGapNormals;
	int mainGapStartIndex, hitIndex=0;
	IwVector3d outVec, outVecUnit, downVec, zDownVec, downVecUnit, thirdVec;
	IwTArray<int> dotValues;
	dotValues.Add(-1);
	dotValues.Add(-1);
	dotValues.Add(0);
	IwTArray<bool> hitEachSurf;
	double anteriorDropDistance = 10.0 + pDoc->GetAdvancedControlJigs()->GetAnteriorSurfaceDropExtension();
	if ( anteriorDropDistance < 0 )
		anteriorDropDistance = 0.0;

	for (int i=0; i<nStep; i++)
	{
		//initialize 
		startNotchGapIndex = -1;
		endNotchGapIndex = -1;
		hitIndex=1;
		oneScanPnts.RemoveAll();
		oneScanNormals.RemoveAll();
		//
		posParam = posDom.GetMin() + i*posDeltaParam;
		negParam = negDom.GetMin() + i*negDeltaParam;
		//
		posUVProfile->EvaluatePoint(posParam, posPnt3d);
		posPnt2d = IwPoint2d(posPnt3d.x, posPnt3d.y);
		//
		negUVProfile->EvaluatePoint(negParam, negPnt3d);
		negPnt2d = IwPoint2d(negPnt3d.x, negPnt3d.y);
		//
		tempVec = posPnt3d - negPnt3d;
		tempVec.Unitize();
		// Intersect with notchUVProfile
		bInt = IntersectCurveByVector(notchUVProfile, notchUVProfile->GetNaturalInterval(), posPnt3d, tempVec, NULL, 100, posIntPnt3d, intParam, tang);
		if ( bInt )
		{
			IntersectCurveByVector(notchUVProfile, notchUVProfile->GetNaturalInterval(), negPnt3d, tempVec, NULL, 100, negIntPnt3d, intParam, tang);
			posIntPnt2d = IwPoint2d(posIntPnt3d.x, posIntPnt3d.y);
			negIntPnt2d = IwPoint2d(negIntPnt3d.x, negIntPnt3d.y);
		}
		// Scan along the line from negPnt2d to posPnt2d
		deltaParamJ = (posPnt2d - negPnt2d)/(nStepJ-1);
		for (int j=0; j<nStepJ; j++)
		{
			paramJ = negPnt2d + j*deltaParamJ;
			tempVecJ0 = posIntPnt2d - paramJ;
			tempVecJ1 = negIntPnt2d - paramJ;
			// if the parameter paramJ is in between but not too close to notchProfile, 
			// then skip now and bridge this gap by a smooth curve later
			if ( bInt && tempVecJ0.Dot(tempVecJ1) < 0 && 
				 tempVecJ0.Length() > 1 && tempVecJ1.Length() > 1 )
			{
				if ( startNotchGapIndex == -1 )
					startNotchGapIndex = j-1;// if first time come here (notch gap), the startNotchGapIndex is the previous index.
			}
			else // project onto sketch surface
			{
				if ( startNotchGapIndex > -1 && endNotchGapIndex == -1 )
					endNotchGapIndex = j;// if already entered the notch gap and this is the first time come here (condylar region), the endNotchGapIndex is the index.
				sketchSurf->EvaluatePoint(paramJ, sketchPnt);
				sketchSurf->EvaluateNormal(paramJ, FALSE, FALSE, sketchNormal);
				oneScanNormals.Add(sketchNormal);
				bInts = IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt, &hitIndex, &uv, &dotValues, &hitEachSurf);
static int missHit = 0;
if(missHit)
ShowPoint(pDoc, outerPnt, cyan);
				// Whether cartilage and osteophyte are all hit
				bothHit = (hitEachSurf.GetAt(0)==true && hitEachSurf.GetAt(1)==true);

				// if not hit both cartilage and osteophyte surfaces (a surface is missing hit), 
				// move side way a little bit and re-intersect again.
				if ( !bothHit ) 
				{
if(missHit)
ShowPoint(pDoc, outerPnt, blue);
					IwPoint3d sketchPnt0, sketchPnt1, sketchPnt2, sketchPnt3;
					IwPoint3d outerPnt0, outerPnt1, outerPnt2, outerPnt3;
					int hitIndex0, hitIndex1, hitIndex2, hitIndex3;
					sketchSurf->EvaluatePoint(paramJ+IwPoint2d(0.33*deltaParamJ.x, 0.33*deltaParamJ.x), sketchPnt0);
					sketchSurf->EvaluatePoint(paramJ+IwPoint2d(-0.33*deltaParamJ.x, 0.33*deltaParamJ.x), sketchPnt1);
					sketchSurf->EvaluatePoint(paramJ+IwPoint2d(-0.33*deltaParamJ.x, -0.33*deltaParamJ.x), sketchPnt2);
					sketchSurf->EvaluatePoint(paramJ+IwPoint2d(0.33*deltaParamJ.x, -0.33*deltaParamJ.x), sketchPnt3);
					IwPoint2d uv0, uv1, uv2, uv3;
					IwTArray<bool> hitEachSurf0, hitEachSurf1, hitEachSurf2, hitEachSurf3;
					IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt0+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt0, &hitIndex0, &uv0, &dotValues, &hitEachSurf0);
					IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt1+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt1, &hitIndex1, &uv1, &dotValues, &hitEachSurf1);
					IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt2+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt2, &hitIndex2, &uv2, &dotValues, &hitEachSurf2);
					IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt3+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt3, &hitIndex3, &uv3, &dotValues, &hitEachSurf3);
					// Now we have 4 points. Average points which hit both
					int hitNum=0;
					if ( hitEachSurf0.GetAt(0)==true && hitEachSurf0.GetAt(1)==true &&
						 hitEachSurf2.GetAt(0)==true && hitEachSurf2.GetAt(1)==true )
					{
						outerPnt = 0.5*outerPnt0 + 0.5*outerPnt2;
						hitNum=2;
					}
					else if ( hitEachSurf1.GetAt(0)==true && hitEachSurf1.GetAt(1)==true &&
						      hitEachSurf3.GetAt(0)==true && hitEachSurf3.GetAt(1)==true )
					{
						outerPnt = 0.5*outerPnt1 + 0.5*outerPnt3;
						hitNum=2;
					}
					if ( hitNum == 0 )// try again with a bit wider range
					{
						IwPoint3d sketchPnt00, sketchPnt11, sketchPnt22, sketchPnt33;
						IwPoint3d outerPnt00, outerPnt11, outerPnt22, outerPnt33;
						int hitIndex00, hitIndex11, hitIndex22, hitIndex33;
						sketchSurf->EvaluatePoint(paramJ+IwPoint2d(1.23*deltaParamJ.x, 1.23*deltaParamJ.x), sketchPnt00);
						sketchSurf->EvaluatePoint(paramJ+IwPoint2d(-1.23*deltaParamJ.x, 1.23*deltaParamJ.x), sketchPnt11);
						sketchSurf->EvaluatePoint(paramJ+IwPoint2d(-1.23*deltaParamJ.x, -1.23*deltaParamJ.x), sketchPnt22);
						sketchSurf->EvaluatePoint(paramJ+IwPoint2d(1.23*deltaParamJ.x, -1.23*deltaParamJ.x), sketchPnt33);
						IwPoint2d uv00, uv11, uv22, uv33;
						IwTArray<bool> hitEachSurf00, hitEachSurf11, hitEachSurf22, hitEachSurf33;
						IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt00+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt00, &hitIndex00, &uv00, &dotValues, &hitEachSurf00);
						IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt11+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt11, &hitIndex11, &uv11, &dotValues, &hitEachSurf11);
						IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt22+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt22, &hitIndex22, &uv22, &dotValues, &hitEachSurf22);
						IntersectSurfacesByFireRay(totalSurfacesMain, sketchPnt33+backwardDist*sketchNormal, -sketchNormal, maxHitDist, outerPnt33, &hitIndex33, &uv33, &dotValues, &hitEachSurf33);
						// Now we have 4 points. Average points which hit both
						if ( hitEachSurf00.GetAt(0)==true && hitEachSurf00.GetAt(1)==true &&
							 hitEachSurf22.GetAt(0)==true && hitEachSurf22.GetAt(1)==true )
						{
							outerPnt = 0.5*outerPnt00 + 0.5*outerPnt22;
							hitNum=2;
						}
						else if ( hitEachSurf11.GetAt(0)==true && hitEachSurf11.GetAt(1)==true &&
							      hitEachSurf33.GetAt(0)==true && hitEachSurf33.GetAt(1)==true )
						{
							outerPnt = 0.5*outerPnt11 + 0.5*outerPnt33;
							hitNum=2;
						}
						else
						{
							IwPoint3d aBackUpPoint;
							// This is an extreme backup.
							// 0. Whether any hitEachSurf is valid
							if (hitEachSurf0.GetAt(0)==true && hitEachSurf0.GetAt(1)==true)
								aBackUpPoint = outerPnt0;
							else if ( hitEachSurf1.GetAt(0)==true && hitEachSurf1.GetAt(1)==true )
								aBackUpPoint = outerPnt1;
							else if ( hitEachSurf2.GetAt(0)==true && hitEachSurf2.GetAt(1)==true )
								aBackUpPoint = outerPnt2;
							else if ( hitEachSurf3.GetAt(0)==true && hitEachSurf3.GetAt(1)==true )
								aBackUpPoint = outerPnt3;
							else if ( hitEachSurf00.GetAt(0)==true && hitEachSurf00.GetAt(1)==true )
								aBackUpPoint = outerPnt00;
							else if ( hitEachSurf11.GetAt(0)==true && hitEachSurf11.GetAt(1)==true )
								aBackUpPoint = outerPnt11;
							else if ( hitEachSurf22.GetAt(0)==true && hitEachSurf22.GetAt(1)==true )
								aBackUpPoint = outerPnt22;
							else if ( hitEachSurf33.GetAt(0)==true && hitEachSurf33.GetAt(1)==true )
								aBackUpPoint = outerPnt33;
							// 1. use the previous point in oneScanPnts 
							else if ( oneScanPnts.GetSize() > 0 )
								aBackUpPoint = oneScanPnts.GetLast();
							if (aBackUpPoint.IsInitialized())
							{
								IwPoint3d sketchHighPnt = sketchPnt + +20*sketchNormal;
								tempVec = aBackUpPoint - sketchHighPnt;
								double dotValue = tempVec.Dot(sketchNormal);
								outerPnt = sketchHighPnt + dotValue*sketchNormal;// the aBackUpPoint point projected onto the sketchNormal line
							}
							else
							{// Whatever outerPnt is our point
								outerPnt;
							}
						}
					}
				}
				// If we need to apply troGroAddThickness
				if ( troGroAddThickness )
				{
					// determine center shift
					int centerShift;
					if ( i < (troGroAddThicknessStep+troGroTranWidthStepJ) )
					{
						double ratio = (double)i/(double)troGroAddThicknessStep;
						centerShift = (1.0-ratio)*nCenterAntShiftJ + ratio*nCenterNotchShiftJ;
					}
					else
					{
						centerShift = 0;
					}
					// determine jDistRatio
					double jDistRatio = fabs((double)(j-nCenterStepJ-centerShift));
					jDistRatio = 1.0 - jDistRatio/troGroTranWidthStepJ;
					// make troGroAddThickness as trapezoid
					jDistRatio = 1.5*jDistRatio;
					if ( jDistRatio > 1.0 )
					{
						jDistRatio = 1.0;
					}
					if ( jDistRatio > 0.0 ) // if within the transition width
					{
						double effectTroGroAddThickness = 0.0;
						double iDistRatio = 0.0;
						if ( i < troGroAddThicknessStep )
						{
							iDistRatio = 1.0;
						}
						else if ( i < (troGroAddThicknessStep+troGroTranWidthStepJ) )
						{
							iDistRatio = fabs((double)(i-troGroAddThicknessStep));
							iDistRatio = 1.0 - iDistRatio/troGroTranWidthStepJ;
						}
						effectTroGroAddThickness = (*troGroAddThickness)*jDistRatio*iDistRatio;
						outerPnt += effectTroGroAddThickness*sketchNormal;
					}

				}
				// Check distance to referenceSurface, if needed.
				if (referenceSurface)
				{
					IwPoint3d cPnt;
					IwPoint2d param2d;
					double distToRef = DistFromPointToSurface(outerPnt, referenceSurface, cPnt, param2d);
					if ( distToRef < referenceDistance ) // a dip
					{
						IntersectSurfaceByLine(referenceSurface, outerPnt, -sketchNormal, cPnt);
						outerPnt = cPnt + (referenceDistance+0.5-0.05)*sketchNormal;// Why "+0.5-0.05"? see how it pass referenceDistance value in CMakeOuterSurfaceJigs::Reset()
					}
				}
				// Add to oneScanPnts
				oneScanPnts.Add(outerPnt);
if (0)
ShowPoint(pDoc, outerPnt, red);
			}
		}
		// have notch gap, need to bridge an arc curve
		if ( startNotchGapIndex > -1 && endNotchGapIndex > -1 )
		{
			startBridgePnt = oneScanPnts.GetAt(startNotchGapIndex);
			endBridgePnt = oneScanPnts.GetAt(startNotchGapIndex+1);// the next one in oneScanPnts is the end bridge point
			startBridgeVec = oneScanPnts.GetAt(startNotchGapIndex) - oneScanPnts.GetAt(startNotchGapIndex-1);
			startBridgeVec.Unitize();
			endBridgeVec = oneScanPnts.GetAt(startNotchGapIndex+2) - oneScanPnts.GetAt(startNotchGapIndex+1);
			endBridgeVec.Unitize();
			// fair the vectors
			fairPnts.RemoveAll();
			fairVectors.RemoveAll();
			fairPnts.Add(startBridgePnt);
			fairPnts.Add(endBridgePnt);
			fairVectors.Add(startBridgeVec);
			fairVectors.Add(endBridgeVec);
			IwBSplineCurve::FairBlendCurveDerivatives(fairPnts, fairVectors); // This function only handles 2 points/vectors
			IwBSplineCurve* bridgeCurve;
			IwBSplineCurve::CreateInterpolatingCurve(pDoc->GetIwContext(), IW_CP_CHORDLENGTH, 3, 3, fairPnts, fairVectors, NULL, FALSE, bridgeCurve);
if (0)
	ShowCurve(pDoc, bridgeCurve, blue);
			bridgeSpan = endNotchGapIndex - startNotchGapIndex;
			bridgeDom = bridgeCurve->GetNaturalInterval();
			bridgePnts.RemoveAll();
			bridgeCurve->EquallySpacedPoints(bridgeDom.GetMin(), bridgeDom.GetMax(), bridgeSpan, 0.01, &bridgePnts, NULL);
			bridgePnts.RemoveAt(bridgePnts.GetSize()-1);// remove the last one
			bridgePnts.RemoveAt(0);// remove the first one
			oneScanPnts.InsertAt(startNotchGapIndex+1, &bridgePnts);// insert into oneScanPnts
			oneScanNormals.InsertAt(startNotchGapIndex+1, sketchNormal, bridgePnts.GetSize());// insert into oneScanNormals
			// Record indexes where are notch gaps
			mainGapStartIndex = mainContactPnts.GetSize()+29+startNotchGapIndex+1;
			for (unsigned k=0;k<bridgePnts.GetSize();k++)
			{
				notchGapIndexes.Add(mainGapStartIndex+k);
				notchGapNormals.Add(sketchNormal);// Although it's not accurate, better than nothing.
			}
		}
		// Even ML Space for the oneScanPnts
		EvenMLScanSpace(pDoc, oneScanPnts, oneScanNormals);

		// Determine negDropPnts
		if ( bAnteriorOnly )
		{
			outVecUnit = -xAxis;
			outVec = 30.0*refSizeRatio*outVecUnit;
			paramJ = negPnt2d + 0*deltaParamJ;
			sketchSurf->EvaluatePoint(paramJ, sketchPnt);
			sketchSurf->EvaluateNormal(paramJ, FALSE, FALSE, sketchNormal);
			downVecUnit = -sketchNormal;
			// need to rotate if applys
			double F3AngleDeg = pDoc->GetAdvancedControlJigs()->GetAnteriorSurfaceApproachDegree();
			if (F3AngleDeg > 0)
			{			
				if ( i < F3AngleDeg )
					F3AngleDeg = (double)i;
				double rotRadian = F3AngleDeg/180.0*IW_PI;
				IwAxis2Placement rotMatAntDownVec=IwAxis2Placement(IwVector3d(0,0,0), mechAxes.GetXAxis(), downVecUnit);
				rotMatAntDownVec.RotateAboutAxis(rotRadian, mechAxes.GetXAxis());
				downVecUnit = rotMatAntDownVec.GetYAxis();
			}
			// How deep to go down?
			tempVec = oneScanPnts[0] - sketchPnt;
			double aboveSketchDist = tempVec.Dot(sketchNormal);
			// For bAnteriorOnly, the zDownVec magnitude varies such that it only drop below sketch surface 12.5mm.
			zDownVec = (anteriorDropDistance+aboveSketchDist+additionalDropLength)*refSizeRatio*downVecUnit;
			outExtensionDistance = 0.0;
		}
		else
		{
			outVecUnit = oneScanPnts.GetAt(0) - negDropCenter;
			outVecUnit = outVecUnit.ProjectToPlane(mechAxes.GetZAxis());
			outVecUnit.Unitize();
			outVec = 30.0*refSizeRatio*outVecUnit;
			double zDownLength = 30*refSizeRatio + additionalDropLength;
			if (!bAccurate) // outer surface
				zDownLength += 3.5;
			zDownVec = zDownLength*zAxis;
			outExtensionDistance = 0.0;
		}
		IwBSplineCurve *negJumpCurve=NULL;
		if (!bAnteriorOnly && jumpCurves) // Thus, anterior surface no needs to jump
			negJumpCurve=jumpCurves->GetAt(1);
		DroppingCurveWithJump(pDoc, totalSurfaces, oneScanPnts[0], zDownVec, outVec, dropOffset, negJumpCurve, bAccurate, outExtensionDistance, negDropPnts, NULL, referenceSurface, referenceDistance );
		negDropPnts.ReverseArray(0, negDropPnts.GetSize());// reverse the array
		negDropPnts.RemoveLast();
		// Determine posDropPnts
		if ( bAnteriorOnly )
		{
			outVecUnit = xAxis;
			outVec = 30.0*refSizeRatio*outVecUnit;
			paramJ = negPnt2d + (nStepJ-1)*deltaParamJ;
			sketchSurf->EvaluatePoint(paramJ, sketchPnt);
			sketchSurf->EvaluateNormal(paramJ, FALSE, FALSE, sketchNormal);
			downVecUnit = -sketchNormal;
			// need to rotate if applys
			double F3AngleDeg = pDoc->GetAdvancedControlJigs()->GetAnteriorSurfaceApproachDegree();
			if (F3AngleDeg>0)
			{
				if ( i < F3AngleDeg )
					F3AngleDeg = (double)i;
				double rotRadian = F3AngleDeg/180.0*IW_PI;
				IwAxis2Placement rotMatAntDownVec=IwAxis2Placement(IwVector3d(0,0,0), mechAxes.GetXAxis(), downVecUnit);
				rotMatAntDownVec.RotateAboutAxis(rotRadian, mechAxes.GetXAxis());
				downVecUnit = rotMatAntDownVec.GetYAxis();
			}
			// How deep to go down?
			tempVec = oneScanPnts.GetLast() - sketchPnt;
			double aboveSketchDist = tempVec.Dot(sketchNormal);
			// For bAnteriorOnly, the zDownVec magnitude varies such that it only drop below sketch surface 12.5mm.
			zDownVec = (anteriorDropDistance+aboveSketchDist+additionalDropLength)*refSizeRatio*downVecUnit;
			outExtensionDistance = 0.0;
		}
		else
		{
			outVecUnit = oneScanPnts.GetLast() - posDropCenter;
			outVecUnit = outVecUnit.ProjectToPlane(mechAxes.GetZAxis());
			outVecUnit.Unitize();
			outVec = 30.0*refSizeRatio*outVecUnit;
			double zDownLength = 30*refSizeRatio + additionalDropLength;
			if (!bAccurate)// outer surface
				zDownLength += 3.5;
			zDownVec = zDownLength*refSizeRatio*zAxis;
			outExtensionDistance = 0.0;
		}
		IwBSplineCurve *posJumpCurve=NULL;
		if (!bAnteriorOnly && jumpCurves)// Thus, anterior surface no needs to jump
			posJumpCurve=jumpCurves->GetAt(0);
		DroppingCurveWithJump(pDoc, totalSurfaces, oneScanPnts.GetLast(), zDownVec, outVec, dropOffset, posJumpCurve, bAccurate, outExtensionDistance, posDropPnts, NULL, referenceSurface, referenceDistance);
		posDropPnts.RemoveAt(0);
		// Append together
		mainContactPnts.Append(negDropPnts);// 29 elements
		mainContactPnts.Append(oneScanPnts);
		mainContactPnts.Append(posDropPnts);// 29 elements
		// Check medialTabIndex, which scan is the closest to the medial tab point
		if ( medialTabPoint.DistanceBetween(oneScanPnts.GetAt(0)) < minMedialTabDist )
		{
			medialTabIndex = i;
			minMedialTabDist = medialTabPoint.DistanceBetween(oneScanPnts.GetAt(0));
		}
		if ( medialTabPoint.DistanceBetween(oneScanPnts.GetLast()) < minMedialTabDist )
		{
			medialTabIndex = i;
			minMedialTabDist = medialTabPoint.DistanceBetween(oneScanPnts.GetLast());
		}
	}
if (0)
{
for (unsigned kk=0; kk<mainContactPnts.GetSize(); kk++)
	ShowPoint(pDoc, mainContactPnts.GetAt(kk), black);
}
	// Need to smooth the totalContactPnts in the gaps between left/right notch profile
	int currIndex, prevIndex, postIndex;//, leftIndex, rightIndex;
	int lastIndex = mainContactPnts.GetSize()-1;
	IwPoint3d currPnt, avgPnt, intPnt;
	for (int iter=0; iter<4; iter++)
	{
		for (unsigned i=0; i<notchGapIndexes.GetSize(); i++)
		{
			// Only use AP adjacent points to average 
			currIndex = notchGapIndexes.GetAt(i);
			prevIndex = currIndex - nStepJ -29 - 29;
			postIndex = currIndex + nStepJ + 29 + 29;
			if ( prevIndex < 0 || postIndex > lastIndex)
				continue;
			currPnt = mainContactPnts.GetAt(currIndex);
			avgPnt = 0.5*mainContactPnts.GetAt(prevIndex) + 0.5*mainContactPnts.GetAt(postIndex);
			mainContactPnts.SetAt(currIndex, 0.5*currPnt+0.5*avgPnt);
		}
	}

	// Need to make this region (the gap between left/right notch profile) 
	// being the outmost surface of osteophyte and cartilage for accurate (inner/anterior) surfaces.
	if (bAccurate)
	{
		IwVector3d currNormal;
		for (unsigned i=0; i<notchGapIndexes.GetSize(); i++)
		{
			// Only use AP adjacent points to average 
			currIndex = notchGapIndexes.GetAt(i);
			currPnt = mainContactPnts.GetAt(currIndex);
			currNormal = notchGapNormals.GetAt(i);
			bInts = IntersectSurfacesByFireRay(totalSurfacesMain, currPnt+backwardDist*currNormal, -currNormal, maxHitDist, intPnt);
			if ( bInt )
			{
				tempVec = intPnt - currPnt;
				if ( tempVec.Dot(currNormal) > 0 ) // the intPnt is above the currPnt
				{
					// Update the point
					mainContactPnts.SetAt(currIndex, intPnt);
				}
			}
		}
	}

if (0)
{
for (unsigned kk=0; kk<notchGapIndexes.GetSize(); kk++)
	ShowPoint(pDoc, mainContactPnts.GetAt(notchGapIndexes.GetAt(kk)), red);
}

	// Need to smooth the surface when troGroAddThickness exists. Otherwise, too pumpy.
	IwTArray<IwPoint3d> smoothContactPnts;
	if ( !troGroAddThickness )
	{
		smoothContactPnts.Append(mainContactPnts);
	}
	else
	{
		IwPoint3d currPnt, leftPnt, lleftPnt, rightPnt, rrightPnt;
		IwPoint3d topPnt, ttopPnt, botPnt, bbotPnt;
		IwPoint3d avgPntLR, intPntLeft, intPntRight, avgPnt0;
		IwPoint3d avgPntTB, intPntTop, intPntBot, avgPnt1, avgPnt;
		int currIndex;
		IwTArray<int> hugeMoveIndexes;
		double hugeMove = 0.25;
		for (int i=0; i<nStep; i++)
		{
			for (int j=0; j<(nStepJ+29+29); j++)
			{
				currIndex = i*(nStepJ+29+29)+j;
				if ( i<2 || i>(nStep-3) || j<(29+2) || j>(nStepJ+29-3) )
				{
					smoothContactPnts.Add(mainContactPnts.GetAt(currIndex));
				}
				else
				{
					currPnt = mainContactPnts.GetAt(currIndex);
					leftPnt = mainContactPnts.GetAt(currIndex-1);
					lleftPnt = mainContactPnts.GetAt(currIndex-2);
					rightPnt = mainContactPnts.GetAt(currIndex+1);
					rrightPnt = mainContactPnts.GetAt(currIndex+2);
					avgPntLR = 0.5*(leftPnt + rightPnt);
					intPntLeft = 2*leftPnt - lleftPnt;
					intPntRight = 2*rightPnt - rrightPnt;
					avgPnt0 = 0.5*avgPntLR + 0.25*intPntLeft + 0.25*intPntRight;
					topPnt = mainContactPnts.GetAt(currIndex-(nStepJ+29+29));
					ttopPnt = mainContactPnts.GetAt(currIndex-2*(nStepJ+29+29));
					botPnt = mainContactPnts.GetAt(currIndex+(nStepJ+29+29));
					bbotPnt = mainContactPnts.GetAt(currIndex+2*(nStepJ+29+29));
					avgPntTB = 0.5*(topPnt + botPnt);
					intPntTop = 2*topPnt - ttopPnt;
					intPntBot = 2*botPnt - bbotPnt;
					avgPnt1 = 0.5*avgPntTB + 0.25*intPntTop + 0.25*intPntBot;
					avgPnt = 0.5*(avgPnt0+avgPnt1);
					smoothContactPnts.Add(avgPnt);
					if ( currPnt.DistanceBetween(avgPnt) > hugeMove )
						hugeMoveIndexes.Add(currIndex);
				}
			}
		}
		// Smooth 2nd time
		for (int iter=0; iter<3; iter++)
		{
			for (unsigned i=0; i<hugeMoveIndexes.GetSize(); i++)
			{
				currIndex = hugeMoveIndexes.GetAt(i);
				currPnt = smoothContactPnts.GetAt(currIndex);
				leftPnt = smoothContactPnts.GetAt(currIndex-1);
				lleftPnt = smoothContactPnts.GetAt(currIndex-2);
				rightPnt = smoothContactPnts.GetAt(currIndex+1);
				rrightPnt = smoothContactPnts.GetAt(currIndex+2);
				avgPntLR = 0.5*(leftPnt + rightPnt);
				intPntLeft = 2*leftPnt - lleftPnt;
				intPntRight = 2*rightPnt - rrightPnt;
				avgPnt0 = 0.5*avgPntLR + 0.25*intPntLeft + 0.25*intPntRight;
				topPnt = smoothContactPnts.GetAt(currIndex-(nStepJ+29+29));
				ttopPnt = smoothContactPnts.GetAt(currIndex-2*(nStepJ+29+29));
				botPnt = smoothContactPnts.GetAt(currIndex+(nStepJ+29+29));
				bbotPnt = smoothContactPnts.GetAt(currIndex+2*(nStepJ+29+29));
				avgPntTB = 0.5*(topPnt + botPnt);
				intPntTop = 2*topPnt - ttopPnt;
				intPntBot = 2*botPnt - bbotPnt;
				avgPnt1 = 0.5*avgPntTB + 0.25*intPntTop + 0.25*intPntBot;
				avgPnt = 0.5*(avgPnt0+avgPnt1);
				smoothContactPnts.SetAt(currIndex, avgPnt);
			}
		}
	}

	// bAnteriorOnly, smooth AP cliffs for anterior surface
	if (bAnteriorOnly && pDoc->GetAdvancedControlJigs()->GetAnteriorSurfaceApproachDegree()>0 )
	{
		double interCheckDist = 5.0 - ((int)pDoc->GetAdvancedControlJigs()->GetAnteriorSurfaceApproachDegree())%5;
		if (IS_EQ_TOL6(interCheckDist,1.0))
			interCheckDist = 0.5; // special distance to indicate finer AP cliff smoothing
		EvenContactPointsSpace(pDoc, totalSurfaces, bAccurate, smoothContactPnts, nStep, nStepJ+29+29, interCheckDist);
	}
	// However, can NOT apply the same function EvenContactPointsSpace() to inner and outer surface since we do not know interference is against 3 or 6 surfaces.

	// Deform anterior dropping to prevent from overlapping
	if ( !bAnteriorOnly )
	{
		int retreatNo = bAccurate ? 6 : 2;
		if ( medialTabIndex-retreatNo > 0 )// if the medial anterior tab is not too close to the anterior end
		{
			IwVector3d transVec;
			IwTArray<IwPoint3d> negDeformDropPnts0, posDeformDropPnts0;// Both will have 30 elements
			for (int j=0; j<30; j++)
			{
				negDeformDropPnts0.Add(smoothContactPnts.GetAt((medialTabIndex-retreatNo)*(nStepJ+29+29)+j));
				posDeformDropPnts0.Add(smoothContactPnts.GetAt((medialTabIndex-retreatNo)*(nStepJ+29+29)+nStepJ+29-1+j));
			}
			// Smooth a bit
			IwTArray<IwPoint3d> negDeformDropPnts, posDeformDropPnts;
			negDeformDropPnts.Append(negDeformDropPnts0);
			posDeformDropPnts.Append(posDeformDropPnts0);
			for (int j=1; j<29; j++)
			{
				negDeformDropPnts.SetAt(j, 0.5*negDeformDropPnts0.GetAt(j)+0.25*negDeformDropPnts0.GetAt(j-1)+0.25*negDeformDropPnts0.GetAt(j+1));
				posDeformDropPnts.SetAt(j, 0.5*posDeformDropPnts0.GetAt(j)+0.25*posDeformDropPnts0.GetAt(j-1)+0.25*posDeformDropPnts0.GetAt(j+1));
			}
			//for (int i=0; i<(medialTabIndex-retreatNo); i++)
			int k = -1;
			for (int i=(medialTabIndex-retreatNo-1); i>-1; i--) // reverse
			{
				k++;
				// Note negDeformDropPnts & posDeformDropPnts have 30 elements
				// Replace the negative side drop points by negDeformDropPnts
				transVec = smoothContactPnts.GetAt(i*(nStepJ+29+29)+29) - negDeformDropPnts.GetLast();
				for (int negJ=0; negJ<30; negJ++)
					smoothContactPnts.SetAt(i*(nStepJ+29+29)+negJ, transVec+negDeformDropPnts.GetAt(negJ));
				// Replace the positive side drop points by posDeformDropPnts
				transVec = smoothContactPnts.GetAt(i*(nStepJ+29+29)+nStepJ+28) - posDeformDropPnts.GetAt(0);
				for (int posJ=0; posJ<30; posJ++)
					smoothContactPnts.SetAt(i*(nStepJ+29+29)+nStepJ+28+posJ, transVec+posDeformDropPnts.GetAt(posJ));
				if ( k < 3 ) // Smooth a bit more
				{
					for (int j=1; j<29; j++)
					{
						negDeformDropPnts.SetAt(j, 0.5*negDeformDropPnts.GetAt(j)+0.25*negDeformDropPnts.GetAt(j-1)+0.25*negDeformDropPnts.GetAt(j+1));
						posDeformDropPnts.SetAt(j, 0.5*posDeformDropPnts.GetAt(j)+0.25*posDeformDropPnts.GetAt(j-1)+0.25*posDeformDropPnts.GetAt(j+1));
					}
				}
			}
		}
	}

	// Create the main contact surface
	int uDeg=3, vDeg=3;
	if ( bAnteriorOnly )
	{
		uDeg=2;
		vDeg=2;
	}
	IwBSplineSurface* mainContactSurf = NULL;
	IwBSplineSurface::ApproximatePoints(pDoc->GetIwContext(), smoothContactPnts, nStep, nStepJ+29+29, uDeg, vDeg, NULL, mainContactSurf);
	mainContactSurf->ReparametrizeWithArcLength();
	mainContactSurf->Reverse(IW_SP_U);// flip normal such normal is toward the viewers.
	
	IwBrep* mainContactBrep = new (pDoc->GetIwContext()) IwBrep(); 
	IwFace* mainContactFace;
	mainContactBrep->CreateFaceFromSurface(mainContactSurf, mainContactSurf->GetNaturalUVDomain(), mainContactFace);

	contactBrep = mainContactBrep;

}

/////////////////////////////////////////////////////////////////////
// Actually only AP space needs to be even.
void CMakeInnerSurfaceJigs::EvenContactPointsSpace
(
	CTotalDoc*& pDoc,						// I:
	IwTArray<IwSurface*>& totalSurfaces,	// I: 
	bool bAccurate,							// I:
	IwTArray<IwPoint3d>& contactPnts,		// I/O: total points = APSize X MLSize
	int APSize,								// I: 
	int MLSize,								// I:
	double interferenceCheckDistance		// I: could be 0, 0.5, 2, 3, 4mm.
)
{
	int dropSize = 29;
	int dropSizeExtended = dropSize + 10;
	int maxIndex = APSize*MLSize;

	double APRatio = 3.0;
	// If interferenceCheckDistance = 0.5, we should do finer AP cliff smoothing
	if ( IS_EQ_TOL6(interferenceCheckDistance, 0.5) )
		APRatio = 1.5;
	else if ( IS_EQ_TOL6(interferenceCheckDistance, 2.0) )
		APRatio = 2.0;
	else if ( IS_EQ_TOL6(interferenceCheckDistance, 3.0) )
		APRatio = 2.5;

	// Even AP space by AP scans one by one
	int index;
	IwPoint3d pnt;
	IwTArray<int> movedIndexes;
	for (int j=0; j<MLSize; j++)
	{
		if (j>dropSizeExtended && j<(MLSize-dropSizeExtended))// skip in the "narrowed" main area
			continue;

		IwTArray<IwPoint3d> pointsInAScan;
		IwTArray<bool> pointsMoved;
		IwTArray<int> indexes;
		bool anyMoved;
		for (int i=0; i<APSize; i++) 
		{
			index = i*MLSize + j;
			pnt = contactPnts.GetAt(index);
			indexes.Add(index);
			pointsInAScan.Add(pnt);
		}
		// Even space
		anyMoved = EvenAPScanSpace(pDoc, totalSurfaces, bAccurate, APRatio, pointsInAScan, pointsMoved);
		// Set back to contactPnts
		if ( anyMoved )
		{
			for (unsigned i=0; i<pointsInAScan.GetSize(); i++) 
			{
				if ( pointsMoved.GetAt(i) )
				{
					index = indexes.GetAt(i);
					pnt = pointsInAScan.GetAt(i);
					contactPnts.SetAt(index, pnt);
					movedIndexes.AddUnique(index);
					// Adjacent points in ML direction
					if ( (index+1) < maxIndex )
						movedIndexes.AddUnique(index+1);
					if ( (index+2) < maxIndex )
						movedIndexes.AddUnique(index+2);
					if ( (index-1) > -1 )
						movedIndexes.AddUnique(index-1);
					if ( (index-2) > -1 )
						movedIndexes.AddUnique(index-2);
				}
			}
		}
	}

	// if interferenceCheckDistance = 0, do not need to do the AP/ML smooth, and interference check.
	if ( IS_EQ_TOL6(interferenceCheckDistance, 0.0) )
		return;

	// Even AP and ML space all moved points 
	int preIndexAP, postIndexAP, preIndexML, postIndexML;
	IwPoint3d prePntAP, postPntAP, prePntML, postPntML, pntAvg;
	int iteration = 10;
	double ppDist, stableDist = 0.125, moveDist = 1.0, bigDist = 20.0;
	bool bStable = true;
	for (int iter = 0; iter<iteration; iter++)
	{
		bStable = true;
		for (unsigned i=0; i<movedIndexes.GetSize(); i++)
		{
			index = movedIndexes.GetAt(i);
			preIndexAP = index - MLSize;
			postIndexAP = index + MLSize;
			preIndexML = index - 1;
			postIndexML = index + 1;
			pnt = contactPnts.GetAt(index);
			prePntAP = contactPnts.GetAt(preIndexAP);
			postPntAP = contactPnts.GetAt(postIndexAP);
			prePntML = contactPnts.GetAt(preIndexML);
			postPntML = contactPnts.GetAt(postIndexML);
			if ( pnt.DistanceBetween(prePntAP) > bigDist || pnt.DistanceBetween(postPntAP) > bigDist) // pnt is the end point and prePntAP is in the other side
			{
				pntAvg = 0.5*(prePntML+postPntML);// average
			}
			else if ( pnt.DistanceBetween(prePntML) > bigDist || pnt.DistanceBetween(postPntML) > bigDist ) //prePntML is in the other side
			{
				pntAvg = 0.5*(prePntAP+postPntAP);// average
			}
			else
			{
				pntAvg = 0.25*(prePntAP+postPntAP+prePntML+postPntML);// average
			}
			pnt = 0.55*pnt + 0.45*pntAvg;
			ppDist = pntAvg.DistanceBetween(pnt);
			contactPnts.SetAt(index, pnt); // set back
			if ( ppDist > stableDist )
				bStable = false;
		}

		// Check interference against totalSurfaces
		IwPoint3d cPnt, vec, normal;
		IwPoint3d vec1, vec2, normal1, normal2, normalAvg;
		IwPoint2d paramUV;
		double dist, dotValue;
		int surfIndex;
		bool hit;
		for (unsigned i=0; i<movedIndexes.GetSize(); i++)
		{
			index = movedIndexes.GetAt(i);
			pnt = contactPnts.GetAt(index);
			dist = DistFromPointToSurfaces(pnt, totalSurfaces, cPnt, paramUV, surfIndex);
			if (dist > 0 ) // got a solution
			{
				totalSurfaces.GetAt(surfIndex)->EvaluateNormal(paramUV, FALSE, FALSE, normal);
				hit = IntersectSurfacesByFireRay(totalSurfaces, pnt+interferenceCheckDistance*normal, -normal, 2*interferenceCheckDistance, cPnt, &surfIndex);//to get the outer-most point of these 6 surface
				if (hit) 
				{
					vec = pnt - cPnt;
					dotValue = normal.Dot(vec);
					if ( dotValue < 0.1 ) // (Almost) interference
					{
						pnt = cPnt + 0.11*normal;  
						contactPnts.SetAt(index, pnt);
					}
				}
			}
		}

		if ( bStable )
			break;
	}


}

bool CMakeInnerSurfaceJigs::EvenAPScanSpace
(
	CTotalDoc*& pDoc,						// I:
	IwTArray<IwSurface*>& totalSurfaces,	// I:
	bool bAccurate,							// I:
	double ratio,							// I:
	IwTArray<IwPoint3d>& APScanPnts,		// I/O: contact points along a AP scan 
	IwTArray<bool>& pointsMoved			// O: The points are moved or not
)
{
	pointsMoved.RemoveAll();

	bool anyMoved = false;

	int untouchSpace = 1;
	IwPoint3d pnt, prePnt, postPnt;
	double preDistance, postDistance, minDistance, maxDistance;
	// Search for big space region
	double lengthRatio, criticalRatio = ratio;
	double criticalDistance;
	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;
	if (bAccurate)
	{
		criticalDistance = 2.0*refSizeRatio;
	}
	else
	{
		criticalDistance = 3.75*refSizeRatio;
	}
	bool lookingFor2ndUnevenSpace = false;
	int minIndex1 = -1;
	int maxIndex1 = -1;
	int minIndex2 = -1;
	int maxIndex2 = -1;
	double dotValue;
	IwVector3d preVec, postVec;
	// initialize pointsMoved.Add(false);
	int APScanSize = (int)APScanPnts.GetSize();
	for (int i=0; i<untouchSpace; i++)
		pointsMoved.Add(false);
	for (int i=untouchSpace; i<APScanSize-untouchSpace; i++) // from 6 ~ size-6, to avoid the first/last 6 points
	{
		pnt = APScanPnts.GetAt(i);
		prePnt = APScanPnts.GetAt(i-1);
		postPnt = APScanPnts.GetAt(i+1);
		preVec = prePnt - pnt;
		postVec = postPnt - pnt;
		preDistance = pnt.DistanceBetween(prePnt);
		postDistance = pnt.DistanceBetween(postPnt);
		if ( preDistance > postDistance )
		{
			minDistance = postDistance;
			maxDistance = preDistance;
		}
		else
		{
			minDistance = preDistance;
			maxDistance = postDistance;
		}
		lengthRatio = maxDistance / minDistance;
		dotValue = preVec.Dot(postVec);
		if ( maxDistance > criticalDistance || (lengthRatio > criticalRatio && maxDistance > 0.5*criticalDistance) || dotValue>0 ) // uneven space or zig-zag 
		{
			if ( !lookingFor2ndUnevenSpace ) // i.e., looking for the 1st uneven space
			{
				if (minIndex1 == -1) // no min index yet
					minIndex1 = i;
				maxIndex1 = i;
			}
			else // i.e., looking for the 2nd uneven space
			{
				if (minIndex2 == -1) // no min index yet
					minIndex2 = i;
				maxIndex2 = i;
			}
ShowPoint(pDoc, pnt, red);
		}
		else
		{
			if (!lookingFor2ndUnevenSpace && minIndex1 != -1 && maxIndex1 != -1)
			{
				lookingFor2ndUnevenSpace = true;
			}
		}
		// initialize
		pointsMoved.Add(false);
	}
	// initialize pointsMoved.Add(false);
	for (int i=0; i<untouchSpace; i++)
		pointsMoved.Add(false);

	// 
	if ( minIndex1 == -1 )// all good space
	{
		return false;
	}

	// Even the 1st uneven space
	int preIndex1 = minIndex1 - 5;
	if (preIndex1<1)
		preIndex1 = 1;
	int postIndex1 = maxIndex1 + 6;
	if (postIndex1>(APScanSize-1))
		postIndex1 = APScanSize-1;

	IwTArray<IwPoint3d> pointsToEven1;
	IwTArray<int> indexes1;
	for (int i=preIndex1; i<postIndex1; i++)
	{
		pnt = APScanPnts.GetAt(i);
		pointsToEven1.Add(pnt);
		indexes1.Add(i);
static int dispPointsToEven = 0;
if (dispPointsToEven)
ShowPoint(pDoc, pnt, magenta);
	}
	// Even point space
	EvenPointsSpace(pointsToEven1);
	// Set back
	int index;
	for (unsigned i=0; i<indexes1.GetSize(); i++)
	{
		pnt = pointsToEven1.GetAt(i);
		index = indexes1.GetAt(i);
		APScanPnts.SetAt(index, pnt);
		pointsMoved.SetAt(index, true);
	}

	// Even the 2nd uneven space
	if ( minIndex2 == -1 ) // no 2nd uneven space
	{
		return true;
	}

	int preIndex2 = minIndex2 - 5;
	if (preIndex2<1)
		preIndex2 = 1;
	int postIndex2 = maxIndex2 + 6;
	if (postIndex2>(APScanSize-1))
		postIndex2 = APScanSize-1;

	IwTArray<IwPoint3d> pointsToEven2;
	IwTArray<int> indexes2;
	for (int i=preIndex2; i<postIndex2; i++)
	{
		pnt = APScanPnts.GetAt(i);
		pointsToEven2.Add(pnt);
		indexes2.Add(i);
	}
	// Even point space
	EvenPointsSpace(pointsToEven2);
	// Set back
	for (unsigned i=0; i<indexes2.GetSize(); i++)
	{
		pnt = pointsToEven2.GetAt(i);
		index = indexes2.GetAt(i);
		APScanPnts.SetAt(index, pnt);
		pointsMoved.SetAt(index, true);
	}

	return true;
}

bool CMakeInnerSurfaceJigs::EvenMLScanSpace
(
	CTotalDoc*& pDoc,					// I:
	IwTArray<IwPoint3d>& MLScanPnts,	// I/O:
	IwTArray<IwPoint3d>& MLScanNormals	// I:
)
{
	bool moved = false;
	double ratio, maxRatio = 2.0;
	double minRatio = 1.0/maxRatio;
	double preDist, postDist;
	IwVector3d vec, normal;
	IwPoint3d prePnt, curPnt, postPnt, avgPnt;
	int iters = 3;
	for (int iter=0; iter< iters; iter++)
	{
		for (unsigned i=1; i<MLScanPnts.GetSize()-1; i++) // skip first and last
		{
			normal = MLScanNormals.GetAt(i);
			curPnt = MLScanPnts.GetAt(i);
			prePnt = MLScanPnts.GetAt(i-1);
			postPnt = MLScanPnts.GetAt(i+1);
			preDist = curPnt.DistanceBetween(prePnt);
			postDist = curPnt.DistanceBetween(postPnt);
			ratio = preDist/postDist;
			if ( ratio > maxRatio || ratio < minRatio )
			{
				avgPnt = 0.5*prePnt + 0.5*postPnt;
				vec = avgPnt - curPnt;
				if ( vec.Dot(normal) > 0 ) // the avgPnt is moving outward to create a void space
				{
					MLScanPnts.SetAt(i, avgPnt);
					moved = true;
if (iter==0)
ShowPoint(pDoc, curPnt, magenta);
				}
			}
		}
		if (moved)
			maxRatio = 1.5;
		else // not moved
			break;
	}

	return moved;
}

///////////////////////////////////////////////////////////////////////////
// Please note that the AnteriorDroppingSurface is not the anterior surface
// (for F3 contact surface). The AnteriorDroppingSurface is a surface
// which prevents the F1 main contact surface from undercutting the 
// concave regions near anterior. Why? The F1 main contact surface 
// is projected from the sketch surface normal. If there is any surface
// that is behind the silhouette, this area will become the undercut area
// of the F1 contact surface. 
///////////////////////////////////////////////////////////////////////////
void CMakeInnerSurfaceJigs::DetermineAnteriorDroppingSurface
(
	CTotalDoc*& pDoc,						// I:
	IwTArray<IwSurface*>& totalSurfaces,	// I: the [0~2] are osteophyte surface, the [3~5] are cartilage surfaces. 
	bool bTimeTest,							// I: need time test or not, in case if too slow
	IwBrep*& antDropBrep					// O:
)
{
	antDropBrep = NULL;

	// Get outline profile feature points
	IwTArray<IwPoint3d> cutFeaturePoints, antOuterPoints;
	IFemurImplant::FemoralCuts_GetCutsFeaturePoints(cutFeaturePoints, antOuterPoints);

	// Get femoral axes
	IwAxis2Placement mechAxes = IFemurImplant::FemoralAxes_GetMechanicalAxes();
	// matrix for rotating along x-axis
	IwAxis2Placement rotMat, rotatedAxes;
	double rotRadian = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceApproachDegree()/180.0*IW_PI;
	rotMat.RotateAboutAxis(rotRadian, IwVector3d(1,0,0));
	rotMat.TransformAxis2Placement(mechAxes, rotatedAxes);
	IwVector3d zAxis = rotatedAxes.GetZAxis();
	IwVector3d yAxis = rotatedAxes.GetYAxis();
	IwVector3d xAxis = rotatedAxes.GetXAxis();
	// get ref size
	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;

	// Get outline profile jigs projected curve points
	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	IwTArray<IwPoint3d> outlineProfileProjectedPoints;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(outlineProfileProjectedPoints);

	// Get cartilage 3 surfaces
	CCartilageSurface* pCartiSurf = pDoc->GetCartilageSurface();
	if ( pCartiSurf == NULL )
		return;
	IwBrep* cartiSurfBrep = pCartiSurf->GetIwBrep();
	if ( cartiSurfBrep == NULL )
		return;

	// Get cartilage edge
	CCartilageSurface* pCartilageSurface = pDoc->GetCartilageSurface();

	// Determine the closest point on cut feature points 18th and 19th both are on the anterior / anterior chamfer cut edge
	IwPoint3d pnt18 = cutFeaturePoints.GetAt(18);
	IwPoint3d pnt19 = cutFeaturePoints.GetAt(19);
	// determine the closest point of outlineProfileProjectedPoints on (pnt18, z-axis) plane
	IwPoint3d pnt18Proj, pnt19Proj;
	bool bInt18 = IntersectIwTArrayPointsByPlane(outlineProfileProjectedPoints, pnt18, zAxis, 10.0, pnt18Proj);
	bool bInt19 = IntersectIwTArrayPointsByPlane(outlineProfileProjectedPoints, pnt19, zAxis, 10.0, pnt19Proj);
	// Adjust pnt18 and pnt19
	IwVector3d tempVec;
	IwPoint3d outPnt18, outPnt19;
	if ( bInt18 && bInt19 )
	{
		tempVec = pnt18Proj - pnt18;
		outPnt18 = pnt18 + (tempVec.Dot(xAxis) + 2.0)*xAxis;
		tempVec = pnt19Proj - pnt19;
		outPnt19 = pnt19 + (tempVec.Dot(xAxis)- 2.0)*xAxis;
	}
	else // No intersection -> means the anterior outline profile is too distal (no exceed ant/antChamfer cut edge)
	{
		outPnt18 = pnt18;
		outPnt19 = pnt19;
	}

if (0)
{
	ShowPoint(pDoc, outPnt18, red);
	ShowPoint(pDoc, outPnt19, blue);
//	ShowPoint(pDoc, antTip, black);
}
	double antDropDegree = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceAnteriorDropDegree();
	// Rotate anterior drop degree 
	double rotRadian2 = -antDropDegree/(180.0/IW_PI);
	IwAxis2Placement rotMat2, rotatedAxes2;
	// matrix for rotating along x-axis
	rotMat2.RotateAboutAxis(rotRadian2, IwVector3d(1,0,0));
	// rotate rotateAxes based on originalFemAxes
	rotMat2.TransformAxis2Placement(mechAxes, rotatedAxes2);
	IwVector3d zAxisRot = rotatedAxes2.GetZAxis();

	// Prepare for dropping
	double downLength = 25*refSizeRatio + pDoc->GetAdvancedControlJigs()->GetInnerSurfaceAdditionalDropLength();
	IwVector3d downVec = downLength*zAxisRot;
	IwVector3d outVec = 25*refSizeRatio*yAxis;
	double scanLength = outPnt18.DistanceBetween(outPnt19);
	int nStep = (int)(scanLength/0.8);
	int nDropPntSize;
	tempVec = outPnt19 - outPnt18;
	IwVector3d deltaVec = tempVec/nStep;
	IwPoint3d sPnt, intPnt;
	bool bInt;
	IwTArray<IwPoint3d> MNPnts, dropPnts;
	// time test
	QDateTime startTime = QDateTime::currentDateTime();
	int elapseSeconds=0;
	bool takeTooLong = false;
	for (int i=0; i<(nStep+1); i++)
	{
		sPnt = outPnt18 + i*deltaVec + outVec;// move out (y-axis) a little bit
		// project along y-axis to hit the osteo/carti surfaces
		bInt = IntersectSurfacesByLine(totalSurfaces, sPnt, yAxis, intPnt);
		if (!bInt)
			continue;
		DroppingCurve(pDoc, totalSurfaces, intPnt, downVec, outVec, 0.0, dropPnts);
		MNPnts.Append(dropPnts);
		dropPnts.RemoveAll();
		if (bTimeTest)
		{
			elapseSeconds = startTime.secsTo(QDateTime::currentDateTime());
			if ( elapseSeconds > 60 ) // > 1 minute
			{
				takeTooLong = true;
				break;
			}
		}
	}
	if (takeTooLong)
		return;

	nDropPntSize = MNPnts.GetSize()/(nStep+1);
	IwBSplineSurface* antDropSurf = NULL;
	IwBSplineSurface::ApproximatePoints(pDoc->GetIwContext(), MNPnts, nStep+1, nDropPntSize, 3, 3, NULL, antDropSurf);

	antDropBrep = new (pDoc->GetIwContext()) IwBrep(); 
	IwFace* antDropFace;
	antDropBrep->CreateFaceFromSurface(antDropSurf, antDropSurf->GetNaturalUVDomain(), antDropFace);

}

///////////////////////////////////////////////////////////////////
// This function check whether the inner surface along the dropping 
// edges is smooth. 
void CMakeInnerSurfaceJigs::ValidateContactSurface
(
	CTotalDoc*& pDoc,		// I:
	IwBrep*& contactBrep,	// I:
	bool& flipNormal,		// O:
	IwTArray<IwPoint3d>& flipPoints	// O:
)
{
	flipNormal=false;
	flipPoints.RemoveAll();

	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;

	if ( contactBrep == NULL )
		return;

	IwAxis2Placement mechAxes = IFemurImplant::FemoralAxes_GetMechanicalAxes();
	// matrix for rotating along x-axis
	IwAxis2Placement rotMat, rotatedAxes;
	double rotRadian = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceApproachDegree()/180.0*IW_PI;
	rotMat.RotateAboutAxis(rotRadian, IwVector3d(1,0,0));
	rotMat.TransformAxis2Placement(mechAxes, rotatedAxes);
	IwVector3d xAxis = rotatedAxes.GetXAxis();
	IwVector3d yAxis = rotatedAxes.GetYAxis();
	IwVector3d zAxis = rotatedAxes.GetZAxis();

	IwPoint3d posDroppingCenter, negDroppingCenter;
	pOutlineProfileJigs->GetDroppingCenters(posDroppingCenter, negDroppingCenter);

	IwTArray<IwFace*> faces;
	contactBrep->GetFaces(faces);
	if ( faces.GetSize() != 1 )
		return;

	IwFace* face = faces.GetAt(0);
	IwBSplineSurface* cSurf = (IwBSplineSurface*)face->GetSurface();
	IwExtent2d dom = cSurf->GetNaturalUVDomain();
	double deltaU = dom.GetUInterval().GetLength()/100.0;
	double minU = dom.GetUMin();
	double minV = dom.GetVMin();
	double maxV = dom.GetVMax();

	IwPoint3d pnt;
	IwVector3d vec, normal;
	IwPoint2d uv;
	for (int i=1; i<100; i++)// Not include the first and last ones.
	{
		// min V
		uv=IwPoint2d(minU+i*deltaU, minV);
		cSurf->EvaluatePoint(uv, pnt);
		cSurf->EvaluateNormal(uv, FALSE, FALSE, normal);
		normal.Unitize();
		vec = pnt - negDroppingCenter;
		vec = vec.ProjectToPlane(zAxis);
		vec.Unitize();
		if ( vec.Dot(normal) < -0.5 ) // > 120 degrees, flip
			flipPoints.Add(pnt);
		// max V
		uv=IwPoint2d(minU+i*deltaU, maxV);
		cSurf->EvaluatePoint(uv, pnt);
		cSurf->EvaluateNormal(uv, FALSE, FALSE, normal);
		normal.Unitize();
		vec = pnt - posDroppingCenter;
		vec = vec.ProjectToPlane(zAxis);
		vec.Unitize();
		if ( vec.Dot(normal) < -0.5 ) // > 120 degrees, flip
			flipPoints.Add(pnt);
	}

	if ( flipPoints.GetSize() > 0 )
		flipNormal = true;

	return;
}


void CMakeInnerSurfaceJigs::DisplayErrorMessages()
{
	if ( m_manActType != MAN_ACT_TYPE_EDIT )
	{
		m_errorCodes.RemoveAll();
		return;
	}

	int		button;
	int		errorMsgNum = (int)m_errorCodes.GetSize();
	if (errorMsgNum == 0) return;

	QString totalErrorMessages(tr(""));
	for (int i=0; i<errorMsgNum; i++)
	{
		CMakeInnerSurfaceJigsErrorCodes errorCode = m_errorCodes.GetAt(i);
		QString msg;
		GetErrorCodeMessage(errorCode, msg);
		totalErrorMessages += msg + "\n\n";
	}

	button = QMessageBox::critical( NULL, 
									tr("Warning Messages!"), 
									totalErrorMessages, 
									QMessageBox::Ok, 0 );	

	m_errorCodes.RemoveAll();	
}

void CMakeInnerSurfaceJigs::GetErrorCodeMessage(CMakeInnerSurfaceJigsErrorCodes& errorCode, QString& errorMsg)
{
	int errorNum = sizeof( MakeInnerSurfaceJigsErrorCodes ) / sizeof( CMakeInnerSurfaceJigsErrorCodesEntry );

	errorMsg = tr("");
	for( int i = 0; i < errorNum; i++ )
	{
		if( MakeInnerSurfaceJigsErrorCodes[i].ErrorCodes == errorCode )
		{
			errorMsg = MakeInnerSurfaceJigsErrorCodes[i].ErrorMessages;
			break;
		}
	}

}

bool CMakeInnerSurfaceJigs::OnReviewNext()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_INNER_SURFACE_JIGS, true);
	return true;
}

bool CMakeInnerSurfaceJigs::OnReviewBack()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_INNER_SURFACE_JIGS, false);
	return true;
}

bool CMakeInnerSurfaceJigs::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;


	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_INNER_SURFACE_JIGS))
    	OnAccept();
	return true;
}

void CMakeInnerSurfaceJigs::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 5;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 ) 
	{
		// Display inner surface wireframe
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_WIREFRAME );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display osteophyte
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display outline
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Hide side surface
		pEntity = m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Hide femoral axis, if displayed from previous review step
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		vp->LateralView(false);
	}
	else if ( viewIndex == 1 ) 
	{
		vp->MedialView(false);
	}
	else if ( viewIndex == 2 ) 
	{
		// Hide osteophyte
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Hide outline
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		vp->BottomView(false);
	}
	else if ( viewIndex == 3 ) 
	{
		// Hide inner surface
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Display outer surface wireframe
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_WIREFRAME );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		vp->BottomView(false);
	}
	else if ( viewIndex == 4 ) 
	{
		// Hide outer surface
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Display side surface
		pEntity = m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		vp->TopView(false);
	}
	m_predefinedViewIndex++;
}
