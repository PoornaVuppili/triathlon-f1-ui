#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

class TEST_EXPORT_TW CMakeAnteriorSurfaceJigs : public CManager
{
    Q_OBJECT

public:
					CMakeAnteriorSurfaceJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CMakeAnteriorSurfaceJigs();

	CTotalView*		GetView() {return ((CTotalView*)m_pView);};
	virtual void	Display(Viewport* vp=NULL);
    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );

	static void		CreateAnteriorSurfaceJigsObject(CTotalDoc* pDoc);

protected:
	void			DetermineLateralCutPeakPointAndF3Profile();

private:
	void			Reset(bool activateUI=true);

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnCancel();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewBack();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();
	void			OnReviewValidateJigs();

protected:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actMakeAnteriorSurfaceJigs;
	QAction*					m_actReset;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewRework;
	QAction*					m_actReviewValidateJigs;

	int							m_predefinedViewIndex;

	IwPoint3d					m_currLateralPeakPoint;
	IwTArray<IwPoint3d>			m_currF3ProfilePoints;
};
