#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\DataDoc.h"
#pragma warning( disable : 4996 4805 )
#include <QList.h>
#include <IwContext.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Basics.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\OpenGLView.h"

#include <qmessagebox.h>
#include <QFileDialog>
#include <QApplication>

class CTotalView;
class CTotalEntPanel;
class CTotalEntPanelJigs;
class CFemoralAxes;
class CFemoralPart;
class CFemoralCuts;
class COutlineProfile;
class CAdvancedControl;
class CCartilageSurface;
class COutlineProfileJigs;
class CInnerSurfaceJigs;
class COuterSurfaceJigs;
class CSideSurfaceJigs;
class CSolidPositionJigs;
class CStylusJigs;
class CAnteriorSurfaceJigs;
class CAdvancedControlJigs;

class TEST_EXPORT_TW CTotalDoc : public CDataDoc
	{
public:

	CTotalDoc(QObject *parent=0);
	~CTotalDoc();
	static CTotalDoc* GetTotalDoc() {return (CTotalDoc*)m_pTotalDoc;};
	static void SetTotalDoc(CTotalDoc* pDoc) {m_pTotalDoc=pDoc;};
	CTotalView*	GetView() {return ((CTotalView*)m_pView);};
	CTotalEntPanel*	GetEntPanel() {return ((CTotalEntPanel*)m_pEntPanel);};
	void			SetEntPanelJigs( CTotalEntPanelJigs* pEntPanelJigs ) {m_pEntPanelJigs=pEntPanelJigs;};
	CTotalEntPanelJigs*	GetEntPanelJigs() {return ((CTotalEntPanelJigs*)m_pEntPanelJigs);};

	virtual void		Clear();
	virtual void		AddEntity( CEntity* pEntity, bool bAddToEntPanel = true );
	virtual bool		ImportPart( const QString& sFileName, CEntRole eEntRole = ENT_ROLE_NONE );
    virtual bool		ImportPart(IwBrep* brep, CEntRole eEntRole = ENT_ROLE_NONE);
    virtual bool		ImportPart(IwPoint3d pt, CEntRole eEntRole = ENT_ROLE_NONE);
	bool				ImportFemoralPart( const QString& sFileName, CEntRole eEntRole = ENT_ROLE_NONE);
	void				DeleteEntitiesBelongToFemoralImplant();
	void				DeleteEntitiesBelongToJigs();
	virtual void		SetEntPanelStatusMarker( CEntRole eEntRole, bool status );
	virtual void		UpdateEntPanelStatusMarker( CEntRole eEntRole, bool bUpToDate=true, bool implicitlyUpdate=true );
	virtual void		SetEntPanelValidateStatus( CEntRole eEntRole, CEntValidateStatus status, QString message );
	void				UpdateEntPanelStatusMarkerJigs( CEntRole eEntRole, bool bUpToDate=true, bool implicitlyUpdate=true );
	virtual void		SetEntPanelFontColor(CEntRole entRole, CColor color);
	virtual void		SetEntPanelToolTip(CEntRole entRole, QString message);
	virtual void		SetModified( bool bModified );
	virtual bool		Save( const QString& sDocFileName, bool incrementalSave=false );
	virtual bool		Load( const QString& sDocFileName );
	virtual bool		LoadWithOptions( const QString& sDocFileName, QString options=QString("") );
	virtual void		TogglePartDisplayMode( int id, CDisplayMode displayMode );
	bool				IsAutoCreateJOCSteps();
	bool				IsAutoCreateMajorSteps();
	bool				IsAutoCreateAllSteps();
	virtual void		InitializeValidationReport() override;
	virtual void		AppendValidationReportEntry(QString const& entry, bool goodResult=true) override;
	virtual QString		GetValidationReport(bool& allGoodResults) override;
	virtual void		InitializeValidationReportJigs();
	virtual void		AppendValidationReportJigsEntry(QString const& entry, bool goodResult=true);
	virtual QString		GetValidationReportJigs(bool& allGoodResults);

	void				GetImplantAPSizeRange(double& APHigh, double& APLow);
	void				GetImplantMLSizeRange(double& MLHigh, double& MLLow);
	void				GetImplantMedialDistalRadiusRange(double& RadiusHigh, double& RadiusLow);
	void				GetImplantLateralDistalRadiusRange(double& RadiusHigh, double& RadiusLow);
	void				GetImplantMedialPosteriorRadiusRange(double& RadiusHigh, double& RadiusLow);
	void				GetImplantLateralPosteriorRadiusRange(double& RadiusHigh, double& RadiusLow);

    bool                GetImplantActualOverhang(double& overMax);

	virtual bool		IsFemoralImplantDesignComplete();
	virtual bool		IsFemoralImplantDesignWithValidAPMLSizes();
	virtual bool		IsFemoralJigsDesignComplete();

	bool				GetFemurImplantPosteriorTips(IwPoint3d& medialPnt, IwPoint3d& lateralPnt);
	virtual void		ToggleEntPanelItemByID(int id, bool On) override; // over write in DataDoc
	virtual void		RemoveEntityFromEntPanel( CEntRole eEntRole ) override;// over write in DataDoc
	void				HideAllFemurImplantEntities();
	void				HideAllFemurJigsEntities();
	void				DeleteEntitiesAfterFemoralAxes();

	// Special purpose function
	virtual void		SpecialCheckForCartilageThickness();

	// Special purpose to support KTotalDocPS
	virtual void		Mates_ToggleOn(int SetNo) {};
	virtual bool		GetMotionPatternsExtendToolbar() {return false;}
	virtual void		DeleteEntitiesBelongToPSFeatures() {};

	CFemoralPart*		GetFemur();
	CPart*				GetHip();
	CFemoralAxes*		GetFemoralAxes();
	CFemoralCuts*		GetFemoralCuts();
	COutlineProfile*	GetOutlineProfile();
	CAdvancedControl*	GetAdvancedControl();

	//// Jigs ////
	CFemoralPart*		GetOsteophyteSurface();
	CCartilageSurface*	GetCartilageSurface();
	COutlineProfileJigs*	GetOutlineProfileJigs();
	CInnerSurfaceJigs*		GetInnerSurfaceJigs();
	COuterSurfaceJigs*		GetOuterSurfaceJigs();
	CSideSurfaceJigs*		GetSideSurfaceJigs();
	CSolidPositionJigs*		GetSolidPositionJigs();
	CStylusJigs*			GetStylusJigs();
	CAnteriorSurfaceJigs*	GetAnteriorSurfaceJigs();
	CAdvancedControlJigs*	GetAdvancedControlJigs();
	CAdvancedControlJigs*	GetAdvancedControlJigsPointer();

public:
	static CTotalDoc *m_pTotalDoc;

protected:
	CTotalEntPanelJigs*	m_pEntPanelJigs;

private:
	ValidationReport			m_validationReportJigs;
	bool						m_validationJigsAllGoodResults;
	
	};



