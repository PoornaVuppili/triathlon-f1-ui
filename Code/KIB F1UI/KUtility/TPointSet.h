///////////////////////////////////////////////////////////////////////
//	File:    PointSet.h
//	Purpose: A random set of vertices with (x,y) coordinates. The vertices
//    can be moved, inserted or deleted. The implmentation stores the 
//    coordinates in one stl::vector so that they can be directly accessed
//    by, for example, OpenGL. Refer to TContour.h for an orderly set of points.
///////////////////////////////////////////////////////////////////////

#ifndef POINTSET_H
#define POINTSET_H

#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
using namespace std;

// PointSet class
template<typename T> class PointSet
{
protected:
    vector<T> vertex;  // x1,y1,x2,y2 ...

public:
    // Constructor
    PointSet() {}

    // Destructor
    ~PointSet() {}

    // Get the number of vertices
    int GetLength() const { return (int) (vertex.size()>>1); } 

    // Get the capacity of the buffer
    int GetCapacity() const { return (int) vertex.capacity() >> 1; }

    // Set the capacity of the buffer
    void SetCapacity(int s) { vertex.reserve(s<<1); }

    // Get the x coordinate of the specified coordinate index
    T  X(int p) const { return vertex[p<<1]; }

    // Get the y coordinate of the specified coordinate index
    T  Y(int p) const { return vertex[(p<<1)+1]; }

    // Set the x,y coordinate of the specified coordinate index
    void  SetXY(int p, T x, T y)  { vertex[p<<1] = x; vertex[(p<<1)+1] = y; }

    // Comparison
    bool operator== (const PointSet<T>& ps) const
    {
        int n = GetLength();
        if( n != ps.GetLength() )
            return false;
        for(int i=0; i<n; i++)
            if( X(i) != ps.X(i) || Y(i) != ps.Y(i) )
                return false;
        return true;
    }

    bool operator!= (const PointSet<T>& ps) const
    {
        int n = GetLength();
        if( n != ps.GetLength() )
            return true;
        for(int i=0; i<n; i++)
            if( X(i) != ps.X(i) || Y(i) != ps.Y(i) )
                return true;
        return false;
    }

    // Get the pointer to the vertice coordinates: [x1,y1,x2,y2,x3,y3 ...
    T* GetVertices() { return vertex.size()==0 ? NULL : & vertex[0]; }

    // Append a new vertex at the end of the current one
    void Append(T x,T y)
    {
        vertex.push_back( x );
        vertex.push_back( y );
    }

    // Append a point set at the end of current one
    void Append(const PointSet<T>& ps)
    {
        SetCapacity( GetCapacity() + ps.GetCapacity() );
        for(int i=0; i<ps.GetLength(); i++)
            Append(ps.X(i), ps.Y(i));
    }

    // Insert a new vertex at the specified location
    void Insert(int p,T x,T y)
    {
        int p2 = p << 1;
        vertex.insert( vertex.begin()+p2, y);
        vertex.insert( vertex.begin()+p2, x);
    }

    // Remove a vertex at the specified location
    void Remove(int p)
    {
        int p2 = p << 1;
        vertex.erase(vertex.begin()+p2, vertex.begin()+p2+2);
    }

    // Resize the vertex buffer
    void Resize(int n)
    {
        vertex.resize(n<<1);
    }

    // Empty the vertex buffer
    void Clear()
    {
        vertex.clear();
    }

    // Get the bounding box of the vertices
    void GetBound(T& left,T& right,T& top, T& bottom) const;

    // Save to a file
    bool ToFile(const char* fileName)
    {
        ofstream output(fileName);
        if ( output.fail() )
            return false;
        output << *this;
        return true;
    }

    // Load from a file
    bool FromFile(const char* fileName)
    {
        ifstream input(fileName);
        if ( input.fail() )
            return false;
        input >> *this;
        return true;
    }
};

//////////////////////////////////////////
// Implementations
//////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Function name:    GetBound
// Function purpose: Get the bounding box of the vertices
// Input & Output:
//   left, right, top, bottom: replaced with the bounding box parameters. 
//      They are unchanged if the point set is empty.
// Return: None
///////////////////////////////////////////////////////////////////////
template <typename T> void PointSet<T>::GetBound(T& left, T& right, T& top, T& bottom) const
{
    int num = vertex.size();
    if( num < 2 )
        return;
    const T* p = &vertex[0];
    T  v;

    v = *p++;
    left = right = v;
    v = *p++;
    top = bottom = v;
    int i = 2;
    while( i < num )
    {
        v = *p++;
        if( v < left )
            left = v;
        if( v > right )
            right = v;
        ++i;

        v = *p++;
        if( v < top )
            top = v;
        if( v > bottom )
            bottom = v;
        ++i;
    }
}

///////////////////////////////////////////////////////////////////////
// Function name:    operator>>
// Function purpose: Input the content of a pointset from a stream
//   composed of several lines of comma-separated x, y coordinates
// Input:
//   output: the ostream reference
//   pt: a reference to a PointSet object
// Output:
//   the ostream reference
///////////////////////////////////////////////////////////////////////
template <typename T> istream& operator>>(istream& input, PointSet<T>& ps)
{
    string str,str1,str2;
    T x,y;
    ps.Clear();
    while( getline(input, str) )
    {
        int p = str.find(',');
        if( p>0 && p < str.size()-1 )
        {
            str1 = str.substr(0, p);
            str2 = str.substr(p+1);

            stringstream s1(str1);
            stringstream s2(str2);
            s1 >> x;
            s2 >> y;
            ps.Append(x,y);
        }
    }
    return input;
}

///////////////////////////////////////////////////////////////////////
// Function name:    operator<<
// Function purpose: Output the content of the point set to a stream
//   as several lines of comma-separated x, y coordinates
// Input:
//   output: the ostream reference
//   pt: a const reference to a PointSet object
// Output:
//   the ostream reference
///////////////////////////////////////////////////////////////////////
template <typename T> ostream& operator<<(ostream& output, const PointSet<T>& ps)
{
    for(int i=0; i<ps.GetLength(); i++)
        output << ps.X(i) << ',' << ps.Y(i) << std::endl;
    return output;
}


//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef PointSet<int>       PointSetInt;
typedef PointSet<float>     PointSetFloat;
typedef PointSet<double>    PointSetDouble;

#endif //POINTSET_H

