#ifndef ENTITYVIEW_H
#define ENTITYVIEW_H

#include <QObject>
#pragma warning( disable : 4996 )
#include "IwExtent3d.h"
#pragma warning( default : 4996)
#include "..\KUtility\TPickRay.h"
#include "Measure.h"

#include <vector>
using namespace std;

// entity display type - mesh, surface, subdivision ..
enum EntityShowType
{
    EntityShowFace,
    EntityShowEdge,
    EntityShowVertex
};

class TEST_EXPORT EntityView : public QObject
{
    Q_OBJECT

public:
    EntityView(QObject *parent=NULL);
    ~EntityView();

    // Whether anything is shown
    virtual bool IsShow() { return show; }

    // Set the show property
    virtual void SetShow(bool b);

    // Get the shown region
    virtual IwExtent3d GetShowRegion() { return region; }

    // Draw the shown opaque content, which must be provided by the derived class
    virtual void Draw() = 0;

    // Draw the shown transparent content
    virtual void DrawTransparent() {}

    // Pick the entity for measurement
    //  (I) ray         : a ray that picks an entity for measurement
    //  (I) tolerance   : the tolerance for picking
    //  (O) ent         : the picked entity
    //  (I, optional) mask: the candidate entity types of selection, default to all types 
	//  (I, optional) entToAvoid: the entity to avoid picking (maybe picked already)
    // Return:
    //  true        : successful and the picked entity is 'ent' with the picked point 'pt', 
    //  false       : failed
    virtual bool Measure(const PickRayFloat& ray, float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL) = 0;

    ///////////////////////////////////
    /// Intersection Related
    ///////////////////////////////////

    // Whether show intersection
    virtual bool IsShowIntersection() { return showIntersection; }

    // Set the intersection show property
    virtual void SetShowIntersection(bool b);

    // Toggle opaque/transparent drawing
    virtual void SetShowOpaque(bool b);
    bool IsShowOpaque() const { return opaque; }

signals:
    // This signal is emitted when the 'show' property is changed. 
    void showChanged(bool);

    // This signal is emitted when the 'showIntersection' property is changed. 
    void showIntersectionChanged(bool);

protected:
    // whether show the content
    bool show;
    // whether draw opaque
    bool opaque;

    // shown region
    IwExtent3d region;

    // whether show the intersection
    bool showIntersection;
};

#endif // ENTITYVIEW_H
