#pragma once

#include <QWidget>
#include <QDialog>

class QLineEdit;
class QString;
class QLabel;
class QRadioButton;
class QButtonGroup;


class CImplantHeaderDlg : public QDialog
	{
	Q_OBJECT

private slots:
	void OnOK();

public:
	CImplantHeaderDlg( QWidget* parent, int nPaientId, int nSide = 0 );

	QLabel*					m_labelPatientId;
	QLineEdit*				m_editPatientId;

	QRadioButton*			m_radioL;
	QRadioButton*			m_radioR;

	QPushButton*			m_buttonOK;
	QPushButton*			m_buttonCancel;

	int						m_nSide;
	};
