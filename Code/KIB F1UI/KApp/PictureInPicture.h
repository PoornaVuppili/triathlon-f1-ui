#pragma once

#ifndef PICTUREINPICTURE_H_INCLUDED
#define PICTUREINPICTURE_H_INCLUDED

#include <Windows.h>
#include <IwVector3d.h>
#include "../KUtility/TRigid3.h"

class Viewport;

class PictureInPicture
{
   Viewport   *pVp;
   IwVector3d  xa, ya, za;

   Rigid3Float wasVpTransform;
   int wasMinX, wasMaxX, wasMinY, wasMaxY;
   float wasLe, wasRi, wasBo, wasTo, wasNe, wasFa;

public:
   enum Location { eTopLeft, eTopRight, eBottomLeft, eBottomRight };

   PictureInPicture(Viewport *vp, Location loc,
                    IwAxis2Placement const &picCS,
                    IwVector3d const &modelRange,
                    SIZE const &screenMargin,
                    SIZE const &screenRect);
   
   ~PictureInPicture();
};

#endif // ndef PICTUREINPICTURE_H_INCLUDED
