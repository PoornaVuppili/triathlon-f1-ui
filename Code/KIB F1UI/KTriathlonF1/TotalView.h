#pragma once

#include "..\KAppTotal\OpenGLView.h"
#include "TotalMainWindow.h"
#include "..\KAppTotal\TriathlonF1Definitions.h"

#include <QTextEdit>
#include <QCheckBox>
#include <QPushButton>

class CTotalDoc;
class CTotalMainWindow;

class TEST_EXPORT_TW CTotalView : public COpenGLView
{
    Q_OBJECT

public:
    CTotalView( QObject *parent = 0 );
    virtual ~CTotalView();
	CTotalDoc*	GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};
	CTotalMainWindow*	GetMainWindow() {return ((CTotalMainWindow*)m_pMainWindow);};

	// For review
	bool				DisplayReviewCommentDialog(CEntRole eRole); // returns 'true' if "OK" is pressed, otherwise 'false'

public slots:
	void				OnViewFront();
	void				OnViewBack();
	void				OnViewTop();
	void				OnViewBottom();
	void				OnViewLeft();
	void				OnViewRight();
	void				OnViewFrontIso();
	void				OnViewBackIso();

	void				OnImportFemurSurface(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnMakeFemoralAxes(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewFemoralAxes() {OnMakeFemoralAxes(MAN_ACT_TYPE_REVIEW);};
	void				OnHandleFemoralAxes(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnAutoInitializeAllSteps(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	bool				OnAutoInitializeJCOSteps(bool calledFromInitializeAllSteps=false);
	void				OnAutoRegenerateJCOSteps();
	void				OnAutoRefineJCOSteps();
	void				OnAutoInitializeFromFemoralAxes();
	void				OnManualDefaultJCOSteps();
	void				OnDefineOutlineProfile(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewOutlineProfile(){OnDefineOutlineProfile(MAN_ACT_TYPE_REVIEW);};
	void				OnCutFemur(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewFemoralCuts(){OnCutFemur(MAN_ACT_TYPE_REVIEW);};
	void				OnHandleFillets(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnAutoInitializeFPCSteps(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);

	void				OnAdvancedControl();
	void				OnTest1();

	void				OnImportOsteophyteSurface(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnHandleCartilage(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnAutoInitializeAllJigsSteps(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnAutoInitializeDrivenJigsSteps(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnDefineCartilage(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewCartilage(){OnDefineCartilage(MAN_ACT_TYPE_REVIEW);};
	void				OnDefineOutlineProfileJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewOutlineProfileJigs(){OnDefineOutlineProfileJigs(MAN_ACT_TYPE_REVIEW);};
	void				OnHandleOuterSurfaceJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnMakeInnerSurfaceJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewInnerSurfaceJigs(){OnMakeInnerSurfaceJigs(MAN_ACT_TYPE_REVIEW);};
	void				OnMakeOuterSurfaceJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewOuterSurfaceJigs(){OnMakeOuterSurfaceJigs(MAN_ACT_TYPE_REVIEW);};
	void				OnMakeSideSurfaceJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnMakeSolidPositionJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewSolidPositionJigs(){OnMakeSolidPositionJigs(MAN_ACT_TYPE_REVIEW);};
	void				OnDefineStylusJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewStylusJigs(){OnDefineStylusJigs(MAN_ACT_TYPE_REVIEW);};
	void				OnMakeAnteriorSurfaceJigs(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnReviewAnteriorSurfaceJigs(){OnMakeAnteriorSurfaceJigs(MAN_ACT_TYPE_REVIEW);};
	void				OnOutputJigs();
	void				OnAdvancedControlJigs();

	void				OnMeasureScreenDistance();
	void				OnMeasureScreenAngle();
	void				OnMeasureScreenRadius();
	void				OnMeasureEntityDistance();
	void				OnMeasureEntityAngle();
	void				OnMeasureCurveRadius();
	void				OnAnalyzeImplant(CManagerActivateType manActType=MAN_ACT_TYPE_EDIT);
	void				OnValidateImplant(CManagerActivateType manActType=MAN_ACT_TYPE_VALIDATE, bool displayReportDlg=true);
	void				OnAcceptValidationReport();
	void				OnPrintValidationReport(QString outputFileName=QString(""));
	QTextEdit*			GetValidationReportTextEdit(){return m_validationReportTextEdit;};

	void				OnValidateJigs(CManagerActivateType manActType=MAN_ACT_TYPE_VALIDATE, bool displayReportDlg=true);
	void				OnAcceptValidationReportJigs();
	void				OnPrintValidationReportJigs(QString outputFileName=QString(""));
	QTextEdit*			GetValidationReportJigsTextEdit(){return m_validationReportJigsTextEdit;};

	void				OnFileInfo();
	void				OnPatientInfo();
	void				OnDetermineViewingNormalRegion();
	void				OnAcceptThisManager(CManager* thisManager);
	void				OnAcceptAnalyzeImplantManager();

	void				OnActivateNextReviewManager(CManager* pManagerToBeDeleted=NULL, CEntRole eRole=ENT_ROLE_NONE, bool bNext=true);
	void				OnActivateNextJigsReviewManager(CManager* pManagerToBeDeleted=NULL, CEntRole eRole=ENT_ROLE_JIGS_NONE, bool bNext=true);
	void				OnGoToPSFeaturesReview();
	void				OnCancelThisManager(CManager* thisManager);
	void				OnCancelAllMeasurementManagers();
	void				OnSetModifiedMarkerToFemoralJigs();

	void				OnDoNothing(){};
	// special purpose

protected:
	void				RefineAnteriorProfileOnlyInAJCO();

protected:
	QDialog*			m_validationReportDlg;
	QTextEdit*			m_validationReportTextEdit;
	QDialog*			m_validationReportJigsDlg;
	QTextEdit*			m_validationReportJigsTextEdit;
	QCheckBox*			m_chkNotableConditions;

private:
	
};
