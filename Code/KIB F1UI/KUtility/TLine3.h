#ifndef TLINE3_H
#define TLINE3_H

#include "TPoint3.h"
#include "TVector3.h"

template <typename T>
class TLine3
{
protected:
    TPoint3<T> o;    // origin
    TVector3<T> d;   // direction

public:
    TLine3() {}

    ///////////////////////////////////////////////////////////////////////
    // Function name:    TLine3
    // Function purpose: Constructor
    // Input: the coordinates of two points
    // Output: None
    ///////////////////////////////////////////////////////////////////////
    TLine3(T x1, T y1, T z1, T x2, T y2, T z2)
    { 
        o.SetXYZ(x1,y1,z1);
        d.SetXYZ(x2-x1, y2-y1, z2-z1);
        d.normalize();
    }

    TLine3(const TPoint3<T>& p1, const TPoint3<T>& p2)
    { 
        o = p1;
        d = (p2 - p1).normalized();
    }

    // Get values
    TPoint3<T> GetOrigin()     const { return o; }
    TVector3<T> GetDirection() const { return d; }

    // Set values
    void SetOrigin(const TPoint3<T>& p)     { o = p; }
    void SetDirection(const TVector3<T>& p) { d = p.normalized(); }

    // Calculate the distance to a point
    T Distance(const TPoint3<T>& p) const
    {
        // project the point on the line
        TPoint3<T> proj = o + d*((p - o).dot(d));
        return p.Distance(proj);
    }
    T Distance(T x, T y, T z) const
    {
        return Distance(TPoint3<T>(x,y,z));
    }

    // Find the point of projection on the line
    TPoint3<T> Project(const TPoint3<T>& p) const
    {
        return o + d*((p - o).dot(d));
    }

    // Test if two lines are parallel
    bool IsParallel(const TLine3<T>& ln, T tol = NumTraits<T>::dummy_precision()) const
    {
        T v = d.dot(ln.GetDirection());
        return (1-fabs(v)<tol);
    }

    // Test if two lines are colinear
    bool IsColinear(const TLine3<T>& ln, T tol = NumTraits<T>::dummy_precision()) const
    {
        return (IsParallel(ln, tol) && Distance(ln.GetOrigin())<tol);
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Intersect
    // Function purpose: Find the point of intersection with another line
    // Input:
    //   line: the other line
    // Return: an intersect flag
    //   IntersectFlag::Coincident, intersectPoint is undefined
    //   IntersectFlag::Parallel, intersectPoint is undefined
    //   IntersectFlag::Intersect, intersectPoint is the point of intersection
    //   IntersectFlag::NonIntersect, intersectPoint is the point that is
    //                                the closest to the other line
    ///////////////////////////////////////////////////////////////////////
    int Intersect(const TLine3<T>& line, TPoint3<T>& intersectPoint, T tol = NumTraits<T>::dummy_precision()) const
    {
        // Implementation is based on
        // http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/

        if( IsParallel(line) )
        {
            return Distance(line.GetOrigin())<tol ?
                IntersectFlag::Coincident : IntersectFlag::Parallel;
        }

        TPoint3<T> p1 = o;
        TPoint3<T> p21 = d;

        TPoint3<T> p3 = line.GetOrigin();
        TPoint3<T> p13 = p1 - p3;
        TPoint3<T> p43 = line.GetDirection();

        double d1343 = p13.dot(p43);
        double d4321 = p43.dot(p21);
        double d1321 = p13.dot(p21);
        double d4343 = p43.dot(p43);
        double d2121 = p21.dot(p21);

        double denom = d2121 * d4343 - d4321 * d4321;
        double numer = d1343 * d4321 - d1321 * d4343;

        double mua = numer / denom;
        double mub = (d1343 + d4321 * mua) / d4343;

        Point3Double pmua(p1.GetX()+mua*p21.GetX(), p1.GetY()+mua*p21.GetY(), p1.GetZ()+mua*p21.GetZ());
        Point3Double pmub(p3.GetX()+mub*p43.GetX(), p3.GetY()+mub*p43.GetY(), p3.GetZ()+mub*p43.GetZ());

        if( pmua.Distance(pmub) < tol )
        {
            intersectPoint.SetXYZ( (pmua.GetX()+pmub.GetX())/2, (pmua.GetY()+pmub.GetY())/2, (pmua.GetZ()+pmub.GetZ())/2 );
            return IntersectFlag::Intersect;
        }
        else
        {
            intersectPoint.SetXYZ( pmua.GetX(), pmua.GetY(), pmua.GetZ() );
            return IntersectFlag::NonIntersect;
        }
    }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TLine3<int>     Line3Int;
typedef TLine3<float>   Line3Float;
typedef TLine3<double>  Line3Double;

#endif //TLINE3_H
