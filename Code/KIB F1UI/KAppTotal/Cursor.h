#pragma once

enum CCursorType
	{
	CURSOR_NONE = -1,
	CURSOR_STANDARD,
	CURSOR_SELECTION,
	CURSOR_QUESTION,
	CURSOR_SKETCH
	};


