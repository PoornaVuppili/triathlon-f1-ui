/****************************************************************************
** Meta object code from reading C++ file 'ImplantTotalPS.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../ImplantTotalPS.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ImplantTotalPS.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ImplantTotalPS_t {
    QByteArrayData data[5];
    char stringdata0[70];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ImplantTotalPS_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ImplantTotalPS_t qt_meta_stringdata_ImplantTotalPS = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ImplantTotalPS"
QT_MOC_LITERAL(1, 15, 22), // "OnDesignStageCommenced"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 7), // "Module*"
QT_MOC_LITERAL(4, 47, 22) // "OnDesignStageCompleted"

    },
    "ImplantTotalPS\0OnDesignStageCommenced\0"
    "\0Module*\0OnDesignStageCompleted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ImplantTotalPS[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x0a /* Public */,
       1,    2,   37,    2, 0x0a /* Public */,
       4,    1,   42,    2, 0x0a /* Public */,
       4,    2,   45,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    2,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    2,    2,

       0        // eod
};

void ImplantTotalPS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ImplantTotalPS *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnDesignStageCommenced((*reinterpret_cast< Module*(*)>(_a[1]))); break;
        case 1: _t->OnDesignStageCommenced((*reinterpret_cast< Module*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->OnDesignStageCompleted((*reinterpret_cast< Module*(*)>(_a[1]))); break;
        case 3: _t->OnDesignStageCompleted((*reinterpret_cast< Module*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ImplantTotalPS::staticMetaObject = { {
    QMetaObject::SuperData::link<Implant::staticMetaObject>(),
    qt_meta_stringdata_ImplantTotalPS.data,
    qt_meta_data_ImplantTotalPS,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ImplantTotalPS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ImplantTotalPS::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ImplantTotalPS.stringdata0))
        return static_cast<void*>(this);
    return Implant::qt_metacast(_clname);
}

int ImplantTotalPS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Implant::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
