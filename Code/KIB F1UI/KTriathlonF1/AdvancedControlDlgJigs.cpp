#include "AdvancedControlDlgJigs.h"
#include "AdvancedControlJigs.h"
#include <qpushbutton.h>
#include <qfiledialog.h>
#include <qbuttongroup.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qstring.h>
#include <qgridlayout.h>
#include <qcheckbox.h>
#include <qtabwidget.h>

#include <QRadioButton>
#include <QDoubleSpinBox>

CAdvancedControlDlgJigs::CAdvancedControlDlgJigs( QWidget* parent, Qt::WindowFlags flags ) : QDialog( parent, flags )
{
	QGridLayout *dataLayout = createDataLayout();
	setLayout(dataLayout);
}

QGridLayout* CAdvancedControlDlgJigs::createDataLayout()
{
	bool underDev = false;
	bool enableAdvCtrlParamsModified = false;
	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (underDevelopment != NULL && QString("1") == underDevelopment)
	{
		underDev = true;
		enableAdvCtrlParamsModified = true;
	}

	QGridLayout *mainLayout = new QGridLayout;

	// create 2 layout on left and right.
	QVBoxLayout *rightLayout = new QVBoxLayout;
	QVBoxLayout *leftLayout = new QVBoxLayout;
	mainLayout->addLayout(rightLayout, 0, 0, 1, 7);
	mainLayout->addLayout(leftLayout, 0, 8, 1, 1, Qt::AlignBottom);

	// Right Layout
	QTabWidget *tabWidget = new QTabWidget;
	rightLayout->addWidget(tabWidget);

	// The order follows the importance and frequent use of the tabs
	QWidget *rightCartilageSurfaceWidget = new QWidget();
	tabWidget->addTab(rightCartilageSurfaceWidget, tr("Cartilage Surface"));
	QWidget *rightInnerSurfaceWidget = new QWidget();
	tabWidget->addTab(rightInnerSurfaceWidget, tr("Inner Surface"));
	QWidget *rightOuterSurfaceWidget = new QWidget();
	tabWidget->addTab(rightOuterSurfaceWidget, tr("Outer Surface"));
	QWidget *rightAnteriorSurfaceWidget = new QWidget();
	tabWidget->addTab(rightAnteriorSurfaceWidget, tr("Anterior Surface"));
	QWidget *rightSideSurfaceWidget = new QWidget();
	tabWidget->addTab(rightSideSurfaceWidget, tr("Side Surface"));
	QWidget *rightAutoStepsWidget = new QWidget();
	tabWidget->addTab(rightAutoStepsWidget, tr("Auto Steps"));
	QWidget *rightSolidPositionJigsWidget = new QWidget();
	tabWidget->addTab(rightSolidPositionJigsWidget, tr("Solid Position Jigs"));
	QWidget *rightStylusJigsWidget = new QWidget();
	tabWidget->addTab(rightStylusJigsWidget, tr("Stylus Jigs"));
	QWidget *rightOutputResultsWidget = new QWidget();
	tabWidget->addTab(rightOutputResultsWidget, tr("Output Results"));
	QWidget *rightOsteophyteSurfaceWidget = new QWidget();
	tabWidget->addTab(rightOsteophyteSurfaceWidget, tr("Osteophyte Surface"));
	QWidget *rightOutlineProfileWidget = new QWidget();
	tabWidget->addTab(rightOutlineProfileWidget, tr("Outline Profile"));
	
	/////// rightAutoStepsWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the AutoSteps tab
	QHBoxLayout *AutoStepsLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *AutoStepsFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextAutoAllSteps = new QLabel(tr("Auto all design steps:"));
	AutoStepsFirstColumnLayout->addWidget(labelTextAutoAllSteps, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAutoMajorSteps = new QLabel(tr("Auto driven design steps:"));
	AutoStepsFirstColumnLayout->addWidget(labelTextAutoMajorSteps, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAutoNoneSteps = new QLabel(tr("None:"));
	AutoStepsFirstColumnLayout->addWidget(labelTextAutoNoneSteps, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *AutoStepsSecondColumnLayout = new QVBoxLayout;
	m_buttonGroupAuto = new QButtonGroup();
	m_chkAutoAllSteps = new QRadioButton(tr(""));
	AutoStepsSecondColumnLayout->addWidget(m_chkAutoAllSteps, 0, Qt::AlignTop);
	m_buttonGroupAuto->addButton(m_chkAutoAllSteps);
	m_chkAutoAllSteps->setEnabled(true);
	m_chkAutoDrivenSteps = new QRadioButton(tr(""));
	AutoStepsSecondColumnLayout->addWidget(m_chkAutoDrivenSteps, 0, Qt::AlignTop);
	m_buttonGroupAuto->addButton(m_chkAutoDrivenSteps);
	m_chkAutoDrivenSteps->setEnabled(true);
	m_chkAutoNoneSteps = new QRadioButton(tr(""));
	AutoStepsSecondColumnLayout->addWidget(m_chkAutoNoneSteps, 0, Qt::AlignTop);
	m_buttonGroupAuto->addButton(m_chkAutoNoneSteps);
	m_chkAutoNoneSteps->setEnabled(true);
	m_chkAutoNoneSteps->setChecked(true);// make none checked. If any other is checked, it will be turned off.
	
	AutoStepsLayout->addLayout(AutoStepsFirstColumnLayout);
	AutoStepsLayout->addLayout(AutoStepsSecondColumnLayout);
	rightAutoStepsWidget->setLayout(AutoStepsLayout);

	/////// rightOsteophyteSurfaceWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the Osteophyte Surface tab
	QHBoxLayout *OsteophyteSurfaceLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *OsteophyteSurfaceFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextOsteophyteSurface = new QLabel(tr("No item here."));
	OsteophyteSurfaceFirstColumnLayout->addWidget(labelTextOsteophyteSurface, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *OsteophyteSurfaceSecondColumnLayout = new QVBoxLayout;

	OsteophyteSurfaceLayout->addLayout(OsteophyteSurfaceFirstColumnLayout);
	OsteophyteSurfaceLayout->addLayout(OsteophyteSurfaceSecondColumnLayout);
	rightOsteophyteSurfaceWidget->setLayout(OsteophyteSurfaceLayout);

	/////// rightCartilageSurfaceWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the CartilageSurface tab
	QHBoxLayout *CartilageSurfaceLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *CartilageSurfaceFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextCartilageThickness = new QLabel(tr("Cartilage thickness:"));
	CartilageSurfaceFirstColumnLayout->addWidget(labelTextCartilageThickness, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextTroGroAddCartilageThickness = new QLabel(tr("Trochlear additional thickness:"));
	CartilageSurfaceFirstColumnLayout->addWidget(labelTextTroGroAddCartilageThickness, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextTroGroCartilageTransitionWidth = new QLabel(tr("Trochlear transition width:"));
	CartilageSurfaceFirstColumnLayout->addWidget(labelTextTroGroCartilageTransitionWidth, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextCartilageInitializeEdgeOnly = new QLabel(tr("Initialize cartilage edge only:"));
	CartilageSurfaceFirstColumnLayout->addWidget(labelTextCartilageInitializeEdgeOnly, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextCartilageDisplaySmooth = new QLabel(tr("Display smooth button:"));
	CartilageSurfaceFirstColumnLayout->addWidget(labelTextCartilageDisplaySmooth, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextCartilageUseWaterTightSurface = new QLabel(tr("Use water tight surface:"));
	CartilageSurfaceFirstColumnLayout->addWidget(labelTextCartilageUseWaterTightSurface, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *CartilageSurfaceSecondColumnLayout = new QVBoxLayout;
	m_spinCartilageThickness = new QDoubleSpinBox();
	m_spinCartilageThickness->setRange(0, 10);
	m_spinCartilageThickness->setSingleStep(0.25);
	m_spinCartilageThickness->setEnabled(enableAdvCtrlParamsModified);
	CartilageSurfaceSecondColumnLayout->addWidget(m_spinCartilageThickness, 0, Qt::AlignTop);
	m_spinTroGroAddCartilageThickness = new QDoubleSpinBox();
	m_spinTroGroAddCartilageThickness->setRange(0, 10);
	m_spinTroGroAddCartilageThickness->setSingleStep(0.25);
	CartilageSurfaceSecondColumnLayout->addWidget(m_spinTroGroAddCartilageThickness, 0, Qt::AlignTop);
	m_spinTroGroCartilageTransitionWidth = new QDoubleSpinBox();
	m_spinTroGroCartilageTransitionWidth->setRange(3, 30);
	m_spinTroGroCartilageTransitionWidth->setSingleStep(0.5);
	CartilageSurfaceSecondColumnLayout->addWidget(m_spinTroGroCartilageTransitionWidth, 0, Qt::AlignTop);
	m_chkCartilageInitializeEdgeOnly = new QCheckBox(tr(""));
	CartilageSurfaceSecondColumnLayout->addWidget(m_chkCartilageInitializeEdgeOnly, 0, Qt::AlignTop);
	m_chkCartilageInitializeEdgeOnly->setEnabled(true);
	m_chkCartilageDisplaySmooth = new QCheckBox(tr(""));
	CartilageSurfaceSecondColumnLayout->addWidget(m_chkCartilageDisplaySmooth, 0, Qt::AlignTop);
	m_chkCartilageDisplaySmooth->setEnabled(true);
	m_chkCartilageUseWaterTightSurface = new QCheckBox(tr(""));
	CartilageSurfaceSecondColumnLayout->addWidget(m_chkCartilageUseWaterTightSurface, 0, Qt::AlignTop);
	m_chkCartilageUseWaterTightSurface->setEnabled(true);

	CartilageSurfaceLayout->addLayout(CartilageSurfaceFirstColumnLayout);
	CartilageSurfaceLayout->addLayout(CartilageSurfaceSecondColumnLayout);
	rightCartilageSurfaceWidget->setLayout(CartilageSurfaceLayout);

	/////// rightOutlineProfileWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the outline profile tab
	QHBoxLayout *OutlineProfileLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *OutlineProfileFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextOutlineSquareOffPosteriorTip = new QLabel(tr("Square off posterior tips:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineSquareOffPosteriorTip, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutlineSquareOffCornerRadiusRatio = new QLabel(tr("Square off corner radius ratio:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineSquareOffCornerRadiusRatio, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutlineSquareOffTransitionFactor = new QLabel(tr("Square off transition ratio:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineSquareOffTransitionFactor, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *OutlineProfileSecondColumnLayout = new QVBoxLayout;
	m_chkOutlineSquareOffPosteriorTip = new QCheckBox(tr(""));
	m_chkOutlineSquareOffPosteriorTip->setEnabled(true);
	OutlineProfileSecondColumnLayout->addWidget(m_chkOutlineSquareOffPosteriorTip, 0, Qt::AlignTop);
	m_spinOutlineSquareOffCornerRadiusRatio = new QDoubleSpinBox();
	m_spinOutlineSquareOffCornerRadiusRatio->setRange(0, 1.0);
	m_spinOutlineSquareOffCornerRadiusRatio->setSingleStep(0.05);
	m_spinOutlineSquareOffCornerRadiusRatio->setEnabled(true);
	OutlineProfileSecondColumnLayout->addWidget(m_spinOutlineSquareOffCornerRadiusRatio, 0, Qt::AlignTop);
	m_spinOutlineSquareOffTransitionFactor = new QDoubleSpinBox();
	m_spinOutlineSquareOffTransitionFactor->setRange(0, 10.0);
	m_spinOutlineSquareOffTransitionFactor->setSingleStep(0.25);
	m_spinOutlineSquareOffTransitionFactor->setEnabled(true);
	OutlineProfileSecondColumnLayout->addWidget(m_spinOutlineSquareOffTransitionFactor, 0, Qt::AlignTop);

	OutlineProfileLayout->addLayout(OutlineProfileFirstColumnLayout);
	OutlineProfileLayout->addLayout(OutlineProfileSecondColumnLayout);
	rightOutlineProfileWidget->setLayout(OutlineProfileLayout);

	/////// rightInnerSurfaceWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the innerSurface tab
	QHBoxLayout *InnerSurfaceLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *InnerSurfaceFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextInnerSurfaceApproachDegree = new QLabel(tr("F1 approaching angle:"));
	InnerSurfaceFirstColumnLayout->addWidget(labelTextInnerSurfaceApproachDegree, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextInnerSurfaceDropOffsetDiatance = new QLabel(tr("Tab extra offset distance:"));
	InnerSurfaceFirstColumnLayout->addWidget(labelTextInnerSurfaceDropOffsetDiatance, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextInnerSurfaceAdditionalDropLength = new QLabel(tr("Extra drop-down distance:"));
	InnerSurfaceFirstColumnLayout->addWidget(labelTextInnerSurfaceAdditionalDropLength, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextInnerSurfaceAdditionalPosteriorCoverage = new QLabel(tr("Extra posterior coverage:"));
	InnerSurfaceFirstColumnLayout->addWidget(labelTextInnerSurfaceAdditionalPosteriorCoverage, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextInnerSurfaceAnteriorDropDegree = new QLabel(tr("Anterior drop angle:"));
	InnerSurfaceFirstColumnLayout->addWidget(labelTextInnerSurfaceAnteriorDropDegree, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextInnerSurfaceDisplayAnteriorDropSurface = new QLabel(tr("Display ant drop surface:"));
	InnerSurfaceFirstColumnLayout->addWidget(labelTextInnerSurfaceDisplayAnteriorDropSurface, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *InnerSurfaceSecondColumnLayout = new QVBoxLayout;
	m_spinInnerSurfaceApproachDegree = new QDoubleSpinBox();
	m_spinInnerSurfaceApproachDegree->setRange(-15.0, 45.0);
	m_spinInnerSurfaceApproachDegree->setSingleStep(5.0);
	InnerSurfaceSecondColumnLayout->addWidget(m_spinInnerSurfaceApproachDegree, 0, Qt::AlignTop);
	m_spinInnerSurfaceDropOffsetDistance = new QDoubleSpinBox();
	m_spinInnerSurfaceDropOffsetDistance->setRange(0, 5.0);
	m_spinInnerSurfaceDropOffsetDistance->setSingleStep(0.1);
	InnerSurfaceSecondColumnLayout->addWidget(m_spinInnerSurfaceDropOffsetDistance, 0, Qt::AlignTop);
	m_spinInnerSurfaceAdditionalDropLength = new QDoubleSpinBox();
	m_spinInnerSurfaceAdditionalDropLength->setRange(-20, 50);
	m_spinInnerSurfaceAdditionalDropLength->setSingleStep(1.0);
	InnerSurfaceSecondColumnLayout->addWidget(m_spinInnerSurfaceAdditionalDropLength, 0, Qt::AlignTop);
	m_spinInnerSurfaceAdditionalPosteriorCoverage = new QDoubleSpinBox();
	m_spinInnerSurfaceAdditionalPosteriorCoverage->setRange(-30, 50);
	m_spinInnerSurfaceAdditionalPosteriorCoverage->setSingleStep(0.5);
	InnerSurfaceSecondColumnLayout->addWidget(m_spinInnerSurfaceAdditionalPosteriorCoverage, 0, Qt::AlignTop);
	m_spinInnerSurfaceAnteriorDropDegree = new QDoubleSpinBox();
	m_spinInnerSurfaceAnteriorDropDegree->setRange(0, 10.0);
	m_spinInnerSurfaceAnteriorDropDegree->setSingleStep(0.25);
	InnerSurfaceSecondColumnLayout->addWidget(m_spinInnerSurfaceAnteriorDropDegree, 0, Qt::AlignTop);
	m_chkInnerSurfaceDisplayAnteriorDropSurface = new QCheckBox(tr(""));
	InnerSurfaceSecondColumnLayout->addWidget(m_chkInnerSurfaceDisplayAnteriorDropSurface, 0, Qt::AlignTop);
	m_chkInnerSurfaceDisplayAnteriorDropSurface->setEnabled(enableAdvCtrlParamsModified);

	InnerSurfaceLayout->addLayout(InnerSurfaceFirstColumnLayout);
	InnerSurfaceLayout->addLayout(InnerSurfaceSecondColumnLayout);
	rightInnerSurfaceWidget->setLayout(InnerSurfaceLayout);

	/////// rightOuterSurfaceWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the outerSurface tab
	QHBoxLayout *OuterSurfaceLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *OuterSurfaceFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextOuterSurfaceOffsetDiatance = new QLabel(tr("Offset distance (thickness):"));
	OuterSurfaceFirstColumnLayout->addWidget(labelTextOuterSurfaceOffsetDiatance, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOuterSurfaceTroGroAddThickness = new QLabel(tr("Trochlear additional thickness:"));
	OuterSurfaceFirstColumnLayout->addWidget(labelTextOuterSurfaceTroGroAddThickness, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOuterSurfaceTroGroAddThicknessTransitionWidth = new QLabel(tr("Trochlear transition width (%):"));
	OuterSurfaceFirstColumnLayout->addWidget(labelTextOuterSurfaceTroGroAddThicknessTransitionWidth, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOuterSurfaceDisplaySmooth = new QLabel(tr("Remove spikes:"));
	OuterSurfaceFirstColumnLayout->addWidget(labelTextOuterSurfaceDisplaySmooth, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOuterSurfaceDropDegree = new QLabel(tr("Drop angle (deg):"));
	if(underDev) 
		OuterSurfaceFirstColumnLayout->addWidget(labelTextOuterSurfaceDropDegree, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOuterSurfaceDisplayAnteriorDropSurface = new QLabel(tr("Display ant drop n offset surfaces:"));
	OuterSurfaceFirstColumnLayout->addWidget(labelTextOuterSurfaceDisplayAnteriorDropSurface, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *OuterSurfaceSecondColumnLayout = new QVBoxLayout;
	m_spinOuterSurfaceOffsetDistance = new QDoubleSpinBox();
	m_spinOuterSurfaceOffsetDistance->setRange(3.5, 10.0);// minimum 3.5mm
	m_spinOuterSurfaceOffsetDistance->setSingleStep(0.1);
	m_spinOuterSurfaceOffsetDistance->setEnabled(true);
	OuterSurfaceSecondColumnLayout->addWidget(m_spinOuterSurfaceOffsetDistance, 0, Qt::AlignTop);
	m_spinOuterSurfaceTroGroAddThickness = new QDoubleSpinBox();
	m_spinOuterSurfaceTroGroAddThickness->setRange(0, 10);
	m_spinOuterSurfaceTroGroAddThickness->setSingleStep(0.25);
	OuterSurfaceSecondColumnLayout->addWidget(m_spinOuterSurfaceTroGroAddThickness, 0, Qt::AlignTop);
	m_spinOuterSurfaceTroGroAddThicknessTransitionWidth = new QDoubleSpinBox();
	m_spinOuterSurfaceTroGroAddThicknessTransitionWidth->setRange(0.01, 0.5);
	m_spinOuterSurfaceTroGroAddThicknessTransitionWidth->setSingleStep(0.01);
	OuterSurfaceSecondColumnLayout->addWidget(m_spinOuterSurfaceTroGroAddThicknessTransitionWidth, 0, Qt::AlignTop);
	m_chkOuterSurfaceEnforceSmooth = new QCheckBox(tr(""));
	OuterSurfaceSecondColumnLayout->addWidget(m_chkOuterSurfaceEnforceSmooth, 0, Qt::AlignTop);
	m_chkOuterSurfaceEnforceSmooth->setEnabled(true);
	m_spinOuterSurfaceDropDegree = new QDoubleSpinBox();
	m_spinOuterSurfaceDropDegree->setRange(-20.0, 20.0);
	m_spinOuterSurfaceDropDegree->setSingleStep(0.25);
	if (underDev)
		OuterSurfaceSecondColumnLayout->addWidget(m_spinOuterSurfaceDropDegree, 0, Qt::AlignTop);
	m_chkOuterSurfaceDisplayAnteriorDropSurface = new QCheckBox(tr(""));
	OuterSurfaceSecondColumnLayout->addWidget(m_chkOuterSurfaceDisplayAnteriorDropSurface, 0, Qt::AlignTop);
	m_chkOuterSurfaceDisplayAnteriorDropSurface->setEnabled(enableAdvCtrlParamsModified);

	OuterSurfaceLayout->addLayout(OuterSurfaceFirstColumnLayout);
	OuterSurfaceLayout->addLayout(OuterSurfaceSecondColumnLayout);
	rightOuterSurfaceWidget->setLayout(OuterSurfaceLayout);

	/////// rightSideSurfaceWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the sideSurface tab
	QHBoxLayout *SideSurfaceLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *SideSurfaceFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextSideSurfaceExtendedWidth = new QLabel(tr("Side extended width:"));
	SideSurfaceFirstColumnLayout->addWidget(labelTextSideSurfaceExtendedWidth, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *SideSurfaceSecondColumnLayout = new QVBoxLayout;
	m_spinSideSurfaceExtendedWidth = new QDoubleSpinBox();
	m_spinSideSurfaceExtendedWidth->setRange(-5.0, 10.0);
	m_spinSideSurfaceExtendedWidth->setSingleStep(0.1);
	m_spinSideSurfaceExtendedWidth->setEnabled(true);
	SideSurfaceSecondColumnLayout->addWidget(m_spinSideSurfaceExtendedWidth, 0, Qt::AlignTop);

	SideSurfaceLayout->addLayout(SideSurfaceFirstColumnLayout);
	SideSurfaceLayout->addLayout(SideSurfaceSecondColumnLayout);
	rightSideSurfaceWidget->setLayout(SideSurfaceLayout);

	/////// rightSolidPositionJigsWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the Solid Position Jigs tab
	QHBoxLayout *SolidPositionJigsLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *SolidPositionJigsFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextSolidPositionJigsToleranceLevel = new QLabel(tr("Tolerance level:"));
	SolidPositionJigsFirstColumnLayout->addWidget(labelTextSolidPositionJigsToleranceLevel, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *SolidPositionJigsSecondColumnLayout = new QVBoxLayout;
	m_spinSolidPositionJigsToleranceLevel = new QSpinBox();
	m_spinSolidPositionJigsToleranceLevel->setRange(1, 6);
	m_spinSolidPositionJigsToleranceLevel->setSingleStep(1);
	SolidPositionJigsSecondColumnLayout->addWidget(m_spinSolidPositionJigsToleranceLevel, 0, Qt::AlignTop);

	SolidPositionJigsLayout->addLayout(SolidPositionJigsFirstColumnLayout);
	SolidPositionJigsLayout->addLayout(SolidPositionJigsSecondColumnLayout);
	rightSolidPositionJigsWidget->setLayout(SolidPositionJigsLayout);

	/////// rightStylusJigsWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the stylus jigs tab
	QHBoxLayout *StylusJigsLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *StylusJigsFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextStylusJigsClearance = new QLabel(tr("Desired stylus clearance:"));
	StylusJigsFirstColumnLayout->addWidget(labelTextStylusJigsClearance, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextStylusJigsDisplayVirtualFeaturesOnly = new QLabel(tr("Display virtual features only:"));
	StylusJigsFirstColumnLayout->addWidget(labelTextStylusJigsDisplayVirtualFeaturesOnly, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextStylusJigsFullControl = new QLabel(tr("Fully control stylus parameters:"));
	StylusJigsFirstColumnLayout->addWidget(labelTextStylusJigsFullControl, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *StylusJigsSecondColumnLayout = new QVBoxLayout;
	m_spinStylusJigsClearance = new QDoubleSpinBox();
	m_spinStylusJigsClearance->setRange(0.0, 10.0);
	m_spinStylusJigsClearance->setSingleStep(0.1);
	StylusJigsSecondColumnLayout->addWidget(m_spinStylusJigsClearance, 0, Qt::AlignTop);
	m_chkStylusJigsDisplayVirtualFeaturesOnly = new QCheckBox(tr(""));
	m_chkStylusJigsDisplayVirtualFeaturesOnly->setChecked(true);
	StylusJigsSecondColumnLayout->addWidget(m_chkStylusJigsDisplayVirtualFeaturesOnly, 0, Qt::AlignTop);
	m_chkStylusJigsFullControl = new QCheckBox(tr(""));
	m_chkStylusJigsFullControl->setChecked(true);
	StylusJigsSecondColumnLayout->addWidget(m_chkStylusJigsFullControl, 0, Qt::AlignTop);

	StylusJigsLayout->addLayout(StylusJigsFirstColumnLayout);
	StylusJigsLayout->addLayout(StylusJigsSecondColumnLayout);
	rightStylusJigsWidget->setLayout(StylusJigsLayout);

	/////// rightAnteriorSurfaceWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the anteriorSurface tab
	QHBoxLayout *AnteriorSurfaceLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *AnteriorSurfaceFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextAnteriorSurfaceApproachDegree = new QLabel(tr("F3 approaching angle:"));
	AnteriorSurfaceFirstColumnLayout->addWidget(labelTextAnteriorSurfaceApproachDegree, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAnteriorSurfaceAntExtension = new QLabel(tr("Additional anterior extension:"));
	AnteriorSurfaceFirstColumnLayout->addWidget(labelTextAnteriorSurfaceAntExtension, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAnteriorSurfaceDropExtension = new QLabel(tr("Additional drop extension:"));
	AnteriorSurfaceFirstColumnLayout->addWidget(labelTextAnteriorSurfaceDropExtension, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *AnteriorSurfaceSecondColumnLayout = new QVBoxLayout;
	m_spinAnteriorSurfaceApproachDegree = new QDoubleSpinBox();
	m_spinAnteriorSurfaceApproachDegree->setRange(0.0, 45.0);
	m_spinAnteriorSurfaceApproachDegree->setSingleStep(1.0);
	AnteriorSurfaceSecondColumnLayout->addWidget(m_spinAnteriorSurfaceApproachDegree, 0, Qt::AlignTop);
	m_spinAnteriorSurfaceAntExtension = new QDoubleSpinBox();
	m_spinAnteriorSurfaceAntExtension->setRange(0.0, 50.0);
	m_spinAnteriorSurfaceAntExtension->setSingleStep(0.5);
	AnteriorSurfaceSecondColumnLayout->addWidget(m_spinAnteriorSurfaceAntExtension, 0, Qt::AlignTop);
	m_spinAnteriorSurfaceDropExtension = new QDoubleSpinBox();
	m_spinAnteriorSurfaceDropExtension->setRange(-25.0, 50.0);
	m_spinAnteriorSurfaceDropExtension->setSingleStep(0.5);
	AnteriorSurfaceSecondColumnLayout->addWidget(m_spinAnteriorSurfaceDropExtension, 0, Qt::AlignTop);

	AnteriorSurfaceLayout->addLayout(AnteriorSurfaceFirstColumnLayout);
	AnteriorSurfaceLayout->addLayout(AnteriorSurfaceSecondColumnLayout);
	rightAnteriorSurfaceWidget->setLayout(AnteriorSurfaceLayout);

	/////// rightOutputWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the OutputResults tab
	QHBoxLayout *OutputLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *OutputFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextOutputOsteophyteSurface = new QLabel(tr("Output osteophyte surface:"));
	OutputFirstColumnLayout->addWidget(labelTextOutputOsteophyteSurface, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputCartilageSurface = new QLabel(tr("Output cartilage surface:"));
	OutputFirstColumnLayout->addWidget(labelTextOutputCartilageSurface, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputThreeSurfaces = new QLabel(tr("Output inner outer side surfaces:"));
	OutputFirstColumnLayout->addWidget(labelTextOutputThreeSurfaces, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputAnteriorSurfaceRegardless = new QLabel(tr("Output anterior surface regardless:"));
	OutputFirstColumnLayout->addWidget(labelTextOutputAnteriorSurfaceRegardless, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *OutputSecondColumnLayout = new QVBoxLayout;
	m_chkOutputOsteophyteSurface = new QCheckBox(tr(""));
	m_chkOutputOsteophyteSurface->setChecked(false);
	OutputSecondColumnLayout->addWidget(m_chkOutputOsteophyteSurface, 0, Qt::AlignTop);
	m_chkOutputCartilageSurface = new QCheckBox(tr(""));
	OutputSecondColumnLayout->addWidget(m_chkOutputCartilageSurface, 0, Qt::AlignTop);
	m_chkOutputCartilageSurface->setChecked(false);
	m_chkOutputThreeSurfaces = new QCheckBox(tr(""));
	OutputSecondColumnLayout->addWidget(m_chkOutputThreeSurfaces, 0, Qt::AlignTop);
	m_chkOutputThreeSurfaces->setChecked(false);
	m_chkOutputAnteriorSurfaceRegardless = new QCheckBox(tr(""));
	OutputSecondColumnLayout->addWidget(m_chkOutputAnteriorSurfaceRegardless, 0, Qt::AlignTop);
	m_chkOutputAnteriorSurfaceRegardless->setChecked(false);

	OutputLayout->addLayout(OutputFirstColumnLayout);
	OutputLayout->addLayout(OutputSecondColumnLayout);
	rightOutputResultsWidget->setLayout(OutputLayout);

	//////////////////////////////////////////////////////////////////////////////////
	//// Left Layout
	//// Two buttons
	QVBoxLayout *vlayoutButtons = new QVBoxLayout;

	QPushButton *OKButton = new QPushButton(tr("OK"), NULL);
	connect(OKButton, SIGNAL(clicked(bool)), this, SLOT(accept()));
	vlayoutButtons->addWidget(OKButton);
	QPushButton *CancelButton = new QPushButton(tr("Cancal"), NULL);
	connect(CancelButton, SIGNAL(clicked(bool)), this, SLOT(reject()));
	vlayoutButtons->addWidget(CancelButton);

	leftLayout->addLayout(vlayoutButtons);

	return mainLayout;
}

// Auto Steps
bool CAdvancedControlDlgJigs::GetAutoAllSteps()
{
	return m_chkAutoAllSteps->isChecked();
}
void CAdvancedControlDlgJigs::SetAutoAllSteps(bool flag)
{
	m_chkAutoAllSteps->setChecked(flag);
}

bool CAdvancedControlDlgJigs::GetAutoDrivenSteps()
{
	return m_chkAutoDrivenSteps->isChecked();
}
void CAdvancedControlDlgJigs::SetAutoDrivenSteps(bool flag)
{
	m_chkAutoDrivenSteps->setChecked(flag);
}

// Osteophyte Surface

// Cartilage Surface
double CAdvancedControlDlgJigs::GetCartilageThickness()
{
	return m_spinCartilageThickness->value();
}
void CAdvancedControlDlgJigs::SetCartilageThickness(double val)
{
	m_spinCartilageThickness->setValue(val);
}
double CAdvancedControlDlgJigs::GetTrochlearGrooveAdditionalCartilageThickness()
{
	return m_spinTroGroAddCartilageThickness->value();
}
void CAdvancedControlDlgJigs::SetTrochlearGrooveAdditionalCartilageThickness(double val)
{
	m_spinTroGroAddCartilageThickness->setValue(val);
}
double CAdvancedControlDlgJigs::GetTrochlearGrooveCartilageTransitionWidth()
{
	return m_spinTroGroCartilageTransitionWidth->value();
}
void CAdvancedControlDlgJigs::SetTrochlearGrooveCartilageTransitionWidth(double val)
{
	m_spinTroGroCartilageTransitionWidth->setValue(val);
}
bool CAdvancedControlDlgJigs::GetCartilageInitializeEdgeOnly()
{
	if (m_chkCartilageInitializeEdgeOnly->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetCartilageInitializeEdgeOnly(bool val)
{
	if ( val )
		m_chkCartilageInitializeEdgeOnly->setCheckState(Qt::Checked);
	else
		m_chkCartilageInitializeEdgeOnly->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetCartilageUseWaterTightSurface()
{
	if (m_chkCartilageUseWaterTightSurface->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetCartilageUseWaterTightSurface(bool val)
{
	if ( val )
		m_chkCartilageUseWaterTightSurface->setCheckState(Qt::Checked);
	else
		m_chkCartilageUseWaterTightSurface->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetCartilageDisplaySmooth()
{
	if (m_chkCartilageDisplaySmooth->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetCartilageDisplaySmooth(bool val)
{
	if ( val )
		m_chkCartilageDisplaySmooth->setCheckState(Qt::Checked);
	else
		m_chkCartilageDisplaySmooth->setCheckState(Qt::Unchecked);
}

// Outline Profile
bool CAdvancedControlDlgJigs::GetOutlineSquareOffPosteriorTip()
{
	if (m_chkOutlineSquareOffPosteriorTip->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOutlineSquareOffPosteriorTip(bool val)
{
	if ( val )
		m_chkOutlineSquareOffPosteriorTip->setCheckState(Qt::Checked);
	else
		m_chkOutlineSquareOffPosteriorTip->setCheckState(Qt::Unchecked);
	//
	m_spinOutlineSquareOffCornerRadiusRatio->setEnabled(val);
	m_spinOutlineSquareOffTransitionFactor->setEnabled(val);
}
double CAdvancedControlDlgJigs::GetOutlineSquareOffCornerRadiusRatio()
{
	return m_spinOutlineSquareOffCornerRadiusRatio->value();
}
void CAdvancedControlDlgJigs::SetOutlineSquareOffCornerRadiusRatio(double val)
{
	m_spinOutlineSquareOffCornerRadiusRatio->setValue(val);
}
double CAdvancedControlDlgJigs::GetOutlineSquareOffTransitionFactor()
{
	return m_spinOutlineSquareOffTransitionFactor->value();
}
void CAdvancedControlDlgJigs::SetOutlineSquareOffTransitionFactor(double val)
{
	m_spinOutlineSquareOffTransitionFactor->setValue(val);
}
// Inner Surface
double CAdvancedControlDlgJigs::GetInnerSurfaceApproachDegree()
{
	return m_spinInnerSurfaceApproachDegree->value();
}
void CAdvancedControlDlgJigs::SetInnerSurfaceApproachDegree(double val)
{
	m_spinInnerSurfaceApproachDegree->setValue(val);
}
double CAdvancedControlDlgJigs::GetInnerSurfaceDropOffsetDistance()
{
	return m_spinInnerSurfaceDropOffsetDistance->value();
}
void CAdvancedControlDlgJigs::SetInnerSurfaceDropOffsetDistance(double val)
{
	m_spinInnerSurfaceDropOffsetDistance->setValue(val);
}
double CAdvancedControlDlgJigs::GetInnerSurfaceAdditionalDropLength()
{
	return m_spinInnerSurfaceAdditionalDropLength->value();
}
void CAdvancedControlDlgJigs::SetInnerSurfaceAdditionalDropLength(double val)
{
	m_spinInnerSurfaceAdditionalDropLength->setValue(val);
}
double CAdvancedControlDlgJigs::GetInnerSurfaceAdditionalPosteriorCoverage()
{
	return m_spinInnerSurfaceAdditionalPosteriorCoverage->value();
}
void CAdvancedControlDlgJigs::SetInnerSurfaceAdditionalPosteriorCoverage(double val)
{
	m_spinInnerSurfaceAdditionalPosteriorCoverage->setValue(val);
}
double CAdvancedControlDlgJigs::GetInnerSurfaceAnteriorDropDegree()
{
	return m_spinInnerSurfaceAnteriorDropDegree->value();
}
void CAdvancedControlDlgJigs::SetInnerSurfaceAnteriorDropDegree(double val)
{
	m_spinInnerSurfaceAnteriorDropDegree->setValue(val);
}
bool CAdvancedControlDlgJigs::GetInnerSurfaceDisplayAnteriorDropSurface()
{
	if (m_chkInnerSurfaceDisplayAnteriorDropSurface->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetInnerSurfaceDisplayAnteriorDropSurface(bool val)
{
	if ( val )
		m_chkInnerSurfaceDisplayAnteriorDropSurface->setCheckState(Qt::Checked);
	else
		m_chkInnerSurfaceDisplayAnteriorDropSurface->setCheckState(Qt::Unchecked);
}
// Outer Surface
double CAdvancedControlDlgJigs::GetOuterSurfaceOffsetDistance()
{
	return m_spinOuterSurfaceOffsetDistance->value();
}
void CAdvancedControlDlgJigs::SetOuterSurfaceOffsetDistance(double val)
{
	m_spinOuterSurfaceOffsetDistance->setValue(val);
}
double CAdvancedControlDlgJigs::GetOuterSurfaceTrochlearGrooveAdditionalThickness()
{
	return m_spinOuterSurfaceTroGroAddThickness->value();
}
void CAdvancedControlDlgJigs::SetOuterSurfaceTrochlearGrooveAdditionalThickness(double val)
{
	m_spinOuterSurfaceTroGroAddThickness->setValue(val);
}
double CAdvancedControlDlgJigs::GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth()
{
	return m_spinOuterSurfaceTroGroAddThicknessTransitionWidth->value();
}
void CAdvancedControlDlgJigs::SetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth(double val)
{
	m_spinOuterSurfaceTroGroAddThicknessTransitionWidth->setValue(val);
}
double CAdvancedControlDlgJigs::GetOuterSurfaceDropDegree()
{
	return m_spinOuterSurfaceDropDegree->value();
}
void CAdvancedControlDlgJigs::SetOuterSurfaceDropDegree(double val)
{
	m_spinOuterSurfaceDropDegree->setValue(val);
}
bool CAdvancedControlDlgJigs::GetOuterSurfaceDisplayAnteriorDropSurface()
{
	if (m_chkOuterSurfaceDisplayAnteriorDropSurface->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOuterSurfaceDisplayAnteriorDropSurface(bool val)
{
	if ( val )
		m_chkOuterSurfaceDisplayAnteriorDropSurface->setCheckState(Qt::Checked);
	else
		m_chkOuterSurfaceDisplayAnteriorDropSurface->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetOuterSurfaceEnforceSmooth()
{
	if (m_chkOuterSurfaceEnforceSmooth->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOuterSurfaceEnforceSmooth(bool val)
{
	if ( val )
		m_chkOuterSurfaceEnforceSmooth->setCheckState(Qt::Checked);
	else
		m_chkOuterSurfaceEnforceSmooth->setCheckState(Qt::Unchecked);
}
// Side Surface
double CAdvancedControlDlgJigs::GetSideSurfaceExtendedWidth()
{
	return m_spinSideSurfaceExtendedWidth->value();
}
void CAdvancedControlDlgJigs::SetSideSurfaceExtendedWidth(double val)
{
	m_spinSideSurfaceExtendedWidth->setValue(val);
}

// Solid Position Jigs
int CAdvancedControlDlgJigs::GetSolidPositionJigsToleranceLevel()
{
	return m_spinSolidPositionJigsToleranceLevel->value();
}
void CAdvancedControlDlgJigs::SetSolidPositionJigsToleranceLevel(int val)
{
	m_spinSolidPositionJigsToleranceLevel->setValue(val);
}
// Stylus Jigs
bool CAdvancedControlDlgJigs::GetStylusJigsDisplayVirtualFeaturesOnly()
{
	if (m_chkStylusJigsDisplayVirtualFeaturesOnly->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetStylusJigsDisplayVirtualFeaturesOnly(bool val)
{
	if ( val )
		m_chkStylusJigsDisplayVirtualFeaturesOnly->setCheckState(Qt::Checked);
	else
		m_chkStylusJigsDisplayVirtualFeaturesOnly->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetStylusJigsFullControl()
{
	if (m_chkStylusJigsFullControl->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetStylusJigsFullControl(bool val)
{
	if ( val )
		m_chkStylusJigsFullControl->setCheckState(Qt::Checked);
	else
		m_chkStylusJigsFullControl->setCheckState(Qt::Unchecked);
}
double CAdvancedControlDlgJigs::GetStylusJigsClearance()
{
	return m_spinStylusJigsClearance->value();
}
void CAdvancedControlDlgJigs::SetStylusJigsClearance(double val)
{
	m_spinStylusJigsClearance->setValue(val);
}
// Anterior Surface
double CAdvancedControlDlgJigs::GetAnteriorSurfaceApproachDegree()
{
	return m_spinAnteriorSurfaceApproachDegree->value();
}
void CAdvancedControlDlgJigs::SetAnteriorSurfaceApproachDegree(double val)
{
	m_spinAnteriorSurfaceApproachDegree->setValue(val);
}
double CAdvancedControlDlgJigs::GetAnteriorSurfaceAntExtension()
{
	return m_spinAnteriorSurfaceAntExtension->value();
}
void CAdvancedControlDlgJigs::SetAnteriorSurfaceAntExtension(double val)
{
	m_spinAnteriorSurfaceAntExtension->setValue(val);
}
double CAdvancedControlDlgJigs::GetAnteriorSurfaceDropExtension()
{
	return m_spinAnteriorSurfaceDropExtension->value();
}
void CAdvancedControlDlgJigs::SetAnteriorSurfaceDropExtension(double val)
{
	m_spinAnteriorSurfaceDropExtension->setValue(val);
}
// Outputs
bool CAdvancedControlDlgJigs::GetOutputOsteophyteSurface()
{
	if (m_chkOutputOsteophyteSurface->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOutputOsteophyteSurface(bool val)
{
	if ( val )
		m_chkOutputOsteophyteSurface->setCheckState(Qt::Checked);
	else
		m_chkOutputOsteophyteSurface->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetOutputCartilageSurface()
{
	if (m_chkOutputCartilageSurface->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOutputCartilageSurface(bool val)
{
	if ( val )
		m_chkOutputCartilageSurface->setCheckState(Qt::Checked);
	else
		m_chkOutputCartilageSurface->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetOutputThreeSurfaces()
{
	if (m_chkOutputThreeSurfaces->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOutputThreeSurfaces(bool val)
{
	if ( val )
		m_chkOutputThreeSurfaces->setCheckState(Qt::Checked);
	else
		m_chkOutputThreeSurfaces->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlgJigs::GetOutputAnteriorSurfaceRegardless()
{
	if (m_chkOutputAnteriorSurfaceRegardless->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlgJigs::SetOutputAnteriorSurfaceRegardless(bool val)
{
	if ( val )
		m_chkOutputAnteriorSurfaceRegardless->setCheckState(Qt::Checked);
	else
		m_chkOutputAnteriorSurfaceRegardless->setCheckState(Qt::Unchecked);
}