#include "Entity.h"
#include "DataDoc.h"


CEntTypeEntry CEntity::EntTypes[] = 
	{
		{ ENT_TYPE_PART,				"Part"						},
		{ ENT_TYPE_FEMORAL_AXES,		"Femoral Axes"				},
		{ ENT_TYPE_FEMORAL_PART,		"Femoral Part"				},
		{ ENT_TYPE_FEMORAL_CUTS,		"Femoral Cuts"				},
		{ ENT_TYPE_ADVANCED_CONTROL,	"Advanced Control"			},
	};


CEntRoleEntry CEntity::EntRoles[] = 
	{
		// Femur Implants
		{ ENT_ROLE_NONE,				"Temp"						},
		
		{ ENT_ROLE_FEMUR,				"Femur Surface"				},
		
		{ ENT_ROLE_HIP,					"Hip"						},
	
		{ ENT_ROLE_FEMORAL_AXES,		"Femoral Axes"				},
		{ ENT_ROLE_FEMORAL_CUTS,		"Femoral Cuts"				},
		{ ENT_ROLE_OUTLINE_PROFILE,		"Outline Profile"			},
		{ ENT_ROLE_ADVANCED_CONTROL,	"Advanced Control"			},
		//// Femur Jigs ////
		{ ENT_ROLE_JIGS_NONE,			"Temp Jigs"					},
		{ ENT_ROLE_ADVANCED_CONTROL_JIGS,	"Advanced Control Jigs"	},
		{ ENT_ROLE_OSTEOPHYTE_SURFACE,	"Osteophyte"				},
		{ ENT_ROLE_CARTILAGE_SURFACE,	"Cartilage"					},
		{ ENT_ROLE_OUTLINE_PROFILE_JIGS,"Outline Profile Jigs"		},
		{ ENT_ROLE_INNER_SURFACE_JIGS,	"Inner Surface Jigs"		},
		{ ENT_ROLE_OUTER_SURFACE_JIGS,	"Outer Surface Jigs"		},
		{ ENT_ROLE_SIDE_SURFACE_JIGS,	"Side Surface Jigs"			},
		{ ENT_ROLE_SOLID_POSITION_JIGS,	"Solid Position Jigs"		},
		{ ENT_ROLE_STYLUS_JIGS,			"Stylus Jigs"				},
		{ ENT_ROLE_ANTERIOR_SURFACE_JIGS,"Anterior Surface Jigs"	},

		//// Femur Jigs PS
		{ ENT_ROLE_JIGS_PS_NONE,			"Temp Jigs PS"				},

		//// Implant Notes
		{ ENT_ROLE_IMPLANT_APPLICABLE_NOTES,	"Implant Applicable Notes"	},
		{ ENT_ROLE_IMPLANT_NC_FEMUR_HARDWARE,	"Implant Fem Hardware"	},
        { ENT_ROLE_IMPLANT_NC_FEMUR_LARGE_UNDERNANG, "Large Femoral Underhang" }

	};


CDispModeEntry CEntity::DispModes[] = 
	{
		{ DISP_MODE_NONE,				"None"						},
		{ DISP_MODE_HIDDEN,				"Hidden"					},
		{ DISP_MODE_WIREFRAME,			"Wireframe"					},
//		{ DISP_MODE_SHADE,				"Shade"						}, // Retired in iTW6025
		{ DISP_MODE_DISPLAY,			"Display"					},
		{ DISP_MODE_TRANSPARENCY,		"Transparency"				},
		{ DISP_MODE_DISPLAY_CT,			"Display CT"				},
		{ DISP_MODE_TRANSPARENT_CT,		"Transparent CT"			},
		{ DISP_MODE_DISPLAY_NET,		"Display Net"				},
		{ DISP_MODE_TRANSPARENCY_NET,	"Transparency Net"			}
	};



CEntity::CEntity()
{
	m_id = -1;
	m_bUdateToDate = true;
	m_sValidateMessage = "";
	m_2ndItem = NULL;
}


CEntity::CEntity( CDataDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int id )
{
	m_pDoc		= pDoc;
	m_eEntRole	= eEntRole;

	// All the NONE
	if ( eEntRole == ENT_ROLE_NONE || eEntRole == ENT_ROLE_JIGS_NONE || eEntRole == ENT_ROLE_JIGS_PS_NONE)
		m_sEntName = GetFileNameWithExtension(sFileName);
	else
		m_sEntName = RoleName( eEntRole );

	m_sFileName = sFileName;

	m_id = id;

	m_bUdateToDate = true;
	m_2ndItem = NULL;
	
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;

	m_sValidateMessage = "";
	m_designTime = 0;

	if( m_id == -1 )
	{
		m_id = m_pDoc->GetAvlEntId();
	}
	else
	{
		m_pDoc->UpdateAvlEntId( m_id );
	}
	
	if( m_sFileName == "" )
	{
		FormFileName();
	}
}


CEntity::~CEntity()
{
}


void CEntity::Display()
{
}


void CEntity::FormFileName()
{
	QString		sPatientId = m_pDoc->GetPatientId();
	QString		sEntId;
	
	sEntId.sprintf( "%03d", m_id );

	// All the NONE
	if ( this->GetEntRole() == ENT_ROLE_NONE || this->GetEntRole() == ENT_ROLE_JIGS_NONE || this->GetEntRole() == ENT_ROLE_JIGS_PS_NONE)
	{
		m_sFileName = QString( "%1" ).arg( m_sEntName );
	}
	else
	{
		m_sFileName = QString( "%1 %2 %3" ).arg( sPatientId ).arg( sEntId ).arg( m_sEntName );
	}

	m_sFileName.replace( ' ', '_' );
}


void CEntity::SetEntId( int id )
{
	m_id = id;
}


int CEntity::GetEntId() const
{
	return m_id;
}


void CEntity::SetEntRole( CEntRole eEntRole )
{
	m_eEntRole = eEntRole;

	m_sEntName = RoleName( eEntRole );
}


CEntRole CEntity::GetEntRole() const
{
	return m_eEntRole;
}


void CEntity::SetEntName( const QString& sEntName )
{
	m_sEntName = sEntName;
}

QString	 CEntity::GetEntName() const
{
	return m_sEntName;
}


void CEntity::SetEntType( CEntType eEntType )
{
	m_eEntType = eEntType;
}


CEntType CEntity::GetEntType()
{
	return m_eEntType;
}


void CEntity::SetFileName( const QString& sFileName )
{
	m_sFileName = sFileName;
}


QString CEntity::GetFileName()
{
	return m_sFileName;
}
	

void CEntity::SetDisplayMode( CDisplayMode eDisplayMode )
{
	m_eDisplayMode = eDisplayMode;

	m_pDoc->UpdateBounds();

	m_pDoc->SetModified( true );
}

CDisplayMode CEntity::GetDisplayMode()
{
	return m_eDisplayMode;
}

void CEntity::SetColor( const CColor& color )
{
	m_color = color;

	m_pDoc->SetModified( true );
}


CColor CEntity::GetColor()
{
	return m_color;
}


CDataDoc* CEntity::GetDoc()
{
	return m_pDoc;
}


QString	CEntity::EntTypeName( CEntType eEntType )
{
	int				i, nEntTypes;
	QString			sEntType = "";

	nEntTypes = sizeof( EntTypes ) / sizeof( CEntTypeEntry );

	for( i = 0; i < nEntTypes; i++ )
	{
		if( EntTypes[i].eEntType == eEntType )
		{
			sEntType = EntTypes[i].sEntType;
			break;
		}
	}

	return sEntType;
}


CEntType CEntity::EntTypeByName( const QString& sEntType )
{
	int				i, nEntTypes;
	CEntType		eEntType = ENT_TYPE_NONE;

	nEntTypes = sizeof( EntTypes ) / sizeof( CEntTypeEntry );

	for( i = 0; i < nEntTypes; i++ )
	{
		if( EntTypes[i].sEntType == sEntType )
		{
			eEntType = EntTypes[i].eEntType;
			break;
		}
	}

	return eEntType;
}


QString	CEntity::RoleName( CEntRole eEntRole )
{
	int				i, nEntRoles;
	QString			sEntRole = "Temp";

	nEntRoles = sizeof( EntRoles ) / sizeof( CEntRoleEntry );

	for( i = 0; i < nEntRoles; i++ )
	{
		if( EntRoles[i].eEntRole == eEntRole )
		{
			sEntRole = EntRoles[i].sEntRole;
			break;
		}
	}

	return sEntRole;
}


CEntRole CEntity::RoleByName( const QString& sEntRole )
{
	int				i, nEntRoles;
	CEntRole		eEntRole = ENT_ROLE_NONE;

	nEntRoles = sizeof( EntRoles ) / sizeof( CEntRoleEntry );

	for( i = 0; i < nEntRoles; i++ )
	{
		if( EntRoles[i].sEntRole == sEntRole )
		{
			eEntRole = EntRoles[i].eEntRole;
			break;
		}
	}

	return eEntRole;
}


QString CEntity::DispModeName( CDisplayMode eDispMode )
{
	int				i, nDispModes;
	QString			sDispMode = "" ;

	nDispModes = sizeof( DispModes ) / sizeof( CDispModeEntry );

	for( i = 0; i < nDispModes; i++ )
	{
		if( DispModes[i].eDispMode == eDispMode )
		{
			sDispMode = DispModes[i].sDispMode;
			break;
		}
	}

	return sDispMode;
}


CDisplayMode CEntity::DispModeByName( const QString& sDispMode )
{
	int				i, nDispModes;
	CDisplayMode	eDispMode = DISP_MODE_NONE;

	nDispModes = sizeof( DispModes ) / sizeof( CDispModeEntry );

	for( i = 0; i < nDispModes; i++ )
	{
		if( DispModes[i].sDispMode == sDispMode )
		{
			eDispMode = DispModes[i].eDispMode;
			break;
		}
	}

	return eDispMode;
}

void CEntity::SetValidateStatus
(
	CEntValidateStatus vStatus, 
	QString const& message
) 
{
	m_validateStatus=vStatus;
	m_sValidateMessage=message;
}

int	CEntity::GetDesignTime()
{
	return m_designTime;
}

void CEntity::SetDesignTime(int designTime)
{
	m_designTime = designTime;
}
