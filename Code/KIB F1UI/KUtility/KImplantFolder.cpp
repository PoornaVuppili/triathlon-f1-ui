#include "KImplantFolder.h"

#include "NewImplant.h"

bool KCreateImplantFolder(const QString& basePath, QList<QPair<QString, QString> >& result, QWidget* parent)
{
    // prompt the user to enter the implant number and information
    NewImplant ni(basePath, parent);
    if( ni.exec()== QDialog::Rejected )
        return false;

    // put the information in the 'result'
    result.clear();
    result.append( QPair<QString, QString>("ImplantPath", ni.GetImplantName()) );
    result.append( QPair<QString, QString>("ImplantType", ni.GetImplantType()) );
    result.append( QPair<QString, QString>("Knee", ni.GetKneeType()) );

    QString str = ni.GetCompartmentType();
    if( !str.isEmpty() )
        result.append( QPair<QString, QString>("Compartment", str) );

    str = ni.GetCountryCode();
    if( !str.isEmpty() )
        result.append( QPair<QString, QString>("Country", str) );

    str = ni.GetZone();
    if( !str.isEmpty() )
        result.append( QPair<QString, QString>("Zone", str) );

    return true;
}

#include <vector>
#include <string>
#include <algorithm>
#include <Windows.h>

QStringList KGetSubdirListOf(QString const &folder)
{
#if 0
	QStringList implants = uniDir.entryList(QDir::Dirs, QDir::Name);
#else
	std::vector<std::string> myDirList;
	WIN32_FIND_DATA ffd;
	HANDLE fh = FindFirstFile((folder + "\\*").toUtf8(), &ffd);
	if (fh == INVALID_HANDLE_VALUE)
		return QStringList();

	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			myDirList.push_back(ffd.cFileName);
	}
	while (FindNextFile(fh, &ffd) != 0);

	FindClose(fh);

	std::sort(myDirList.begin(), myDirList.end());

	QStringList implants;
	for (int i = 0, n = myDirList.size(); i < n; ++i)
		implants.push_back(myDirList[i].c_str());
#endif
	return implants;
}
