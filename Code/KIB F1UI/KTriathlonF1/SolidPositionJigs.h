#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW CSolidPositionJigs : public CPart
{
public:
	CSolidPositionJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_SOLID_POSITION_JIGS, const QString& sFileName="", int nEntId=-1 );
	~CSolidPositionJigs();

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );

private:
	void			SetSPJigsModifiedFlag( bool bModified );

private:
	bool			m_bSPJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bSPJigsDataModified;// Geometric entities have not been saved yet to the file. 

};