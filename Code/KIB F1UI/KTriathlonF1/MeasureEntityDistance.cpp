#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "SelectedEntity.h"
#include "MeasureEntityDistance.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"

///////////////////////////////////////////////////////////////////////////////////////////
// Methods for CMeasureEntityDistance
///////////////////////////////////////////////////////////////////////////////////////////
CMeasureEntityDistance::CMeasureEntityDistance( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMeasureEntityDistance";

	m_toolbar = new QToolBar("Measure Distance");

	m_textMinMax = new QLineEdit(tr(" "));
	m_textMinMax->setReadOnly(true);
	m_textMinMax->setFixedWidth(32);
	m_textMinMax->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMinMax);

	QLabel* labelResultDistance = new QLabel(tr(" Distance:"));
	labelResultDistance->setFont(m_fontBold);
	m_toolbar->addWidget(labelResultDistance);

	m_textMeasuredDistance = new QLineEdit(tr(" "));
	m_textMeasuredDistance->setReadOnly(true);
	m_textMeasuredDistance->setFixedWidth(84);
	m_textMeasuredDistance->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMeasuredDistance);

	m_checkPointVertex = new QCheckBox("Pnt/Vertex");
	m_checkPointVertex->setCheckState(Qt::Checked);
	m_checkPointVertex->setFont(m_fontBold);
	m_toolbar->addWidget(m_checkPointVertex);

	m_checkCurveEdge = new QCheckBox("Crv/Edge");
	m_checkCurveEdge->setCheckState(Qt::Checked);
	m_checkCurveEdge->setFont(m_fontBold);
	m_toolbar->addWidget(m_checkCurveEdge);

	m_checkSurfaceFace = new QCheckBox("Surf/Face");
	m_checkSurfaceFace->setCheckState(Qt::Checked);
	m_checkSurfaceFace->setFont(m_fontBold);
	m_toolbar->addWidget(m_checkSurfaceFace);

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	m_checkLocalResult = new QCheckBox("Local Result");
	m_checkLocalResult->setCheckState(Qt::Checked);
	m_checkLocalResult->setFont(m_fontBold);
	m_toolbar->addWidget(m_checkLocalResult);

	m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	m_mouseLeftDown = false;
	m_firstHitEntity.Empty();
	m_secondHitEntity.Empty();

	m_glEdgeDisplayList = 0;
	m_bEdgeModified = false;
	m_prevCursorClicked = QPoint(-1, -1);
	m_prevCurveClicked = NULL;
	m_prevFaceClicked = NULL;

}

CMeasureEntityDistance::~CMeasureEntityDistance()
{
}

bool CMeasureEntityDistance::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseLeftDown = true;

	m_selectableBreps.RemoveAll();
	m_selectableBrepsAreOpaque.RemoveAll();
	m_selectableCurves.RemoveAll();
	m_selectablePoints.RemoveAll();

	// whenever left button down, always update the selectable entities.
	if (CTotalDoc::GetTotalDoc())
		CTotalDoc::GetTotalDoc()->GetSelectableEntities(m_selectableBreps, m_selectableBrepsAreOpaque, m_selectableCurves, m_selectablePoints);

	// select the entities
	if (m_firstHitEntity.IsEmpty() && m_secondHitEntity.IsEmpty())
	{
		m_prevCursorClicked = QPoint(-1, -1);
		m_prevCurveClicked = NULL;
		m_prevFaceClicked = NULL;
		SelectEntities(cursor, m_firstHitEntity);
	}
	else if ( !m_firstHitEntity.IsEmpty() && !m_secondHitEntity.IsEmpty() )
	{
		m_firstHitEntity.Empty();
		m_secondHitEntity.Empty();
		m_textMinMax->clear();
		m_textMeasuredDistance->clear();
		m_prevCursorClicked = QPoint(-1, -1);
		m_prevCurveClicked = NULL;
		m_prevFaceClicked = NULL;

		SelectEntities(cursor, m_firstHitEntity);
	}
	else if ( !m_firstHitEntity.IsEmpty() && m_secondHitEntity.IsEmpty() )
	{
		SelectEntities(cursor, m_secondHitEntity, &m_firstHitEntity);
		if ( !m_secondHitEntity.IsEmpty() )
		{
			bool bothArePoints = (m_secondHitEntity.GetPoint().IsInitialized() &&
								  m_firstHitEntity.GetPoint().IsInitialized());
			if ( !bothArePoints && m_secondHitEntity.IsIdentical(m_firstHitEntity) )
			{
				m_secondHitEntity.Empty();
			}
			else
			{
				// distance calculation is time consuming. We here update screen first.
				m_pView->Redraw();
				// determine the distance
				QApplication::setOverrideCursor( Qt::WaitCursor );
				QString distText, minMaxText;
				bool isMinDist = true;
				bool localResult = (m_checkLocalResult->checkState() == Qt::Checked);
				double dist = m_firstHitEntity.DistanceBetween(m_secondHitEntity, &isMinDist, localResult);
				distText.setNum(dist, 'f', 3);
				if (isMinDist)
					minMaxText = QString("Min");
				else
					minMaxText = QString("Max");
				m_textMinMax->clear();
				m_textMeasuredDistance->clear();
				if (dist < 0)
					m_textMeasuredDistance->setText("No solution");
				else
				{
					m_textMinMax->setText(minMaxText);
					m_textMeasuredDistance->setText(distText);
				}
				QApplication::restoreOverrideCursor();
			}
		}
	}
	else
	{
		m_firstHitEntity.Empty();
		m_secondHitEntity.Empty();
		m_prevCursorClicked = QPoint(-1, -1);
		m_prevCurveClicked = NULL;
		m_prevFaceClicked = NULL;
	}
	
	m_bEdgeModified = true;

	m_pView->Redraw();

	return true;
}

bool CMeasureEntityDistance::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{	
	m_prevCursorClicked = QPoint(-1, -1);
	m_prevCurveClicked = NULL;
	m_prevFaceClicked = NULL;

	m_pView->Redraw();

	return true;
}
bool CMeasureEntityDistance::MouseUp( const QPoint& cursor, Viewport* vp )
{

	m_pView->Redraw();


	return true;
}


bool CMeasureEntityDistance::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{

	m_pView->Redraw();

	return true;
}


bool CMeasureEntityDistance::MouseRight( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{

	m_pView->Redraw();
	return true;
}

bool CMeasureEntityDistance::keyPressEvent( QKeyEvent* event, Viewport* viewport )
{
	if ( event->key() == Qt::Key_Escape )
		Reject();

	return false; // false: do not execute subsequent actions.
}

void CMeasureEntityDistance::Display(Viewport* vp)
{		
	// determine one pixel size
	int UV0[2];
	UV0[0] = 0;
	UV0[1] = 0;
	int UV5[2];
	UV5[0] = 1;
	UV5[1] = 1;
	IwPoint3d cpt0, cpt1, cpt;
	m_pView->UVtoXYZ(UV0, cpt0);
	m_pView->UVtoXYZ(UV5, cpt1);
	cpt = cpt1 - cpt0;
	double onePixelSize = cpt.Length();
	// determine viewVec offset 
	CBounds3d bounds = ((CTotalView*)m_pView)->GetDocBounds();
	double d = dist( bounds.pt0, bounds.pt1 );
	IwVector3d viewVec = 0.3*d*m_pView->GetViewingVector();

	glDrawBuffer( GL_BACK );

	glDisable( GL_LIGHTING );

	// Display closest points
	glPointSize(5);
	glLineWidth(3.0);

	glColor3ub( 255, 0, 0 );
	if ( m_firstHitEntity.IsClosestPointValid() )
		m_firstHitEntity.DisplayClosestPoint();

	if ( m_secondHitEntity.IsClosestPointValid() )
		m_secondHitEntity.DisplayClosestPoint();

	// Display Entities
	glColor3ub( 255, 255, 0 );

	if( m_bEdgeModified && m_glEdgeDisplayList > 0 )
	{
		if( glIsList( m_glEdgeDisplayList ) )
			glDeleteLists( m_glEdgeDisplayList, 1 );

		m_glEdgeDisplayList = 0;
	}

	if( m_glEdgeDisplayList == 0 )
	{
		QApplication::setOverrideCursor( Qt::WaitCursor );

		m_glEdgeDisplayList = m_pDoc->GetAvlDisplayList();
		glNewList( m_glEdgeDisplayList, GL_COMPILE_AND_EXECUTE );

		if ( !m_firstHitEntity.IsEmpty() )
		{
			m_firstHitEntity.Display(IwVector3d(0,0,0), onePixelSize); // since within display list, dot not offset view vector
		}

		if ( !m_secondHitEntity.IsEmpty() )
		{
			m_secondHitEntity.Display(IwVector3d(0,0,0), onePixelSize); // since within display list, dot not offset view vector
		}
		
		glEndList();

		QApplication::restoreOverrideCursor();
	}
	else
	{
		glCallList( m_glEdgeDisplayList );
	}

	// Display coordinates of points
	glColor3ub( 0, 0, 0 );
	if ( !m_firstHitEntity.IsEmpty() )
	{		
		if (m_firstHitEntity.GetPoint().IsInitialized() )
		{
			IwPoint3d pnt = m_firstHitEntity.GetPoint();
			QString totalStr, posX, posY, posZ;
			posX.setNum(pnt.x, 'f', 3);
			posY.setNum(pnt.y, 'f', 3);
			posZ.setNum(pnt.z, 'f', 3);
			totalStr = QString(" (%1,%2,%3)").arg(posX).arg(posY).arg(posZ);
			QFont font( "Tahoma", 12);
			//font.setBold(true);
			GetViewWidget()->renderText(pnt.x-viewVec.x, pnt.y-viewVec.y, pnt.z-viewVec.z, totalStr, font);
		}
	}

	if ( !m_secondHitEntity.IsEmpty() )
	{
		if (m_secondHitEntity.GetPoint().IsInitialized() )
		{
			IwPoint3d pnt = m_secondHitEntity.GetPoint();
			QString totalStr, posX, posY, posZ;
			posX.setNum(pnt.x, 'f', 3);
			posY.setNum(pnt.y, 'f', 3);
			posZ.setNum(pnt.z, 'f', 3);
			totalStr = QString(" (%1,%2,%3)").arg(posX).arg(posY).arg(posZ);
			QFont font( "Tahoma", 12);
			//font.setBold(true);
			GetViewWidget()->renderText(pnt.x-viewVec.x, pnt.y-viewVec.y, pnt.z-viewVec.z, totalStr, font);
		}
	}

	m_bEdgeModified = false;

	glPointSize(1);
	glLineWidth(1.0);

	glEnable( GL_LIGHTING );
}

void CMeasureEntityDistance::OnAccept()
{
	m_bDestroyMe = true;
	m_pView->Redraw();
}

void CMeasureEntityDistance::SelectEntities
(
	const QPoint&		cursor,				// I: cursor position 
	CSelectedEntity&	hitEntity,			// O:
	CSelectedEntity*	exclusiveEntity		// I: the entity to avoid (has been selected before).
)
{

	// Get viewing vector from eyes to screen
	IwVector3d viewVec = m_pView->GetViewingVector();
	// determine the selection tolerance - 5 pixels
	int UV0[2];
	UV0[0] = cursor.x();
	UV0[1] = cursor.y();
	int UV5[2];
	UV5[0] = cursor.x()+5;
	UV5[1] = cursor.y();
	IwPoint3d cpt0, cpt5, cpt;
	m_pView->UVtoXYZ(UV0, cpt0);
	m_pView->UVtoXYZ(UV5, cpt5);
	cpt = cpt5 - cpt0;
	double sTol = cpt.Length();
	IwPoint3d viewPoint = (IwPoint3d)cpt0;

	if (m_checkPointVertex->checkState() == Qt::Checked)
	{
		IwPoint3d pnt;
		IwPoint3d* exclusivePoint = NULL;
		if (exclusiveEntity)
		{
			pnt = exclusiveEntity->GetPoint();
			if ( pnt.IsInitialized() )
				exclusivePoint = &pnt;
		}
		// find the hit points and vertexes
		// if found one, just return.
		IwPoint3d pointHitPoint;
		double pointHeight;
		bool bPntHit = CMeasureEntityDistance::HitPoints(m_selectablePoints, viewPoint, viewVec, sTol, pointHitPoint, pointHeight, exclusivePoint);
		IwPoint3d vertexHitPoint;
		double vertexHeight;
		bool bVerHit = CMeasureEntityDistance::HitVertexes(m_selectableBreps, viewPoint, viewVec, sTol, vertexHitPoint, vertexHeight, exclusivePoint);
		if (bPntHit && bVerHit)
		{
			if (pointHeight<vertexHeight)
				hitEntity.AddPoint(pointHitPoint);
			else
				hitEntity.AddPoint(vertexHitPoint);
			
			return;
		}
		else if (bPntHit)
		{
			hitEntity.AddPoint(pointHitPoint);
			return;
		}
		else if (bVerHit)
		{
			hitEntity.AddPoint(vertexHitPoint);
			return;
		}
	}
	if (m_checkCurveEdge->checkState() == Qt::Checked)
	{
		IwCurve* exclusiveCurve = NULL;
		IwEdge* exclusiveEdge = NULL;
		if (exclusiveEntity)
		{

			bool curveOnly = true;
			exclusiveCurve = exclusiveEntity->GetCurve(curveOnly);
			exclusiveEdge = exclusiveEntity->GetEdge();
		}
		// find the hit curves and edges
		IwCurve* curveHitCurve = NULL;
		IwPoint3d curveHitPoint;
		double curveHitParam;
		double curveHeight;
		bool bCrvHit = CMeasureEntityDistance::HitCurves(m_selectableCurves, viewPoint, viewVec, sTol, curveHitCurve, curveHitPoint, curveHitParam, curveHeight, exclusiveCurve);
		IwEdge* edgeHitEdge = NULL;
		IwPoint3d edgeHitPoint;
		double edgeHitParam;
		double edgeHeight;
		bool bEdgeHit = CMeasureEntityDistance::HitEdges(m_selectableBreps, viewPoint, viewVec, sTol, edgeHitEdge, edgeHitPoint, edgeHitParam, edgeHeight, exclusiveEdge);
		if (bCrvHit && bEdgeHit)
		{
			if (curveHeight<edgeHeight)
				hitEntity.AddCurve(curveHitCurve, curveHitPoint, curveHitParam);
			else
				hitEntity.AddEdge(edgeHitEdge, edgeHitPoint, edgeHitParam);
			
			// if found one, return.
			return;
		}
		else if (bCrvHit)
		{
			hitEntity.AddCurve(curveHitCurve, curveHitPoint, curveHitParam);
			return;
		}
		else if (bEdgeHit)
		{
			hitEntity.AddEdge(edgeHitEdge, edgeHitPoint, edgeHitParam);
			return;
		}
	}
	if (m_checkSurfaceFace->checkState() == Qt::Checked)
	{
		IwFace* exclusiveFace = NULL;
		if (exclusiveEntity)
		{
			exclusiveFace = exclusiveEntity->GetFace();
		}
		// find the hit faces
		IwFace* hitFace;
		IwPoint3d hitPoint;
		IwPoint2d hitParam;
		double faceHeight;
		bool bFaceHit = CMeasureEntityDistance::HitFaces(m_selectableBreps, viewPoint, viewVec, sTol, hitFace, hitPoint, hitParam, faceHeight, exclusiveFace);

		// if found one, return.
		if (bFaceHit)
		{
			hitEntity.AddFace(hitFace, hitPoint, hitParam);
			return;
		}
	}

	// if all unchecked, find the first intersection point with cursor extension line
	if (m_checkPointVertex->checkState() == Qt::Unchecked &&
		m_checkCurveEdge->checkState() == Qt::Unchecked &&
		m_checkSurfaceFace->checkState() == Qt::Unchecked)
	{
		// find the hit curves
		IwCurve* curveHitCurve = NULL;
		IwPoint3d curveHitPoint;
		double curveHitParam;
		double curveHeight;
		IwCurve *excEntCrv = NULL;

		// if the cursor clicks on the same place twice, then the 2nd hit point will be on the 2nd entity.
		if ( cursor == m_prevCursorClicked )
			excEntCrv = m_prevCurveClicked;

		bool bCrvHit = CMeasureEntityDistance::HitCurves(m_selectableCurves, viewPoint, viewVec, sTol, curveHitCurve, curveHitPoint, curveHitParam, curveHeight, excEntCrv);
		
		// find the hit faces
		IwFace* hitFace;
		IwPoint3d hitPoint;
		IwPoint2d hitParam;
		double faceHeight;
		IwFace *excEntFace = NULL;

		// if the cursor clicks on the same place twice, then the 2nd hit point will be on the 2nd entity.
		if ( cursor == m_prevCursorClicked )
			excEntFace = m_prevFaceClicked;

		bool bFaceHit = CMeasureEntityDistance::HitFaces(m_selectableBreps, viewPoint, viewVec, sTol, hitFace, hitPoint, hitParam, faceHeight, excEntFace);

		if (bCrvHit && bFaceHit)
		{
			if (curveHeight<faceHeight)
			{
				// Set the hitpoint
				hitEntity.AddPoint(curveHitPoint);
				// set the hit entity for hitpoint only
				m_prevCurveClicked = curveHitCurve;
				m_prevFaceClicked = NULL;
			}
			else
			{
				// set the hitpoint
				hitEntity.AddPoint(hitPoint);	
				// set the hit entity for hitpoint only
				m_prevCurveClicked = NULL;
				m_prevFaceClicked = hitFace;
			}

			m_prevCursorClicked = cursor;
			// if found one, return.
			return;
		}
		else if (bCrvHit)
		{
			// set the hitpoint
			hitEntity.AddPoint(curveHitPoint);
			// set the hit entity for hitpoint only
			m_prevCurveClicked = curveHitCurve;
			m_prevFaceClicked = NULL;

			m_prevCursorClicked = cursor;
			return;
		}
		else if (bFaceHit)
		{
			// then set the hitpoint
			hitEntity.AddPoint(hitPoint);
			// set the hit entity for hitpoint only
			m_prevCurveClicked = NULL;
			m_prevFaceClicked = hitFace;

			m_prevCursorClicked = cursor;
			return;
		}

	}

}

bool CMeasureEntityDistance::HitPoints
(
	IwTArray<IwPoint3d>& pointList, 
	IwPoint3d& origin, 
	IwVector3d& dir, 
	double tol,
	IwPoint3d& hitPoint,			// O: the hit point
	double& height,					// O: a reference value to indicate how close to the viewer. smaller the closer.
	IwPoint3d* exclusivePoint		// I: point to avoid
)
{
	bool hit = false;

	if (pointList.GetSize() == 0) return hit;

	// unitize the searching vector
	IwVector3d dirVec = dir;
	dirVec.Unitize();

	IwPoint3d pnt;
	IwVector3d vec, cross;
	double dist, dot, minDot = 1000000000;
	for (unsigned i=0; i<pointList.GetSize(); i++)
	{
		pnt = pointList.GetAt(i);
		if (exclusivePoint && (*exclusivePoint)==pnt)
			continue;
		vec = pnt - origin;
		cross = dirVec*vec;
		dist = cross.Length();
		if (dist < tol) // the dist is less then the tol
		{
			dot = dirVec.Dot(vec);
			if (dot < minDot) // get the point that is the closest to the viewers.
			{
				hitPoint = pnt;
				height = dot;
				minDot = dot;
			}
		}
	}

	if (minDot < 1000000000)
		hit = true;

	return hit;
}

bool CMeasureEntityDistance::HitVertexes
(
	IwTArray<IwBrep*>& brepList, 
	IwPoint3d& origin, 
	IwVector3d& dir, 
	double tol, 
	IwPoint3d& hitVertex,		// O:
	double& height,				// O:
	IwPoint3d* exclusivePoint	// I: point to avoid
)
{
	bool hit = false;

	if (brepList.GetSize() == 0) return hit;

	IwTArray<IwPoint3d> pointList;
	IwBrep* brep;
	IwTArray<IwVertex*> vertexes;
	IwVertex* vertex;
	IwPoint3d pnt;
	for (unsigned i=0; i<brepList.GetSize(); i++)
	{
		brep = brepList.GetAt(i);
		brep->GetVertices(vertexes);
		for (unsigned j=0; j<vertexes.GetSize(); j++)
		{
			vertex = vertexes.GetAt(j);
			pnt = vertex->GetPoint();
			pointList.Add(pnt);
		}
	}

	hit = CMeasureEntityDistance::HitPoints(pointList, origin, dir, tol, hitVertex, height, exclusivePoint);

	return hit;
}

bool CMeasureEntityDistance::HitCurves
(
	IwTArray<IwCurve*>& curveList, 
	IwPoint3d& origin, 
	IwVector3d& dir, 
	double tol, 
	IwCurve*& hitCurve,				// O:
	IwPoint3d& hitPoint,			// O:
	double& hitParam,				// O:
	double& height,					// O:
	IwCurve* exclusiveCurve			// I: curve to avoid
)
{
	IwContext& iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	bool hit = false;

	if (curveList.GetSize() == 0) return hit;

	// unitize the searching vector
	IwVector3d dirVec = dir;
	dirVec.Unitize();
	IwPoint3d eyePnt = origin - 10000*dirVec;

	IwCurve* crv;
	IwPoint3d pnt;
	IwVector3d vec, cross;
	double dot, minDot = 1000000000;
	double twoTol = 2*tol;
	IwExtent3d bBox;
	IwLine* hitLine = NULL;
	IwLine::CreateCanonical(iwContext, origin, dirVec, hitLine);

	for (unsigned i=0; i<curveList.GetSize(); i++)
	{
		crv = curveList.GetAt(i);
		if ( !crv ) continue;
		if (exclusiveCurve && exclusiveCurve == crv) 
			continue;
		// Curve's bounding box needs to intersects with cursor extension line, 
		// otherwise the curve is too far away to be picked. 
		crv->CalculateBoundingBox(crv->GetNaturalInterval(), &bBox);
		bBox.ExpandAbsolute(10*tol);
		if ( bBox.IntersectsLine3d(eyePnt, dirVec) )
		{
			IwSolutionArray sols;
			crv->GlobalCurveSolve(crv->GetNaturalInterval(), *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			if (sols.GetSize() > 0)
			{
				for (unsigned j=0; j<sols.GetSize(); j++)
				{
					IwSolution sol = sols[j];
					if (sol.m_vStart.m_dSolutionValue < tol)
					{
						crv->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt);
						vec = pnt - origin;
						dot = dirVec.Dot(vec);
						if (dot < minDot)
						{
							hitCurve = crv;
							hitPoint = pnt;
							hitParam = sol.m_vStart.m_adParameters[0];
							height = dot;
							minDot = dot;
						}
					}
				}
			}
		}
	}

	IwObjDelete delLine(hitLine);

	if (minDot < 1000000000)
		hit = true;

	return hit;
}


bool CMeasureEntityDistance::HitEdges
(
	IwTArray<IwBrep*>& brepList, 
	IwPoint3d& origin, 
	IwVector3d& dir, 
	double tol, 
	IwEdge*& hitEdge,				// O:
	IwPoint3d& hitPoint,			// O:
	double& hitParam,				// O:
	double& height,					// O:
	IwEdge* exclusiveEdge			// I:
)
{
	IwContext& iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	bool hit = false;

	// collect all the edges 
	if (brepList.GetSize() == 0) return hit;

	IwTArray<IwEdge*> allEdgeList;
	IwTArray<IwEdge*> edgeList;
	IwBrep* brep;
	for (unsigned i=0; i<brepList.GetSize(); i++)
	{
		brep = brepList.GetAt(i);
		brep->GetEdges(edgeList);
		allEdgeList.Append(edgeList);
	}

	// Search which edge is hit

	if (allEdgeList.GetSize() == 0) return hit;

	// unitize the searching vector
	IwVector3d dirVec = dir;
	dirVec.Unitize();
	IwPoint3d eyePnt = origin - 200*dirVec;

	IwCurve* crv;
	IwEdge* edge;
	IwPoint3d pnt;
	IwVector3d vec, cross;
	double dot, minDot = 1000000000;
	double twoTol = 2*tol;
	IwExtent3d bBox;
	IwLine* hitLine = NULL;
	IwLine::CreateCanonical(iwContext, origin, dirVec, hitLine);

	for (unsigned i=0; i<allEdgeList.GetSize(); i++)
	{
		edge = allEdgeList.GetAt(i);
		if (exclusiveEdge && exclusiveEdge == edge)
			continue;
		crv = edge->GetCurve();
		// Edge's bounding box needs to intersects with cursor extension line, 
		// otherwise the edge is too far away to be picked. 
		edge->CalculateBoundingBox(&bBox);
		bBox.ExpandAbsolute(10*tol);
		if ( bBox.IntersectsLine3d(eyePnt, dirVec) )
		{
			IwSolutionArray sols;
			crv->GlobalCurveSolve(edge->GetInterval(), *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			if (sols.GetSize() > 0)
			{
				for (unsigned j=0; j<sols.GetSize(); j++)
				{
					IwSolution sol = sols[j];
					if (sol.m_vStart.m_dSolutionValue < tol)
					{
						crv->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt);
						vec = pnt - origin;
						dot = dirVec.Dot(vec);
						if (dot < minDot)
						{
							hitEdge = edge;
							hitPoint = pnt;
							hitParam = sol.m_vStart.m_adParameters[0];
							height = dot;
							minDot = dot;
						}
					}
				}
			}
		}
	}

	IwObjDelete delLine(hitLine);

	if (minDot < 1000000000)
		hit = true;

	return hit;
}

bool CMeasureEntityDistance::HitFaces
(
	IwTArray<IwBrep*>& brepList, 
	IwPoint3d& origin, 
	IwVector3d& dir, 
	double tol, 
	IwFace*& hitFace,				// O:
	IwPoint3d& hitPoint,			// O:
	IwPoint2d& hitParam,			// O:
	double& height,					// O:
	IwFace* exclusiveFace			// I: face to avoid
)
{
	IwContext& iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	bool hit = false;

	// collect all the edges 
	if (brepList.GetSize() == 0) return hit;

	IwTArray<IwFace*> allFaceList;
	IwTArray<IwFace*> faceList;
	IwBrep* brep;
	for (unsigned i=0; i<brepList.GetSize(); i++)
	{
		brep = brepList.GetAt(i);
		brep->GetFaces(faceList);
		allFaceList.Append(faceList);
	}

	// Search which face is hit

	if (allFaceList.GetSize() == 0) return hit;

	// unitize the searching vector
	IwVector3d dirVec = dir;
	dirVec.Unitize();
	IwPoint3d eyePnt = origin - 200*dirVec;

	IwSurface* surf;
	IwFace* face;
	IwPoint3d pnt;
	IwVector3d vec, cross;
	double dot, minDot = 1000000000;
	double twoTol = 2*tol;
	IwExtent3d bBox;
	IwLine* hitLine = NULL;
	IwLine::CreateCanonical(iwContext, origin, dirVec, hitLine);

	for (unsigned i=0; i<allFaceList.GetSize(); i++)
	{
		face = allFaceList.GetAt(i);
		if (exclusiveFace && exclusiveFace==face)
			continue;
		surf = face->GetSurface();
		// face's bounding box needs to intersects with cursor extension line, 
		// otherwise the face is too far away to be picked. 
		face->CalculateBoundingBox(bBox);
		bBox.ExpandAbsolute(10*tol);
		if ( bBox.IntersectsLine3d(eyePnt, dirVec) )
		{
			IwSolutionArray sols;
			surf->GlobalCurveSolve(surf->GetNaturalUVDomain(), *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			if (sols.GetSize() > 0)
			{
				for (unsigned j=0; j<sols.GetSize(); j++)
				{
					IwSolution sol = sols[j];
					if (sol.m_vStart.m_dSolutionValue < tol)
					{
						IwVector2d hitUV(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
						IwPointClassification pClass;
						face->PointClassify(hitUV, FALSE, TRUE, pClass);
						if (pClass.GetPointClass() == IW_PC_FACE)
						{
							surf->EvaluatePoint(hitUV, pnt);
							vec = pnt - origin;
							dot = dirVec.Dot(vec);
							if (dot < minDot)
							{
								hitFace = face;
								hitPoint = pnt;
								hitParam = IwPoint2d(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
								height = dot;
								minDot = dot;
							}
						}
					}
				}
			}
		}
	}

	IwObjDelete delLine(hitLine);

	if (minDot < 1000000000)
		hit = true;

	return hit;
}

bool CMeasureEntityDistance::GetPoints(double &x1, double &y1, double &z1, double &x2, double &y2, double &z2)
{
	bool hasPoint1 = false;
	bool hasPoint2 = false;

	if ( !m_firstHitEntity.IsEmpty() )
	{		
		if (m_firstHitEntity.GetPoint().IsInitialized() )
		{
			IwPoint3d pnt = m_firstHitEntity.GetPoint();
			x1 = pnt.x;
			y1 = pnt.y;
			z1 = pnt.z;
			hasPoint1 = true;
		}
	}

	if ( !m_secondHitEntity.IsEmpty() )
	{
		if (m_secondHitEntity.GetPoint().IsInitialized() )
		{
			IwPoint3d pnt = m_secondHitEntity.GetPoint();
			x2 = pnt.x;
			y2 = pnt.y;
			z2 = pnt.z;
			hasPoint2 = true;
		}
	}

	return (hasPoint1 && hasPoint2);
}