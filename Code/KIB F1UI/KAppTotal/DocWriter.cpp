#include "TriathlonF1Definitions.h"
#include "DocWriter.h"
#include "DataDoc.h"
#include "OpenGLView.h"
#include "MainWindow.h"
#include "Part.h"
#include "Globals.h"

#include <QFile>

CDocWriter::CDocWriter( CDataDoc* pDoc, const QString& sDocFolder )
{
	m_pDoc			= pDoc;
	m_sDocFolder	= sDocFolder;
}


CDocWriter::~CDocWriter()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////
// The pEntity->Save() will be called between startEntToSave and endEntToSave
///////////////////////////////////////////////////////////////////////////////////////////////
bool CDocWriter::SaveFile(bool incrementalSave, QString& CRPS, CEntRole startEntToSave, CEntRole endEntToSave)
{
	m_pDoc->AppendLog( QString("CDocWriter::SaveFile, incrementalSave=%1").arg(incrementalSave));

	QString			sBrepFileName, sTessFileName;
	QString			sLine, sEntRole, sEntType, sFileName, sPartType, sDisplayMode;
	QString			sDocPath, sDocName;
	bool			bOK;
	CEntity*		pEntity;
	CPart*			pPart;
	int				i, nEnts;
	CEntRole		eEntRole;
	CEntType		eEntType;
	CPartType		ePartType;
	CDisplayMode	eDisplayMode;
	CColor			color;
	int				nEntStatus;
	int				nValidateStatus;
	QString			sValidateMessage;
	int				nEntId;
	double			designTime;
	QString			sImplantSide, sImplantZone, sPatientId;

	QDir			dir( m_sDocFolder );

	/*QString strFemurFolder = m_sDocFolder;

	int femFolderidx = -1;
	femFolderidx = strFemurFolder.indexOf("SolidWorks", 0);

	strFemurFolder = strFemurFolder.left(femFolderidx  + 1 + 15);

	QDir dirFemur(strFemurFolder);

	if(!dirFemur.exists())
	{
		bool bOK = dirFemur.mkdir( strFemurFolder );
	}*/

	if( !dir.exists() )
	{
		bool bOK = dir.mkdir( m_sDocFolder );

		if( !bOK )
		{
			Msg( "Cannot save the document. Please check permissions." );
			m_pDoc->AppendLog( QString("CDocWriter::SaveFile, Cannot save the document."));
			return false;
		}
	}

	sDocPath = m_sDocFolder + "\\header.txt~";

	QFile			File( sDocPath );
	QTextStream		out( &File );

	bOK = File.open( QIODevice::WriteOnly | QIODevice::Text );

	if( !bOK )
	{
		m_pDoc->AppendLog( QString("CDocWriter::SaveFile, File.open() fail."));
		return false;
	}

	if ( CRPS == QString("CR") )
		out << "TriathlonF1 header file\n";
	else if ( CRPS == QString("PS") )
		out << "TriathlonF1PS header file\n";
	else if ( CRPS == QString("MS") )
		out << "TriathlonF1MS header file\n";
	else
		out << "TriathlonF1 header file\n";

	char *underPStoCR = getenv("ITOTAL_PSToCR");
	
	int				nBeta = m_pDoc->isProductionSoftware() ? 0 : 1;
	QString			version;
	if (nBeta) 
		if ( underPStoCR!= NULL && QString("1") == underPStoCR)
			version = QString("BetaPSToCR");
		else
			version = QString("Beta");
	else 
		version = QString("Prod");
	int				nRel = ITOTAL_RELEASE;
	int				nRev = ITOTAL_REVISION;
	int				nDataFileVersion = ITOTAL_DATAFILE_VERSION;
	QString			appVersion = m_pDoc->GetView()->GetMainWindow()->GetAppVersion();
	QString			sDate = QDateTime::currentDateTime().toString(Qt::ISODate);
	QString			versionInfo = QString( " v%1.%2.%3.%4 %5 %6." ).arg( nRel ).arg( nRev ).arg( nDataFileVersion ).arg( appVersion ).arg(version).arg( sDate );
	double			totalDesignTime = m_pDoc->GetTotalDesignTime()/997.0;// simple way to code/decode
	QString			totalDesignTimeStr = QString("%1").arg(totalDesignTime, 0, 'f', 6);
	QString			currentUserInfo = QString( "%1 at %2%3:%4S\n").arg( m_pDoc->GetCurrentUserName()).arg( m_pDoc->GetCurrentComputerName()).arg(versionInfo).arg(totalDesignTimeStr);
	// Whenever save the file, we need to update current user info
	m_pDoc->SetCurrentUserInfo(currentUserInfo);

	out << "\n"; // must keep a newline, otherwise, reader is not able to open the file.

	out << QString( "%1%2\n").arg( "BETAVERSION:", -40).arg( nBeta );
	out << QString( "%1%2\n").arg( "RELEASE:", -40 ).arg( nRel );
	out << QString( "%1%2\n").arg( "REVISION:", -40 ).arg( nRev );
	out << QString( "%1%2\n").arg( "DATAFILEVERSION:", -40 ).arg( nDataFileVersion );
	out << QString( "%1%2\n").arg( "BUILD:", -40 ).arg( "" );// no longer use in iTW6

	out << "\n";

	out << QString( "%1%2\n").arg( "DESIGNSTATUS:", -40 ).arg( -1 ); // obsoleted

	out << "\n";

	// Set back the previous user's information (who ever saved the file.)
	QList<QString> previousUserInfo;
	int prevUserCount = m_pDoc->GetPreviousUserInfo(previousUserInfo);
	for (int i=0; i<prevUserCount; i++)
		out << QString( "%1%2\n").arg( "USER HISTORY:", -40 ).arg( previousUserInfo.at(i) );
	// Set the current user info
	out << QString( "%1%2\n").arg( "USER HISTORY:", -40 ).arg( currentUserInfo );
	out << "\n";

	// Set the patient info
	sPatientId = m_pDoc->GetPatientId();
	m_pDoc->GetImplantSide( sImplantSide );
	CImplantRegulatoryZone implantRegZone = m_pDoc->GetImplantZone();//
	if ( implantRegZone == IMPLANT_ZONE_US_FDA )
		sImplantZone = QString("US_FDA");
	else if ( implantRegZone == IMPLANT_ZONE_CE_MARK )
		sImplantZone = QString("CE_MARK");
	else if ( implantRegZone == IMPLANT_ZONE_NEW_ZONE )
		sImplantZone = QString("NEW_ZONE");
	else
		sImplantZone = QString("UNKNOWN");

	m_pDoc->AppendLog( QString("CDocWriter::SaveFile, sPatientId=%1.").arg(sPatientId) );

	out << QString( "%1%2\n").arg( "PATIENT ID:", -40 ).arg( sPatientId );
	out << QString( "%1%2\n").arg( "IMPLANT SIDE:", -40 ).arg( sImplantSide );
	out << QString( "%1%2\n").arg( "IMPLANT ZONE:", -40 ).arg( sImplantZone );

	// Set the entity info
	nEnts = m_pDoc->GetNumOfEntities();

	for( i = 0; i < nEnts; i++ )
	{
		pEntity = m_pDoc->GetEntity( i );

		nEntId = pEntity->GetEntId();

		nEntStatus = pEntity->GetUpToDateStatus() ? 1 : 0;
		nValidateStatus = (int)pEntity->GetValidateStatus(sValidateMessage);
		// Replace "\n" by "#&"
		sValidateMessage.replace(QString("\n"), QString("#&"), Qt::CaseSensitive);

		eEntRole = pEntity->GetEntRole();
		sEntRole = CEntity::RoleName( eEntRole );

		pEntity->FormFileName();
		sFileName = pEntity->GetFileName();
		
		eEntType = pEntity->GetEntType();
		sEntType = CEntity::EntTypeName( eEntType );

		designTime = pEntity->GetDesignTime()/997.0;// simple way to decode

		if( eEntType == ENT_TYPE_PART )
			{
			pPart = (CPart*) pEntity;
			ePartType = pPart->GetPartType();

			sPartType = "";

			switch( ePartType )
				{
				case PART_TYPE_STL:		
					sPartType = "STL";
					break;

				case PART_TYPE_BREP:
					sPartType = "Brep";
					break;
				}
			}

		eDisplayMode = pEntity->GetDisplayMode();

		sDisplayMode = CEntity::DispModeName( eDisplayMode );

		color = pEntity->GetColor();

		out << endl;

		out << QString( "%1%2\n").arg(		"ENTITY ID:",		-40 ).arg( nEntId );
		out << QString( "%1%2\n").arg(		"ENTITY ROLE:",		-40 ).arg( sEntRole );
		out << QString( "%1%2\n").arg(		"ENTITY TYPE:",		-40 ).arg( sEntType );
		out << QString( "%1%2\n").arg(		"FILE NAME:",		-40 ).arg( sFileName );
		out << QString( "%1%2 %3 %4\n").arg("ENTITY COLOR:",	-40 ).arg( color[0] ).arg( color[1] ).arg( color[2] );
		out << QString( "%1%2\n").arg(		"DISPLAY MODE:",	-40 ).arg( sDisplayMode );
		out << QString( "%1%2\n").arg(		"UPTODATE STATUS:",	-40 ).arg( nEntStatus );
		out << QString( "%1%2\n").arg(		"VALIDATE STATUS:",	-40 ).arg( nValidateStatus );
		out << QString( "%1%2\n").arg(		"VALIDATE MESSAGE:",-40 ).arg( sValidateMessage );
		out << QString( "%1%2\n").arg(		"STATISTIC:",-40 ).arg( designTime, 0, 'f', 6 );//Do not use "Design Time". Users can see it. use "STATISTIC:"
		
		if( eEntType == ENT_TYPE_PART )
			out << QString( "%1%2\n").arg( "PART TYPE:", -40 ).arg( sPartType );
		
		out << "END ENTITY\n";

		if (startEntToSave==endEntToSave) // save everything
		{
			pEntity->Save( m_sDocFolder, incrementalSave );
		}
		else if ( eEntRole >= startEntToSave && eEntRole <= endEntToSave ) // only save entity in between
		{
			pEntity->Save( m_sDocFolder, incrementalSave );
		}
	}

	File.close();

	m_pDoc->AppendLog( QString("CDocWriter::SaveFile, save completely. "));

	return true;
}

bool CDocWriter::FolderExists(QString docFolderName)
{
	QDir			dir( docFolderName );

	return dir.exists();
}



