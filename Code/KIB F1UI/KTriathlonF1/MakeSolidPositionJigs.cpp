#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "MakeSolidPositionJigs.h"
#include "SolidPositionJigs.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "SideSurfaceJigs.h"
#include "OutlineProfileJigs.h"
#include "IFemurImplant.h"
#include "AdvancedControlJigs.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "..\KApp\SwitchBoard.h"

CMakeSolidPositionJigsErrorCodesEntry CMakeSolidPositionJigs::MakeSolidPositionJigsErrorCodes[] = 
{
	{ MAKE_SOLID_POSITION_JIGS_ERROR_CODES_NONE,	"None."	},
	{ MAKE_SOLID_POSITION_JIGS_ERROR_CODES_INPUT_SURFACES_NOT_GOOD,		"Solid position jig can not be created.\nInner, outer, and side surfaces do not define a closed space."	},
	{ MAKE_SOLID_POSITION_JIGS_ERROR_CODES_TOO_THIN,		"Solid position jig is too thin.\nCheck any dips in the outer surface jig."	},
};


CMakeSolidPositionJigs::CMakeSolidPositionJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
	bool activateUI = (manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW);

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->StartTimeLog("MakeSolidPositionJigs");

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMakeSolidPositionJigs";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actDispDistalPlanes = NULL;
	m_displayDistalPlanes = false;
	m_actValidate = NULL;
	m_predefinedViewIndex = 0;

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Make Solid Position Jigs");
		m_actMakeSolidPositionJigs = m_toolbar->addAction("PositionJigs");
		SetActionAsTitle( m_actMakeSolidPositionJigs );

		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );

		m_actDispDistalPlanes = m_toolbar->addAction("Distal");
		m_actDispDistalPlanes->setToolTip("Display distal cut planes");
		EnableAction( m_actDispDistalPlanes, true );

		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );

		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		// There is no "Cancel" for outer surface
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		//m_toolbar->setMovable( false );	
		//m_toolbar->setVisible( true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actDispDistalPlanes, SIGNAL( triggered() ), this, SLOT( OnDisplayDistalPlanes() ) );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
	//	m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );
	}
	else if (m_manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Make Solid Position Jigs");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );

		m_actMakeSolidPositionJigs = m_toolbar->addAction("PositionJigs");
		SetActionAsTitle( m_actMakeSolidPositionJigs );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		m_pMainWindow->connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		m_pMainWindow->connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		m_pMainWindow->connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	CSolidPositionJigs* solidPositionJigs = m_pDoc->GetSolidPositionJigs();
	if ( solidPositionJigs == NULL ) // if no runtime data
	{
		solidPositionJigs = new CSolidPositionJigs( m_pDoc );
		m_pDoc->AddEntity( solidPositionJigs, true );
		OnReset();
	}
	else
	{
		if ( !solidPositionJigs->GetUpToDateStatus() )	
		{
			OnReset();
		}
		m_validateStatus = solidPositionJigs->GetValidateStatus(m_validateMessage);
		if ( manActType == MAN_ACT_TYPE_EDIT && m_validateStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			OnValidate();
		}
		else if ( manActType == MAN_ACT_TYPE_REVIEW && m_validateStatus != VALIDATE_STATUS_GREEN ) // Force to display too thin points for review
		{
			OnValidate();
		}
		else if (manActType == MAN_ACT_TYPE_VALIDATE)
		{
			OnValidate();
		}
		else
		{
			SetValidateIcon(m_validateStatus, m_validateMessage);
		}
	}

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	// If any errors, display error messages.
	DisplayErrorMessages();

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

	m_pView->Redraw();
}

CMakeSolidPositionJigs::~CMakeSolidPositionJigs()
{
	CSolidPositionJigs* solidPositionJigs = m_pDoc->GetSolidPositionJigs();
	if ( solidPositionJigs != NULL )
	{
		int previoisDesignTime = solidPositionJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		solidPositionJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}
	SwitchBoard::removeSignalSender(this);
}

void CMakeSolidPositionJigs::Display(Viewport* vp)
{		
	glDrawBuffer( GL_BACK );

	if ( m_displayDistalPlanes && m_distalCutPlanePoints.GetSize() > 0 )
	{
		glColor4ub( darkGray[0], darkGray[1], darkGray[2], m_pDoc->GetTransparencyFactor());
		glLineWidth(1.0);

		IwVector3d tempVec0, tempVec1, norm;

		// positive cut plane
		tempVec0 = m_distalCutPlanePoints.GetAt(1) - m_distalCutPlanePoints.GetAt(0);
		tempVec1 = m_distalCutPlanePoints.GetAt(2) - m_distalCutPlanePoints.GetAt(0);
		norm = tempVec0*tempVec1;
		norm.Unitize();
		glBegin( GL_QUADS );
			glNormal3d( norm.x, norm.y, norm.z );

			glVertex3d( m_distalCutPlanePoints.GetAt(0).x, m_distalCutPlanePoints.GetAt(0).y, m_distalCutPlanePoints.GetAt(0).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(1).x, m_distalCutPlanePoints.GetAt(1).y, m_distalCutPlanePoints.GetAt(1).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(3).x, m_distalCutPlanePoints.GetAt(3).y, m_distalCutPlanePoints.GetAt(3).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(2).x, m_distalCutPlanePoints.GetAt(2).y, m_distalCutPlanePoints.GetAt(2).z );
		glEnd();
		glBegin( GL_LINE_LOOP );
			glVertex3d( m_distalCutPlanePoints.GetAt(0).x, m_distalCutPlanePoints.GetAt(0).y, m_distalCutPlanePoints.GetAt(0).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(1).x, m_distalCutPlanePoints.GetAt(1).y, m_distalCutPlanePoints.GetAt(1).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(3).x, m_distalCutPlanePoints.GetAt(3).y, m_distalCutPlanePoints.GetAt(3).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(2).x, m_distalCutPlanePoints.GetAt(2).y, m_distalCutPlanePoints.GetAt(2).z );
		glEnd();
		// step cut plane
		tempVec0 = m_distalCutPlanePoints.GetAt(1+2) - m_distalCutPlanePoints.GetAt(0+2);
		tempVec1 = m_distalCutPlanePoints.GetAt(2+2) - m_distalCutPlanePoints.GetAt(0+2);
		norm = tempVec0*tempVec1;
		norm.Unitize();
		glBegin( GL_QUADS );
			glNormal3d( norm.x, norm.y, norm.z );

			glVertex3d( m_distalCutPlanePoints.GetAt(0+2).x, m_distalCutPlanePoints.GetAt(0+2).y, m_distalCutPlanePoints.GetAt(0+2).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(1+2).x, m_distalCutPlanePoints.GetAt(1+2).y, m_distalCutPlanePoints.GetAt(1+2).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(3+2).x, m_distalCutPlanePoints.GetAt(3+2).y, m_distalCutPlanePoints.GetAt(3+2).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(2+2).x, m_distalCutPlanePoints.GetAt(2+2).y, m_distalCutPlanePoints.GetAt(2+2).z );
		glEnd();
		glBegin( GL_LINE_LOOP );
			glVertex3d( m_distalCutPlanePoints.GetAt(0+2).x, m_distalCutPlanePoints.GetAt(0+2).y, m_distalCutPlanePoints.GetAt(0+2).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(1+2).x, m_distalCutPlanePoints.GetAt(1+2).y, m_distalCutPlanePoints.GetAt(1+2).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(3+2).x, m_distalCutPlanePoints.GetAt(3+2).y, m_distalCutPlanePoints.GetAt(3+2).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(2+2).x, m_distalCutPlanePoints.GetAt(2+2).y, m_distalCutPlanePoints.GetAt(2+2).z );
		glEnd();
		// negative cut plane
		tempVec0 = m_distalCutPlanePoints.GetAt(1+4) - m_distalCutPlanePoints.GetAt(0+4);
		tempVec1 = m_distalCutPlanePoints.GetAt(2+4) - m_distalCutPlanePoints.GetAt(0+4);
		norm = tempVec0*tempVec1;
		norm.Unitize();
		glBegin( GL_QUADS );
			glNormal3d( norm.x, norm.y, norm.z );

			glVertex3d( m_distalCutPlanePoints.GetAt(0+4).x, m_distalCutPlanePoints.GetAt(0+4).y, m_distalCutPlanePoints.GetAt(0+4).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(1+4).x, m_distalCutPlanePoints.GetAt(1+4).y, m_distalCutPlanePoints.GetAt(1+4).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(3+4).x, m_distalCutPlanePoints.GetAt(3+4).y, m_distalCutPlanePoints.GetAt(3+4).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(2+4).x, m_distalCutPlanePoints.GetAt(2+4).y, m_distalCutPlanePoints.GetAt(2+4).z );
		glEnd();
		glBegin( GL_LINE_LOOP );
			glVertex3d( m_distalCutPlanePoints.GetAt(0+4).x, m_distalCutPlanePoints.GetAt(0+4).y, m_distalCutPlanePoints.GetAt(0+4).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(1+4).x, m_distalCutPlanePoints.GetAt(1+4).y, m_distalCutPlanePoints.GetAt(1+4).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(3+4).x, m_distalCutPlanePoints.GetAt(3+4).y, m_distalCutPlanePoints.GetAt(3+4).z );
			glVertex3d( m_distalCutPlanePoints.GetAt(2+4).x, m_distalCutPlanePoints.GetAt(2+4).y, m_distalCutPlanePoints.GetAt(2+4).z );
		glEnd();

	}

}

bool CMakeSolidPositionJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
    m_posStart = cursor;
	
	return true;
}

bool CMakeSolidPositionJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // 
	{
		// if the clicked-down is the same as the button-up point, change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}

	return true;
}

void CMakeSolidPositionJigs::OnReset()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	MyReset();

	//// Check outer face and inner face distance
	//ValidateThickness(m_pDoc);

	// If any errors, display error messages.
	DisplayErrorMessages();
}

void CMakeSolidPositionJigs::MyReset()
{
	bool gotFinalResult = false;

	CAdvancedControlJigs* advCtrlJigs = m_pDoc->GetAdvancedControlJigs();
	int tolLevel = advCtrlJigs->GetSolidPositionJigsToleranceLevel();
	// tolLevel = 3 is the default setting. If tolLevel != 3, user has chosen the best tolerance level.
	// We try the best tolLevel first. If do not get the result, then try the other tolLevels one by one.
	if ( tolLevel != 3 )
	{
		gotFinalResult = Reset(tolLevel, false);
	}
	
	if ( !gotFinalResult )
	{
		tolLevel = 3;
		gotFinalResult = Reset(tolLevel, false);
		if ( !gotFinalResult )
		{
			tolLevel = 4;
			gotFinalResult = Reset(tolLevel, false);
			if ( !gotFinalResult )
			{
				tolLevel = 5;
				gotFinalResult = Reset(tolLevel, false);
				if ( !gotFinalResult )
				{
					tolLevel = 2;
					gotFinalResult = Reset(tolLevel, false);
					if ( !gotFinalResult )
					{
						tolLevel = 1;
						gotFinalResult = Reset(tolLevel, false);
						if ( !gotFinalResult )
						{
							tolLevel = 6;
							gotFinalResult = Reset(tolLevel, true);
							if ( !gotFinalResult )
								tolLevel = 3;
						}
					}
				}
			}
		}
		advCtrlJigs->SetSolidPositionJigsToleranceLevel(tolLevel);
	}

	if (gotFinalResult)
		DetermineF2Profiles();

	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_SOLID_POSITION_JIGS, m_validateStatus, m_validateMessage);

}

bool CMakeSolidPositionJigs::Reset(int tolLevel, bool finalTrial)
{
	bool gotFinalResult = true;

	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::Reset(), tolLevel=%1, finalTrial=%2, starting ...").arg(tolLevel).arg(finalTrial) );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	IwContext&iwContext = m_pDoc->GetIwContext();

	CAdvancedControlJigs* advCtrlJigs = m_pDoc->GetAdvancedControlJigs();

	m_updateSuccessfully = false;

	CSolidPositionJigs* pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();
	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	CSideSurfaceJigs* pSideSurfaceJigs = m_pDoc->GetSideSurfaceJigs();

	if (pSolidPositionJigs==NULL ||
		pInnerSurfaceJigs==NULL ||
		pSideSurfaceJigs==NULL ||
		pOuterSurfaceJigs==NULL)
	{
		QApplication::restoreOverrideCursor();
		return gotFinalResult;
	}

	IwBrep* prevBrep = pSolidPositionJigs->GetIwBrep();
	if (prevBrep) 
	{
		IwObjDelete(prevBrep);
		pSolidPositionJigs->SetIwBrep(NULL);
	}

	IwBrep* innerBrep = pInnerSurfaceJigs->GetIwBrep();
	IwBrep* outerBrep = pOuterSurfaceJigs->GetIwBrep();
	IwBrep* sideBrep = pSideSurfaceJigs->GetIwBrep();
	if (innerBrep==NULL ||
		sideBrep==NULL ||
		outerBrep==NULL)
	{
		QApplication::restoreOverrideCursor();
		return gotFinalResult;
	}

	// Copy 3 Breps
	IwBrep* innerBrepCopy = new (iwContext) IwBrep(*innerBrep);
	IwBrep* outerBrepCopy = new (iwContext) IwBrep(*outerBrep);
	IwBrep* sideBrepCopy = new (iwContext) IwBrep(*sideBrep);
	innerBrepCopy->SewAndOrient();
	outerBrepCopy->SewAndOrient();
	sideBrepCopy->SewAndOrient();

	IwBrep *rBrep = NULL;
	double mergeTol = 0.001;
	double mergeAngTol = 0.01;

	// Note, these tolerance levels are not linear;
	if (tolLevel == 1)
	{
		mergeTol = 0.25;
		mergeAngTol = 25.0*IW_PI/180.0;
	}
	else if (tolLevel == 2)
	{
		mergeTol = 0.015;
		mergeAngTol = 5.0*IW_PI/180.0;
	}
	else if (tolLevel == 3)
	{
		mergeTol = 0.001;
		mergeAngTol = 2.5*IW_PI/180.0;
	}
	else if (tolLevel == 4)
	{
		mergeTol = 0.0005;
		mergeAngTol = 1*IW_PI/180.0;
	}
	else if (tolLevel == 5)
	{
		mergeTol = 0.00002;
		mergeAngTol = 0.25*IW_PI/180.0;
	}
	else if (tolLevel == 6)
	{
		mergeTol = 0.00001;
		mergeAngTol = 0.125*IW_PI/180.0;
	}

	// Somehow, merge by the order of inner -> outer -> side seems get a more robust result.
	// Merge inner & outer Brep.
	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::OnReset(), merge inner & outer Brep.") );
	IwBrep* rBrep3=NULL;
	IwMerge merge3(iwContext, innerBrepCopy, outerBrepCopy, mergeTol, mergeAngTol);
	merge3.NonManifoldBoolean(IW_BO_MERGE, rBrep3);

	if (rBrep3 == NULL)
	{
		if (finalTrial)
		{
			if (innerBrepCopy) IwObjDelete(innerBrepCopy);
			QApplication::restoreOverrideCursor();
			AddErrorCodes(MAKE_SOLID_POSITION_JIGS_ERROR_CODES_INPUT_SURFACES_NOT_GOOD);
			m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, false, false);
			return gotFinalResult;
		}
		else
		{
			QApplication::restoreOverrideCursor();
			return !gotFinalResult;
		}
	}
	rBrep3->SewAndOrient();

	// Merge inner & outer and side Brep.
	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::OnReset(), merge inner & outer and side Brep.") );

	IwBrep* rBrep4=NULL;
	IwMerge merge4(iwContext, rBrep3, sideBrepCopy, mergeTol, mergeAngTol);
	merge4.NonManifoldBoolean(IW_BO_MERGE, rBrep4);

	if (rBrep4 == NULL)
	{
		if (finalTrial)
		{
			QApplication::restoreOverrideCursor();
			AddErrorCodes(MAKE_SOLID_POSITION_JIGS_ERROR_CODES_INPUT_SURFACES_NOT_GOOD);
			m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, false, false);
			return gotFinalResult;
		}
		else
		{
			QApplication::restoreOverrideCursor();
			return !gotFinalResult;
		}
	}
	//
	rBrep = rBrep4;

	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::OnReset(), MakeManifold().") );

	// Make manifold (solid body)
	if (rBrep)
	{
		IwStatus error = rBrep->MakeManifold();
if (0)
{
IwTArray<IwBrep*> trimmedBrep;
trimmedBrep.Add(rBrep);
WriteIges(QString("C:\\Temp\\SolidPosition.igs"), NULL, NULL, NULL, &trimmedBrep, NULL);
}
		if ( error != IW_SUCCESS )
		{
			if (finalTrial)
			{
				QApplication::restoreOverrideCursor();
				AddErrorCodes(MAKE_SOLID_POSITION_JIGS_ERROR_CODES_INPUT_SURFACES_NOT_GOOD);
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, false, false);
				return gotFinalResult;
			}
			else
			{
				QApplication::restoreOverrideCursor();
				return !gotFinalResult;
			}
		}
	}


	// If the Brep contains more than one volume, then only take the largest one (the region with max faces). 
	IwTArray<IwRegion*> regions;
	rBrep->GetRegions(regions);
	if (regions.GetSize()>2)
	{
		unsigned maxFaceNo=0;
		IwTArray<IwShell*> shells;
		IwTArray<IwFaceuse*> faceUses;
		IwTArray<IwFace*> faces;
		for (unsigned i=0; i<regions.GetSize(); i++)
		{
			IwRegion* region = regions.GetAt(i);
			IwShell* shell;
			if ( !region->IsVoid() )// not infinite (outside) region
			{	
				region->GetShells(shells);
				for (unsigned j=0; j<shells.GetSize(); j++)
				{
					shell = shells.GetAt(j);
					shell->GetFaceuses(faceUses);
					if (faceUses.GetSize() > maxFaceNo)
					{
						maxFaceNo = faceUses.GetSize();
						faces.RemoveAll();
						for (unsigned k=0; k<faceUses.GetSize();k++)
						{
							faces.Add(faceUses.GetAt(k)->GetFace());
						}
					}
				}
			}
		}
		// Actually maxFaceNo should be 15. 
		// If it's less than this number, something is wrong.
		if (maxFaceNo < 10) 
		{
			if (finalTrial)
			{
				QApplication::restoreOverrideCursor();
				AddErrorCodes(MAKE_SOLID_POSITION_JIGS_ERROR_CODES_INPUT_SURFACES_NOT_GOOD);
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, false, false);
				return gotFinalResult;
			}
			else
			{
				QApplication::restoreOverrideCursor();
				return !gotFinalResult;
			}
		}
		// If there are more than 2 regions, i.e., more than 1 volume of solid body,
		// take the region with max faces.
		if ( regions.GetSize()>2 )
		{
			IwBrep* singleVolumeBrep = new (iwContext) IwBrep();
			rBrep->CopyFaces(faces, singleVolumeBrep);
			singleVolumeBrep->SewAndOrient();
			if (rBrep) IwObjDelete(rBrep);
			rBrep = singleVolumeBrep;
		}
	}

	// 
	rBrep->SewAndOrient();
	//
	pSolidPositionJigs->SetIwBrep(rBrep);
	pSolidPositionJigs->MakeTess();

	//Change status sign of itself
	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, false, false);

	m_pView->Redraw();

	m_updateSuccessfully = true;

	QApplication::restoreOverrideCursor();

	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::OnReset(), finished.") );

	return gotFinalResult;
}

void CMakeSolidPositionJigs::OnDisplayDistalPlanes()
{
	m_displayDistalPlanes = !m_displayDistalPlanes;

	if ( m_distalCutPlanePoints.GetSize() == 0 )
	{
		DetermineDistalPlanePoints(m_pDoc, m_distalCutPlanePoints);
	}

	ReDraw();
}

void CMakeSolidPositionJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::OnAccept()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("MakeSolidPositionJigs");

	CSolidPositionJigs* pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();

	if ( pSolidPositionJigs && !pSolidPositionJigs->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, true, true);

	if ( pSolidPositionJigs )
	{
		if ( m_validateStatus == VALIDATE_STATUS_NOT_VALIDATE ) // use runtime m_validateStatus, rather than solidPosition->GetValidateStatus()
		{
			OnValidate();
		}
	}

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CMakeSolidPositionJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::OnCancel()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("MakeSolidPositionJigs");

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CMakeSolidPositionJigs::DisplayErrorMessages()
{
	if ( m_manActType != MAN_ACT_TYPE_EDIT )
	{
		m_errorCodes.RemoveAll();
		return;
	}

	int		button;
	int		errorMsgNum = (int)m_errorCodes.GetSize();
	if (errorMsgNum == 0) return;

	QString totalErrorMessages(tr(""));
	for (int i=0; i<errorMsgNum; i++)
	{
		CMakeSolidPositionJigsErrorCodes errorCode = m_errorCodes.GetAt(i);
		QString msg;
		GetErrorCodeMessage(errorCode, msg);
		totalErrorMessages += msg + "\n\n";
	}

	button = QMessageBox::critical( NULL, 
									tr("Error Messages!"), 
									totalErrorMessages, 
									QMessageBox::Ok, 0 );	

	m_errorCodes.RemoveAll();	
}

void CMakeSolidPositionJigs::GetErrorCodeMessage(CMakeSolidPositionJigsErrorCodes& errorCode, QString& errorMsg)
{
	int errorNum = sizeof( MakeSolidPositionJigsErrorCodes ) / sizeof( CMakeSolidPositionJigsErrorCodesEntry );

	errorMsg = tr("");
	for( int i = 0; i < errorNum; i++ )
	{
		if( MakeSolidPositionJigsErrorCodes[i].ErrorCodes == errorCode )
		{
			errorMsg = MakeSolidPositionJigsErrorCodes[i].ErrorMessages;
			break;
		}
	}

}

void CMakeSolidPositionJigs::OnValidate()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_validateStatus = Validate(m_pDoc, m_validateMessage);

	QApplication::restoreOverrideCursor();

	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_SOLID_POSITION_JIGS, m_validateStatus, m_validateMessage);
	m_pDoc->GetSolidPositionJigs()->SetValidateStatus(m_validateStatus, m_validateMessage);
}

///////////////////////////////////////////////////////////////////////
// This function validates users settings.
///////////////////////////////////////////////////////////////////////
CEntValidateStatus CMakeSolidPositionJigs::Validate
(
	CTotalDoc* pDoc,						// I:
	QString &message						// O: error messages
)
{
	pDoc->AppendLog( QString("CMakeSolidPositionJigs::Validate()") );

	pDoc->AppendValidationReportJigsEntry("\n");
	pDoc->AppendValidationReportJigsEntry("**** Position Jig ****");

	// initialize data
	message.clear();
	int vStatus = 0; 

	IwBrep* solidBrep = pDoc->GetSolidPositionJigs()->GetIwBrep();
	// check whether Brep exists or not
	if ( solidBrep==NULL )
	{
		message = QString("No Position Jig body.");
		pDoc->AppendValidationReportJigsEntry( QString("Fail: ") + message , false );
		vStatus = 2;
		return (CEntValidateStatus)vStatus;
	}
	// check whether is solid body or not
	solidBrep->SewAndOrient();
	if (!solidBrep->IsManifoldSolid())
	{
		message = QString("Position Jig is not a solid body.");
		pDoc->AppendValidationReportJigsEntry( QString("Fail: ") + message , false );
		vStatus = 2;
		return (CEntValidateStatus)vStatus;
	}

	double F1ApproachAngle = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceApproachDegree();
	if ( !IS_EQ_TOL6(F1ApproachAngle, 0.0) )
	{
		message += QString("F1 approach angle is: %1 degrees wrt mech axis. ").arg(F1ApproachAngle, 0, 'f', 2);
	}

	if ( pDoc->GetValidateTableStatus("POSITION JIGS THICKNESS STATUS") != -1 )
	{
		// Check thickness here.
		double minThicknessAllowed = pDoc->GetVarTableValue("JIGS POSITION JIG MIN THICKNESS");
		double minThickness;
		IwPoint3d minPnt0, minPnt1;
		bool isOnEdge;
		bool reliable = ValidateThickness(pDoc, minThickness, minPnt0, minPnt1, isOnEdge);
		minThickness = floor(100*minThickness+0.5)/100.0;
		if ( reliable )
		{
			if ( minThickness < minThicknessAllowed )
			{
				if (isOnEdge)
				{
					int myStatus = 0; 
					pDoc->AppendValidateMessage( QString("POSITION JIGS THICKNESS"), myStatus, message );
					myStatus = 1;// if isOnEdge, it is a warning only.
					vStatus = max(vStatus, myStatus);
					message += QString(" Minimum thickness is: %1 mm. ").arg(minThickness, 0, 'f', 2);
					ShowPoint(pDoc, minPnt0, red);
					ShowPoint(pDoc, minPnt1, orange);
					pDoc->AppendValidationReportJigsEntry( QString("Pass: Position Jig thickness (>=%1mm) is valid: %2mm, because minimum thickness is on edge.").arg(minThicknessAllowed, 0, 'f', 2).arg(minThickness, 0, 'f', 2), true );
				}
				else
				{
					pDoc->AppendValidateMessage( QString("POSITION JIGS THICKNESS"), vStatus, message );
					message += QString(" Minimum thickness is: %1 mm. ").arg(minThickness, 0, 'f', 2);
					ShowPoint(pDoc, minPnt0, red);
					ShowPoint(pDoc, minPnt1, orange);
					pDoc->AppendValidationReportJigsEntry( QString("Fail: Position Jig thickness (>=%1mm) is not valid: %2mm").arg(minThicknessAllowed, 0, 'f', 2).arg(minThickness, 0, 'f', 2), false );
				}
			}
			else
			{
				pDoc->AppendValidationReportJigsEntry( QString("Pass: Position Jig thickness (>=%1mm) is valid: %2mm").arg(minThicknessAllowed, 0, 'f', 2).arg(minThickness, 0, 'f', 2), true );
			}
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for positioning jig thickness.");		
			vStatus = max(vStatus, 2);
			pDoc->AppendValidationReportJigsEntry( QString("Fail: Software validation is not reliable. Please proceed manual checking for positioning jig thickness."), false );
		}
	}

	if ( pDoc->GetValidateTableStatus("POSITION JIGS F2 NOTCH THICKNESS STATUS") != -1 )
	{
		// Check notch thickness here.
		double minThicknessAllowed = pDoc->GetVarTableValue("JIGS F2 NOTCH MIN THICKNESS");
		double minThickness;
		bool reliable = ValidateF2NotchThickness(pDoc, minThickness);
		minThickness = floor(100*minThickness+0.5)/100.0;
		if ( reliable )
		{
			if ( minThickness < minThicknessAllowed )
			{
				pDoc->AppendValidateMessage( QString("POSITION JIGS F2 NOTCH THICKNESS"), vStatus, message );
				message += QString(" Minimum thickness is: %1 mm. ").arg(minThickness, 0, 'f', 2);
			}
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for positioning jig F2 notch thickness.");		
			vStatus = max(vStatus, 2);
		}
	}

	if ( pDoc->GetValidateTableStatus("POSITION JIGS TAB CONTACT STATUS") != -1 )
	{
		bool bAllGood, bMedialAntContact, bLateralMidContact, bMedialPegContact;
		bool bReliable = ValidateTabContact(pDoc, bMedialAntContact, bLateralMidContact, bMedialPegContact);
		if ( bReliable )
		{
			bAllGood = bMedialAntContact && bLateralMidContact && bMedialPegContact;
			if ( !bAllGood )
			{
				pDoc->AppendValidateMessage( QString("POSITION JIGS TAB CONTACT"), vStatus, message );
				QString mAntMsg, lMidMsg, mPegMsg;
				if ( !bMedialAntContact )
					mAntMsg = QString("MedialAnteriorTab");
				if ( !bLateralMidContact )
					lMidMsg = QString("LateralMidTab");
				if ( !bMedialPegContact )
					mPegMsg = QString("MedialPegTab");
				message += QString("\n  : %1 %2 %3 not contact properly.").arg(mAntMsg).arg(lMidMsg).arg(mPegMsg);
				pDoc->AppendValidationReportJigsEntry( QString("Fail: Tabs do not contact properly with osteophyte."), false );
			}
			else
			{
				pDoc->AppendValidationReportJigsEntry( QString("Pass: 3 tabs contact properly with osteophyte."), true );
			}
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for tab contact.");		
			vStatus = max(vStatus, 2);
			pDoc->AppendValidationReportJigsEntry( QString("Fail: Software validation is not reliable. Please proceed manual checking for tab contact."), false );

		}
	}

	if ( vStatus == 0 )
	{
		if ( message.isEmpty() )
			message = QString("All pass.");
		else
			message += QString("\nAll pass.");
	}

	return (CEntValidateStatus)vStatus;
}

void CMakeSolidPositionJigs::SetValidateIcon(CEntValidateStatus vStatus, QString message)
{
	if ( m_actValidate )
	{
		m_actValidate->setToolTip( message );
		if ( vStatus == VALIDATE_STATUS_RED )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Red.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_YELLOW )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Yellow.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_GREEN )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Green.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			m_actValidate->setToolTip( QString("Not validate.") );
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate.png" ) );
		}
	}
}

bool CMakeSolidPositionJigs::ValidateThickness
(
	CTotalDoc* pDoc,		// I:
	double& minThickness,	// O:
	IwPoint3d& minPnt0,		// O: min thickness point
	IwPoint3d& minPnt1,		// O: min thickness point
	bool& isOnEdge			// O: whether the min points are on the face edges 
)
{
	isOnEdge = false;

	CSolidPositionJigs* pSolidPositionJigs = pDoc->GetSolidPositionJigs();
	if ( pSolidPositionJigs == NULL )
		return false;
	IwBrep* solidBrep = pSolidPositionJigs->GetIwBrep();
	if ( solidBrep == NULL )
		return false;

	bool isSolid = solidBrep->IsManifoldSolid();
	if ( !isSolid )
		return false;

	IwFace *face, *outerFace, *innerFace;
	IwTArray<IwFace*> faces;
	solidBrep->GetFaces(faces);
	double areaOuter, areaInner;
	double dArea, dVol;
	IwPoint3d dBCtr, dMom[2];

	// the outer face and the inner face could be the first one and second one in faces array, 
	// according the way we create the solid poistion jig in Reset().
	outerFace = faces.GetAt(0);
	outerFace->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), areaOuter, dVol, dBCtr, dMom );
	innerFace = faces.GetAt(1);
	innerFace->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), areaInner, dVol, dBCtr, dMom );

	double areaRatio = areaInner/areaOuter;
	if ( areaInner > 2000.0 && areaRatio < 1.5 && areaRatio > 0.66 )// both areas should be big && equte close.
	{
		// We have already get the right outer face and inner face
	}
	else
	{
		double maxArea = -HUGE_DOUBLE;
		for (unsigned i=0; i<faces.GetSize(); i++)
		{
			face = faces.GetAt(i);
			face->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), dArea, dVol, dBCtr, dMom );
			if ( dArea > maxArea )
			{
				maxArea = dArea;
				outerFace = face;
			}
		}

		maxArea = -HUGE_DOUBLE;
		for (unsigned i=0; i<faces.GetSize(); i++)
		{
			face = faces.GetAt(i);
			face->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), dArea, dVol, dBCtr, dMom );
			if ( face != outerFace && dArea > maxArea )
			{
				maxArea = dArea;
				innerFace = face;
			}
		}
	}

	IwPoint3d pnt0, pnt1;
	minThickness = DistFromFaceToFace(outerFace, innerFace, pnt0, pnt1);
	minPnt0 = pnt0;
	minPnt1 = pnt1;

	// Check isOnEdge or not
	IwTArray<IwEdge*> edges0, edges1;
	outerFace->GetEdges(edges0);
	innerFace->GetEdges(edges1);
	IwPoint3d cPnt;
	double distEdge0 = DistFromPointToEdges(pnt0, edges0, cPnt);
	double distEdge1 = DistFromPointToEdges(pnt1, edges1, cPnt);
	if ( fabs(distEdge0)<0.1 || fabs(distEdge1)<0.1 )
		isOnEdge = true;

	if ( minThickness < 0 )
		return false;

	return true;
}

bool CMakeSolidPositionJigs::ValidateF2NotchThickness
(
	CTotalDoc* pDoc,			// I:
	double& minNotchThickness	// O:
)
{
	CSolidPositionJigs* pSolidPositionJigs = pDoc->GetSolidPositionJigs();
	if ( pSolidPositionJigs == NULL )
		return true;
	IwTArray<IwCurve*> F2Profiles = pSolidPositionJigs->GetIwCurves();
	if ( F2Profiles.GetSize() == 0 )
		return false;

	//
	IwAxis2Placement wslAxis = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	// 
	COutlineProfileJigs *pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return true;
	IwTArray<IwPoint3d> projectedOutlinePoints;
	pOutlineProfileJigs->GetOutlineProfileJigsProjectedCurvePoints(projectedOutlinePoints);
	unsigned nSize = projectedOutlinePoints.GetSize();
	int lowerIndex = (int) (0.35*nSize);
	int upperIndex = (int) (0.65*nSize);
if (0)
{
ShowPoint(pDoc, projectedOutlinePoints.GetAt(lowerIndex), red);
ShowPoint(pDoc, projectedOutlinePoints.GetAt(upperIndex), brown);
}
	IwTArray<IwPoint3d> nearNotchPoints;
	for (int i=lowerIndex; i<upperIndex; i++)
	{
		nearNotchPoints.Add(projectedOutlinePoints.GetAt(i));
	}
	// Determine the highest notch point
	IwVector3d zAxis = wslAxis.GetZAxis();
	IwPoint3d farAnteriorPoint = wslAxis.GetOrigin() + 45*wslAxis.GetYAxis();
	IwPoint3d cPnt;
	int cIndex;
	DistFromPointToIwTArray(farAnteriorPoint, nearNotchPoints, &zAxis, cPnt, &cIndex);
if (0)
	ShowPoint(pDoc, cPnt, blue);

	minNotchThickness = DistFromPointToCurves(cPnt, F2Profiles, cPnt);

	return true;
}

bool CMakeSolidPositionJigs::OnReviewNext()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_SOLID_POSITION_JIGS, true);
	return true;
}

bool CMakeSolidPositionJigs::OnReviewBack()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_SOLID_POSITION_JIGS, false);
	return true;
}

bool CMakeSolidPositionJigs::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;


	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_SOLID_POSITION_JIGS))
    	OnAccept();
	return true;
}

void CMakeSolidPositionJigs::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 2;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 )
	{
		// Display solid position jig
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}

		vp->BottomView(false);
	}
	else if ( viewIndex == 1 ) 
	{
		vp->TopView(false);
	}

	m_predefinedViewIndex++;
}

//// FOR DISTAL CUT PLANES ////////////////////////////////////////////////
// Calculate the 8 points for 3 distal cut planes (medial, lateral, and step cuts)
void CMakeSolidPositionJigs::DetermineDistalPlanePoints
(
	CTotalDoc* pDoc,						// I:
	IwTArray<IwPoint3d>& planeCornerPoints	// O:
)
{
	planeCornerPoints.RemoveAll();
	double refSize = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize();
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	IwTArray<IwPoint3d> cutPnts, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPnts, cutOrientations);
	// side edge points
	IwPoint3d posEdgePnt = wslAxes.GetOrigin() + 0.75*refSize*wslAxes.GetXAxis();
	IwPoint3d negEdgePnt = wslAxes.GetOrigin() - 0.75*refSize*wslAxes.GetXAxis();
	// the distal cuts are the 6th, 7th, and 10th.
	IwPoint3d tempPnt, pnt0, pnt1, pnt2, pnt3;
	// project onto the positive edge point
	pnt0 = cutPnts.GetAt(6).ProjectPointToPlane(posEdgePnt, wslAxes.GetXAxis());
	// project onto step cut plane
	pnt1 = pnt0.ProjectPointToPlane(cutPnts.GetAt(10), wslAxes.GetXAxis());
	// project onto pnt0 first
	tempPnt = cutPnts.GetAt(7).ProjectPointToPlane(pnt0, wslAxes.GetYAxis());
	// Then project onto the negative edge point
	pnt3 = tempPnt.ProjectPointToPlane(negEdgePnt, wslAxes.GetXAxis());
	// project onto step cut plane
	pnt2 = pnt3.ProjectPointToPlane(cutPnts.GetAt(10), wslAxes.GetXAxis());
	// Note the order
	planeCornerPoints.Add(pnt0+0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt0-0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt1+0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt1-0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt2+0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt2-0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt3+0.75*refSize*wslAxes.GetYAxis());
	planeCornerPoints.Add(pnt3-0.75*refSize*wslAxes.GetYAxis());

	
}

void CMakeSolidPositionJigs::DetermineF2Profiles()
{
	m_pDoc->AppendLog( QString("CMakeSolidPositionJigs::DetermineF2Profiles()") );

	CSolidPositionJigs* solidPositionJigs = m_pDoc->GetSolidPositionJigs();
	if (solidPositionJigs == NULL )
		return;

	// Set empty curves to delete old profiles
	IwTArray<IwCurve*> F2ProfileCurves;
	solidPositionJigs->SetIwCurves(F2ProfileCurves);

	// Get F1 Brep
	IwBrep* Brep = solidPositionJigs->GetIwBrep();
	if ( Brep == NULL )
		return;

	// Create distal planes
	IwTArray<IwPoint3d> planeCornerPoints;
	CMakeSolidPositionJigs::DetermineDistalPlanePoints(m_pDoc, planeCornerPoints);
	IwBSplineSurface *posPlane=NULL, *negPlane=NULL, *stepPlane=NULL;
	IwBSplineSurface::CreateBilinearSurface(m_pDoc->GetIwContext(), planeCornerPoints.GetAt(0), planeCornerPoints.GetAt(1), planeCornerPoints.GetAt(2), planeCornerPoints.GetAt(3), posPlane);
	IwBSplineSurface::CreateBilinearSurface(m_pDoc->GetIwContext(), planeCornerPoints.GetAt(4), planeCornerPoints.GetAt(5), planeCornerPoints.GetAt(6), planeCornerPoints.GetAt(7), negPlane);
	IwBSplineSurface::CreateBilinearSurface(m_pDoc->GetIwContext(), planeCornerPoints.GetAt(2), planeCornerPoints.GetAt(3), planeCornerPoints.GetAt(4), planeCornerPoints.GetAt(5), stepPlane);

	// Intersect with inner and outer surfaces
	IwTArray<IwCurve*> crvs, innerCurves, outerCurves, trimmedInnerCurves, trimmedOuterCurves;
	double tol(0.01);
	// inner surface
	bool isInnerFace = true;
	IwFace* innerFace = GetMainFace(m_pDoc, isInnerFace);
	if (innerFace==NULL)
		return;
	IwBSplineSurface *innerSurf = (IwBSplineSurface*)innerFace->GetSurface();
	IntersectSurfaces(innerSurf, posPlane, crvs, 0.01, 0.1, NULL, NULL);
	innerCurves.Append(crvs);
	crvs.RemoveAll();
	IntersectSurfaces(innerSurf, negPlane, crvs, 0.01, 0.1, NULL, NULL);
	innerCurves.Append(crvs);
	crvs.RemoveAll();
	IntersectSurfaces(innerSurf, stepPlane, crvs, 0.01, 0.1, NULL, NULL);
	innerCurves.Append(crvs);
	crvs.RemoveAll();
	// Trim with face
	innerFace->CreateCurvesByTrimming(m_pDoc->GetIwContext(), &tol, &innerCurves, NULL, &trimmedInnerCurves, NULL);

	// outer surface
	isInnerFace = false;
	IwFace* outerFace = GetMainFace(m_pDoc, isInnerFace);
	if (outerFace == NULL)
		return;
	IwBSplineSurface *outerSurf = (IwBSplineSurface*)outerFace->GetSurface();
	IntersectSurfaces(outerSurf, posPlane, crvs, tol, 0.1, NULL, NULL);
	outerCurves.Append(crvs);
	crvs.RemoveAll();
	IntersectSurfaces(outerSurf, negPlane, crvs, 0.01, 0.1, NULL, NULL);
	outerCurves.Append(crvs);
	crvs.RemoveAll();
	IntersectSurfaces(outerSurf, stepPlane, crvs, 0.01, 0.1, NULL, NULL);
	outerCurves.Append(crvs);
	crvs.RemoveAll();
	// Trim with face
	outerFace->CreateCurvesByTrimming(m_pDoc->GetIwContext(), &tol, &outerCurves, NULL, &trimmedOuterCurves, NULL);
	
	//
	F2ProfileCurves.Append(trimmedInnerCurves);
	F2ProfileCurves.Append(trimmedOuterCurves);
	//
	solidPositionJigs->SetIwCurves(F2ProfileCurves);

	// delete objects
	if (posPlane)
		IwObjDelete(posPlane);
	if (negPlane)
		IwObjDelete(negPlane);
	if (stepPlane)
		IwObjDelete(stepPlane);
}

IwFace* CMakeSolidPositionJigs::GetMainFace
(
	CTotalDoc* pDoc, // I:
	bool bInnerFace	 // I: if TRUE, return the face containing the inner surface jig, otherwise return the outer face
)
{
	IwFace* theFace = NULL;
	IwFace *innerFace = NULL, *outerFace = NULL;

	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	CSolidPositionJigs* pSolidPositionJigs = pDoc->GetSolidPositionJigs();
	if ( pSolidPositionJigs == NULL )
		return theFace;

	IwBrep* positionJigsBrep = pSolidPositionJigs->GetIwBrep();

	// Get the largest 2 faces
	IwFace* face;
	IwTArray<IwFace*> faces;
	positionJigsBrep->GetFaces(faces);
	// 
	double area0, area1;
	double dArea, dVol;
	IwPoint3d dBCtr, dMom[2];
	// the outer face and the inner face could be the first one and second one in faces array, 
	// according the way we create the solid poistion jig in Reset().
	IwFace* face0 = faces.GetAt(0);// outer
	face0->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), area0, dVol, dBCtr, dMom );
	IwFace* face1 = faces.GetAt(1);// inner
	face1->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), area1, dVol, dBCtr, dMom );

	double areaRatio = area1/area0;
	if ( area1 > 2000.0 && areaRatio < 1.5 && areaRatio > 0.66 )// both areas should be big && equtely close.
	{
		// We have already get the right outer face and inner face
	}
	else
	{
		double maxArea = -HUGE_DOUBLE;
		for (unsigned i=0; i<faces.GetSize(); i++)
		{
			face = faces.GetAt(i);
			face->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), dArea, dVol, dBCtr, dMom );
			if ( dArea > maxArea )
			{
				maxArea = dArea;
				face0 = face;
			}
		}

		maxArea = -HUGE_DOUBLE;
		for (unsigned i=0; i<faces.GetSize(); i++)
		{
			face = faces.GetAt(i);
			face->ComputeProperties(IW_OT_SAME, 0.1, 0.1, IwVector3d(0,0,0), dArea, dVol, dBCtr, dMom );
			if ( face != face0 && dArea > maxArea )
			{
				maxArea = dArea;
				face1 = face;
			}
		}
	}
	// Need to further identify which one is outer and inner faces by the control points;
	ULONG uCount0, vConnt0, uCount1, vConnt1;
	IwTArray<IwPoint3d> ctrlPnts0, ctrlPnts1;
	IwTArray<double> weights0, weights1;
	IwBSplineSurface* surf0 = (IwBSplineSurface*)face0->GetSurface();
	surf0->GetControlPointNet(uCount0, vConnt0, ctrlPnts0, weights0);
	IwBSplineSurface* surf1 = (IwBSplineSurface*)face1->GetSurface();
	surf1->GetControlPointNet(uCount1, vConnt1, ctrlPnts1, weights1);

	if ( ctrlPnts0.GetSize() < ctrlPnts1.GetSize() )// Inner surface has much more control points
	{
		outerFace = face0;
		innerFace = face1;
	}
	else
	{
		outerFace = face1;
		innerFace = face0;
	}

	//
	if (bInnerFace)
		theFace = innerFace;
	else
		theFace = outerFace;

	return theFace;
}

bool CMakeSolidPositionJigs::DetermineTabFilletEdges
(
	CTotalDoc* pDoc,				// I:
	IwBrep*& filletBrep,			// I:
	IwTArray<IwEdge*>& filletEdges,	// O:
	IwTArray<IwPoint3d>& filletPoints// O:
)
{
	// Need to check outline profile jigs, inner surface jigs, outer surface jigs, and side surface jigs
	// are all up-to-date. Otherwise, the tab points in outline profile will not coincide with tab concave corners.

	//
	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return false;
	CInnerSurfaceJigs* pInnerSurfaceJigs = pDoc->GetInnerSurfaceJigs();
	if (pInnerSurfaceJigs==NULL)
		return false;
	COuterSurfaceJigs* pOuterSurfaceJigs = pDoc->GetOuterSurfaceJigs();
	if (pOuterSurfaceJigs==NULL)
		return false;
	CSideSurfaceJigs* pSideSurfaceJigs = pDoc->GetSideSurfaceJigs();
	if (pSideSurfaceJigs==NULL)
		return false;

	if ( !pOutlineProfileJigs->GetUpToDateStatus() ||
		 !pInnerSurfaceJigs->GetUpToDateStatus() ||
		 !pOuterSurfaceJigs->GetUpToDateStatus() ||
		 !pSideSurfaceJigs->GetUpToDateStatus() )
	{
		return false;
	}

	IwBrep* innerBrep = pInnerSurfaceJigs->GetIwBrep();

	// get tab points
	IwTArray<IwPoint3d> tabPoints;
	pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
	if ( tabPoints.GetSize() != 6 )
		return false;

	// Remove the current filletEdges & filletPoints here to make sure the return ones are 6 even they are not accurate.
	filletEdges.RemoveAll();
	filletPoints.RemoveAll();

	// get vertices
	IwTArray<IwVertex*> vertices;
	filletBrep->GetVertices(vertices);

	// Get the tab concave edges
	double tolDist = 0.5;
	IwTArray<IwEdge*> vEdges;
	IwEdge* vEdge;
	IwTArray<IwVertex*> edgeVertices;
	IwPoint3d tPnt, vPnt, cPnt;
	IwVertex *sVert, *eVert;
	double sDist, eDist;
	bool gotThisFilletEdge;
	for (unsigned i=0; i<tabPoints.GetSize(); i++)
	{
		gotThisFilletEdge = false;
		tPnt = tabPoints.GetAt(i);
		for (unsigned j=0; j<vertices.GetSize(); j++)
		{
			vPnt = vertices.GetAt(j)->GetPoint();
			if ( tPnt.DistanceBetween(vPnt) < tolDist )
			{
				vertices.GetAt(j)->GetEdges(vEdges);
				for (unsigned k=0; k<vEdges.GetSize(); k++)
				{
					vEdges.GetAt(k)->GetVertices(sVert, eVert);
					sDist = DistFromPointToBrep(sVert->GetPoint(), innerBrep, cPnt);
					eDist = DistFromPointToBrep(eVert->GetPoint(), innerBrep, cPnt);
					// The fillet edges arethe only edges which connect the inner surface and the outer surface. 
					// One of the sDist or eDist should be zero, but not both or none.
					if ( (sDist-tolDist)*(eDist-tolDist)<0 ) 
					{
						vEdge = vEdges.GetAt(k);
						filletEdges.Add(vEdge);
						gotThisFilletEdge = true;
						// Get the fillet point, which is the edge end point on outer surface.
						vEdge->GetVertices(edgeVertices);
						if ( tPnt.DistanceBetween(edgeVertices.GetAt(0)->GetPoint()) < tolDist )
							filletPoints.Add(edgeVertices.GetAt(1)->GetPoint());
						else
							filletPoints.Add(edgeVertices.GetAt(0)->GetPoint());
					}
					if ( gotThisFilletEdge )
						break;
				}
			}
			if (gotThisFilletEdge)
				break;
		}
	}

	// if anything wrong, still make 6 fillet points to prevent screwing up in stylus control points.
	if ( filletEdges.GetSize() != 6 )
	{
		for ( int i=filletEdges.GetSize(); i<6; i++)
		{
			filletEdges.Add(filletEdges.GetLast());
			filletPoints.Add(filletPoints.GetLast());
		}
		return false;
	}

	return true;
}

bool CMakeSolidPositionJigs::ValidateTabContact
(
	CTotalDoc* pDoc, 
	bool &bMedialAntContact,		// O:
	bool &bLateralMidContact,		// O:
	bool &bMedialPegContact			// O:
)
{
	bool bReliable = false;
	bMedialAntContact = false;
	bLateralMidContact = false;
	bMedialPegContact = false;

	// Get axes
	IwAxis2Placement wslAxis = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	IwAxis2Placement mechAxis = IFemurImplant::FemoralAxes_GetMechanicalAxes();
	// matrix for rotating along x-axis
	IwAxis2Placement rotMat, rotatedAxes;
	double rotRadian = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceApproachDegree()/180.0*IW_PI;
	rotMat.RotateAboutAxis(rotRadian, IwVector3d(1,0,0));
	rotMat.TransformAxis2Placement(mechAxis, rotatedAxes);
	IwVector3d projectVectorTowardAnkle = -rotatedAxes.GetZAxis();
	projectVectorTowardAnkle.Unitize();

	// Get dropping offset around tabe
	double droppingOffset = pDoc->GetAdvancedControlJigs()->GetInnerSurfaceDropOffsetDistance();

	// Get osteophyte Brep
	CInnerSurfaceJigs* innerSurfaceJigs = pDoc->GetInnerSurfaceJigs();
	if ( innerSurfaceJigs == NULL  ) 
		return false;
	IwBrep *osteoOffsetBrep;
	innerSurfaceJigs->GetOffsetOsteophyteBrep(osteoOffsetBrep);
	if (osteoOffsetBrep==NULL)
		return false;

	// Get solid implant jigs Brep
	CSolidPositionJigs* solidPositionJigs = pDoc->GetSolidPositionJigs();
	if (solidPositionJigs == NULL)
		return false;
	IwBrep* pBrep = solidPositionJigs->GetIwBrep();
	if ( pBrep == NULL )
		return false;

	// Get outline profile jigs info
	COutlineProfileJigs*			outlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if (outlineProfileJigs==NULL)
		return false;
	IwTArray<IwPoint3d> tabEndPoints;
	outlineProfileJigs->GetOutlineProfileTabEndPoints(tabEndPoints);
	if (tabEndPoints.GetSize() != 6 )
		return false;
	IwPoint3d medialAntTabPnt0 = tabEndPoints.GetAt(0);
	IwPoint3d medialAntTabPnt1 = tabEndPoints.GetAt(1);
	IwPoint3d lateralMidTabPnt0 = tabEndPoints.GetAt(2);
	IwPoint3d lateralMidTabPnt1 = tabEndPoints.GetAt(3);
	IwPoint3d medialPegTabPnt0 = tabEndPoints.GetAt(4);
	IwPoint3d medialPegTabPnt1 = tabEndPoints.GetAt(5);
	// 
	IwPoint3d posDroppingCenter, negDroppingCenter;
	outlineProfileJigs->GetDroppingCenters(posDroppingCenter, negDroppingCenter);
	IwPoint3d medialDroppingCenter, lateralDroppingCenter;
	if (pDoc->IsPositiveSideLateral())
	{
		lateralDroppingCenter = posDroppingCenter;
		medialDroppingCenter = negDroppingCenter;
	}
	else
	{
		lateralDroppingCenter = negDroppingCenter;
		medialDroppingCenter = posDroppingCenter;
	}


	// Get the inner face
	IwFace *innerFace = GetMainFace(pDoc, true);
	if ( innerFace == NULL )
		return false;

	// Search for the tab end edges
	IwTArray<IwEdge*> edges;
	IwEdge *edge, *mAntEdge=NULL, *lMidEdge=NULL, *mPegEdge=NULL;
	double vertexDeviation = 1.5;
	double dist0, dist1;
	IwVertex *v0, *v1;
	innerFace->GetEdges(edges);
	for (unsigned i=0; i<edges.GetSize(); i++)
	{
		edge = edges.GetAt(i);
		edge->GetVertices(v0, v1);
		// Search for the medial anterior tab end edge
		if ( mAntEdge == NULL )
		{
			dist0 = medialAntTabPnt0.DistanceBetween(v0->GetPoint());
			dist1 = medialAntTabPnt1.DistanceBetween(v1->GetPoint());
			if (dist0 < vertexDeviation && dist1 < vertexDeviation)
			{
				mAntEdge = edge;
			}
			else
			{
				dist0 = medialAntTabPnt0.DistanceBetween(v1->GetPoint());// swap to v1
				dist1 = medialAntTabPnt1.DistanceBetween(v0->GetPoint());// swap to v0
				if (dist0 < vertexDeviation && dist1 < vertexDeviation)
				{
					mAntEdge = edge;
				}
			}
		}
		// Search for the lateral mid tab end edge
		if ( lMidEdge == NULL )
		{
			dist0 = lateralMidTabPnt0.DistanceBetween(v0->GetPoint());
			dist1 = lateralMidTabPnt1.DistanceBetween(v1->GetPoint());
			if (dist0 < vertexDeviation && dist1 < vertexDeviation)
			{
				lMidEdge = edge;
			}
			else
			{
				dist0 = lateralMidTabPnt0.DistanceBetween(v1->GetPoint());// swap to v1
				dist1 = lateralMidTabPnt1.DistanceBetween(v0->GetPoint());// swap to v0
				if (dist0 < vertexDeviation && dist1 < vertexDeviation)
				{
					lMidEdge = edge;
				}
			}
		}
		// Search for the medial peg tab end edge
		if ( mPegEdge == NULL )
		{
			dist0 = medialPegTabPnt0.DistanceBetween(v0->GetPoint());
			dist1 = medialPegTabPnt1.DistanceBetween(v1->GetPoint());
			if (dist0 < vertexDeviation && dist1 < vertexDeviation)
			{
				mPegEdge = edge;
			}
			else
			{
				dist0 = medialPegTabPnt0.DistanceBetween(v1->GetPoint());// swap to v1
				dist1 = medialPegTabPnt1.DistanceBetween(v0->GetPoint());// swap to v0
				if (dist0 < vertexDeviation && dist1 < vertexDeviation)
				{
					mPegEdge = edge;
				}
			}
		}
	}
	//
	if ( mAntEdge == NULL || lMidEdge == NULL || mPegEdge == NULL )
		return false;
if (0)
{
ShowEdge(pDoc, mAntEdge, red);
ShowEdge(pDoc, lMidEdge, blue);
ShowEdge(pDoc, mPegEdge, orange);
}

	///////////////////////////////////////////////////////////////////////////
	// Sample 5 points around mAntEdge
	double offsetDist = 0.35, insetDist = 0.35+0.2; // "0.2" the step size when doing InnerSurface dropping. Thus, the worse case may have "0.2mm" gaps.
	double moveTowardAnkleDist = 20.0;
	IwPoint3d pnt, pntInset, pntOffset;
	IwVector3d outwardVec;
	IwTArray<IwPoint3d> mAntEdgePointsInset;
	IwTArray<IwPoint3d> mAntEdgePointsOffset;
	IwExtent1d edgeDom = mAntEdge->GetInterval();
	for (double ratio = 0.166; ratio < 0.98; ratio+=0.166)
	{
		mAntEdge->GetCurve()->EvaluatePoint(edgeDom.Evaluate(ratio), pnt);
		outwardVec = pnt - medialDroppingCenter;
		outwardVec = outwardVec.ProjectToPlane(wslAxis.GetZAxis());
		outwardVec.Unitize();
		pntInset = pnt - (droppingOffset+insetDist)*outwardVec;
		mAntEdgePointsInset.Add(pntInset);
		pntOffset = pnt + (-droppingOffset+offsetDist)*outwardVec;
		mAntEdgePointsOffset.Add(pntOffset);
	}
	// Validate these 5+5 points. The inset points should intersect with osset osteophyte, but offset should not.
	IwPoint3d intPnt;
	IwVector3d tempVec;
	double dotValue;
	int insetValidCount=0, offsetValidCount=0;
	bool gotIt;
	for (unsigned i=0; i<mAntEdgePointsInset.GetSize(); i++)
	{
		// Inset
		pnt = mAntEdgePointsInset.GetAt(i);
		gotIt = IntersectBrepByLine(osteoOffsetBrep, pnt+moveTowardAnkleDist*projectVectorTowardAnkle, projectVectorTowardAnkle, intPnt); // Move toward ankle a bit to get the correct intersection point
		if ( gotIt )
		{
			tempVec = intPnt - pnt;
			dotValue = tempVec.Dot(projectVectorTowardAnkle);
			if ( dotValue > 0 ) // the intersection point is in the ankle side, which is correct.
			{
				insetValidCount++;
			}
			else
			{
if (0)
	ShowPoint(pDoc, intPnt, red);
			}
		}
		else
		{}
		// Offset
		pnt = mAntEdgePointsOffset.GetAt(i);
		gotIt = IntersectBrepByLine(osteoOffsetBrep, pnt+moveTowardAnkleDist*projectVectorTowardAnkle, projectVectorTowardAnkle, intPnt); // Move toward ankle a bit to get the correct intersection point
		if ( gotIt )
		{
			tempVec = intPnt - pnt;
			dotValue = tempVec.Dot(projectVectorTowardAnkle);
			if ( dotValue <= 0 ) // the intersection point is in the hip side, which is correct.
			{
				offsetValidCount++;
			}
			else
			{
if (0)
	ShowPoint(pDoc, intPnt, blue);
			}
		}
		else
		{
			offsetValidCount++; // We do not expect intersection. No intersection is considered as "correct".
		}
	}
	bMedialAntContact = insetValidCount>2 && offsetValidCount>2; // at least 3 points are valid.

	///////////////////////////////////////////////////////////////////////////
	// Sample 5 points around lMidEdge
	IwTArray<IwPoint3d> lMidEdgePointsInset;
	IwTArray<IwPoint3d> lMidEdgePointsOffset;
	edgeDom = lMidEdge->GetInterval();
	for (double ratio = 0.166; ratio < 0.98; ratio+=0.166)
	{
		lMidEdge->GetCurve()->EvaluatePoint(edgeDom.Evaluate(ratio), pnt);
		outwardVec = pnt - lateralDroppingCenter;
		outwardVec = outwardVec.ProjectToPlane(wslAxis.GetZAxis());
		outwardVec.Unitize();
		pntInset = pnt - (droppingOffset+insetDist)*outwardVec;
		lMidEdgePointsInset.Add(pntInset);
		pntOffset = pnt + (-droppingOffset+offsetDist)*outwardVec;
		lMidEdgePointsOffset.Add(pntOffset);
	}
	// Validate these 5+5 points. The inset points should intersect with osset osteophyte, but offset should not.
	insetValidCount=0, offsetValidCount=0;
	for (unsigned i=0; i<lMidEdgePointsInset.GetSize(); i++)
	{
		// Inset
		pnt = lMidEdgePointsInset.GetAt(i);
		gotIt = IntersectBrepByLine(osteoOffsetBrep, pnt+moveTowardAnkleDist*projectVectorTowardAnkle, projectVectorTowardAnkle, intPnt); // Move toward ankle a bit to get the correct intersection point
		if ( gotIt )
		{
			tempVec = intPnt - pnt;
			dotValue = tempVec.Dot(projectVectorTowardAnkle);
			if ( dotValue > 0 ) // the intersection point is in the ankle side, which is correct.
			{
				insetValidCount++;
			}
			else
			{
if (0)
	ShowPoint(pDoc, intPnt, red);
			}
		}
		else
		{}
		// Offset
		pnt = lMidEdgePointsOffset.GetAt(i);
		gotIt = IntersectBrepByLine(osteoOffsetBrep, pnt+moveTowardAnkleDist*projectVectorTowardAnkle, projectVectorTowardAnkle, intPnt); // Move toward ankle a bit to get the correct intersection point
		if ( gotIt )
		{
			tempVec = intPnt - pnt;
			dotValue = tempVec.Dot(projectVectorTowardAnkle);
			if ( dotValue <= 0 ) // the intersection point is in the hip side, which is correct.
			{
				offsetValidCount++;
			}
			else
			{
if (0)
	ShowPoint(pDoc, intPnt, blue);
			}
		}
		else
		{
			offsetValidCount++; // We do not expect intersection. No intersection is considered as "correct".
		}
	}
	bLateralMidContact = insetValidCount>2 && offsetValidCount>2; // at least 3 points are valid.

	///////////////////////////////////////////////////////////////////////////
	// Sample 5 points around mPegEdge
	IwTArray<IwPoint3d> mPegEdgePointsInset;
	IwTArray<IwPoint3d> mPegEdgePointsOffset;
	edgeDom = mPegEdge->GetInterval();
	for (double ratio = 0.166; ratio < 0.98; ratio+=0.166)
	{
		mPegEdge->GetCurve()->EvaluatePoint(edgeDom.Evaluate(ratio), pnt);
		outwardVec = pnt - medialDroppingCenter;
		outwardVec = outwardVec.ProjectToPlane(wslAxis.GetZAxis());
		outwardVec.Unitize();
		pntInset = pnt - (droppingOffset+insetDist)*outwardVec;
		mPegEdgePointsInset.Add(pntInset);
		pntOffset = pnt + (-droppingOffset+offsetDist)*outwardVec;
		mPegEdgePointsOffset.Add(pntOffset);
	}
	// Validate these 5+5 points. The inset points should intersect with osset osteophyte, but offset should not.
	insetValidCount=0, offsetValidCount=0;
	for (unsigned i=0; i<mPegEdgePointsInset.GetSize(); i++)
	{
		// Inset
		pnt = mPegEdgePointsInset.GetAt(i);
		gotIt = IntersectBrepByLine(osteoOffsetBrep, pnt+moveTowardAnkleDist*projectVectorTowardAnkle, projectVectorTowardAnkle, intPnt); // Move toward ankle a bit to get the correct intersection point
		if ( gotIt )
		{
			tempVec = intPnt - pnt;
			dotValue = tempVec.Dot(projectVectorTowardAnkle);
			if ( dotValue > 0 ) // the intersection point is in the ankle side, which is correct.
			{
				insetValidCount++;
			}
			else
			{
if (0)
	ShowPoint(pDoc, intPnt, red);
			}
		}
		else
		{}
		// Offset
		pnt = mPegEdgePointsOffset.GetAt(i);
		gotIt = IntersectBrepByLine(osteoOffsetBrep, pnt+moveTowardAnkleDist*projectVectorTowardAnkle, projectVectorTowardAnkle, intPnt); // Move toward ankle a bit to get the correct intersection point
		if ( gotIt )
		{
			tempVec = intPnt - pnt;
			dotValue = tempVec.Dot(projectVectorTowardAnkle);
			if ( dotValue <= 0 ) // the intersection point is in the hip side, which is correct.
			{
				offsetValidCount++;
			}
			else
			{
if (0)
	ShowPoint(pDoc, intPnt, blue);
			}
		}
		else
		{
			offsetValidCount++; // We do not expect intersection. No intersection is considered as "correct".
		}
	}
	bMedialPegContact = insetValidCount>2 && offsetValidCount>2; // at least 3 points are valid.

	//
	bReliable = true;

	return bReliable;
}
