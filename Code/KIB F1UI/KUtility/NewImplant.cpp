#include "NewImplant.h"
#include <QValidator>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include "KWaitCursor.h"

#include <exception>
using namespace std;

NewImplant::NewImplant(QString implantRoot, QWidget *parent)
    : basePath(implantRoot), QDialog(parent)
{
    ui.setupUi(this);

    ui.lineEditImplantNum->setValidator(new QRegExpValidator(QRegExp("[^?/,!\\\\*]*"), this));

    // database settings
    QSettings settings;
    //db.SetParameters(
    //    settings.value("DatabaseName", "CMISPRD").toString(),
    //    settings.value("DatabaseHostname", "ODAP0.Stryker.local").toString(),
    //    settings.value("DatabasePort", 1521).toInt(), 
    //    settings.value("DatabaseUsername", "confsoft").toString(),
    //    settings.value("DatabasePassword", "apps_readonly").toString() );
    hasDatabase = db.TestConnection();
    ui.pushButtonQuery->setEnabled(hasDatabase);

    ui.radioButtonUni->setEnabled(false);
    ui.radioButtonDuo->setEnabled(false);
    ui.radioButtonTotal->setEnabled(false);
    ui.radioButtonTotalPS->setEnabled(false);
    ui.radioButtonLeft->setEnabled(false);
    ui.radioButtonRight->setEnabled(false);
    ui.radioButtonMedial->setEnabled(false);
    ui.radioButtonLateral->setEnabled(false);
    ui.comboBoxCountry->setEnabled(false);
	ui.comboBoxZone->setEnabled(false);
    ui.pushButtonOK->setEnabled(false);

	ui.comboBoxCountry->setInsertPolicy(QComboBox::InsertAtCurrent);

	connect(ui.radioButtonUni, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonDuo, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonTotal,   SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonTotalPS, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonLeft, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonRight, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonMedial, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	connect(ui.radioButtonLateral, SIGNAL(toggled(bool)), this, SLOT(on_RadioButton_toggled(bool)));
	
    // load the country/zone information from the application folder
    QFile file( QDir::current().absoluteFilePath("CountryZone.txt") );
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(this, "Error", "Missing the country zone settings. The software isn't installed correctly.");
        return;
    }
    
    while (!file.atEnd())
    {
        QList<QByteArray> ls = file.readLine().split('=');
        if( ls.size()==2 )
        {
			QString country = ls[0].trimmed();
			QString zone = ls[1].trimmed();
			pairCountryZone.append( qMakePair(country,zone) );
            
			if( zones.indexOf(zone) == -1 )
				zones.append( zone );
        }
    }

    // add the list of countries to the combo box
    ui.comboBoxCountry->addItem( "-" );
    for(int i=0; i<pairCountryZone.size(); i++)
        ui.comboBoxCountry->addItem( pairCountryZone[i].first );
	// add 'Other' at the end for entering new country
	ui.comboBoxCountry->addItem("Other");

	// add the list of zones
	ui.comboBoxZone->addItem( "-" );
	for(int i=0; i<zones.size(); i++)
		ui.comboBoxZone->addItem( zones[i] );
}

NewImplant::~NewImplant()
{
}

QString NewImplant::GetImplantName()
{
    return ui.lineEditImplantNum->text().trimmed();
}

QString NewImplant::GetImplantType()
{
	if( ui.radioButtonUni->isChecked() )
		return "iUni";
    else if( ui.radioButtonDuo->isChecked() )
        return "iDuo";
    else if( ui.radioButtonTotal->isChecked() )
        return "TriathlonF1";
    else if( ui.radioButtonTotalPS->isChecked() )
        return "TriathlonF1PS";
    else
        return "";
}

QString NewImplant::GetKneeType()
{
    return ui.radioButtonLeft->isChecked() ? "Left" : "Right";
}

QString NewImplant::GetCompartmentType()
{
    if( ui.radioButtonTotal->isChecked() || ui.radioButtonTotalPS->isChecked() )
        return "";
    else
        return ui.radioButtonMedial->isChecked() ? "Medial" : "Lateral";
}

// Get the country and zone information
QString NewImplant::GetCountryCode()
{
    return ui.comboBoxCountry->currentIndex()<=0 ? "" : ui.comboBoxCountry->currentText();
}

QString NewImplant::GetZone()
{
    return ui.comboBoxZone->currentIndex()<=0 ? "" : ui.comboBoxZone->currentText();
}

void NewImplant::on_lineEditImplantNum_textChanged(const QString & str)
{
    bool e = !str.isEmpty();

    ui.radioButtonUni->setEnabled(false);
    ui.radioButtonDuo->setEnabled(false);
    ui.radioButtonTotal->setEnabled(e);
    ui.radioButtonTotalPS->setEnabled(e);
    ui.radioButtonLeft->setEnabled(e);
    ui.radioButtonRight->setEnabled(e);
    ui.radioButtonMedial->setEnabled(e);
    ui.radioButtonLateral->setEnabled(e);
    ui.comboBoxCountry->setEnabled(e);
	ui.comboBoxZone->setEnabled(e);
    ui.labelQueryResult->clear();

    if( e )
    {
        // auto-query the database if the full 7-digit serial has been entered
        if( hasDatabase && str.length()>=7 )
            on_pushButtonQuery_clicked();
        else
            ResetEntries();
    }

    EnableDisableOK();
}

// reset all entry
void NewImplant::ResetEntries()
{
    QList<QButtonGroup*> list;
	list << ui.buttonGroupType << ui.buttonGroupKnee << ui.buttonGroupCompartment;
	for(int i=0; i<list.size(); i++)
	{
		QButtonGroup* bg = list[i];
		QAbstractButton* ab = bg->checkedButton();
		if( ab )
		{
			bg->setExclusive(false);
			ab->setChecked(false);
			bg->setExclusive(true);
		}
	}
	
    ui.radioButtonUni->setEnabled(false);
	ui.radioButtonDuo->setEnabled(false);
	ui.radioButtonTotal->setEnabled(true);
	ui.radioButtonTotalPS->setEnabled(true);
	ui.radioButtonLeft->setEnabled(true);
	ui.radioButtonRight->setEnabled(true);
	ui.radioButtonMedial->setEnabled(true);
	ui.radioButtonLateral->setEnabled(true);;

    ui.comboBoxCountry->setEnabled(true);
    ui.comboBoxCountry->setCurrentIndex(0);
	ui.comboBoxCountry->setEditable(false);
	ui.comboBoxZone->setEnabled(true);
	ui.comboBoxZone->setCurrentIndex(0);
}

void NewImplant::on_pushButtonQuery_clicked()
{
    KWaitCursor wait;
    int i;
    
    // get the implant information from the database
    QString desc, country;
    if( db.Query(GetImplantName(), desc, country) )
    {
        QString str = "<font color='green'>Record found: " + desc + ", " + country + ".<font>";
        ui.labelQueryResult->setText(str);

        // make the selection based on the database entry and prevent user changes

        // Example description:
        //   iDUO G2, Right Medial
        //   iTOTAL CR, Left Knee
        //   
        bool b = false;
        if( desc.indexOf("iUni",0, Qt::CaseInsensitive)!=-1 )
            ui.radioButtonUni->setChecked(false);
        else if( desc.indexOf("iDuo",0, Qt::CaseInsensitive)!=-1 )
            ui.radioButtonDuo->setChecked(false);
        else if( (i=desc.indexOf("TriathlonF1",0, Qt::CaseInsensitive))!=-1 )
        {
            if( desc.mid(i,10).indexOf("PS",0,Qt::CaseInsensitive)!=-1 )
                ui.radioButtonTotalPS->setChecked(true);
            else
                ui.radioButtonTotal->setChecked(true);
        }
        else
            b = true;
        ui.radioButtonUni->setEnabled(false);
        ui.radioButtonDuo->setEnabled(false);
        ui.radioButtonTotal->setEnabled(b);
        ui.radioButtonTotalPS->setEnabled(b);

        b = false;
        if( desc.indexOf("Left",0, Qt::CaseInsensitive)!=-1 )
            ui.radioButtonLeft->setChecked(true);
        else if( desc.indexOf("Right",0, Qt::CaseInsensitive)!=-1 )
            ui.radioButtonRight->setChecked(true);
        else
            b = true;
        ui.radioButtonLeft->setEnabled(b);
        ui.radioButtonRight->setEnabled(b);

		b = false;
        if( ui.radioButtonUni->isChecked() || ui.radioButtonDuo->isChecked() )
        {
            if( desc.indexOf("Medial",0, Qt::CaseInsensitive)!=-1 )
                ui.radioButtonMedial->setChecked(true);
            else if( desc.indexOf("Lateral",0, Qt::CaseInsensitive)!=-1 )
                ui.radioButtonLateral->setChecked(true);
            else
                b = true;
		}
		else
		{
			QAbstractButton* ab = ui.buttonGroupCompartment->checkedButton();
			if( ab )
			{
				ui.buttonGroupCompartment->setExclusive(false);
				ab->setChecked(false);
				ui.buttonGroupCompartment->setExclusive(true);
			}
		}
        ui.radioButtonMedial->setEnabled(b);
        ui.radioButtonLateral->setEnabled(b);
        
		b = true;
        if( (i = ui.comboBoxCountry->findText(country)) > 0 )
		{
			b = false;
			ui.comboBoxCountry->setCurrentIndex( i );
			ui.comboBoxZone->setCurrentIndex( ui.comboBoxZone->findText(pairCountryZone[i-1].second) );
			ui.comboBoxZone->setEnabled(false);
		}
		else if( !country.isEmpty() )
		{
			b = false;
			ui.comboBoxCountry->setItemText(ui.comboBoxCountry->count()-1, country);
			ui.comboBoxCountry->setCurrentIndex( ui.comboBoxCountry->count()-1 );

			QMessageBox::information(parentWidget(), "Warning", "The country code is not recognized. Select the zone manually.");
		}
		ui.comboBoxCountry->setEditable(false);
        ui.comboBoxCountry->setEnabled(b);
    }
    else
    {
        ui.labelQueryResult->setText( "<font color='red'>Record not found. Manually enter the information.<font>" );

        ResetEntries();
    }
}

void NewImplant::on_RadioButton_toggled(bool)
{
	bool total = ui.radioButtonTotal->isChecked() || ui.radioButtonTotalPS->isChecked();

	if( total )
	{
		QAbstractButton* ab = ui.buttonGroupCompartment->checkedButton();
		if( ab )
		{
			ui.buttonGroupCompartment->setExclusive(false);
			ab->setChecked(false);
			ui.buttonGroupCompartment->setExclusive(true);
		}
	}

    ui.radioButtonMedial->setEnabled(!total);
    ui.radioButtonLateral->setEnabled(!total);
	ui.comboBoxCountry->setEditable(false);
    EnableDisableOK();
}

void NewImplant::on_comboBoxCountry_currentIndexChanged(int index)
{
	// select the last 'Other' -> let user enter the country code
	if( index==ui.comboBoxCountry->count()-1 )
	{
		ui.comboBoxCountry->setEditable(true);
		QLineEdit* le = ui.comboBoxCountry->lineEdit();
		if( le->text().length()>2 )
			le->setText("");
		le->setInputMask(">AA");
		le->selectAll();
	}
	else
	{
		ui.comboBoxCountry->setEditable(false);

		if( index==0 )
			ui.comboBoxZone->setCurrentIndex(0);
		else
			ui.comboBoxZone->setCurrentIndex( ui.comboBoxZone->findText(pairCountryZone[index-1].second) );
	}

    EnableDisableOK();
}

void NewImplant::on_comboBoxZone_currentIndexChanged(int index)
{
	EnableDisableOK();
}

void NewImplant::EnableDisableOK()
{
    ui.pushButtonOK->setEnabled( !GetImplantName().isEmpty() &&
        (ui.radioButtonUni->isChecked() || ui.radioButtonDuo->isChecked() || ui.radioButtonTotal->isChecked() || ui.radioButtonTotalPS->isChecked()) &&
        (ui.radioButtonLeft->isChecked() || ui.radioButtonRight->isChecked()) &&
        (ui.radioButtonTotal->isChecked() || ui.radioButtonTotalPS->isChecked() || (ui.radioButtonMedial->isChecked() || ui.radioButtonLateral->isChecked())) &&
        ui.comboBoxCountry->currentIndex()>=1 && ui.comboBoxZone->currentIndex()>=1 );
}

void NewImplant::on_pushButtonOK_clicked()
{
    bool stat = true;

    try
    {
        QString implantName = GetImplantName();
		if (implantName.isEmpty())
			throw exception("Enter a new implant serial number");
			
		// If directory doesn't exist, create it
		QString outputDir = basePath + QDir::separator() + implantName + QDir::separator();
		QDir newDir(outputDir);
		if (newDir.exists())
			throw exception("Implant already exists!");

		// Create directories
		if( !newDir.mkdir(outputDir) ||
            !newDir.mkdir("CM") ||
            !newDir.mkdir("CT") ||
		    !newDir.mkdir("Segmentation") ||
		    !newDir.mkdir("SolidWorks") ||
		    !newDir.mkdir("Surfaces") ||
            !newDir.mkdir("STL") )
            throw exception("Failed to create the implant folder");

        newDir.setPath(outputDir + "CM");
        newDir.mkdir("FEMUR");

        newDir.setPath(outputDir + "SolidWorks");
        newDir.mkdir("Femur");
    }
    catch(exception& e)
    {
        QMessageBox::information(parentWidget(), "Exception", e.what());
        stat = false;
    }

    if( stat )
        accept();
}

void NewImplant::on_pushButtonCancel_clicked()
{
    reject();
}
