#pragma once

#include <QtGui>
#include "..\KAppTotal\Entity.h"
#include "..\KTriathlonF1\TotalDocReader.h"
#include "TotalDocPS.h"

class KTotalDocPSReader : public CTotalDocReader
	{
public:
	KTotalDocPSReader( KTotalDocPS* pDoc, const QString& sDocFileName );
	~KTotalDocPSReader();
	KTotalDocPS* GetTotalPSDoc() {return ((KTotalDocPS*)m_pDoc);};

	bool			LoadPSEntities(CEntRole startEntRole=ENT_ROLE_IMPLANT_PS_NONE, CEntRole endEntRole=ENT_ROLE_JIGS_PS_END);

private:

private:
	
	};