#pragma once

#include <QtGui>
#include "Part.h"
#include "DataDoc.h"

class CDocWriter
	{
public:
	CDocWriter( CDataDoc* pDoc, const QString& sDocFileName );
	~CDocWriter();

	bool			SaveFile(bool incrementalSave=true, QString& CRPS=QString(""), CEntRole startEntToSave=ENT_ROLE_NONE, CEntRole endEntToSave=ENT_ROLE_NONE);
	static bool		FolderExists(QString docFolderName);

private:
	CDataDoc*		m_pDoc;
	QString			m_sDocFolder;
	};
