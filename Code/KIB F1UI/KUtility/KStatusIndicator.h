#ifndef KSTATUSINDICATOR_H
#define KSTATUSINDICATOR_H

#include <QObject>
#include <QStatusBar>
#include <QProgressBar>

class KStatusIndicator : public QObject
{
    Q_OBJECT

public:
    KStatusIndicator(QObject *parent=NULL);
    ~KStatusIndicator();

    void SetStatusBar(QStatusBar* s) { status = s; }
    void SetProgressBar(QProgressBar* p) { progress = p; }

public slots:
    void ShowMessage(QString msg, bool bForceUpdate = false);
    void ProgressInit(int maxv);
    void ProgressInit(int minv, int maxv);
    void ProgressSet(int val);
    void ProgressReset();

private:
    QProgressBar* progress;
    QStatusBar* status;
};

// global instance
extern KStatusIndicator kStatus;

#endif // KSTATUSINDICATOR_H
