#pragma once

#include <qaction.h>
#include <qcombobox.h>
#include <qtoolbar.h>
#include "..\KUtility\CoordinateSystem.h"

class PredefinedViewToolBar : public QToolBar
{
	Q_OBJECT
public:
	PredefinedViewToolBar();
	~PredefinedViewToolBar();

	void ResetToggles(bool keepOrientationInfo=false);

	void ToggleFrontView(bool autoRotZ180=true);
	void ToggleBackView(bool autoRotZ180=true);
	void ToggleLeftView(bool autoRotZ180=true);
	void ToggleRightView(bool autoRotZ180=true);
	void ToggleTopView(bool autoRotZ180=true);
	void ToggleBottomView(bool autoRotZ180=true);
	void ToggleIsoView(bool autoRotZ180=true);
	void ToggleIsoBackView(bool autoRotZ180=true);

	inline bool GetFrontView(bool *parallelToFrontView=NULL)   { if (parallelToFrontView) *parallelToFrontView = frontViewOrientation; return frontView;   }
	inline bool GetBackView(bool *parallelToBackView=NULL)    { if (parallelToBackView) *parallelToBackView = backViewOrientation; return backView;    }
	inline bool GetLeftView(bool *parallelToLeftView=NULL)    { if (parallelToLeftView) *parallelToLeftView = leftViewOrientation; return leftView;    }
	inline bool GetRightView(bool *parallelToRightView=NULL)   { if (parallelToRightView) *parallelToRightView = rightViewOrientation; return rightView;   }
	inline bool GetTopView(bool *parallelToTopView=NULL)     { if (parallelToTopView) *parallelToTopView = topViewOrientation; return topView;     }
	inline bool GetBottomView(bool *parallelToBottomView=NULL)  { if (parallelToBottomView) *parallelToBottomView = bottomViewOrientation; return bottomView;  }
	inline bool GetIsoView(bool *parallelToIsoView=NULL)     { if (parallelToIsoView) *parallelToIsoView = isoViewOrientation; return isoView;     }
	inline bool GetIsoBackView(bool *parallelToIsoBackView=NULL) { if (parallelToIsoBackView) *parallelToIsoBackView = isoBackViewOrientation; return isoBackView; }

	IwAxis2Placement GetCurrentSystem();

    int GetNumberOfSystems() const { return axesList->count(); }
    QString GetCurrentSystemName() { return axesList->currentText(); }
    void SetCurrentSystemName(const QString& name);

signals:
	void BroadcastCurrentCoordSystem(const CoordSystem& axes);

protected slots:
	void OnCoordSystem(const CoordSystem& system, bool switchCurrent=true);
	void OnRemoveCoordSystem(QString name);
	void OnSetCurrentCoordSystem(QString name);
	void handleSelectionChanged(int index);

private:
	bool frontView;
	bool backView;
	bool leftView;
	bool rightView;
	bool topView;
	bool bottomView;
	bool isoView;
	bool isoBackView;
	bool frontViewOrientation; // indicate it's parallel to front view regardless rotating along z 180 or not.
	bool backViewOrientation;
	bool leftViewOrientation;
	bool rightViewOrientation;
	bool topViewOrientation;
	bool bottomViewOrientation;
	bool isoViewOrientation;
	bool isoBackViewOrientation;

	QComboBox *axesList;
	QAction *axesListAction;
};
