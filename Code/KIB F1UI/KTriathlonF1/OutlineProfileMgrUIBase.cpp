#include "OutlineProfileMgrUIBase.h"
#include "TotalView.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Manager.h"
#include "OutlineProfile.h"
#include "UndoRedoCmd.h"
#include "DefineOutlineProfile.h"
#include "..\KApp\SwitchBoard.h"
#include "OutlineProfileMgrModel.h"
#include "ValidateOutlineProfile.h"
#include "..\KAppTotal\Utilities.h"

#include <QMessageBox>
#include <QApplication>

using namespace Validation::OutlineProfile;

OutlineProfileMgrUIBase::OutlineProfileMgrUIBase(CTotalDoc* doc, CManagerActivateType manActType, CManager *owner, OutlineProfileMgrModel *model)  
   :m_addDeleteMode(0),
	m_oldOutlineProfileExist(true),
	m_leftButtonDown(false),
	m_mgrModel(model),
	m_owner(owner),
	m_manActType(manActType),
	m_pDoc(doc),
	m_displayRuler(false)
{
	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT || manActType == MAN_ACT_TYPE_REVIEW );

	//////////////////////////////////////////////////////////////////////////////////
	///////////////////ACTIONS taken in the child classes
	// After the base class is done initializing the data depending on the requested
	// action (i.e. INITIALIZE/RESET or no OutlineProfile exists) T
	// The child classes:
	//		1. Determine the notch width points; the 2D child flattens the points
	//		2. Initialize the MouseSearchList, in the case of the 2D the search list is flattened.
	//		3. Apply the Refine action if the Activation type is REFINE
	//		4. Validate the Outline Profile if activation type isn't INITIALIZE/RESET
	//		5. Accept the outline profile if activation type isn't for manual editing
	//////////////////////////////////////////////////////////////////////////////////
	////ATTENTION: If the actions in the child classes change (for example a new child is added),
	//					or some of the activation types are removed
	//					please update this comment
	//////////////////////////////////////////////////////////////////////////////////

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);

	if ( activateUI )
		emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	// Get the runtime data
	COutlineProfile*			outlineProfile = m_pDoc->GetOutlineProfile();

	if ( outlineProfile == NULL || // if no object
		 outlineProfile->GetIwBrep() == NULL || // if no final result
		 manActType == MAN_ACT_TYPE_RESET )
	{
		if ( outlineProfile == NULL )
		{
			outlineProfile = new COutlineProfile( m_pDoc );
			m_pDoc->AddEntity( outlineProfile, true );
			m_oldOutlineProfileExist = false;
		}
		
		UpdateSketchSurface();

		// Reset is the way to construct the outline profile.
		Reset();
	}
	else
	{
		UpdateSketchSurface();
		m_mgrModel->InitializeFeaturePoints();
		m_validateStatus = outlineProfile->GetValidateStatus(m_validateMessage);
		((CDefineOutlineProfile*)m_owner)->SetValidateIcon(m_validateStatus, m_validateMessage);
	}

}

OutlineProfileMgrUIBase::~OutlineProfileMgrUIBase()
{
	SwitchBoard::removeSignalSender(this);

}

void OutlineProfileMgrUIBase::DetermineNotchEighteenWidthPoints()
{
	m_mgrModel->DetermineNotchEighteenWidthPoints();
}

void OutlineProfileMgrUIBase::Display(Viewport* vp)
{
	glDisable( GL_LIGHTING );

	IwVector3d viewVec = m_owner->GetView()->GetViewingVector();

	glColor3ub( magenta[0], magenta[1], magenta[2] );
	glPointSize(5);
	DisplayNotchEighteenWidthPoints(viewVec);

	glColor3ub( red[0], red[1], red[2] );
	glPointSize(3);
	DisplayNonQualifiedLocations(viewVec);

	glLineWidth( 1.0 );

	if(!m_leftButtonDown)
	{
		IwPoint3d caughtPoint;
		int caughtIndex;
		if ( m_owner->GetMouseMoveCaughtPoint(caughtPoint, caughtIndex) )
		{
			if ( m_mgrModel->FeaturePointsInfo().GetAt(caughtIndex) == 1 ||
				 m_mgrModel->FeaturePointsInfo().GetAt(caughtIndex) == 2 ||
				 m_mgrModel->FeaturePointsInfo().GetAt(caughtIndex) == 11 ||
				 m_mgrModel->FeaturePointsInfo().GetAt(caughtIndex) == 12 )
			{	// the caught point is arc center, display its start/end points
				IwPoint3d sPnt = m_owner->GetMouseMoveSearchingList().GetAt(caughtIndex-1);
				sPnt = sPnt -50*viewVec; // move closer to the viewer
				IwPoint3d ePnt = m_owner->GetMouseMoveSearchingList().GetAt(caughtIndex+1);
				ePnt = ePnt -50*viewVec; // move closer to the viewer
				glBegin( GL_POINTS );
					glVertex3d( sPnt.x, sPnt.y, sPnt.z );
					glVertex3d( ePnt.x, ePnt.y, ePnt.z );
				glEnd();
			}
			else if ( m_mgrModel->FeaturePointsInfo().GetAt(caughtIndex) == 3 )
			{	// the caught point is arc start point, display its center/end points
				IwPoint3d cPnt = m_owner->GetMouseMoveSearchingList().GetAt(caughtIndex+1);
				cPnt = cPnt -50*viewVec; // move closer to the viewer
				IwPoint3d ePnt = m_owner->GetMouseMoveSearchingList().GetAt(caughtIndex+2);
				ePnt = ePnt -50*viewVec; // move closer to the viewer
				glBegin( GL_POINTS );
					glVertex3d( cPnt.x, cPnt.y, cPnt.z );
					glVertex3d( ePnt.x, ePnt.y, ePnt.z );
				glEnd();
			}
			else if ( m_mgrModel->FeaturePointsInfo().GetAt(caughtIndex) == 4 )
			{	// the caught point is arc end point, display its start/center points
				IwPoint3d sPnt = m_owner->GetMouseMoveSearchingList().GetAt(caughtIndex-2);
				sPnt = sPnt -50*viewVec; // move closer to the viewer
				IwPoint3d cPnt = m_owner->GetMouseMoveSearchingList().GetAt(caughtIndex-1);
				cPnt = cPnt -50*viewVec; // move closer to the viewer
				glBegin( GL_POINTS );
					glVertex3d( sPnt.x, sPnt.y, sPnt.z );
					glVertex3d( cPnt.x, cPnt.y, cPnt.z );
				glEnd();
			}
		}

		glPointSize(1);
	}

	glEnable( GL_LIGHTING );
}

void OutlineProfileMgrUIBase::DisplayNotchEighteenWidthPoints(IwVector3d viewVec)
{
	glBegin( GL_POINTS );
		IwPoint3d pnt;
		for ( unsigned i=1; i<m_mgrModel->NotchEighteenWidthPoints().GetSize(); i++ )
		{
			pnt = m_mgrModel->NotchEighteenWidthPoints().GetAt(i) - 20*viewVec;// make them closer to the viewer's eyes
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}
	glEnd();
}

void OutlineProfileMgrUIBase::DisplayNonQualifiedLocations(IwVector3d viewVec)
{
	if ( m_mgrModel->ValidateUnQualifiedLocations().GetSize() == 0 )
		return;

	glBegin( GL_POINTS );
		IwPoint3d pnt;
		for ( unsigned i=0; i<m_mgrModel->ValidateUnQualifiedLocations().GetSize(); i++ )
		{
			pnt = m_mgrModel->ValidateUnQualifiedLocations().GetAt(i) - 15*viewVec;// make them closer to the viewer's eyes
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}
	glEnd();
}

void OutlineProfileMgrUIBase::OnRefine()
{
	Refine();
}

bool OutlineProfileMgrUIBase::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	bool continuousResponse = true; 
	m_owner->SetPositionStart(cursor);
	m_leftButtonDown = true;

	if (m_manActType == MAN_ACT_TYPE_REVIEW) // no response if under review
		m_leftButtonDown = false;

	m_owner->ReDraw();

	return continuousResponse;
}

bool OutlineProfileMgrUIBase::MouseUp( const QPoint& cursor, Viewport* vp )
{
	m_leftButtonDown = false;

	m_owner->ReDraw();

	return true;
}

bool OutlineProfileMgrUIBase::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	bool continuousResponse = true;

	if (m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( m_addDeleteMode == 0 && m_owner->GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			m_owner->MoveCaughtPoint(cursor);
			continuousResponse = false;
		}
		m_owner->SetPositionLast(cursor);
	}
	else // call the owner's base MouseMove(); i.e. CManager::MouseMove(...);
		((CDefineOutlineProfile*)m_owner)->BaseMouseMove(cursor, keyModifier, vp);

	m_owner->ReDraw();

	return continuousResponse;
}

bool OutlineProfileMgrUIBase::MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp )
{
	if (m_owner->GetPositionStart() == cursor && m_manActType != MAN_ACT_TYPE_REVIEW) // no response if under review
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( m_owner->GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{		
			CDefineOutlineProfileUndoData prevUndoData, postUndoData;
			// Get the undo data before any changes
			GetDefineOutlineProfileUndoData(prevUndoData);

			if ( m_mgrModel->ConvertPoint(caughtIndex) )
			{
				// Get the undo data after changes
				GetDefineOutlineProfileUndoData(postUndoData);
				QUndoCommand *defineOutlineProfileCmd = new DefineOutlineProfileCmd(m_owner, prevUndoData, postUndoData);
				m_owner->GetUndoStack()->push( defineOutlineProfileCmd );
			}
            return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////
// This function is to reset the outline profile.
////////////////////////////////////////////////////////////////////
void OutlineProfileMgrUIBase::OnReset()
{
	int button = QMessageBox::warning( NULL, 
									QString("Outline Profile Reset!"), 
									QString("Outline Profile will be reset."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return;

	Reset();
}

void OutlineProfileMgrUIBase::OnAdd()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUIBase::OnAdd()") );

	QApplication::setOverrideCursor( Qt::PointingHandCursor );
	m_addDeleteMode = 1; // 0: none, 1:add, 2:delete

	((CDefineOutlineProfile*)m_owner)->EnableAddAction(false);
	m_owner->GetView()->Redraw();
}

void OutlineProfileMgrUIBase::OnDel()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUIBase::OnDel()") );

	QApplication::setOverrideCursor( Qt::PointingHandCursor );
	m_addDeleteMode = 2; // 0: none, 1:add, 2:delete

	((CDefineOutlineProfile*)m_owner)->EnableDelAction(false);
	m_owner->GetView()->Redraw();
}

void OutlineProfileMgrUIBase::OnAccept()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUIBase::OnAccept()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("OutlineProfileMgrUIBase");

	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile && m_manActType != MAN_ACT_TYPE_RESET)
	{
		if ( m_validateStatus == VALIDATE_STATUS_NOT_VALIDATE ) // use runtime m_validateStatus, rather than outlineProfile->GetValidateStatus()
		{
			OnValidate();
		}
	}

	// The latest feature points have been updated to outlineProfile, if any.
	// Therefore, here compare with the old feature points
	if ( !outlineProfile->GetUpToDateStatus() || m_mgrModel->IsMemberDataChanged())
		m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_OUTLINE_PROFILE, true, true);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();
}

void OutlineProfileMgrUIBase::OnCancel()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUIBase::OnCancel()") );

	if ( m_manActType == MAN_ACT_TYPE_EDIT )
		m_pDoc->EndTimeLog("OutlineProfileMgrUIBase");
	
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();

	if (outlineProfile)
	{
		if ( !m_oldOutlineProfileExist ) // Delete the newly created one.
		{
			m_pDoc->DeleteEntityById( outlineProfile->GetEntId() );
		}
		else // set back the old data
		{
			m_mgrModel->CancelChanges();
		}
	}
}

void OutlineProfileMgrUIBase::OnDispRuler()
{
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();

	if (outlineProfile)
	{
		m_displayRuler = !m_displayRuler;
		outlineProfile->SetDisplayOutlineProfileRuler(m_displayRuler);
	}
}

void OutlineProfileMgrUIBase::UpdateSketchSurface()
{
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if ( outlineProfile == NULL )
		return;

	IwBSplineSurface* skchSurf=NULL;
	if (CDefineOutlineProfile::DetermineSketchSurface(m_pDoc, skchSurf))
		outlineProfile->SetSketchSurface(skchSurf);

	//ShowSurface(m_pDoc, skchSurf, "skshSurf", blue);//PSV

}

void OutlineProfileMgrUIBase::Refine()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	bool activateUI = ( m_manActType == MAN_ACT_TYPE_EDIT );

	///////////////////////////////////////////////////////////
	CDefineOutlineProfileUndoData prevUndoData, postUndoData;
	// Get the undo data before any changes
	if ( activateUI )
		GetDefineOutlineProfileUndoData(prevUndoData);

	m_mgrModel->Refine(m_manActType);

	// Get the undo data after changes
	if ( activateUI )
	{
		GetDefineOutlineProfileUndoData(postUndoData);
		QUndoCommand *defineOutlineProfileCmd = new DefineOutlineProfileCmd(m_owner, prevUndoData, postUndoData);
		m_owner->GetUndoStack()->push( defineOutlineProfileCmd );
	}

	QApplication::restoreOverrideCursor();
}

void OutlineProfileMgrUIBase::Reset()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUIBase::Reset()") );

	m_mgrModel->DetermineInitialOutlineProfile();
	m_mgrModel->UpdateValidationPoints();

	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	((CDefineOutlineProfile*)m_owner)->SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE, m_validateStatus, m_validateMessage);
	m_mgrModel->ClearValidationData();

	// Change status sign
	bool activateUI = ( m_manActType == MAN_ACT_TYPE_EDIT );
	if ( activateUI )
		m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_OUTLINE_PROFILE, false, false);
}

void OutlineProfileMgrUIBase::DetermineOutlineProfile()
{
	m_mgrModel->DetermineInitialOutlineProfile();
}

void OutlineProfileMgrUIBase::MovePoint(int caughtIndex, IwPoint2d surfParam)
{
	CDefineOutlineProfileUndoData prevUndoData, postUndoData;
	// Get the undo data before any changes
	GetDefineOutlineProfileUndoData(prevUndoData);

	m_mgrModel->MovePoint(caughtIndex, surfParam);

	// Get the undo data after changes
	GetDefineOutlineProfileUndoData(postUndoData);

	QUndoCommand *defineOutlineProfileCmd = new DefineOutlineProfileCmd(m_owner, prevUndoData, postUndoData);
	m_owner->GetUndoStack()->push( defineOutlineProfileCmd );
}

bool OutlineProfileMgrUIBase::AddPoint(IwPoint2d surfParam)
{
	QApplication::restoreOverrideCursor();
	m_addDeleteMode = 0;
	((CDefineOutlineProfile*)m_owner)->EnableDelAction(true);
	((CDefineOutlineProfile*)m_owner)->EnableAddAction(true);

	CDefineOutlineProfileUndoData prevUndoData, postUndoData;
	// Get the undo data before any changes
	GetDefineOutlineProfileUndoData(prevUndoData);

	if (m_mgrModel->AddPoint(surfParam)>=0) // if -1, not insert
	{
		// Get the undo data after changes
		GetDefineOutlineProfileUndoData(postUndoData);
		QUndoCommand *defineOutlineProfileCmd = new DefineOutlineProfileCmd(m_owner, prevUndoData, postUndoData);
		m_owner->GetUndoStack()->push( defineOutlineProfileCmd );
		return true;
	}
	return false;
}

void OutlineProfileMgrUIBase::DeletePoint(int caughtIndex)
{
	QApplication::restoreOverrideCursor();
	m_addDeleteMode = 0; // none
	((CDefineOutlineProfile*)m_owner)->EnableDelAction(true);

	if (caughtIndex > -1)
	{
		CDefineOutlineProfileUndoData prevUndoData, postUndoData;
		// Get the undo data before any changes
		GetDefineOutlineProfileUndoData(prevUndoData);

		if ( m_mgrModel->DeletePoint(caughtIndex) ) 
		{
			// Get the undo data after changes
			GetDefineOutlineProfileUndoData(postUndoData);
			QUndoCommand *defineOutlineProfileCmd = new DefineOutlineProfileCmd(m_owner, prevUndoData, postUndoData);
			m_owner->GetUndoStack()->push( defineOutlineProfileCmd );
		}
	}
}

/////////////////////////////////////////////////////////////////
// This function currently only refines the anterior outer points
// the 2 points above the ant/ant-chamfer cut edge.
/////////////////////////////////////////////////////////////////
void OutlineProfileMgrUIBase::RefineAnteriorOuterPoints
(
	IwTArray<IwPoint3d>& cutFeatPnts,	// I:
	IwTArray<IwPoint3d>& antOuterPnts	// I/O:
)
{
	m_mgrModel->RefineAnteriorOuterPoints(cutFeatPnts, antOuterPnts);
}

void OutlineProfileMgrUIBase::GetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData)
{
	undoData.featurePoints.RemoveAll();
	undoData.featurePointsInfo.RemoveAll();
	undoData.featurePointsRemap.RemoveAll();

	undoData.featurePoints.Append(m_mgrModel->FeaturePoints());
	undoData.featurePointsInfo.Append(m_mgrModel->FeaturePointsInfo());
	undoData.featurePointsRemap.Append(m_mgrModel->FeaturePointsRemap());
}

void OutlineProfileMgrUIBase::SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData)
{
	m_mgrModel->SetModelData(undoData.featurePoints, undoData.featurePointsInfo, undoData.featurePointsRemap);

	COutlineProfile*		outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile)
	{
		// We need to call SetOutlineProfileFeaturePoints() and GetOutlineProfileFeaturePoints()
		// such that all the m_currFeaturePoints are in the correct positions, especially for
		// the arc end points.
		m_mgrModel->SetOutlineProfileFeaturePoints();
		//GetOutlineProfileFeaturePoints()
		//happens in the child's method

		// Whenever parameters are changed, the validation status is invalid.
		m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
		m_validateMessage = QString("Not validate.");
		((CDefineOutlineProfile*)m_owner)->SetValidateIcon(m_validateStatus, m_validateMessage);
		m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE, m_validateStatus, m_validateMessage);
		m_mgrModel->ClearValidationData();
	}
}

void OutlineProfileMgrUIBase::OnValidate()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	m_validateStatus = Validate(m_pDoc, m_validateMessage, m_mgrModel->ValidateUnQualifiedLocations());

	QApplication::restoreOverrideCursor();

	((CDefineOutlineProfile*)m_owner)->SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE, m_validateStatus, m_validateMessage);
	m_pDoc->GetOutlineProfile()->SetValidateStatus(m_validateStatus, m_validateMessage);
}
