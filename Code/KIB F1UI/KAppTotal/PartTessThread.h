#pragma once

#include <QtGui>
#include <QThread>
#include "Part.h"

class CPartTessThread : public QThread
	{
public:
	CPartTessThread( CEntity* pPart, int visibility = 0 );// 0: tess all, 1: tess visible only, 2: tess invisible only
	CPartTessThread( QList< CEntity* >& partList, int visibility = 0 ); // 0: tess all, 1: tess visible only, 2: tess invisible only
	~CPartTessThread();

	void			run();
	bool			MakeTess();

signals:

private:
	int					m_visibility;
	CEntity*			m_part;
	QList< CEntity* >	m_partList;
	};