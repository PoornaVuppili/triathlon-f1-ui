#ifndef TLINE2_H
#define TLINE2_H

#include "TPoint2.h"

template <typename T>
class TLine2
{
protected:
    // Line: n.x * x + n.y * y + c = 0
    TPoint2<T> n;   // normal
    T c;            // constant

public:
    ///////////////////////////////////////////////////////////////////////
    // Function name:    TLine2
    // Function purpose: Constructor
    // Input: the coordinates of two points
    // Output: None
    ///////////////////////////////////////////////////////////////////////
    TLine2(T x1, T y1, T x2, T y2)
    { 
        n.x() = y2-y1;
        n.y() = x1-x2;
        n.normalize();
        c = -n.x() * x1 - n.y() * y1;
    }

    TLine2(const TPoint2<T>& p1, const TPoint2<T>& p2)
    { 
        n.x() = p2.y()-p1.y();
        n.y() = p1.x()-p2.x();
        n.normalize();
        c = -n.x() * p1.x() - n.y() * p1.y();
    }

    TPoint2<T> GetNormal() const { return n; }
    T GetConstant() const { return c; }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    SignedDistance
    // Function purpose: Compute the signed distance from a point to the line
    // Input: the point coordinate
    // Output: the signed distance
    ///////////////////////////////////////////////////////////////////////
    T SignedDistance(const TPoint2<T>& pt)
    {
        return n.dot(pt) + c;
    }

    T SignedDistance(T x, T y)
    {
        return n.dot(TPoint2<T>(x,y)) + c;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    SignedDistance
    // Function purpose: Compute the Euclidean distance from a point to the line (>=0)
    // Input: the point coordinate
    // Output: the Euclidean distance
    ///////////////////////////////////////////////////////////////////////
    T EuclideanDistance(const TPoint2<T>& pt)
    {
        return fabs( SignedDistance(pt) );
    }

    T EuclideanDistance(T x, T y)
    {
        return fabs( SignedDistance(x,y) );
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    WhichSide
    // Function purpose: Determine which side is the given point relative
    //    to the line
    // Input: the point coordinate
    // Output: 0 (on the line) 1 (positive side) -1 (negative side)
    ///////////////////////////////////////////////////////////////////////
    int WhichSide(T x, T y)
    {
        return Sign( SignedDistance(x,y) );
    }

    int WhichSide(const TPoint2<T>& pt)
    { 
        return Sign( SignedDistance(pt) );
    }

    // Test if two lines are parallel
    bool IsParallel(const TLine2<T>& ln, T tol = NumTraits<T>::dummy_precision()) const
    {
        return fabs(1-fabs(n.dot(ln.GetNormal())))<tol;
    }

    // Test if two lines are colinear
    bool IsColinear(const TLine2<T>& ln, T tol = NumTraits<T>::dummy_precision()) const
    {
        T v = n.dot(ln.GetNormal());
        return v>0 ? fabs(1-v)<tol && fabs(ln.GetConstant()-c)<tol :
                     fabs(1+v)<tol && fabs(ln.GetConstant()+c)<tol;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Intersect
    // Function purpose: Find the point of intersection between two lines
    // Input:
    //   line: the other line 
    // Output:
    //   intersectPoint: the point of intersection if exists
    // Return:
    //   an intersect flag
    ///////////////////////////////////////////////////////////////////////
    int Intersect(const TLine2<T>& line, TPoint2<T>& intersectPoint, T tol = NumTraits<T>::dummy_precision()) const
    {
        TPoint2<T> nn = line.GetNormal();
        T nc = line.GetConstant();

        T v = n.dot(nn);

        if( v>0 && fabs(1-v)<tol)
            return fabs(nc-c)<tol ? IntersectFlag::Coincident : IntersectFlag::Parallel;
        else if( v<0 && fabs(1+v)<tol )
            return fabs(nc+c)<tol ? IntersectFlag::Coincident : IntersectFlag::Parallel;

        // use double for computation internally, and convert to type T at return
        double n1 = n.x();
        double n2 = n.y();
        double nn1 = nn.x();
        double nn2 = nn.y();

        intersectPoint.x() = (T)(-c*nn2+nc*n2)/(n1*nn2-nn1*n2);
        intersectPoint.y() = (T)(-c*nn1+nc*n1)/(n2*nn1-nn2*n1);

        return IntersectFlag::Intersect;
    }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TLine2<float>   Line2Float;
typedef TLine2<double>  Line2Double;

#endif //TLINE2_H
