#pragma once

#include <QWidget>
#include <QDialog>

class QTableWidget;
class QString;
class QLabel;
class QButtonGroup;
class QCheckBox;
class QTableWidget;
class QPushButton;
class QTableWidgetItem;
class QLineEdit;


class CFileNewDlg : public QDialog
	{
	Q_OBJECT

public:
	CFileNewDlg(QWidget * parent = 0);

	QString				GetSelectedCaseDir();

private slots:
	void				OnSearchEdited(const QString& txt);
	void				OnCaseChanged();
	void				OnItemDoubleClicked(QTableWidgetItem *);
	void				OnOK();

private:
	void				FillCasesLists( const QString& sDir );
	QTableWidget*				m_listCases;

	QLineEdit*					m_lineEditSearch;

	QPushButton*				m_buttonOK;
	QPushButton*				m_buttonCancel;

	QString						m_sUNIDir;
	};
