#include "DocReader.h"
#include "DataDoc.h"
#include <QtGui>
#include <QApplication>
#include <QMessageBox>

CDocReader::CDocReader( CDataDoc* pDoc, const QString& sDocFolder )
{
	m_pDoc			= pDoc;
	m_sDocFolder	= sDocFolder;
}


CDocReader::~CDocReader()
{
}

QString CDocReader::GetKeyValue(const QString& sDocFileName, QString key)
{
	QString					keyValue = QString("");
	QString					sLine; 
	QString					sTag, sValue;
	QFile					File( sDocFileName );
	QTextStream				in( &File );
	bool					bOK;

	bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	if( !bOK )
		return keyValue;

	// Read through the file.
	while( !in.atEnd() )
	{
		sLine = in.readLine();

		if( sLine == "" )
			continue;

		if ( ParseInputLine( sLine, sTag, sValue ) )
		{
			if ( sTag == key )
			{
				keyValue = sValue;
				break;
			}
		}
	}

	File.close();

	return keyValue;
}

bool CDocReader::ParseInputLine( const QString& sLine, QString& sTag, QString& sValue )
	{

	int			idx;
	QRegExp		rxNonSpaceOrTab( "[^ \t]" );

	idx = sLine.indexOf( ':' );

	if( idx < 0 )
		return false;

	sTag = sLine.left( idx );

	sValue = sLine.mid( idx + 1 );

	idx = sValue.indexOf( rxNonSpaceOrTab );

	sValue = sValue.mid( idx );

	return true;
	}


void CDocReader::InitEntity()
	{
	m_bEntity		= false;
	m_eEntRole		= ENT_ROLE_NONE;
	m_eEntityType	= ENT_TYPE_NONE;
	m_sFileName		= "";
	m_ePartType		= PART_TYPE_NONE;
	m_eDisplayMode	= DISP_MODE_NONE;
	}

////////////////////////////////////////////////////////////////////
// This function loads all the info in header.txt, but not entities.
////////////////////////////////////////////////////////////////////
bool CDocReader::LoadHeaderFileInfo()
{
	m_pDoc->AppendLog( QString("CDocReader::LoadHeaderFileInfo()") );

	QString					sLine;
	QFile					File( m_sDocFolder + "\\header.txt" );
	QTextStream				in( &File );
	bool					bOK;
	CPart*					pPart;
	CEntity*				pEntity;
	QString					sTag, sValue;
	int						r, g, b;
	float					vInfo1[12], vInfo2[7];
	char					buf[160];
	CColor					color;
	int						m_nEntityId;

	// initialize 
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE; // this parameter was introduced in v4.0.0 Old files do not have this value.

	bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	if( !bOK )
		return false;

	InitEntity();


	sLine = in.readLine();	// read format version
	sLine = in.readLine();	// empty lines

	// BETAVERSION
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag.isEmpty() || (sTag == "BETAVERSION" && sValue.toInt() == 1) )
	{
		// If the current system is not a beta version, but the file is saved by a beta version
		if ( m_pDoc->isProductionSoftware() )
		{
			char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
			if (underDevelopment == NULL || QString("0") == underDevelopment)
			{
				File.close();
				
				QApplication::restoreOverrideCursor();
				int		button;
				//button = QMessageBox::warning( NULL, 
				//								QString("Beta Version!"), 
				//								QString("File was saved in a beta version.\nIt can not be opened by the production software."), 
				//								QMessageBox::Ok);	
				//return false;
			}
		}
	} 
	// RELEASE
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag == "RELEASE" )
	{
		int rel;
		int dotPos = sValue.indexOf(".");
		if ( dotPos == -1 )// no dot
		{
			rel = sValue.toInt();
		}
		else // some beta version files contain "." to saperate implant version vs jigs version. We do not support such files anymore.
		{
			File.close();
				
			QApplication::restoreOverrideCursor();
			int		button;
			button = QMessageBox::warning( NULL, 
											QString("Beta Version!"), 
											QString("File was saved in an early beta version.\nIt can not be opened by the software."), 
											QMessageBox::Ok);	
			return false;			
		}
		m_pDoc->SetOpeningFileReleaseNumber( rel );
	} 
	// REVISION
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag == "REVISION" )
	{
		int rev = sValue.toInt();
		m_pDoc->SetOpeningFileRevisionNumber( rev );
	} 
	// DATAFILEVERSION data file version
	// set default value for the old data files.
	m_pDoc->SetOpeningFileDataFileVersionNumber( 0 );
	// Only available in v4.0.0
	if ( m_pDoc->IsOpeningFileRightVersion(4,0,0) )
	{
		sLine = in.readLine();	
		ParseInputLine( sLine, sTag, sValue );
		if( sTag == "DATAFILEVERSION" )
		{
			int v = sValue.toInt();
			m_pDoc->SetOpeningFileDataFileVersionNumber( v );
		} 
	}
	// Here get the read-in release, revision, and version numbers
	int Release, Revision, DFVersion;
	m_pDoc->GetOpeningFileReleaseNumber(Release);
	m_pDoc->GetOpeningFileRevisionNumber(Revision);
	m_pDoc->GetOpeningFileDataFileVersionNumber(DFVersion);
	// Check whether file is in old format, v1.8 and earlier.
	if ( Release<= 1 && Revision <=8 )
	{
		QApplication::restoreOverrideCursor();
		int		button;
		button = QMessageBox::warning( NULL, 
										QString("Old File Version!"), 
										QString("TriathlonF1 file was saved in an old format.\nThis version of software can not open the file."), 
										QMessageBox::Ok);	
		return false;
	}
	// check whether file is in old format
	if ( Release < ITOTAL_RELEASE ||
		 (Release == ITOTAL_RELEASE &&
		  Revision < ITOTAL_REVISION) )
	{ 
		{
			QApplication::restoreOverrideCursor();
			QApplication::restoreOverrideCursor();
			int		button;
			button = QMessageBox::warning( NULL, 
											QString("Old File Version!"), 
											QString("TriathlonF1 file was saved in an old format.\nThis version of software may not be able to open it correctly.\nDo you want to open the file?"), 
											QMessageBox::Yes, QMessageBox::No);	
			if ( button == QMessageBox::No )
			{
				return false;
			}
			m_pDoc->GetView()->Redraw();
			QTimer::singleShot(150, m_pDoc->GetView(), SLOT(OnDoNothing()));// Let's wait for OS update.
			QApplication::setOverrideCursor( Qt::WaitCursor );
		}
	}
	// BUILD, only available in v1.9 and later versions. But no longer available in iTW6.
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag == "BUILD" )
	{
		QString bld = sValue;
		//m_pDoc->SetOpeningFileBuildNumber( bld );
	} 

	// Check whether the file is a future version.
	bool fileSavedByNewerVersion = false;
	if ( Release > ITOTAL_RELEASE )
	{
		fileSavedByNewerVersion = true;
	}
	if ( Release == ITOTAL_RELEASE &&
		 Revision > ITOTAL_REVISION )
	{
		fileSavedByNewerVersion = true;
	}
	if ( Release == ITOTAL_RELEASE &&
		 Revision == ITOTAL_REVISION &&
		 DFVersion > ITOTAL_DATAFILE_VERSION)
	{
		fileSavedByNewerVersion = true;
	}
	if ( fileSavedByNewerVersion )
	{
		File.close();
		
		QApplication::restoreOverrideCursor();
		int		button;
		button = QMessageBox::warning( NULL, 
										QString("Future File Version!"), 
										QString("TriathlonF1Wroks file is saved in a future version.\nYou need to upgrade software to open this file."), 
										QMessageBox::Ok);	
		return false;
	}

	sLine = in.readLine();	// empty lines

	// DESIGN STATUS
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag == "DESIGNSTATUS" ) // obsoleted
	{}

	// Read through the file.
	while( !in.atEnd() )
	{
		sLine = in.readLine();

		if( sLine == "" )
			continue;

		bOK = ParseInputLine( sLine, sTag, sValue );

		if( sTag == "USER HISTORY" )
		{
			m_pDoc->AddPreviousUserInfo( sValue );
		} 
		else if( sTag == "PATIENT ID" )
		{
			if ( Release < 1 ||
				 (Release == 1 && Revision == 0) )
			{
				sValue = QString("000") + sValue;
			}
			//m_pDoc->SetPatientId( sValue ); //no need in v6
		}
		else if( sTag == "IMPLANT SIDE" )
		{
			//m_pDoc->SetImplantSide( sValue ); //no need in v6
		}
		else if( sTag == "IMPLANT ZONE" )
		{
			//m_pDoc->SetImplantZone( sValue ); //no need in v6
		}
	}

	File.close();

	return true;

}


/////////////////////////////////////////////////////////////
// This function returns the data file version info. It never 
// set them to m_pDoc.
// Note, bld is no longer available in iTW6.
/////////////////////////////////////////////////////////////
bool CDocReader::ReadDataFileVersionInfo(QString& type, int& release, int& revision, int& dataFileVersion)
{
	QString					sLine;
	QFile					File( m_sDocFolder + "\\header.txt" );
	QTextStream				in( &File );
	bool					bOK;
	QString					sTag, sValue;
	CColor					color;

	bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	if( !bOK )
		return false;


	sLine = in.readLine();	// read format version
	bool bPS = sLine.contains("TriathlonF1PS", Qt::CaseInsensitive);
	if ( bPS )
		type = QString("PS");
	else
		type = QString("CR");

	sLine = in.readLine();	// empty lines

	// BETAVERSION
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );

	// RELEASE
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag == "RELEASE" )
	{
		release = sValue.toInt();
	} 
	// REVISION
	sLine = in.readLine();	
	ParseInputLine( sLine, sTag, sValue );
	if( sTag == "REVISION" )
	{
		revision = sValue.toInt();
	} 
	// DATAFILEVERSION data file version
	// set default value for the old data files.
	dataFileVersion = 0;
	// Only available in v4.0.0
	if ( release>=4 )
	{
		sLine = in.readLine();	
		ParseInputLine( sLine, sTag, sValue );
		if( sTag == "DATAFILEVERSION" )
		{
			dataFileVersion = sValue.toInt();
		} 
	}

	// BUILD, only available in v1.9 and later versions. But no longer available in iTW6
	//sLine = in.readLine();	
	//ParseInputLine( sLine, sTag, sValue );
	//if( sTag == "BUILD" )
	//{
	//	bld = sValue;
	//} 

	File.close();

	return true;
}