#include "KLockFile.h"
#include <QFile>

KLockSingleFile::KLockSingleFile(bool removeWhenRelease) : remove(removeWhenRelease), handle(INVALID_HANDLE_VALUE)
{
}

KLockSingleFile::~KLockSingleFile()
{
    Release();
}

bool KLockSingleFile::Create(QString path)
{
    HANDLE hFile = CreateFile( path.toUtf8(),
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ,// allow subsequent read
        NULL,           // no security attributes
        CREATE_ALWAYS,  // creating a new file
        0,              // not overlapped index/O
        NULL);
    if (hFile == INVALID_HANDLE_VALUE)
        return false;

    // release the previous lock
    Release();

    // write the user name to the file
    QString name;
    char *value;
    size_t len;
    if( _dupenv_s( &value, &len, "USERNAME" ) == 0 )
    {
        name = value;
        free(value);
    }
   
    DWORD dwNumBytesWritten = 0;
    WriteFile(hFile, name.toUtf8(), name.length(), &dwNumBytesWritten, NULL);
    FlushFileBuffers(hFile);

    OVERLAPPED ov;
    ov.Offset = 0;
    ov.OffsetHigh = 0;

    if( LockFileEx(hFile, LOCKFILE_FAIL_IMMEDIATELY, 0, 0, 100, &ov) )
    {
        filePath = path;
        handle = hFile;
        return true;
    }
    else
    {
        CloseHandle(hFile);
        DeleteFile(path.toUtf8());
        return false;
    }
}

bool KLockSingleFile::Release()
{
    OVERLAPPED ov;
    ov.Offset = 0;
    ov.OffsetHigh = 0;

    if( !QFile::exists(filePath) ||
        !UnlockFileEx(handle, 0, 0, 100, &ov) )
        return false;

    CloseHandle(handle);
    if( remove )
        DeleteFile(filePath.toUtf8());
    
    filePath = "";
    handle = INVALID_HANDLE_VALUE;
    return true;
}

