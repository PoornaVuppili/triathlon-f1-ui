#ifndef KIMPLANTFOLDER_H
#define KIMPLANTFOLDER_H

#include <QPair>
#include <QList>
#include <QString>

class QWidget;

////////////////////////////////////////
// All header files of the lib
////////////////////////////////////////

////////////////////////////////////////
// Global functions
////////////////////////////////////////

// Get the list of subdirectories of a given directory, reimplemented using Windows API
QStringList KGetSubdirListOf(QString const &folder);


// Create the implant folder structure. This function prompts a dialog
// that allows the user to enter the case number and implant type information.
// Then a folder is created based on the entered case #, together with a set of subfolders:
//      CM
//          FEMUR
//      CT
//      Segmentation
//      SolidWorks
//      Surfaces
//      STL
// 
// Parameters:
//   basePath (I) : the root folder where a new case is created
//   result   (O) : if the implant folder is created successfully, this returns
//                  pairs of strings containing the implant information including:
//                      result[0] : key = ImplantPath, value = implant case #
//                      result[1] : key = ImplantType, value =  iUni, iDuo or TriathlonF1
//                      result[2] : key = Knee       , value =  Left or Right
//                  if the implant type isn't TriathlonF1
//                      result[3] : key = Compartment, value =  Medial or Laterial 
//                  additional fields: Country, Zone
//   parent   (I, optional) : the parent widget for the dialog
// Return:
//   true: successful; false : failed.
bool KCreateImplantFolder(const QString& basePath, QList<QPair<QString, QString> >& result, QWidget* parent=NULL);

#endif // KIMPLANTFOLDER_H
