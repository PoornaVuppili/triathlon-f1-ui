#ifndef TPOINT2_H
#define TPOINT2_H

#include "MathOp.h"
#include "Eigen/Core"
using namespace Eigen;

template <typename T>
class TPoint2 : public Matrix<T,2,1>
{
public:
    TPoint2() : Matrix(0,0) {}
    TPoint2(T a, T b) : Matrix(a,b) {}

    // construct TPoint2 from Matrix expressions
    template<typename derived>
    TPoint2(const MatrixBase<derived>& other)
        : Matrix(other) { }

    // Assign Matrix expressions to TPoint2
    template<typename derived>
    TPoint2& operator = (const MatrixBase<derived>& other)
    {
        Matrix::operator=(other);
        return *this;
    }

    T Distance(T xv, T yv) const
    {
        xv -= x();
        yv -= y();
        return std::sqrt(xv*xv + yv*yv);
    }

    template<typename Derived>
    T Distance(const MatrixBase<Derived>& pt) const
    {
        T xv = pt.x() - x();
        T yv = pt.y() - y();
        return std::sqrt(xv*xv + yv*yv);
    } 
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TPoint2<int>    Point2Int;
typedef TPoint2<float>  Point2Float;
typedef TPoint2<double> Point2Double;

#endif //TPoint2_H
