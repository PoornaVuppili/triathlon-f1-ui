#pragma once

#include "..\KApp\EntityView.h"
#include "..\KApp\ViewWidget.h"
#include <QtGui>
#include <QStack>
#include <QKeyEvent>
#include "TriathlonF1Definitions.h"
#include "Basics.h"
#include "Manager.h"
#include "Globals.h"

class CDataDoc;
class CMainWindow;
class CRangeSlider;

enum CMouseMode
	{ 
	MOUSE_MODE_NONE, 
	MOUSE_MODE_TRANSLATE,
	MOUSE_MODE_SCALE,
	MOUSE_MODE_ROTATE,
	MOUSE_MODE_SPIN,
	};


enum CPredefindedView
	{ 
	NONE_VIEW,
	FRONT_VIEW, 
	FRONT_VIEW_PLUS, 
	BACK_VIEW,				// For Femur 
	BACK_VIEW_PLUS,			// For Femur
	TOP_VIEW, 
	TOP_VIEW_PLUS, 
	BOTTOM_VIEW, 
	BOTTOM_VIEW_PLUS, 
	LEFT_VIEW, 
	LEFT_VIEW_PLUS, 
	RIGHT_VIEW, 
	RIGHT_VIEW_PLUS, 
	ISO1_VIEW, 
	ISO1_VIEW_PLUS, 
	ISO2_VIEW, 
	ISO2_VIEW_PLUS 
	};


struct CClipStatus
	{
	bool		bEnabled;
	double		left[3];
	double		right[3];

	CClipStatus()
		{
		bEnabled = true;
		Init();
		}

	void Init()
		{
		for( int i = 0; i < 3; i++ )
			{
			left[i] = 0.0;
			right[i] = 1.0;
			}
		}
	};


struct CClipPlane
	{
	bool		bActive;
	double		eqn[4];

	CClipPlane()
		{
		Init();
		}

	void Init()
		{
		bActive = false;
		eqn[0] = eqn[1] = eqn[2] = eqn[3] = 0.0;
		}
	};


class COpenGLView : public EntityView
	{
    Q_OBJECT

public:
    COpenGLView(QObject *parent = 0);
    virtual ~COpenGLView();

	void				SetDoc( CDataDoc* pDoc ) {m_pDoc=pDoc;};
	CDataDoc*			GetDoc() {return m_pDoc;};

	void				SetMainWindow( CMainWindow* pMainWindow ) {m_pMainWindow=pMainWindow;};
	CMainWindow*		GetMainWindow() {return m_pMainWindow;};

	virtual IwExtent3d	GetShowRegion();
    virtual void		Draw();
    virtual void        DrawTransparent();

	virtual void		DisplayPoint( const IwPoint3d& pt, const CColor& color );
	virtual CBounds3d	GetDocBounds();
	virtual CBounds3d	ComputeDocBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL );
	virtual bool		IsDocEmpty();
	virtual bool		IsDocModified();

	void				UpdateShowRegion();
	ViewWidget*			GetViewWidget() {return viewWidget;};
	CManager*			GetManager();
	void				SetManager(CManager* manager, bool stacking = true);
	void				PushManagerInStack(CManager*& manager);
	CManager*			PopManagerFromStack();
	CManager*			GetLastManager();
	Manager*			GetGlobalManager() {return viewWidget->GetManager();};// Global manager could be this local module manager or other module manager. 
	void				ClearView();
	void				ClearManagers();
	void				SetPrompt( const QString& sText );

	void				SetCursorAndPrompt( CCursorType eCursorMode, QString sPrompt );
	void				ClearCursorAndPrompt();
	void				SetCursor( CCursorType eCursorType );
	CCursorType			GetCursorType();

	void				SetSelectionMode( const QString& sText );
	void				SetQuestionMode( const QString& sText );
	void				SetSketchMode( const QString& sText );
	
	void				Redraw();
	void				DisplayPrompt();

	void				Enter2dMode( bool bFrontBuffer = false );
	void				Restore3dMode();

	void				SetXOR();
	void				ClearXOR();

	void				Zoom( QPoint point0, QPoint point1 );

	void				SetPredefinedView( int eView, double scale = 0.9, bool doPlusView=true );

	void				XYZtoUV( IwPoint3d& pt, int uv[2] );
	void				XYZtoUVW( IwPoint3d& pt, int uvw[3] );
	void				UVtoXYZ( int uv[2], IwPoint3d& pt );
	void				UVWtoXYZ( int uvw[3], IwPoint3d& pt );
	void				UVtoXYZ( QPoint point, IwPoint3d& pt );
	void				UVto2D( QPoint point, double& x, double& y );
	void				UVto2D( int uv[2], double& x, double& y );
	void				SetDebugFlag( bool bFlag );
	void				DrawMarker( IwPoint3d& pt );
	void				SetAdditionViewingRotations( double xRot, double yRot, double zRot) {m_AdditionViewingRotations = IwPoint3d(xRot, yRot, zRot);};

	IwVector3d			GetViewingVector(int screenDir=2, bool unitized=true);
	IwVector3d			GetLayerOffsetVector(int layerNo=0, IwPoint3d pnt=IwPoint3d());
	void				SetClipPlaneInfo(double* eqn);
	virtual void		SetMotionStudioMatrix(IwAxis2Placement const& matrix);
	virtual bool		GetMotionStudioMatrix(IwAxis2Placement& matrix);

    // Pick a point for measurement
    virtual bool Measure(const PickRayFloat& ray, float tolerance, Point3Float& pt);
	virtual bool Measure(const PickRayFloat& ray, float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
	// to support intersection with image view
	void				ClearIntersectionDisplayListAndPlaneInfo(int index);
	void				DisplayIntersection(int startIndex, int endIndex, IwPoint3d planePnt, IwVector3d planeNormal);
	void				CreateDisplayListsForEntitiesCrossSections(int startIndex, IwPoint3d planePnt, IwVector3d planeNormal);

public slots:
	void				OnTessellationHigh();
	void				OnTessellationFine();
	void				OnTessellationNormal();
	void				OnTessellationCoarse();

	void				OnTransparencyTransparentTotal();
	void				OnTransparencyTransparentPlus();
	void				OnTransparencyTransparent();
	void				OnTransparencyNormal();
	void				OnTransparencyOpaque();
	void				OnTransparencyOpaquePlus();

	void				OnArrowKeyRotationPP();
	void				OnArrowKeyRotationP();
	void				OnArrowKeyRotation();
	void				OnArrowKeyRotationM();
	void				OnArrowKeyRotationMM();
	void				OnArrowKeyRotationMMM();

protected:
	void				SetClipPlane(bool enable=true);

protected:
	CDataDoc*				m_pDoc;
	CMainWindow*			m_pMainWindow;


	IwPoint3d				m_AdditionViewingRotations;//To change the predefined view 
	
	CPredefindedView		m_preDefinedView;

	CManager*				m_pManager;
	QStack< CManager* >		m_pManagerStack;

	CCursorType				m_eCursorType;
	QString					m_sPrompt;

	bool					m_bFrontBuffer;
	bool					m_bDebugFlag;
	double					m_clipPlaneEqn[4];

	// to support visual transformation of femur components.
	double					m_motionStudioMatrix[16];

	// to support intersection with image view
	IwTArray<GLuint>		m_intersectionDisplayLists;	// display lists for (number of series view)X(number of displayed entities)
	IwTArray<CColor>		m_intersectionDisplayColors;// one color for one display list
	IwTArray<IwPoint3d>		m_intersectionPlanesInfo;	// the image view plane info for each display list (origin+normal)
	int						m_intersectedEntities;		// how many entities been intersected
	};
