#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "ImportOsteophyteSurface.h"
#include "FemoralPart.h"
#include "IFemurImplant.h"
#include "AdvancedControlJigs.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"
#include "..\KApp\SwitchBoard.h"
#include "..\KAppTotal\Utilities.h"

CImportOsteophyteSurface::CImportOsteophyteSurface( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
	bool activateUI = (manActType==MAN_ACT_TYPE_EDIT);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CImportOsteophyteSurface";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;

	if ( activateUI )
	{
		m_toolbar = new QToolBar("Import Osteophyte Surf");
		m_actImportOsteophyteSurf = m_toolbar->addAction("Osteophyte");
		SetActionAsTitle( m_actImportOsteophyteSurf );
		// Reset
		m_actReset = m_toolbar->addAction("Reload");
		EnableAction( m_actReset, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );// No Cancel

	}

	CFemoralPart* osteophyteSurf = m_pDoc->GetOsteophyteSurface();
	if ( osteophyteSurf == NULL  ) // if no object
	{
		// Reset is the way to import the osteophyte surface.
		Reset( activateUI );
	}
	else
	{
		// Load the parameters
	}

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	if ( !activateUI )
		OnAccept();

}

CImportOsteophyteSurface::~CImportOsteophyteSurface()
{
	SwitchBoard::removeSignalSender(this);
}

void CImportOsteophyteSurface::CreateOsteophyteSurfaceObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_OSTEOPHYTE_SURFACE) == NULL )
	{
		CFemoralPart* pOsteophyteSurf = new CFemoralPart(pDoc, ENT_ROLE_OSTEOPHYTE_SURFACE);
		pDoc->AddEntity(pOsteophyteSurf, true);
	}

	return;
}

void CImportOsteophyteSurface::OnReset()
{
	int button = QMessageBox::warning( NULL, 
									QString("Osteophyte Reset!"), 
									QString("Osteophyte will be reloaded."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return;

	Reset();
}

//////////////////////////////////////////////////////////////////////
// Reset is the way to import osteophyte surface
//////////////////////////////////////////////////////////////////////
void CImportOsteophyteSurface::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CImportOsteophyteSurface::Reset()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	if ( m_pDoc->GetOsteophyteSurface() )
	{
		if (underDevelopment != NULL && QString("1") == underDevelopment)
		{
			QApplication::restoreOverrideCursor();
			int	button = QMessageBox::information(mainWindow, "Info", "Do you want to delete all jigs design features?", QMessageBox::Yes, QMessageBox::No);
			if (button == QMessageBox::Yes)
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
				m_pDoc->DeleteEntitiesBelongToJigs();
			}
			else
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
			}
		}
		else
		{
			QApplication::restoreOverrideCursor();
			int	button = QMessageBox::information(mainWindow, "Info", "Reload will delete all jigs design features. Do you want to continue?", QMessageBox::Yes, QMessageBox::No);
			if (button == QMessageBox::Yes)
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
				m_pDoc->DeleteEntitiesBelongToJigs();
			}
			else
			{
				return;
			}
		}
	}

    // Check if parts are available
    Implant* implant = mainWindow->GetImplant();

	IwBrep *femurInnerJigBrep = NULL;

	QString surfDir = mainWindow->GetImplant()->GetInfo("SurfDir");
	QString ImplantName = mainWindow->GetImplant()->GetInfo("ImplantName");
	QString fileName = surfDir + QDir::separator() + ImplantName + QString("_Femur_Inner_jig.IGS");
	femurInnerJigBrep = ReadIGES(fileName);

	if (underDevelopment != NULL && QString("1") == underDevelopment)
	{
		if(femurInnerJigBrep==NULL )
		{
			QApplication::restoreOverrideCursor();
			
			QApplication::setOverrideCursor( Qt::WaitCursor );
			// Read IGES
			surfDir = mainWindow->GetImplant()->GetInfo("SurfDir");
			ImplantName = mainWindow->GetImplant()->GetInfo("ImplantName");
			fileName = surfDir + QDir::separator() + ImplantName + QString("_Femur_Inner_jig.IGS");
			femurInnerJigBrep = ReadIGES(fileName);
		}
	}

    if(femurInnerJigBrep==NULL )
    {
		QApplication::restoreOverrideCursor();
        QMessageBox::information(mainWindow, "Error", "Missing the surface");
        return;
    }

	bool bOK = m_pDoc->ImportPart( femurInnerJigBrep, ENT_ROLE_OSTEOPHYTE_SURFACE );
	if ( !bOK ) // Need to check whether the osteophyte surface template is loaded correctly.
	{
		QApplication::restoreOverrideCursor();
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Osteophyte surface error!"), 
										tr("Osteophyte surface file does not exist, or\nosteophyte surface template is not correct, or\nosteophyte surface quality is not good."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return;
	}

	// For silhouette creation
	CFemoralPart* pPart = m_pDoc->GetOsteophyteSurface();
	IwAxis2Placement mechAxes = IFemurImplant::FemoralAxes_GetMechanicalAxes();
	pPart->SetColor( brown );
	// Determine silhouettes
	IwTArray<IwCurve*> totalSCurves, sCurves, simpleCurves;
	IwBSplineCurve *crv;
	IwBSplineSurface *leftSideSurface, *rightSideSurface;
	CFemoralPart::DetermineSilhouetteCurves(m_pDoc->GetIwContext(), pPart->GetSinglePatchSurface(), mechAxes.GetZAxis(), sCurves, 0.1);
	totalSCurves.Append(sCurves);
	pPart->GetSideSurfaces(leftSideSurface, rightSideSurface);
	CFemoralPart::DetermineSilhouetteCurves(m_pDoc->GetIwContext(), leftSideSurface, mechAxes.GetZAxis(), sCurves, 0.1);
	totalSCurves.Append(sCurves);
	CFemoralPart::DetermineSilhouetteCurves(m_pDoc->GetIwContext(), rightSideSurface, mechAxes.GetZAxis(), sCurves, 0.1);
	totalSCurves.Append(sCurves);
	for (unsigned i=0; i<totalSCurves.GetSize(); i++)
	{
		crv = (IwBSplineCurve*)totalSCurves.GetAt(i);
		crv->RemoveKnots(0.25);
		simpleCurves.Add(crv);
	}
	pPart->SetIwCurves(simpleCurves);

	if ( m_pDoc->GetVarTableValue("JIGS TAB LENGTH REFER TO DISTAL CUTS") == 1 )
	{
		// Create distal cut profiles on osteophyte surface
		IwAxis2Placement femAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
		IwTArray<IwPoint3d> cutPnts, cutOrientations;
		IFemurImplant::FemoralCuts_GetCutsInfo(cutPnts, cutOrientations);
		IwPoint3d posDistCutPnt = cutPnts.GetAt(6);
		IwPoint3d negDistCutPnt = cutPnts.GetAt(7);
		// Create a positive distal cut plane
		IwBSplineSurface *posDistalCutPlane, *negDistalCutPlane;
		IwPoint3d planePnt00 = posDistCutPnt + 75*femAxes.GetYAxis();
		IwPoint3d planePnt10 = planePnt00 + 75*femAxes.GetXAxis();
		IwPoint3d planePnt11 = planePnt10 - 150*femAxes.GetYAxis();
		IwPoint3d planePnt01 = planePnt00 - 75*femAxes.GetYAxis();
		IwBSplineSurface::CreateBilinearSurface(m_pDoc->GetIwContext(), planePnt00, planePnt10, planePnt01, planePnt11, posDistalCutPlane);
		IwTArray<IwCurve*> totalIntCurves, simpleIntCurves, intCurves;
		IntersectSurfaces(pPart->GetSinglePatchSurface(), posDistalCutPlane, intCurves, 0.1, 0.1);
		totalIntCurves.Append(intCurves);
		intCurves.RemoveAll();
		IntersectSurfaces(leftSideSurface, posDistalCutPlane, intCurves, 0.1, 0.1);
		totalIntCurves.Append(intCurves);
		intCurves.RemoveAll();
		IntersectSurfaces(rightSideSurface, posDistalCutPlane, intCurves, 0.1, 0.1);
		totalIntCurves.Append(intCurves);
		intCurves.RemoveAll();
		// Create a negative distal cut plane
		planePnt00 = negDistCutPnt + 75*femAxes.GetYAxis();
		planePnt10 = planePnt00 - 75*femAxes.GetXAxis();
		planePnt11 = planePnt10 - 150*femAxes.GetYAxis();
		planePnt01 = planePnt00 - 75*femAxes.GetYAxis();
		IwBSplineSurface::CreateBilinearSurface(m_pDoc->GetIwContext(), planePnt00, planePnt10, planePnt01, planePnt11, negDistalCutPlane);
		IntersectSurfaces(pPart->GetSinglePatchSurface(), negDistalCutPlane, intCurves, 0.1, 0.1);
		totalIntCurves.Append(intCurves);
		intCurves.RemoveAll();
		IntersectSurfaces(leftSideSurface, negDistalCutPlane, intCurves, 0.1, 0.1);
		totalIntCurves.Append(intCurves);
		intCurves.RemoveAll();
		IntersectSurfaces(rightSideSurface, negDistalCutPlane, intCurves, 0.1, 0.1);
		totalIntCurves.Append(intCurves);
		intCurves.RemoveAll();
		for (unsigned i=0; i<totalIntCurves.GetSize(); i++)
		{
			crv = (IwBSplineCurve*)totalIntCurves.GetAt(i);
			crv->RemoveKnots(0.25);
			simpleIntCurves.Add(crv);
		}
		pPart->SetViewProfiles_ThisIsWrongFunctionToCall(simpleIntCurves);
		pPart->SetDisplayViewProfiles(true);
		////////////////////////////////////////////////////////////////////////////
		// NOTE HERE - Osteophyte surface is defined as a FemoralPart class.
		// However, osteophyte surface never needs view profiles (m_viewProfileCurves).
		// We here borrow this class member to store and display the distal cut profile curves.
		// DO NOT call CFemoralPart::SetViewProfiles_ThisIsWrongFunctionToCall()
		// in other places.
		////////////////////////////////////////////////////////////////////////////

		if ( posDistalCutPlane )
			IwObjDelete(posDistalCutPlane);
		if ( negDistalCutPlane )
			IwObjDelete(negDistalCutPlane);
	}

	// Set time stamp
	QDateTime osteoTime = QDateTime::currentDateTime();
	uint osteoTimeStamp = osteoTime.toTime_t();
	m_pDoc->GetAdvancedControlJigs()->SetOsteophyteTimeStampForOuter(osteoTimeStamp);
	m_pDoc->GetAdvancedControlJigs()->SetOsteophyteTimeStampForInner(osteoTimeStamp);

	// Change the status sign of itself
	if ( activateUI )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OSTEOPHYTE_SURFACE, false, false);

	m_pView->Redraw();

	QApplication::restoreOverrideCursor();
}

void CImportOsteophyteSurface::OnAccept()
{
	m_pDoc->AppendLog( QString("CImportOsteophyteSurface::OnAccept()") );

	CPart* pOsteophyteSurface = m_pDoc->GetOsteophyteSurface();

	// Once osteophyte surface is imported, no way to modify (unless delete it).
	// It should be always up-to-date.
	if ( pOsteophyteSurface && !pOsteophyteSurface->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OSTEOPHYTE_SURFACE, true, true);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	m_bDestroyMe = true;
	m_pView->Redraw();
}


void CImportOsteophyteSurface::OnCancel()
{
	m_pDoc->AppendLog( QString("CImportOsteophyteSurface::OnCancel()") );

	m_bDestroyMe = true;
	m_pView->Redraw();
}
