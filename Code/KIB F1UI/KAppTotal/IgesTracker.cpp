#include <QtGui>
#include "IgesTracker.h"
#include "ProgressWidget.h"


CIgesTracker::CIgesTracker( CProgressWidget* pProgressWidget )
	{
	m_pProgressWidget = pProgressWidget;
	}


IwStatus CIgesTracker::Heartbeat()
	{
	return IW_SUCCESS;
	}


IwStatus CIgesTracker::StartCountdown( unsigned int count )
	{
	m_iCount = 0;

	m_pProgressWidget->Start( count );

	return IW_SUCCESS;
	}


IwStatus CIgesTracker::Tick()
	{
	m_pProgressWidget->Update( m_iCount++ );

	return IW_SUCCESS;
	}


IwStatus CIgesTracker::EndCountdown()
	{
	m_iCount = 0;
	m_pProgressWidget->Reset();

	return IW_SUCCESS;
	}



