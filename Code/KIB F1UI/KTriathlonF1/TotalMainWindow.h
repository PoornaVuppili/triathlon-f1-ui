#pragma once

#include "..\KAppTotal\MainWindow.h"
#include "..\KAppTotal\Part.h"

class CTotalView;
class CTotalEntPanel;
class CTotalEntPanelJigs;
class CTotalDoc;

class TEST_EXPORT_TW CTotalMainWindow : public CMainWindow
{
    Q_OBJECT

public:
    CTotalMainWindow();
	~CTotalMainWindow();
	CTotalDoc*	GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};
	CTotalView*	GetView() {return ((CTotalView*)m_pView);};
	CTotalEntPanel*	GetEntPanel() {return ((CTotalEntPanel*)m_pEntPanel);};
	CTotalEntPanelJigs*	GetEntPanelJigs() {return ((CTotalEntPanelJigs*)m_pEntPanelJigs);};

    virtual bool    Initialize();
	bool			InitializeJigs();
    virtual void	EnableActions( bool bEnable );
	virtual void	EnableMakingActions( bool bEnable);
	void			EnableMakingActionsJigs( bool bEnable);
    // Get the data file version info
    virtual QString GetDataFileVersionInfo();

	bool			FileOpen(QString &elem=QString(""), QString options=QString(""));
	bool			FileNew();
	void			FileSaveWithDefaultName(QString sDir);
	bool			FileClose(bool withCancel=true);
    void			FileCloseSilent();
	virtual QString	WindowTitle();
	virtual void	LoadingFileFinished();
	void			SetJCOSToolbarVisible(bool bVisible);
    bool			AllStepsComplete(std::string const &phase, std::string const &subphase) const override;
    bool            OutputResults() const override;
	void			HideAllEntities();
	bool			IsAllProfileDesignComplete();

public slots:
	virtual void	OnFileNew();
	virtual void	OnFileOpen(QString &elem=QString(""));
	void			OnFileOpenWithOptions(QString &elem=QString(""), QString options=QString(""));
	virtual void	OnFileClose();
	virtual void	OnFileSave(bool bWarning=true);
	virtual void	OnFileOpenAsReadOnly();
	virtual void	OnFileSaveACopy();
	bool			OnImportSurfaces();
	virtual void	OnEnableMakingActions();
	void			OnEnableMakingActionsJigs();
	void			OnCleanUpTempDisplay();
	virtual void	OnImportCADEntity();
	void			OnImportCADEntityJigs();
	void			OnFemoralImplantReview();
	void			OnFemoralJigsReview();

protected slots:
	void			OnSaveNewFile();
	void			OnDoNothing(){};
	void			OnBestFit();

signals:
    void            DesignStageCommenced(Module*, int); // since this module has two stages
    void            DesignStageComplete(Module*, int);

protected:
	IwAxis2Placement	GetImportCADEntityTransformationMatrix();
	void			UpdateTessellationAndTransparencyTogglesJigs();
	void			UpdateTessellationAndTransparencyToggles();

private:
	bool			ImportIgesPart( CEntRole eEntRole, const QString& sTitle );
	bool			ImportXMLPart( CEntRole eEntRole, const QString& sTitle );
	virtual QString		GetFileDefaultName();

public:
    void emitDesignStageCommenced(int);
    void emitDesignStageComplete(int);

protected:
	QMenu*			m_menuView;

	QMenu*			m_menuFemur;
	QAction*		m_actMakeFemoralAxes;
	QAction*		m_actCutFemur;
	QAction*		m_actMakeInnerSurface;
	QAction*		m_actDefineOutlineProfile;
	QAction*		m_actMakeSideSurface;
	QAction*		m_actDefineJCurves;
	QAction*		m_actDefineCrossProfiles;
	QAction*		m_actMakeOuterSurface;
	QAction*		m_actMakeSolidImplant;
	QAction*		m_actAddFillets;
	QAction*		m_actDefinePegs;
	QAction*		m_actConvertCastingImplant;
	QAction*		m_actOutput;
	QAction*		m_actSeparator;
	QAction*		m_actAdvancedControl;
	QAction*		m_actTest1;

	QAction*		m_actAnalyzeImplant;
    QAction*        m_actValidateImplant;
    QAction*        m_actReviewImplant;

	QMenu*			m_menuInfo;
	QAction*		m_actPatientInfo;
	QAction*		m_actUserHistory;

	// Jigs
	CTotalEntPanelJigs*		m_pEntPanelJigs;
	QMenu*					m_menuFileJigs;
	QAction*				m_actFileSaveNewFile;
	QAction*				m_actFileJigsImportCADEntity;

	QMenu*			m_menuFemurJigs;
	QAction*		m_actImportOsteophytesJigs;
	QAction*		m_actDefineCartilageJigs;
	QAction*		m_actDefineOutlineProfileJigs;
	QAction*		m_actCreateOsteophytesJigs;
	QAction*		m_actMakeInnerSurfaceJigs;
	QAction*		m_actMakeOuterSurfaceJigs;
	QAction*		m_actMakeSideSurfaceJigs;
	QAction*		m_actMakeSolidPositionJigs;
	QAction*		m_actDefineStylusJigs;
	QAction*		m_actMakeAnteriorSurfaceJigs;
	QAction*		m_actOutputJigs;
	QAction*		m_actAdvancedControlJigs;

	QMenu*			m_menuToolsJigs;
	QAction*        m_actReviewJigs;
	QAction*        m_actValidateJigs;

	QMenu*			m_menuInfoJigs;

	// View
 	QMenu*			m_menuViewJigs;
	QActionGroup*	m_groupTessellationJigs;
	QMenu*			m_menuViewTessellationJigs;
	QAction*		m_actTessellationHighJigs;	
	QAction*		m_actTessellationFineJigs;	
	QAction*		m_actTessellationNormalJigs;	
	QAction*		m_actTessellationCoarseJigs;	

	QActionGroup*	m_groupTransparencyJigs;
	QMenu*			m_menuViewTransparencyJigs;
	QAction*		m_actTransparencyTransparentTotalJigs;	
	QAction*		m_actTransparencyTransparentPlusJigs;	
	QAction*		m_actTransparencyTransparentJigs;	
	QAction*		m_actTransparencyNormalJigs;	
	QAction*		m_actTransparencyOpaqueJigs;	
	QAction*		m_actTransparencyOpaquePlusJigs;


   // toolbars
    QToolBar*		JCOSToolbar;

};
