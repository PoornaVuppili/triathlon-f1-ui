/****************************************************************************
** Meta object code from reading C++ file 'MakeOuterSurfaceJigs.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../MakeOuterSurfaceJigs.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MakeOuterSurfaceJigs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CMakeOuterSurfaceJigs_t {
    QByteArrayData data[9];
    char stringdata0[101];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CMakeOuterSurfaceJigs_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CMakeOuterSurfaceJigs_t qt_meta_stringdata_CMakeOuterSurfaceJigs = {
    {
QT_MOC_LITERAL(0, 0, 21), // "CMakeOuterSurfaceJigs"
QT_MOC_LITERAL(1, 22, 7), // "OnReset"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 8), // "OnAccept"
QT_MOC_LITERAL(4, 40, 8), // "OnCancel"
QT_MOC_LITERAL(5, 49, 13), // "OnDispSurface"
QT_MOC_LITERAL(6, 63, 13), // "OnDispAntDrop"
QT_MOC_LITERAL(7, 77, 11), // "OnDispOsteo"
QT_MOC_LITERAL(8, 89, 11) // "OnDispCarti"

    },
    "CMakeOuterSurfaceJigs\0OnReset\0\0OnAccept\0"
    "OnCancel\0OnDispSurface\0OnDispAntDrop\0"
    "OnDispOsteo\0OnDispCarti"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CMakeOuterSurfaceJigs[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    0,   53,    2, 0x08 /* Private */,
       7,    0,   54,    2, 0x08 /* Private */,
       8,    0,   55,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CMakeOuterSurfaceJigs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CMakeOuterSurfaceJigs *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnReset(); break;
        case 1: _t->OnAccept(); break;
        case 2: _t->OnCancel(); break;
        case 3: _t->OnDispSurface(); break;
        case 4: _t->OnDispAntDrop(); break;
        case 5: _t->OnDispOsteo(); break;
        case 6: _t->OnDispCarti(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject CMakeOuterSurfaceJigs::staticMetaObject = { {
    QMetaObject::SuperData::link<CManager::staticMetaObject>(),
    qt_meta_stringdata_CMakeOuterSurfaceJigs.data,
    qt_meta_data_CMakeOuterSurfaceJigs,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CMakeOuterSurfaceJigs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CMakeOuterSurfaceJigs::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CMakeOuterSurfaceJigs.stringdata0))
        return static_cast<void*>(this);
    return CManager::qt_metacast(_clname);
}

int CMakeOuterSurfaceJigs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CManager::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
