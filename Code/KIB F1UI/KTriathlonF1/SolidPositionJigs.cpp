#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#pragma warning( default : 4996 4805 )
#include "SolidPositionJigs.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"
#pragma warning( disable : 4996 4805 )
#include <IwTess.h>
#pragma warning( default : 4996 4805 )


CSolidPositionJigs::CSolidPositionJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = white;

	m_bSPJigsModified = false;
	m_bSPJigsDataModified = false;

}

CSolidPositionJigs::~CSolidPositionJigs()
{

}

void CSolidPositionJigs::Display()
{
	CPart::Display();
}

bool CSolidPositionJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	m_bSPJigsDataModified = false;

	return true;
}


bool CSolidPositionJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);

	m_bSPJigsDataModified = false;

	return true;
}


void CSolidPositionJigs::SetSPJigsModifiedFlag( bool bModified )
{
	m_bSPJigsModified = bModified;
	if (m_bSPJigsModified)
	{
		m_bSPJigsModified = true;
		m_pDoc->SetModified(m_bSPJigsModified);
	}
}


