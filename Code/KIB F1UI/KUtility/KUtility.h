#ifndef KUTILITY_H
#define KUTILITY_H

#include <qstring.h>
#include "..\KUtility\Eigen\Dense"
using namespace Eigen;
// Lib version
#define KUTILITY_VERSION "0.0"

// Constants
#define SMALL_VALUE  1E-5
#define LARGE_VALUE  1E10

// Functions
template<class T> void DeletePtr(T*& p)
{
    delete p;
    p = NULL;
}

template<class T> void DeleteArrayPtr(T*& p)
{
    delete[] p;
    p = NULL;
}

// image viewing type
enum ViewType
{
    Axial,
    Coronal,
    Sagittal
};

// date time format string
extern const char* formatDateTime;

// get the current user name
QString CurrentUserName();

QString GetSerialNumberFromFolderName(QString const& folderName);

bool gt(double a, double b);
bool lt(double a, double b);

double ClosestDistanceError(MatrixXd& movMat, MatrixXd& fixMat, Vector3d& directionError);

// Status indicator - includes a status bar and a progress bar
#include "KStatusIndicator.h"

// Wait cursor
#include "KWaitCursor.h"

// SMLib interface
#include "SMLibInterface.h"

// Property file
#include "KPropertyFile.h"

#endif // KUTILITY_H
