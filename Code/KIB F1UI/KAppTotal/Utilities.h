#pragma once

#include "valgebra.h"
#pragma warning( disable : 4996 4805 )
#ifdef GetObject
#undef GetObject
#endif

#include <iwtslib_all.h>
#include <IwMerge.h>
#pragma warning( default : 4996 4805 )
#include "Globals.h"
#include "Entity.h"
typedef int BOOL;

class IwBrep;
class IwFace;
class IwCurve;
class CPart;
class CDataDoc;

enum CEdgeConvexity
	{
	EDGE_UNDEFINED,
	EDGE_TANGENT,
	EDGE_CONVEX,
	EDGE_CONCAVE
	};


enum CCurveEnd
	{
	START_POINT,
	END_POINT
	};


#define INVERT_CURVE	true
#define IS_EQ_TOL6(a,b) (fabs((a)-(b))<0.000001)
#define IS_EQ_TOL3(a,b) (fabs((a)-(b))<0.001)

template < class CElem >
BOOL IsInArray( CElem& a, IwTArray< CElem >& array )
	{
	for( ULONG i = 0; i < array.GetSize(); i++ )
		{
		if( a == array[i] )
			return TRUE;
		}

	return FALSE;
	}

template < class CElem >
BOOL IsInArray( CElem& a, int nElems, CElem* array )
	{
	for( int i = 0; i < nElems; i++ )
		{
		if( a == array[i] )
			return TRUE;
		}

	return FALSE;
	}


template < class CElem >
int IdxInArray( CElem& a, IwTArray< CElem >& array )
	{
	for( ULONG i = 0; i < array.GetSize(); i++ )
		{
		if( a == array[i] )
			return i;
		}

	return -1;
	}

template < class CElem >
int IdxInArray( CElem& a, int nElems, CElem* array )
	{
	for( int i = 0; i < nElems; i++ )
		{
		if( a == array[i] )
			return i;
		}

	return -1;
	}

template < class CElem >
void ArraysIntersection( IwTArray< CElem >& ar1, IwTArray< CElem >& ar2, IwTArray< CElem >& result )
	{
	IwTArray< CElem >		ar;

	for( ULONG i = 0; i < ar1.GetSize(); i++ )
		{
		if( IsInArray( ar1[i], ar2 ) )
			ar.Add( ar1[i] );
		}

	result.ReSet();
	result.Append( ar );
	}


template < class CElem >
void ArraysSubtraction( IwTArray< CElem >& ar1, IwTArray< CElem >& ar2, IwTArray< CElem >& result )
	{
	//CEdgeArray			ar;
	IwTArray< CElem >		ar;

	for( ULONG i = 0; i < ar1.GetSize(); i++ )
		{
		if( !IsInArray( ar1[i], ar2 ) )
			ar.Add( ar1[i] );
		}

	result.ReSet();
	result.Append( ar );
	}


template < class CElem >
void ArraysUnion( IwTArray< CElem >& ar1, IwTArray< CElem >& ar2, IwTArray< CElem >& result )
	{
	IwTArray< CElem >		ar;

	for( ULONG i = 0; i < ar1.GetSize(); i++ )
		ar.AddUnique( ar1[i] );

	for( ULONG i = 0; i < ar2.GetSize(); i++ )
		ar.AddUnique( ar2[i] );

	result.ReSet();
	result.Append( ar );
	}


template < class CElem >
void AppendToArray( IwTArray< CElem >& to, IwTArray< CElem >& from, int i0, int i1 )
	{
	for( int i = i0; i <= i1; i++ )
		to.Add( from[i] );
	}


enum CCompVal { MinVal, MaxVal };


inline bool fBigger( double a, double b )
	{
	return a > b;
	}


inline bool fSmaller( double a, double b )
	{
	return a < b;
	}


typedef bool (*CCompareFunc)( double a, double b );


//int FindFirstCurve( CCurveArray& Curves, int ic, CCompVal eComp, CTransform& trf = CTransform() );


bool PierceBrep( 
		IwBrep*						pBrep, 
		const IwPoint3d&			pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt, 
		IwFace**					pFace = NULL );

bool PierceSurface( 
		IwSurface*					pSurface, 
		const IwPoint3d&				pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt, 
		double*						uv = NULL );

bool GetBrepNormalAtPoint( 
		IwBrep*						pBrep, 
		const IwPoint3d&				pt, 
		IwVector3d&					nm, 
		double						tol = 0.1,
		double*						uv = NULL );

IwVector3d GetSurfNormalAtPoint( 
		IwSurface*					pSurf, 
		const IwPoint3d&				pt );

void CollectSmoothSurfaceFaces( 
		IwFace*						pFace, 
		IwTArray< IwFace* >&			Faces );

void SetFacesId( 
		IwBrep*						pBrep, 
		const char*					filename = NULL );

void SetEdgesId( 
		IwBrep*						pBrep );

void CheckFaces( 
		IwBrep*						pBrep );

void CollectLaminaEdgeCurves( 
		IwBrep*						pBrep, 
		IwTArray< IwCurve* >&		rCurves );

IwBrep* MakeBox( 
			const IwPoint3d&			orig,
			const IwVector3d&		vx,
			const IwVector3d&		vy,
			double					l,
			double					w,
			double					h );


IwBrep* MakeCylinder( 
			const IwPoint3d&			orig,
			const IwVector3d&		vx,
			const IwVector3d&		vy,
			double					r,
			double					h );


IwBrep* MakeCylinder( 
			const IwPoint3d&			orig,
			const IwVector3d&		axis,
			double					r,
			double					h );

IwBrep* Boolean_ByIteration( 
			IwBooleanOperationType	eOperation,
			IwBrep*					pBrep1, 
			IwBrep*					pBrep2,
			double					tol = 0.0,
			double					angTol = 0.0,
			bool					bCheckSolidResult = false); 

IwBrep* Boolean( 
			IwBooleanOperationType	eOperation,
			IwBrep*					pBrep1, 
			IwBrep*					pBrep2,
			double					tol = 0.0,
			double					angTol = 0.0,
			bool					bCopyInputBreps = false); 

CPart* AddNewPart( 
			CDataDoc*				pDoc, 
			IwBrep*					pBrep, 
			CEntRole				eEntRole, 
			const CColor&			color );
/*
bool FindBrepSmoothArea( 
			IwBrep*					pBrep,
			const IwPoint3d&			pt,
			const IwVector3d&		vec,
			IwPoint3d&				pnt,
			CFaceArray&				Faces );

bool FindBrepSmoothArea( 
			IwBrep*					pBrep,
			int						idStartFace,
			CFaceArray&				Faces );
*/
//void FindSmoothAreaBorder(
//			const CFaceArray&		Faces, 
//			double					angle, 
//			CEdgeArray&				Edges );

//void FindFacesBorder( 
//			const CFaceArray&		Faces, 
//			CEdgeArray&				BorderEdges );

void FindCommonEdges( 
			const CFaceArray&		Faces1, 
			const CFaceArray&		Faces2, 
			CEdgeArray&				CommonEdges );
/*
void MarkBrepNontangentEdges( 
			IwBrep*					pBrep );
*/
//IwFace* GetFace( 
//			IwBrep*					pBrep, 
//			int						idFace );
//
/*
void ClearBrepFaces( 
			IwBrep*					pBrep );
*/
/*
void ClearBrepEdges( 
			IwBrep*					pBrep );
*/
void SortSectionCurves( 
			CCurveArray&			Curves, 
			int						iStart = 0 );

void SortCurvesChain( 
			CCurveArray&			Curves, 
			int						iStart = 0 );

int SortCurves( 
			CCurveArray&			Curves, 
			CIntArray&				nCurves );

IwBSplineCurve* MakeSplineFromCurves( 
			CCurveArray				curves,
			const IwPoint3d&			pt );

IwPoint3d GetCurveStartPoint( 
			IwCurve*				pCurve );

IwPoint3d GetCurveEndPoint( 
			IwCurve*				pCurve );

IwVector3d GetCurveStartTangent( 
			IwCurve*				pCurve, 
			bool					bNomalize = true );

IwVector3d GetCurveEndTangent( 
			IwCurve*				pCurve, 
			bool					bNomalize = true );

IwBSplineCurve* MakeLine( 
			const IwPoint3d&			p0, 
			const IwPoint3d&			p1 );

IwBSplineCurve* MakeArc( 
			const IwPoint3d&			cnt, 
			const IwPoint3d&			p0, 
			const IwPoint3d&			p1 );

IwBSplineCurve* AddLine( 
			const IwPoint3d&			p0, 
			const IwPoint3d&			p1, 
			CCurveArray&			Curves );

IwBSplineCurve* AddArc( 
			const IwPoint3d&			cnt, 
			const IwPoint3d&			p0, 
			const IwPoint3d&			p1, 
			CCurveArray&			Curves );

IwBSplineCurve* AddLine( 
			const IwPoint3d&			p0, 
			const IwPoint3d&			p1, 
			CSplineArray&			Curves );

IwBSplineCurve* AddArc( 
			const IwPoint3d&			cnt, 
			const IwPoint3d&			p0, 
			const IwPoint3d&			p1, 
			CSplineArray&			Curves );

IwBrep* MakeLinearSweep( 
		CCurveArray&				Curves,
		const IwVector3d&			vec,
		double						height,
		bool						bMakeCaps = true );

IwBrep* MakeRotationalSweep( 
		CCurveArray&				Curves,
		const IwPoint3d&				pt,			
		const IwVector3d&			vec,
		double						angle );

long MakePlanarFace( 
		IwBrep*						pBrep, 
		CCurveArray&				curves, 
		IwFace**					pNewFace = NULL );

bool IsCurvePlanar( 
		IwCurve*					pCurve, 
		IwPoint3d&					pt, 
		IwVector3d&					nm );

IwBrep* NewBrep( 
		double						tol );

IwBrep* CopyBrep( 
		IwBrep const*						pBrep );

IwEdge const* FindAdjacentEdgeAtDirection(
											IwEdge const* e,
											IwVector3d const& dir,
											double deg_tol
											);

IwVertex const* FindVertex(			IwBrep const* pBrep, IwPoint3d const& P, double tol = 1e-4);

IwCurve* CopyCurve(					IwCurve const* pCurve,
									IwAxis2Placement const* xform = nullptr);

IwBSplineCurve* CopyCurve( 
		IwBSplineCurve*				pCurve, 
		bool						bInvertCurve = false );

IwTArray<IwCurve *> CopyCurves(		IwTArray<IwCurve *>const& crvs,
									IwAxis2Placement const* xform = nullptr);

bool IsPointOnTorus(
									IwPoint3d const& c,
									IwVector3d const& a,
									double const R,
									double const r,

									IwPoint3d const& p,
									double tol = 1e-2
									);

IwEdge* GetIncidentEdge(			IwVertex const* V0, IwVertex const* V1);


IwPoint3d VertexCenter(				IwTArray<IwVertex*> const& vs);
IwPoint3d EdgeVertexCenter(			IwEdge const* e);
IwPoint3d FaceVertexCenter(			IwFace const* f);

IwBSplineSurface* CopySurface( 
		IwBSplineSurface*			pSurf );

int GetFaceIndex( 
		IwBrep*						pBrep, 
		IwFace*						pFace );

int GetEdgeIndex( 
		IwBrep*						pBrep, 
		IwEdge*						pEdge );

IwBSplineCurve* GetEdgeCurve( 
		IwEdge*						pEdge );

IwFace* GetFace( 
		IwBrep*						pBrep, 
		int							iFace );

IwEdge* GetEdge( 
		IwBrep*						pBrep, 
		int							iEdge );
/*
bool CrossSectSmoothArea( 
		IwBrep*						pBrep,
		const IwPoint3d&				pt,
		const IwVector3d&			vec,
		const IwVector3d&			nm,
		CCurveArray&				curves );
*/
void ApproximateChainsWithSplines(
		const CIntArray&			nc,
		const CCurveArray&			curves,
		CSplineArray&				splines );

IwBSplineCurve*	ApproximateWithBSpline( 
		CCurveArray&				Curves,
		double						tolFactor=1.0);

IwBSplineCurve*	ApproximateClosedCurvesWithBSpline( 
		CCurveArray&				Curves,
		IwPoint3d					newSealPoint,
		double						tolFactor=1.0);

/*
IwBSplineCurve* CrossSectSmoothArea( 
		IwBrep*						pBrep,
		const IwPoint3d&				pt,
		const IwVector3d&			vec,
		const IwVector3d&			nm );
*/
double DropPointOnCurve( 
		IwCurve*					pCurve, 
		const IwPoint3d&				pt, 
		IwPoint3d*					pnt = NULL );

//void OrientCurve( 
//		IwCurve*					pCurve, 
//		int							icoord, 
//		const CTransform&			trf );

void GetCurveEndPoints( 
		IwCurve*					pCurve, 
		IwPoint3d&					pt0, 
		IwPoint3d&					pt1 );

void GetCurveEndParams(  
		IwCurve*					pCurve, 
		double&						t0, 
		double&						t1 );

void GetEdgeEndParams( 
		IwEdge*						pEdge, 
		double&						t0, 
		double&						t1 );

void GetEdgeEndPoints( 
		IwEdge*						pEdge, 
		IwPoint3d&					p0, 
		IwPoint3d&					p1 );

bool IntersectBrepByLine( 
		IwBrep*						pBrep, 
		const IwPoint3d&			pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt,
		IwFace** face=NULL);

bool IntersectBrepByCurve( 
		IwBrep*						pBrep, 
		IwCurve*					curve, 
		IwPoint3d&					pnt,
		IwPoint3d*					refPt=NULL,
		IwFace**					face=NULL);

bool IntersectFaceByLine( 
		IwFace*						pFace, 
		const IwPoint3d&			pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt);

bool IntersectSurfaceByLine( 
		IwSurface*					pSurface, 
		const IwPoint3d&				pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt, 
		double*						uv = NULL );

bool IntersectSurfaceByFireRay( 
		IwSurface*					pSurface, 
		const IwPoint3d&				pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt, 
		double*						uv = NULL );

bool IntersectSurfaceByLine( 
		IwSurface*					pSurface, 
		IwPoint3d&					pt, 
		IwVector3d&					vec, 
		IwPoint2d&					guessUV,
		IwPoint3d&					pnt, 
		IwVector3d&					normal,
		IwPoint2d&					intUV);

bool IntersectSurfacesByLine(
		IwTArray<IwSurface*>		surfaces, 
		IwPoint3d&					pt, 
		IwVector3d&					vec, 
		IwPoint3d&					pnt, 
		int*						surfIndex = NULL, 
		double*						uv = NULL );

bool IntersectSurfacesByFireRay(
		IwTArray<IwSurface*>		surfaces, 
		IwPoint3d&					pt, 
		IwVector3d&					vec, 
		double						maxDistToHit,
		IwPoint3d&					pnt, 
		int*						surfIndex = NULL, 
		IwPoint2d*					uv=NULL,
		IwTArray<int>*				dotValues=NULL,
		IwTArray<bool>*				hitEachSurf=NULL);


bool IntersectSurfaceByCurve( 
		const IwSurface*			pSurface, 
		const IwCurve*				pCurve, 
		IwTArray<IwPoint3d>&		intPnts, 
		IwTArray<IwPoint2d>&		intUVParams,
		IwTArray<double>&			intParams);

bool IntersectSurfaceByCurves( 
		const IwSurface*			pSurface, 
		const IwTArray<IwCurve*>	pCurves, 
		IwTArray<IwPoint3d>&		intPnts, 
		IwTArray<IwPoint2d>&		intUVParams,
		IwTArray<double>&			intParams);

int IntersectCurveByPlane( 
		IwCurve*					pCurve, 
		const IwPoint3d&				pt, 
		const IwVector3d&			nm,
		CPointArray&				pts,
		IwTArray<double>*			params=NULL);

int IntersectCurveByPlane2( 
		IwCurve*					pCurve, 
		const IwPoint3d&				pt, 
		const IwVector3d&			nm,
		CPointArray&				pts );

bool IntersectCurveByPlane( 
		IwCurve*					pCurve, 
		const IwPoint3d&				pnt, 
		const IwVector3d&			nrm,
		IwPoint3d&					pt,
		double*						t = NULL );

bool IntersectCurvesByPlane( 
		IwTArray<IwCurve*>			pCurves, 
		const IwPoint3d&			pnt, 
		const IwVector3d&			nrm,
		IwPoint3d&					pt,
		double*						t = NULL,
		int*						crvIndex = NULL);

int IntersectEdgeByPlane( 
		IwEdge*						pEdge, 
		const IwPoint3d&			pt, 
		const IwVector3d&			nm,
		CPointArray&				pts);

bool IntersectEdgeByPlane( 
		IwEdge*						pEdge, 
		const IwPoint3d&			pnt, 
		const IwVector3d&			nrm,
		IwPoint3d&					pt );

bool IntersectEdgesByPlane( 
		IwTArray<IwEdge*>			pEdges, 
		const IwPoint3d&			pnt, 
		const IwVector3d&			nrm,
		IwPoint3d&					pt,
		int*						edgeIndex=NULL);

bool IntersectCurveByVector(
	    IwCurve*					pCurve,
		IwExtent1d					crvIntval,
		IwPoint3d					point,
		IwVector3d					vector,
		double*						guessParam,
		double						maxDist,
		IwPoint3d&					intersectPnt,
		double&						intersectParam,
		IwVector3d&					intersectTangent);

bool IntersectCurvesByVector(
	    IwTArray<IwCurve*>			curves,
		IwPoint3d					point,
		IwVector3d					vector,
		IwPoint3d&					intersectPnt,
		double&						intersectParam,
		int&						whichOne,
		double*						tol=NULL);

bool IntersectVectorByVector(
		IwPoint3d pnt0,
		IwVector3d vector0,
		IwPoint3d pnt1,
		IwVector3d vector1,
		IwPoint3d& intersectPnt);

bool IntersectPlaneByVector(
		IwPoint3d PlanePnt,
		IwVector3d PlaneNormal,
		IwPoint3d vectorPnt,
		IwVector3d vector,
		IwPoint3d& intersectPnt);

bool IntersectPolylinesByLine(
	    IwTArray<IwPoint3d> polylines,
		IwTArray<IwPoint3d> line,
		IwVector3d* normalVector=NULL);

bool IntersectIwTArrayPointsByPlane( 
		IwTArray<IwPoint3d>			pnts, 
		IwPoint3d&					pt, 
		IwVector3d&					nm,
		double						distTol,
		IwPoint3d&					intPnt,
		int *index=NULL);

//IwBSplineCurve* CrossSectBrep( 
//		IwBrep*						pBrep,
//		const IwPoint3d&				pt,
//		const IwVector3d&			nm,
//		int							ic, 
//		CCompVal					eComp, 
//		CTransform&					trf = CTransform() );

IwBSplineCurve* CrossSectBrep( 
		IwBrep*						pBrep,
		const IwPoint3d&				pt,
		const IwVector3d&			nm );

int CrossSectSurface(
		IwBSplineSurface*			pSurface,
		const IwPoint3d&				pt,
		const IwVector3d&			nm,
		CCurveArray&				curves );

IwBSplineCurve* MakeBrepSection( 
		IwBrep*						pBrep,
		const IwPoint3d&				pt,
		const IwVector3d&			nm );

void ComputePointsOnCurve( 
		IwCurve*					pCurve, 
		double						t0, 
		double						t1, 
		int							np, 
		IwTArray<IwPoint3d>&		pts, 
		bool						bReset = false );

void ComputePointsAndTangentsOnCurve( 
		IwCurve*					pCurve, 
		double						t0, 
		double						t1, 
		int							np, 
		CPointArray&				pts, 
		CVectorArray&				tns, 
		bool						bReset = false );

void ComputePointsOnCurve( 
		IwCurve*					pCurve, 
		int							np, 
		IwTArray<IwPoint3d>&		pts, 
		bool						bReset );

void ComputePointsAndTangentsOnCurve( 
		IwCurve*					pCurve, 
		int							np, 
		CPointArray&				pts, 
		CVectorArray&				tns, 
		bool						bReset );

void ComputePointsOnCurve(
		IwEdge*						pEdge, 
		int							np, 
		IwTArray<IwPoint3d>&		pts );

void ComputePointsAndTangentsOnCurve(
		IwEdge*						pEdge, 
		int							np, 
		CPointArray&				pts,
		CVectorArray&				tns );

void EvalCurve( 
		IwCurve*					pCurve, 
		double						t, 
		IwPoint3d&					pt, 
		IwVector3d&					tn );

void EvalCurvePoint( 
		IwCurve*					pCurve, 
		double						t, 
		IwPoint3d&					pt );

void EvalCurveTangent( 
		IwCurve*					pCurve, 
		double						t, 
		IwVector3d&					tn,
		bool						bNormalize = true );

IwVector3d EvalSurfacePoint( 
		IwSurface*					pSurf, 
		double						uv[2] );

IwVector3d EvalSurfacePoint( 
		IwSurface*					pSurf, 
		double						u,
		double						v );

IwVector3d EvalSurfaceNormal( 
		IwSurface*					pSurf, 
		double						uv[2] );

IwVector3d EvalSurfaceNormal( 
		IwSurface*					pSurf, 
		double						u,
		double						v );

void EvalSurface( 
		IwSurface*					pSurf, 
		double						uv[2], 
		IwPoint3d&					pt, 
		IwVector3d&					nm );

void EvalSurface( 
		IwSurface*					pSurf, 
		double						u,
		double						v,
		IwPoint3d&					pt, 
		IwVector3d&					nm );

void EvalSurfacePrincipalCurvature( 
		IwSurface*					pSurf, 
		double						uv[2],
		double&						crv1, 
		double&						crv2 );

void EvalSurfacePrincipalCurvature( 
		IwSurface*					pSurf, 
		double						u,
		double						v,
		double&						crv1, 
		double&						crv2 );

bool FilletBrep( 
		IwBrep*						pBrep, 
		CEdgeArray&					Edges, 
		double						rad );

void HighlightEdges( 
		CEdgeArray&					Edges, 
		bool						bHighlight );

IwBSplineCurve* ApproximatePoints( 
		const CPointArray&			pts, 
		double						tol = 0.0001 );

IwBSplineCurve*	InterpolatePoints(
		const CPointArray&			pts, 
		const IwVector3d&			tn0, 
		const IwVector3d&			tn1 );

IwBSplineCurve*	InterpolatePoints( 
		const CPointArray&			pts, 
		const CVectorArray&			tns );

IwBSplineCurve*	InterpolatePoints( 
		const CPointArray&			pts, 
		const CVectorArray&			tns, 
		const CDoubleArray&			knots );

IwBSplineCurve*	InterpolatePoints( 
		const CPointArray&			pts, 
		const CDoubleArray&			params );

IwBSplineCurve*	InterpolatePoints( 
		const CPointArray&			pts, 
		const CDoubleArray&			params,
		IwVector3d&					tn0,
		IwVector3d&					tn1 );

IwBSplineCurve*	MakeUniformSpline( 
		const CPointArray&			pts, 
		IwVector3d					tn0,				
		IwVector3d					tn1 );

IwBSplineCurve*	MakeUniformSpline( 
		const CPointArray&			pts ) ;

IwBSplineCurve*	MakeHermiteUniformSpline( 
		const CPointArray&			pts ) ;

IwBSplineSurface* MakeLoftSurface( 
		CSplineArray				Curves, 
		double						tol,
		IwBSplineCurve*				pRail1 = NULL,
		IwBSplineCurve*				pRail2 = NULL,
		CDoubleArray*				params = NULL,
		bool						bRemoveKnots = false );

IwBrep* MakeLoftBrep( 
		CSplineArray				Curves, 
		double						tol,
		IwBSplineCurve*				pRail1 = NULL,
		IwBSplineCurve*				pRail2 = NULL,
		CDoubleArray*				params = NULL,
		bool						bRemoveKnots = false );

IwBSplineSurface* MakeGordonSurface( 
		CSplineArray&				uCurves, 
		CSplineArray&				vCurves );

IwBrep* MakeGordonBrep( 
		CSplineArray&				uCurves, 
		CSplineArray&				vCurves );

IwBSplineSurface* MakeRuledSurface( 
		IwBSplineCurve*				pCurve0, 
		IwBSplineCurve*				pCurve1 );

IwBrep* MakeRuledBrep( 
		IwBSplineCurve*				pCurve0, 
		IwBSplineCurve*				pCurve1 );

IwBSplineSurface* ApproximateSurfaceByBSplineSurface( 
		IwBSplineSurface*			pSurf,
		int							nu,
		int							nv,
		bool						bDelete = true );

IwBrep* CutBrepByPlane( 
		IwBrep*						pBrep, 
		const IwPoint3d&			pt, 
		const IwVector3d&			nm, 
		double						*refSize );

IwBrep* CutBrepByPlanarPolygon( 
		IwBrep*						pBrep, 
		IwTArray<IwPoint3d>			planarPolygonPoints,
		const IwVector3d&			nm, 
		double						*refSize );

IwPlane* MakePlaneSurf( 
		const IwPoint3d&				pt,
		const IwVector3d&			vec0,
		const IwVector3d&			vec1,
		double						w,
		double						h );

IwBrep* MakePlane( 
		const IwPoint3d&				pt,
		const IwVector3d&			vx,
		const IwVector3d&			vy,
		double						w,
		double						h );

bool WriteBrep( 
		IwBrep*						pBrep, 
		const char*					filename );

bool WriteBrep( 
		IwBrep*						pBrep, 
		const QString&				sFileName );

IwBrep* ReadBrep( 
		const char*					filename );

IwBrep* ReadBrep( 
		const QString&				sFileName );

bool WriteSurfaces( 
		CSurfArray&					surfaces, 
		const char*					filename );

bool WriteSurfaces( 
		CSurfArray&					surfaces, 
		const QString&				sFileName );

bool ReadSurfaces( 
		const char*					filename,
		CSurfArray&				surfaces );

bool ReadSurfaces( 
		const QString&				sFileName,
		CSurfArray&				surfaces );

IwBrep* ReadIGES( 
		const QString&				sFileName );

IwTArray<IwCurve *> ReadCurvesFromIges( const QString& sFileName );

	bool WriteCurves( 
		CCurveArray&				curves, 
		const char*					filename );

bool WriteCurves( 
		CCurveArray&				curves, 
		const QString&				sFileName );

bool ReadCurves( 
		const char*					filename, 
		CCurveArray&				curves );

bool ReadCurves( 
		const QString&				sFileName, 
		CCurveArray&				curves );

bool WriteIwp( 
		IwBrep*						pBrep, 
		CCurveArray&				curves, 
		CSurfArray&					surfaces, 
		const char*					filename );

bool WriteIwp( 
		IwBrep*						pBrep, 
		CCurveArray&				curves, 
		CSurfArray&					surfaces, 
		const QString&				sFileName );

bool ReadIwp( 
		IwBrep**					ppBrep, 
		CCurveArray&				curves, 
		CSurfArray&					surfaces, 
		const char*					filename );

bool ReadIwp( 
		IwBrep**					ppBrep, 
		CCurveArray&				curves, 
		CSurfArray&					surfaces, 
		const QString&				sFileName );

void RenameIwbToIwp( 
		const char*					filename );

IwPoint3d ComputeArcCenter( 
		const IwPoint3d&				pt0,
		const IwPoint3d&				pt1,
		const IwPoint3d&				pt2 );
/*
void WriteIges( 
		const char*					filename,
		IwBrep*						pBrepTrimmedSurf,
		IwBrep*						pBrepSolid);

void WriteIges( 
		QString						filename,
		IwBrep*						pBrepTrimmedSurf,
		IwBrep*						pBrepSolid);

void WriteIges( 
		const char*					filename,
		IwBrep*						pBrep, 
		CSurfArray*					pSurfArray );
*/
/*
void WriteIges( 
		const char*					filename,
		CBrepArray*					pBrepArray, 
		CSurfArray*					pSurfArray );
*/
void WriteIges(	
		const char*					filename, 
		CPointArray*				pPointArray, 
		CCurveArray*				pCurveArray, 
		CSurfArray*					pSurfArray, 
		CBrepArray*					pBrepTrimmedSurfArray,
		CBrepArray*					pBrepSolidArray);

void WriteIges(	
		QString const&              filename, 
		CPointArray*				pPointArray, 
		CCurveArray*				pCurveArray, 
		CSurfArray*					pSurfArray, 
		CBrepArray*					pBrepTrimmedSurfArray,
		CBrepArray*					pBrepSolidArray);

IwFace* GetBrepFace( 
		IwBrep*						pBrep, 
		int							iUserIndex2 );

void CloseBrepHoles( 
		IwBrep*						pBrep );

CEdgeConvexity GetEdgeConvexity( 
		IwEdge*						pEdge );

bool IsConvexEdge( 
		IwEdge*						pEdge );

bool IsConcaveEdge( 
		IwEdge*						pEdge );

bool SewBrep( 
		IwBrep*						pBrep, 
		double						tol );

IwBSplineCurve* JoinCurves( 
		IwTArray<IwCurve*>&			Curves,
		bool						bDelete = true );

IwBSplineSurface* JoinSurfaces( 
		IwBSplineSurface*				Surface1,
		IwBSplineSurface*				Surface2);

IwFace*	MakeSimpleFaceFromCurves( 
		IwBrep*						pBrep, 
		IwSurface*					pSurface, 
		const CCurveArray&			Curves );

double ApproxEdgeLength( 
		IwEdge*						pEdge );

void OrientCurve( 
		IwCurve*					pCurve, 
		CCurveEnd					eStartEnd, 
		const						IwPoint3d& pt );

void TrimCurve( 
		IwCurve*					pCurve, 
		double						t0, 
		double						t1 );

void TrimCurve( 
		IwCurve*					pCurve, 
		const IwPoint3d&				pt0, 
		const IwPoint3d&				pt1 );

bool FilletEdges( 
		IwBrep*						pBrep, 
		CEdgeArray&					edges, 
		CDoubleArray&				rads );

bool FilletEdges( 
		IwBrep*						pBrep, 
		CEdgeArray&					edges, 
		double						rad );

double MarchAlongCurve( 
		IwCurve*					pCurve, 
		double						t0, 
		double						dt, 
		double						d_needed );

double ComputeCircleByThreePoints(
		const IwPoint3d&				p0,
		const IwPoint3d&				p1,
		const IwPoint3d&				p2,
		IwPoint3d&					cnt );

void InvertCurve( 
		IwCurve*					pCurve );

//IwBSplineCurve* SplitAndOrientCurve( 
//		IwBSplineCurve*				pCurve,
//		int							ic, 
//		CCompVal					eComp, 
//		CTransform&					trf );

//double FindExtremePointOnCurve( 
//		IwCurve*					pCurve, 
//		int							ic, 
//		CCompVal					eComp, 
//		CTransform&					trf, 
//		IwPoint3d*					pnt = NULL );

void AddLogPanelText( 
		const QString&				sText );

void GetFaceBounds( 
		IwFace*						pFace, 
		IwPoint3d&					pt0, 
		IwPoint3d&					pt1 );

int FindCurvePointsParallelToVector( 
		IwCurve*					pCurve, 
		double						t0,
		double						t1,
		const IwVector3d&			vec,
		CPointArray&				pts );

int FindCurvePointsPerpendicularToVector( 
		IwCurve*					pCurve, 
		double						t0,
		double						t1,
		const IwVector3d&			vec,
		CPointArray&				pts );

bool IntersectCurves( 
		IwCurve*					pCurve0, 
		IwCurve*					pCurve1, 
		const IwPoint3d&				pt, 
		IwPoint3d&					pnt,
		double&						t0,
		double&						t1 );

bool IntersectCurves( 
		IwCurve*					pCurve0, 
		IwCurve*					pCurve1, 
		IwExtent1d*					interval0,
		IwExtent1d*					interval1,
		const IwPoint3d&				pt, 
		IwPoint3d&					pnt,
		double&						t0,
		double&						t1 );

IwBSplineSurface* GetFaceSurface( 
		IwFace*						pFace );

IwBrep* MakeBrepFromSurface( 
		double						tol, 
		IwSurface*					pSurface );

double DistFromEdgeToSurface( 
		IwEdge*						pEdge, 
		IwSurface*					pSurf );

double DistFromCurveToEdge( 
		IwEdge*						pEdge, 
		IwCurve*					pCurve,
		IwPoint3d*					curvePnt=NULL,
		IwPoint3d*					edgePnt=NULL);

double DistFromCurveToEdges( 
		IwCurve*					pCurve,
		IwTArray<IwEdge*>			pEdges,
		IwPoint3d*					curvePnt=NULL,
		IwPoint3d*					edgePnt=NULL); 

double DistFromCurveToSurface(
		IwBSplineCurve*				pCurve, 
		IwExtent1d*					paramInterval, 
		IwSurface*					pSurf );

double DistFromCurveToSurface( 
		IwBSplineCurve*				pCurve, 
		IwSurface*					pSurf );

double DistFromCurveToSurface(
		IwBSplineCurve*				pCurve, 
		IwExtent1d					searchParamInterval, 
		IwSurface*					pSurf,
		IwVector2d*					guessSParam,
		IwPoint3d*					closestPointOnCurve,
		IwPoint3d*					closestPointOnSurface);
/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
double DistFromPointToBrep(
	    IwPoint3d					point,
		IwBrep*						brep,
		IwPoint3d					&closestPoint);

/////////////////////////////////////////////////////////
// This function determines the local minimum distance.
double DistFromPointToBrepLocal(
	    IwPoint3d					point,
		IwBrep*						brep,
		IwPoint3d					initGuess,
		IwPoint3d					&closestPoint);

/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
double DistFromPlaneToBrep(
	    IwPoint3d					point,
		IwVector3d					normal,
		IwVector3d					*xAxis,
		IwExtent2d					*domain,
		IwBrep*						brep,
		IwPoint3d					&closestPoint);

/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
double DistFromPointToSurface(
	    IwPoint3d					point,
		IwBSplineSurface*			surface,
		IwPoint3d					&closestPoint,
		IwPoint2d					&param);
/////////////////////////////////////////////////////////
double DistFromPointToSurfaces(
	    IwPoint3d					point,
		IwTArray<IwSurface*>		surfaces,
		IwPoint3d					&closestPoint,
		IwPoint2d					&param,
		int							&surfaceIndex);
/////////////////////////////////////////////////////////
// This function determines the local minimum distance.
double DistFromPointToSurface(
	    IwPoint3d					point,
		IwBSplineSurface*			surface,
		IwPoint2d					guessParam,
		IwPoint3d					&closestPoint,
		IwPoint2d					&param);

/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
double DistFromPointToEdge(
	    IwPoint3d					point,
		IwEdge*						edge,
		IwPoint3d					&closestPoint,
		IwVector3d*					optVector=NULL);

double DistFromPointToEdges(
	    IwPoint3d					point,
		IwTArray<IwEdge*>			edges,
		IwPoint3d					&closestPoint);

// This function determines the global minimum distance.
double DistFromPointToCurve(
	    IwPoint3d					point,
		IwBSplineCurve*				curve,
		IwPoint3d					&closestPoint,
		double						&param,
		IwExtent1d* searchInterval=NULL);
// This function determines the local minimum distance.
double DistFromPointToCurve(
	    IwPoint3d					point,
		IwBSplineCurve*				curve,
		double						guessParam,
		IwPoint3d					&closestPoint,
		double						&param);
// This function determines the global minimum distance.
double DistFromPointToCurves(
	    IwPoint3d					point,
		IwTArray<IwCurve*> const    &curves,
		IwPoint3d					&closestPoint,
		int							*crvIndex=NULL,
		double						*param=NULL
		);

double DistFromPointToIwTArray(
	    IwPoint3d					point,
		IwTArray<IwPoint3d>			points,
		IwVector3d*					normal,
		IwPoint3d					&closestPoint,
		int*						index);

double DistFromLineToIwTArray(
	    IwPoint3d					linePnt,
	    IwVector3d					lineVec,
		IwTArray<IwPoint3d>			points,
		IwVector3d*					normal,
		IwPoint3d					&closestPoint,
		int*						index);

/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
double DistFromCurveToCurve(
	    IwBSplineCurve*				firstCurve,
		IwBSplineCurve*				secondCurve,
		IwPoint3d					&firstClosestPoint,
		double						&firstParam,
		IwPoint3d					&secondClosestPoint,
		double						&secondParam,
		IwVector3d*					optVector = NULL
		);
/////////////////////////////////////////////////////////
// This function determines the global minimum distances.
bool DistFromCurveToCurve(
	    IwBSplineCurve*				firstCurve,
		IwBSplineCurve*				secondCurve,
		IwTArray<double>			&distances,
		IwTArray<IwPoint3d>			&firstClosestPoints,
		IwTArray<double>			&firstParams,
		IwTArray<IwPoint3d>			&secondClosestPoints,
		IwTArray<double>			&secondParams,
		double						*targetDist=NULL
		);
/////////////////////////////////////////////////////////
// This function determines the local minimum distance.
double DistFromCurveToCurve(
	    IwBSplineCurve*				firstCurve,
		IwExtent1d&					firstGuessInterval,
		IwBSplineCurve*				secondCurve,
		IwExtent1d&					secondGuessInterval,
		IwPoint3d					&firstClosestPoint,
		double						&firstParam,
		IwPoint3d					&secondClosestPoint,
		double						&secondParam
		);

double DistFromCurveEndToCurveEnd(
	    IwBSplineCurve*				firstCurve,
		IwBSplineCurve*				secondCurve,
		IwPoint3d					&firstClosestPoint,
		IwPoint3d					&secondClosestPoint,
		double						*firstParam=NULL,
		double						*secondParam=NULL
		);

double DistFromFaceToFace(
	IwFace* face0,
	IwFace* face1,
	IwPoint3d& pnt0,
	IwPoint3d& pnt1,
	IwVector3d* optVector=NULL
	);

double DistFromFacesToFaces(
	IwTArray<IwFace*> faces0,
	IwTArray<IwFace*> faces1,
	IwPoint3d& pnt0,
	IwPoint3d& pnt1,
	IwVector3d* optVector=NULL,
	double* tol=NULL
	);

double DistFromFacesToCurve(
	IwTArray<IwFace*> faces,
	IwCurve* curve,
	IwPoint3d& pnt0,
	IwPoint3d& pnt1,
	IwVector3d* optVector=NULL
	);

double DistFromFaceToCurve(
	IwFace* face,
	IwCurve* curve,
	IwPoint3d& pnt0,
	IwPoint3d& pnt1,
	IwVector3d* optVector=NULL
	);

IwBSplineCurve*	ExtendCurve( 
		IwBSplineCurve*				pSpline, 
		int							iside,
		double						d );

void SetColorAttr( 
		IwAObject*					pObject, 
		const CColor&				color );

void SetLabelAttr( 
		IwAObject*					pObject, 
		const QString&				sLabel );

void SetNameAttr( 
		IwAObject*					pObject, 
		const QString&				sName );

void MarkFaceEdges( 
		IwFace*						pFace, 
		int							flag );

IwBSplineCurve* ApproximateCurve( 
		IwBSplineCurve*				pCurve, 
		double						tol, 
		bool						bDelete );

IwBSplineCurve* ApproximateCurves( 
	    IwContext&					iwContext,
		IwTArray<IwBSplineCurve*>	curves, 
		double						tol);

IwBSplineCurve* ProjectCurveOnPlane( 
		IwBSplineCurve*				pCurve, 
		const IwPoint3d&				pt,
		const IwVector3d&			nm,
		const IwVector3d&			vec,
		bool						bDelete );

IwBSplineSurface* GetBrepSurface( 
		IwBrep*						pBrep );

void CopyFaces( 
		IwBrep*						pBrepFrom, 
		IwBrep*						pBrepTo );


void SplitBrepFaces( 
		IwBrep*						pBrep, 
		const CFaceArray&			Faces, 
		const IwPoint3d&				pt,
		const IwVector3d&			nm );

void SplitBrepFace(
		IwBrep*						pBrep, 
		IwFace*						pFace, 
		const IwPoint3d&				pt,
		const IwVector3d&			nm );

bool IntersectSurfaces(
		IwBSplineSurface*			pSurf1,	
		IwBSplineSurface*			pSurf2,
		CCurveArray&				Curves,
		double						tol = 0.00001,
		double						angtol = 0.001, // Radian
		IwTArray<IwCurve*>			*surface1UVCurves=NULL,
		IwTArray<IwCurve*>			*surface2UVCurves=NULL
		);

bool IntersectSurfaceByPlane
(
	IwBSplineSurface*		pSurf,				// I:
	IwPoint3d				pnt,				// I:
	IwVector3d				normal,				// I:
	CCurveArray&			Curves,				// O:
	double					tol = 0.001,		// I::
	double					angtol  = 0.01,		// I: radian
	IwTArray<IwCurve*>		*surfaceUVCurves=NULL// O:
);

void ShowPoint( 
		CDataDoc*					pDoc,
		const IwPoint3d&				pt, 
		const CColor&				color );

void ShowCurve( 
		CDataDoc*					pDoc,
		IwCurve*					pCurve, 
		const CColor&				color );

void ShowEdge(
		CDataDoc*					pDoc,
		IwEdge*						pEdge, 
		const CColor&				color );

void ShowEdges( 
		CDataDoc*					pDoc,
		const CEdgeArray&			edges,
		const CColor&				color);

void ShowFace(
		CDataDoc*					pDoc,
		IwFace*						pFace,
		const CColor&				color);

void ShowFaces(
		CDataDoc*					pDoc,
		IwTArray<IwFace*>			pFaces,
		const CColor&				color);

void ShowBrepEdge(
		CDataDoc*					pDoc,
		IwBrep*						pBrep, 
		int							iEdge, 
		const CColor&				color );

CPart* ShowBrep( 
		CDataDoc*					pDoc,
		IwBrep*						pBrep,
		const char*					sPartName,
		const CColor&				color );

void ShowSurface( 
		CDataDoc*					pDoc,
		IwSurface*					pSurf,
		const char*					sName,
		const CColor&				color );

long GenerateIwBrepDisplayList( 
		CDataDoc*					pDoc,
		IwBrep*						pBrep,
		bool						bDisplayIsoCurves,
		CColor						color,
		long&						faceDisplayList,
		long&						edgeDisplayList);

double ComputePointOnCurveAtDist(
		IwBSplineCurve*				pCurve,
		double						tStart,
		double						dist,
		IwPoint3d*					pt = NULL );

void ComputeEquallySpacedPoints(
		IwBSplineCurve*				pCurve,
		double						t0,	
		double						t1,
		int							np,
		IwTArray<IwPoint3d>&		pts,
		IwTArray<double>*			params = NULL );

IwFace* GetEdgeOtherFace( 
		IwEdge*						pEdge, 
		IwFace*						pFace );

IwEdge* GetFaceOtherEdge( 
		IwFace*						pFace,
		IwEdge*						pEdge 
		 );

void GetFaceConnectedEdges( 
		IwFace*						pFace,
		IwEdge*						pEdge,
		IwEdge*&					posEdge,
		IwEdge*&					negEdge
		 );

void TraceAdjacentFaces(
		IwFace*						pFace,
		IwEdge*						pEdge,
		int							traceSteps,
		IwTArray<IwFace*>&			adjFaces);

void GetFaceNeighbors( 
		IwFace*						pFace, 
		CFaceArray&					NeighborFaces );

IwEdge* GetFacesCommonEdge( 
		IwFace*						pFace0, 
		IwFace*						pFace1 );

int FindCurveInflectionPoints( 
		IwCurve*					pCurve, 
		double						t0,
		double						t1,
		CDoubleArray&				params,
		CPointArray&				pts );

void ComputeBestCubicSeg(
		IwPoint3d&					pt0,
		IwPoint3d&					pt1,
		IwVector3d&					tn0,
		IwVector3d&					tn1 );

IwPoint3d DropPointOnBrep( 
		IwBrep*						pBrep, 
		IwPoint3d&					pt, 
		double*						uv, 
		IwFace*&					pFace );

IwPoint3d DropPointOnBrep( 
		IwBrep*						pBrep, 
		IwPoint3d&					pt );


void FindPointOnRayAtHeightAboveFemur( 
		IwBrep*						pBrep, 
		IwPoint3d					p0,
		IwVector3d					vec,
		double						height,
		IwPoint3d&					pt );

IwPoint3d DropPointOnSurface( 
		IwSurface*					pSurf, 
		const IwPoint3d&				pt, 
		double*						uv = NULL );

IwPoint3d DropPointOnPlane( 
		const IwPoint3d&				planeOrigin, 
		const IwVector3d&			planeNormal,
		const IwPoint3d&				pointToBeDropped);

bool ThickenPlane(
		IwBSplineSurface*			basePlane,
		double						thickenDist,
		IwBrep*&					rBrep);

bool ThickenSurface(
		IwBSplineSurface*			baseSurface,
		double						thickenDist,
		IwBrep*&					rBrep);

IwBSplineCurve* CreateArc(
		IwContext&					iwContext,
		IwPoint3d&					startPoint, 
		IwVector3d&					startVector,
		IwPoint3d&					endPoint,
		IwPoint3d&					arcCenter,
		double&						radius);

IwBSplineCurve* CreateArc(
		IwContext&					iwContext,
		IwPoint3d&					startPoint, 
		IwVector3d&					startVector,
		IwVector3d&					upVector,
		double						radius,
		double						angleDegrees
		);

IwBSplineCurve* CreateArc3(
		IwContext&					iwContext,
		IwPoint3d&					startPoint, 
		IwPoint3d&					midPoint,
		IwPoint3d&					endPoint,
		IwPoint3d&					arcCenter,
		double&						radius);

IwBSplineCurve* CreateArcOnPlane(
		IwContext&					iwContext,
		IwPoint3d					startPoint, 
		IwPoint3d					endPoint,
		IwPoint3d					planePoint,
		IwVector3d					planeNormal,
		double						radius,
		IwPoint3d&					centerPoint
		);

IwBrep* CreateLinearSweep(
		IwContext&					iwContext,
		IwTArray<IwCurve*>		profileCurves,
		IwVector3d					sweepVector,
		bool						bCapEnds,
		CDataDoc*					pDoc=NULL	);

void SortIwTArray(IwTArray<double>& iwArray, bool ascending=true);
void SortIwTArray(IwTArray<IwPoint3d>& iwArray, int elementToSort=0, bool ascending=true);
void EvenPointsSpace(IwTArray<IwPoint3d>& pointsToEven);

void ChainLoopConvexOffset
(
	IwContext& iwContext,
	IwTArray<IwBSplineCurve*> chainLoop,
	IwVector3d offsetNormal,
	double offsetDistance,
	IwTArray<IwBSplineCurve*>& offsetCurves
);
void ChainLoopConvexCleanUp(IwTArray<IwBSplineCurve*>& chainLoop);

bool IsPointInsideTriangle(IwPoint3d point, IwPoint3d t0, IwPoint3d t1, IwPoint3d t2); 

IwBSplineCurve* OffsetCurveAlongSurfaceAndNormal(IwContext& iwContext, IwBSplineSurface* surf, IwBSplineCurve* uvCrv, double sideOffsetStart, double sideOffsetEnd, double endExtension, double normalOffset, int intervalSize=0);
IwBSplineCurve* OffsetCurveAlongSurfaceNormal(IwContext& iwContext, IwBSplineSurface* surf, IwBSplineCurve* uvCrv, double dOffset, int intervalSize=0, double decay=0);

IwVector3d RotateVectorAlongVector(IwVector3d inputVector, IwPoint3d rotatingCenter, IwVector3d rotatingAxis, double rotatingAngleRadian);

bool IsConicSurface(IwBSplineSurface* surf, bool* isCylinder=NULL, bool* isFillet=NULL, IwAxis2Placement* conicAxis=NULL);
bool IsRuledSurface(IwBSplineSurface* surf, IwVector3d* rulingVector=NULL);

IwEdgeuse* GetFilletEdgeuse(IwEdge* pEdge);

IwPoint3d MirrorPointAlongLine(IwPoint3d pntToMirror, IwPoint3d linePnt, IwVector3d lineVector);

bool MarchToEqualDistance(IwPoint3d pnt, IwVector3d marchDir, IwBSplineCurve* curve, IwPoint3d& equalDistPosition);

bool GetEdgeLengths(IwBrep* pBrep, IwTArray<double>& edgeLengths, IwTArray<IwEdge*>& edges);

bool IsFileNameExistInFolder(QString folderName, QString fileNameExt, QString fileName0, QString fileName1, QString fileName2);