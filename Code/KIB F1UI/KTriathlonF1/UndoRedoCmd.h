#pragma once

#include <QtGui>
#include <QUndoStack>
#include <QUndoCommand>
#include "MakeFemoralAxes.h"
#include "DefineOutlineProfile.h"

#include "..\KAppTotal\Manager.h"

class MakeFemoralAxesCmd : public QUndoCommand
{
	public:
    MakeFemoralAxesCmd(CManager* manager, 
		CMakeFemoralAxesUndoData &prevUndoData, 
		CMakeFemoralAxesUndoData &postUndoData, 
		QUndoCommand *parent = 0);
    void undo();
    void redo();

private:
	CManager*			m_pManager;
	CMakeFemoralAxesUndoData	m_prevUndoData;
	CMakeFemoralAxesUndoData	m_postUndoData;	
};

//class CutFemurCmd : public QUndoCommand
//{
//	public:
//    CutFemurCmd(CManager* manager, 
//		CCutFemurUndoData &prevUndoData, 
//		CCutFemurUndoData &postUndoData, 
//		QUndoCommand *parent = 0);
//    void undo();
//    void redo();
//
//private:
//	CManager*			m_pManager;
//	CCutFemurUndoData	m_prevUndoData;
//	CCutFemurUndoData	m_postUndoData;
//	
//};


class DefineOutlineProfileCmd : public QUndoCommand
{
	public:
    DefineOutlineProfileCmd(CManager* manager, 
		CDefineOutlineProfileUndoData &prevUndoData, 
		CDefineOutlineProfileUndoData &postUndoData, 
		QUndoCommand *parent = 0);
    void undo();
    void redo();

private:
	CManager*						m_pManager;
	CDefineOutlineProfileUndoData	m_prevUndoData;
	CDefineOutlineProfileUndoData	m_postUndoData;
};
