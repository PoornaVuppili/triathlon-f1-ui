#ifndef TESSTRIANGLE_H
#define TESSTRIANGLE_H

#include <vector>
#include "TPoint3.h"
#include "TVector3.h"

template <typename T>
class TTessTriangle
{
public:
    TPoint3<T> vertex[3];
    TVector3<T> normal[3];

    // calculate the distance from a point to triangle
    //  point         (I): input point
    //  closest       (O): if not null, the closest point
    //  closestnormal (O): if not null, the closest point normal
    T Distance(const TPoint3<T>& point, TPoint3<T>* closest=nullptr, TVector3<T>* closestnormal=nullptr) const;
};

//=============== Implementation ===============

template <typename T>
T TTessTriangle<T>::Distance(const TPoint3<T>& point, TPoint3<T>* closest, TVector3<T>* closestnormal) const
{
    TVector3<T> diff = vertex[0] - point;
    TVector3<T> edge0 = vertex[1] - vertex[0];
    TVector3<T> edge1 = vertex[2] - vertex[0];
    T a00 = edge0.dot(edge0);
    T a01 = edge0.dot(edge1);
    T a11 = edge1.dot(edge1);
    T b0 = diff.dot(edge0);
    T b1 = diff.dot(edge1);
    T c = diff.dot(diff);
    T det = std::abs(a00*a11 - a01*a01);
    T s = a01*b1 - a11*b0;
    T t = a01*b0 - a00*b1;
    T sqrDistance;

    if (s + t <= det)
    {
        if (s < (T)0)
        {
            if (t < (T)0)  // region 4
            {
                if (b0 < (T)0)
                {
                    t = (T)0;
                    if (-b0 >= a00)
                    {
                        s = (T)1;
                        sqrDistance = a00 + ((T)2)*b0 + c;
                    }
                    else
                    {
                        s = -b0 / a00;
                        sqrDistance = b0*s + c;
                    }
                }
                else
                {
                    s = (T)0;
                    if (b1 >= (T)0)
                    {
                        t = (T)0;
                        sqrDistance = c;
                    }
                    else if (-b1 >= a11)
                    {
                        t = (T)1;
                        sqrDistance = a11 + ((T)2)*b1 + c;
                    }
                    else
                    {
                        t = -b1 / a11;
                        sqrDistance = b1*t + c;
                    }
                }
            }
            else  // region 3
            {
                s = (T)0;
                if (b1 >= (T)0)
                {
                    t = (T)0;
                    sqrDistance = c;
                }
                else if (-b1 >= a11)
                {
                    t = (T)1;
                    sqrDistance = a11 + ((T)2)*b1 + c;
                }
                else
                {
                    t = -b1 / a11;
                    sqrDistance = b1*t + c;
                }
            }
        }
        else if (t < (T)0)  // region 5
        {
            t = (T)0;
            if (b0 >= (T)0)
            {
                s = (T)0;
                sqrDistance = c;
            }
            else if (-b0 >= a00)
            {
                s = (T)1;
                sqrDistance = a00 + ((T)2)*b0 + c;
            }
            else
            {
                s = -b0 / a00;
                sqrDistance = b0*s + c;
            }
        }
        else  // region 0
        {
            // minimum at interior point
            T invDet = ((T)1) / det;
            s *= invDet;
            t *= invDet;
            sqrDistance = s*(a00*s + a01*t + ((T)2)*b0) +
                t*(a01*s + a11*t + ((T)2)*b1) + c;
        }
    }
    else
    {
        T tmp0, tmp1, numer, denom;

        if (s < (T)0)  // region 2
        {
            tmp0 = a01 + b0;
            tmp1 = a11 + b1;
            if (tmp1 > tmp0)
            {
                numer = tmp1 - tmp0;
                denom = a00 - ((T)2)*a01 + a11;
                if (numer >= denom)
                {
                    s = (T)1;
                    t = (T)0;
                    sqrDistance = a00 + ((T)2)*b0 + c;
                }
                else
                {
                    s = numer / denom;
                    t = (T)1 - s;
                    sqrDistance = s*(a00*s + a01*t + ((T)2)*b0) +
                        t*(a01*s + a11*t + ((T)2)*b1) + c;
                }
            }
            else
            {
                s = (T)0;
                if (tmp1 <= (T)0)
                {
                    t = (T)1;
                    sqrDistance = a11 + ((T)2)*b1 + c;
                }
                else if (b1 >= (T)0)
                {
                    t = (T)0;
                    sqrDistance = c;
                }
                else
                {
                    t = -b1 / a11;
                    sqrDistance = b1*t + c;
                }
            }
        }
        else if (t < (T)0)  // region 6
        {
            tmp0 = a01 + b1;
            tmp1 = a00 + b0;
            if (tmp1 > tmp0)
            {
                numer = tmp1 - tmp0;
                denom = a00 - ((T)2)*a01 + a11;
                if (numer >= denom)
                {
                    t = (T)1;
                    s = (T)0;
                    sqrDistance = a11 + ((T)2)*b1 + c;
                }
                else
                {
                    t = numer / denom;
                    s = (T)1 - t;
                    sqrDistance = s*(a00*s + a01*t + ((T)2)*b0) +
                        t*(a01*s + a11*t + ((T)2)*b1) + c;
                }
            }
            else
            {
                t = (T)0;
                if (tmp1 <= (T)0)
                {
                    s = (T)1;
                    sqrDistance = a00 + ((T)2)*b0 + c;
                }
                else if (b0 >= (T)0)
                {
                    s = (T)0;
                    sqrDistance = c;
                }
                else
                {
                    s = -b0 / a00;
                    sqrDistance = b0*s + c;
                }
            }
        }
        else  // region 1
        {
            numer = a11 + b1 - a01 - b0;
            if (numer <= (T)0)
            {
                s = (T)0;
                t = (T)1;
                sqrDistance = a11 + ((T)2)*b1 + c;
            }
            else
            {
                denom = a00 - ((T)2)*a01 + a11;
                if (numer >= denom)
                {
                    s = (T)1;
                    t = (T)0;
                    sqrDistance = a00 + ((T)2)*b0 + c;
                }
                else
                {
                    s = numer / denom;
                    t = (T)1 - s;
                    sqrDistance = s*(a00*s + a01*t + ((T)2)*b0) +
                        t*(a01*s + a11*t + ((T)2)*b1) + c;
                }
            }
        }
    }

    // Account for numerical round-off error.
    if (sqrDistance < (T)0)
    {
        sqrDistance = (T)0;
    }

    if( closest )
        *closest = vertex[0] + s*edge0 + t*edge1;
    if( closestnormal )
        *closestnormal = (normal[0] + s*(normal[1]-normal[0]) + t*(normal[2]-normal[0])).normalized();
    return sqrt(sqrDistance);
}

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TTessTriangle<int> TessTriangleInt;
typedef TTessTriangle<float> TessTriangleFloat;
typedef TTessTriangle<double> TessTriangleDouble;

#endif //TESSTRIANGLE_H
