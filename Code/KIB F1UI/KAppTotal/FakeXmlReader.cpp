#include "FakeXmlReader.h"
#include <qfile.h>
#include "Part.h"

CFakeXmlReader::CFakeXmlReader( const QString& sFileName )
{
	m_sFileName = sFileName;
}


CFakeXmlReader::~CFakeXmlReader()
{
}

bool CFakeXmlReader::GetIwPoint3d(const QString& tag, IwPoint3d& point)
{
	bool OK = false;

	QString str = this->GetString(tag);
	if ( !str.isEmpty() )
	{
		QStringList sl = str.split(",");
		if( sl.size() == 3 )
		{
			point = IwPoint3d( sl[0].toFloat(), sl[1].toFloat(), sl[2].toFloat() );
			OK = true;
		}
	}

	return OK;
}

QString CFakeXmlReader::GetString(const QString& tag)
{
	QString str;
	QString	sLine;
	int stIdx, edIdx;
	QString startTag = QString("<") + tag + QString(">");
	QString endTag = QString("</") + tag + QString(">");
	int tagLength = startTag.size();

	if( !QFile::exists(m_sFileName) )
        return str;

    QFile file( m_sFileName );

	QTextStream				in( &file );

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return str;

	do
	{
		sLine = in.readLine();
		stIdx = sLine.indexOf( startTag );
		edIdx = sLine.indexOf( endTag, stIdx+1 );
		if (stIdx != -1 && edIdx != -1)
		{
			str = sLine.mid(stIdx+tagLength, edIdx-stIdx-tagLength);
			break;
		}

	} while ( !in.atEnd() );

	file.close();

	return str;
}