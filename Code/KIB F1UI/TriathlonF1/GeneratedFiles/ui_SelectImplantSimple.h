/********************************************************************************
** Form generated from reading UI file 'SelectImplantSimple.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTIMPLANTSIMPLE_H
#define UI_SELECTIMPLANTSIMPLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectImplantSimpleClass
{
public:
    QWidget *layoutWidget_1;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButtonOpen;
    QPushButton *pushButtonCancel;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout;
    QLabel *label_6;
    QLineEdit *lineEdit;
    QPushButton *pushButtonBrowse;
    QLabel *label;
    QComboBox *comboImplantList;
    QLabel *labelInfo;

    void setupUi(QDialog *SelectImplantSimpleClass)
    {
        if (SelectImplantSimpleClass->objectName().isEmpty())
            SelectImplantSimpleClass->setObjectName(QString::fromUtf8("SelectImplantSimpleClass"));
        SelectImplantSimpleClass->setWindowModality(Qt::ApplicationModal);
        SelectImplantSimpleClass->resize(424, 168);
        layoutWidget_1 = new QWidget(SelectImplantSimpleClass);
        layoutWidget_1->setObjectName(QString::fromUtf8("layoutWidget_1"));
        layoutWidget_1->setGeometry(QRect(216, 116, 193, 37));
        horizontalLayout_5 = new QHBoxLayout(layoutWidget_1);
        horizontalLayout_5->setSpacing(10);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        pushButtonOpen = new QPushButton(layoutWidget_1);
        pushButtonOpen->setObjectName(QString::fromUtf8("pushButtonOpen"));
        pushButtonOpen->setEnabled(false);

        horizontalLayout_5->addWidget(pushButtonOpen);

        pushButtonCancel = new QPushButton(layoutWidget_1);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        horizontalLayout_5->addWidget(pushButtonCancel);

        layoutWidget_2 = new QWidget(SelectImplantSimpleClass);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(16, 23, 397, 58));
        gridLayout = new QGridLayout(layoutWidget_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(layoutWidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 0, 0, 1, 1);

        lineEdit = new QLineEdit(layoutWidget_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 1, 1, 1);

        pushButtonBrowse = new QPushButton(layoutWidget_2);
        pushButtonBrowse->setObjectName(QString::fromUtf8("pushButtonBrowse"));

        gridLayout->addWidget(pushButtonBrowse, 0, 2, 1, 1);

        label = new QLabel(layoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        comboImplantList = new QComboBox(layoutWidget_2);
        comboImplantList->setObjectName(QString::fromUtf8("comboImplantList"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboImplantList->sizePolicy().hasHeightForWidth());
        comboImplantList->setSizePolicy(sizePolicy);

        gridLayout->addWidget(comboImplantList, 1, 1, 1, 1);

        labelInfo = new QLabel(SelectImplantSimpleClass);
        labelInfo->setObjectName(QString::fromUtf8("labelInfo"));
        labelInfo->setGeometry(QRect(20, 88, 393, 16));
        QPalette palette;
        QBrush brush(QColor(0, 85, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush1(QColor(120, 120, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        labelInfo->setPalette(palette);
        QWidget::setTabOrder(lineEdit, pushButtonBrowse);
        QWidget::setTabOrder(pushButtonBrowse, comboImplantList);
        QWidget::setTabOrder(comboImplantList, pushButtonOpen);
        QWidget::setTabOrder(pushButtonOpen, pushButtonCancel);

        retranslateUi(SelectImplantSimpleClass);

        pushButtonOpen->setDefault(true);


        QMetaObject::connectSlotsByName(SelectImplantSimpleClass);
    } // setupUi

    void retranslateUi(QDialog *SelectImplantSimpleClass)
    {
        SelectImplantSimpleClass->setWindowTitle(QCoreApplication::translate("SelectImplantSimpleClass", "Select an implant", nullptr));
        pushButtonOpen->setText(QCoreApplication::translate("SelectImplantSimpleClass", "Open", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("SelectImplantSimpleClass", "Cancel", nullptr));
        label_6->setText(QCoreApplication::translate("SelectImplantSimpleClass", "Implant root:", nullptr));
        pushButtonBrowse->setText(QCoreApplication::translate("SelectImplantSimpleClass", "Browse...", nullptr));
        label->setText(QCoreApplication::translate("SelectImplantSimpleClass", "Serial:", nullptr));
        labelInfo->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SelectImplantSimpleClass: public Ui_SelectImplantSimpleClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTIMPLANTSIMPLE_H
