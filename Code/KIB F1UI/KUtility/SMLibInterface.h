#ifndef SMLIBINTERFACE_H
#define SMLIBINTERFACE_H

#pragma warning( disable : 4996 4805 )

#ifdef GetObject
#undef GetObject
#endif

#include <iwos_extern.h>
#include <IwBSplineCurve.h>
#include <IwBSplineSurface.h>
#include <IwTArray.h>
#include <hwtranslatorgeneric.h>
#include "IwBrep.h"
#pragma warning( default : 4996 4805 )

// Context object for SMlib
extern IwContext* iwContext;

extern void ClearContext();
extern void ResetContext();

// Class with the interface of HwProgressTrackerInterface
class HwProgress : public HwProgressTrackerInterface
{
public:
    HwProgress() : counter(0) {}

    virtual IwStatus Heartbeat();
    virtual IwStatus StartCountdown( unsigned int count );
    virtual IwStatus Tick ();
    virtual IwStatus EndCountdown ();
private:
    unsigned int counter;
};

#endif //SMLIBINTERFACE_H
