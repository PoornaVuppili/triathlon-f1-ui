#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class QLineEdit;

class TEST_EXPORT_TW CMeasureScreenAngle : public CManager
{
    Q_OBJECT

public:
					CMeasureScreenAngle( CTotalView* pView );
	virtual			~CMeasureScreenAngle();
    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseRight ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* viewport=NULL );
	bool			Reject() {OnAccept();return true;};

	virtual void	Display(Viewport* vp=NULL);

private:

public slots:
	void			OnAccept();

private slots:
	void			OnTwoPointsCalculation();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actTwoPointsCalculation;

	QLineEdit*					m_textMeasuredAngle;

	bool						m_mouseLeftDown;
	bool						m_mouseMiddleDown;

	int							m_mouseClickedCount;
	IwPoint3d					m_mouseFirstDownPoint;
	IwPoint3d					m_mouseSecondDownPoint;
	IwPoint3d					m_mouseThirdDownPoint;
	IwPoint3d					m_mouseFourthDownPoint;
};
