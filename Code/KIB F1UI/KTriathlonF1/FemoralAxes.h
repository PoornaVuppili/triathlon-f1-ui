#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Entity.h"
#include "..\KAppTotal\valgebra.h"
#include "..\KAppTotal\Basics.h"
#include "..\KUtility\CoordinateSystem.h"
#pragma warning( disable : 4996 4805 )
#include <IwTArray.h>
#pragma warning( default : 4996 4805 )

class CTotalDoc;

class TEST_EXPORT_TW CFemoralAxes : public CEntity
{
	Q_OBJECT

public:
	CFemoralAxes( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_FEMORAL_AXES, const QString& sFileName="", int nEntId=-1 );
	~CFemoralAxes();
	
	virtual void	Display();
	bool			Save( const QString& sDir, bool incrementalSave=true );
	bool			Load( const QString& sDir );
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);
	virtual CBounds3d		GetBounds() {return m_Bounds;};
	virtual CBounds3d		ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL );

	void			SetParams(IwPoint3d&, IwPoint3d&, IwPoint3d&, IwPoint3d&, double, double, double, IwPoint3d&, IwPoint3d&, double, double);
	void			GetParams(IwPoint3d&, IwPoint3d&, IwPoint3d&, IwPoint3d&, double&, double&, double&, IwPoint3d&, IwPoint3d&, double&, double&);
	void			GetAnteriorCutFlexionObliqueAngles(double& flexAngle, double& obliAngle){flexAngle=m_antCutFlexionAngle;obliAngle=m_antCutObliqueAngle;};
	void			GetAntCutPlaneInfo(IwPoint3d& antCutPlaneCenter, IwVector3d& antCutPlaneOrientation, IwVector3d* antCutPlaneRotationCenter=NULL);
	void			GetMechanicalAxes(IwAxis2Placement&);
	void			GetImplantAxes(IwAxis2Placement&);
	void			GetWhiteSideLineAxes(IwAxis2Placement&);
	void			GetSweepAxes(IwAxis2Placement&);
	double			GetEpiCondyleSize() {return m_size;};
	void			GetAxesPoints(IwTArray<IwPoint3d>& axesPoints);
	void			DisplayAntCutPlane() {m_displayAntCutPlane = !m_displayAntCutPlane;};
	bool			GetDisplayAntCutPlane() {return m_displayAntCutPlane;};
	void			SetParamsValidation(IwPoint3d&, IwPoint3d&, IwPoint3d&, IwPoint3d&, double, double, double, IwPoint3d&);
	void			GetParamsValidation(IwPoint3d&, IwPoint3d&, IwPoint3d&, IwPoint3d&, double&, double&, double&, IwPoint3d&);
	void			EnableCTScanDisplayOnTroclearPlane(bool enable);
	void			SetBackUpAntCutPlaneRotationCenters(IwTArray<IwPoint3d> centerPoints);
	bool			GetBackUpAntCutPlaneRotationCenters(IwTArray<IwPoint3d>& centerPoints);
	void			EmptyBackUpAntCutPlaneRotationCenters() {m_backUpAntCutPlaneRotationCenters.RemoveAll();};
	void			GetCutPlaneCorners(IwTArray<IwPoint3d>& cutPlaneCorners) {cutPlaneCorners.RemoveAll();cutPlaneCorners.Append(m_cutPlaneCorners);};
	bool			GetBearingRegionBoundaries(IwTArray<IwCurve*>& posSilhouettes, IwTArray<IwCurve*>& negSilhouettes);
	void			SetBearingRegionBoundaries(IwTArray<IwCurve*>& posSilhouettes, IwTArray<IwCurve*>& negSilhouettes);

	void            SetProximalRefPoint(IwPoint3d &proximalRefPoint);
	IwPoint3d       GetRefProximalPoint();


signals:
	void			BroadcastCoordSystem(const CoordSystem&, bool);

private:
	bool			DetermineAxes();
	void			SetModifiedFlag( bool bModified );
	void			DisplayAnteriorPlane();

private:
	bool			m_bModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bDataModified;// Geometric entities have not been saved yet to the file. 
	bool			m_bDisplayCTScanOnTroclearPlane;

	const static QString axesName;
	const static QString mechAxesName;

	CBounds3d				m_Bounds;
	IwPoint3d				m_hipPoint;
	IwPoint3d				m_proximalPoint;
	IwPoint3d				m_leftEpiCondylarPoint; // left side of the patient
	IwPoint3d				m_rightEpiCondylarPoint;// right side of the patient
	double					m_antCutFlexionAngle;
	double					m_antCutObliqueAngle;
	double					m_femoralAxesFlexionAngle;
	double					m_femoralAxesYRotAngle;
	double					m_femoralAxesZRotAngle;
	IwPoint3d				m_antCutPlaneCenter;			// Anterior cut plane center. Also serve as default anterior outline ear shape indentation center.
	IwPoint3d				m_antCutPlaneRotationCenter;	// new in iTWv6. Generally it is the anterior peak of the lateral bone cut profile. Users want to fix this point  when adjust axis and flexion angle.
	IwAxis2Placement		m_mechanicalAxes;				// use m_hipPoint and m_proximalPoint to define the z-axis. this axes never been changed by m_antCutFlexionAngle and m_femoralAxesFlexionAngle.
	IwAxis2Placement		m_sweepAxes;					// The axes for constructing outer surface. m_antCutFlexionAngle and m_femoralAxesFlexionAngle will flex the axes.
	IwAxis2Placement		m_whiteSideLineAxes;			// The femoral axes to construct the femoral implant. The only difference between m_sweepAxes and m_whiteSideLineAxes is the origin. m_antCutFlexionAngle and m_femoralAxesFlexionAngle will flex the axes.
	double					m_size;
	IwTArray<IwPoint3d>		m_axesPoints;					// for display purpose only
	IwTArray<IwPoint3d>		m_mechAxesPoints;				// for display purpose only
	IwTArray<IwPoint3d>		m_cutPlaneCorners;
	bool					m_displayAntCutPlane;
	IwTArray<IwCurve*>		m_posSilhouetteCurves;			// To indicate bearing region boundary
	IwTArray<IwCurve*>		m_negSilhouetteCurves;			// To indicate bearing region boundary

	//// For Validation. The ideal locations determined during validation  //////////////////////////
	IwPoint3d				m_hipPointValidation;
	IwPoint3d				m_proximalPointValidation;
	IwPoint3d				m_leftEpiCondylarPointValidation; 
	IwPoint3d				m_rightEpiCondylarPointValidation;
	double					m_antCutFlexionAngleValidation;
	double					m_antCutObliqueAngleValidation;
	double					m_femoralAxesFlexionAngleValidation;
	IwPoint3d				m_antCutPlaneCenterValidation;

	//// Initialization data, avialable only during initialization //////////////////
	IwTArray<IwPoint3d>		m_backUpAntCutPlaneRotationCenters;

	IwPoint3d				m_proximalRefPoint;

};