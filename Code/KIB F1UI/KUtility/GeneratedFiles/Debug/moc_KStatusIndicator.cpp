/****************************************************************************
** Meta object code from reading C++ file 'KStatusIndicator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../KStatusIndicator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KStatusIndicator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KStatusIndicator_t {
    QByteArrayData data[11];
    char stringdata0[100];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KStatusIndicator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KStatusIndicator_t qt_meta_stringdata_KStatusIndicator = {
    {
QT_MOC_LITERAL(0, 0, 16), // "KStatusIndicator"
QT_MOC_LITERAL(1, 17, 11), // "ShowMessage"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 3), // "msg"
QT_MOC_LITERAL(4, 34, 12), // "bForceUpdate"
QT_MOC_LITERAL(5, 47, 12), // "ProgressInit"
QT_MOC_LITERAL(6, 60, 4), // "maxv"
QT_MOC_LITERAL(7, 65, 4), // "minv"
QT_MOC_LITERAL(8, 70, 11), // "ProgressSet"
QT_MOC_LITERAL(9, 82, 3), // "val"
QT_MOC_LITERAL(10, 86, 13) // "ProgressReset"

    },
    "KStatusIndicator\0ShowMessage\0\0msg\0"
    "bForceUpdate\0ProgressInit\0maxv\0minv\0"
    "ProgressSet\0val\0ProgressReset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KStatusIndicator[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   44,    2, 0x0a /* Public */,
       1,    1,   49,    2, 0x2a /* Public | MethodCloned */,
       5,    1,   52,    2, 0x0a /* Public */,
       5,    2,   55,    2, 0x0a /* Public */,
       8,    1,   60,    2, 0x0a /* Public */,
      10,    0,   63,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    6,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,

       0        // eod
};

void KStatusIndicator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KStatusIndicator *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ShowMessage((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 1: _t->ShowMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->ProgressInit((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->ProgressInit((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->ProgressSet((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->ProgressReset(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KStatusIndicator::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_KStatusIndicator.data,
    qt_meta_data_KStatusIndicator,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KStatusIndicator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KStatusIndicator::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KStatusIndicator.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int KStatusIndicator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
