#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "FemoralCuts.h"
#include "FemoralPart.h"
#include "FemoralAxes.h"
#include "OutlineProfile.h"
#include "AdvancedControl.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"
#include "IFemurImplant.h"

CFemoralCuts::CFemoralCuts(  CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_FEMORAL_CUTS );

	m_isFeatPntsUpToDate = false;

	m_glOffsetViewProfileCurveList = 0;
	m_glOuterSideSurfaceIntersectionCurveList = 0;
	m_glCutEdgesCurveList = 0;
	m_bOffsetViewProfileModified = false;
	m_bOuterSideSurfaceIntersectionModified = false;
	m_bCutEdgesModified = false;
	m_bFCDataModified = false;
	m_bOffsetViewProfileDisplay = false; 
	m_bOuterSideSurfaceIntersectionDisplay = false;

	m_cutPoints.SetSize(11);
	m_cutOrientations.SetSize(11);
	m_additionalDistalCutsDistance = 0.0;
	m_forceToOptimizeCuts = false;

	m_eDisplayMode = DISP_MODE_DISPLAY;
}

CFemoralCuts::~CFemoralCuts()
{

}

bool CFemoralCuts::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bFCDataModified ) 
		return bOK;

	// 		
	sFileName = sDir + "\\" + m_sFileName + "_fc.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	unsigned nSize = m_cutPoints.GetSize();
	File.write( (char*) &nSize, sizeof( int ) );

	for (unsigned i=0; i<nSize; i++)
		File.write( (char*) &m_cutPoints.GetAt(i), sizeof( IwPoint3d ) );

	for (unsigned i=0; i<nSize; i++)
		File.write( (char*) &m_cutOrientations.GetAt(i), sizeof( IwVector3d ) );


	File.close();

	m_bFCDataModified = false;

	//// For Validation //////////////
	sFileName = sDir + "\\" + m_sFileName + "_fc_validation.dat~";

	QFile				FileValidation( sFileName );

	bOK = FileValidation.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	nSize = m_cutPointsValidation.GetSize();
	FileValidation.write( (char*) &nSize, sizeof( int ) );

	for (unsigned i=0; i<nSize; i++)
		FileValidation.write( (char*) &m_cutPointsValidation.GetAt(i), sizeof( IwPoint3d ) );

	for (unsigned i=0; i<nSize; i++)
		FileValidation.write( (char*) &m_cutOrientationsValidation.GetAt(i), sizeof( IwVector3d ) );

	FileValidation.close();


	return true;
}


bool CFemoralCuts::Load( const QString& sDir )
{
	CPart::Load(sDir);
	//LoadInputs();

	//return true;

	CCurveArray			sCurves;

	bool				bOK;
	QString				sFileName;

	// Variables for TriathlonF1 version 0.0
	sFileName = sDir + "\\" + m_sFileName + "_fc.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );


	if( !bOK )
		return false;

	int nSize;
	// Variables for TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{

		File.read( (char*) &nSize, sizeof( int ) );

		IwPoint3d pnt;
		m_cutPoints.RemoveAll();
		for (int i=0; i<nSize; i++)
		{
			File.read( (char*) &pnt, sizeof( IwPoint3d ) );
			m_cutPoints.Add(pnt);
		}

		IwVector3d vec;
		m_cutOrientations.RemoveAll();
		for (int i=0; i<nSize; i++)
		{
			File.read( (char*) &vec, sizeof( IwVector3d ) );
			m_cutOrientations.Add(vec);
		}
	}

	File.close();

	m_bFCDataModified = false;

	////////////////////////////////////////////////////
	//// For validation
	sFileName = sDir + "\\" + m_sFileName + "_fc_validation.dat";

	QFile				FileValidation( sFileName );

	bOK = FileValidation.open( QIODevice::ReadOnly );


	if( !bOK )
		return false;

	FileValidation.read( (char*) &nSize, sizeof( int ) );

	IwPoint3d pnt;
	m_cutPointsValidation.RemoveAll();
	for (int i=0; i<nSize; i++)
	{
		FileValidation.read( (char*) &pnt, sizeof( IwPoint3d ) );
		m_cutPointsValidation.Add(pnt);
	}

	IwVector3d vec;
	m_cutOrientationsValidation.RemoveAll();
	for (int i=0; i<nSize; i++)
	{
		FileValidation.read( (char*) &vec, sizeof( IwVector3d ) );
		m_cutOrientationsValidation.Add(vec);
	}

	FileValidation.close();


	return true;
}

bool CFemoralCuts::LoadInputs(const QString& sDir)
{
	CPart::LoadIGES(sDir + "\\" + m_sFileName + ".igs");

	IwTArray<IwPoint3d> arrOfCutPoints, arrOfCutOrientations;
	IFemurImplant::FemoralCutsInp_GetCutsInfo(arrOfCutPoints, arrOfCutOrientations);

	m_cutPoints = arrOfCutPoints;
	m_cutOrientations = arrOfCutOrientations;
	m_cutPointsValidation = arrOfCutPoints;
	m_cutOrientationsValidation = arrOfCutOrientations;

	return true;
}

void CFemoralCuts::Display()
{
	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	glDisable( GL_LIGHTING );

	// Display OuterSideSurfaceIntersection curves
	if ( m_bOuterSideSurfaceIntersectionDisplay )
	{
		if( m_bOuterSideSurfaceIntersectionModified && m_glOuterSideSurfaceIntersectionCurveList > 0 )
			DeleteDisplayList( m_glOuterSideSurfaceIntersectionCurveList );

		if( m_glOuterSideSurfaceIntersectionCurveList == 0 )
		{
			m_glOuterSideSurfaceIntersectionCurveList = NewDisplayList();
			glNewList( m_glOuterSideSurfaceIntersectionCurveList, GL_COMPILE_AND_EXECUTE );
			DisplayFemoralCutsOuterSideSurfacesIntersection();
			glEndList();
			SetOuterSideSurfaceIntersectionModifiedFlag(false);
		}
		else
		{
			glCallList( m_glOuterSideSurfaceIntersectionCurveList );
		}
	}

	// Display CutEdges
	if ( m_bOuterSideSurfaceIntersectionDisplay )
	{
		if( m_bCutEdgesModified && m_glCutEdgesCurveList > 0 )
			DeleteDisplayList( m_glCutEdgesCurveList );

		if( m_glCutEdgesCurveList == 0 )
		{
			m_glCutEdgesCurveList = NewDisplayList();
			glNewList( m_glCutEdgesCurveList, GL_COMPILE_AND_EXECUTE );
			DisplayCutEdgeLines();
			glEndList();
			SetCutEdgesModifiedFlag(false);
		}
		else
		{
			glCallList( m_glCutEdgesCurveList );
		}
	}

	glEnable( GL_LIGHTING );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	//for(int idx = 0; idx < m_edgeFeaturePoints.GetSize(); idx++)
	//{
	//	IwPoint3d pnt = m_edgeFeaturePoints[idx];

	//	glPointSize(12);
	//	glColor3ub( 255, 0, 0 );

	//	glBegin( GL_POINTS );
	//	glVertex3d( pnt.x, pnt.y, pnt.z );
	//	glEnd();
	//}

	for(int idx = 0; idx < m_FeaturePoints.GetSize(); idx++)//m_AntOuterPoints
	{
		IwPoint3d pnt = m_FeaturePoints[idx];

		glPointSize(6);
		glColor3ub( 0, 255, 0 );

		glBegin( GL_POINTS );
		glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}

	for(int idx = 0; idx < m_AntOuterPoints.GetSize(); idx++)//m_AntOuterPoints
	{
		IwPoint3d pnt = m_AntOuterPoints[idx];

		glPointSize(6);
		glColor3ub( 255, 0, 0 );

		glBegin( GL_POINTS );
		glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}

	// We need to call CPart::Display(),
	CPart::Display();

}

void CFemoralCuts::DisplayFemoralCutsViewProfiles()
{			
	IwPoint3d	pt;

	IwPoint3d sPData[64];

	IwTArray<IwPoint3d> sPnts( 64, sPData );

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	//// DRAW View Profiles ///////////////////////////////////////////////////////
	// We do not need the blend (Transparency) for silhouette

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(1.0);

	for( unsigned i = 0; i < 2; i++ ) 
	{

		if (positiveSideIsLateral)
		{
			if (i==0) // lateral J curve
				glColor3ub( cyan[0], cyan[1], cyan[2]);
			else if (i==1) // medial J curve
				glColor3ub( magenta[0], magenta[1], magenta[2]);
		}
		else
		{
			if (i==0) // medial J curve
				glColor3ub( magenta[0], magenta[1], magenta[2]);
			else if (i==1) // lateral J curve
				glColor3ub( cyan[0], cyan[1], cyan[2]);
		}

		IwBSplineCurve *pCurve = m_offsetViewProfileCurves.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

}

void CFemoralCuts::DisplayFemoralCutsOuterSideSurfacesIntersection()
{

	IwPoint3d	pt;

	IwPoint3d sPData[64];

	IwTArray<IwPoint3d> sPnts( 64, sPData );

	//// DRAW View Profiles ///////////////////////////////////////////////////////
	// We do not need the blend (Transparency) for silhouette

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(1.0);

	//// Display outerSideIntersectionCurves
	glColor3ub( green[0], green[1], green[2]);
	for( unsigned i = 0; i < m_outerSideIntersectionCurves.GetSize(); i++ ) 
	{


		IwBSplineCurve *pCurve = (IwBSplineCurve*)m_outerSideIntersectionCurves.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	//// Display thickness curves
	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}
	for( unsigned i = 0; i < m_thicknessCurves.GetSize(); i++ ) 
	{
		if ( i < 2 )
		{
			if ( positiveSideIsLateral )
				glColor3ub( cyan[0], cyan[1], cyan[2]);
			else
				glColor3ub( magenta[0], magenta[1], magenta[2]);
		}
		else
		{
			if ( positiveSideIsLateral )
				glColor3ub( magenta[0], magenta[1], magenta[2]);
			else
				glColor3ub( cyan[0], cyan[1], cyan[2]);
		}

		IwBSplineCurve *pCurve = (IwBSplineCurve*)m_thicknessCurves.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	CColor		Acyan	= CColor( 0, 128, 128 );
	CColor		Amagenta	= CColor( 128, 0, 128 );
	for( unsigned i = 0; i < m_thicknessCurvesNotch.GetSize(); i++ ) 
	{
		if ( i < 1 )
		{
			if ( positiveSideIsLateral )
				glColor3ub( Acyan[0], Acyan[1], Acyan[2]);
			else
				glColor3ub( Amagenta[0], Amagenta[1], Amagenta[2]);
		}
		else
		{
			if ( positiveSideIsLateral )
				glColor3ub( Amagenta[0], Amagenta[1], Amagenta[2]);
			else
				glColor3ub( Acyan[0], Acyan[1], Acyan[2]);
		}

		IwBSplineCurve *pCurve = (IwBSplineCurve*)m_thicknessCurvesNotch.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

}

void CFemoralCuts::DisplayCutEdgeLines()
{

	IwPoint3d	pt;

	IwPoint3d sPData[64];

	IwTArray<IwPoint3d> sPnts( 64, sPData );

	//// DRAW View Profiles ///////////////////////////////////////////////////////
	// We do not need the blend (Transparency) for silhouette

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(1.0);

	glColor3ub( green[0], green[1], green[2]);
	for( unsigned i = 0; i < m_cutEdgeCurves.GetSize(); i++ ) 
	{


		IwBSplineCurve *pCurve = (IwBSplineCurve*)m_cutEdgeCurves.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

}

/////////////////////////////////////////////////////////////////////////
bool CFemoralCuts::IsMemberDataChanged
	(
	/*double& distalCutAngle,*/					// I:
	IwTArray<IwPoint3d>& cutPoints,			// I:
	IwTArray<IwVector3d>& cutOrientations	// I:
	)
{
	//CAdvancedControl* advCtrl = m_pDoc->GetAdvancedControl();
	//if ( !IS_EQ_TOL6( distalCutAngle, advCtrl->GetDistalCutAngle()) )
	//{
	//	return true;
	//}

	if ( cutPoints.GetSize() != m_cutPoints.GetSize() )
		return true;
	for (unsigned i=0; i<cutPoints.GetSize(); i++)
	{
		if ( cutPoints.GetAt(i) != m_cutPoints.GetAt(i) )
			return true;
	}

	if ( cutOrientations.GetSize() != m_cutOrientations.GetSize() )
		return true;
	for (unsigned i=0; i<cutOrientations.GetSize(); i++)
	{
		if ( cutOrientations.GetAt(i) != m_cutOrientations.GetAt(i) )
			return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////
bool CFemoralCuts::IsMemberDataChanged
	(
	/*double& distalCutAngle,*/					// I:
	IwTArray<IwPoint3d>& cutPoints,			// I:
	IwTArray<IwVector3d>& cutOrientations,	// I:
	/*double& distalCutAngle2,*/				// I:
	IwTArray<IwPoint3d>& cutPoints2,		// I:
	IwTArray<IwVector3d>& cutOrientations2	// I:
	)
{
	//if ( !IS_EQ_TOL6( distalCutAngle, distalCutAngle2 ) )
	//{
	//	return true;
	//}

	if ( cutPoints.GetSize() != cutPoints2.GetSize() )
		return true;
	for (unsigned i=0; i<cutPoints.GetSize(); i++)
	{
		if ( cutPoints.GetAt(i) != cutPoints2.GetAt(i) )
			return true;
	}

	if ( cutOrientations.GetSize() != cutOrientations2.GetSize() )
		return true;
	for (unsigned i=0; i<cutOrientations.GetSize(); i++)
	{
		if ( cutOrientations.GetAt(i) != cutOrientations2.GetAt(i) )
			return true;
	}

	return false;
}

void CFemoralCuts::GetCutsInfo
	(
	IwTArray<IwPoint3d>& cutPoints, 
	IwTArray<IwVector3d>& cutOrientations
	)
{
	//IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);

	cutPoints.RemoveAll();
	cutOrientations.RemoveAll();

	cutPoints.Append(m_cutPoints);
	cutOrientations.Append(m_cutOrientations);

	return;
}

/////////////////////////////////////////////////////////////////////////////
void CFemoralCuts::SetCutsInfo
	(
	IwTArray<IwPoint3d>& cutPoints,			// I:
	IwTArray<IwVector3d>& cutOrientations,	// I:
	IwBrep* Brep							// I: 
	)
{

	SetCutsInfoOnly(cutPoints, cutOrientations);

	// Update brep
	if (m_pBrep!=NULL) 
		IwObjDelete(m_pBrep);

	m_pBrep = Brep;

	// Need to update Tessellation
	MakeTess();

	return;
}

/////////////////////////////////////////////////////////////////////////////
void CFemoralCuts::SetCutsInfoOnly
	(
	IwTArray<IwPoint3d>& cutPoints,			// I:
	IwTArray<IwVector3d>& cutOrientations	// I:
	)
{
	m_cutPoints.RemoveAll();
	m_cutOrientations.RemoveAll();

	m_cutPoints.Append(cutPoints);
	m_cutOrientations.Append(cutOrientations);

	// Whenever set new info, the feature points are out of date
	m_isFeatPntsUpToDate = false;

	// Update the cut edges
	UpdateCutEdges();

	m_bFCDataModified = true;

	return;
}

/////////////////////////////////////////////////////////////////////////////
void CFemoralCuts::SetCutsInfoValidation
	(
	IwTArray<IwPoint3d>& cutPoints,			// I:
	IwTArray<IwVector3d>& cutOrientations	// I:
	)
{
	m_cutPointsValidation.RemoveAll();
	m_cutOrientationsValidation.RemoveAll();

	m_cutPointsValidation.Append(cutPoints);
	m_cutOrientationsValidation.Append(cutOrientations);

	m_bFCDataModified = true;

	return;
}

/////////////////////////////////////////////////////////////////////////////
void CFemoralCuts::GetCutsInfoValidation
	(
	IwTArray<IwPoint3d>& cutPoints,			// I:
	IwTArray<IwVector3d>& cutOrientations	// I:
	)
{
	cutPoints.RemoveAll();
	cutOrientations.RemoveAll();

	cutPoints.Append(m_cutPointsValidation);
	cutOrientations.Append(m_cutOrientationsValidation);

	return;
}

void CFemoralCuts::UpdateCutEdges()
{
	// delete the line objects
	for (unsigned i=0; i<m_cutEdgeCurves.GetSize(); i++)
	{
		IwBSplineCurve* line = (IwBSplineCurve*)m_cutEdgeCurves.GetAt(i);
		if (line) IwObjDelete(line);
	}
	m_cutEdgeCurves.RemoveAll();

	IwBSplineCurve* cutEdgeLine = NULL;
	// Handle the first 8 cuts (0~7th), but we only generate 6 cut edge lines
	double extraNotchGapDist = m_pDoc->GetVarTableValue("CASTING IMPLANT SIDE STACKING");
	for (unsigned i=0; i<4; i++) // These with extra notch gap dist to extend the inner surface coverage
	{
		DetermineCutEdge(i, i+2, extraNotchGapDist, cutEdgeLine);
		if (cutEdgeLine)
			m_cutEdgeCurves.Add((IwCurve*)cutEdgeLine);

	}
	for (unsigned i=4; i<6; i++) // without extra notch gap dist
	{
		DetermineCutEdge(i, i+2, 0.0, cutEdgeLine);
		if (cutEdgeLine)
			m_cutEdgeCurves.Add((IwCurve*)cutEdgeLine);

	}
	int prevIndex = 6;
	int	postIndex = 8;
	DetermineCutEdge(prevIndex, postIndex, 0.0, cutEdgeLine);
	if (cutEdgeLine)
		m_cutEdgeCurves.Add((IwCurve*)cutEdgeLine);

	prevIndex = 7;
	postIndex = 8;
	DetermineCutEdge(prevIndex, postIndex, 0.0, cutEdgeLine);
	if (cutEdgeLine)
		m_cutEdgeCurves.Add((IwCurve*)cutEdgeLine);

	prevIndex = 8;
	postIndex = 9;
	DetermineCutEdge(prevIndex, postIndex, 0.0, cutEdgeLine);
	if (cutEdgeLine)
		m_cutEdgeCurves.Add((IwCurve*)cutEdgeLine);

	m_bCutEdgesModified = true; // force to update cut edge display

}

////////////////////////////////////////////////////////////////////////////////
// This function determines the cut edge between the prevCutIndex & nextCutIndex
////////////////////////////////////////////////////////////////////////////////
void CFemoralCuts::DetermineCutEdge
	(
	int prevCutIndex,				// I:
	int nextCutIndex,				// I:
	double extraNotchGapDist,		// I:
	IwBSplineCurve*& cutEdgeCurve	// O:
	)
{
	cutEdgeCurve = NULL;
	IwContext& iwContext = m_pDoc->GetIwContext();

	CFemoralAxes*		femoralAxes = GetTotalDoc()->GetFemoralAxes();
	if (femoralAxes==NULL) return;
	double femSize = femoralAxes->GetEpiCondyleSize();

	IwVector3d prevOrientation = m_cutOrientations.GetAt(prevCutIndex);
	IwVector3d nextOrientation = m_cutOrientations.GetAt(nextCutIndex);
	IwVector3d xAxisVector = prevOrientation*nextOrientation;
	xAxisVector.Unitize();

	IwPoint3d prevCutPnt, nextCutPnt, intersectPnt, offsetPnt;
	IwVector3d prevTangentCut, nextTangentCut;
	// get notch cut location
	IwPoint3d projectedPlaneOrigin = m_cutPoints.GetLast();
	// offset away from the notch toward the lateral side to leave a little bit gap 
	CAdvancedControl* advCtrl = GetTotalDoc()->GetAdvancedControl();
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		// Lateral is same direction with x axis
		//projectedPlaneOrigin = projectedPlaneOrigin + (extraNotchGapDist+advCtrl->GetInnerSurfaceNotchGapDistance())*xAxisVector;
	}
	else
	{
		// Lateral side is the opposite direction with x axis
		//projectedPlaneOrigin = projectedPlaneOrigin - (extraNotchGapDist+advCtrl->GetInnerSurfaceNotchGapDistance())*xAxisVector;
	}

	IwVector3d projectedPlaneNormal = xAxisVector;
	double param;
	IwSolutionArray sols;
	IwSolution sol;
	IwBoolean needMoreSols = FALSE;
	IwLine *prevCutLine = NULL, *nextCutLine = NULL;
	IwBSplineCurve* cutEdgeLine = NULL;

	prevCutPnt = m_cutPoints.GetAt(prevCutIndex);
	prevCutPnt = prevCutPnt.ProjectPointToPlane(projectedPlaneOrigin, projectedPlaneNormal);
	nextCutPnt = m_cutPoints.GetAt(nextCutIndex);
	nextCutPnt = nextCutPnt.ProjectPointToPlane(projectedPlaneOrigin, projectedPlaneNormal);
	prevTangentCut = xAxisVector*prevOrientation;
	prevTangentCut = prevTangentCut.ProjectToPlane(projectedPlaneNormal);
	prevTangentCut.Unitize();
	nextTangentCut = xAxisVector*nextOrientation;
	nextTangentCut = nextTangentCut.ProjectToPlane(projectedPlaneNormal);
	nextTangentCut.Unitize();

	prevCutLine = NULL;
	nextCutLine = NULL;
	IwLine::CreateCanonical(iwContext, prevCutPnt, prevTangentCut, prevCutLine);
	IwLine::CreateCanonical(iwContext, nextCutPnt, nextTangentCut, nextCutLine);
	//Get the intersection point between adjacent 2 cuts
	prevCutLine->IntersectWithLine(prevCutLine->GetNaturalInterval(), *nextCutLine, nextCutLine->GetNaturalInterval(), 0.001, needMoreSols, sols);
	if (sols.GetSize() == 0)
	{
		if (prevCutLine) IwObjDelete(prevCutLine);
		if (nextCutLine) IwObjDelete(nextCutLine);	
		return;
	}

	sol = sols.GetAt(0);
	param = sol.m_vStart.m_adParameters[0];
	prevCutLine->EvaluatePoint(param, intersectPnt);
	if (abs(prevCutIndex-nextCutIndex) == 1 && prevCutIndex > 7) // anterior chamfer and anterior cuts (8th & 9th)
	{
		offsetPnt = intersectPnt + 0.85*femSize*xAxisVector;
		intersectPnt = intersectPnt - 0.85*femSize*xAxisVector;
	}
	else if (prevCutIndex%2==0) // in positive side
	{
		offsetPnt = intersectPnt + 0.85*femSize*xAxisVector;
	}
	else // negative side 
	{
		offsetPnt = intersectPnt - 0.85*femSize*xAxisVector;
	}

	// Create the cut edge
	cutEdgeLine = NULL;
	IwBSplineCurve::CreateLineSegment(iwContext, 3, intersectPnt, offsetPnt, cutEdgeLine);
	//cutEdgeLine->ReparametrizeWithArcLength();
	if (cutEdgeLine)
		cutEdgeCurve = cutEdgeLine;

	if (prevCutLine) IwObjDelete(prevCutLine);
	if (nextCutLine) IwObjDelete(nextCutLine);

}

/////////////////////////////////////////////////////////////////
void CFemoralCuts::DetermineCutPlaneEdges
	(
	IwTArray<IwCurve*>& cutPlaneEdges // O:
	)
{
	CFemoralAxes*		femoralAxes = GetTotalDoc()->GetFemoralAxes();
	if (femoralAxes==NULL) return;

	UpdateCutEdges();

	IwContext& iwContext = m_pDoc->GetIwContext();
	IwTArray<IwCurve*> myCutPlaneEdges;
	// make another copy of cut edges.
	for (unsigned i=0; i<m_cutEdgeCurves.GetSize(); i++)
	{
		IwCurve *cutEdge = m_cutEdgeCurves.GetAt(i);
		IwCurve *myCutEdge = NULL;
		cutEdge->Copy(iwContext, myCutEdge);
		cutPlaneEdges.Add(myCutEdge);
	}

	// offset the first 2 (0th & 1st) to be the new cut plane edges
	IwAxis2Placement femAxes;
	femoralAxes->GetSweepAxes(femAxes);
	double femSize = femoralAxes->GetEpiCondyleSize();

	// first cut edge
	IwCurve *firstCutEdge = NULL;
	m_cutEdgeCurves.GetAt(0)->Copy(iwContext, firstCutEdge);
	IwVector3d firstOffsetVec = femAxes.GetXAxis()*m_cutOrientations.GetAt(0);
	IwAxis2Placement firstTrans = IwAxis2Placement(0.35*femSize*firstOffsetVec, IwVector3d(1,0,0), IwVector3d(0,1,0));
	firstCutEdge->Transform(firstTrans);
	// second cut edge
	IwCurve *secondCutEdge = NULL;
	m_cutEdgeCurves.GetAt(1)->Copy(iwContext, secondCutEdge);
	IwVector3d secondOffsetVec = femAxes.GetXAxis()*m_cutOrientations.GetAt(1);
	IwAxis2Placement secondTrans = IwAxis2Placement(0.35*femSize*secondOffsetVec, IwVector3d(1,0,0), IwVector3d(0,1,0));
	secondCutEdge->Transform(secondTrans);
	// last cut edge
	IwCurve *lastCutEdge = NULL;
	m_cutEdgeCurves.GetLast()->Copy(iwContext, lastCutEdge);
	IwVector3d lastOffsetVec = femAxes.GetXAxis()*m_cutOrientations.GetAt(m_cutOrientations.GetSize()-2);// "-2" is the last cut on anterior
	IwAxis2Placement lastTrans = IwAxis2Placement(-0.75*femSize*lastOffsetVec, IwVector3d(1,0,0), IwVector3d(0,1,0));
	lastCutEdge->Transform(lastTrans);


	// add to cutPlaneEdges
	cutPlaneEdges.InsertAt(0, firstCutEdge);
	cutPlaneEdges.InsertAt(1, secondCutEdge);
	cutPlaneEdges.Add(lastCutEdge);

	if ( GetTotalDoc()->GetOutlineProfile()==NULL )
		return;

	// We need to trim to prevent too wide
	IwPoint3d posPoint, negPoint;
	double MLSize = GetTotalDoc()->GetOutlineProfile()->GetImplantMLSize(&posPoint, &negPoint);
	if ( MLSize < 0 ) return; // something wrong, or OutlineProfile is not ready yet.
	posPoint = posPoint + 10*femAxes.GetXAxis();
	negPoint = negPoint - 10*femAxes.GetXAxis();


	IwPoint3d pnt;
	IwPoint3d intPoint, sPnt, ePnt;
	IwVector3d tempVec, tempVec2;
	double param;
	IwExtent1d interval;
	for (unsigned i=0; i<cutPlaneEdges.GetSize(); i++)
	{
		IwCurve* edge = cutPlaneEdges.GetAt(i);
		// intersect with posPoint
		if ( IntersectCurveByPlane(edge, posPoint, femAxes.GetXAxis(), pnt, &param) )
		{
			interval = edge->GetNaturalInterval();
			edge->GetEnds(sPnt, ePnt);
			intPoint = pnt;
			tempVec = sPnt - intPoint;
			if ( tempVec.Dot(femAxes.GetXAxis()) > 0 ) // outside of positive x-axis
			{
				edge->Trim(IwExtent1d(param, interval.GetMax()));
			}
			else
			{
				tempVec = ePnt - intPoint;
				if ( tempVec.Dot(femAxes.GetXAxis()) > 0 ) // outside of positive x-axis
				{
					edge->Trim(IwExtent1d(interval.GetMin(), param));
				}
			}
		}
		if ( IntersectCurveByPlane(edge, negPoint, femAxes.GetXAxis(), pnt, &param) )
		{
			interval = edge->GetNaturalInterval();
			edge->GetEnds(sPnt, ePnt);
			intPoint = pnt;
			tempVec = sPnt - intPoint;
			if ( tempVec.Dot(-femAxes.GetXAxis()) > 0 ) // outside of negative x-axis
			{
				edge->Trim(IwExtent1d(param, interval.GetMax()));
			}
			else
			{
				tempVec = ePnt - intPoint;
				if ( tempVec.Dot(-femAxes.GetXAxis()) > 0 ) // outside of negative x-axis
				{
					edge->Trim(IwExtent1d(interval.GetMin(), param));
				}
			}
		}
	}

}

///////////////////////////////////////////////////////////////
// The outputs from this function are to help construct the 
// outline profile.
///////////////////////////////////////////////////////////////
void CFemoralCuts::GetFeaturePoints
	(
	IwTArray<IwPoint3d>& featurePoints,	// O: total 21 points
	IwTArray<IwPoint3d>* antOuterPoints	// O: optional outputs, total 9 points
	)
{
	if(1)
	{
		featurePoints.RemoveAll();

		if ( !m_isFeatPntsUpToDate )
			DetermineFeaturePoints(); 

		if ( m_isFeatPntsUpToDate )
		{
			featurePoints.Append(m_FeaturePoints);
			if ( antOuterPoints )
			{
				antOuterPoints->RemoveAll();
				antOuterPoints->Append(m_AntOuterPoints);
			}
		}
	}
	else
	{
		featurePoints.RemoveAll();
		featurePoints = IFemurImplant::FemoralCutsInp_GetCutsFeaturePoints();

		m_FeaturePoints.RemoveAll();
		m_FeaturePoints.Append(featurePoints);

		DetermineAntFeaturePoints();

		if ( antOuterPoints )
		{
			antOuterPoints->RemoveAll();
			antOuterPoints->Append(m_AntOuterPoints);
		}
	}

	return;
}

///////////////////////////////////////////////////////////////
void CFemoralCuts::GetFeaturePointsAtVertexes
	(
	IwTArray<IwPoint3d>& featurePoints
	)
{
	featurePoints.RemoveAll();

	// Determine the feature points at vertexes
	bool withDefaultDistToEdge = false;
	DetermineFeaturePoints(withDefaultDistToEdge); 
	featurePoints.Append(m_FeaturePoints);

	// Set back the feature points to default location
	withDefaultDistToEdge = true;
	DetermineFeaturePoints(withDefaultDistToEdge); 

}

////////////////////////////////////////////////////////////////////////////////////
// This function determines the cut feature points m_FeaturePoints, which includes 
// (by the order) 
// the positive posterior tip point, negative post tip point,
// 2 edge points on positive side and 2 edge points on negative side on
// the edge between post cut and 2nd post chamfer cut.
// 2 edge points on positive side and 2 edge points on negative side on
// the edge between 2nd post chamfer cut and 1st post chamfer cut.
// 2 edge points on positive side and 2 edge points on negative side on
// the edge between 1st post chamfer cut and distal cut.
// 2 edge points on positive side and 2 edge points on negative side on
// the edge between distal cut and anterior chamfer cut.
// 2 edge points on the edge between anterior chamfer cut and anterior cut.
// Total 21 points.
///////////////////////////////////////////////////////////////////////////////////
bool CFemoralCuts::DetermineFeaturePoints
	(
	bool withDefaultDistToEdge	// Use distanceToCutEdge from configuration file or not
	)
{
	// Empty m_FeaturePoints
	m_FeaturePoints.RemoveAll();

	double distanceToCutEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");
	if ( !withDefaultDistToEdge )
		distanceToCutEdge = 0;

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Get femoral Axes
	IwVector3d xAxis = IwVector3d(1,0,0);
	if ( GetTotalDoc()->GetFemoralAxes() )
	{
		IwAxis2Placement sweepAxes;
		GetTotalDoc()->GetFemoralAxes()->GetSweepAxes(sweepAxes);
		xAxis = sweepAxes.GetXAxis();
	}

	double posDistanceToCutEdge2, negDistanceToCutEdge2;
	int posIndex = 0, posIndex1, posIndex2;
	IwFace* cutFace = NULL;
	IwTArray<IwFace*> cutFaces;
	IwTArray<IwEdge*> commonCutEdges;
	IwPoint3d extremePnt;
	bool gotExPnt;
	bool gotEdge;
	bool gotFace;
	IwCurve* curve;
	IwExtent1d edgeDomain;
	double length, ratio;
	IwPoint3d edgePntLeft, edgePntRight, tempPnt;

	//////// Search the extreme point for positive side posterior cut
	posIndex = 0;
	cutFace = NULL;
	cutFaces.RemoveAll();
	gotFace = false;
	gotExPnt = false;
	
	/*try
	{*/
		gotFace = SearchCutFaces(posIndex, cutFaces);
		if (gotFace)
		{
			if (cutFaces.GetSize() > 1) // If posterior cut has more than 1 face, remove the faces without linear edges. They are isolated faces.
			{
				commonCutEdges.RemoveAll();
				int i0=0, i2=2;
				gotEdge = SearchCommonCutEdges(i0, i2, commonCutEdges);
				if (gotEdge)
				{
					IwEdge* connectedEdge = commonCutEdges.GetAt(0);
					IwTArray<IwEdge*> edges;
					bool isConnected = false;
					for (int i=cutFaces.GetSize()-1; i>-1; i--)
					{
						isConnected = false;
						cutFaces.GetAt(i)->GetEdges(edges);
						for (unsigned j=0; j<edges.GetSize(); j++)
						{
							if (edges.GetAt(j) == connectedEdge)
							{
								isConnected = true;
								break;
							}
						}
						if ( isConnected == false )
							cutFaces.RemoveAt(i); // remove backward
					}
				}
			}
			cutFace = cutFaces.GetAt(0); // 0th face is the largest face
			gotExPnt = CalculateMostPosteriorPointOnPosteriorFaceEdges(cutFace, extremePnt, &distanceToCutEdge);
			if ( !gotExPnt ) 
			{
				m_FeaturePoints.RemoveAll();
				//return false;
			}
			m_FeaturePoints.Add(extremePnt);
		}
		//////// Search the extreme point for negative side posterior cut
		posIndex = 1;
		cutFace = NULL;
		cutFaces.RemoveAll();
		gotFace = false;
		gotExPnt = false;
		gotFace = SearchCutFaces(posIndex, cutFaces);
		if (gotFace)
		{
			if (cutFaces.GetSize() > 1) // If posterior cut has more than 1 face, remove the faces without linear edges. They are isolated faces.
			{
				commonCutEdges.RemoveAll();
				int i1=1, i3=3;
				gotEdge = SearchCommonCutEdges(i1, i3, commonCutEdges);
				if (gotEdge)
				{
					IwEdge* connectedEdge = commonCutEdges.GetAt(0);
					IwTArray<IwEdge*> edges;
					bool isConnected = false;
					for (int i=cutFaces.GetSize()-1; i>-1; i--)
					{
						isConnected = false;
						cutFaces.GetAt(i)->GetEdges(edges);
						for (unsigned j=0; j<edges.GetSize(); j++)
						{
							if (edges.GetAt(j) == connectedEdge)
							{
								isConnected = true;
								break;
							}
						}
						if ( isConnected == false )
							cutFaces.RemoveAt(i); // remove backward
					}
				}
			}
			cutFace = cutFaces.GetAt(0); // 0th face is the largest face
			gotExPnt = CalculateMostPosteriorPointOnPosteriorFaceEdges(cutFace,  extremePnt, &distanceToCutEdge);
			if ( !gotExPnt )
			{
				m_FeaturePoints.RemoveAll();
				//return false;
			}
			m_FeaturePoints.Add(extremePnt);
		}

		//IwTArray<IwPoint3d> arrOfImplantFeaturePoint = IFemurImplant::ImplantInp_GetCutsFeaturePointInfo();

		//////// Search the edge feature points for positive side 2nd post chamfer cut 
		posIndex1=0;
		posIndex2=2;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges);

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[2];
		//	endPnt2 = arrOfImplantFeaturePoint[3];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}

		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex2, posIndex1, curve);
		//}

		if (gotEdge)
		{
			//curve = commonCutEdges.GetAt(0)->GetCurve();
			edgeDomain = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve->Length(edgeDomain, 0.1, length);
			ratio = edgeDomain.GetLength()/length;
			curve->EvaluatePoint(edgeDomain.GetMin()+ratio*distanceToCutEdge, edgePntLeft);
			curve->EvaluatePoint(edgeDomain.GetMax()-ratio*distanceToCutEdge, edgePntRight);
			// Left point should have a bigger x
			if ( (edgePntLeft-edgePntRight).Dot(xAxis) < 0)
			{
				tempPnt = edgePntLeft;
				edgePntLeft = edgePntRight;
				edgePntRight = tempPnt;
			}
			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);
		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}
		//////// Search the edge feature points for negative side 2nd post chamfer cut 
		posIndex1=1;
		posIndex2=3;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[4];
		//	endPnt2 = arrOfImplantFeaturePoint[5];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}
		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex2, posIndex1, curve);
		//}

		if (gotEdge)
		{
			IwCurve* curve0 = curve;//commonCutEdges.GetAt(0)->GetCurve();
			IwExtent1d edgeDomain0 = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve0->Length(edgeDomain0, 0.1, length);
			ratio = edgeDomain0.GetLength()/length;
			IwPoint3d edgePntLeft0, edgePntLeft1;
			curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
			curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
			// edgePntLeft should have a bigger x
			if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
				edgePntLeft = edgePntLeft0;
			else
				edgePntLeft = edgePntLeft1;

			IwCurve* curve1 = curve;//commonCutEdges.GetLast()->GetCurve();
			IwExtent1d edgeDomain1 = curve->GetNaturalInterval();//commonCutEdges.GetLast()->GetInterval();
			curve1->Length(edgeDomain1, 0.1, length);
			ratio = edgeDomain1.GetLength()/length;
			IwPoint3d edgePntRight0, edgePntRight1;
			curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
			curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
			// edgePntRight should have a smaller x
			if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
				edgePntRight = edgePntRight0;
			else
				edgePntRight = edgePntRight1;

			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);
		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}

		if(0)
		{
			//////// Search the edge feature points for positive side 1st post chamfer cut 
			posIndex1=2;
			posIndex2=4;
			commonCutEdges.RemoveAll();
			gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);
			if (gotEdge)
			{
				IwCurve* curve0 = commonCutEdges.GetAt(0)->GetCurve();
				IwExtent1d edgeDomain0 = commonCutEdges.GetAt(0)->GetInterval();
				curve0->Length(edgeDomain0, 0.1, length);
				ratio = edgeDomain0.GetLength()/length;
				IwPoint3d edgePntLeft0, edgePntLeft1;
				curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
				curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
				// edgePntLeft should have a bigger x
				if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
					edgePntLeft = edgePntLeft0;
				else
					edgePntLeft = edgePntLeft1;

				IwCurve* curve1 = commonCutEdges.GetLast()->GetCurve();
				IwExtent1d edgeDomain1 = commonCutEdges.GetLast()->GetInterval();
				curve1->Length(edgeDomain1, 0.1, length);
				ratio = edgeDomain1.GetLength()/length;
				IwPoint3d edgePntRight0, edgePntRight1;
				curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
				curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
				// edgePntRight should have a smaller x
				if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
					edgePntRight = edgePntRight0;
				else
					edgePntRight = edgePntRight1;

				m_FeaturePoints.Add(edgePntLeft);
				m_FeaturePoints.Add(edgePntRight);
			}
			else
			{
				m_FeaturePoints.RemoveAll();
				return false;
			}
			//////// Search the edge feature points for negative side 1st post chamfer cut 
			posIndex1=3;
			posIndex2=5;
			commonCutEdges.RemoveAll();
			gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);
			if (gotEdge)
			{
				IwCurve* curve0 = commonCutEdges.GetAt(0)->GetCurve();
				IwExtent1d edgeDomain0 = commonCutEdges.GetAt(0)->GetInterval();
				curve0->Length(edgeDomain0, 0.1, length);
				ratio = edgeDomain0.GetLength()/length;
				IwPoint3d edgePntLeft0, edgePntLeft1;
				curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
				curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
				// edgePntLeft should have a bigger x
				if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
					edgePntLeft = edgePntLeft0;
				else
					edgePntLeft = edgePntLeft1;

				IwCurve* curve1 = commonCutEdges.GetLast()->GetCurve();
				IwExtent1d edgeDomain1 = commonCutEdges.GetLast()->GetInterval();
				curve1->Length(edgeDomain1, 0.1, length);
				ratio = edgeDomain1.GetLength()/length;
				IwPoint3d edgePntRight0, edgePntRight1;
				curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
				curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
				// edgePntRight should have a smaller x
				if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
					edgePntRight = edgePntRight0;
				else
					edgePntRight = edgePntRight1;

				m_FeaturePoints.Add(edgePntLeft);
				m_FeaturePoints.Add(edgePntRight);
			}
			else
			{
				m_FeaturePoints.RemoveAll();
				return false;
			}
		}

		//////// Search the edge feature points for positive side distal cut 
		posIndex1=4;
		posIndex2=6;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[6];
		//	endPnt2 = arrOfImplantFeaturePoint[7];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}
		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex2, posIndex1, curve);
		//}

		if (gotEdge)
		{
			IwCurve* curve0 = curve;//commonCutEdges.GetAt(0)->GetCurve();
			IwExtent1d edgeDomain0 = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve0->Length(edgeDomain0, 0.1, length);
			ratio = edgeDomain0.GetLength()/length;
			IwPoint3d edgePntLeft0, edgePntLeft1;
			curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
			curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
			// edgePntLeft should have a bigger x
			if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
				edgePntLeft = edgePntLeft0;
			else
				edgePntLeft = edgePntLeft1;

			IwCurve* curve1 = curve;//commonCutEdges.GetLast()->GetCurve();
			IwExtent1d edgeDomain1 = curve->GetNaturalInterval();//commonCutEdges.GetLast()->GetInterval();
			curve1->Length(edgeDomain1, 0.1, length);
			ratio = edgeDomain1.GetLength()/length;
			IwPoint3d edgePntRight0, edgePntRight1;
			curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
			curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
			// edgePntRight should have a smaller x
			if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
				edgePntRight = edgePntRight0;
			else
				edgePntRight = edgePntRight1;

			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);
		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}
		//////// Search the edge feature points for negative side distal cut 
		posIndex1=5;
		posIndex2=7;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[8];
		//	endPnt2 = arrOfImplantFeaturePoint[9];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}
		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex2, posIndex1, curve);
		//}

		if (gotEdge)
		{
			IwCurve* curve0 = curve;//commonCutEdges.GetAt(0)->GetCurve();
			IwExtent1d edgeDomain0 = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve0->Length(edgeDomain0, 0.1, length);
			ratio = edgeDomain0.GetLength()/length;
			IwPoint3d edgePntLeft0, edgePntLeft1;
			curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
			curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
			// edgePntLeft should have a bigger x
			if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
				edgePntLeft = edgePntLeft0;
			else
				edgePntLeft = edgePntLeft1;

			IwCurve* curve1 = curve;//commonCutEdges.GetLast()->GetCurve();
			IwExtent1d edgeDomain1 = curve->GetNaturalInterval();//commonCutEdges.GetLast()->GetInterval();
			curve1->Length(edgeDomain1, 0.1, length);
			ratio = edgeDomain1.GetLength()/length;
			IwPoint3d edgePntRight0, edgePntRight1;
			curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
			curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
			// edgePntRight should have a smaller x
			if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
				edgePntRight = edgePntRight0;
			else
				edgePntRight = edgePntRight1;

			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);
		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}
		//////// Search the edge feature points for positive side ant chamfer cut 
		posIndex1=6;
		posIndex2=8;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);// sort multiple edges by positive x position

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[10];
		//	endPnt2 = arrOfImplantFeaturePoint[11];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}
		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex1, posIndex2, curve);
		//}

		if (gotEdge)
		{
			IwCurve* curve0 = curve;//commonCutEdges.GetAt(0)->GetCurve();
			IwExtent1d edgeDomain0 = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve0->Length(edgeDomain0, 0.1, length);
			ratio = edgeDomain0.GetLength()/length;
			IwPoint3d edgePntLeft0, edgePntLeft1;
			curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
			curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
			// edgePntLeft should have a bigger x
			if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
				edgePntLeft = edgePntLeft0;
			else
				edgePntLeft = edgePntLeft1;

			IwCurve* curve1 = curve;//commonCutEdges.GetLast()->GetCurve();
			IwExtent1d edgeDomain1 = curve->GetNaturalInterval();//commonCutEdges.GetLast()->GetInterval();
			curve1->Length(edgeDomain1, 0.1, length);
			ratio = edgeDomain1.GetLength()/length;
			IwPoint3d edgePntRight0, edgePntRight1;
			curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
			curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
			// edgePntRight should have a smaller x
			if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
				edgePntRight = edgePntRight0;
			else
				edgePntRight = edgePntRight1;

			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);
		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}
		//////// Search the edge feature points for negative side ant chamfer cut 
		posIndex1=7;
		posIndex2=8;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1);// sort multiple edges by positive x position

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[12];
		//	endPnt2 = arrOfImplantFeaturePoint[13];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}
		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex1, posIndex2, curve);
		//}

		if (gotEdge)
		{
			IwCurve* curve0 = curve;//commonCutEdges.GetAt(0)->GetCurve();
			IwExtent1d edgeDomain0 = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve0->Length(edgeDomain0, 0.1, length);
			ratio = edgeDomain0.GetLength()/length;
			IwPoint3d edgePntLeft0, edgePntLeft1;
			curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
			curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
			// edgePntLeft should have a bigger x
			if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
				edgePntLeft = edgePntLeft0;
			else
				edgePntLeft = edgePntLeft1;

			IwCurve* curve1 = curve;//commonCutEdges.GetLast()->GetCurve();
			IwExtent1d edgeDomain1 = curve->GetNaturalInterval();//commonCutEdges.GetLast()->GetInterval();
			curve1->Length(edgeDomain1, 0.1, length);
			ratio = edgeDomain1.GetLength()/length;
			IwPoint3d edgePntRight0, edgePntRight1;
			curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
			curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
			// edgePntRight should have a smaller x
			if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
				edgePntRight = edgePntRight0;
			else
				edgePntRight = edgePntRight1;

			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);
		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}
		//////// Search the edge feature points between ant chamfer and anterior cuts 
		posIndex1=8;
		posIndex2=9;
		commonCutEdges.RemoveAll();
		gotEdge = SearchCommonCutEdges(posIndex1, posIndex2, commonCutEdges, 1); // sort multiple edges by positive x position

		if (gotEdge) curve = commonCutEdges.GetAt(0)->GetCurve();

		//if(!gotEdge)
		//{
		//	IwPoint3d endPnt1, endPnt2;
		//	endPnt1 = arrOfImplantFeaturePoint[14];
		//	endPnt2 = arrOfImplantFeaturePoint[15];

		//	IwBSplineCurve * pBSC = NULL;
		//	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, endPnt1, endPnt2, pBSC);
		//	curve = pBSC;
		//	gotEdge = true;

		//	if(1)
		//	{
		//		ShowCurve(m_pDoc, curve, green);
		//	}
		//	//gotEdge = ApproxCommonEdgeCurveForTwoNonInterFaces(posIndex2, posIndex1, curve);
		//}

		if (gotEdge)
		{
			IwCurve* curve0 = curve;//commonCutEdges.GetAt(0)->GetCurve();
			IwExtent1d edgeDomain0 = curve->GetNaturalInterval();//commonCutEdges.GetAt(0)->GetInterval();
			curve0->Length(edgeDomain0, 0.1, length);
			ratio = edgeDomain0.GetLength()/length;
			IwPoint3d edgePntLeft0, edgePntLeft1;
			curve0->EvaluatePoint(edgeDomain0.GetMin()+ratio*distanceToCutEdge, edgePntLeft0);
			curve0->EvaluatePoint(edgeDomain0.GetMax()-ratio*distanceToCutEdge, edgePntLeft1);
			// edgePntLeft should have a bigger x
			if ( (edgePntLeft0-edgePntLeft1).Dot(xAxis) > 0)
				edgePntLeft = edgePntLeft0;
			else
				edgePntLeft = edgePntLeft1;

			IwCurve* curve1 = curve;//commonCutEdges.GetLast()->GetCurve();
			IwExtent1d edgeDomain1 = curve->GetNaturalInterval();//commonCutEdges.GetLast()->GetInterval();
			curve1->Length(edgeDomain1, 0.1, length);
			ratio = edgeDomain1.GetLength()/length;
			IwPoint3d edgePntRight0, edgePntRight1;
			curve1->EvaluatePoint(edgeDomain1.GetMin()+ratio*distanceToCutEdge, edgePntRight0);
			curve1->EvaluatePoint(edgeDomain1.GetMax()-ratio*distanceToCutEdge, edgePntRight1);
			// edgePntRight should have a smaller x
			if ( (edgePntRight0-edgePntRight1).Dot(xAxis) < 0)
				edgePntRight = edgePntRight0;
			else
				edgePntRight = edgePntRight1;

			m_FeaturePoints.Add(edgePntLeft);
			m_FeaturePoints.Add(edgePntRight);

		}
		else
		{
			m_FeaturePoints.RemoveAll();
			//return false;
		}

	/*}
	catch(exception& e)
	{
		m_FeaturePoints.RemoveAll();
	}*/

	if(m_FeaturePoints.GetSize() == 0 || m_FeaturePoints.GetSize() != 16)
	{
		m_FeaturePoints.RemoveAll();
		m_FeaturePoints = IFemurImplant::ImplantInp_GetCutsFeaturePointInfo();
	}

	IwTArray<IwPoint3d> avgOf2345and6789Points;
	avgOf2345and6789Points.Add((m_FeaturePoints[2] + m_FeaturePoints[6])/2);
	avgOf2345and6789Points.Add((m_FeaturePoints[3] + m_FeaturePoints[7])/2);
	avgOf2345and6789Points.Add((m_FeaturePoints[4] + m_FeaturePoints[8])/2);
	avgOf2345and6789Points.Add((m_FeaturePoints[5] + m_FeaturePoints[9])/2);

	m_FeaturePoints.InsertAt(6, &avgOf2345and6789Points);

	m_isFeatPntsUpToDate = true;

	////////////////////////////////////////////////////////////////////
	// Determine the anterior feature points. They are optional outputs.
	////////////////////////////////////////////////////////////////////

	// Get anterior cut face
	//IwTArray<IwFace*> antCutFaces;
	//int antIndex = 9;
	//SearchCutFaces(antIndex, antCutFaces);
	//IwFace* antCutFace = antCutFaces.GetAt(0);
	//IwSurface* antSurf = antCutFace->GetSurface();
	//IwPoint3d antPnt, antNormal;
	//antSurf->IsPlanar(0.001, &antPnt, &antNormal);

	IwPoint3d antPnt, antNormal;
	antPnt = m_cutPoints[9];
	antNormal = m_cutOrientations[9];

	IwAxis2Placement wslAxes;
	GetTotalDoc()->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);

	// Get anterior feature points
	IwTArray<IwPoint3d> antFeaturePoints;
	if ( GetTotalDoc()->GetOutlineProfile() )
		GetTotalDoc()->GetOutlineProfile()->GetAnteriorEarFeaturePoints(antFeaturePoints);
	if ( antFeaturePoints.GetSize() == 0 )
		COutlineProfile::GetInitialAnteriorEarFeaturePoints(GetTotalDoc(), antFeaturePoints);
	if ( antFeaturePoints.GetSize() == 0 ) return true;// Return true since these are optional outputs. 

	// Get femur (main area) surface 
	IwBSplineSurface* femurSurface = GetTotalDoc()->GetFemur()->GetSinglePatchSurface();

	IwPoint3d antTipPoint = antFeaturePoints.GetAt(0);
	IwPoint3d posAntEarPoint = antFeaturePoints.GetAt(1);
	IwPoint3d negAntEarPoint = antFeaturePoints.GetAt(2);
	IwPoint3d posAntAntChamferCutEdgePoint = m_FeaturePoints.GetAt(18);
	IwPoint3d negAntAntChamferCutEdgePoint = m_FeaturePoints.GetAt(19);
	// We need to create 2nd and 3rd points on the anterior cut plane lateral side
	//double posDistanceToCutEdge2, negDistanceToCutEdge2;

	double posP2, posP25, negP2, negP25;
	if (positiveSideIsLateral)
	{
		posP2 = 0.2;
		posP25 = 0.55;
		posDistanceToCutEdge2 = distanceToCutEdge;
		negP2 = 0.2;
		negP25 = 0.50;
		negDistanceToCutEdge2 = distanceToCutEdge;// + 0.5;
	}
	else
	{
		posP2 = 0.2;
		posP25 = 0.50;
		posDistanceToCutEdge2 = distanceToCutEdge;// + 0.5;
		negP2 = 0.2;
		negP25 = 0.55;
		negDistanceToCutEdge2 = distanceToCutEdge;
	}
	// antCutEdge2 is on the lower middle of posAntEarPoint and posAntAntChamferCutEdge
	IwPoint3d antCutEdge2 = posP2*posAntEarPoint+(1.0-posP2)*posAntAntChamferCutEdgePoint;
	IwPoint3d intPnt;
	IntersectSurfaceByLine(femurSurface, antCutEdge2 + 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d posAntCutEdgePoint2 = intPnt - posDistanceToCutEdge2*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether posAntCutEdgePoint2 is in the correct location
	IwVector3d CutEdgeToEarVec = posAntEarPoint - posAntAntChamferCutEdgePoint;
	IwVector3d CutEdgeToPoint2Vec = posAntCutEdgePoint2 - posAntAntChamferCutEdgePoint;
	IwVector3d crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())>0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then posAntCutEdgePoint2 is the lower middle point of the CutEdgeToEarVec
		posAntCutEdgePoint2 = posP2*posAntEarPoint + (1.0-posP2)*posAntAntChamferCutEdgePoint;
	}

	// antCutEdge25 is on the upper middle of posAntEarPoint and posAntAntChamferCutEdge
	IwPoint3d antCutEdge25 = posP25*posAntEarPoint+(1.0-posP25)*posAntAntChamferCutEdgePoint;
	IntersectSurfaceByLine(femurSurface, antCutEdge25 + 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d posAntCutEdgePoint25 = intPnt - distanceToCutEdge*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether posAntCutEdgePoint2 is in the correct location
	CutEdgeToEarVec = posAntEarPoint - posAntAntChamferCutEdgePoint;
	CutEdgeToPoint2Vec = posAntCutEdgePoint25 - posAntAntChamferCutEdgePoint;
	crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())>0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then posAntCutEdgePoint25 is the extension of posAntAntChamferCutEdgePoint to posAntCutEdgePoint2
		double extendLength = antCutEdge25.DistanceBetween(posAntAntChamferCutEdgePoint);
		IwVector3d tempVec = posAntCutEdgePoint2 - posAntAntChamferCutEdgePoint;
		tempVec.Unitize();
		posAntCutEdgePoint25 = posAntAntChamferCutEdgePoint + extendLength*tempVec;
	}

	// antCutEdge2 is on the lower middle of negAntEarPoint and negAntAntChamferCutEdge
	antCutEdge2 = negP2*negAntEarPoint+(1.0-negP2)*negAntAntChamferCutEdgePoint;
	IntersectSurfaceByLine(femurSurface, antCutEdge2 - 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d negAntCutEdgePoint2 = intPnt + negDistanceToCutEdge2*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether negAntCutEdgePoint2 is in the correct location
	CutEdgeToEarVec = negAntEarPoint - negAntAntChamferCutEdgePoint;
	CutEdgeToPoint2Vec = negAntCutEdgePoint2 - negAntAntChamferCutEdgePoint;
	crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())<0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then negAntCutEdgePoint2 is the lower middle point of the CutEdgeToEarVec
		negAntCutEdgePoint2 = negP2*negAntEarPoint + (1.0-negP2)*negAntAntChamferCutEdgePoint;
	}

	// antCutEdge25 is on the middle of negAntEarPoint and negAntAntChamferCutEdge
	antCutEdge25 = negP25*negAntEarPoint+(1.0-negP25)*negAntAntChamferCutEdgePoint;
	IntersectSurfaceByLine(femurSurface, antCutEdge25 - 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d negAntCutEdgePoint25 = (IwPoint3d)intPnt + distanceToCutEdge*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether negAntCutEdgePoint2 is in the correct location
	CutEdgeToEarVec = negAntEarPoint - negAntAntChamferCutEdgePoint;
	CutEdgeToPoint2Vec = negAntCutEdgePoint25 - negAntAntChamferCutEdgePoint;
	crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())<0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then negAntCutEdgePoint25 is the extension of negAntAntChamferCutEdgePoint to negAntCutEdgePoint2
		double extendLength = antCutEdge25.DistanceBetween(negAntAntChamferCutEdgePoint);
		IwVector3d tempVec = negAntCutEdgePoint2 - negAntAntChamferCutEdgePoint;
		tempVec.Unitize();
		negAntCutEdgePoint25 = negAntAntChamferCutEdgePoint + extendLength*tempVec;
	}

	m_AntOuterPoints.RemoveAll();
	// Note - antOuterPnts follow CCW direction
	m_AntOuterPoints.Add(negAntAntChamferCutEdgePoint);
	m_AntOuterPoints.Add(negAntCutEdgePoint2);
	m_AntOuterPoints.Add(negAntCutEdgePoint25);
	m_AntOuterPoints.Add(negAntEarPoint);
	m_AntOuterPoints.Add(antTipPoint);
	m_AntOuterPoints.Add(posAntEarPoint);
	m_AntOuterPoints.Add(posAntCutEdgePoint25);
	m_AntOuterPoints.Add(posAntCutEdgePoint2);
	m_AntOuterPoints.Add(posAntAntChamferCutEdgePoint);

	// Add the antTipPoint to the m_FeaturePoints
	// Need to project onto the ant cut plane
	IwPoint3d antCutTipPoint = antTipPoint.ProjectPointToPlane(antPnt, antNormal);
	m_FeaturePoints.Add(antCutTipPoint);

	return true;
}

bool CFemoralCuts::DetermineAntFeaturePoints()
{
	////////////////////////////////////////////////////////////////////
	// Determine the anterior feature points. They are optional outputs.
	////////////////////////////////////////////////////////////////////

	// Get anterior cut face
	IwTArray<IwFace*> antCutFaces;
	int antIndex = 9;
	SearchCutFaces(antIndex, antCutFaces);
	IwFace* antCutFace = antCutFaces.GetAt(0);
	IwSurface* antSurf = antCutFace->GetSurface();
	IwPoint3d antPnt, antNormal;
	antSurf->IsPlanar(0.001, &antPnt, &antNormal);

	IwAxis2Placement wslAxes;
	GetTotalDoc()->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);

	// Get anterior feature points
	IwTArray<IwPoint3d> antFeaturePoints;
	if ( GetTotalDoc()->GetOutlineProfile() )
		GetTotalDoc()->GetOutlineProfile()->GetAnteriorEarFeaturePoints(antFeaturePoints);
	if ( antFeaturePoints.GetSize() == 0 )
		COutlineProfile::GetInitialAnteriorEarFeaturePoints(GetTotalDoc(), antFeaturePoints);
	if ( antFeaturePoints.GetSize() == 0 ) return true;// Return true since these are optional outputs. 

	// Get femur (main area) surface 
	IwBSplineSurface* femurSurface = GetTotalDoc()->GetFemur()->GetSinglePatchSurface();

	IwPoint3d antTipPoint = antFeaturePoints.GetAt(0);
	IwPoint3d posAntEarPoint = antFeaturePoints.GetAt(1);
	IwPoint3d negAntEarPoint = antFeaturePoints.GetAt(2);
	IwPoint3d posAntAntChamferCutEdgePoint = m_FeaturePoints.GetAt(18);
	IwPoint3d negAntAntChamferCutEdgePoint = m_FeaturePoints.GetAt(19);
	// We need to create 2nd and 3rd points on the anterior cut plane lateral side
	double posP2, posP25, negP2, negP25;

	double posDistanceToCutEdge2, negDistanceToCutEdge2;
	double distanceToCutEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	if (positiveSideIsLateral)
	{
		posP2 = 0.2;
		posP25 = 0.55;
		posDistanceToCutEdge2 = distanceToCutEdge;
		negP2 = 0.2;
		negP25 = 0.50;
		negDistanceToCutEdge2 = distanceToCutEdge;// + 0.5;
	}
	else
	{
		posP2 = 0.2;
		posP25 = 0.50;
		posDistanceToCutEdge2 = distanceToCutEdge;// + 0.5;
		negP2 = 0.2;
		negP25 = 0.55;
		negDistanceToCutEdge2 = distanceToCutEdge;
	}
	// antCutEdge2 is on the lower middle of posAntEarPoint and posAntAntChamferCutEdge
	IwPoint3d antCutEdge2 = posP2*posAntEarPoint+(1.0-posP2)*posAntAntChamferCutEdgePoint;
	IwPoint3d intPnt;
	IntersectSurfaceByLine(femurSurface, antCutEdge2 + 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d posAntCutEdgePoint2 = intPnt - posDistanceToCutEdge2*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether posAntCutEdgePoint2 is in the correct location
	IwVector3d CutEdgeToEarVec = posAntEarPoint - posAntAntChamferCutEdgePoint;
	IwVector3d CutEdgeToPoint2Vec = posAntCutEdgePoint2 - posAntAntChamferCutEdgePoint;
	IwVector3d crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())>0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then posAntCutEdgePoint2 is the lower middle point of the CutEdgeToEarVec
		posAntCutEdgePoint2 = posP2*posAntEarPoint + (1.0-posP2)*posAntAntChamferCutEdgePoint;
	}

	// antCutEdge25 is on the upper middle of posAntEarPoint and posAntAntChamferCutEdge
	IwPoint3d antCutEdge25 = posP25*posAntEarPoint+(1.0-posP25)*posAntAntChamferCutEdgePoint;
	IntersectSurfaceByLine(femurSurface, antCutEdge25 + 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d posAntCutEdgePoint25 = intPnt - distanceToCutEdge*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether posAntCutEdgePoint2 is in the correct location
	CutEdgeToEarVec = posAntEarPoint - posAntAntChamferCutEdgePoint;
	CutEdgeToPoint2Vec = posAntCutEdgePoint25 - posAntAntChamferCutEdgePoint;
	crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())>0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then posAntCutEdgePoint25 is the extension of posAntAntChamferCutEdgePoint to posAntCutEdgePoint2
		double extendLength = antCutEdge25.DistanceBetween(posAntAntChamferCutEdgePoint);
		IwVector3d tempVec = posAntCutEdgePoint2 - posAntAntChamferCutEdgePoint;
		tempVec.Unitize();
		posAntCutEdgePoint25 = posAntAntChamferCutEdgePoint + extendLength*tempVec;
	}

	// antCutEdge2 is on the lower middle of negAntEarPoint and negAntAntChamferCutEdge
	antCutEdge2 = negP2*negAntEarPoint+(1.0-negP2)*negAntAntChamferCutEdgePoint;
	IntersectSurfaceByLine(femurSurface, antCutEdge2 - 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d negAntCutEdgePoint2 = intPnt + negDistanceToCutEdge2*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether negAntCutEdgePoint2 is in the correct location
	CutEdgeToEarVec = negAntEarPoint - negAntAntChamferCutEdgePoint;
	CutEdgeToPoint2Vec = negAntCutEdgePoint2 - negAntAntChamferCutEdgePoint;
	crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())<0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then negAntCutEdgePoint2 is the lower middle point of the CutEdgeToEarVec
		negAntCutEdgePoint2 = negP2*negAntEarPoint + (1.0-negP2)*negAntAntChamferCutEdgePoint;
	}

	// antCutEdge25 is on the middle of negAntEarPoint and negAntAntChamferCutEdge
	antCutEdge25 = negP25*negAntEarPoint+(1.0-negP25)*negAntAntChamferCutEdgePoint;
	IntersectSurfaceByLine(femurSurface, antCutEdge25 - 50*wslAxes.GetXAxis(), wslAxes.GetXAxis(), intPnt);
	IwPoint3d negAntCutEdgePoint25 = (IwPoint3d)intPnt + distanceToCutEdge*wslAxes.GetXAxis();// leave 1.0mm from the actual intersection 
	// However we need to verify whether negAntCutEdgePoint2 is in the correct location
	CutEdgeToEarVec = negAntEarPoint - negAntAntChamferCutEdgePoint;
	CutEdgeToPoint2Vec = negAntCutEdgePoint25 - negAntAntChamferCutEdgePoint;
	crossVec = CutEdgeToEarVec*CutEdgeToPoint2Vec;
	if ( crossVec.Dot(wslAxes.GetZAxis())<0 ) // if it is in the inner side of CutEdgeToEarVec
	{
		// then negAntCutEdgePoint25 is the extension of negAntAntChamferCutEdgePoint to negAntCutEdgePoint2
		double extendLength = antCutEdge25.DistanceBetween(negAntAntChamferCutEdgePoint);
		IwVector3d tempVec = negAntCutEdgePoint2 - negAntAntChamferCutEdgePoint;
		tempVec.Unitize();
		negAntCutEdgePoint25 = negAntAntChamferCutEdgePoint + extendLength*tempVec;
	}

	m_AntOuterPoints.RemoveAll();
	// Note - antOuterPnts follow CCW direction
	m_AntOuterPoints.Add(negAntAntChamferCutEdgePoint);
	m_AntOuterPoints.Add(negAntCutEdgePoint2);
	m_AntOuterPoints.Add(negAntCutEdgePoint25);
	m_AntOuterPoints.Add(negAntEarPoint);
	m_AntOuterPoints.Add(antTipPoint);
	m_AntOuterPoints.Add(posAntEarPoint);
	m_AntOuterPoints.Add(posAntCutEdgePoint25);
	m_AntOuterPoints.Add(posAntCutEdgePoint2);
	m_AntOuterPoints.Add(posAntAntChamferCutEdgePoint);

	// Add the antTipPoint to the m_FeaturePoints
	// Need to project onto the ant cut plane
	IwPoint3d antCutTipPoint = antTipPoint.ProjectPointToPlane(antPnt, antNormal);
	//m_FeaturePoints.Add(antCutTipPoint);

	return true;
}

//////////////////////////////////////////////////////////
// This function return 3 points on each edge on 
// anterior chamfer face, and distal cut faces.
//////////////////////////////////////////////////////////
bool CFemoralCuts::DetermineEdgeFeaturePoints
	(
	IwTArray<IwPoint3d>* cutFeaturePoints,	// I:
	IwTArray<IwPoint3d>& edgeFeaturePoints	// O:
	)
{
	edgeFeaturePoints.RemoveAll();

	IwTArray<IwPoint3d> featurePoints;
	if ( cutFeaturePoints == NULL )
	{
		IwTArray<IwPoint3d> cutFeatPnts;
		GetFeaturePoints(cutFeatPnts, NULL);
		featurePoints.Append(cutFeatPnts);
	}
	else
	{
		featurePoints.Append(*cutFeaturePoints);
	}

	double distanceToCutEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE") + 0.15;// 0.15 as allowance

	IwAxis2Placement wslAxes;
	GetTotalDoc()->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	IwTArray<IwFace*> cutFaces;
	// search positive side distal cut face
	int posIndex = 6;
	bool gotFace = SearchCutFaces(posIndex, cutFaces);
	// find the closest points on the edges
	IwPoint3d cPnt, cPntPlus, cPntMinus;
	IwVector3d tempVec, tempVecPlus, tempVecMinus;
	double param;
	double distMin = 100000, distMinPlus = 100000, distMinMinus = 100000;
	double dist, distPlus, distMinus;
	if ( gotFace )
	{
		// get 3 points between 10th and 14th feature points
		IwPoint3d posClosestPnt6Plus, posPnt6Plus = 0.72*featurePoints.GetAt(10)+0.28*featurePoints.GetAt(14);
		IwPoint3d posClosestPnt6, posPnt6 = 0.5*featurePoints.GetAt(10)+0.5*featurePoints.GetAt(14);
		IwPoint3d posClosestPnt6Minus, posPnt6Minus = 0.28*featurePoints.GetAt(10)+0.72*featurePoints.GetAt(14);
		for (unsigned i=0; i<cutFaces.GetSize(); i++)
		{
			IwFace* face = cutFaces.GetAt(i);
			IwTArray<IwEdge*> edges;
			face->GetEdges(edges);
			for (unsigned j=0; j<edges.GetSize(); j++)
			{
				IwEdge* edge = edges.GetAt(j);
				IwCurve* crv = edge->GetCurve();
				if ( !crv->IsLinear(0.01) )
				{
					dist = DistFromPointToEdge(posPnt6Minus, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMinMinus )
					{
						posClosestPnt6Minus = cPnt;
						distMinMinus = dist;
					}
					dist = DistFromPointToEdge(posPnt6, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMin )
					{
						posClosestPnt6 = cPnt;
						distMin = dist;
					}
					dist = DistFromPointToEdge(posPnt6Plus, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMinPlus )
					{
						posClosestPnt6Plus = cPnt;
						distMinPlus = dist;
					}
				}
			}
		}
		// move inward from the edge with distanceToCutEdge
		// Notice, it follows the CCW order to add into edgeFeaturePoints
		posClosestPnt6Minus = posClosestPnt6Minus - distanceToCutEdge*wslAxes.GetXAxis();
		posClosestPnt6 = posClosestPnt6 - distanceToCutEdge*wslAxes.GetXAxis();
		posClosestPnt6Plus = posClosestPnt6Plus - distanceToCutEdge*wslAxes.GetXAxis();
		// Need to determine whether one edge point (preferred) or 2 edge points are enough to approximate the boundary edge
		// interpolate a curve by featurePoints 10th, 14th, & posClosestPnt6
		IwTArray<IwPoint3d> intPnts;
		intPnts.Add(featurePoints.GetAt(10));
		intPnts.Add(posClosestPnt6);
		intPnts.Add(featurePoints.GetAt(14));
		IwBSplineCurve* edgeCurveBy1;
		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy1);
		distPlus = DistFromPointToCurve(posClosestPnt6Plus, edgeCurveBy1, cPnt, param);
		tempVecPlus = cPnt - posClosestPnt6Plus;
		distMinus = DistFromPointToCurve(posClosestPnt6Minus, edgeCurveBy1, cPnt, param);
		tempVecMinus = cPnt - posClosestPnt6Minus;
		if ( distPlus < distanceToCutEdge && distMinus < distanceToCutEdge )
		{// use posClosestPnt6 to define the edge boundary
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			edgeFeaturePoints.Add(posClosestPnt6);
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else if ( tempVecPlus.Dot(wslAxes.GetXAxis()) > 0 && tempVecMinus.Dot(wslAxes.GetXAxis()) > 0)// posClosestPnt6Plus & posClosestPnt6Plus are inside the edgeCurveBy1.  
		{// use posClosestPnt6 to define the edge boundary
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			edgeFeaturePoints.Add(posClosestPnt6);
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else 
		{
			intPnts.RemoveAll();
			intPnts.Add(featurePoints.GetAt(10));
			intPnts.Add(posClosestPnt6Plus);
			//intPnts.Add(posClosestPnt6);
			intPnts.Add(posClosestPnt6Minus);
			intPnts.Add(featurePoints.GetAt(14));
			IwBSplineCurve* edgeCurveBy2;
			IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy2);
			dist = DistFromPointToCurve(posClosestPnt6, edgeCurveBy2, cPnt, param);
			tempVec = cPnt - posClosestPnt6;
			if ( dist < distanceToCutEdge || tempVec.Dot(wslAxes.GetXAxis()) < 0 )// cPnt is inside of the posClosestPnt6.  
			{// use posClosestPnt6Minus & posClosestPnt6Plus to define the edge boundary
				edgeFeaturePoints.Add(posClosestPnt6Minus);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(posClosestPnt6Plus);
			}
			else
			{// use posClosestPnt6 to define the edge boundary
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(posClosestPnt6);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			}
			if ( edgeCurveBy2 )
				IwObjDelete(edgeCurveBy2);
		}

		if ( edgeCurveBy1 )
			IwObjDelete(edgeCurveBy1);
	}
	else
	{
		edgeFeaturePoints.RemoveAll();
		return false;
	}

	// search negative side distal cut face
	posIndex = 7;
	gotFace = SearchCutFaces(posIndex, cutFaces);
	// find the closest points on the edges
	distMin = 100000, distMinPlus = 100000, distMinMinus = 100000;
	if ( gotFace )
	{
		// get 3 points between 13th and 17th feature points
		IwPoint3d negClosestPnt7Plus, negPnt7Plus = 0.72*featurePoints.GetAt(13)+0.28*featurePoints.GetAt(17);
		IwPoint3d negClosestPnt7, negPnt7 = 0.5*featurePoints.GetAt(13)+0.5*featurePoints.GetAt(17);
		IwPoint3d negClosestPnt7Minus, negPnt7Minus = 0.28*featurePoints.GetAt(13)+0.72*featurePoints.GetAt(17);
		for (unsigned i=0; i<cutFaces.GetSize(); i++)
		{
			IwFace* face = cutFaces.GetAt(i);
			IwTArray<IwEdge*> edges;
			face->GetEdges(edges);
			for (unsigned j=0; j<edges.GetSize(); j++)
			{
				IwEdge* edge = edges.GetAt(j);
				IwCurve* crv = edge->GetCurve();
				if ( !crv->IsLinear(0.01) )
				{
					dist = DistFromPointToEdge(negPnt7Minus, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMinMinus )
					{
						negClosestPnt7Minus = cPnt;
						distMinMinus = dist;
					}
					dist = DistFromPointToEdge(negPnt7, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMin )
					{
						negClosestPnt7 = cPnt;
						distMin = dist;
					}
					dist = DistFromPointToEdge(negPnt7Plus, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMinPlus )
					{
						negClosestPnt7Plus = cPnt;
						distMinPlus = dist;
					}
				}
			}
		}
		// move inward from the edge with distanceToCutEdge
		// Notice, it follows the CCW order to add into edgeFeaturePoints
		negClosestPnt7Plus = negClosestPnt7Plus + distanceToCutEdge*wslAxes.GetXAxis();
		negClosestPnt7 = negClosestPnt7 + distanceToCutEdge*wslAxes.GetXAxis();
		negClosestPnt7Minus = negClosestPnt7Minus + distanceToCutEdge*wslAxes.GetXAxis();
		// Need to determine whether one edge point (preferred) or 2 edge points are enough to approximate the boundary edge
		// interpolate a curve by featurePoints 13th, 17th, & negClosestPnt7
		IwTArray<IwPoint3d> intPnts;
		intPnts.Add(featurePoints.GetAt(13));
		intPnts.Add(negClosestPnt7);
		intPnts.Add(featurePoints.GetAt(17));
		IwBSplineCurve* edgeCurveBy1;
		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy1);
		distPlus = DistFromPointToCurve(negClosestPnt7Plus, edgeCurveBy1, cPnt, param);
		tempVecPlus = cPnt - negClosestPnt7Plus;
		distMinus = DistFromPointToCurve(negClosestPnt7Minus, edgeCurveBy1, cPnt, param);
		tempVecMinus = cPnt - negClosestPnt7Minus;
		if ( distPlus < distanceToCutEdge && distMinus < distanceToCutEdge )
		{// use negClosestPnt7 to define the edge boundary
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			edgeFeaturePoints.Add(negClosestPnt7);
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else if ( tempVecPlus.Dot(wslAxes.GetXAxis()) > 0 && tempVecMinus.Dot(wslAxes.GetXAxis()) > 0)// negClosestPnt7Plus & negClosestPnt7Plus are inside the edgeCurveBy1.  
		{// use negClosestPnt7 to define the edge boundary
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			edgeFeaturePoints.Add(negClosestPnt7);
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else 
		{
			intPnts.RemoveAll();
			intPnts.Add(featurePoints.GetAt(13));
			intPnts.Add(negClosestPnt7Plus);
			//intPnts.Add(negClosestPnt7);
			intPnts.Add(negClosestPnt7Minus);
			intPnts.Add(featurePoints.GetAt(17));
			IwBSplineCurve* edgeCurveBy2;
			IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy2);
			dist = DistFromPointToCurve(negClosestPnt7, edgeCurveBy2, cPnt, param);
			tempVec = cPnt - negClosestPnt7;
			if ( dist < distanceToCutEdge || tempVec.Dot(wslAxes.GetXAxis()) > 0 )// cPnt is inside of the negClosestPnt7.  
			{// use negClosestPnt7Minus & negClosestPnt7Plus to define the edge boundary
				edgeFeaturePoints.Add(negClosestPnt7Plus);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(negClosestPnt7Minus);
			}
			else
			{// use negClosestPnt7 to define the edge boundary
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(negClosestPnt7);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			}
			if ( edgeCurveBy2 )
				IwObjDelete(edgeCurveBy2);
		}

		if ( edgeCurveBy1 )
			IwObjDelete(edgeCurveBy1);
	}
	else
	{
		edgeFeaturePoints.RemoveAll();
		return false;
	}

	// search anterior chamfer cut face
	posIndex = 8;
	gotFace = SearchCutFaces(posIndex, cutFaces);
	// find the closest points on the edges
	distMin = 100000, distMinPlus = 100000, distMinMinus = 100000;
	if ( gotFace )
	{
		// get 3 points between 14th and 18th feature points
		IwPoint3d posClosestPnt8Plus, posPnt8Plus = 0.72*featurePoints.GetAt(14)+0.28*featurePoints.GetAt(18);
		IwPoint3d posClosestPnt8, posPnt8 = 0.5*featurePoints.GetAt(14)+0.5*featurePoints.GetAt(18);
		IwPoint3d posClosestPnt8Minus, posPnt8Minus = 0.28*featurePoints.GetAt(14)+0.72*featurePoints.GetAt(18);
		for (unsigned i=0; i<cutFaces.GetSize(); i++)
		{
			IwFace* face = cutFaces.GetAt(i);
			IwTArray<IwEdge*> edges;
			face->GetEdges(edges);
			for (unsigned j=0; j<edges.GetSize(); j++)
			{
				IwEdge* edge = edges.GetAt(j);
				IwCurve* crv = edge->GetCurve();
				if ( !crv->IsLinear(0.01) )
				{
					dist = DistFromPointToEdge(posPnt8Minus, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMinMinus )
					{
						posClosestPnt8Minus = cPnt;
						distMinMinus = dist;
					}
					dist = DistFromPointToEdge(posPnt8, edge, cPnt);
					if ( dist>-0.5 && dist < distMin )
					{
						posClosestPnt8 = cPnt;
						distMin = dist;
					}
					dist = DistFromPointToEdge(posPnt8Plus, edge, cPnt);
					if ( dist>-0.5 && dist < distMinPlus )
					{
						posClosestPnt8Plus = cPnt;
						distMinPlus = dist;
					}
				}
			}
		}
		// move inward from the edge with distanceToCutEdge
		// Notice, it follows the CCW order to add into edgeFeaturePoints
		posClosestPnt8Minus = posClosestPnt8Minus - distanceToCutEdge*wslAxes.GetXAxis();
		posClosestPnt8 = posClosestPnt8 - distanceToCutEdge*wslAxes.GetXAxis();
		posClosestPnt8Plus = posClosestPnt8Plus - distanceToCutEdge*wslAxes.GetXAxis();
		// Need to determine whether one edge point (preferred) or 2 edge points are enough to approximate the boundary edge
		// interpolate a curve by featurePoints 14th, 18th, & posClosestPnt8
		IwTArray<IwPoint3d> intPnts;
		intPnts.Add(featurePoints.GetAt(14));
		intPnts.Add(posClosestPnt8);
		intPnts.Add(featurePoints.GetAt(18));
		IwBSplineCurve* edgeCurveBy1;
		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy1);
		distPlus = DistFromPointToCurve(posClosestPnt8Plus, edgeCurveBy1, cPnt, param);
		tempVecPlus = cPnt - posClosestPnt8Plus;
		distMinus = DistFromPointToCurve(posClosestPnt8Minus, edgeCurveBy1, cPnt, param);
		tempVecMinus = cPnt - posClosestPnt8Minus;
		if ( distPlus < distanceToCutEdge && distMinus < distanceToCutEdge )
		{// use posClosestPnt8 to define the edge boundary
			if ( positiveSideIsLateral )
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			else
				edgeFeaturePoints.Add(posClosestPnt8Minus);// posClosestPnt8Minus should be added if medial side
			edgeFeaturePoints.Add(posClosestPnt8);
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else if ( tempVecPlus.Dot(wslAxes.GetXAxis()) > 0 && tempVecMinus.Dot(wslAxes.GetXAxis()) > 0)// posClosestPnt8Plus & posClosestPnt8Plus are inside the edgeCurveBy1.  
		{// use posClosestPnt8 to define the edge boundary
			if ( positiveSideIsLateral )
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			else
				edgeFeaturePoints.Add(posClosestPnt8Minus);// posClosestPnt8Minus should be added if medial side
			edgeFeaturePoints.Add(posClosestPnt8);
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else 
		{
			intPnts.RemoveAll();
			intPnts.Add(featurePoints.GetAt(14));
			intPnts.Add(posClosestPnt8Plus);
			//intPnts.Add(posClosestPnt8);
			intPnts.Add(posClosestPnt8Minus);
			intPnts.Add(featurePoints.GetAt(18));
			IwBSplineCurve* edgeCurveBy2;
			IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy2);
			dist = DistFromPointToCurve(posClosestPnt8, edgeCurveBy2, cPnt, param);
			tempVec = cPnt - posClosestPnt8;
			if ( dist < distanceToCutEdge || tempVec.Dot(wslAxes.GetXAxis()) < 0 )// cPnt is inside of the posClosestPnt8.  
			{// use posClosestPnt8Minus & posClosestPnt8Plus to define the edge boundary
				edgeFeaturePoints.Add(posClosestPnt8Minus);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(posClosestPnt8Plus);
			}
			else
			{// use posClosestPnt8 to define the edge boundary
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(posClosestPnt8);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			}
			if ( edgeCurveBy2 )
				IwObjDelete(edgeCurveBy2);
		}

		if ( edgeCurveBy1 )
			IwObjDelete(edgeCurveBy1);
	}
	else
	{
		edgeFeaturePoints.RemoveAll();
		return false;
	}

	// search anterior chamfer cut face (just do it again)
	posIndex = 8;
	gotFace = SearchCutFaces(posIndex, cutFaces);
	// find the closest points on the edges
	distMin = 100000, distMinPlus = 100000, distMinMinus = 100000;
	if ( gotFace )
	{
		// get 3 points between 17th and 19th feature points
		IwPoint3d negClosestPnt8Plus, negPnt8Plus = 0.72*featurePoints.GetAt(17)+0.28*featurePoints.GetAt(19);
		IwPoint3d negClosestPnt8, negPnt8 = 0.5*featurePoints.GetAt(17)+0.5*featurePoints.GetAt(19);
		IwPoint3d negClosestPnt8Minus, negPnt8Minus = 0.28*featurePoints.GetAt(17)+0.72*featurePoints.GetAt(19);
		for (unsigned i=0; i<cutFaces.GetSize(); i++)
		{
			IwFace* face = cutFaces.GetAt(i);
			IwTArray<IwEdge*> edges;
			face->GetEdges(edges);
			for (unsigned j=0; j<edges.GetSize(); j++)
			{
				IwEdge* edge = edges.GetAt(j);
				IwCurve* crv = edge->GetCurve();
				if ( !crv->IsLinear(0.01) )
				{
					dist = DistFromPointToEdge(negPnt8Minus, edge, cPnt);
					// dist = -1 if no solution
					if ( dist>-0.5 && dist < distMinMinus )
					{
						negClosestPnt8Minus = cPnt;
						distMinMinus = dist;
					}
					dist = DistFromPointToEdge(negPnt8, edge, cPnt);
					if ( dist>-0.5 && dist < distMin )
					{
						negClosestPnt8 = cPnt;
						distMin = dist;
					}
					dist = DistFromPointToEdge(negPnt8Plus, edge, cPnt);
					if ( dist>-0.5 && dist < distMinPlus )
					{
						negClosestPnt8Plus = cPnt;
						distMinPlus = dist;
					}
				}
			}
		}
		// move inward from the edge with distanceToCutEdge
		// Notice, it follows the CCW order to add into edgeFeaturePoints
		negClosestPnt8Plus = negClosestPnt8Plus + distanceToCutEdge*wslAxes.GetXAxis();
		negClosestPnt8 = negClosestPnt8 + distanceToCutEdge*wslAxes.GetXAxis();
		negClosestPnt8Minus = negClosestPnt8Minus + distanceToCutEdge*wslAxes.GetXAxis();
		// Need to determine whether one edge point (preferred) or 2 edge points are enough to approximate the boundary edge
		// interpolate a curve by featurePoints 17th, 19th, & negClosestPnt8
		IwTArray<IwPoint3d> intPnts;
		intPnts.Add(featurePoints.GetAt(17));
		intPnts.Add(negClosestPnt8);
		intPnts.Add(featurePoints.GetAt(19));
		IwBSplineCurve* edgeCurveBy1;
		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy1);
		distPlus = DistFromPointToCurve(negClosestPnt8Plus, edgeCurveBy1, cPnt, param);
		tempVecPlus = cPnt - negClosestPnt8Plus;
		distMinus = DistFromPointToCurve(negClosestPnt8Minus, edgeCurveBy1, cPnt, param);
		tempVecMinus = cPnt - negClosestPnt8Minus;
		if ( distPlus < distanceToCutEdge && distMinus < distanceToCutEdge )
		{// use negClosestPnt8 to define the edge boundary
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			edgeFeaturePoints.Add(negClosestPnt8);
			if ( positiveSideIsLateral )
				edgeFeaturePoints.Add(negClosestPnt8Minus);
			else
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else if ( tempVecPlus.Dot(wslAxes.GetXAxis()) > 0 && tempVecMinus.Dot(wslAxes.GetXAxis()) > 0)// negClosestPnt8Plus & negClosestPnt8Plus are inside the edgeCurveBy1.  
		{// use negClosestPnt8 to define the edge boundary
			edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			edgeFeaturePoints.Add(negClosestPnt8);
			if ( positiveSideIsLateral )
				edgeFeaturePoints.Add(negClosestPnt8Minus);
			else
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
		}
		else 
		{
			intPnts.RemoveAll();
			intPnts.Add(featurePoints.GetAt(17));
			intPnts.Add(negClosestPnt8Plus);
			//intPnts.Add(negClosestPnt8);
			intPnts.Add(negClosestPnt8Minus);
			intPnts.Add(featurePoints.GetAt(19));
			IwBSplineCurve* edgeCurveBy2;
			IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, edgeCurveBy2);
			dist = DistFromPointToCurve(negClosestPnt8, edgeCurveBy2, cPnt, param);
			tempVec = cPnt - negClosestPnt8;
			if ( dist < distanceToCutEdge || tempVec.Dot(wslAxes.GetXAxis()) > 0 )// cPnt is inside of the negClosestPnt8.  
			{// use negClosestPnt8Minus & negClosestPnt8Plus to define the edge boundary
				edgeFeaturePoints.Add(negClosestPnt8Plus);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(negClosestPnt8Minus);
			}
			else
			{// use negClosestPnt8 to define the edge boundary
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
				edgeFeaturePoints.Add(negClosestPnt8);
				edgeFeaturePoints.Add(IwPoint3d(0,0,0));
			}
			if ( edgeCurveBy2 )
				IwObjDelete(edgeCurveBy2);
		}

		if ( edgeCurveBy1 )
			IwObjDelete(edgeCurveBy1);
	}
	else
	{
		edgeFeaturePoints.RemoveAll();
		return false;
	}

	m_edgeFeaturePoints.RemoveAll();
	m_edgeFeaturePoints = edgeFeaturePoints;
	return true;
}

bool CFemoralCuts::SearchCutFaces
	(
	int &posIndex,					// I:
	IwTArray<IwFace*>& cutFaces		// O: always sort by the size of area
	)
{
	cutFaces.RemoveAll();

	if ( m_cutPoints.GetSize() == 0 ) return false;
	if ( m_pBrep == NULL ) return false;

	IwPoint3d cutPoint = m_cutPoints.GetAt(posIndex);
	IwVector3d cutVector = m_cutOrientations.GetAt(posIndex);

	// Call the next static function
	bool succeed = CFemoralCuts::SearchCutFaces(m_pBrep, cutPoint, cutVector, cutFaces);

	return succeed;
}

/* static */
bool CFemoralCuts::SearchCutFaces
	(
	IwBrep*& pBrep,					// I:
	IwPoint3d& cutPoint,			// I:
	IwVector3d& cutVector,			// I:
	IwTArray<IwFace*>& cutFaces		// O: always sort by the size of area
	)
{
	cutFaces.RemoveAll();

	IwTArray<IwFace*> faces;
	IwPoint3d pnt;
	IwVector3d normal, orient, vec;

	pBrep->GetFaces(faces);
	//for (unsigned i=0; i < faces.GetSize(); i++)
	//{
	//	IwFace* face = faces.GetAt(i);
	//	IwSurface* surf = face->GetSurface();
	//	if ( surf->IsPlanar(0.0001, &pnt, &normal) ) // cut face should be planar
	//	{
	//		vec = cutPoint - pnt;
	//		if ( vec.Length() < 0.001 )
	//			vec = IwVector3d(0,0,0);
	//		orient = cutVector;
	//		// normal should be parallel and cutPoint should be <0.025mm deviated from cut face


	//		double rdAngRad;
	//		orient.IsParallelTo(normal, rdAngRad);

	//		bool orientCheck = orient.IsParallelTo(normal, 0.025);
	//		double dotProd = fabs(orient.Dot(vec));
	//		if ( orient.IsParallelTo(normal, 0.025) && fabs(orient.Dot(vec)) < 0.0125 ) // 0.025 degree & 1/2*0.025 distance to cut face
	//		{
	//			cutFaces.Add(face);
	//		}
	//	}
	//}

	IwFace * pDropFace = NULL;
	double uv = 0;
	DropPointOnBrep(pBrep, cutPoint, &uv, pDropFace);

	IwSurface* surf = pDropFace->GetSurface();
	bool isPlanar = surf->IsPlanar(0.0001, &pnt, &normal);

	if(isPlanar == false) return false;

	cutFaces.Add(pDropFace);

	if (cutFaces.GetSize() == 0) return false;
	if (cutFaces.GetSize() == 1) return true; // No need to sort.

	// Sort by the area
	double dArea,  dVol;
	IwVector3d dBCtr, dMom[2];
	IwTArray<double> areas;
	for (unsigned i=0; i<cutFaces.GetSize(); i++)
	{
		cutFaces.GetAt(i)->ComputeProperties(IW_OT_SAME, 0.01, 0.01, IwVector3d(0,0,0), dArea, dVol, dBCtr, dMom );
		areas.Add(dArea);
	}

	IwFace* swapFace;
	double swapArea;
	for (unsigned i=0; i<cutFaces.GetSize(); i++)
	{
		for(unsigned j=i+1; j<cutFaces.GetSize(); j++)
		{
			if ( areas.GetAt(i) < areas.GetAt(j) )
			{
				swapFace = cutFaces.GetAt(i);
				cutFaces.SetAt(i, cutFaces.GetAt(j));
				cutFaces.SetAt(j, swapFace);
				swapArea = areas.GetAt(i);
				areas.SetAt(i, areas.GetAt(j));
				areas.SetAt(j, swapArea);
			}
		}
	}

	return true;
}

/* static */
// This function only checks 10 cuts. The step cut is not included.
bool CFemoralCuts::IsAllCutsExist(IwBrep*& pBrep, IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutVectors)
{
	CBrepArray arrOfBreps;
	arrOfBreps.Add(pBrep);
	WriteIges("C:\\PS\\Ops\\770116L_FemCut.igs", NULL, NULL, NULL, NULL, &arrOfBreps);

	if ( cutPoints.GetSize() < 11 )
		return false;

	bool allCutsExist = true, aCutExist;
	IwPoint3d cutPoint, cutVector;
	IwTArray<IwFace*> cutFaces;
	for (unsigned i=0; i<10; i++)
	{
		cutPoint = cutPoints.GetAt(i);
		cutVector = cutVectors.GetAt(i);
		aCutExist = CFemoralCuts::SearchCutFaces(pBrep, cutPoint, cutVector, cutFaces);
		if ( !aCutExist )
		{
			allCutsExist = false;
			break;
		}
	}

	return allCutsExist;
}


bool CFemoralCuts::ApproxCommonEdgeCurveForTwoNonInterFaces
	(
	int &posIndex1,						// I:
	int &posIndex2,						// I:
	IwCurve *& commonEdgeCurve      	// O:
	)
{
	IwTArray<IwFace*> cutFaces1, cutFaces2;
	bool gotFace1=false, gotFace2=false;

	// Search face1
	gotFace1 = SearchCutFaces(posIndex1, cutFaces1);
	if ( !gotFace1 ) return false;

	// Search face2
	gotFace2 = SearchCutFaces(posIndex2, cutFaces2);
	if ( !gotFace2 ) return false;

	IwBSplineSurface * pSurf1 = (IwBSplineSurface*)cutFaces1.GetAt(0)->GetSurface();
	IwBSplineSurface * pSurf2 = (IwBSplineSurface*)cutFaces2.GetAt(0)->GetSurface();

	IwAxis2Placement mechAxes = IFemurImplant::FemoralAxes_GetMechanicalAxes();

	IwPoint3d farPointOnPosXAxes = mechAxes.GetOrigin() + 100*mechAxes.GetXAxis();
	IwPoint3d farPointOnNegXAxes = mechAxes.GetOrigin() - 100*mechAxes.GetXAxis();

	IwPoint2d param2d;
	IwPoint3d posExtreamePoint;
	DistFromPointToSurface(farPointOnPosXAxes, pSurf1, posExtreamePoint, param2d);

	IwPoint3d negExtreamePoint;
	DistFromPointToSurface(farPointOnNegXAxes, pSurf1, negExtreamePoint, param2d);

	IwPlane plane1(m_cutPoints.GetAt(posIndex1), m_cutOrientations.GetAt(posIndex1));
	IwPlane plane2(m_cutPoints.GetAt(posIndex2), m_cutOrientations.GetAt(posIndex2));

	IwTArray<IwCurve*> intCurves;
	IntersectSurfaces(&plane1, &plane2, intCurves);
	if(intCurves.GetSize() == 0) return false;

	IwCurve * pCurve = intCurves.GetAt(0);

	double param;
	IwPoint3d posEdgePoint;
	DistFromPointToCurve(posExtreamePoint, (IwBSplineCurve*)pCurve, posEdgePoint, param);

	IwPoint3d negEdgePoint;
	DistFromPointToCurve(negExtreamePoint, (IwBSplineCurve*)pCurve, negEdgePoint, param);

	IwContext& iwContext = m_pDoc->GetIwContext();

	IwBSplineCurve * pRetCurve = NULL;
	IwBSplineCurve::CreateLineSegment(iwContext, 3, posEdgePoint, negEdgePoint, pRetCurve);

	commonEdgeCurve = pRetCurve;

	if(0)
	{
		ShowCurve(m_pDoc, commonEdgeCurve, green);
	}

	return true;
}



bool CFemoralCuts::SearchCommonCutEdges
	(
	int &posIndex1,						// I:
	int &posIndex2,						// I:
	IwTArray<IwEdge*> &commonCutEdges,	// O:
	int howToSortEdges					// I: if exist multiple edges, how to sort the edges
	// 0: sort by length, 1: sort by positive x position, -1: sort by negative x position
	)
{
	commonCutEdges.RemoveAll();

	IwTArray<IwEdge*> myCommonCutEdges;

	IwTArray<IwFace*> cutFaces1, cutFaces2;
	bool gotFace1=false, gotFace2=false;

	// Search face1
	gotFace1 = SearchCutFaces(posIndex1, cutFaces1);
	if ( !gotFace1 ) return false;

	// Search face2
	gotFace2 = SearchCutFaces(posIndex2, cutFaces2);
	if ( !gotFace2 ) return false;

	//IwContext& iwContext = m_pDoc->GetIwContext();
	//IwBrep *pTestBrep1 = new (iwContext)IwBrep();
	//
	//m_pBrep->CopyFaces(cutFaces1, pTestBrep1);

	//CBrepArray arrOfBrep1;
	//arrOfBrep1.Add(pTestBrep1);
	//WriteIges("C:\\PS\\Ops\\pTestBrep1.iges", NULL, NULL, NULL, NULL, &arrOfBrep1);

	//IwBrep *pTestBrep2 = new (iwContext)IwBrep();
	//
	//m_pBrep->CopyFaces(cutFaces2, pTestBrep2);

	//CBrepArray arrOfBrep2;
	//arrOfBrep2.Add(pTestBrep2);
	//WriteIges("C:\\PS\\Ops\\pTestBrep2.iges", NULL, NULL, NULL, NULL, &arrOfBrep2);

	IwTArray<IwEdge*> edges1;
	IwTArray<IwFace*> facesSurrounding;
	IwEdge* edge;
	IwFace* face;

	m_pBrep->MakeManifold();
	m_pBrep->SewAndOrient();

	for (unsigned i=0; i<cutFaces1.GetSize(); i++)
	{
		cutFaces1.GetAt(i)->GetEdges(edges1);
		for (unsigned j=0; j<edges1.GetSize(); j++)
		{
			edge = edges1.GetAt(j);
			edge->GetFaces(facesSurrounding);
			for(unsigned k=0; k<facesSurrounding.GetSize(); k++)
			{
				face = facesSurrounding.GetAt(k);
				for (unsigned l=0; l<cutFaces2.GetSize(); l++)
				{
					if (face == cutFaces2.GetAt(l))
					{
						myCommonCutEdges.AddUnique(edge);
					}
				}
			}
		}
	}
	if (myCommonCutEdges.GetSize() == 0) return false;

	// Sort based on whichone
	// whichOne=0 -> sort by length
	// whichOne=1 -> sort by positive x-axis position
	// whichOne=-1 -> sort by negative x-axis position
	IwAxis2Placement sweepAxes;
	GetTotalDoc()->GetFemoralAxes()->GetSweepAxes(sweepAxes);
	for (unsigned i=0; i<myCommonCutEdges.GetSize(); i++)
	{
		if (howToSortEdges == 0 )// sort by length
		{
			IwEdge* edge0 = myCommonCutEdges.GetAt(i);
			IwCurve* crv0 = edge0->GetCurve();
			double length0;
			crv0->Length(edge0->GetInterval(), 0.001, length0);
			for (unsigned j=i+1; j<myCommonCutEdges.GetSize(); j++)
			{
				IwEdge* edge1 = myCommonCutEdges.GetAt(j);
				IwCurve* crv1 = edge1->GetCurve();
				double length1;
				crv1->Length(edge1->GetInterval(), 0.001, length1);
				if (length1 > length0)
				{
					myCommonCutEdges.SetAt(i, edge1);
					myCommonCutEdges.SetAt(j, edge0);
				}
			}
		}
		else 
		{
			IwEdge* edge0 = myCommonCutEdges.GetAt(i);
			IwVertex *sVertex0, *eVertex0;
			edge0->GetVertices(sVertex0, eVertex0);
			IwPoint3d midPnt0 = 0.5*sVertex0->GetPoint() + 0.5*eVertex0->GetPoint();
			for (unsigned j=i+1; j<myCommonCutEdges.GetSize(); j++)
			{
				IwEdge* edge1 = myCommonCutEdges.GetAt(j);
				IwVertex *sVertex1, *eVertex1;
				edge1->GetVertices(sVertex1, eVertex1);
				IwPoint3d midPnt1 = 0.5*sVertex1->GetPoint() + 0.5*eVertex1->GetPoint();
				if (howToSortEdges==1)
				{
					if ( (midPnt1-midPnt0).Dot(sweepAxes.GetXAxis()) > 0 )// midPnt1 in more positive side
					{
						myCommonCutEdges.SetAt(i, edge1);
						myCommonCutEdges.SetAt(j, edge0);
					}
				}
				else if (howToSortEdges==-1)
				{
					if ( (midPnt1-midPnt0).Dot(sweepAxes.GetXAxis()) < 0 )// midPnt1 in more negative side
					{
						myCommonCutEdges.SetAt(i, edge1);
						myCommonCutEdges.SetAt(j, edge0);
					}
				}
			}
		}
	}
	commonCutEdges.Append(myCommonCutEdges);





	return true;
}

bool CFemoralCuts::CalculateMostPosteriorPointOnPosteriorFaceEdges
	(
	IwFace* face,			// I:
	IwPoint3d& extremePnt,	// O:
	double *distanceToCutEdge	// I: optional for additional offset distance from edge
	)
{
	IwAxis2Placement sweepAxes;
	GetTotalDoc()->GetFemoralAxes()->GetSweepAxes(sweepAxes);
	IwVector3d zAxis = sweepAxes.GetZAxis();

	double addInsetDist = 0;
	if (distanceToCutEdge)
		addInsetDist = *distanceToCutEdge;

	// get the face center by the peripherical sampling points
	IwTArray<IwEdge*> edges;
	face->GetEdges(edges);
	IwPoint3d pnt;
	IwPoint3d theFaceCenter = IwPoint3d(0,0,0);
	int totalPntNum = 0;
	for (unsigned j=0; j<edges.GetSize(); j++)
	{
		IwEdge* edge = edges.GetAt(j);
		IwCurve* curve = edge->GetCurve();
		IwExtent1d domain = curve->GetNaturalInterval();
		double delta = domain.GetLength()/10.0;
		for (int i=0; i<10; i++)
		{
			curve->EvaluatePoint(domain.GetMin() + i*delta, pnt);
			totalPntNum++;
			theFaceCenter += pnt;
		}
	}
	theFaceCenter /=totalPntNum;

	IwPoint3d farZPnt = theFaceCenter + 100*zAxis;
	IwPoint3d cPnt;

	DistFromPointToEdges(farZPnt, edges, cPnt);

	extremePnt = cPnt - addInsetDist*zAxis;

	return true;
}

bool CFemoralCuts::SamplingPointsOnFaceOuterEdge
	(
	IwFace* face,						// I:
	IwVector3d faceNormal,				// I:
	IwTArray<IwPoint3d>& connectedPnts,	// I:
	double &samplingDist,				// I:
	IwTArray<IwEdge*> &edgesToAvoid,	// I:
	IwTArray<IwPoint3d>& samplingPnts	// O:
	)
{
	samplingPnts.RemoveAll();

	IwContext& iwContext = m_pDoc->GetIwContext();
	IwTArray<IwEdge*> edges;
	IwPoint3d pnt;
	IwVector3d normal;
	IwExtent1d crvDomain;
	IwPoint3d exPnt;
	double length, maxLength = -100000;
	double tol;

	IwTArray<IwLoop*> loops;
	face->GetLoops(loops);
	IwLoop* firstLoop = loops.GetAt(0);
	IwLoopuse *pLU1, *pLU2;
	firstLoop->GetLoopuses(pLU1,pLU2);
	IwTArray<IwEdgeuse*> edgeUses;
	pLU1->GetEdgeuses(edgeUses);

	// get the edge number that is to avoid.
	// the search of outer edge will start there.

	int startSearchEdgeNo = 0;
	int nEdgeSize = edgeUses.GetSize();
	for (int j=0; j<nEdgeSize; j++)
	{
		IwEdgeuse* edgeuse = edgeUses.GetAt(j);
		IwEdge* edge = edgeuse->GetEdge();
		if (edgesToAvoid.GetSize() > 0 &&
			edge == edgesToAvoid.GetAt(0))// find any one
		{
			startSearchEdgeNo = j;
			break;
		}
	}

	for (int j=startSearchEdgeNo; j<(nEdgeSize+startSearchEdgeNo); j++)
	{
		IwEdgeuse* edgeuse = edgeUses.GetAt(j%nEdgeSize);
		IwEdge* edge = edgeuse->GetEdge();
		IwCurve* curve = edge->GetCurve();
		// Should we avoid this edge
		bool avoidThisEdge = false;
		for (unsigned jj=0; jj<edgesToAvoid.GetSize(); jj++)
		{
			if ( edge == edgesToAvoid.GetAt(jj) )
			{
				avoidThisEdge = true;
				break;
			}
		}

		// Get the anterior outer edges
		if ( !avoidThisEdge )
		{
			IwBSplineCurve* bCurve = (IwBSplineCurve*)curve;
			IwBSplineCurve* offsetCurve = NULL;
			double offsetDist = -1.0;
			if (edgeuse->GetOrientation() == IW_OT_SAME)
				offsetDist = 1.0;
			bCurve->CreateSimpleOffset(iwContext, 0.01, faceNormal, offsetDist, offsetCurve, tol);
			IwPoint3d stPnt, edPnt;
			bool connected = false;
			offsetCurve->GetEnds(stPnt, edPnt);
			for (unsigned k=0; k<connectedPnts.GetSize(); k++)
			{
				// Anterior cut may have "M" situation, which needs both stPnt and edPnt distance to connected points.
				if (stPnt.DistanceBetween(connectedPnts.GetAt(k)) < 0.75)
				{
					connected = true;
					connectedPnts.Add(stPnt);
					connectedPnts.Add(edPnt);
					break;
				}
				if (edPnt.DistanceBetween(connectedPnts.GetAt(k)) < 0.75)
				{
					connected = true;
					connectedPnts.Add(stPnt);
					connectedPnts.Add(edPnt);
					break;
				}

			}
			if (connected)
			{
				if (edgeuse->GetOrientation() == IW_OT_OPPOSITE)
				{
					IwExtent1d newInv;
					offsetCurve->ReverseParameterization(offsetCurve->GetNaturalInterval(), newInv);
				}
				crvDomain = offsetCurve->GetNaturalInterval();
				offsetCurve->Length(crvDomain, 0.01, length);
				int samplingNum = (int)length/samplingDist + 1;
				double deltaT = (crvDomain.GetMax()-crvDomain.GetMin())/samplingNum;
				double t = crvDomain.GetMin();
				for (int i=0; i< (samplingNum+1); i++, t+=deltaT)
				{
					offsetCurve->EvaluatePoint(t, pnt);
					samplingPnts.AddUnique(pnt);
				}
			}
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////
// This function returns 2 points and 2 vectors in anterior and posterior to help 
// construct the sketch surface.
///////////////////////////////////////////////////////////////////////////////////////
bool CFemoralCuts::GetAntPostInfo(IwTArray<IwPoint3d> &antPostInfo)
{
	antPostInfo.RemoveAll();

	IwTArray<IwPoint3d> femoralCutsFeaturePoints;
	GetFeaturePoints(femoralCutsFeaturePoints, NULL);

	if (femoralCutsFeaturePoints.GetSize() != 21) return false;

	IwTArray<IwPoint3d> femoralCutPoints;
	IwTArray<IwVector3d> femoralCutOrientation;
	GetCutsInfo(femoralCutPoints, femoralCutOrientation);

	// Get femoral axes information
	CFemoralAxes*		femoralAxes = GetTotalDoc()->GetFemoralAxes();
	if (femoralAxes==NULL) return false;

	IwAxis2Placement axes;
	femoralAxes->GetSweepAxes(axes);

	// Get left/right implant info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	// Get the cut info
	IwPoint3d antTipPoint;
	IwVector3d antTipVector;
	antTipPoint = femoralCutsFeaturePoints.GetAt(20);
	antTipPoint = antTipPoint.ProjectPointToPlane(axes.GetOrigin(), axes.GetXAxis());
	antTipVector = axes.GetXAxis()*femoralCutOrientation.GetAt(9);

	CFemoralAxes *pFemoralAxes = GetTotalDoc()->GetFemoralAxes();
	double flexAngle, obliAngle;
	pFemoralAxes->GetAnteriorCutFlexionObliqueAngles(flexAngle, obliAngle);
	IwPoint3d antPoint;
	IwVector3d antVector;
	if ( obliAngle > 0 )// Get the higher point
		antPoint = femoralCutsFeaturePoints.GetAt(18);
	else
		antPoint = femoralCutsFeaturePoints.GetAt(19);
	antPoint = antPoint.ProjectPointToPlane(axes.GetOrigin(), axes.GetXAxis());
	antVector = axes.GetXAxis()*femoralCutOrientation.GetAt(9);

	IwPoint3d postPoint;
	IwVector3d postVector;
	IwPoint3d postTipPoint;
	IwVector3d postTipVector;
	if (implantSide == IMPLANT_SIDE_LEFT)
	{
		postPoint = femoralCutsFeaturePoints.GetAt(3);
		postPoint = postPoint.ProjectPointToPlane(axes.GetOrigin(), axes.GetXAxis());
		postVector = axes.GetXAxis()*femoralCutOrientation.GetAt(0);
		postTipPoint = femoralCutsFeaturePoints.GetAt(0);
		postTipPoint = postTipPoint.ProjectPointToPlane(axes.GetOrigin(), axes.GetXAxis());
		postTipVector = axes.GetXAxis()*femoralCutOrientation.GetAt(0);
	}
	else
	{
		postPoint = femoralCutsFeaturePoints.GetAt(4);
		postPoint = postPoint.ProjectPointToPlane(axes.GetOrigin(), axes.GetXAxis());
		postVector = axes.GetXAxis()*femoralCutOrientation.GetAt(1);
		postTipPoint = femoralCutsFeaturePoints.GetAt(1);
		postTipPoint = postTipPoint.ProjectPointToPlane(axes.GetOrigin(), axes.GetXAxis());
		postTipVector = axes.GetXAxis()*femoralCutOrientation.GetAt(1);
	}

	antPostInfo.Add(antTipPoint);
	antPostInfo.Add(antTipVector);
	antPostInfo.Add(antPoint);
	antPostInfo.Add(antVector);
	antPostInfo.Add(postPoint);
	antPostInfo.Add(postVector);
	antPostInfo.Add(postTipPoint);
	antPostInfo.Add(postTipVector);

	return true;

}

//////////////////////////////////////////////////////////////////////
// This function returns the center of the 1st/2nd posterior cut edge 
bool CFemoralCuts::GetCondylarCutCenters
	(
	IwPoint3d& posCenter,	// O: the cut center in positive condyle
	IwPoint3d& negCenter	// O: the cut center in negative condyle
	)
{
	//DetermineFeaturePoints();

	if (m_FeaturePoints.GetSize() != 21) return false;

	posCenter = 0.5*(m_FeaturePoints.GetAt(6) + m_FeaturePoints.GetAt(7)); // 6th & 7th feature points
	negCenter = 0.5*(m_FeaturePoints.GetAt(8) + m_FeaturePoints.GetAt(9)); // 8th & 9th feature points

	return true;
}

///////////////////////////////////////////////////////////
bool CFemoralCuts::GetCondylarCutWidths
	(
	double& posWidth,	// O: the cut width in positive condyle
	double& negWidth	// O: the cut width in negative condyle
	)
{
	//DetermineFeaturePoints();

	if (m_FeaturePoints.GetSize() != 21) return false;

	posWidth =/* m_FeaturePoints.GetAt(6).DistanceBetween(m_FeaturePoints.GetAt(7));*/ 0.5*(m_FeaturePoints.GetAt(2).DistanceBetween(m_FeaturePoints.GetAt(3)) + m_FeaturePoints.GetAt(6).DistanceBetween(m_FeaturePoints.GetAt(7))); // 2nd & 3rd & 6th & 7th feature points
	negWidth = /*m_FeaturePoints.GetAt(8).DistanceBetween(m_FeaturePoints.GetAt(9));*/ 0.5*(m_FeaturePoints.GetAt(4).DistanceBetween(m_FeaturePoints.GetAt(5)) + m_FeaturePoints.GetAt(8).DistanceBetween(m_FeaturePoints.GetAt(9))); // 4th & 5th & 8th & 9th feature points

	return true;
}
void CFemoralCuts::SetDisplayOffsetViewProfile(bool flag)
{
	m_bOffsetViewProfileDisplay = flag;

	return;

}

void CFemoralCuts::SetDisplayOuterSideIntersectionCurves(bool flag)
{
	m_bOuterSideSurfaceIntersectionDisplay = flag;

	return;

}

void CFemoralCuts::SetOuterSideIntersectionCurves(IwTArray<IwCurve*>& intersectCurves)
{
	// empty the data
	for (unsigned i=0; i<m_outerSideIntersectionCurves.GetSize(); i++)
	{
		IwCurve* crv = m_outerSideIntersectionCurves.GetAt(i);
		IwObjDelete delCrv(crv);
	}
	m_outerSideIntersectionCurves.RemoveAll();

	m_outerSideIntersectionCurves.Append(intersectCurves);

	m_bOuterSideSurfaceIntersectionModified = true; // set modified such that to update the curve display list

}

void CFemoralCuts::GetOuterSideIntersectionCurves(IwTArray<IwCurve*>& intersectCurves)
{
	intersectCurves.RemoveAll();

	intersectCurves.Append(m_outerSideIntersectionCurves);

}

void CFemoralCuts::SetThicknessCurves(IwTArray<IwCurve*>& thicknessCurves)
{
	// empty the data
	for (unsigned i=0; i<m_thicknessCurves.GetSize(); i++)
	{
		IwCurve* crv = m_thicknessCurves.GetAt(i);
		IwObjDelete delCrv(crv);
	}
	m_thicknessCurves.RemoveAll();

	m_thicknessCurves.Append(thicknessCurves);

	m_bOuterSideSurfaceIntersectionModified = true; // set modified such that to update the curve display list. Use m_bOuterSideSurfaceIntersectionModified is correct.

}

void CFemoralCuts::GetThicknessCurves(IwTArray<IwCurve*>& thicknessCurves)
{
	thicknessCurves.RemoveAll();

	thicknessCurves.Append(m_thicknessCurves);

}

void CFemoralCuts::SetThicknessCurvesNotch(IwTArray<IwCurve*>& thicknessCurvesNotch)
{
	// empty the data
	for (unsigned i=0; i<m_thicknessCurvesNotch.GetSize(); i++)
	{
		IwCurve* crv = m_thicknessCurvesNotch.GetAt(i);
		IwObjDelete delCrv(crv);
	}
	m_thicknessCurvesNotch.RemoveAll();

	m_thicknessCurvesNotch.Append(thicknessCurvesNotch);

	m_bOuterSideSurfaceIntersectionModified = true; // set modified such that to update the curve display list. Use m_bOuterSideSurfaceIntersectionModified is correct.

}

void CFemoralCuts::GetThicknessCurvesNotch(IwTArray<IwCurve*>& thicknessCurvesNotch)
{
	thicknessCurvesNotch.RemoveAll();

	thicknessCurvesNotch.Append(m_thicknessCurvesNotch);

}

void CFemoralCuts::GetSelectableEntities
	(
	IwTArray<IwBrep*>& breps,		// O:
	IwTArray<IwCurve*>& curves,		// O:
	IwTArray<IwPoint3d>& points		// O:
	)
{
	if (m_bOuterSideSurfaceIntersectionDisplay)
	{
		curves.Append(m_outerSideIntersectionCurves);
		curves.Append(m_cutEdgeCurves);
	}

	// 
	CPart::GetSelectableEntities(breps, curves, points);

}

void CFemoralCuts::SetOffsetViewProfileModifiedFlag(bool flag)
{
	m_bOffsetViewProfileModified = flag;
	if (m_bOffsetViewProfileModified)
	{
		m_bFCDataModified = true;
		m_pDoc->SetModified(m_bOffsetViewProfileModified);
	}
}

void CFemoralCuts::SetOuterSideSurfaceIntersectionModifiedFlag(bool flag)
{
	m_bOuterSideSurfaceIntersectionModified = flag;
	if (m_bOuterSideSurfaceIntersectionModified)
	{
		m_bFCDataModified = true;
		m_pDoc->SetModified(m_bOuterSideSurfaceIntersectionModified);
	}
}

void CFemoralCuts::SetCutEdgesModifiedFlag(bool flag)
{
	m_bCutEdgesModified = flag;
	if (m_bCutEdgesModified)
	{
		m_bFCDataModified = true;
		m_pDoc->SetModified(m_bCutEdgesModified);
	}
}

void CFemoralCuts::GetCutEdgeCurves
	(
	IwTArray<IwCurve*>& cutEdgeCurves
	)
{
	cutEdgeCurves.RemoveAll();
	if ( m_cutEdgeCurves.GetSize() == 0 )
		UpdateCutEdges();

	cutEdgeCurves.Append(m_cutEdgeCurves);
}

bool CFemoralCuts::GetCutEdgeAngles
	(
	double& A0,		// O: angle between ant/ant chamfer cut
	double& A1,		// O: angle between ant chamfer cut / distal cut
	double& posA2,	// O: angle between distal cut / 1st posterior chamfer cut in positive side 
	double& posA3,  // O: angle between 1st / 2nd posterior chamfer cuts in positive side 
	double& posA4,	// O: angle between 2nd posterior chamfer / posterior cuts in positive side
	double& negA2,	// O: angle between distal cut / 1st posterior chamfer cut in negative side 
	double& negA3,	// O: angle between 1st / 2nd posterior chamfer cuts in negative side
	double& negA4	// O: angle between 2nd posterior chamfer / posterior cuts in negative side
	)
{
	if ( m_cutOrientations.GetSize() != 11 )
		return false;

	// Determine the angle 
	// Anterior cut edge angle
	double antAngleRadian;
	m_cutOrientations.GetAt(8).AngleBetween(m_cutOrientations.GetAt(9), antAngleRadian);
	// Anterior chamfer
	double antChanferAngleRadian;
	m_cutOrientations.GetAt(6).AngleBetween(m_cutOrientations.GetAt(8), antChanferAngleRadian);

	// positive side
	double posDistalPostAngleRadian;
	m_cutOrientations.GetAt(6).AngleBetween(m_cutOrientations.GetAt(4), posDistalPostAngleRadian);
	double posPostChamferAngleRadian;
	m_cutOrientations.GetAt(4).AngleBetween(m_cutOrientations.GetAt(2), posPostChamferAngleRadian);
	double posPostAngleRadian;
	m_cutOrientations.GetAt(2).AngleBetween(m_cutOrientations.GetAt(0), posPostAngleRadian);

	// negative side
	double negDistalPostAngleRadian;
	m_cutOrientations.GetAt(7).AngleBetween(m_cutOrientations.GetAt(5), negDistalPostAngleRadian);
	double negPostChamferAngleRadian;
	m_cutOrientations.GetAt(5).AngleBetween(m_cutOrientations.GetAt(3), negPostChamferAngleRadian);
	double negPostAngleRadian;
	m_cutOrientations.GetAt(3).AngleBetween(m_cutOrientations.GetAt(1), negPostAngleRadian);

	//
	A0 = antAngleRadian;
	A1 = antChanferAngleRadian;
	posA2 = posDistalPostAngleRadian;
	posA3 = posPostChamferAngleRadian;
	posA4 = posPostAngleRadian;
	negA2 = negDistalPostAngleRadian;
	negA3 = negPostChamferAngleRadian;
	negA4 = negPostAngleRadian;

	return true;
}

double CFemoralCuts::GetAnteriorFlangeRatio
	(
	IwPoint3d &antAntChamEdgePoint	// O: anterior / ant chamfer edge point
	)
{
	double flangeRatio = 0;
	CFemoralAxes* pFemoralAxes = GetTotalDoc()->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return flangeRatio;

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	//
	IwTArray<IwPoint3d> featurePoints, anteriorFeaturePoints;
	GetFeaturePoints(featurePoints, &anteriorFeaturePoints);
	if ( featurePoints.GetSize() > 0 && anteriorFeaturePoints.GetSize() == 9 )// have to be 9
	{
		IwAxis2Placement wslAxes;
		pFemoralAxes->GetWhiteSideLineAxes(wslAxes);
		IwVector3d vecLength;
		double antFlange, antChamFlange;
		vecLength = anteriorFeaturePoints.GetAt(4) - featurePoints.GetAt(18);
		vecLength = vecLength.ProjectToPlane(wslAxes.GetXAxis());
		antFlange = vecLength.Length();
		if ( positiveSideIsLateral )
			vecLength = featurePoints.GetAt(15) - featurePoints.GetAt(18);// medial side length
		else
			vecLength = featurePoints.GetAt(14) - featurePoints.GetAt(18);// medial side length
		vecLength = vecLength.ProjectToPlane(wslAxes.GetXAxis());
		antChamFlange = vecLength.Length();
		flangeRatio = antFlange/antChamFlange;
		//
		antAntChamEdgePoint = 0.5*featurePoints.GetAt(18) + 0.5*featurePoints.GetAt(19);
	}

	return flangeRatio;
}

bool  CFemoralCuts::MoveAnteriorCut(IwPoint3d refPoint)
{
	IwTArray<IwPoint3d> cutPoints;
	IwTArray<IwVector3d> cutOrientations;
	GetCutsInfo(cutPoints, cutOrientations);
	if ( cutPoints.GetSize() < 10 )
		return false;

	IwPoint3d antCutPoint = cutPoints.GetAt(9);
	IwVector3d antCutOrientation = cutOrientations.GetAt(9);

	antCutPoint = antCutPoint.ProjectPointToPlane(refPoint, antCutOrientation);
	cutPoints.SetAt(9, antCutPoint);
	SetCutsInfoOnly(cutPoints, cutOrientations);

	return true;
}

void CFemoralCuts::GetCutsDelta
	(
	double& distalDelta, 
	double& postChamferCutDelta1, 
	double& postChamferCutDelta2, 
	double& posteriorDelta
	)
{
	distalDelta = -1;
	postChamferCutDelta1 = -1;
	postChamferCutDelta2 = -1;
	posteriorDelta = -1;

	IwTArray<IwPoint3d> cutPoints;
	IwTArray<IwVector3d> cutOrientations;
	GetCutsInfo(cutPoints, cutOrientations);
	if ( cutPoints.GetSize() < 10 )
		return;

	// distalDelta
	IwVector3d tempVec = cutPoints.GetAt(6) - cutPoints.GetAt(7);
	distalDelta = tempVec.Dot(cutOrientations.GetAt(6));
	distalDelta = fabs(distalDelta);
	// posteriorDelta
	tempVec = cutPoints.GetAt(0) - cutPoints.GetAt(1);
	posteriorDelta = tempVec.Dot(cutOrientations.GetAt(0));
	posteriorDelta = fabs(posteriorDelta);

	// need cut edges for postChamferCutDelta
	if (m_cutEdgeCurves.GetSize() < 6)
		return;
	// postChamferCutDelta2 - the distance from posterior chamfer cut face2 to the 2 edges of post cham cut face3
	// 2 edges for face3
	IwCurve* edgeCurve31 = m_cutEdgeCurves.GetAt(1); // See UpdateCutEdges() for details.
	IwCurve* edgeCurve32 = m_cutEdgeCurves.GetAt(3);
	// get end points
	IwPoint3d stPnt31, endPnt31, stPnt32, endPnt32; 
	edgeCurve31->GetEnds(stPnt31, endPnt31);
	edgeCurve32->GetEnds(stPnt32, endPnt32);
	// Distance to face2
	tempVec = endPnt31 - cutPoints.GetAt(2);
	double dist21 = fabs(tempVec.Dot(cutOrientations.GetAt(2)));
	tempVec = endPnt32 - cutPoints.GetAt(2);
	double dist22 = fabs(tempVec.Dot(cutOrientations.GetAt(2)));
	postChamferCutDelta2 = min(dist21, dist22);

	// postChamferCutDelta1 - the distance from posterior chamfer cut face4 to the 2 edges of post cham cut face5
	// 2 edges for face5
	IwCurve* edgeCurve51 = m_cutEdgeCurves.GetAt(3);
	IwCurve* edgeCurve52 = m_cutEdgeCurves.GetAt(5);
	// get end points
	IwPoint3d stPnt51, endPnt51, stPnt52, endPnt52; 
	edgeCurve51->GetEnds(stPnt51, endPnt51);
	edgeCurve52->GetEnds(stPnt52, endPnt52);
	// Distance to face4
	tempVec = endPnt51 - cutPoints.GetAt(4);
	double dist41 = fabs(tempVec.Dot(cutOrientations.GetAt(4)));
	tempVec = endPnt52 - cutPoints.GetAt(4);
	double dist42 = fabs(tempVec.Dot(cutOrientations.GetAt(4)));
	postChamferCutDelta1 = min(dist41, dist42);

	return;
}