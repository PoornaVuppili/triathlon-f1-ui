#include <qfile.h>
#include "STLReader.h"
#include "Part.h"


CSTLReader::CSTLReader( CPart* pPart, const QString& sFileName )
	{
	m_sFileName	= sFileName;
	m_pPart	= pPart;
	}


CSTLReader::~CSTLReader()
	{
	}


bool CSTLReader::LoadFile()
	{ 
	QFile				File( m_sFileName );
	QString				status;
	long				lFileLength, nTotalFaces;
	int					iBlock, nBlocks, iFace, nFaces;
	CBounds3d			Bounds;
	const int			BUFFER_SIZE = sizeof( CSTLFace) * N_FACETS_IN_BUFFER;
	CSTLFace			FaceBuf[ N_FACETS_IN_BUFFER ];
	CSTLFace*			pSTLFace;
	bool				bLoadedOK = true;
	CFacet				Facet;
	bool				bOK;

	// Open the real file from hard drive
	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	// Take its length
	lFileLength = File.size();

	File.seek( HEADER_SIZE ); // jump past header
	nTotalFaces = ( lFileLength - HEADER_SIZE ) / FACET_SIZE;

	// **************  Read the file first time to get bounding  ***************
	nBlocks = nTotalFaces / N_FACETS_IN_BUFFER + 1;

	for	( iBlock = 0; iBlock < nBlocks; iBlock++ )
		{
		nFaces = File.read( (char*) FaceBuf, BUFFER_SIZE ) / sizeof( CSTLFace );

		for( iFace = 0; iFace < nFaces; iFace++ )
			{
			// Update bounding
			pSTLFace = FaceBuf + iFace;

			Facet.points[0] = IwPoint3d(pSTLFace->pt[0][0], pSTLFace->pt[0][1],pSTLFace->pt[0][2]);
			Facet.points[1] = IwPoint3d(pSTLFace->pt[1][0], pSTLFace->pt[1][1],pSTLFace->pt[1][2]);
			Facet.points[2] = IwPoint3d(pSTLFace->pt[2][0], pSTLFace->pt[2][1],pSTLFace->pt[2][2]);

			m_pPart->AddFacet( Facet );

			Bounds.Expand( pSTLFace->pt[0] );
			Bounds.Expand( pSTLFace->pt[1] );
			Bounds.Expand( pSTLFace->pt[2] );
			}
		}

	File.close();

	m_pPart->UpdateBounds( Bounds );

	return true;
	}

