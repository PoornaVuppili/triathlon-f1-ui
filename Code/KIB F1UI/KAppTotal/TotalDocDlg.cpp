#include <windows.h>
#include "TotalDocDlg.h"
#include "QDirModel.h"
#include "QSortFilterProxyModel"
#include "QDialogButtonBox"
#include "QApplication.h"
#include "QDesktopWidget.h"
#include "Qurl.h"
#include "QSplitter.h"
#include "InfoTextReader.h"


CTotalDocDlg::CTotalDocDlg( QWidget* parent, int iOpenMode, const QString& sDirectory, const QString& filter ) :
				QFileDialog( parent, Qt::Widget )
{
	show();
	setFileMode( AnyFile );

	switch( iOpenMode )
		{
		case LOAD_DOC:	
			setAcceptMode( AcceptOpen );
			setWindowTitle( "Load TriathlonF1 Document" );
			break;

		case SAVE_DOC:	
			setAcceptMode( AcceptSave );	
			setWindowTitle( "Save TriathlonF1 Document" );
			break;
		}

	setDirectory( sDirectory );
	
	
	//setNameFilter( filter ); 

	setViewMode(QFileDialog::List);

	setIconProvider( new CTotalDocIconProvider );

	QObjectList	childs = children();

	int n = childs.count();

	for( int i = 0; i < n; i++ )
	{
		QObject*	pChild = childs[i];

		QString sName = pChild->objectName();

		if( pChild->objectName() == "fileNameEdit" )
			m_pLineEdit = (QLineEdit*) pChild;

		if( pChild->objectName() == "splitter" )
		{
			QSplitter*		pSplitter = (QSplitter*) pChild;
			QList< int >	sizes;
			int				width;

			width = pSplitter->width();

			sizes.append( 0 );
			sizes.append( width );

			pSplitter->setSizes( sizes );
		}

		if( pChild->objectName() == "buttonBox" )
		{
			QDialogButtonBox*					pButtonBox = (QDialogButtonBox*) pChild;
			QDialogButtonBox::StandardButton	eAcceptButton;

			switch( iOpenMode )
			{
				case LOAD_DOC:	eAcceptButton = QDialogButtonBox::Open;		break;
				case SAVE_DOC:	eAcceptButton = QDialogButtonBox::Save;		break;
			}

			m_pAcceptButton = pButtonBox->button( eAcceptButton );
		}
	}

	connect( this, SIGNAL( directoryEntered( const QString& ) ), 
			 this, SLOT( OnDirEntered( const QString& ) ) );

	connect( this, SIGNAL( currentChanged( const QString& ) ),
			 this, SLOT( OnCurrentChanged( const QString& ) ) );

	connect( m_pAcceptButton, SIGNAL( clicked() ), this, SLOT( OnAccept() ) );

	connect( m_pLineEdit, SIGNAL( returnPressed() ), this, SLOT( OnAccept() ) );

}

	
void CTotalDocDlg::OnCurrentChanged( const QString& sPathName )
{
	QString		sFileName = "";
	if( sPathName.endsWith( ".tw" ) || sPathName.endsWith( ".tb" ) )
	{
		int	idx1 = sPathName.lastIndexOf( "/" );
		int	idx2 = sPathName.lastIndexOf( "\\" );
		int	idx  = max( idx1, idx2 );
		
		sFileName = sPathName.mid( idx + 1 );
	}
	else
	{
		// search for the _info.txt file
		QString infoTxtFileName;
		QDir fileDir(sPathName);
		QStringList fileList = fileDir.entryList(QDir::Files | QDir::Hidden);
		for (int i=0; i<fileList.count(); i++)
		{
			QString fileName(sPathName + QDir::separator() + fileList.at(i));
			if ( fileList.at(i).endsWith("_info.txt") )
			{
				infoTxtFileName = sPathName + QDir::separator() + fileList.at(i);
				break;
			}
		}

		// if _info.txt file exists
		if ( !infoTxtFileName.isEmpty() )
		{
			QFile			infoFile(infoTxtFileName);

			CInfoTextReader infoReader(infoTxtFileName);
		
			// if it is an TriathlonF1 case
			if ( infoReader.IsFileLoaded() && 
                (infoReader.GetString("ImplantType") == QString ("TriathlonF1") || 
                 infoReader.GetString("ImplantType") == QString ("TriathlonF1PS")) )
			{
				QString subDirectory = sPathName + QDir::separator() + QString("SolidWorks") /*+ QDir::separator() + QString("Femur")*/;
				QDir femurDir(subDirectory);
				if ( femurDir.exists() )
					setDirectory( subDirectory );
			}
	
		}

	}

	m_pLineEdit->setText( sFileName );
}


void CTotalDocDlg::OnDirEntered( const QString& directory ) 
{
	if( directory.endsWith( ".tw" ) || directory.endsWith( ".tb" ) )
		QDialog::accept();
}


void CTotalDocDlg::OnAccept()
{
	QStringList		list = selectedFiles();
	QString			sFileName = list[0];

	if( sFileName.endsWith( ".tw" ) || sFileName.endsWith( ".tb" ) )
		QDialog::accept();
}


void CTotalDocDlg::CenterWindow()
{
	int W = QApplication::desktop()->width();
	int H = QApplication::desktop()->height();

	int w = width();
	int h = height();

	int x = ( W - w ) / 2.0;
	int y = ( H - h ) / 2.0;

	setGeometry( x, y, w, h );
}


CTotalDocIconProvider::CTotalDocIconProvider() : QFileIconProvider()
{
}

	
QIcon CTotalDocIconProvider::icon( const QFileInfo& info ) const
{
	QString		sFileName = info.fileName();

	if( sFileName.endsWith( ".tw" ) || sFileName.endsWith( ".tb" ) )
		return QIcon( ":/Resources/TriathlonF1.png" );

	return QFileIconProvider::icon( info );
}


QString CTotalDocIconProvider::type( const QFileInfo& info ) const
{
	QString		sFileName = info.fileName();

	if( info.isDir() && (sFileName.endsWith( ".tw" )||sFileName.endsWith( ".tb" )) )
		return "TriathlonF1 Document";

	return QFileIconProvider::type( info );
}


CTotalDocProxyModel::CTotalDocProxyModel( QObject* parent ) : QSortFilterProxyModel( parent )
{
}


bool CTotalDocProxyModel::filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const
{
	QModelIndex index0 = sourceModel()->index( sourceRow, 0, sourceParent );
	QModelIndex index1 = sourceModel()->index( sourceRow, 1, sourceParent );
	QModelIndex index2 = sourceModel()->index( sourceRow, 2, sourceParent );

	QString		str0 = sourceModel()->data( index0 ).toString();
	QString		str1 = sourceModel()->data( index1 ).toString();
	QString		str2 = sourceModel()->data( index2 ).toString();

	if( str2 == "TriathlonF1 Document"	|| 
		str2 == "File Folder"			||
		str2 == "Local Disk"			||
		str2 == "Drive"					||
		str2 == "System Folder"			||
		str2 == "Network Drive"			)
		return true;

	return false;
}


bool CTotalDocProxyModel::filterAcceptsColumn( int source_column, const QModelIndex& source_parent ) const
{
	if( source_column == 1 )
		return false;

	return true;
}

