#include <qpushbutton.h>
#include <qfiledialog.h>
#include <qbuttongroup.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qstring.h>
#include <qgridlayout.h>
#include <qtablewidget.h>
#include <qheaderview.h>
#include <QDateTime.h>

#include "FileNewDlg.h"
#include "InfoTextReader.h"
#include "Globals.h"

CFileNewDlg::CFileNewDlg( QWidget * parent ) : QDialog( parent )
{
	QStringList				labels;
	QString					sServerDir;

	setWindowTitle( "Select case" );

	QVBoxLayout*		layoutMain = new QVBoxLayout;

	QHBoxLayout*		layoutLists = new QHBoxLayout;	
	QHBoxLayout*		layoutSearch = new QHBoxLayout;
	QHBoxLayout*		layoutButtons = new QHBoxLayout;

	m_listCases			= new QTableWidget( 0, 3, this );

	QLabel* labelSearch = new QLabel(tr("Search case number:"));
	m_lineEditSearch	= new QLineEdit("");

	m_buttonOK			= new QPushButton( "OK" );
	m_buttonCancel		= new QPushButton( "Cancel" );

	m_lineEditSearch->setReadOnly(false);
	m_lineEditSearch->setFixedWidth(96);

	m_buttonOK->setFixedWidth( 80 );
	m_buttonCancel->setFixedWidth( 80 );
 
	layoutLists->addWidget( m_listCases, 1 );

	layoutSearch->addWidget(labelSearch);
	layoutSearch->addWidget(m_lineEditSearch);
	
	layoutButtons->addWidget( m_buttonOK );
	layoutButtons->addSpacing( 10 );
	layoutButtons->addWidget( m_buttonCancel );

	layoutMain->addLayout( layoutLists );
	layoutMain->addSpacing( 10 );
	layoutMain->addLayout( layoutSearch );
	layoutMain->addSpacing( 10 );
	layoutMain->addLayout( layoutButtons );

	setLayout( layoutMain );
	setFixedWidth( 360 );
	setMinimumHeight( 400 );

	labels.clear();
	labels << "Case" << "Date" << "Side";
	m_listCases->setHorizontalHeaderLabels( labels );

	m_listCases->horizontalHeader()->setSectionResizeMode( QHeaderView::Stretch );

	m_listCases->verticalHeader()->hide();

	m_listCases->setSelectionBehavior( QAbstractItemView::SelectRows );

	m_buttonOK->setEnabled( false );

	connect( m_listCases, SIGNAL( itemSelectionChanged() ), this, SLOT( OnCaseChanged() ) );
	connect( m_listCases, SIGNAL( itemDoubleClicked(QTableWidgetItem *) ), this, SLOT( OnItemDoubleClicked(QTableWidgetItem *) ) );

	connect( m_lineEditSearch, SIGNAL( textEdited(const QString&) ), this, SLOT( OnSearchEdited(const QString&) ) );

	connect( m_buttonOK, SIGNAL( clicked() ), this, SLOT( OnOK() ) );
	connect( m_buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );

	sServerDir = QString("X:");

	m_sUNIDir = sServerDir + "\\uni";

	FillCasesLists( m_sUNIDir );

}

void CFileNewDlg::FillCasesLists( const QString& sDir )
{
	QDir					dir( sDir );
	QTableWidgetItem*		itemName;
	QTableWidgetItem*		itemDate;
	QTableWidgetItem*		itemSide;
	int						row;

	m_listCases->setRowCount( 0 );

	foreach( QFileInfo finfo, dir.entryInfoList( QDir::Dirs ) )
	{
		if ( !finfo.isDir() ) continue;

		QString			sName = finfo.baseName();
		QString			infoFileName = finfo.filePath() + QDir::separator() + sName + "_info.txt";
		QFile			infoFile(infoFileName);

		if ( !infoFile.exists() ) continue;

		CInfoTextReader infoReader(infoFileName);
		if ( !infoReader.IsFileLoaded() ) continue;
		
		if ( infoReader.GetString("ImplantType") == QString ("TriathlonF1") ||
             infoReader.GetString("ImplantType") == QString ("TriathlonF1PS") )
		{
			QString			sDate = finfo.lastModified().toString( "MM-dd-yyyy" );

			itemName = new QTableWidgetItem;
			itemDate = new QTableWidgetItem;
			itemSide = new QTableWidgetItem;

			itemName->setData( 0, sName );
			itemName->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			itemDate->setData( 0, sDate );
			itemDate->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			itemSide->setData( 0, infoReader.GetString("Knee") );
			itemSide->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

			row = m_listCases->rowCount();

			m_listCases->insertRow( row );
			m_listCases->setRowHeight( row, 18 );
			
			m_listCases->setItem( row, 0, itemName );		
			m_listCases->setItem( row, 1, itemDate );	
			m_listCases->setItem( row, 2, itemSide );
		}
	}
}

QString CFileNewDlg::GetSelectedCaseDir()
{
	QString					sDir, sCase, sCaseDir;

	sDir = m_sUNIDir;

	QList<QTableWidgetItem*> selectList = m_listCases->selectedItems();

	sCase = selectList[0]->text();

	sCaseDir = sDir + "\\" + sCase;

	return sCaseDir;

}

void CFileNewDlg::OnSearchEdited(const QString& txt)
{
	// deselect, if any.
	QList<QTableWidgetItem*> selectList = m_listCases->selectedItems();
	if ( selectList.size() > 0 )
	{
		m_listCases->setCurrentItem(selectList[0], QItemSelectionModel::Deselect);
		m_listCases->setCurrentItem(selectList[1], QItemSelectionModel::Deselect);
		m_listCases->setCurrentItem(selectList[2], QItemSelectionModel::Deselect);
	}
	// scroll the item to the top
	QString searchText = txt;
	while (searchText.startsWith("0"))// ignore all "0"
	{
		searchText = searchText.remove(0, 1);
	}
	if ( searchText.isEmpty() )
		return;

	QTableWidgetItem* item;
	QString itemText;
	for (int row=0; row<m_listCases->rowCount(); row++)
	{
		item = m_listCases->item(row, 0);
		itemText = item->text();
		while (itemText.startsWith("0"))// ignore all "0"
		{
			itemText = itemText.remove(0, 1);
		}
		if ( !itemText.isEmpty() )
		{
			if ( itemText.startsWith(searchText) )
			{
				m_listCases->scrollToItem(item, QAbstractItemView::PositionAtTop);
				break;
			}
		}
	}

}

void CFileNewDlg::OnCaseChanged()
{
	QList<QTableWidgetItem*> selectList = m_listCases->selectedItems();
	if ( selectList.size() > 0 )
	{
		m_buttonOK->setEnabled( true );
		m_lineEditSearch->setText(selectList[0]->text());
	}
	else
	{
		m_buttonOK->setEnabled( false );
		m_lineEditSearch->clear();
	}

	
}

void  CFileNewDlg::OnItemDoubleClicked(QTableWidgetItem *)
{
	OnOK();
}

void CFileNewDlg::OnOK()
{
	accept();
}
