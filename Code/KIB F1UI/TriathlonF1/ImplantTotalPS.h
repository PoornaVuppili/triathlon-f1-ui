#ifndef IMPLANTPS_H
#define IMPLANTPS_H

// Implant information
#include <QObject>
#include <QImage>
#include <QDomDocument>
#include <QDateTime>

#include "..\KApp\Implant.h"
class SelectImplant;
class SelectImplantSimple;
class Module;

class CTotalMainWindow;
class KTotalMainWindowPS;
class QMenu;

#include <map>

class ImplantTotalPS : public Implant
{
    Q_OBJECT

public:
    ImplantTotalPS(QObject *parent=NULL);

    // Initialize modules. If the module has menus, they are inserted before the menu 'before' if not NULL.
    // Otherwise, they are added at the end. If the module has toolbars, they are added to the main toolbar.
    virtual bool InitializeModules(QMenu* before=NULL);
    // Release modules
    virtual void ReleaseModules();

    // Get the version info of all modules
    virtual QStringList GetModuleVersionInfo();

    // Load the implant
    //virtual bool LoadImplant(SelectImplant* si) { return false; }
    virtual bool LoadImplant(SelectImplant* si);
	virtual bool LoadImplant(SelectImplantSimple* sis);

    // Save the implant
    virtual bool SaveImplant();

    // Save the Rework implant
    virtual bool SaveReworkImplant();

    // Whether an implant is open
    virtual bool HasImplant() const { return !implantName.isEmpty(); }

    // Whether the implant is changed
    virtual bool IsImplantChanged() const;

    // Get the implant info by name
    virtual QString GetInfo(const QString& name) const;

    // Get the module by name
    virtual Module* GetModule(const QString& name);

    // Enable/disable actions of menus and toolbars
    virtual void EnableActions(bool enable);

    // Set options
    void SetOption();

	void StartProfileReview();

public slots:
    void OnDesignStageCommenced(Module*);
    void OnDesignStageCommenced(Module*, int);
    void OnDesignStageCompleted(Module*);
    void OnDesignStageCompleted(Module*, int);

protected:
    // clear the module content
    virtual void ClearModule();

    // modules
	KTotalMainWindowPS* totalPS;
    CTotalMainWindow*   femBasis; // those are proxies
    CTotalMainWindow*   femJigs;

    // implant information
    // - implant serial #
    QString implantName;

    // - implant container Name #
    QString implantContainerName;//PSV---

    // - implant type: iUni, iDuo or TriathlonF1
    QString implantType;
    // - knee type: Left or Right
    QString kneeType;
    // - compartment: Medial or Lateral (for TriathlonF1 only)
    QString compartmentType;
    // - country code and regulartory zone 
    QString country, zone;
    // - names of subfolders of an implant
    QString implantDir,imgDir,segDir,surfDir,stlDir,sldworksDir;
    // - implant status file
    QString implantStatusFile;
    // - template folder
    QString templateFolder;
    
    // Status document
    QDomDocument docStatus;
    // User name
    QDomElement nameElement;
    // Previous Users
    QDomElement userHistory; // potentially (even likely) empty
   
    // Status of total ps items
    QDomElement totalPSElement;
   
    // Status of view
    QDomElement viewElement;

    // Status of entity view (left-hand side panel)
    QDomElement treeElement;

    // actual overrider for creating configuration (connects table name and file name)
    Configuration* createConfig(QString const &tableName) override;

    virtual void HideAll();
    virtual void HideAllExcept(Module* pMod);

    enum eDesignStage { NOTYET, FEMUR_BASIS, FEM_JIGS };
    eDesignStage m_designStage;
    std::multimap<Module*,eDesignStage> m_designStageByModule;

    bool AllStepsComplete(std::string const &phase, std::string const &subphase) const override;
    virtual int CurrentStage(QString *name = nullptr) const override;

    bool OutputResults(std::string const &phase, std::string const &subphase) const override;

    void SetDesignStage(eDesignStage s);
    void MoveToNextDesignStage();

    bool IsInFemurBasis() const { return m_designStage == FEMUR_BASIS; }
    bool IsInFemJigs() const { return m_designStage == FEM_JIGS; }

    struct User
    {
        QString name;
        QString workstation;
        QDateTime birth; // when the object was created (or read from file)
        int exmode;      // from KApp/Implant.h : 1 = dev, 2 = beta, 3 = prod
        QString version; // what version the user is (or was, if read,) using
    };

    std::vector<User> uHistory;

    void parseUserHistory(); // parses the 'userHistory' QDomElement and fills 'uHistory' member
};

#endif // IMPLANTPS_H
