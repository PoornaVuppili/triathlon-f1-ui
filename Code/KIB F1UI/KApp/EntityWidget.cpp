#include "EntityWidget.h"
#include "Implant.h"
#include "EntityItem.h"
#include "ViewWidget.h"

#include "../KUtility/KXmlHelper.h"

#include <QMessageBox>
#include <QResizeEvent>

#include <QModelIndex>

// global instance
EntityWidget* entityWidget = NULL;

EntityWidget::EntityWidget(QWidget *parent)
    : QTreeView(parent)
{
    setModel(&model);

    model.setColumnCount(2);
    model.setHorizontalHeaderLabels( QStringList() << "Item" << "Setting" );

    connect(this, SIGNAL(clicked(const QModelIndex&)), this, SLOT(OnItemClicked(const QModelIndex&)));
    connect(this, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(OnItemDoubleClicked(const QModelIndex&)));
    connect(&model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(OnModelItemChanged(QStandardItem*)));
}

EntityWidget::~EntityWidget()
{
    ClearContent();
}

void EntityWidget::ClearContent()
{
    model.clear();
    items.clear();

    model.setColumnCount(2);
    model.setHorizontalHeaderLabels( QStringList() << "Item" << "Setting" );
}

// Add an item
void EntityWidget::AddItem(EntityItem* ei)
{
    int num = (int)items.size();
    items.push_back(ei);

    QList<QStandardItem*> its = ei->GetItems();
    model.appendRow( its );

    setExpanded(model.index(num, 0), true);
    if( its.size()==1 )
        setFirstColumnSpanned(num, QModelIndex(), true);
}

// Remove an item
void EntityWidget::RemoveItem(EntityItem* ei)
{
    for(int i=0; i<items.size(); i++)
    {
        if( items[i] == ei )
        {
            model.removeRow(i);
            items.erase( items.begin() + i );
            break;
        }
    }
}

// Check item exists or not
bool EntityWidget::IsItemExist(EntityItem* ei)
{
    for(int i=0; i<items.size(); i++)
    {
        if( items[i] == ei )
        {
            return true;
        }
    }
	return false;
}

void EntityWidget::AllowClose(bool canClose)
{
    for(int i=0; i<items.size(); i++)
        items[i]->AllowClose(canClose);
}

void EntityWidget::resizeEvent ( QResizeEvent * event )
{
    int s = event->size().width()*4/5;
    setColumnWidth(0, s);
    setColumnWidth(1, event->size().width()-s);

    QTreeView::resizeEvent(event);
}

// get the root row of the index
int EntityWidget::GetRootRow(const QModelIndex& index)
{
    QModelIndex idx, rootIndex = index;
    while( (idx = rootIndex.parent()) != QModelIndex() )
        rootIndex = idx;

    return rootIndex.row();
}

void EntityWidget::mousePressEvent ( QMouseEvent * event )
{
    EntityItem::ClickButton = event->buttons();

    if( EntityItem::ClickButton == Qt::RightButton )
    {
        // for right click (left-click is processed by OnItemClicked)
        QModelIndex index = indexAt(event->pos());
		int i = GetRootRow(index);
		if(i >= 0)
			items[ i ]->OnItemRightClicked(index);
    }

    QTreeView::mousePressEvent(event);
}

void EntityWidget::OnItemClicked(const QModelIndex& index)
{
    items[ GetRootRow(index) ]->OnItemClicked(index);
}

void EntityWidget::OnItemDoubleClicked(const QModelIndex& index)
{
    items[ GetRootRow(index) ]->OnItemDoubleClicked(index);
}

void EntityWidget::OnModelItemChanged(QStandardItem* item)
{
    items[ GetRootRow(item->index()) ]->OnModelItemChanged(item);

    viewWidget->update();
}

void EntityWidget::SetAllChecked(bool check)
{
	for(int i=0; i<model.rowCount(); i++)
		SetAllChecked(model.item(i), check);
}

void EntityWidget::SetAllChecked(QStandardItem *item, bool check)
{
	for(int i=0; i<item->rowCount(); i++)
		SetAllChecked(item->child(i), check);

	if(item->isCheckable())
		item->setCheckState(check ? Qt::Checked : Qt::Unchecked);
}

EntityItem const* findItemWithText(std::vector<EntityItem*> const& vi, QString const& name, QModelIndex &ind)
{
    int i = 0;
    for (auto it = vi.begin(); it != vi.end(); ++it, ++i)
    {
        QString itn;
        EntityItem const* ei = *it;
        if (ei->GetText() == name)
        {
            ind = ei->GetIndex();
            return ei;
        }
    }

    return nullptr;
}

// this function restores the entity widget's root items' "rolled up" or "rolled down" status
// ASSUMPTION: all visibility nodes in the 'element' refer to the existing items in this object
void EntityWidget::SetStatus(QDomElement elem)
{
    if (elem.tagName() =="Tree" && elem.attribute("Version") == "1.0" && elem.hasChildNodes())
    {
        auto kids = elem.childNodes();
        for (int ich = 0, nch = kids.count(); ich < nch; ++ich)
        {
            auto kid = kids.at(ich);
            QString name = kid.toElement().text();
            QModelIndex index;
            if (auto it = findItemWithText(items, name, index))
            {
                // found the item with the name
                auto nmap = kid.attributes();
                auto at = nmap.namedItem("Expanded");
                QString strExp = at.toAttr().value();
                if (strExp == "0")
                    this->collapse(index);
                else
                    this->expand(index);
            }
        }
    }
}

// this function collects the entity widget's root items' "rolled up" or "rolled down" status
// and encodes them in the subnodes of the 'element'
void EntityWidget::GetStatus(QDomElement elem)
{
    elem.setTagName("Tree");
    elem.setAttribute("Version", "1.0");

    // remove all child nodes
    while (elem.hasChildNodes())
        elem.removeChild(elem.lastChild());

    // for all items, collect the state, save it with the name (text) of the item
    for (int i = 0; i < items.size(); ++i)
    {
        QDomDocument doc = elem.ownerDocument();
        QDomElement itemElem = doc.createElement("Item");
        elem.appendChild(itemElem);
        itemElem.appendChild(doc.createTextNode(items[i]->GetText()));
        itemElem.setAttribute("Expanded", QString("%1").arg(isExpanded(items[i]->GetIndex())));
    }
}
