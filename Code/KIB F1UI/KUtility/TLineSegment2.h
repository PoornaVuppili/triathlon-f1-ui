#ifndef TLINESEGMENT2_H
#define TLINESEGMENT2_H

#include "TPoint2.h"
#include "TLine2.h"

template <typename T>
class TLineSegment2
{
public:
    ///////////////////////////////////////////////////////////////////////
    // Function name:    TLineSegment2
    // Function purpose: Constructor
    // Input: the coordinates of two points
    // Output: None
    ///////////////////////////////////////////////////////////////////////
    TLineSegment2() {}

    TLineSegment2(T x1, T y1, T x2, T y2)
    {
        p1.x() = x1;
        p1.y() = y1;
        p2.x() = x2;
        p2.y() = y2;
    }

    TLineSegment2(const TPoint2<T>& _p1, const TPoint2<T>& _p2)
    { 
        p1 = _p1;
        p2 = _p2;
    }

    // Get values
    TPoint2<T> GetP1() const { return p1; }
    TPoint2<T> GetP2() const { return p2; }

    // Set values
    void SetP1(T xv, T yv) { p1.x() = xv; p1.y() = yv; }
    void SetP2(T xv, T yv) { p2.x() = xv; p2.y() = yv; }
    void SetP1(const TPoint2<T>& p) { p1 = p; }
    void SetP2(const TPoint2<T>& p) { p2 = p; }

    // Test if two line segments are parallel
    bool IsParallel(const TLineSegment2<T>& ls) const
    {
        TLine2<T> l1(p1, p2);
        TLine2<T> l2(ls.GetP1(), ls.GetP2());
        return l1.IsParallel(l2);
    }

    // Test if two line segments are colinear
    bool IsColinear(const TLineSegment2<T>& ls) const
    {
        TLine2<T> l1(p1, p2);
        TLine2<T> l2(ls.GetP1(), ls.GetP2());
        return l1.IsColinear(l2);
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Distance
    // Function purpose: Find the distance from a point to a line segment
    // Input: the point coordinate
    // Output: None
    ///////////////////////////////////////////////////////////////////////
    T Distance(T x, T y)
    {
        // use double for computation internally, and convet to type T at return
        double x1 = p1.x();
        double y1 = p1.y();
        double x2 = p2.x();
        double y2 = p2.y();

        double a = x2 - x1;
        double b = y2 - y1;
        double v = 1 / (a*a + b*b);
        double k = - v * ( a*(x1 - x) + b*(y1 - y) );

        // the point with the smallest distance is on the line segment
        double tx,ty;
        if( k>=0 && k<=1 )
        {
            tx = x1 + k * a;
            ty = y1 + k * b;
            tx -= x;
            ty -= y;
            return (T)sqrt(tx*tx + ty*ty);
        }
        else
        {
            // otherwise return the smaller distance to the ends of line segment
            tx = x1 - x;
            ty = y1 - y;
            double td1 = sqrt(tx * tx + ty * ty);
            tx = x2 - x;
            ty = y2 - y;
            double td2 = sqrt(tx * tx + ty * ty);
            return (T)((td1<td2) ? td1 : td2);
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Intersect
    // Function purpose: Find the point of intersection with another line segment
    // Input:
    //   lineSeg: the other line segment
    // Output:
    //   intersectPoint: the point of intersection if exists
    // Return:
    //   an intersect flag
    // Note: when the line segments are coincident, whether they overlap or
    //   not is not addressed.
    ///////////////////////////////////////////////////////////////////////
    int Intersect(const TLineSegment2<T>& lineSeg, TPoint2<T>& intersectPoint, T tol = NumTraits<T>::dummy_precision()) const
    {
        // use double for computation internally, and convet to type T at return
        double cx = (double) p1.x();
        double cy = (double) p1.y();
        double dx = (double) p2.x() - cx;
        double dy = (double) p2.y() - cy;

        double xi = (double) lineSeg.GetP1().x();
        double yi = (double) lineSeg.GetP1().y();
        double xj = (double) lineSeg.GetP2().x();
        double yj = (double) lineSeg.GetP2().y();

        double ex = xj - xi;
        double ey = yj - yi;

        double denom = ey*dx -ex*dy;
        double na = ex*(cy - yi) - ey*(cx - xi);
        double nb = dx*(cy - yi) - dy*(cx - xi);

        if( fabs(denom)<tol )
        {
            // the lines are coincident or parallel
            if(fabs(na)<tol && fabs(nb)<tol )
                return IntersectFlag::Coincident;
            else
                return IntersectFlag::Parallel;
        }

        double ua = na / denom;
        double ub = nb / denom;

        if(ua >= -tol && ua <= 1.0+tol && ub >= -tol && ub <= 1.0+tol)
        {
            intersectPoint = TPoint2<T>((T)(cx + ua*dx), (T)(cy + ua*dy) );
            return IntersectFlag::Intersect;
        }
        else 
            return IntersectFlag::NonIntersect;
    }

protected:
    TPoint2<T> p1, p2;
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TLineSegment2<int> LineSegment2Int;
typedef TLineSegment2<float> LineSegment2Float;
typedef TLineSegment2<double> LineSegment2Double;

#endif //TLINESEGMENT2_H
