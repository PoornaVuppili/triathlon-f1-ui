/****************************************************************************
** Meta object code from reading C++ file 'TotalEntPanelJigs.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../TotalEntPanelJigs.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TotalEntPanelJigs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CTotalEntPanelJigs_t {
    QByteArrayData data[8];
    char stringdata0[113];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CTotalEntPanelJigs_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CTotalEntPanelJigs_t qt_meta_stringdata_CTotalEntPanelJigs = {
    {
QT_MOC_LITERAL(0, 0, 18), // "CTotalEntPanelJigs"
QT_MOC_LITERAL(1, 19, 10), // "OnRedefine"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 17), // "OnRegenerateAStep"
QT_MOC_LITERAL(4, 49, 25), // "OnRegenerateMultipleSteps"
QT_MOC_LITERAL(5, 75, 15), // "OnDeleteAllJigs"
QT_MOC_LITERAL(6, 91, 12), // "OnExportIges"
QT_MOC_LITERAL(7, 104, 8) // "OnDelete"

    },
    "CTotalEntPanelJigs\0OnRedefine\0\0"
    "OnRegenerateAStep\0OnRegenerateMultipleSteps\0"
    "OnDeleteAllJigs\0OnExportIges\0OnDelete"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CTotalEntPanelJigs[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x0a /* Public */,
       3,    0,   45,    2, 0x0a /* Public */,
       4,    0,   46,    2, 0x0a /* Public */,
       5,    0,   47,    2, 0x0a /* Public */,
       6,    0,   48,    2, 0x0a /* Public */,
       7,    0,   49,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CTotalEntPanelJigs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CTotalEntPanelJigs *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnRedefine(); break;
        case 1: _t->OnRegenerateAStep(); break;
        case 2: _t->OnRegenerateMultipleSteps(); break;
        case 3: _t->OnDeleteAllJigs(); break;
        case 4: _t->OnExportIges(); break;
        case 5: _t->OnDelete(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject CTotalEntPanelJigs::staticMetaObject = { {
    QMetaObject::SuperData::link<CEntPanel::staticMetaObject>(),
    qt_meta_stringdata_CTotalEntPanelJigs.data,
    qt_meta_data_CTotalEntPanelJigs,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CTotalEntPanelJigs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CTotalEntPanelJigs::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CTotalEntPanelJigs.stringdata0))
        return static_cast<void*>(this);
    return CEntPanel::qt_metacast(_clname);
}

int CTotalEntPanelJigs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CEntPanel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
