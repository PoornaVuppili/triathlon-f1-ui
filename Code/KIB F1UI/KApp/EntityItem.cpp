#include "EntityItem.h"

Qt::MouseButtons EntityItem::ClickButton = Qt::NoButton;

char EntityItem::modifiedCharacter()
{
   return '~';
}

char EntityItem::outOfDateCharacter()
{
   return '*';
}

QFont EntityItem::uNOTeditableFont; // regular
QFont EntityItem::ueditableFont;    // bold

int setEntityItemFonts(QFont &reg, QFont &bld)
{
   reg.setBold(false);
   bld.setBold(true);
   return 777;
}

int EntityItem::fontsCreatedFlag = setEntityItemFonts(EntityItem::uNOTeditableFont, EntityItem::ueditableFont);

EntityItem::EntityItem(QTreeView* view)
    : QObject()
    , treeView(view)
    , allowClose(false)       // default to not allowing to close
    , modified(false)
    , ueditable(true)
    , validated(eNeedsValidation)
    , uptodate(eUpToDateUnkown)
{
}

EntityItem::~EntityItem()
{
}

//  A helper function to create a standard item
QStandardItem* EntityItem::NewItem(const QString& txt, bool selectable, bool editable, bool checkable, Qt::CheckState state)
{
    QStandardItem* it = new QStandardItem(txt);

    it->setSelectable( selectable );
    it->setEditable( editable );
    it->setCheckable( checkable );
    if( checkable )
        it->setCheckState( state );

    return it;
}

// Common icons
QIcon& EntityItem::GetIcon_ShowIntersect()
{
    static QIcon icon(":/Resources/overlay12.png");
    return icon;
}

QIcon& EntityItem::GetIcon_HideIntersect()
{
    // icon for hiding the slice intersection
    static QIcon icon(":/Resources/overlay12_d.png");
    return icon;
}

void EntityItem::SetUpToDateStatusNoLowerThan(UpToDateStatus status)
{
    UpToDateStatus was = uptodate; uptodate = max(uptodate, status);
    if (was >= 0)
        SetModified(uptodate != was);
}

// Set the modified mark.
void EntityItem::SetModified(bool b)
{
    if( items.size()==0 || modified == b )
        return;

    modified = b;

    QString txt = items[0]->text();
    if( b )
    {
        if( txt.startsWith(modifiedCharacter()) )
            return;

        txt.prepend(modifiedCharacter());     // add the '*' at the front
    }
    else
    {
        if( !txt.startsWith(modifiedCharacter()) )
            return;

        txt.remove(0, 1);    // remove the '*' at the front
    }
    items[0]->setText(txt);
}

void EntityItem::SetValidateStatus(ValidatedStatus status)
{
   if (items.size() == 0 || validated == status) // no text or same status already
      return;

   validated = status;
}

void EntityItem::UpdateUI()
{
   // make sure that the color and text correspond to the flags
   if (items.size() == 0) // no text or same status already
      return;

   QString theText = items[0]->text();
   QColor darkRed(128, 0, 0), darkGreen(42, 127, 42), darkYellow(152, 128, 0), black(0,0,0);

   if (modified)
   {
      if (!theText.startsWith(modifiedCharacter()))
         theText.prepend(modifiedCharacter());
   }
   else if (uptodate > eUpToDate)
   {
      if (!theText.startsWith(outOfDateCharacter()))
         theText.prepend(outOfDateCharacter());
   }
   else // not modified, not out of date
   {
      while (theText.startsWith(modifiedCharacter()) || theText.startsWith(outOfDateCharacter()))
         theText.remove(0, 1); // remove the leading character
   }

   items[0]->setText(theText);

   // currently make the text different color
   if (validated == eNotValid )// red
      items[0]->setForeground( QBrush(darkRed) );
   else if ( validated == eValidatedWithWarnings ) // yellow
      items[0]->setForeground( QBrush(darkYellow) ); 
   else if ( validated == eValidatedOK) // green
      items[0]->setForeground( QBrush(darkGreen) );
   else	// black
      items[0]->setForeground( QBrush(black) );		

   if (ueditable)
      items[0]->setFont(ueditableFont);
   else
      items[0]->setFont(uNOTeditableFont);
}

//static
EntityItem::UpToDateStatus EntityItem::UpToDateStatusFromString(QString const & str)
{
   if (str == UpToDateStatusString(eUpToDate)) return eUpToDate;
   else if (str == UpToDateStatusString(eNeedsChecking)) return eNeedsChecking;
   else if (str == UpToDateStatusString(eNeedsEditing)) return eNeedsEditing;
   else return eUpToDateUnkown;
}

//static
QString EntityItem::UpToDateStatusString(EntityItem::UpToDateStatus upstat)
{
   switch (upstat)
   {
   case eUpToDate:
      return "eUpToDate";
   case eNeedsChecking:
      return "eNeedsChecking";
   case eNeedsEditing:
      return "eNeedsEditing";
   case eUpToDateUnkown:
   default:
      return "eUpToDateUnkown";
   }
}


// find a row for sub-item a few levels below this one
int EntityItem::GetDescendantRow(QStandardItem* si)
{
	if( !items.isEmpty() )
	{
		for( QModelIndex idx = si->index(); idx.parent() != QModelIndex(); idx = idx.parent() )
		{
			if( idx.parent() == items[0]->index() )
				return idx.row();
		}
	}

	return -1;
}

// find a row for sub-item a few levels below this one
int EntityItem::GetDescendantRow(const QModelIndex& index)
{
	if( !items.isEmpty() )
	{
		for( QModelIndex idx = index; idx.parent() != QModelIndex(); idx = idx.parent() )
		{
			if( idx.parent() == items[0]->index() )
				return idx.row();
		}
	}

	return -1;
}

QString EntityItem::GetText() const
{
    QString rv;
    if (!items.isEmpty())
        rv = items[0]->text();
    while (rv.startsWith(modifiedCharacter()))
        rv.remove(0,1);
    return rv;
}
