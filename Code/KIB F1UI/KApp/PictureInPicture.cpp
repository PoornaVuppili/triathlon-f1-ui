#include "PictureInPicture.h"
#include "Viewport.h"
#include <IwVector3d.h>
#include <IwExtent3d.h>
#include <IwAxis2Placement.h>

PictureInPicture::PictureInPicture(Viewport *vp, Location loc,

                                   IwAxis2Placement const &picCS,

                                   IwVector3d const &modelRange,
                                   
                                   SIZE const &screenMargin,
                                   SIZE const &screenRect)
   : pVp(vp)
   , wasVpTransform(vp->GetTransform())
{
   IwVector3d center(picCS.GetOrigin()), xa(picCS.GetXAxis()), ya(picCS.GetYAxis()), za(picCS.GetZAxis());

   pVp->GetViewport(wasMinX, wasMaxX, wasMinY, wasMaxY);
   pVp->GetViewRange(wasLe, wasRi, wasBo, wasTo, wasNe, wasFa);

   glPushAttrib(GL_VIEWPORT_BIT);

   glMatrixMode(GL_PROJECTION);
   glPushMatrix();

   Rigid3Float transform;
   transform.SetIdentity();

   Rotate3Float rotMat(xa.x, xa.y, xa.z,
                       ya.x, ya.y, ya.z,
                       za.x, za.y, za.z);

   transform.SetRotationMatrix(rotMat);
   IwPoint3d o = transform * center;
   transform.SetTranslationVector(-o);

   int minX, maxX, minY, maxY;
   vp->GetViewport(minX, maxX, minY, maxY);

   glLoadIdentity();

   // sizes for ortho are derived from the rect so that the scale is 1.0
   glOrtho(-modelRange.x/2, modelRange.x/2,
           -modelRange.y/2, modelRange.y/2,
           -modelRange.z/2, modelRange.z/2);

   switch (loc)
   {
      case eTopLeft:
         glViewport(minX + screenMargin.cx, maxY - screenMargin.cy - screenRect.cy, screenRect.cx, screenRect.cy);
         break;
      case eBottomLeft:
         glViewport(minX + screenMargin.cx, minY + screenMargin.cy, screenRect.cx, screenRect.cy);
         break;
      case eTopRight:
         glViewport(maxX - screenMargin.cx - screenRect.cx, maxY - screenMargin.cy - screenRect.cy, screenRect.cx, screenRect.cy);
         break;
      case eBottomRight:
      default:
         glViewport(maxX - screenMargin.cx - screenRect.cx, minY + screenMargin.cy, screenRect.cx, screenRect.cy);
         break;
   }

   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();

   glLoadIdentity();
   float m[16];
   transform.GetGLMatrix(m);
   glMultMatrixf(m);
}

PictureInPicture::~PictureInPicture()
{
   glMatrixMode(GL_MODELVIEW);
   glPopMatrix(); // GL_MODELVIEW is popped

   glMatrixMode(GL_PROJECTION);
   glPopMatrix(); // GL_PROJECTION is popped

   pVp->SetTransform(wasVpTransform);
   pVp->SetViewport(wasMinX, wasMaxX, wasMinY, wasMaxY);
   pVp->SetViewRange(wasLe, wasRi, wasBo, wasTo, wasNe, wasFa);

   glPopAttrib();
}

