#include "AdvancedControlDlg.h"
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qradiobutton.h>
#include <qfiledialog.h>
#include <qbuttongroup.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qstring.h>
#include <qgridlayout.h>
#include <qcheckbox.h>
#include <qtabwidget.h>


CAdvancedControlDlg::CAdvancedControlDlg( QWidget* parent, Qt::WindowFlags flags, int forCRPS ) : QDialog( parent, flags )
{
	m_forCRPS = forCRPS;

	QGridLayout *dataLayout = createDataLayout();
	setLayout(dataLayout);
}

QGridLayout* CAdvancedControlDlg::createDataLayout()
{
	bool enableAdvCtrlParamsModified = false;
	bool underDev = false;
	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (underDevelopment != NULL && QString("1") == underDevelopment)
	{
		enableAdvCtrlParamsModified = true;
		underDev = true;
	}

	QGridLayout *mainLayout = new QGridLayout;

	// create 2 layout on left and right.
	QVBoxLayout *rightLayout = new QVBoxLayout;
	QVBoxLayout *leftLayout = new QVBoxLayout;
	mainLayout->addLayout(rightLayout, 0, 0, 1, 7);
	mainLayout->addLayout(leftLayout, 0, 8, 1, 1, Qt::AlignBottom);

	// Right Layout
	QTabWidget *tabWidget = new QTabWidget;
	rightLayout->addWidget(tabWidget);

	QWidget *rightAutoJOCStepsWidget = new QWidget();
	tabWidget->addTab(rightAutoJOCStepsWidget, tr("Auto Steps"));

	QWidget *rightFemoralAxesWidget = new QWidget();
	tabWidget->addTab(rightFemoralAxesWidget, tr("Femoral Axes"));

	QWidget *rightFemoralCutsWidget = new QWidget();
	tabWidget->addTab(rightFemoralCutsWidget, tr("Femoral Cuts"));

	QWidget *rightOutlineProfileWidget = new QWidget();
	tabWidget->addTab(rightOutlineProfileWidget, tr("Outline Profiles"));

	QWidget *rightOutputResults2Widget = new QWidget();
	if (underDev)
		tabWidget->addTab(rightOutputResults2Widget, tr("Output Results 2"));

	/////// rightFemoralAxesWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the FemoralAxes tab
	QHBoxLayout *FemoralAxesLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *FemoralAxesFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextFemoralAxesEnableRefine = new QLabel(tr("Enable refine:"));
	FemoralAxesFirstColumnLayout->addWidget(labelTextFemoralAxesEnableRefine, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextFemoralImplantCoronalRadius = new QLabel(tr("Femoral implant coronal radius:"));
	FemoralAxesFirstColumnLayout->addWidget(labelTextFemoralImplantCoronalRadius, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextTrochlearGrooveRadius = new QLabel(tr("Trochlear groove radius:"));
	FemoralAxesFirstColumnLayout->addWidget(labelTextTrochlearGrooveRadius, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextFemoralAxesDisplayMechAxes = new QLabel(tr("Display mech axes:"));
	FemoralAxesFirstColumnLayout->addWidget(labelTextFemoralAxesDisplayMechAxes, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *FemoralAxesSecondColumnLayout = new QVBoxLayout;
	m_chkFemoralAxesEnableRefine = new QCheckBox(tr(""));;
	m_chkFemoralAxesEnableRefine->setEnabled(true); 
	FemoralAxesSecondColumnLayout->addWidget(m_chkFemoralAxesEnableRefine, 0, Qt::AlignTop);
	m_spinFemoralImplantCoronalRadius = new QDoubleSpinBox();
	m_spinFemoralImplantCoronalRadius->setRange(10, 100);
	m_spinFemoralImplantCoronalRadius->setSingleStep(0.5);
	m_spinFemoralImplantCoronalRadius->setEnabled(enableAdvCtrlParamsModified);
	FemoralAxesSecondColumnLayout->addWidget(m_spinFemoralImplantCoronalRadius, 0, Qt::AlignTop);
	m_spinTrochlearGrooveRadius = new QDoubleSpinBox();
	m_spinTrochlearGrooveRadius->setRange(5, 80); 
	m_spinTrochlearGrooveRadius->setSingleStep(0.5);
	m_spinTrochlearGrooveRadius->setEnabled(enableAdvCtrlParamsModified);
	FemoralAxesSecondColumnLayout->addWidget(m_spinTrochlearGrooveRadius, 0, Qt::AlignTop);
	m_chkFemoralAxesDisplayMechAxes = new QCheckBox(tr(""));;
	m_chkFemoralAxesDisplayMechAxes->setEnabled(true); 
	FemoralAxesSecondColumnLayout->addWidget(m_chkFemoralAxesDisplayMechAxes, 0, Qt::AlignTop);

	FemoralAxesLayout->addLayout(FemoralAxesFirstColumnLayout);
	FemoralAxesLayout->addLayout(FemoralAxesSecondColumnLayout);
	rightFemoralAxesWidget->setLayout(FemoralAxesLayout);
	
	/////// rightAutoJOCStepsWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the AutoJOCSteps tab
	QHBoxLayout *AutoJOCStepsLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *AutoJOCStepsFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextAutoAllSteps = new QLabel(tr("Auto all design steps:"));
	AutoJOCStepsFirstColumnLayout->addWidget(labelTextAutoAllSteps, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAutoMajorSteps = new QLabel(tr("Auto major design steps:"));
	AutoJOCStepsFirstColumnLayout->addWidget(labelTextAutoMajorSteps, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAutoJOCSteps = new QLabel(tr("Auto JCurves, Outline Profile, and Femoral Cuts:"));
	AutoJOCStepsFirstColumnLayout->addWidget(labelTextAutoJOCSteps, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAutoNoneSteps = new QLabel(tr("None:"));
	AutoJOCStepsFirstColumnLayout->addWidget(labelTextAutoNoneSteps, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAutoAdditionalIterations = new QLabel(tr("Additional automation iterations:"));
	if (underDev)
		AutoJOCStepsFirstColumnLayout->addWidget(labelTextAutoAdditionalIterations, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *AutoJOCStepsSecondColumnLayout = new QVBoxLayout;
	m_buttonGroupAutoJOC = new QButtonGroup();
	m_chkAutoAllSteps = new QRadioButton(tr(""));
	AutoJOCStepsSecondColumnLayout->addWidget(m_chkAutoAllSteps, 0, Qt::AlignTop);
	m_buttonGroupAutoJOC->addButton(m_chkAutoAllSteps);
	m_chkAutoAllSteps->setEnabled(true);
	m_chkAutoMajorSteps = new QRadioButton(tr(""));
	AutoJOCStepsSecondColumnLayout->addWidget(m_chkAutoMajorSteps, 0, Qt::AlignTop);
	m_buttonGroupAutoJOC->addButton(m_chkAutoMajorSteps);
	m_chkAutoMajorSteps->setEnabled(true);
	m_chkAutoJOCSteps = new QRadioButton(tr(""));
	AutoJOCStepsSecondColumnLayout->addWidget(m_chkAutoJOCSteps, 0, Qt::AlignTop);
	m_buttonGroupAutoJOC->addButton(m_chkAutoJOCSteps);
	m_chkAutoJOCSteps->setEnabled(true);
	m_chkAutoNoneSteps = new QRadioButton(tr(""));
	AutoJOCStepsSecondColumnLayout->addWidget(m_chkAutoNoneSteps, 0, Qt::AlignTop);
	m_buttonGroupAutoJOC->addButton(m_chkAutoNoneSteps);
	m_chkAutoNoneSteps->setEnabled(true);
	m_chkAutoNoneSteps->setChecked(true);// make none checked. If any other is checked, it will be turned off.
	m_spinAutoAdditionalIterations = new QSpinBox();
	m_spinAutoAdditionalIterations->setRange(0, 10);
	m_spinAutoAdditionalIterations->setSingleStep(1);
	if (underDev)
		AutoJOCStepsSecondColumnLayout->addWidget(m_spinAutoAdditionalIterations, 0, Qt::AlignTop);
	
	AutoJOCStepsLayout->addLayout(AutoJOCStepsFirstColumnLayout);
	AutoJOCStepsLayout->addLayout(AutoJOCStepsSecondColumnLayout);
	rightAutoJOCStepsWidget->setLayout(AutoJOCStepsLayout);

	/////// rightFemoralCutsWidget Tab ///////////////////////////////////////////////////////
	// create layout for the FemoralCuts tab
	QHBoxLayout *FemoralCutsLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *FemoralCutsFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextFemoralCutsEnableReset = new QLabel(tr("Enable reset:"));
	FemoralCutsFirstColumnLayout->addWidget(labelTextFemoralCutsEnableReset, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextFemoralCutsOnlyMoveNormal = new QLabel(tr("Ctrl point only moves along normal:"));
	FemoralCutsFirstColumnLayout->addWidget(labelTextFemoralCutsOnlyMoveNormal, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextViewProfileOffsetDistance = new QLabel(tr("Default bone cut depth:"));
	FemoralCutsFirstColumnLayout->addWidget(labelTextViewProfileOffsetDistance, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextCutLineDisplayLength = new QLabel(tr("Cutline display length:"));
	FemoralCutsFirstColumnLayout->addWidget(labelTextCutLineDisplayLength, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextAchieveDesiredPostCuts = new QLabel(tr("Achieve desired posterior bone cuts:"));
	FemoralCutsFirstColumnLayout->addWidget(labelTextAchieveDesiredPostCuts, 0, Qt::AlignLeft|Qt::AlignTop);
	// Be moved to JCurves
	
	QLabel* labelTextCutsExtraAllowance = new QLabel(tr("Cut thickness extra allowance:"));
	FemoralCutsFirstColumnLayout->addWidget(labelTextCutsExtraAllowance, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *FemoralCutsSecondColumnLayout = new QVBoxLayout;
	m_chkFemoralCutsEnableReset = new QCheckBox(tr(""));;
	m_chkFemoralCutsEnableReset->setEnabled(true); 
	FemoralCutsSecondColumnLayout->addWidget(m_chkFemoralCutsEnableReset, 0, Qt::AlignTop);
	m_chkFemoralCutsOnlyMoveNormal = new QCheckBox(tr(""));;
	m_chkFemoralCutsOnlyMoveNormal->setEnabled(true); 
	FemoralCutsSecondColumnLayout->addWidget(m_chkFemoralCutsOnlyMoveNormal, 0, Qt::AlignTop);
	m_spinViewProfileOffsetDistance = new QDoubleSpinBox();
	m_spinViewProfileOffsetDistance->setRange(0.1, 50);
	m_spinViewProfileOffsetDistance->setSingleStep(0.1);
	m_spinViewProfileOffsetDistance->setEnabled(enableAdvCtrlParamsModified);
	FemoralCutsSecondColumnLayout->addWidget(m_spinViewProfileOffsetDistance, 0, Qt::AlignTop);
	m_spinCutLineDisplayLength = new QDoubleSpinBox();
	m_spinCutLineDisplayLength->setRange(1, 50);
	m_spinCutLineDisplayLength->setSingleStep(1);
	FemoralCutsSecondColumnLayout->addWidget(m_spinCutLineDisplayLength, 0, Qt::AlignTop);
	m_chkAchieveDesiredPostCuts = new QCheckBox(tr(""));
	m_chkAchieveDesiredPostCuts->setEnabled(false); 
	FemoralCutsSecondColumnLayout->addWidget(m_chkAchieveDesiredPostCuts, 0, Qt::AlignTop);
	// Be moved to JCurves
	
	m_spinCutsExtraAllowance = new QDoubleSpinBox();
	m_spinCutsExtraAllowance->setRange(0.0, 10.0);
	m_spinCutsExtraAllowance->setSingleStep(0.01);
	m_spinCutsExtraAllowance->setEnabled(true);
	FemoralCutsSecondColumnLayout->addWidget(m_spinCutsExtraAllowance, 0, Qt::AlignTop);

	FemoralCutsLayout->addLayout(FemoralCutsFirstColumnLayout);
	FemoralCutsLayout->addLayout(FemoralCutsSecondColumnLayout);
	rightFemoralCutsWidget->setLayout(FemoralCutsLayout);

	/////// rightOutlineProfileWidget Tab ///////////////////////////////////////////////////////
	// create the layout for the Outline Profile tab
	QHBoxLayout *OutlineProfileLayout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *OutlineProfileFirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextOutlineProfileEnableReset = new QLabel(tr("Enable reset:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineProfileEnableReset, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutlineProfilePosteriorCutFilterRadiusRatio = new QLabel(tr("Posterior cut filter radius (% of condylar width):"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineProfilePosteriorCutFilterRadiusRatio, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutlineProfileSketchSurfaceAntExtension = new QLabel(tr("Sketch surface anterior extension:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineProfileSketchSurfaceAntExtension, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutlineProfileSketchSurfacePostExtension = new QLabel(tr("Sketch surface posterior extension:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineProfileSketchSurfacePostExtension, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutlineProfileAntAirBallDistance = new QLabel(tr("Anterior air ball distance:"));
	OutlineProfileFirstColumnLayout->addWidget(labelTextOutlineProfileAntAirBallDistance, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *OutlineProfileSecondColumnLayout = new QVBoxLayout;
	m_chkOutlineProfileEnableReset = new QCheckBox(tr(""));;
	m_chkOutlineProfileEnableReset->setEnabled(true); 
	OutlineProfileSecondColumnLayout->addWidget(m_chkOutlineProfileEnableReset, 0, Qt::AlignTop);
	m_spinOutlineProfilePosteriorCutFilterRadiusRatio = new QDoubleSpinBox();
	m_spinOutlineProfilePosteriorCutFilterRadiusRatio->setRange(0, 0.5);
	m_spinOutlineProfilePosteriorCutFilterRadiusRatio->setSingleStep(0.01);
	OutlineProfileSecondColumnLayout->addWidget(m_spinOutlineProfilePosteriorCutFilterRadiusRatio, 0, Qt::AlignTop);
	m_spinOutlineProfileSketchSurfaceAntExtension = new QDoubleSpinBox();
	m_spinOutlineProfileSketchSurfaceAntExtension->setRange(0, 100);
	m_spinOutlineProfileSketchSurfaceAntExtension->setSingleStep(1.0);
	OutlineProfileSecondColumnLayout->addWidget(m_spinOutlineProfileSketchSurfaceAntExtension, 0, Qt::AlignTop);
	m_spinOutlineProfileSketchSurfacePostExtension = new QDoubleSpinBox();
	m_spinOutlineProfileSketchSurfacePostExtension->setRange(0, 100);
	m_spinOutlineProfileSketchSurfacePostExtension->setSingleStep(1.0);
	OutlineProfileSecondColumnLayout->addWidget(m_spinOutlineProfileSketchSurfacePostExtension, 0, Qt::AlignTop);
	m_spinOutlineProfileAntAirBallDistance = new QDoubleSpinBox();
	m_spinOutlineProfileAntAirBallDistance->setRange(-20, 20);
	m_spinOutlineProfileAntAirBallDistance->setSingleStep(0.25);
	OutlineProfileSecondColumnLayout->addWidget(m_spinOutlineProfileAntAirBallDistance, 0, Qt::AlignTop);

	OutlineProfileLayout->addLayout(OutlineProfileFirstColumnLayout);
	OutlineProfileLayout->addLayout(OutlineProfileSecondColumnLayout);
	rightOutlineProfileWidget->setLayout(OutlineProfileLayout);

	/////// rightOutputResults2Widget Tab ///////////////////////////////////////////////////////
	// create the layout for the OutputResults2 tab
	QHBoxLayout *OutputResults2Layout = new QHBoxLayout;
	// Add items 
	QVBoxLayout *OutputResults2FirstColumnLayout = new QVBoxLayout;
	QLabel* labelTextOutputNormalized = new QLabel(tr("Output normalized:"));
	OutputResults2FirstColumnLayout->addWidget(labelTextOutputNormalized, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextImportFromNormalized = new QLabel(tr("Import from normalized:"));
	OutputResults2FirstColumnLayout->addWidget(labelTextImportFromNormalized, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputWithTransformation = new QLabel(tr("Output with transformation:"));
	OutputResults2FirstColumnLayout->addWidget(labelTextOutputWithTransformation, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputNormalizedPegsImplant = new QLabel(tr("Output normalized Pegs implant:"));
	OutputResults2FirstColumnLayout->addWidget(labelTextOutputNormalizedPegsImplant, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputNormalizedFilletImplant = new QLabel(tr("Output normalized Fillet implant:"));
	OutputResults2FirstColumnLayout->addWidget(labelTextOutputNormalizedFilletImplant, 0, Qt::AlignLeft|Qt::AlignTop);
	QLabel* labelTextOutputNormalizedFilletImplantMedial = new QLabel(tr("Output norm Fillet implant w medial ref ctr:"));
	OutputResults2FirstColumnLayout->addWidget(labelTextOutputNormalizedFilletImplantMedial, 0, Qt::AlignLeft|Qt::AlignTop);

	QVBoxLayout *OutputResults2SecondColumnLayout = new QVBoxLayout;
	m_chkOutputNormalized = new QCheckBox(tr(""));
	OutputResults2SecondColumnLayout->addWidget(m_chkOutputNormalized, 0, Qt::AlignTop);
	m_chkImportFromNormalized = new QCheckBox(tr(""));
	OutputResults2SecondColumnLayout->addWidget(m_chkImportFromNormalized, 0, Qt::AlignTop);
	m_chkOutputWithTransformation = new QCheckBox(tr(""));
	OutputResults2SecondColumnLayout->addWidget(m_chkOutputWithTransformation, 0, Qt::AlignTop);
	OutputResults2Layout->addLayout(OutputResults2FirstColumnLayout);
	OutputResults2Layout->addLayout(OutputResults2SecondColumnLayout);

	//// Rotation/Translation For Import SolidWorks Parts 
	QVBoxLayout *OutputResults2LayoutPlus = new QVBoxLayout;

	QHBoxLayout *OutputResults2LayoutRotation = new QHBoxLayout;
	QLabel* labelTextImportRotation = new QLabel(tr("Import rotation XYZ:"));
	labelTextImportRotation->setToolTip("iTW generated F3 needs -90 for x rotation.");
	OutputResults2LayoutRotation->addWidget(labelTextImportRotation);
	m_chkImportRotationX = new QDoubleSpinBox();
	m_chkImportRotationX->setDecimals(9);
	m_chkImportRotationX->setRange(-1000000000, 1000000000);
	OutputResults2LayoutRotation->addWidget(m_chkImportRotationX);
	m_chkImportRotationY = new QDoubleSpinBox();
	m_chkImportRotationY->setDecimals(9);
	m_chkImportRotationY->setRange(-1000000000, 1000000000);
	OutputResults2LayoutRotation->addWidget(m_chkImportRotationY);
	m_chkImportRotationZ = new QDoubleSpinBox();
	m_chkImportRotationZ->setDecimals(9);
	m_chkImportRotationZ->setRange(-1000000000, 1000000000);
	OutputResults2LayoutRotation->addWidget(m_chkImportRotationZ);
	//
	QHBoxLayout *OutputResults2LayoutTranslation = new QHBoxLayout;
	QLabel* labelTextImportTranslation = new QLabel(tr("Import translation XYZ:"));
	labelTextImportTranslation->setToolTip("Check translation from femoral implant origin to imported model origin in SolidWorks assembly (F3-Implant).");
	OutputResults2LayoutTranslation->addWidget(labelTextImportTranslation);
	m_chkImportTranslationX = new QDoubleSpinBox();
	m_chkImportTranslationX->setDecimals(9);
	m_chkImportTranslationX->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTranslation->addWidget(m_chkImportTranslationX);
	m_chkImportTranslationY = new QDoubleSpinBox();
	m_chkImportTranslationY->setDecimals(9);
	m_chkImportTranslationY->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTranslation->addWidget(m_chkImportTranslationY);
	m_chkImportTranslationZ = new QDoubleSpinBox();
	m_chkImportTranslationZ->setDecimals(9);
	m_chkImportTranslationZ->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTranslation->addWidget(m_chkImportTranslationZ);

	// For transformation
	// X vector
	QHBoxLayout *OutputResults2LayoutTransformationX = new QHBoxLayout;
	QLabel* labelTextImportTransformationX = new QLabel(tr("Import transform X:"));
	labelTextImportTransformationX->setToolTip("General transformation matrix X vector.");
	OutputResults2LayoutTransformationX->addWidget(labelTextImportTransformationX);
	m_chkImportTransformationXx = new QDoubleSpinBox();
	m_chkImportTransformationXx->setDecimals(9);
	m_chkImportTransformationXx->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationX->addWidget(m_chkImportTransformationXx);
	m_chkImportTransformationXy = new QDoubleSpinBox();
	m_chkImportTransformationXy->setDecimals(9);
	m_chkImportTransformationXy->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationX->addWidget(m_chkImportTransformationXy);
	m_chkImportTransformationXz = new QDoubleSpinBox();
	m_chkImportTransformationXz->setDecimals(9);
	m_chkImportTransformationXz->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationX->addWidget(m_chkImportTransformationXz);
	// Y vector
	QHBoxLayout *OutputResults2LayoutTransformationY = new QHBoxLayout;
	QLabel* labelTextImportTransformationY = new QLabel(tr("Import transform Y:"));
	labelTextImportTransformationY->setToolTip("General transformation matrix Y vector.");
	OutputResults2LayoutTransformationY->addWidget(labelTextImportTransformationY);
	m_chkImportTransformationYx = new QDoubleSpinBox();
	m_chkImportTransformationYx->setDecimals(9);
	m_chkImportTransformationYx->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationY->addWidget(m_chkImportTransformationYx);
	m_chkImportTransformationYy = new QDoubleSpinBox();
	m_chkImportTransformationYy->setDecimals(9);
	m_chkImportTransformationYy->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationY->addWidget(m_chkImportTransformationYy);
	m_chkImportTransformationYz = new QDoubleSpinBox();
	m_chkImportTransformationYz->setDecimals(9);
	m_chkImportTransformationYz->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationY->addWidget(m_chkImportTransformationYz);
	// Transfornation vector
	QHBoxLayout *OutputResults2LayoutTransformationT = new QHBoxLayout;
	QLabel* labelTextImportTransformationT = new QLabel(tr("Import transform T:"));
	labelTextImportTransformationT->setToolTip("General transformation matrix translation vector.");
	OutputResults2LayoutTransformationT->addWidget(labelTextImportTransformationT);
	m_chkImportTransformationTx = new QDoubleSpinBox();
	m_chkImportTransformationTx->setDecimals(9);
	m_chkImportTransformationTx->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationT->addWidget(m_chkImportTransformationTx);
	m_chkImportTransformationTy = new QDoubleSpinBox();
	m_chkImportTransformationTy->setDecimals(9);
	m_chkImportTransformationTy->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationT->addWidget(m_chkImportTransformationTy);
	m_chkImportTransformationTz = new QDoubleSpinBox();
	m_chkImportTransformationTz->setDecimals(9);
	m_chkImportTransformationTz->setRange(-1000000000, 1000000000);
	OutputResults2LayoutTransformationT->addWidget(m_chkImportTransformationTz);

	OutputResults2LayoutPlus->addLayout(OutputResults2Layout);
	OutputResults2LayoutPlus->addLayout(OutputResults2LayoutRotation);
	OutputResults2LayoutPlus->addLayout(OutputResults2LayoutTranslation);
	OutputResults2LayoutPlus->addLayout(OutputResults2LayoutTransformationX);
	OutputResults2LayoutPlus->addLayout(OutputResults2LayoutTransformationY);
	OutputResults2LayoutPlus->addLayout(OutputResults2LayoutTransformationT);
	rightOutputResults2Widget->setLayout(OutputResults2LayoutPlus);

	//////////////////////////////////////////////////////////////////////////////////
	//// Left Layout
	//// Two buttons
	QVBoxLayout *vlayoutButtons = new QVBoxLayout;

	QPushButton *OKButton = new QPushButton(tr("OK"), NULL);
	connect(OKButton, SIGNAL(clicked(bool)), this, SLOT(accept()));
	vlayoutButtons->addWidget(OKButton);
	QPushButton *CancelButton = new QPushButton(tr("Cancel"), NULL);
	connect(CancelButton, SIGNAL(clicked(bool)), this, SLOT(reject()));
	vlayoutButtons->addWidget(CancelButton);

	leftLayout->addLayout(vlayoutButtons);

	return mainLayout;
}

// Auto JOC Steps
bool CAdvancedControlDlg::GetAutoJOCSteps()
{
	return m_chkAutoJOCSteps->isChecked();
}
void CAdvancedControlDlg::SetAutoJOCSteps(bool flag)
{
	m_chkAutoJOCSteps->setChecked(flag);
}

bool CAdvancedControlDlg::GetAutoMajorSteps()
{
	return m_chkAutoMajorSteps->isChecked();
}
void CAdvancedControlDlg::SetAutoMajorSteps(bool flag)
{
	m_chkAutoMajorSteps->setChecked(flag);
}

bool CAdvancedControlDlg::GetAutoAllSteps()
{
	return m_chkAutoAllSteps->isChecked();
}
void CAdvancedControlDlg::SetAutoAllSteps(bool flag)
{
	m_chkAutoAllSteps->setChecked(flag);
}
int CAdvancedControlDlg::GetAutoAdditionalIterations()
{
	return m_spinAutoAdditionalIterations->value();
}
void CAdvancedControlDlg::SetAutoAdditionalIterations(int val)
{
	m_spinAutoAdditionalIterations->setValue(val);
}

// Femoral Axes
bool CAdvancedControlDlg::GetFemoralAxesEnableRefine()
{
	if (m_chkFemoralAxesEnableRefine->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetFemoralAxesEnableRefine(bool val)
{
	if ( val )
		m_chkFemoralAxesEnableRefine->setCheckState(Qt::Checked);
	else
		m_chkFemoralAxesEnableRefine->setCheckState(Qt::Unchecked);
}
double CAdvancedControlDlg::GetFemoralImplantCoronalRadius()
{
	return m_spinFemoralImplantCoronalRadius->value();
}

void CAdvancedControlDlg::SetFemoralImplantCoronalRadius(double val)
{
	m_spinFemoralImplantCoronalRadius->setValue(val);
}

double CAdvancedControlDlg::GetTrochlearGrooveRadius()
{
	return m_spinTrochlearGrooveRadius->value();
}

void CAdvancedControlDlg::SetTrochlearGrooveRadius(double val)
{
	m_spinTrochlearGrooveRadius->setValue(val);
}

bool CAdvancedControlDlg::GetFemoralAxesDisplayMechAxes()
{
	if (m_chkFemoralAxesDisplayMechAxes->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetFemoralAxesDisplayMechAxes(bool val)
{
	if ( val )
		m_chkFemoralAxesDisplayMechAxes->setCheckState(Qt::Checked);
	else
		m_chkFemoralAxesDisplayMechAxes->setCheckState(Qt::Unchecked);
}

// Femoral Cuts
bool CAdvancedControlDlg::GetFemoralCutsEnableReset()
{
	if (m_chkFemoralCutsEnableReset->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetFemoralCutsEnableReset(bool val)
{
	if ( val )
		m_chkFemoralCutsEnableReset->setCheckState(Qt::Checked);
	else
		m_chkFemoralCutsEnableReset->setCheckState(Qt::Unchecked);
}
bool CAdvancedControlDlg::GetFemoralCutsOnlyMoveNormal()
{
	if (m_chkFemoralCutsOnlyMoveNormal->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetFemoralCutsOnlyMoveNormal(bool val)
{
	if ( val )
		m_chkFemoralCutsOnlyMoveNormal->setCheckState(Qt::Checked);
	else
		m_chkFemoralCutsOnlyMoveNormal->setCheckState(Qt::Unchecked);
}
double CAdvancedControlDlg::GetViewProfileOffsetDistance()
{
	return m_spinViewProfileOffsetDistance->value();
}

void CAdvancedControlDlg::SetViewProfileOffsetDistance(double val)
{
	m_spinViewProfileOffsetDistance->setValue(val);
}

double CAdvancedControlDlg::GetCutLineDisplayLength()
{
	return m_spinCutLineDisplayLength->value();
}

void CAdvancedControlDlg::SetCutLineDisplayLength(double val)
{
	m_spinCutLineDisplayLength->setValue(val);
}

bool CAdvancedControlDlg::GetAchieveDesiredPostCuts()
{
	if (m_chkAchieveDesiredPostCuts->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetAchieveDesiredPostCuts(bool val)
{
	if ( val )
		m_chkAchieveDesiredPostCuts->setCheckState(Qt::Checked);
	else
		m_chkAchieveDesiredPostCuts->setCheckState(Qt::Unchecked);
}

double CAdvancedControlDlg::GetCutsExtraAllowance()
{
	return m_spinCutsExtraAllowance->value();
}
void CAdvancedControlDlg::SetCutsExtraAllowance(double val)
{
	m_spinCutsExtraAllowance->setValue(val);
}

// Outline Profile
bool CAdvancedControlDlg::GetOutlineProfileEnableReset()
{
	if (m_chkOutlineProfileEnableReset->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetOutlineProfileEnableReset(bool val)
{
	if ( val )
		m_chkOutlineProfileEnableReset->setCheckState(Qt::Checked);
	else
		m_chkOutlineProfileEnableReset->setCheckState(Qt::Unchecked);
}
double CAdvancedControlDlg::GetOutlineProfilePosteriorCutFilterRadiusRatio()
{
	return m_spinOutlineProfilePosteriorCutFilterRadiusRatio->value();
}
void CAdvancedControlDlg::SetOutlineProfilePosteriorCutFilterRadiusRatio(double val)
{
	m_spinOutlineProfilePosteriorCutFilterRadiusRatio->setValue(val);
}
double CAdvancedControlDlg::GetOutlineProfileSketchSurfaceAntExtension()
{
	return m_spinOutlineProfileSketchSurfaceAntExtension->value();
}
void CAdvancedControlDlg::SetOutlineProfileSketchSurfaceAntExtension(double val)
{
	m_spinOutlineProfileSketchSurfaceAntExtension->setValue(val);
}
double CAdvancedControlDlg::GetOutlineProfileSketchSurfacePostExtension()
{
	return m_spinOutlineProfileSketchSurfacePostExtension->value();
}
void CAdvancedControlDlg::SetOutlineProfileSketchSurfacePostExtension(double val)
{
	m_spinOutlineProfileSketchSurfacePostExtension->setValue(val);
}
double CAdvancedControlDlg::GetOutlineProfileAntAirBallDistance()
{
	return m_spinOutlineProfileAntAirBallDistance->value();
}
void CAdvancedControlDlg::SetOutlineProfileAntAirBallDistance(double val)
{
	m_spinOutlineProfileAntAirBallDistance->setValue(val);
}

bool CAdvancedControlDlg::GetOutputNormalized()
{
	if (m_chkOutputNormalized->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetOutputNormalized(bool flag)
{
	if ( flag )
		m_chkOutputNormalized->setCheckState(Qt::Checked);
	else
		m_chkOutputNormalized->setCheckState(Qt::Unchecked);
}

bool CAdvancedControlDlg::GetImportFromNormalized()
{
	if (m_chkImportFromNormalized->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetImportFromNormalized(bool flag)
{
	if ( flag )
		m_chkImportFromNormalized->setCheckState(Qt::Checked);
	else
		m_chkImportFromNormalized->setCheckState(Qt::Unchecked);
}

bool CAdvancedControlDlg::GetOutputWithTransformation()
{
	if (m_chkOutputWithTransformation->checkState() == Qt::Unchecked) // unchecked
		return false;
	else
		return true;
}
void CAdvancedControlDlg::SetOutputWithTransformation(bool flag)
{
	if ( flag )
		m_chkOutputWithTransformation->setCheckState(Qt::Checked);
	else
		m_chkOutputWithTransformation->setCheckState(Qt::Unchecked);
}

double CAdvancedControlDlg::GetImportRotationX()
{
	return m_chkImportRotationX->value();
}
void CAdvancedControlDlg::SetImportRotationX(double val)
{
	m_chkImportRotationX->setValue(val);
}
double CAdvancedControlDlg::GetImportRotationY()
{
	return m_chkImportRotationY->value();
}
void CAdvancedControlDlg::SetImportRotationY(double val)
{
	m_chkImportRotationY->setValue(val);
}
double CAdvancedControlDlg::GetImportRotationZ()
{
	return m_chkImportRotationZ->value();
}
void CAdvancedControlDlg::SetImportRotationZ(double val)
{
	m_chkImportRotationZ->setValue(val);
}
double CAdvancedControlDlg::GetImportTranslationX()
{
	return m_chkImportTranslationX->value();
}
void CAdvancedControlDlg::SetImportTranslationX(double val)
{
	m_chkImportTranslationX->setValue(val);
}
double CAdvancedControlDlg::GetImportTranslationY()
{
	return m_chkImportTranslationY->value();
}
void CAdvancedControlDlg::SetImportTranslationY(double val)
{
	m_chkImportTranslationY->setValue(val);
}
double CAdvancedControlDlg::GetImportTranslationZ()
{
	return m_chkImportTranslationZ->value();
}
void CAdvancedControlDlg::SetImportTranslationZ(double val)
{
	m_chkImportTranslationZ->setValue(val);
}

double CAdvancedControlDlg::GetImportTransformationXx()
{
	return m_chkImportTransformationXx->value();
}
void CAdvancedControlDlg::SetImportTransformationXx(double val)
{
	m_chkImportTransformationXx->setValue(val);
}
double CAdvancedControlDlg::GetImportTransformationXy()
{
	return m_chkImportTransformationXy->value();
}
void CAdvancedControlDlg::SetImportTransformationXy(double val)
{
	m_chkImportTransformationXy->setValue(val);
}
double CAdvancedControlDlg::GetImportTransformationXz()
{
	return m_chkImportTransformationXz->value();
}
void CAdvancedControlDlg::SetImportTransformationXz(double val)
{
	m_chkImportTransformationXz->setValue(val);
}

double CAdvancedControlDlg::GetImportTransformationYx()
{
	return m_chkImportTransformationYx->value();
}
void CAdvancedControlDlg::SetImportTransformationYx(double val)
{
	m_chkImportTransformationYx->setValue(val);
}
double CAdvancedControlDlg::GetImportTransformationYy()
{
	return m_chkImportTransformationYy->value();
}
void CAdvancedControlDlg::SetImportTransformationYy(double val)
{
	m_chkImportTransformationYy->setValue(val);
}
double CAdvancedControlDlg::GetImportTransformationYz()
{
	return m_chkImportTransformationYz->value();
}
void CAdvancedControlDlg::SetImportTransformationYz(double val)
{
	m_chkImportTransformationYz->setValue(val);
}

double CAdvancedControlDlg::GetImportTransformationTx()
{
	return m_chkImportTransformationTx->value();
}
void CAdvancedControlDlg::SetImportTransformationTx(double val)
{
	m_chkImportTransformationTx->setValue(val);
}
double CAdvancedControlDlg::GetImportTransformationTy()
{
	return m_chkImportTransformationTy->value();
}
void CAdvancedControlDlg::SetImportTransformationTy(double val)
{
	m_chkImportTransformationTy->setValue(val);
}
double CAdvancedControlDlg::GetImportTransformationTz()
{
	return m_chkImportTransformationTz->value();
}
void CAdvancedControlDlg::SetImportTransformationTz(double val)
{
	m_chkImportTransformationTz->setValue(val);
}
