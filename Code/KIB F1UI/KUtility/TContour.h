///////////////////////////////////////////////////////////////////////
//	File:    Contour.h
//	Purpose: A contour line composed of an orderly set of vertices with 
//    (x,y) coordinates. The vertices can be moved, inserted or deleted. 
//    The contour can be open (the first and last point are not linked) 
//    or closed (otherwise). The contour is default to be closed.
//    Refer to TPointSet.h for a random set of points without order.
///////////////////////////////////////////////////////////////////////

#ifndef CONTOUR_H
#define CONTOUR_H

#include "TPointSet.h"
#include "TLineSegment2.h"
#include <limits>

// Contour class
template<typename T> class Contour : public PointSet<T>
{
protected:
    bool closed;        // open or closed

public:
    // Constructor
    Contour() : closed(true) {} // default to close contour

    // Destructor
    ~Contour() {}

    // Get whether the contour is closed
    bool IsClosed() const { return closed; }

    // Set whether the contour is closed
    void SetClosed(bool isclosed) { closed = isclosed; }

    // Comparison
    bool operator== (const Contour<T>& cs) const
    {
        if( closed != cs.IsClosed() )
            return false;
        int n = GetLength();
        if( n != cs.GetLength() )
            return false;
        for(int i=0; i<n; i++)
            if( X(i) != cs.X(i) || Y(i) != cs.Y(i) )
                return false;
        return true;
    }

    bool operator!= (const Contour<T>& cs) const
    {
        if( closed != cs.IsClosed() )
            return true;
        int n = GetLength();
        if( n != cs.GetLength() )
            return true;
        for(int i=0; i<n; i++)
            if( X(i) != cs.X(i) || Y(i) != cs.Y(i) )
                return true;
        return false;
    }

    // Compute the distance of (x,y) to the contour. -1 is returned if 
    // the contour is empty
    T Distance(T x, T y) const;

    // Determine whether (x,y) is equal or less 'dist' from the contour
    bool Near(T x, T y, T dist) const;

    // Get the vertex index that is closest to the specified coordinate
    int NearestVertex(T x, T y) const;

    // Determine whether (x,y) is equal or less 'dist' from one of the vertices
    int NearVertex(T x, T y, T dist) const;

    // Determine whether (x,y) is inside the contour region
    bool IsInternal(T x, T y) const;

    // Determine if the input contour is inside/contains/disjoint/overlaps the current contour.
    // Both are assumed to be closed.
    // Input:
    //    cs - contour to be tested
    // Return:
    //    1 : 'cs' is inside the current contour
    //    2 : ...... contains ..................
    //    -1: .... is disjoint from ............
    //    0 : ...... overlaps ..................
    int RelativeTo(const Contour<T>& cs) const;

    // Save to a file
    bool ToFile(const char* fileName)
    {
        ofstream output(fileName);
        if ( output.fail() )
            return false;
        output << *this;
        return true;
    }

    // Load from a file
    bool FromFile(const char* fileName)
    {
        ifstream input(fileName);
        if ( input.fail() )
            return false;
        input >> *this;
        return true;
    }
};

//////////////////////////////////////////
// Implementations
//////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Function name:    Distance
// Function purpose: Compute the distance of (x,y) to the contour. 
//   -1 is returned if the contour is empty
// Input:
//   x, y: the coordinate
// Output:
//   distance
///////////////////////////////////////////////////////////////////////
template <typename T> T Contour<T>::Distance(T x, T y) const
{
    if( GetLength()==1 )
        return Point2Double(x,y).Distance(X(0),Y(0));

    // find the shortest distance to all contour segments
    T dist = -1;

    int i,j;
    if( closed )
    {
        j = GetLength()-1;
        i = 0;
    }
    else
    {
        j = 0;
        i = 1;
    }
    TLineSegment2<T> lineSeg;
    for(; i<GetLength()-1; i++)
    {
        lineSeg.SetP1( X(i), Y(i) );
        lineSeg.SetP2( X(j), Y(j) );
        T d = lineSeg.Distance(x,y);
        if( dist==-1 || d<dist )
            dist = d;
        j = i;
    }
    return dist;
}

///////////////////////////////////////////////////////////////////////
// Function name:    Near
// Function purpose: Determine whether (x,y) is equal or less 'dist' from the contour
// Input:
//   x, y: the coordinate
// Return:
//   true (yes) false (no)
///////////////////////////////////////////////////////////////////////
template <typename T> bool Contour<T>::Near(T x, T y, T dist) const
{
    if( GetLength()==1 )
        return (Point2Double(x,y).Distance(X(0),Y(0)) <= dist);

    int i,j;
    if( closed )
    {
        j = GetLength()-1;
        i = 0;
    }
    else
    {
        j = 0;
        i = 1;
    }
    TLineSegment2<T> lineSeg;
    for(; i<GetLength()-1; i++)
    {
        lineSeg.SetP1( X(i), Y(i) );
        lineSeg.SetP2( X(j), Y(j) );
        if( lineSeg.Distance(x,y) <= dist )
            return true;
        j = i;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////
// Function name:    NearestVertex
// Function purpose: Get the vertex index that is closest to the specified coordinate
// Input:
//   x, y: the coordinate
// Return:
//   The nearest vertex index
///////////////////////////////////////////////////////////////////////
template <typename T> int Contour<T>::NearestVertex(T x, T y) const
{
    #undef max
    T distmin = std::numeric_limits<T>::max();
    T dist;
    vector<T>::const_iterator it=vertex.begin();
    int i = 0;
    int pos = -1;
    while( it!=vertex.end() )
    {
        T cx = *it++;
        T cy = *it++;

        cx -= x;
        cy -= y;

        dist = cx*cx + cy*cy;
        if( dist < distmin )
        {
            distmin = dist;
            pos = i;
        }

        ++i;
    }
    return pos;
}

///////////////////////////////////////////////////////////////////////
// Function name:    NearVertex
// Function purpose: Determine whether (x,y) is equal or less 'dist' from one
//   of the vertices
// Input:
//   x, y: the coordinate
//   dist: distance
// Return:
//   The vertex index whose distance to (x,y) is within 'dist'. -1 is
//   returned is no such vertex can be found.
///////////////////////////////////////////////////////////////////////
template <typename T> int Contour<T>::NearVertex(T x, T y, T dist) const
{
    T dist2 = dist * dist;
    vector<T>::const_iterator it=vertex.begin();
    int i = 0;
    while( it!=vertex.end() )
    {
        T cx = *it++;
        T cy = *it++;

        cx -= x;
        cy -= y;

        if( cx*cx + cy*cy <= dist2 )
            return i;

        ++i;
    }
    return -1;
}

///////////////////////////////////////////////////////////////////////
// Function name:    IsInternal
// Function purpose: Determine whether (x,y) is internal of the contour region
// Input:
//   x, y: the coordinate
// Return:
//   Internal (true) or not (false). Always returns false for a non-closed contour.
///////////////////////////////////////////////////////////////////////
template <typename T> bool Contour<T>::IsInternal(T x, T y) const
{
    if( !closed )
        return false;

    int i, j;
    bool c = false;
    int n = GetLength();
    for (i = 0, j = n-1; i < n; j = i++) {
        if( ((((Y(i)<=y) && (y<Y(j))) ||
            ((Y(j))<=y) && (y<Y(i)))) &&
            (x < ((double)X(j) - X(i)) * ((double)y - Y(i)) / ((double)Y(j) - Y(i)) + X(i)) )
            
            c = !c;
    }
    return c;
}

// Determine if the input contour is inside/contains/disjoint/overlaps the current contour
// Input:
//    cs - contour to be tested
// Return:
//    1 : 'cs' is inside the current contour
//    2 : ...... contains ..................
//    -1: .... is disjoint from ............
//    0 : ...... overlaps ..................
template <typename T> int Contour<T>::RelativeTo(const Contour<T>& cs) const
{
    int inside = 0;
    int outside = 0;
    const int n = cs.GetLength();
    for(int i=0; i<n; i++)
    {
        if( IsInternal(cs.X(i), cs.Y(i)) )
            ++inside;
        else
            ++outside;
    }
    if( inside == n )
        return 1;
    else if( outside == n )
        return cs.IsInternal(X(0),Y(0)) ? 2 : -1;
    else
        return 0;
}

///////////////////////////////////////////////////////////////////////
// Function name:    operator>>
// Function purpose: Input the content of a pointset from a stream
//   where the first line specifies whether the contour is closed (true)
//   or not, and followed by several lines of comma-separated x, y coordinates
// Input:
//   output: the ostream reference
//   ct: a reference to a Contour object
// Output:
//   the ostream reference
///////////////////////////////////////////////////////////////////////
template <typename T> istream& operator>>(istream& input, Contour<T>& ct)
{
    string str,str1,str2;
    bool close;
    T x,y;
    ct.Clear();

    getline(input, str);
    stringstream s(str);
    s >> close;
    ct.SetClosed(close);

    while( getline(input, str) )
    {
        int p = str.find(',');
        if( p>0 && p < str.size()-1 )
        {
            str1 = str.substr(0, p);
            str2 = str.substr(p+1);

            stringstream s1(str1);
            stringstream s2(str2);
            s1 >> x;
            s2 >> y;
            ct.Append(x,y);
        }
    }
    return input;
}


///////////////////////////////////////////////////////////////////////
// Function name:    operator<<
// Function purpose: Output the content of the contour to a stream
//   where the first line specifies whether the contour is closed (true)
//   or not, and followed by several lines of comma-separated x, y coordinates
// Input:
//   output: the ostream reference
//   ct: a const reference to a Contour object
// Output:
//   the ostream reference
///////////////////////////////////////////////////////////////////////
template <typename T> ostream& operator<<(ostream& output, const Contour<T>& ct)
{
    output << ct.IsClosed() << std::endl;
    for(int i=0; i<ct.GetLength(); i++)
        output << ct.X(i) << ',' << ct.Y(i) << std::endl;
    return output;
}

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

// A contour of integer coordinates
typedef Contour<int> ContourInt;

// A contour of float coordinates
typedef Contour<float> ContourFloat;

// A contour of double coordinates
typedef Contour<double> ContourDouble;

#endif //CONTOUR_H
