#ifndef OCTREETRIANGLE_H
#define OCTREETRIANGLE_H

#include <vector>
using namespace std;

#include "TVector3.h"
#include "TPickRay.h"
#pragma warning( disable : 4996 )
#include "IwVector3d.h"
#pragma warning( default : 4996)

template <typename T> 
struct TTriangle
{
    TPoint3<T> vert[3];    // vertices
	TVector3<T> norm[3];   // normal
};

// Octree triangle node
template <typename T> 
struct TOctreeTriangleNode
{
    TPoint3<T> boundMin, boundMax;     // bounding box
    vector< TTriangle<T> > triangles;     // triangles at the node
    int depth;                      // depth of the node
    int prev;                       // parent node index
    int next;                       // index of the 1st of 8 nodes after split
};

// Octree for triangles
// Note:
//  - For use with a fixed set of triangles
//  - There might be overlap between neighboring octree nodes as, no matter where the spatial divider is, there are
//    going to be triangles that straddle the dividers
//  - The bounding box of each node is first used to assigned a triangle to the appropriate node.
//    After all triangles are added, call the 'Finalize' function to update the bounding box to reflect
//    the 'true' bounding box of internal triangles
template <typename T>
class TOctreeTriangle
{
public:
    // contructor: bounding box of all triangles & maximu level
    TOctreeTriangle() : xBin(0),yBin(0),zBin(0) {}

    // Initialize the # of bin at the lowest level based on a bounding box
    //  so that each bin is roughly equal size in x,y,z
    bool Init(const TPoint3<T> bndMin, const TPoint3<T> bndMax);

    // Add a triangle
    void AddTriangle(TTriangle<T>& tri);

    // Finalize the octree after all the triangles are added
    void Finalize();

    // Clear the content
    void Clear();

    // Get the octree nodes
    vector< TOctreeTriangleNode<T> >& GetNodes() { return nodes; }

    // Pick a point
    bool Pick(const TPickRay<T>& ray, TPoint3<T>& pt);

protected:
    // Add the triangle to the node at the index position. If the node isn't at the root,
    // navigate down the tree until reaching the root. Split the root if necessary.
    void AddTriangle(int index, TTriangle<T>& tri);

    vector< TOctreeTriangleNode<T> > nodes; // buffer of all nodes
    int xBin, yBin, zBin;                   // initial # of bins along x,y,z
    TPoint3<T> bndMin, bndMax;              // bounding box
};

//=============== Implementation ===============

template <typename T>
bool TOctreeTriangle<T>::Init(const TPoint3<T> bndmin, const TPoint3<T> bndmax)
{
    bndMin = bndmin;
    bndMax = bndmax;

    T sx = bndMax.GetX() - bndMin.GetX();
    T sy = bndMax.GetY() - bndMin.GetY();
    T sz = bndMax.GetZ() - bndMin.GetZ();

    // get the minimum dimension
    T smin = min(min(sx,sy),sz);

    xBin = (int)(sx/smin +.5);
    yBin = (int)(sy/smin +.5);
    zBin = (int)(sz/smin +.5);

    nodes.clear();
    for(int z=0; z<zBin; z++)
    {
        T zs = bndMin.GetZ() + z*sz/zBin;
        T ze = bndMin.GetZ() + (z+1)*sz/zBin;

        for(int y=0; y<yBin; y++)
        {
            T ys = bndMin.GetY() + y*sy/yBin;
            T ye = bndMin.GetY() + (y+1)*sy/yBin;

            for(int x=0; x<xBin; x++)
            {
                T xs = bndMin.GetX() + x*sx/xBin;
                T xe = bndMin.GetX() + (x+1)*sx/xBin;

                TOctreeTriangleNode<T> node;
                node.depth = 0;
                node.prev = -1;
                node.next = -1;
                node.boundMin.SetXYZ(xs,ys,zs);
                node.boundMax.SetXYZ(xe,ye,ze);
                nodes.push_back( node );
            }
        }
    }
    nodes.reserve(xBin*yBin*zBin*1600);
    return true;
}

// Add a triangle
template <typename T>
void TOctreeTriangle<T>::AddTriangle(TTriangle<T>& tri)
{
    // inverse of bin size
    T ibx = xBin / (bndMax.GetX() - bndMin.GetX());
    T iby = yBin / (bndMax.GetY() - bndMin.GetY());
    T ibz = zBin / (bndMax.GetZ() - bndMin.GetZ());

    // find which bin of depth 0 should the triangle be assigned to based on the centroid
    TPoint3<T> v = (tri.vert[0] + tri.vert[1] + tri.vert[2])/3;
    int x = (int) min(max((T)0, (v.GetX() - bndMin.GetX()) * ibx), (T)xBin-1);
    int y = (int) min(max((T)0, (v.GetY() - bndMin.GetY()) * iby), (T)yBin-1);
    int z = (int) min(max((T)0, (v.GetZ() - bndMin.GetZ()) * ibz), (T)zBin-1);

    // find which bin of depth 0 should the triangle be
    /*Point3Float v[3];
    int ix[3],iy[3],iz[3];
    for(int i=0; i<3; i++)
    {
        v[i] = tri.vert[i];
        ix[i] = (int) min(max(0.0f, (v[i].GetX() - bndMin.GetX()) * ibx), (float)xBin-1);
        iy[i] = (int) min(max(0.0f, (v[i].GetY() - bndMin.GetY()) * iby), (float)yBin-1);
        iz[i] = (int) min(max(0.0f, (v[i].GetZ() - bndMin.GetZ()) * ibz), (float)zBin-1);
    }
    int x,y,z;
    if( ix[0]==ix[1] || ix[0]==ix[2] )
        x = ix[0];
    else if( ix[1]==ix[2] )
        x = ix[1];
    else
        return;

    if( iy[0]==iy[1] || iy[0]==iy[2] )
        y = iy[0];
    else if( iy[1]==iy[2] )
        y = iy[1];
    else
        return;

    if( iz[0]==iz[1] || iz[0]==iz[2] )
        z = iz[0];
    else if( iz[1]==iz[2] )
        z = iz[1];
    else
        return;*/

    AddTriangle(z*xBin*yBin + y*xBin + x, tri);
}

// Add the triangle to the node at the index position. If the node isn't at the leaf,
// navigate down the tree until reaching the leaf. Split the leaf if necessary.
template <typename T>
void TOctreeTriangle<T>::AddTriangle(int index, TTriangle<T>& tri)
{
    TPoint3<T> pt[3];

    TOctreeTriangleNode<T>* node = &nodes[index];
    pt[0] = node->boundMin;
    pt[1] = (node->boundMin + node->boundMax) * .5;
    pt[2] = node->boundMax;
    while( node->next != -1 )
    {
        int x=0;
        int y=0;
        int z=0;
        for(int i=0; i<3; i++)
        {
            TPoint3<T>& vi = tri.vert[i];
            TPoint3<T>& mid = pt[1];

            x += (vi.GetX() >= mid.GetX());
            y += (vi.GetY() >= mid.GetY());
            z += (vi.GetZ() >= mid.GetZ());
        }
        x >>= 1;
        y >>= 1;
        z >>= 1;
        index = node->next + (z<<2)+(y<<1)+x;
        node = &nodes[index];
        pt[0] = node->boundMin;
        pt[1] = (node->boundMin + node->boundMax) * .5;
        pt[2] = node->boundMax;
    }
    node->triangles.push_back(tri);

    // split the node if necessary..
    if( node->triangles.size()>100 && node->depth<10 )
    {
        int pos = (int)nodes.size(); // position to insert the new nodes
        node->next = pos;

        // create the split nodes
        TOctreeTriangleNode<T> nd;
        nd.depth = node->depth+1;
        nd.prev = index;
        nd.next = -1;
        for(int i=0; i<2; i++)
        for(int j=0; j<2; j++)
        for(int k=0; k<2; k++)
        {
            nd.boundMin.SetXYZ(pt[k  ].GetX(), pt[j  ].GetY(), pt[i  ].GetZ());
            nd.boundMax.SetXYZ(pt[k+1].GetX(), pt[j+1].GetY(), pt[i+1].GetZ());
            nodes.push_back( nd );
        }

        // reassign the triangles
        node = &nodes[index];
        for(size_t t=0; t<node->triangles.size(); t++)
        {
            TTriangle<T>& tt = node->triangles[t];

            int x=0;
            int y=0;
            int z=0;
            for(int i=0; i<3; i++)
            {
                TPoint3<T>& vi = tt.vert[i];
                TPoint3<T>& mid = pt[1];

                x += (vi.GetX() >= mid.GetX());
                y += (vi.GetY() >= mid.GetY());
                z += (vi.GetZ() >= mid.GetZ());
            }
            x >>= 1;
            y >>= 1;
            z >>= 1;

            nodes[pos + (z<<2)+(y<<1)+x].triangles.push_back(tt);
        }
        node->triangles.clear();
    }
}

// Finalize the octree after all the triangles are added
template <typename T>
void TOctreeTriangle<T>::Finalize()
{
    const size_t num = nodes.size();
    const size_t n0 = xBin * yBin * zBin; // depth 0 node
    size_t i;
    
    // update the bounding box of non-depth-0 nodes
    // back -> front
    for(i=num; i>n0; i-=8)
    {
        T xmin = FLT_MAX;
        T ymin = FLT_MAX;
        T zmin = FLT_MAX;
        T xmax = -FLT_MAX;
        T ymax = -FLT_MAX;
        T zmax = -FLT_MAX;

        for(int j=0; j<8; j++)
        {
            TOctreeTriangleNode<T>& node = nodes[i-8+j];
#ifndef NDEBUG
            if( node.prev != nodes[i-8].prev )
                throw "Octree content error";
#endif
            if( node.next==-1 )
            {
                // leaf node
                const size_t n = node.triangles.size();
                if( n==0 )
                    continue;

                T txmin = FLT_MAX;
                T tymin = FLT_MAX;
                T tzmin = FLT_MAX;
                T txmax = -FLT_MAX;
                T tymax = -FLT_MAX;
                T tzmax = -FLT_MAX;
                for(size_t k=0; k<n; k++)
                {
                    TTriangle<T>& t = node.triangles[k];
                    for(int v=0; v<3; v++)
                    {
                        TPoint3<T> vt = t.vert[v];

                        txmin = min(txmin, vt.GetX());
                        tymin = min(tymin, vt.GetY());
                        tzmin = min(tzmin, vt.GetZ());

                        txmax = max(txmax, vt.GetX());
                        tymax = max(tymax, vt.GetY());
                        tzmax = max(tzmax, vt.GetZ());
                    }
                }
                node.boundMin.SetXYZ(txmin, tymin, tzmin);
                node.boundMax.SetXYZ(txmax, tymax, tzmax);

                xmin = min(xmin, txmin);
                ymin = min(ymin, tymin);
                zmin = min(zmin, tzmin);
                                 
                xmax = max(xmax, txmax);
                ymax = max(ymax, tymax);
                zmax = max(zmax, tzmax);
            }
            else
            {
                // non-leaf node (should've be updated previously)
                xmin = min(xmin, node.boundMin.GetX());
                ymin = min(ymin, node.boundMin.GetY());
                zmin = min(zmin, node.boundMin.GetZ());

                xmax = max(xmax, node.boundMax.GetX());
                ymax = max(ymax, node.boundMax.GetY());
                zmax = max(zmax, node.boundMax.GetZ());
            }
        }

        TOctreeTriangleNode<T>& prevNode = nodes[nodes[i-8].prev];
        prevNode.boundMin.SetXYZ(xmin, ymin, zmin);
        prevNode.boundMax.SetXYZ(xmax, ymax, zmax);
    }

    // update the bounding box of depth-0 nodes
    for(i=0; i<n0; i++)
    {
        TOctreeTriangleNode<T>& node = nodes[i];
        if( node.next!=-1 )
            continue;
        const size_t n = node.triangles.size();
        if( n==0 )
            continue;

        T txmin = FLT_MAX;
        T tymin = FLT_MAX;
        T tzmin = FLT_MAX;
        T txmax = -FLT_MAX;
        T tymax = -FLT_MAX;
        T tzmax = -FLT_MAX;
        for(size_t k=0; k<n; k++)
        {
            TTriangle<T>& t = node.triangles[k];
            for(int v=0; v<3; v++)
            {
                TPoint3<T> vt = t.vert[v];

                txmin = min(txmin, vt.GetX());
                tymin = min(tymin, vt.GetY());
                tzmin = min(tzmin, vt.GetZ());

                txmax = max(txmax, vt.GetX());
                tymax = max(tymax, vt.GetY());
                tzmax = max(tzmax, vt.GetZ());
            }
        }
        node.boundMin.SetXYZ(txmin, tymin, tzmin);
        node.boundMax.SetXYZ(txmax, tymax, tzmax);
    }
}

template <typename T>
void TOctreeTriangle<T>::Clear()
{
    nodes.clear();
    xBin = yBin = zBin = 0;
    bndMin.SetXYZ(0,0,0);
    bndMax.SetXYZ(-1,-1,-1);
}

// Pick a point along the ray
template <typename T>
bool TOctreeTriangle<T>::Pick(const TPickRay<T>& ray, TPoint3<T>& pt)
{
    TVector3<T> dir = ray.GetRayDir();
    TPoint3<T> cen = ray.GetRayCen();

    IwVector3d iwDir(dir.GetX(), dir.GetY(), dir.GetZ());
    IwPoint3d  iwCen(cen.GetX(), cen.GetY(), cen.GetZ());

    bool b = false;
    T dist,mindist;

    const int numNode = (int) nodes.size();
    if( numNode==0 )
        return false;

    // node index buffer for test intersection
    vector<int> buf;
    buf.reserve(numNode);

    for(int n=0; n<numNode; n++)
    {
        TOctreeTriangleNode<T>& nd = nodes[n];
        // only process the depth-0 nodes
        if( nd.depth>0 )
            break;
        if( nd.triangles.size()>0 || nd.next>0 )
            buf.push_back(n);
    }
    while( !buf.empty() )
    {
        TOctreeTriangleNode<T>& nd = nodes[buf.back()];
        buf.pop_back();

        // TODO: examine the code below carefully!!!
        // PROBLEM: the declarations of 'bndMin' and 'bndMax' HIDE
        // data members of this class.
        TPoint3<T> bndMin = nd.boundMin;
        TPoint3<T> bndMax = nd.boundMax;
        IwExtent3d bnd( IwVector3d(bndMin.GetX(),bndMin.GetY(),bndMin.GetZ()), 
                        IwVector3d(bndMax.GetX(),bndMax.GetY(),bndMax.GetZ()) ); 
        if( !bnd.IntersectsLine3d( iwCen, iwDir ) )
            continue;

        if( nd.next > 0 )
        {
            // non-leaf node
            for(int n=nd.next; n<nd.next+8; n++)
            {
			    // TODO: examine the code below carefully!!!
				// PROBLEM: the declaration of 'nd' HIDES
				// previous declaration (on line 420).
                TOctreeTriangleNode<T>& nd = nodes[n];
                if( nd.triangles.size()>0 || nd.next>0 )
                    buf.push_back(n);
            }
        }
        else
        {
            // leaf-node: get intesection
            vector<TriangleFloat>& triangles = nd.triangles;
            const size_t num = triangles.size();

            for(size_t i=0; i<num; i++)
            {
                TTriangle<T>& tri = triangles[i];

                TVector3<T>& v0 = tri.vert[0];
                TVector3<T>& v1 = tri.vert[1];
                TVector3<T>& v2 = tri.vert[2];

                TVector3<T> e1 = v1 - v0;
                TVector3<T> e2 = v2 - v0;
                if( fabs(e1.UnitCross(e2).dot(dir)) < 0.001 )
                    continue; // almost parallel

                TVector3<T> v = cen - v0;
                TVector3<T> w1 = e1 - e1.dot(dir)*dir;
                TVector3<T> w2 = e2 - e2.dot(dir)*dir;
                TVector3<T> w = v - v.dot(dir)*dir;

                T w1w1 = w1.dot(w1);
                T w2w2 = w2.dot(w2);
                T w1w2 = w1.dot(w2);
                T ww1 = w.dot(w1);
                T ww2 = w.dot(w2);
                T iw = 1/(w1w1*w2w2-w1w2*w1w2);
                T n1 = (ww1*w2w2 - ww2*w1w2)*iw;
                T n2 = (ww2*w1w1 - ww1*w1w2)*iw;

                if( n1>=0 && n2>=0 && n1+n2<=1 )
                {
                    TPoint3<T> t = v0 + n1*e1 + n2*e2;
                    if( dir.dot(t - cen)>=0 )   // along the ray, not in the reverse direction
                    {
                        dist = cen.Distance(t);
                        if( !b || dist<mindist )
                        {
                            b = true;
                            mindist = dist;
                            pt = t;
                        }
                    }
                }
            }
        }
    }
    return b;
}

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TTriangle<float> TriangleFloat;
typedef TTriangle<double> TriangleDouble;

typedef TOctreeTriangleNode<float> OctreeTriangleNodeFloat;
typedef TOctreeTriangleNode<double> OctreeTriangleNodeDouble;

typedef TOctreeTriangle<float> OctreeTriangleFloat;
typedef TOctreeTriangle<double> OctreeTriangleDouble;

#endif //OCTREETRIANGLE_H
