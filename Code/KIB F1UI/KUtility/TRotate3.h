#ifndef TROTATE3_H
#define TROTATE3_H

//////////////////////////////////////////
// 3D rotation matrix
// (storage: use column-major as Eigen default)
//////////////////////////////////////////

#include "TPoint3.h"
#include "Eigen/Geometry"
using namespace Eigen;

template <typename T>
class TRotate3 : public Matrix<T,3,3>
{
public:
    // Constructor
    TRotate3() { setIdentity(); }

    // construct TRotate3 from Matrix expressions
    template<typename derived>
    TRotate3(const DenseBase<derived>& other)
        : Matrix(other) { }

    // Assign Matrix expressions to TRotate3
    template<typename derived>
    TRotate3& operator = (const MatrixBase<derived>& other)
    {
        Matrix::operator=(other);
        return *this;
    }

    TRotate3(const Quaternion<T>& q)
    {
        *this = q.toRotationMatrix();
    }

    TRotate3(T m00,T m01,T m02,T m10,T m11,T m12,T m20,T m21,T m22)
    {
        operator()(0,0) = m00;
        operator()(0,1) = m01;
        operator()(0,2) = m02;
        operator()(1,0) = m10;
        operator()(1,1) = m11;
        operator()(1,2) = m12;
        operator()(2,0) = m20;
        operator()(2,1) = m21;
        operator()(2,2) = m22;
    }

    TRotate3(const T(&v)[9])    // assuming row-major input
    {
        operator()(0,0) = v[0];
        operator()(0,1) = v[1];
        operator()(0,2) = v[2];
        operator()(1,0) = v[3];
        operator()(1,1) = v[4];
        operator()(1,2) = v[5];
        operator()(2,0) = v[6];
        operator()(2,1) = v[7];
        operator()(2,2) = v[8];
    }

    TRotate3(T angle, T x1, T x2, T x3)
    {
        *this = AngleAxis<T>(angle, TPoint3<T>(x1,x2,x3).normalized()).matrix();
    }

    TRotate3(T angle, const TPoint3<T>& ax)
    {
        *this = AngleAxis<T>(angle, ax.normalized()).matrix();
    }

    // Multiply the current transformation by a rotation matrix
    void RotateX(T degree)
    {
        *this = AngleAxis<T>(degree * PI / 180, Matrix<T,3,1>::UnitX()) * (*this);
    }

    void RotateY(T degree)
    {
        *this = AngleAxis<T>(degree * PI / 180, Matrix<T,3,1>::UnitY()) * (*this);
    }

    void RotateZ(T degree)
    {
        *this = AngleAxis<T>(degree * PI / 180, Matrix<T,3,1>::UnitZ()) * (*this);
    }

    void GetAxisAngle(TPoint3<T>& ax, T& angle) const
    {
        AngleAxis<T> aa(*this);
        ax = aa.axis();
        angle = aa.angle();
    }

    inline TRotate3<T> Inverse() const
    {
        return transpose();
    }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TRotate3<float> Rotate3Float;
typedef TRotate3<double> Rotate3Double;

#endif //TROTATE3_H
