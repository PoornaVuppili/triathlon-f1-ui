#pragma warning( disable : 4996 4805 )
#include "iwgfx_extern.h"
#pragma warning( default : 4996 4805 )

class CPart;

class CTess : public IwPolygonOutputCallback
	{
public:
	CTess( CPart* pPart );
	~CTess();

	virtual IwStatus OutputPolygon (
		ULONG			lPolygonType,
		ULONG			lNumPoints, 
		IwPoint3d*		aPolygonPoints, 
		IwVector3d*		aPolygonNormals,
		IwPoint2d*		aPolygonUVPoints,
		IwSurface*		pSurface,
		IwFace*			pFace
#ifdef SM_VERSION_STRING
#if SM_VERSION_NUMBER >= 80617
                                   , IwGfxArraySet *pArraySet=NULL
#endif
#endif
		) override;
	
private:
	CPart*			m_pPart;
	};

