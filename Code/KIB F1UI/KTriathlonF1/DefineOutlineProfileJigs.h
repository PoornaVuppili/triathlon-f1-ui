#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Entity.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class CPart;
class IwBSplineCurve;



struct CDefineOutlineProfileJigsUndoData
{
	IwTArray<IwPoint3d>			featurePoints;			
	IwTArray<int>				featurePointsInfo;	
	IwPoint3d					posDroppingCenter;
	IwPoint3d					negDroppingCenter;
	IwTArray<IwPoint3d>			tabPoints;			
	IwTArray<IwPoint3d>			tabEndPoints;
	IwTArray<double>			windowCutAngles;
};

class TEST_EXPORT_TW CDefineOutlineProfileJigs : public CManager
{
    Q_OBJECT

public:
					CDefineOutlineProfileJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CDefineOutlineProfileJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

    virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp=NULL );
    virtual void	Display(Viewport* vp=NULL);
	void			DetermineViewingNormalRegion();
	void			DisplayViewingNormalRegion();
	void			DisplayOutlineFeaturePoints(IwVector3d offset=IwVector3d(0,0,0));
	void			DisplayReferencePoints(IwVector3d offset=IwVector3d(0,0,0));
	void			DisplayTabNCenters(IwVector3d offset=IwVector3d(0,0,0));
	void			DisplayWindowCutTips(IwVector3d offset=IwVector3d(0,0,0));

	static void		CreateOutlineProfileJigsObject(CTotalDoc* pDoc);
	void			GetDefineOutlineProfileJigsUndoData(CDefineOutlineProfileJigsUndoData& undoData);
	void			SetDefineOutlineProfileJigsUndoData(CDefineOutlineProfileJigsUndoData& undoData);
	static IwTArray<IwPoint3d> DetermineTabEndLocations(CTotalDoc* pDoc, IwTArray<IwPoint3d>* oTabPoints=NULL);
	static CEntValidateStatus Validate(CTotalDoc* pDoc, QString &message);
										  
private:
	void			Reset(bool activateUI=true);
	void			CalculateSketchSurfaceNormalInfo();
	void			DetermineOutlineProfileJigs();
	void			DetermineTabLocations();
	void			RefineWindowCutAngles();
	void			DetermineDroppingCenters();
	static bool		DetermineWindowCutDistancesToProfile(CTotalDoc* pDoc, IwTArray<double>& windowCutDistancesToProfile);
	bool			ConvertToArc(int index, IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, double minRadius=5.0);
	bool			ConvertToPoint(int index, IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo);
	IwBSplineCurve* GetOutlineProfileReferenceCurve();
	bool			UpdateTabPointForMouseUpEvent(IwPoint3d caughtPoint, int caughtIndex, IwVector3d viewVec, double maxDeviation=2.0);
	bool			UpdateTabCounterPointForMouseUpEvent(IwPoint3d counterPoint, int counterIndex, IwVector3d viewVec, double maxDeviation, IwPoint3d mainCaughtPoint, double expectedDist);
	bool			IsAValidPosition(IwPoint3d UVPnt, int info);
	static bool		ValidateTabWidth(CTotalDoc* pDoc, IwTArray<double>& tabWidths);
	static bool		ValidateProfileLocation(CTotalDoc* pDoc, IwTArray<IwPoint3d>& points, IwTArray<double>& distances);
	void			SetValidateIcon(CEntValidateStatus vStatus, QString message);

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnAccept();
	void			OnCancel();
	void			OnReset();
	void			OnAdd();
	void			OnDel();
	void			OnValidate();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewBack();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actDefineOutlineProfileJigs;
	QAction*					m_actReset;
	QAction*					m_actAdd;
	QAction*					m_actDel;
	QAction*					m_actValidate;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewRework;

	bool						m_oldInnerSurfaceJigsUpToDate;
	bool						m_oldOuterSurfaceJigsUpToDate;
	bool						m_leftButtonDown;
	bool						m_bMouseMiddleDown;
	int							m_predefinedViewIndex;
	IwVector3d					m_viewingNormal;			// The view normal of the determined ViewingNormalRegion
	int							m_addDeleteMode;			// 0: none, 1: add, 2: delete
	IwTArray<IwVector3d>		m_outlineSurfaceNormal;		// When determine the viewing normal region,
	IwTArray<IwPoint3d>			m_outlineSurfacePointsMinU;	// the outline surface normal and points are 
	IwTArray<IwPoint3d>			m_outlineSurfacePointsMaxU;	// repeatedly referred. Therefore, here we
															// prepare such info to save CPU time.
	IwTArray<IwPoint3d>			m_viewingNormalRegion;
	IwTArray<IwPoint3d>			m_manipulatingNormalRegion;

	IwTArray<IwPoint3d>			m_oldFeaturePoints;			// see outline profile header file for definition
	IwTArray<int>				m_oldFeaturePointsInfo;		// see outline profile header file for definition
	IwTArray<IwPoint3d>			m_currFeaturePoints;		// see outline profile header file for definition
	IwTArray<int>				m_currFeaturePointsInfo;	// see outline profile header file for definition
	IwTArray<IwPoint3d>			m_currFeaturePointsXYZ;		// for display purpose
	IwPoint3d					m_oldPosDroppingCenter;
	IwPoint3d					m_oldNegDroppingCenter;
	IwPoint3d					m_currPosDroppingCenter;	// the point location is actually in the negative side of coordinate origin.
	IwPoint3d					m_currNegDroppingCenter;	// the point location is actually in the positive side of coordinate origin.
	IwTArray<IwPoint3d>			m_oldTabPoints;				// The sequence is: [0~1]:medial anterior tab, [2~3]:lateral middle tab, [4~5]: medial peg tab
	IwTArray<IwPoint3d>			m_currTabPoints;			// The sequence is: [0~1]:medial anterior tab, [2~3]:lateral middle tab, [4~5]:medial peg tab
	IwTArray<IwPoint3d>			m_oldTabEndPoints;			// The sequence is: [0~1]:medial anterior tab, [2~3]:lateral middle tab, [4~5]:medial peg tab
	IwTArray<IwPoint3d>			m_currTabEndPoints;			// The sequence is: [0~1]:medial anterior tab, [2~3]:lateral middle tab, [4~5]:medial peg tab
	IwTArray<double>			m_currWindowCutAngles;		// [0]: positive F1 window cut angle, [1]: negative F1, [2]: positive F2, [3]: negative F2
	IwTArray<double>			m_oldWindowCutAngles;
	IwTArray<IwPoint3d>			m_currWindowCutTips;		// [0]: positive F1 window cut angle, [1]: negative F1, [2]: positive F2, [3]: negative F2
	IwTArray<IwPoint3d>			m_oldWindowCutTips;
	IwTArray<double>			m_windowCutDistancesToProfile;	// // [0]: positive F1 window cut angle, [1]: negative F1, [2]: positive F2, [3]: negative F2

	IwTArray<IwPoint3d>			m_referencePoints;			// for display purpose
	IwAxis2Placement			m_sweepAxis;				// for display purpose
	//// For Validation ///////////////////////////////////////////////////////////////
	CEntValidateStatus			m_validateStatus;
	QString						m_validateMessage;


};