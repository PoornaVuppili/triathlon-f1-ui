#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "SideSurfaceJigs.h"
#include "MakeSideSurfaceJigs.h"
#include "OutlineProfileJigs.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "AdvancedControlJigs.h"
#include "UndoRedoCmdJigs.h"

CMakeSideSurfaceJigs::CMakeSideSurfaceJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
	bool activateUI = (manActType==MAN_ACT_TYPE_EDIT);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMakeSideSurfaceJigs";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;

	if ( activateUI )
	{
		m_toolbar = new QToolBar("Side Surface Jigs");
		m_actMakeSideSurfaceJigs = m_toolbar->addAction("SideSurf");
		SetActionAsTitle( m_actMakeSideSurfaceJigs );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		//m_toolbar->setMovable( false );	
		//m_toolbar->setVisible( true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );

	}

	CSideSurfaceJigs* sideSurfaceJigs = m_pDoc->GetSideSurfaceJigs();
	if ( sideSurfaceJigs == NULL  ) // if no object
	{
		if ( sideSurfaceJigs == NULL )
		{
			sideSurfaceJigs = new CSideSurfaceJigs( m_pDoc, ENT_ROLE_SIDE_SURFACE_JIGS );
			m_pDoc->AddEntity( sideSurfaceJigs, true );
		}

		// Reset is the way to make inner surface jigs.
		Reset( activateUI );
	}
	else
	{
		// Call reset to update inner surface jigs
		if ( !sideSurfaceJigs->GetUpToDateStatus() )
			Reset( activateUI );
	}

	if ( !activateUI )
		OnAccept();

}

CMakeSideSurfaceJigs::~CMakeSideSurfaceJigs()
{
	CSideSurfaceJigs* sideSurfaceJigs = m_pDoc->GetSideSurfaceJigs();
	if ( sideSurfaceJigs != NULL )
	{
		int previoisDesignTime = sideSurfaceJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		sideSurfaceJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}

}

void CMakeSideSurfaceJigs::CreateSideSurfaceJigsObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_SIDE_SURFACE_JIGS) == NULL )
	{
		CSideSurfaceJigs* pSideSurfaceJigs = new CSideSurfaceJigs(pDoc, ENT_ROLE_SIDE_SURFACE_JIGS);
		pDoc->AddEntity(pSideSurfaceJigs, true);
	}

	return;
}

void CMakeSideSurfaceJigs::OnReset()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////
void CMakeSideSurfaceJigs::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CMakeSideSurfaceJigs:Reset()") );

	CSideSurfaceJigs* pSideSurfaceJigs = m_pDoc->GetSideSurfaceJigs();
	if ( pSideSurfaceJigs == NULL )
		return;

	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if (pInnerSurfaceJigs==NULL)
		return;
	IwBrep* innerBrep = pInnerSurfaceJigs->GetIwBrep();
	IwTArray<IwFace*> faces;
	innerBrep->GetFaces(faces);
	IwBSplineSurface* innerSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();

	COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	if (pOuterSurfaceJigs==NULL)
		return;
	IwBrep* outerBrep = pOuterSurfaceJigs->GetIwBrep();
	outerBrep->GetFaces(faces);
	IwBSplineSurface* outerSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();

	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Create side surface
	IwBSplineSurface* sideSurfaceUntrimmed = CreateSideSurfaceUntrimmed(m_pDoc);

	IwBrep* sideBrep = TrimSideSurfaceWithTabs(sideSurfaceUntrimmed, innerSurface, outerSurface);
	sideBrep->SewAndOrient();
	pSideSurfaceJigs->SetIwBrep(sideBrep);
	pSideSurfaceJigs->MakeTess();
	pSideSurfaceJigs->SetModifiedFlag(true);

	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SIDE_SURFACE_JIGS, false, false);

	QApplication::restoreOverrideCursor();

	m_pView->Redraw();

}

void CMakeSideSurfaceJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CMakeSideSurfaceJigs::OnAccept()") );

	CSideSurfaceJigs* pSideSurfaceJigs = m_pDoc->GetSideSurfaceJigs();

	if ( pSideSurfaceJigs && !pSideSurfaceJigs->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SIDE_SURFACE_JIGS, true, true);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	m_bDestroyMe = true;
	m_pView->Redraw();
}

void CMakeSideSurfaceJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CMakeSideSurfaceJigs::OnCancel()") );

	m_bDestroyMe = true;
	m_pView->Redraw();

}

bool CMakeSideSurfaceJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{

	ReDraw();

	return true;
}

bool CMakeSideSurfaceJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{

	ReDraw();

	return true;
}

bool CMakeSideSurfaceJigs::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{

	ReDraw();

	return true;
}

IwBSplineSurface* CMakeSideSurfaceJigs::CreateSideSurfaceUntrimmed(CTotalDoc* pDoc)
{
	// Get sketch surface
	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if (pOutlineProfileJigs==NULL)
		return NULL;
	IwBSplineSurface* sketchSurface;
	pOutlineProfileJigs->GetSketchSurface(sketchSurface);

	// Get uv curve
	IwTArray<IwPoint3d> squaredOffCornerArcs, tiltVectors;
	IwTArray<double> tiltParams;
	IwBSplineCurve *UVCurve;
	UVCurve = pOutlineProfileJigs->GetUVCurveOnSketchSurface(true, NULL, &squaredOffCornerArcs);//true=get square-off posterior tips, if applied.
	// Need to calculate tilt angles if squared-off posterior tips
	if ( squaredOffCornerArcs.GetSize() > 0 ) 
	{
		DetermineTiltVectors(pDoc, UVCurve, squaredOffCornerArcs, tiltParams, tiltVectors);
	}

	// get inner surface
	CInnerSurfaceJigs* pInnerSurfaceJigs = pDoc->GetInnerSurfaceJigs();
	if (pInnerSurfaceJigs==NULL)
		return NULL;
	IwBrep* innerBrep = pInnerSurfaceJigs->GetIwBrep();
	IwTArray<IwFace*> faces;
	innerBrep->GetFaces(faces);
	IwBSplineSurface* innerSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();

	// Get outer surface
	COuterSurfaceJigs* pOuterSurfaceJigs = pDoc->GetOuterSurfaceJigs();
	if (pOuterSurfaceJigs==NULL)
		return NULL;
	IwBrep* outerBrep = pOuterSurfaceJigs->GetIwBrep();
	outerBrep->GetFaces(faces);
	IwBSplineSurface* outerSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();

	// Create the side surface untrimmed
	IwBSplineSurface* sideSurfaceUntrimmed = CMakeSideSurfaceJigs::CreateSideSurfaceWithExtension(pDoc, sketchSurface, 
														UVCurve, tiltParams, tiltVectors, innerSurface, outerSurface);

	return sideSurfaceUntrimmed;
}

IwBSplineSurface* CMakeSideSurfaceJigs::CreateSideSurfaceWithExtension
(
	CTotalDoc* pDoc,				// I:
	IwBSplineSurface*& sketchSurf,	// I:
	IwBSplineCurve*& UVCurve,		// I:
	IwTArray<double>& tiltParams,	// I: represent in UVCurve parameter to tilt the side surface parallel to tiltVectors
	IwTArray<IwVector3d>& tiltVectors,	// I: the side surface has to be parallel with this vectors 
	IwBSplineSurface*& innerSurf,	// I:
	IwBSplineSurface*& outerSurf	// I:
)
{
	IwBSplineSurface* sideSurf = NULL;

	IwContext& iwContext = pDoc->GetIwContext();

	double extendedWidth = pDoc->GetAdvancedControlJigs()->GetSideSurfaceExtendedWidth();
	IwExtent1d uvDom = UVCurve->GetNaturalInterval();
	int nStep = 201;
	double delta = uvDom.GetLength()/(nStep-1);
	IwVector3d normal, tiltNormal;
	IwPoint3d pnt, uv3d;
	IwPoint2d uv2d;
	IwPoint3d intInnerPnt, cPnt;
	IwPoint3d innerPnt, outerPnt;
	double paramT;
	IwTArray<IwBSplineCurve*> rulingLines;
	for (int n=0; n<nStep; n++)
	{
		paramT = uvDom.GetMin() + n*delta;
		UVCurve->EvaluatePoint(paramT, uv3d);
		uv2d = IwPoint2d(uv3d.x, uv3d.y);
		sketchSurf->EvaluatePoint(uv2d, pnt);
		sketchSurf->EvaluateNormal(uv2d, FALSE, FALSE, normal);
		tiltNormal = TiltSideSurfaceRulingLine(paramT, normal, tiltParams, tiltVectors);
		// Intersect with inner surface and outer surface
		IntersectSurfaceByLine(innerSurf, pnt, normal, intInnerPnt); // use pnt and normal of sketchSurf to intersect inner surface
		innerPnt = intInnerPnt;
		innerPnt -= extendedWidth*tiltNormal;
		IntersectSurfaceByLine(outerSurf, intInnerPnt, tiltNormal, cPnt); // use intInnerPnt and tiltNormal of inner surface to intersect outer surface
		outerPnt = cPnt;
		outerPnt += extendedWidth*tiltNormal;
		IwBSplineCurve* rulingLine = NULL;
		IwBSplineCurve::CreateLineSegment(iwContext, 3, innerPnt, outerPnt, rulingLine);
		if (rulingLine == NULL) 
			continue;
		rulingLines.Add(rulingLine);
	}

	// Create sweep surface
	IwBSplineSurface *skinSurface=NULL;
	IwBSplineSurface::CreateSkinnedSurface(iwContext, rulingLines, TRUE, IW_SP_U, 0.0, NULL, NULL, FALSE, NULL, NULL, skinSurface);
	if (skinSurface == NULL) 
		return NULL;

	skinSurface->ReparametrizeWithArcLength();

	sideSurf = skinSurface;

	for (int i=0; i<nStep; i++)
	{
		IwBSplineCurve *line = rulingLines.GetAt(i);
		IwObjDelete delLine(line);
	}

	return sideSurf;
}

IwVector3d CMakeSideSurfaceJigs::TiltSideSurfaceRulingLine
(
	double paramT,						// I:
	IwVector3d& normal,					// I:
	IwTArray<double>& tiltParams,		// I:
	IwTArray<IwVector3d>& tiltVectors	// I:
)
{
	IwVector3d tiltNormal = normal;
	double prevParam, postParam;
	IwVector3d prevVector, postVector;

	for (unsigned i=1; i<tiltParams.GetSize(); i++)
	{
		if (paramT>=tiltParams.GetAt(i-1) && paramT<=tiltParams.GetAt(i))
		{
			prevParam = tiltParams.GetAt(i-1);
			postParam = tiltParams.GetAt(i);
			prevVector = tiltVectors.GetAt(i-1);
			postVector = tiltVectors.GetAt(i);
			break;
		}
	}
	double factor;
	if (prevVector.IsInitialized() && postVector.IsInitialized())
	{
		
		factor = (paramT-prevParam)/(postParam-prevParam);
		tiltNormal = prevVector + factor*(postVector-prevVector);
		tiltNormal.Unitize();
	}

	return tiltNormal;
}

IwBrep* CMakeSideSurfaceJigs::TrimSideSurfaceWithTabs
(
	IwBSplineSurface*& sideSurf,	// I: this is the untrimmed side surface
	IwBSplineSurface*& innerSurf,	// I:
	IwBSplineSurface*& outerSurf	// I:
)
{
	IwContext& iwContext = m_pDoc->GetIwContext();

	// create empty sideBrep
	IwBrep* sideBrep = new (iwContext) IwBrep();

	// Create 3 extra copies of sideSurf. Therefore, 
	// we have 4 side surfaces to define the non tab side faces.
	IwBSplineSurface *sideSurf1, *sideSurf2, *sideSurf3;
	IwSurface* surfCopy;
	sideSurf->Copy(iwContext, surfCopy);
	sideSurf1 = (IwBSplineSurface*)surfCopy;
	sideSurf->Copy(iwContext, surfCopy);
	sideSurf2 = (IwBSplineSurface*)surfCopy;
	sideSurf->Copy(iwContext, surfCopy);
	sideSurf3 = (IwBSplineSurface*)surfCopy;
	// determine parametric domain
	IwExtent2d sideSurfDom = sideSurf->GetNaturalUVDomain();

	// 
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();
	//
	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if (pOutlineProfileJigs==NULL)
		return false;
	// Get the tab points and drop center
	IwTArray<IwPoint3d> tabPoints, tabEndPoints;
	pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
	pOutlineProfileJigs->GetOutlineProfileTabEndPoints(tabEndPoints);
	IwPoint3d posDropCenter, negDropCenter;
	pOutlineProfileJigs->GetDroppingCenters(posDropCenter, negDropCenter);

	// Determine side surface parameter at the tab points
	IwTArray<IwPoint2d> trimParams,trimParamsCCW;
	IwPoint2d param2d;
	IwPoint3d cPnt;
	IwBSplineCurve* trimLine;
	IwTArray<IwBSplineCurve*> trimLines;
	for (unsigned i=0; i<tabPoints.GetSize(); i++)
	{
		DistFromPointToSurface(tabPoints.GetAt(i), sideSurf, cPnt, param2d);
		sideSurf->CreateIsoParametricCurve(iwContext, IW_SP_U, param2d.x, 0.01, trimLine);
		trimLines.Add(trimLine);
		trimParams.Add(param2d);
	}

	// Determine drop centers
	IwTArray<IwPoint3d> dropCenters;
	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		dropCenters.Add(negDropCenter);// for anterior tab
		dropCenters.Add(negDropCenter);// for anterior tab
		dropCenters.Add(posDropCenter);// for mid tab
		dropCenters.Add(posDropCenter);// for mid tab
		dropCenters.Add(negDropCenter);// for peg tab
		dropCenters.Add(negDropCenter);// for peg tab
		// make trimParamsCCW follow CCW direction
		trimParamsCCW.Add(trimParams.GetAt(2));
		trimParamsCCW.Add(trimParams.GetAt(3));
		trimParamsCCW.Add(trimParams.GetAt(5));
		trimParamsCCW.Add(trimParams.GetAt(4));
		trimParamsCCW.Add(trimParams.GetAt(1));
		trimParamsCCW.Add(trimParams.GetAt(0));
	}
	else
	{
		positiveSideIsLateral = false;
		dropCenters.Add(posDropCenter);// for anterior tab
		dropCenters.Add(posDropCenter);// for anterior tab
		dropCenters.Add(negDropCenter);// for mid tab
		dropCenters.Add(negDropCenter);// for mid tab
		dropCenters.Add(posDropCenter);// for peg tab
		dropCenters.Add(posDropCenter);// for peg tab
		// make trimParamsCCW follow CCW direction
		trimParamsCCW.Add(trimParams.GetAt(0));
		trimParamsCCW.Add(trimParams.GetAt(1));
		trimParamsCCW.Add(trimParams.GetAt(4));
		trimParamsCCW.Add(trimParams.GetAt(5));
		trimParamsCCW.Add(trimParams.GetAt(3));
		trimParamsCCW.Add(trimParams.GetAt(2));
	}
 
	// trim sideSurf by sideSurfDom.GetMin() and trimParamsCCW.GetAt(0)
	IwExtent2d trimDom = IwExtent2d(sideSurfDom.GetMin().x, sideSurfDom.GetMin().y, trimParamsCCW.GetAt(0).x, sideSurfDom.GetMax().y);
	sideSurf->TrimWithDomain(trimDom);
	// Add to sideBrep
	IwFace* face;
	sideBrep->CreateFaceFromSurface(sideSurf, trimDom, face);

	// trime sideSurf1 by trimParamsCCW.GetAt(1) and trimParamsCCW.GetAt(2)
	trimDom = IwExtent2d(trimParamsCCW.GetAt(1).x, sideSurfDom.GetMin().y, trimParamsCCW.GetAt(2).x, sideSurfDom.GetMax().y);
	sideSurf1->TrimWithDomain(trimDom);
	// Add to sideBrep
	sideBrep->CreateFaceFromSurface(sideSurf1, trimDom, face);

	// trime sideSurf2 by trimParamsCCW.GetAt(3) and trimParamsCCW.GetAt(4)
	trimDom = IwExtent2d(trimParamsCCW.GetAt(3).x, sideSurfDom.GetMin().y, trimParamsCCW.GetAt(4).x, sideSurfDom.GetMax().y);
	sideSurf2->TrimWithDomain(trimDom);
	// Add to sideBrep
	sideBrep->CreateFaceFromSurface(sideSurf2, trimDom, face);

	// trime sideSurf3 by trimParamsCCW.GetAt(5) and sideSurfDom.GetMax()
	trimDom = IwExtent2d(trimParamsCCW.GetAt(5).x, sideSurfDom.GetMin().y, sideSurfDom.GetMax().x, sideSurfDom.GetMax().y);
	sideSurf3->TrimWithDomain(trimDom);
	// Add to sideBrep
	sideBrep->CreateFaceFromSurface(sideSurf3, trimDom, face);

	//////////////////////////////////////////////////////////////////////////
	// Now add tab side surfaces
	bool isInPositiveSide;
	IwVector3d sideVec;
	IwPoint3d cPnt3d;
	IwPoint3d sPnt, ePnt, sPnt0, ePnt0, sPnt1, ePnt1;
	IwVector3d sVec, fVec, normVec;
	IwPoint2d param0, param1, param2, param3, deltaParam, isoParam, tempVec2d;
	IwPoint3d cPnt0, cPnt1, cPnt2, cPnt3, deltaVec, isoPnt;
	double totalSteps;
	double extendedWidth = m_pDoc->GetAdvancedControlJigs()->GetSideSurfaceExtendedWidth();
	IwTArray<IwBSplineCurve*> rulingLines0, rulingLines1, rulingLines2;
	double ratio;
	for (int i=0; i<3; i++) // 3 tabs
	{
		rulingLines0.RemoveAll();
		rulingLines1.RemoveAll();
		rulingLines2.RemoveAll();

		//// Side surface along first tab profile /////////////////////
		// Determine this tab is in positive or negative side
		sideVec = tabPoints.GetAt(i*2) - sweepAxes.GetOrigin();
		sideVec = sideVec.ProjectToPlane(sweepAxes.GetZAxis());
		if ( sideVec.Dot(sweepAxes.GetXAxis()) > 0 )
			isInPositiveSide = true;
		else
			isInPositiveSide = false;

		// The vector at 0th tab point
		trimLines.GetAt(i*2)->GetEnds(sPnt, ePnt);
		sVec = ePnt - sPnt;
		sVec.Unitize();
		// The vector at 0th tab end point
		fVec = tabPoints.GetAt(i*2) - dropCenters.GetAt(i*2);
		fVec = fVec.ProjectToPlane(sweepAxes.GetZAxis());
		fVec.Unitize();
		// Determine the tab profile near 0th tab point
		DistFromPointToSurface(tabEndPoints.GetAt(i*2), innerSurf, cPnt1, param1);
		// Create lines by (sPnt, sVec), and (cPnt1, fVec)
		IwLine *line0 = NULL, *line1 = NULL;
		double paramT0, paramT1;
		IwLine::CreateCanonical(m_pDoc->GetIwContext(), sPnt, sVec, line0);
		IwLine::CreateCanonical(m_pDoc->GetIwContext(), cPnt1, fVec, line1);
		// Now cPnt0 and cPnt1 are the closest points on both lines
		DistFromCurveToCurve(line0, line1, cPnt0, paramT0, cPnt1, paramT1);
		if ( line0 )
			IwObjDelete(line0);
		if ( line1 )
			IwObjDelete(line1);

		// The side surface along first tab profile will fan along 
		// a line defined by cPnt0 and cPnt1 and rotating from sVec to fVec.

		// how many steps needed between param0 and param1
		tempVec2d = param1 - param0;
		totalSteps = ceil(tempVec2d.Length()/2.0);
		deltaParam = tempVec2d/(totalSteps-1);
		deltaVec = (cPnt1-cPnt0)/(totalSteps-1);
		for ( int s=0; s<totalSteps; s++ ) 
		{
			ratio = (s)/(totalSteps-1);
			normVec = (1-ratio)*sVec + ratio*fVec;// note, normVec is actually not linearly rotating.
			normVec.Unitize();
			isoPnt = cPnt0 + s*deltaVec;
			sPnt = isoPnt;
			IntersectSurfaceByLine(outerSurf, sPnt, normVec, cPnt3d);
			ePnt = cPnt3d;
			// The end point is depended on the outer surface and extended by extendedWidth
			ePnt += extendedWidth*normVec;
			// create ruling line
			IwBSplineCurve* rulingLine = NULL;
			IwBSplineCurve::CreateLineSegment(iwContext, 3, sPnt, ePnt, rulingLine);
			if (rulingLine == NULL) 
				continue;
			rulingLines0.Add(rulingLine);
		}
		// Create skin surface by rulingLines0
		IwBSplineSurface *tabProfileSurface0=NULL;
		IwBSplineSurface::CreateSkinnedSurface(iwContext, rulingLines0, TRUE, IW_SP_U, 0.0, NULL, NULL, FALSE, NULL, NULL, tabProfileSurface0);
		tabProfileSurface0->ReparametrizeWithArcLength();
		if ( !isInPositiveSide )
			tabProfileSurface0->Reverse(IW_SP_U);// To make the surface normal out
		sideBrep->CreateFaceFromSurface(tabProfileSurface0, tabProfileSurface0->GetNaturalUVDomain(), face);

		/////////////////////////////////////////////////////////////
		//// Side surface along second tab profile /////////////////////
		// The vector at 1st tab point
		trimLines.GetAt(i*2+1)->GetEnds(sPnt, ePnt);
		sVec = ePnt - sPnt;
		sVec.Unitize();
		// The vector at fanning distance
		fVec = tabPoints.GetAt(i*2+1) - dropCenters.GetAt(i*2+1);
		fVec = fVec.ProjectToPlane(sweepAxes.GetZAxis());
		fVec.Unitize();
		// Determine the tab profile near 1st tab point
		DistFromPointToSurface(tabEndPoints.GetAt(i*2+1), innerSurf, cPnt3, param3);
		// Create lines by (sPnt, sVec), and (cPnt1, fVec)
		IwLine *line2 = NULL, *line3 = NULL;
		double paramT2, paramT3;
		IwLine::CreateCanonical(m_pDoc->GetIwContext(), sPnt, sVec, line2);
		IwLine::CreateCanonical(m_pDoc->GetIwContext(), cPnt3, fVec, line3);
		// cPnt2 and cPnt3 are the closest points on both lines
		DistFromCurveToCurve(line2, line3, cPnt2, paramT2, cPnt3, paramT3);
		if ( line2 )
			IwObjDelete(line2);
		if ( line3 )
			IwObjDelete(line3);

		// The side surface along first tab profile will fan along 
		// a line defined by cPnt2 and cPnt3 and rotating from sVec to fVec.

		// how many steps needed between param2 and param3
		tempVec2d = param3 - param2;
		totalSteps = ceil(tempVec2d.Length()/2.0);
		deltaParam = tempVec2d/(totalSteps-1);
		deltaVec = (cPnt3-cPnt2)/(totalSteps-1);
		for ( int s=0; s<totalSteps; s++ ) 
		{
			ratio = (s)/(totalSteps-1);
			normVec = (1-ratio)*sVec + ratio*fVec;// note, normVec is actually not linearly rotating.
			isoPnt = cPnt2 + s*deltaVec;
			sPnt = isoPnt;
			IntersectSurfaceByLine(outerSurf, sPnt, normVec, cPnt3d);
			ePnt = cPnt3d;
			// The end point is depended on the outer surface and extended by extendedWidth
			ePnt += extendedWidth*normVec;
			// create ruling line
			IwBSplineCurve* rulingLine = NULL;
			IwBSplineCurve::CreateLineSegment(iwContext, 3, sPnt, ePnt, rulingLine);
			if (rulingLine == NULL) 
				continue;
			rulingLines1.Add(rulingLine);
		}
		// Create skin surface by rulingLines1
		IwBSplineSurface *tabProfileSurface1=NULL;
		IwBSplineSurface::CreateSkinnedSurface(iwContext, rulingLines1, TRUE, IW_SP_U, 0.0, NULL, NULL, FALSE, NULL, NULL, tabProfileSurface1);
		tabProfileSurface1->ReparametrizeWithArcLength();
		if ( isInPositiveSide )
			tabProfileSurface1->Reverse(IW_SP_U);// To make the surface normal out
		sideBrep->CreateFaceFromSurface(tabProfileSurface1, tabProfileSurface1->GetNaturalUVDomain(), face);

		/////////////////////////////////////////////////////////////////////
		//// Surface along tab end profile /////////////////////
		// The vector at 0th tab end point
		rulingLines0.GetLast()->GetEnds(sPnt0, ePnt0);
		sVec = ePnt0 - sPnt0;
		sVec.Unitize();
		// The vector at 1st tab end point
		rulingLines1.GetLast()->GetEnds(sPnt1, ePnt1);
		fVec = ePnt1 - sPnt1;
		fVec.Unitize();
		// Tab end profile
		// how many steps needed between param1 and param3
		tempVec2d = param3 - param1;
		totalSteps = ceil(tempVec2d.Length()/2.0);
		deltaParam = tempVec2d/(totalSteps-1);
		deltaVec = (sPnt1-sPnt0)/(totalSteps-1);
		for ( int s=0; s<(totalSteps); s++ ) 
		{
			ratio = (s)/(totalSteps-1);
			normVec = (1-ratio)*sVec + ratio*fVec;// note, normVec is actually not linearly rotating.
			normVec.Unitize();
			isoPnt = sPnt0 + s*deltaVec;
			IntersectSurfaceByLine(innerSurf, isoPnt, normVec, cPnt3d);
			sPnt = cPnt3d;
			IntersectSurfaceByLine(outerSurf, sPnt, normVec, cPnt3d);
			ePnt = cPnt3d;
			// extend by extendedWidth
			sPnt -= extendedWidth*normVec;
			ePnt += extendedWidth*normVec;
			// create ruling line
			IwBSplineCurve* rulingLine = NULL;
			IwBSplineCurve::CreateLineSegment(iwContext, 3, sPnt, ePnt, rulingLine);
			if (rulingLine == NULL) 
				continue;
			rulingLines2.Add(rulingLine);
		}
		// Create skin surface by rulingLines2
		IwBSplineSurface *tabProfileSurface2=NULL;
		IwBSplineSurface::CreateSkinnedSurface(iwContext, rulingLines2, TRUE, IW_SP_U, 0.0, NULL, NULL, FALSE, NULL, NULL, tabProfileSurface2);
		tabProfileSurface2->ReparametrizeWithArcLength();
		if ( !isInPositiveSide )
			tabProfileSurface2->Reverse(IW_SP_U);// To make surface normal out
		sideBrep->CreateFaceFromSurface(tabProfileSurface2, tabProfileSurface2->GetNaturalUVDomain(), face);

	}


	return sideBrep;
}

bool CMakeSideSurfaceJigs::DetermineTiltVectors
(
	CTotalDoc* pDoc,							// I:
	IwBSplineCurve* UVCurve,					// I:
	IwTArray<IwPoint3d>& squaredOffCornerArcs,	// I: in sketch surface UV domain
	IwTArray<double>& tiltParams,				// O: The side surface has to be tilted at tiltParams with tiltVectors
	IwTArray<IwVector3d>& tiltVectors			// O: such that the side surface in posterior tips parallel with posterior cuts.
)
{
	tiltParams.RemoveAll();
	tiltVectors.RemoveAll();

	int nCornerArcSize = (int)squaredOffCornerArcs.GetSize()/3;
	if ( nCornerArcSize != 4 ) // It has to be 4 squared-off corner arcs 
		return false;

	// Get sketch surface
	COutlineProfileJigs* pOutlineProfileJigs = pDoc->GetOutlineProfileJigs();
	if (pOutlineProfileJigs==NULL)
		return NULL;
	IwBSplineSurface* sketchSurface;
	pOutlineProfileJigs->GetSketchSurface(sketchSurface);
	if (sketchSurface==NULL)
		return false;

	// Get fem axis
	IwAxis2Placement wslAxis = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	IwVector3d postTiltVector = -wslAxis.GetZAxis();

	// UVCurve parametric domain
	IwExtent1d crvDom = UVCurve->GetNaturalInterval();
	double crvLength;
	UVCurve->Length(crvDom, 0.01, crvLength);
	double posCornerArcRadius = squaredOffCornerArcs.GetAt(0).DistanceBetween(squaredOffCornerArcs.GetAt(1));
	double negCornerArcRadius = squaredOffCornerArcs.GetAt(10).DistanceBetween(squaredOffCornerArcs.GetAt(11));

	//
	double transitionFactor = pDoc->GetAdvancedControlJigs()->GetOutlineSquareOffTransitionFactor();
	IwPoint3d cPnt, pnt;
	double dist;
	// the beginning of UVCurve (at seam point)
	double seamPntParam0 = crvDom.Evaluate(0);
	IwVector3d seamPntTiltVector0 = IwVector3d();// un-initialized vector

	// 1st arc 
	IwPoint3d arcStartPnt1 = squaredOffCornerArcs.GetAt(0);
	IwPoint3d arcEndPnt1 = squaredOffCornerArcs.GetAt(2);
	// Determine the corresponding UVCurve parameter
	double arcStartparam1, arcEndparam1;
	dist=DistFromPointToCurve(arcStartPnt1, UVCurve, cPnt, arcStartparam1);
	dist=DistFromPointToCurve(arcEndPnt1, UVCurve, cPnt, arcEndparam1);
	// Determine the tilt vector
	IwVector3d arcStartPntTiltVector1 = postTiltVector;
	IwVector3d arcEndPntTiltVector1 = postTiltVector;

	// prev 1st arc 
	double prevArcStartparam1 = arcStartparam1 - transitionFactor*posCornerArcRadius*crvDom.GetLength()/crvLength;
	UVCurve->EvaluatePoint(prevArcStartparam1, pnt);
	IwVector3d prevArcStartPntTiltVector1;
	sketchSurface->EvaluateNormal(IwPoint2d(pnt.x,pnt.y), FALSE, FALSE, prevArcStartPntTiltVector1);

	// 2nd arc 
	IwPoint3d arcStartPnt2 = squaredOffCornerArcs.GetAt(3);
	IwPoint3d arcEndPnt2 = squaredOffCornerArcs.GetAt(5);
	// Determine the corresponding UVCurve parameter
	double arcStartparam2, arcEndparam2;
	dist=DistFromPointToCurve(arcStartPnt2, UVCurve, cPnt, arcStartparam2);
	dist=DistFromPointToCurve(arcEndPnt2, UVCurve, cPnt, arcEndparam2);
	// Determine the tilt vector
	IwVector3d arcStartPntTiltVector2 = postTiltVector;
	IwVector3d arcEndPntTiltVector2 = postTiltVector;

	// post 2nd arc 
	double postArcEndparam2 = arcEndparam2 + transitionFactor*posCornerArcRadius*crvDom.GetLength()/crvLength;
	UVCurve->EvaluatePoint(postArcEndparam2, pnt);
	IwVector3d postArcEndPntTiltVector2;
	sketchSurface->EvaluateNormal(IwPoint2d(pnt.x,pnt.y), FALSE, FALSE, postArcEndPntTiltVector2);

	// 3rd arc 
	IwPoint3d arcStartPnt3 = squaredOffCornerArcs.GetAt(6);
	IwPoint3d arcEndPnt3 = squaredOffCornerArcs.GetAt(8);
	// Determine the corresponding UVCurve parameter
	double arcStartparam3, arcEndparam3;
	dist=DistFromPointToCurve(arcStartPnt3, UVCurve, cPnt, arcStartparam3);
	dist=DistFromPointToCurve(arcEndPnt3, UVCurve, cPnt, arcEndparam3);
	// Determine the tilt vector
	IwVector3d arcStartPntTiltVector3 = postTiltVector;
	IwVector3d arcEndPntTiltVector3 = postTiltVector;

	// prev 3rd arc 
	double prevArcStartparam3 = arcStartparam3 - transitionFactor*posCornerArcRadius*crvDom.GetLength()/crvLength;
	UVCurve->EvaluatePoint(prevArcStartparam3, pnt);
	IwVector3d prevArcStartPntTiltVector3;
	sketchSurface->EvaluateNormal(IwPoint2d(pnt.x,pnt.y), FALSE, FALSE, prevArcStartPntTiltVector3);

	// 4th arc 
	IwPoint3d arcStartPnt4 = squaredOffCornerArcs.GetAt(9);
	IwPoint3d arcEndPnt4 = squaredOffCornerArcs.GetAt(11);
	// Determine the corresponding UVCurve parameter
	double arcStartparam4, arcEndparam4;
	dist=DistFromPointToCurve(arcStartPnt4, UVCurve, cPnt, arcStartparam4);
	dist=DistFromPointToCurve(arcEndPnt4, UVCurve, cPnt, arcEndparam4);
	// Determine the tilt vector
	IwVector3d arcStartPntTiltVector4 = postTiltVector;
	IwVector3d arcEndPntTiltVector4 = postTiltVector;

	// post 4th arc 
	double postArcEndparam4 = arcEndparam4 + transitionFactor*negCornerArcRadius*crvDom.GetLength()/crvLength;
	UVCurve->EvaluatePoint(postArcEndparam4, pnt);
	IwVector3d postArcEndPntTiltVector4;
	sketchSurface->EvaluateNormal(IwPoint2d(pnt.x,pnt.y), FALSE, FALSE, postArcEndPntTiltVector4);

	// Two more points around the posterior tips
	double posPostTipParam = 0.5*arcEndparam1 + 0.5*arcStartparam2;
	IwVector3d posPostTipTiltVector = postTiltVector;
	double negPostTipParam = 0.5*arcEndparam3 + 0.5*arcStartparam4;
	IwVector3d negPostTipTiltVector = postTiltVector;
	// One more point around notch
	double notchParam = 0.5*arcEndparam2 + 0.5*arcStartparam3;
	IwVector3d notchTiltVector = IwVector3d(); // un-initialized vector

	// the ending of UVCurve (at seam point)
	double seamPntParam1 = crvDom.Evaluate(1.0);
	IwVector3d seamPntTiltVector1 = IwVector3d();// un-initialized vector

	/////////////////////////////////////////////////////////////
	// Add to tiltParams & tiltVectors
	// seam point, beginning
	tiltParams.Add(seamPntParam0);
	tiltVectors.Add(seamPntTiltVector0);
	// prev 1st arc
	tiltParams.Add(prevArcStartparam1);
	tiltVectors.Add(prevArcStartPntTiltVector1);
	// 1st arc
	tiltParams.Add(arcStartparam1);
	tiltVectors.Add(arcStartPntTiltVector1);
	tiltParams.Add(arcEndparam1);
	tiltVectors.Add(arcEndPntTiltVector1);
	// between 1~2 arcs
	tiltParams.Add(posPostTipParam);
	tiltVectors.Add(posPostTipTiltVector);
	// 2nd arc
	tiltParams.Add(arcStartparam2);
	tiltVectors.Add(arcStartPntTiltVector2);
	tiltParams.Add(arcEndparam2);
	tiltVectors.Add(arcEndPntTiltVector2);
	// post 2nd arc
	tiltParams.Add(postArcEndparam2);
	tiltVectors.Add(postArcEndPntTiltVector2);
	// notch
	tiltParams.Add(notchParam);
	tiltVectors.Add(notchTiltVector);
	// prev 3rd arc
	tiltParams.Add(prevArcStartparam3);
	tiltVectors.Add(prevArcStartPntTiltVector3);
	// 3rd arc
	tiltParams.Add(arcStartparam3);
	tiltVectors.Add(arcStartPntTiltVector3);
	tiltParams.Add(arcEndparam3);
	tiltVectors.Add(arcEndPntTiltVector3);
	// between 3~4 arcs
	tiltParams.Add(negPostTipParam);
	tiltVectors.Add(negPostTipTiltVector);
	// 4th arc
	tiltParams.Add(arcStartparam4);
	tiltVectors.Add(arcStartPntTiltVector4);
	tiltParams.Add(arcEndparam4);
	tiltVectors.Add(arcEndPntTiltVector4);
	// post 4th arc
	tiltParams.Add(postArcEndparam4);
	tiltVectors.Add(postArcEndPntTiltVector4);
	// seam point, ending
	tiltParams.Add(seamPntParam1);
	tiltVectors.Add(seamPntTiltVector1);

	return true;
}