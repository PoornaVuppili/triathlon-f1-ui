////////////////////////////////////////////////////////////////
// Functions related to screen measurement result
////////////////////////////////////////////////////////////////

#include "Measure.h"
#include "..\KUtility\TCircle3.h"
#include "..\KUtility\KStatusIndicator.h"

MeasureResult MeasureResult::ComputeScreenDistance(MeasureEntity& e0, MeasureEntity& e1)
{
    MeasureResult r;
    if( e0.selectType != SelectPoint || e1.selectType != SelectPoint )
    {
        kStatus.ShowMessage("Error: unsupported entity type");
        return r;
    }

    r.mode = ScreenDistance;
    r.value.distance.x1 = e0.selectValue.point.x;
    r.value.distance.y1 = e0.selectValue.point.y;
    r.value.distance.z1 = e0.selectValue.point.z;
    r.value.distance.x2 = e1.selectValue.point.x;
    r.value.distance.y2 = e1.selectValue.point.y;
    r.value.distance.z2 = e1.selectValue.point.z;
    double dx = e0.selectValue.point.x - e1.selectValue.point.x;
    double dy = e0.selectValue.point.y - e1.selectValue.point.y;
    double dz = e0.selectValue.point.z - e1.selectValue.point.z;
    r.value.distance.distance = sqrt(dx*dx+dy*dy+dz*dz);
    r.value.distance.isMax = false;
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::ComputeScreenAngle(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2, MeasureEntity& e3)
{
    MeasureResult r;
    if( e0.selectType != SelectPoint || e1.selectType != SelectPoint || e2.selectType != SelectPoint || e3.selectType != SelectPoint )
    {
        kStatus.ShowMessage("Error: unsupported entity type");
        return r;
    }

	Vector3d v1,v2,vc;

	// first 3 if's is to return intuitive angle result when two line segments share a vertex
	if( e0==e3 )
	{
		v1 = Map<const Vector3d>(&e1.selectValue.point.x) - Map<const Vector3d>(&e0.selectValue.point.x);
		v2 = Map<const Vector3d>(&e2.selectValue.point.x) - Map<const Vector3d>(&e3.selectValue.point.x);
		vc = Map<const Vector3d>(&e0.selectValue.point.x);
	}
	else if( e1==e2 )
	{
		v1 = Map<const Vector3d>(&e0.selectValue.point.x) - Map<const Vector3d>(&e1.selectValue.point.x);
		v2 = Map<const Vector3d>(&e3.selectValue.point.x) - Map<const Vector3d>(&e2.selectValue.point.x);
		vc = Map<const Vector3d>(&e1.selectValue.point.x);
	}
	else if( e1==e3 )
	{
		v1 = Map<const Vector3d>(&e0.selectValue.point.x) - Map<const Vector3d>(&e1.selectValue.point.x);
		v2 = Map<const Vector3d>(&e2.selectValue.point.x) - Map<const Vector3d>(&e3.selectValue.point.x);
		vc = Map<const Vector3d>(&e1.selectValue.point.x);
	}
	else
	{
		Map<const Vector3d> m0(&e0.selectValue.point.x);
		Map<const Vector3d> m1(&e1.selectValue.point.x);
		Map<const Vector3d> m2(&e2.selectValue.point.x);
		Map<const Vector3d> m3(&e3.selectValue.point.x);
		
		v1 = m1 - m0;
		v2 = m3 - m2;
		vc = (m0-m2).norm() < (m1-m2).norm() ? (m0 + m2)*.5 : (m1 + m2)*.5;
	}

    r.mode = ScreenAngle;
    r.value.angle.xc = vc.x();
    r.value.angle.yc = vc.y();
    r.value.angle.zc = vc.z();
    r.value.angle.x1 = vc.x() + v1.x();
    r.value.angle.y1 = vc.y() + v1.y();
    r.value.angle.z1 = vc.z() + v1.z();
    r.value.angle.x2 = vc.x() + v2.x();
    r.value.angle.y2 = vc.y() + v2.y();
    r.value.angle.z2 = vc.z() + v2.z();
    r.value.angle.degree = acos(std::min(1.0, std::max(-1.0, v1.normalized().dot(v2.normalized()))))*180/PI;
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::ComputeScreenRadius(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2)
{
    MeasureResult r;
    if( e0.selectType != SelectPoint || e1.selectType != SelectPoint || e2.selectType != SelectPoint )
    {
        kStatus.ShowMessage("Error: unsupported entity type");
        return r;
    }

    Circle3Double c(Map<const Vector3d>(&e0.selectValue.point.x), 
                    Map<const Vector3d>(&e1.selectValue.point.x),
                    Map<const Vector3d>(&e2.selectValue.point.x));

    const Point3Double& ct = c.GetCenter();
    const Vector3Double& nor = c.GetNormal();

    r.mode = ScreenRadius;
    r.value.radius.xc = ct.x();
    r.value.radius.yc = ct.y();
    r.value.radius.zc = ct.z();
    r.value.radius.nx = nor.x();
    r.value.radius.ny = nor.y();
    r.value.radius.nz = nor.z();
    r.value.radius.radius = c.GetRadius();
    r.valid = true;
    return r;
}

