#ifndef TARC3_H
#define TARC3_H

#include "TCircle3.h"

template <typename T>
class TArc3 : public TCircle3<T>
{
public:
    // Construct the circle from center, x-axis, y-axis and radius
    TArc3(const TPoint3<T>& c, const TVector3<T>& x, const TVector3<T>& y, T rad, T a0, T a1)
        : TCircle3(c,x,y,rad), angleFrom(a0), angleTo(a1)
    {
    }

    // Get values
    T GetAngleFrom() const { return angleFrom; }
    T GetAngleTo()   const { return angleTo; }

    bool IsCCW() { return true; }   // assume to be CCW

protected:
    T angleFrom, angleTo;  // start and end angles in rad
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TArc3<int> Arc3Int;
typedef TArc3<float> Arc3Float;
typedef TArc3<double> Arc3Double;

#endif //TARC3_H
