#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

class TEST_EXPORT_TW CMakeSideSurfaceJigs : public CManager
{
    Q_OBJECT

public:
					CMakeSideSurfaceJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CMakeSideSurfaceJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

	virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );

	static void		CreateSideSurfaceJigsObject(CTotalDoc* pDoc);

	static IwBSplineSurface*	CreateSideSurfaceWithExtension(CTotalDoc* pDoc, IwBSplineSurface*& sketchSurf, IwBSplineCurve*& UVCurve, IwTArray<double>& tiltParams, IwTArray<IwVector3d>& tiltVectors, IwBSplineSurface*& innerSurf, IwBSplineSurface*& outerSurf);
	static IwBSplineSurface*	CreateSideSurfaceUntrimmed(CTotalDoc* pDoc);
	IwBrep*			TrimSideSurfaceWithTabs(IwBSplineSurface*& sideSurf, IwBSplineSurface*& innerSurf, IwBSplineSurface*& outerSurf);

private:
	void			Reset(bool activateUI=true);
	static bool		DetermineTiltVectors(CTotalDoc* pDoc, IwBSplineCurve* UVCurve, IwTArray<IwPoint3d>& squaredOffCornerArcs, IwTArray<double>& tiltParams, IwTArray<IwPoint3d>& tiltVectors);
	static IwVector3d TiltSideSurfaceRulingLine(double paramT, IwVector3d& normal, IwTArray<double>& tiltParams, IwTArray<IwVector3d>& tiltVectors);

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnCancel();

protected:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actMakeSideSurfaceJigs;
	QAction*					m_actReset;
	
};
