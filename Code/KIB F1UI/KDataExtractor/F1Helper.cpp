#include "F1Helper.h"
#include "pch.h"

#include "../KTriathlonF1/IFemurImplant.h"

#include <qdir.h>
#include <QTextStream>
#include <QMessageBox>

using namespace std;

F1Helper::F1Helper(std::string& fPath, std::string& surfDir) :m_FPath(fPath), m_strSurfDir(surfDir)
{
	bSWLaunch = false;
}

HRESULT F1Helper::Process()
{
	CoInitialize(NULL);

	HRESULT hr = ConnectSW();
	if (SUCCEEDED(hr))
	{
		hr = OpenFile(m_FPath);
		if (SUCCEEDED(hr))
		{
			ExtractFemoralAxis();
			ExtractPegPositionPoints();
			ExtractCutPointsInfo();
			ExportFemCutIGES();
			ExtractImplantPointsInfo();
			ExportJCurvesToIGES();

			//CheckIfAllStrykerDataExtracted();
		}
		hr = CloseFile(m_FPath);
	}

	hr = CloseSW();
	CoUninitialize();

	return S_OK;
}

void F1Helper::WritePointsDataToFile(std::map<std::string, Triplet> pointMap, std::string strFilePath)
{
	QFile file(strFilePath.c_str());

	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;

	QTextStream out(&file);
	for (auto it = pointMap.begin(); it != pointMap.end(); ++it)
	{
		string strPointName = it->first;
		out << strPointName.c_str() << endl;

		Triplet tri = it->second;
		out << tri.X << ", " << tri.Y << ", " << tri.Z << endl;
	}

	file.close();
}

HRESULT F1Helper::ExportFemCutIGES()
{
	ExportIGSOpts(true);

	string strFemCut = m_strSurfDir + "/FemCut";

	QDir femCutDir(strFemCut.c_str());

	long typeOfCaseSkt = GetTypeOfCaseSketch();

	if (typeOfCaseSkt > 1)
		SetTypeOfCaseSketch(1);

	string strIGESPath = strFemCut + "/CutFemur.igs";

	VARIANT_BOOL bRet;
	m_MdlDoc->SaveAs(CComBSTR(strIGESPath.c_str()), &bRet);

	if (typeOfCaseSkt > 1)
		SetTypeOfCaseSketch(typeOfCaseSkt);

	return S_OK;
}

void F1Helper::ExportIGSOpts(bool isSolid)
{
	m_SwApp->SetUserPreferenceToggle(swIGESExportAsWireframe, false);

	if (isSolid == true)
	{
		m_SwApp->SetUserPreferenceToggle(swIGESExportSolidAndSurface, true);
		m_SwApp->SetUserPreferenceToggle(swIGESExportSketchEntities, false);
		m_SwApp->SetUserPreferenceToggle(swIGESExportFreeCurves, false);
	}
	else
	{
		m_SwApp->SetUserPreferenceToggle(swIGESExportSolidAndSurface, false);
		m_SwApp->SetUserPreferenceToggle(swIGESExportSketchEntities, true);
		m_SwApp->SetUserPreferenceToggle(swIGESExportFreeCurves, true);
	}
}

long F1Helper::GetTypeOfCaseSketch()
{
	m_MdlDoc->EditSketch();

	CComPtr<IModelDocExtension> swDocExt;

	m_MdlDoc->get_Extension(&swDocExt);

	VARIANT_BOOL bRet;
	swDocExt->SelectByID2(CComBSTR("Type of Case@Type of Case Sketch"), CComBSTR("DIMENSION"), 0, 0, 0, false, 0, NULL, 0, &bRet);

	CComPtr<IDispatch> pSelMgrDisp;
	m_MdlDoc->get_SelectionManager(&pSelMgrDisp);

	CComPtr<ISelectionMgr> pSelMgr;
	pSelMgrDisp->QueryInterface(&pSelMgr);

	CComPtr<IDispatch> pDisplyDimDisp;
	pSelMgr->GetSelectedObject6(1, 0, &pDisplyDimDisp);

	CComPtr<IDisplayDimension> pDisplyDim;
	pDisplyDimDisp->QueryInterface(&pDisplyDim);

	CComPtr<IDimension> pDim;
	pDisplyDim->GetDimension2(0, &pDim);

	double retVal = 0;
	pDim->GetValue2(CComBSTR(""), &retVal);

	return retVal;
}

void F1Helper::SetTypeOfCaseSketch(int typeVal)
{
	m_MdlDoc->EditSketch();

	CComPtr<IModelDocExtension> swDocExt;

	m_MdlDoc->get_Extension(&swDocExt);

	VARIANT_BOOL bRet;
	swDocExt->SelectByID2(CComBSTR("Type of Case@Type of Case Sketch"), CComBSTR("DIMENSION"), 0, 0, 0, false, 0, NULL, 0, &bRet);

	CComPtr<IDispatch> pSelMgrDisp;
	m_MdlDoc->get_SelectionManager(&pSelMgrDisp);

	CComPtr<ISelectionMgr> pSelMgr;
	pSelMgrDisp->QueryInterface(&pSelMgr);

	CComPtr<IDispatch> pDisplyDimDisp;
	pSelMgr->GetSelectedObject6(1, 0, &pDisplyDimDisp);

	CComPtr<IDisplayDimension> pDisplyDim;
	pDisplyDimDisp->QueryInterface(&pDisplyDim);

	CComPtr<IDimension> pDim;
	pDisplyDim->GetDimension2(0, &pDim);

	VARIANT var;
	var.vt = VT_BSTR;
	var.bstrVal = CComBSTR("");

	long iRet = 0;
	pDim->SetValue3(typeVal, swSetValue_InThisConfiguration, var, &iRet);
}

HRESULT F1Helper::GetCutPointsOrientationMap(std::map<std::string, std::pair<Triplet, Triplet>>& CordOrientMap)
{
	CordOrientMap = m_CordOrientMap;
	return S_OK;
}

void  GetFeatureByName(string iStrFeatName, IFeature* pFeat, IFeature** pDisFeat)
{
	CComPtr<IDispatch> pNextFeatDisp;
	HRESULT hres = pFeat->GetNextFeature(&pNextFeatDisp);

	while (pNextFeatDisp != NULL)
	{
		CComPtr<IFeature> pJcurveFeat = NULL;
		pNextFeatDisp.QueryInterface(&pJcurveFeat);

		CComBSTR featName;
		hres = pJcurveFeat->get_Name(&featName);

		USES_CONVERSION;

	    string strCurrentFeatName = OLE2A(featName);

		if (strCurrentFeatName == iStrFeatName)
		{
			pNextFeatDisp.QueryInterface(pDisFeat);
			return;
		}

		pNextFeatDisp.Release();;
		hres = pJcurveFeat->GetNextFeature(&pNextFeatDisp);
	}
}

std::string F1Helper::GetFemSize()
{
	CComPtr<ICustomPropertyManager> pCustPropMngr;
	CComPtr<IModelDocExtension> swModExtn;

	HRESULT hr = m_MdlDoc->get_Extension(&swModExtn);

	std::string strconfgi = "";
	hr = swModExtn->get_CustomPropertyManager(CComBSTR(strconfgi.c_str()), &pCustPropMngr);

	std::string strPropName = "Femoral Size";
	CComBSTR fieldName(strPropName.c_str());

	CComBSTR value, resolvedValue;
	VARIANT_BOOL retVal;
	hr = pCustPropMngr->Get3(fieldName, TRUE, &value, &resolvedValue, &retVal);

	USES_CONVERSION;
	string strFemSize = OLE2A(resolvedValue);

	return strFemSize;
}

HRESULT F1Helper::ExportJCurvesToIGES()
{
	ExportIGSOpts(false);

	HRESULT hr = S_OK;
	try
	{

		if (m_MdlDoc == NULL)
			return S_OK;

		string strFemSize = GetFemSize();

		CComQIPtr<IPartDoc> pPartDo(m_MdlDoc);
		std::string strFeatName = "Size " + strFemSize;

		CComBSTR bName(strFeatName.c_str());

		CComPtr<IFeature> pFeat;
		hr = pPartDo->IFeatureByName(bName, &pFeat);

		string strJCurvesDir = m_strSurfDir + "//JCurves";
		QDir jCurveDir(strJCurvesDir.c_str());

		if (!jCurveDir.exists())
		{
			bool bOK = jCurveDir.mkdir(jCurveDir.absolutePath());
		}

		CComPtr<IFeature> pMJcurve;
		std::string strJCurveName = strFeatName + " Medial J Curve";
		GetFeatureByName(strJCurveName, pFeat, &pMJcurve);

		m_MdlDoc->ClearSelection2(VARIANT_TRUE);

		if (pMJcurve == NULL)
			return hr;

		string strJCurveIGESPath = strJCurvesDir + "/Medial_JCurve.igs";
		hr = CreateIGSFromJCurveFeat(pMJcurve, strJCurveIGESPath);

		CComPtr<IFeature> pLJcurveFeature;
		strJCurveName = strFeatName + " Lateral J Curve";
		GetFeatureByName(strJCurveName, pFeat, &pLJcurveFeature);

		m_MdlDoc->ClearSelection2(VARIANT_TRUE);

		if (pLJcurveFeature == NULL)
			return hr;

		strJCurveIGESPath = strJCurvesDir + "/Lateral_JCurve.igs";
		hr = CreateIGSFromJCurveFeat(pLJcurveFeature, strJCurveIGESPath);

		CComPtr<IFeature> pTrJcurveFeature;
		strJCurveName = strFeatName + " Trochlea J Curve";
		GetFeatureByName(strJCurveName, pFeat, &pTrJcurveFeature);

		m_MdlDoc->ClearSelection2(VARIANT_TRUE);

		if (pTrJcurveFeature == NULL)
			return hr;

		strJCurveIGESPath = strJCurvesDir + "/Trochlea_JCurve.igs";
		hr = CreateIGSFromJCurveFeat(pTrJcurveFeature, strJCurveIGESPath);
	}
	catch (...)
	{

	}
	return S_OK;
}

HRESULT F1Helper::CreateIGSFromJCurveFeat(IFeature* pFeat, std::string iStrIGSPath)
{
	HRESULT hr = S_OK;
	m_MdlDoc->Insert3DSketch2(VARIANT_TRUE);
	VARIANT_BOOL vtbool;
	pFeat->Select(VARIANT_TRUE, &vtbool);

	CComPtr<ISketchManager> pSktMgr;
	m_MdlDoc->get_SketchManager(&pSktMgr);

	pSktMgr->SketchUseEdge3(VARIANT_FALSE, VARIANT_FALSE, &vtbool);

	m_MdlDoc->Insert3DSketch2(VARIANT_TRUE);
	CComPtr<IDispatch> pDispLastFeat;
	m_MdlDoc->FeatureByPositionReverse(0, &pDispLastFeat);

	CComQIPtr<IFeature> pCreatedFeat(pDispLastFeat);

	VARIANT_BOOL retVal;

	pCreatedFeat->Select2(VARIANT_FALSE, 0, &retVal);


	m_MdlDoc->EditCopy();

	std::string strPartTempPath = "\\\\fsoemdev\\OEM Files\\Stryker\\Triathlon KIB\\Testing\\Approved Documents\\Part.prtdot";

	CComPtr<IDispatch> pDispDoc;
	hr = m_SwApp->NewDocument(CComBSTR(strPartTempPath.c_str()), 0, 0, 0, &pDispDoc);

	CComQIPtr<IModelDoc2> pMdlDoc(pDispDoc);

	pMdlDoc->Paste();
	std::string strIGsPath = "E:\\PSV\\OEM-ED-00336 02 77-02-016L Femoral Layout Triathlon KIB Left.SLDPRT";
	pMdlDoc->SaveAs(CComBSTR(iStrIGSPath.c_str()), &retVal);

	//strIGsPath = "E:\\PSV\\OEM-ED-00336 02 77-02-016L Femoral Layout Triathlon KIB Left.igs";
	//pMdlDoc->SaveAs(CComBSTR(iStrIGSPath.c_str()), &retVal);

	CComBSTR wTitle;
	pMdlDoc->GetTitle(&wTitle);

	m_SwApp->CloseDoc(wTitle);
	return hr;
}

HRESULT F1Helper::GetPointFromSketch(ISketch* pTdSkt, double* oPnt)
{
	HRESULT hr = E_FAIL;
	if (NULL != pTdSkt)
	{
		long skPointCnt;
		hr = pTdSkt->GetSketchPointsCount2(&skPointCnt);
		if (skPointCnt == 1)
		{
			CComPtr<ISketchPoint> skpnt;
			CComPtr<IDispatch> pntDisp;
			CComVariant skPoints;
			hr = pTdSkt->GetSketchPoints2(&skPoints);
			long zeroIndx = 0;
			if (SUCCEEDED(hr))
				hr = SafeArrayGetElement(skPoints.parray, &zeroIndx, &pntDisp);
			if (NULL != pntDisp)
				pntDisp.QueryInterface(&skpnt);
			if (NULL != skpnt)
			{
				skpnt->get_X(&oPnt[0]);
				skpnt->get_Y(&oPnt[1]);
				skpnt->get_Z(&oPnt[2]);
			}
		}
	}
	return hr;
}

HRESULT F1Helper::ExtractPegPositionPoints()
{
	HRESULT hr = E_FAIL;

	string strFemSize = GetFemSize();

	CComQIPtr<IPartDoc> pPartDo(m_MdlDoc);

	std::map<std::string, Triplet> pointsMap;

	string strMedialPegPoint = "Size " + strFemSize + " Medial Peg Point";

	CComBSTR medialPegPointBName(strMedialPegPoint.c_str());

	CComPtr<IFeature> pMedialPegPointFeat;
	hr = pPartDo->IFeatureByName(medialPegPointBName, &pMedialPegPointFeat);

	CComQIPtr<IDispatch> pMedialPegPointDisp;
	hr = pMedialPegPointFeat->GetSpecificFeature2(&pMedialPegPointDisp);

	CComQIPtr<ISketch> pMedialPegPointSketch;
	hr = pMedialPegPointDisp->QueryInterface(&pMedialPegPointSketch);

	double coord[] = { 0.0, 0.0, 0.0 };
	GetPointFromSketch(pMedialPegPointSketch, coord);

	Triplet tri = { coord[0], coord[1], coord[2] };
	pointsMap["Medial Peg Point"] = tri;

	string strLateralPegPoint = "Size " + strFemSize + " Lateral Peg Point";

	CComBSTR lateralPegPointBName(strLateralPegPoint.c_str());

	CComPtr<IFeature> pLateralPegPointFeat;
	hr = pPartDo->IFeatureByName(lateralPegPointBName, &pLateralPegPointFeat);

	CComQIPtr<IDispatch> pLateralPegPointDisp;
	hr = pLateralPegPointFeat->GetSpecificFeature2(&pLateralPegPointDisp);

	CComQIPtr<ISketch> pLateralPegPointSketch;
	hr = pLateralPegPointDisp->QueryInterface(&pLateralPegPointSketch);

	GetPointFromSketch(pLateralPegPointSketch, coord);

	tri.X = coord[0];
	tri.Y = coord[1];
	tri.Z = coord[2];

	pointsMap["Lateral Peg Point"] = tri;

	string strPegPointsDir = m_strSurfDir + "//PegPoints";
	QDir pegPointsDir(strPegPointsDir.c_str());

	if (!pegPointsDir.exists())
	{
		bool bOK = pegPointsDir.mkdir(pegPointsDir.absolutePath());
	}

	WritePointsDataToFile(pointsMap, strPegPointsDir + "//PegPointsInfo.txt");

	return hr;
}

HRESULT F1Helper::ExtractFemoralAxis()
{
	HRESULT hr;
	string arrOfPointNames[] = { "HIP Point", "Notch Point", "Medial Epicondylar Center", "Lateral Epicondylar OMP" };

	CComQIPtr<IPartDoc> pPartDo(m_MdlDoc);

	std::map<std::string, Triplet> pointsMap;

	for (int idx = 0; idx < 4; idx++)
	{
		CComBSTR bName(arrOfPointNames[idx].c_str());

		CComPtr<IFeature> pFeat;
		hr = pPartDo->IFeatureByName(bName, &pFeat);

		CComQIPtr<IDispatch> pDisp;
		hr = pFeat->GetSpecificFeature2(&pDisp);

		CComQIPtr<ISketch> pSketch;
		hr = pDisp->QueryInterface(&pSketch);

		double coord[] = { 0.0, 0.0, 0.0 };
		GetPointFromSketch(pSketch, coord);

		Triplet tri = { coord[0], coord[1], coord[2] };
		pointsMap[arrOfPointNames[idx]] = tri;
	}

	string strFemAxesDataDir = m_strSurfDir + "//FemAxes";
	QDir femAxesDir(strFemAxesDataDir.c_str());

	if (!femAxesDir.exists())
	{
		bool bOK = femAxesDir.mkdir(femAxesDir.absolutePath());
	}

	WritePointsDataToFile(pointsMap, strFemAxesDataDir + "//AxesInfo.txt");

	return hr;
}

HRESULT F1Helper::ConnectSW()
{
	//get active SW application. 
	CComPtr<IUnknown> pUnk;
	HRESULT hres = ::GetActiveObject(__uuidof(SldWorks), 0, &pUnk);
	if (SUCCEEDED(hres) && NULL != pUnk)
	{
		hres = pUnk.QueryInterface(&m_SwApp);
		pUnk.Release();
		if (FAILED(hres))
			return hres;
	}
	else
	{
		hres = m_SwApp.CoCreateInstance(__uuidof(SldWorks), NULL, CLSCTX_LOCAL_SERVER);
		if (FAILED(hres))
			return hres;

		bSWLaunch = true;
	}

	if (m_SwApp)
	{
		m_SwApp->put_Visible(TRUE);
		m_SwApp->put_UserControl(TRUE);
	}


	return S_OK;
}

HRESULT F1Helper::OpenFile(const std::string& fPath)
{
	CComBSTR docPath(fPath.c_str());
	CComPtr<IDispatch> dispDocSpec;
	HRESULT hr = m_SwApp->GetOpenDocSpec(docPath, &dispDocSpec);
	if (SUCCEEDED(hr) && NULL != dispDocSpec)
	{
		CComPtr<IDocumentSpecification> docSpec;
		hr = dispDocSpec.QueryInterface(&docSpec);
		if (NULL != docSpec)
		{
			hr = docSpec->put_DocumentType(swDocPART);
			hr = docSpec->put_ReadOnly(FALSE);
			hr = docSpec->put_Silent(TRUE);
			hr = m_SwApp->OpenDoc7(docSpec, &m_MdlDoc);
		}
	}
	return hr;
}

HRESULT F1Helper::CloseFile(const std::string& fPath)
{
	CComBSTR docPath(fPath.c_str());
	return m_SwApp->CloseDoc(docPath);
}

HRESULT F1Helper::CloseSW()
{
	HRESULT hr = E_FAIL;
	if (bSWLaunch)
	{
		hr = m_SwApp->CloseAllDocuments(TRUE, NULL);

		m_MdlDoc.Release();
		m_MdlDoc = NULL;

		hr = m_SwApp->ExitApp();
		m_SwApp.Release();
		m_SwApp = NULL;

	}

	return hr;
}

HRESULT F1Helper::ComputeCutPntsAndOrientations()
{
	HRESULT hr = E_FAIL;


	return hr;
}

HRESULT F1Helper::ExtractCutPointsInfo()
{
	std::map<std::string, Triplet> pointMap = GetAll3DPointsFromFolder("Cut Face Points");

	string strCutPointsDir = m_strSurfDir + "//FemCut";
	QDir cutPointsDir(strCutPointsDir.c_str());

	if (!cutPointsDir.exists())
	{
		bool bOK = cutPointsDir.mkdir(cutPointsDir.absolutePath());
	}

	WritePointsDataToFile(pointMap, strCutPointsDir + "//CutPointInfo.txt");

	return S_OK;
}

HRESULT F1Helper::ExtractImplantPointsInfo()
{
	std::map<std::string, Triplet> pointMap = GetAll3DPointsFromFolder("Implant Points");

	string strImplantPointsDir = m_strSurfDir + "//Implant";
	QDir implantPointsDir(strImplantPointsDir.c_str());

	if (!implantPointsDir.exists())
	{
		bool bOK = implantPointsDir.mkdir(implantPointsDir.absolutePath());
	}

	WritePointsDataToFile(pointMap, strImplantPointsDir + "//ImplantInfo.txt");

	return S_OK;
}

std::map<std::string, Triplet> F1Helper::GetAll3DPointsFromFolder(const std::string& strFolderName)
{
	CComQIPtr<IPartDoc> pPartDo(m_MdlDoc);
	CComBSTR bName(strFolderName.c_str());

	CComPtr<IFeature> pFolderFeat;
	pPartDo->IFeatureByName(bName, &pFolderFeat);

	CComPtr<IFeatureFolder> featFolder;
	CComPtr<IDispatch>pFeatFolderDisp;
	pFolderFeat->GetSpecificFeature2(&pFeatFolderDisp);

	if (NULL != pFeatFolderDisp)
		pFeatFolderDisp.QueryInterface(&featFolder);

	CComVariant vSkFeats;
	if (NULL != featFolder)
		featFolder->GetFeatures(&vSkFeats);

	long lIndx = 0, uIndx = 0;
	SafeArrayGetLBound(vSkFeats.parray, 1, &lIndx);
	SafeArrayGetUBound(vSkFeats.parray, 1, &uIndx);

	USES_CONVERSION;

	std::map<std::string, Triplet> pointMap;

	for (long idx = lIndx; idx <= uIndx; idx++)
	{
		CComPtr<IDispatch> pDisp;
		SafeArrayGetElement(vSkFeats.parray, &idx, &pDisp);

		CComPtr<IFeature> fldrFeat;
		if (NULL != pDisp)
			pDisp.QueryInterface(&fldrFeat);

		if (NULL != fldrFeat)
		{
			CComBSTR fTname;
			fldrFeat->GetTypeName2(&fTname);
			std::string strTypeName = OLE2A(fTname);
			if (strTypeName == "3DProfileFeature")
			{
				CComBSTR fName;
				fldrFeat->get_Name(&fName);

				std::string strFeatName = OLE2A(fName);

				CComPtr<IDispatch> skDisp;
				fldrFeat->GetSpecificFeature2(&skDisp);

				CComQIPtr<ISketch> thrDSketch;
				if (NULL != skDisp)
					skDisp.QueryInterface(&thrDSketch);

				double coord[] = { 0.0, 0.0, 0.0 };
				GetPointFromSketch(thrDSketch, coord);

				Triplet tri = { coord[0], coord[1], coord[2] };
				pointMap[strFeatName] = tri;
			}
		}
	}

	return pointMap;
}

void F1Helper::CheckIfAllStrykerDataExtracted(bool showDataExtractionStatus)
{
	QString strMissingData = "";

	//Check if femoral axes is extracted
	map<QString, IwPoint3d> mapOfFemAxesPoints = IFemurImplant::FemoralAxesInp_PointsDataFromStrykerLayout();

	QString arrOfPointNames[] = { "HIP Point", "Notch Point", "Medial Epicondylar Center", "Lateral Epicondylar OMP" };

	QString strMissingFromFemAxes = "";
	for (int idx = 0; idx < 4; idx++)
	{
		if (mapOfFemAxesPoints.find(arrOfPointNames[idx]) == mapOfFemAxesPoints.end())
			strMissingFromFemAxes = strMissingFromFemAxes + "\n" + arrOfPointNames[idx];
	}

	if (strMissingFromFemAxes.length() != 0) strMissingData += "\nFemoral Axes:" + strMissingFromFemAxes + "\n";

	//Check if Fem cut info is extracted
	map<QString, IwPoint3d> cutPointsMap = IFemurImplant::GetStrykerFemLayoutCutPointsMap();

	QString arrOfCutPointsNames[] = {"Anterior Chamfer Cut Face Point", "Anterior Cut Face Point", "Lateral Distal Cut Face Point",
	                                 "Lateral Posterior Chamfer Cut Face Point", "Lateral Posterior Cut Face Point", "Medial Distal Cut Face Point",
	                                 "Medial Posterior Chamfer Cut Face Point", "Medial Posterior Cut Face Point"};

	QString strMissingFromFemCut = "";
	for (int idx = 0; idx < 8; idx++)
	{
		if (cutPointsMap.find(arrOfCutPointsNames[idx]) == cutPointsMap.end())
			strMissingFromFemCut = strMissingFromFemCut + "\n" + arrOfCutPointsNames[idx];
	}

	if (strMissingFromFemCut.length() != 0) strMissingData += "\nFemoral Cut:" + strMissingFromFemCut + "\n";

	IwBrep* pFemCutBrep = IFemurImplant::GetStrykerFemCutBrep();
	if (pFemCutBrep == NULL) strMissingData += "\n missing Stryker femLayout \n";

	//Check if implant info is extracted
	map<QString, IwPoint3d> implantPointMap = IFemurImplant::GetStrykerImplantFeaturePointsMap();
	 
	QString arrOfImplantPointNames[] = { "Medial Posterior Chamfer - Posterior Point", "Medial Posterior Chamfer - Posterior Notch Point", "Lateral Posterior Chamfer - Posterior Notch Point", "Lateral Posterior Chamfer - Posterior Point",
							             "Medial Distal - Posterior Chamfer Point",    "Medial Distal - Posterior Chamfer Notch Point",    "Lateral Distal - Posterior Chamfer Notch Point",    "Lateral Distal - Posterior Chamfer Point",
					               		 "Medial Anterior Chamfer - Distal Point",     "Anterior Center Point",                            "Anterior Point",                                    "Lateral Anterior Chamfer - Distal Point",
						             	 "Medial Anterior - Anterior Chamfer Point",   "Lateral Anterior - Anterior Chamfer Point" };

	QString strMissingFromImplant = "";
	for (int idx = 0; idx < 14; idx++)
	{
		if (implantPointMap.find(arrOfImplantPointNames[idx]) == implantPointMap.end())
			strMissingFromImplant = strMissingFromImplant + "\n" + arrOfImplantPointNames[idx];
	}

	if (strMissingFromImplant.length() != 0) strMissingData += "\nImplant Info:" + strMissingFromImplant + "\n";

	//Check if J Curve info is missing
	IwBSplineCurve* pBSCMedialJCurve = IFemurImplant::JCurvesInp_GetJCurve(0);
	if(pBSCMedialJCurve == NULL) strMissingData += "\n Missing Medial JCurve";

	IwBSplineCurve* pBSCLateralJCurve = IFemurImplant::JCurvesInp_GetJCurve(1);
	if (pBSCLateralJCurve == NULL) strMissingData += "\n Missing Lateral JCurve";

	IwBSplineCurve* pBSCTrochleaJCurve = IFemurImplant::JCurvesInp_GetJCurve(1);
	if (pBSCTrochleaJCurve == NULL) strMissingData += "\n Missing load Trochlea JCurve";

	//check if the peg points info is missing
	IwPoint3d posPegPosition, negPegPosition;
	IFemurImplant::PegPointsFromStrykerFemLayout(posPegPosition, negPegPosition);

	if(posPegPosition.IsInitialized() == false || negPegPosition.IsInitialized() == false) strMissingData += "\n\n Missing Extract peg points \n";

	if (strMissingData.length() != 0)
		QMessageBox::critical(NULL, "Missing Stryker input data", "Below data is missing from fem layout: \n" + strMissingData + "\n Please verify the Stryker fem layout");
	else
	{
		if (showDataExtractionStatus == true)
		{
			QString strMsg = QString("Data Extracted from :\n") + QString(m_FPath.c_str());
			QMessageBox::information(NULL, "Data Extraction Complete", strMsg);
		}
	}
}
