#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"
#include "SelectedEntity.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class QLineEdit;
class QCheckBox;
class IwCircle;

class TEST_EXPORT_TW CMeasureCurveRadius : public CManager
{
    Q_OBJECT

public:
					CMeasureCurveRadius( CTotalView* pView );
	virtual			~CMeasureCurveRadius();
    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseRight ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* viewport=NULL );
	bool			Reject() {OnAccept();return true;};

	virtual void	Display(Viewport* vp=NULL);

private:
	void			SelectEntities(const QPoint& cursor, CSelectedEntity& hitEntity);

public slots:
	void			OnAccept();

private slots:

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QLineEdit*					m_textMeasuredRadius;

	bool						m_mouseLeftDown;
	IwTArray<IwBrep*>			m_selectableBreps;
	IwTArray<bool>				m_selectableBrepsAreOpaque;
	IwTArray<IwCurve*>			m_selectableCurves;
	IwTArray<IwPoint3d>			m_selectablePoints;

	int							m_hitCount;
	CSelectedEntity				m_firstHitEntity;

	IwCircle*					m_circle;
	IwTArray<IwPoint3d>			m_circlePnts;
	IwTArray<IwPoint3d>			m_curvatureHairs;
	int							m_minCurvatureIndex;
};
