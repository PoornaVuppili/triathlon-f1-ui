#pragma once
#include <iostream>
#include <map>
#include "pch.h"
#include <string>

//using namespace sldWorks;

struct Triplet
{
	double X;
	double Y;
	double Z;
};

class F1Helper
{
public:
	F1Helper(std::string& fPath, std::string &surfDir);

	HRESULT Process();
	
	void CheckIfAllStrykerDataExtracted(bool showDataExtractionStatus);

private:
	std::string m_FPath;
	std::string m_strSurfDir;

	std::map<std::string, std::pair<Triplet, Triplet>> m_CordOrientMap;


	CComPtr<ISldWorks> m_SwApp;
	CComPtr<IModelDoc2> m_MdlDoc;
	
	bool bSWLaunch;

	//
	HRESULT ConnectSW();
	HRESULT OpenFile(const std::string& fPath);
	HRESULT CloseFile(const std::string& fPath);
	HRESULT CloseSW();

	HRESULT GetPointFromSketch(ISketch* pTdSkt, double* oPnt);

	std::map<std::string, Triplet> GetAll3DPointsFromFolder(const std::string &strFolderName);

	HRESULT ComputeCutPntsAndOrientations();
	HRESULT CreateIGSFromJCurveFeat(IFeature* pFeat, std::string iStrIGSPath);

	//void GetFeatureByName(string iStrFeatName, IFeature* pFeat, IFeature** pDisFeat);

	long GetTypeOfCaseSketch();
	void SetTypeOfCaseSketch(int typeVal);
	void WritePointsDataToFile(std::map<std::string, Triplet> pointMap, std::string strFilePath);

	HRESULT GetCutPointsOrientationMap(std::map<std::string, std::pair<Triplet, Triplet>>& CordOrientMap);
	HRESULT ExtractFemoralAxis();
	HRESULT ExtractPegPositionPoints();
	HRESULT ExtractCutPointsInfo();
	HRESULT ExtractImplantPointsInfo();

	HRESULT ExportJCurvesToIGES();
	HRESULT ExportFemCutIGES();

	void ExportIGSOpts(bool isSolid);

	std::string GetFemSize();
};