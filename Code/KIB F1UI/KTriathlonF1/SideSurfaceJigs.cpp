#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "SideSurfaceJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

CSideSurfaceJigs::CSideSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = CColor(150, 150, 150);

	m_bSSJigsModified = false;
	m_bSSJigsDataModified = false;

}

CSideSurfaceJigs::~CSideSurfaceJigs()
{
}

void CSideSurfaceJigs::Display()
{

	m_bSSJigsModified = false;

	CPart::Display();

}

bool CSideSurfaceJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	m_bSSJigsDataModified = false;

	return true;
}


bool CSideSurfaceJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);

	m_bSSJigsDataModified = false;

	return true;
}

void CSideSurfaceJigs::SetSSJigsModifiedFlag( bool bModified )
{
	m_bSSJigsModified = bModified;
	if (m_bSSJigsModified)
	{
		m_bSSJigsDataModified = true;
		m_pDoc->SetModified(m_bSSJigsModified);
	}
}
