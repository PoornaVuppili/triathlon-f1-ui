#include "Implant.h"
#include "MainWindow.h"
#include "EntityWidget.h"
#include "ViewWidget.h"
#include "Module.h"
#include "../KUtility/KApplicationMode.h"

#include <QMessageBox>
#include <QTextStream>
#include <QSettings>
#include <qmenubar.h>
#include <qfileinfo.h>

Implant::Implant(QObject *parent)
    : QObject(parent)
    , m_executionMode(ExM_UNSET)
{
}

Implant::~Implant()
{
    ReleaseModules();
}

void Implant::AddModuleMenuToolbar(Module* mod, QMenu* before)
{
    int i;

    if( before )
    {
        // insert before the 'Tool' menu
        for(i=0; i<mod->GetNumberOfMenus(); i++)
            mainWindow->menuBar()->insertMenu(before->menuAction(), mod->GetMenu(i) );
    }
    else
    {
        // add the menu
        for(i=0; i<mod->GetNumberOfMenus(); i++)
            mainWindow->menuBar()->addMenu( mod->GetMenu(i) );
    }
    
    for(i=0; i<mod->GetNumberOfToolBars(); i++) {
        // add to the toolbar
        mainWindow->addToolBar( mod->GetToolBar(i) );
    }
}

// Close the implant
bool Implant::CloseImplant()
{
    // save the changes
    if( IsImplantChanged() )
    {
       if ( !mainWindow->IsReviewMode()
#ifndef TESTING_MODE
               && ((isInProdFolder() ) || !isInProdFolder()) // only ask confirmation if saving is allowed/possible
#endif
		  )
	   {
        switch( QMessageBox::information(mainWindow, "Confirmation", "Save the modified implant?",
                QMessageBox::Yes | QMessageBox::No /*| QMessageBox::Cancel*/ ) )
        {
        case QMessageBox::Yes:
            if( !SaveImplant() )
                return false;
            break;

        case QMessageBox::No:
            break;

		// Too late, no way to cancel. 
        //case QMessageBox::Cancel:
        //    return false;

        }
       }
    }

    // clear the content of widgets
    viewWidget->ClearContent();
    entityWidget->ClearContent();

    // clear the module content
    ClearModule();
    mainWindow->SetReviewMode(false); // clear the case review mode, since the case is closed now

    mainWindow->SetSubTitle(mainWindow->GetAppName() + " " + mainWindow->GetAppVersion());
    EnableActions(false);
    return true;
}

// Get the implant info by name
QString Implant::GetInfo(const QString& name) const
{
    QMessageBox::information( mainWindow, "Error", "Info doesn't exist: " + name, QMessageBox::Ok );
    return "";
}

/*bool Implant::IsSimilarImage(const QImage& img1, const QImage& img2)
{
    return IsSimilarImage(img1, img2, 0.995);
}*/

bool Implant::IsSimilarImage(const QImage& img1, const QImage& img2, double thCor, int compareMode )
{
    if( img1 == img2 )
        return true;

    QSize size = img1.size();
    QImage::Format format = img1.format();

    if( size != img2.size() || 
        format != QImage::Format_RGB32 ||
        format != img2.format() )
        return false;

    const int wid = size.width();
    const int hei = size.height();
    int num=0;// = wid * hei;
    double inum;// = 1.0 / num;

    double uMean1 = 0;
    double vMean1 = 0;
    double wMean1 = 0;

    double uMean2 = 0;
    double vMean2 = 0;
    double wMean2 = 0;

    double uVar1 = 0;
    double vVar1 = 0;
    double wVar1 = 0;

    double uVar2 = 0;
    double vVar2 = 0;
    double wVar2 = 0;

    double uCor12 = 0;
    double vCor12 = 0;
    double wCor12 = 0;

    for(int i=0; i<hei; i++)
    for(int j=0; j<wid; j++)
    {
        int u1,v1,w1;
        int u2,v2,w2;

        QColor(img1.pixel(j,i)).getRgb(&u1, &v1, &w1);
        QColor(img2.pixel(j,i)).getRgb(&u2, &v2, &w2);
		if( compareMode==1 && u1==v1 && u1==w1 && u2==v2 && u2==w2 ) //if color only comparison and not colored
			continue;
		else
			num++;

        uMean1 += u1;
        vMean1 += v1;
        wMean1 += w1;

        uMean2 += u2;
        vMean2 += v2;
        wMean2 += w2;

        uVar1 += u1*u1;
        vVar1 += v1*v1;
        wVar1 += w1*w1;

        uVar2 += u2*u2;
        vVar2 += v2*v2;
        wVar2 += w2*w2;

        uCor12 += u1*u2;
        vCor12 += v1*v2;
        wCor12 += w1*w2;
    }

	inum = 1.0 / num;
    uMean1 *= inum;
    uVar1 = uVar1*inum - uMean1*uMean1;
    uMean2 *= inum;
    uVar2 = uVar2*inum - uMean2*uMean2;

    // correlation of u
    uCor12 = (uCor12*inum - uMean1*uMean2)/sqrt(uVar1*uVar2);

    vMean1 *= inum;
    vVar1 = vVar1*inum - vMean1*vMean1;
    vMean2 *= inum;
    vVar2 = vVar2*inum - vMean2*vMean2;

    // correlation of v
    vCor12 = (vCor12*inum - vMean1*vMean2)/sqrt(vVar1*vVar2);

    wMean1 *= inum;
    wVar1 = wVar1*inum - wMean1*wMean1;
    wMean2 *= inum;
    wVar2 = wVar2*inum - wMean2*wMean2;

    // correlation of w
    wCor12 = (wCor12*inum - wMean1*wMean2)/sqrt(wVar1*wVar2);

    // mean difference
    double um12 = fabs(uMean1 - uMean2);
    double vm12 = fabs(vMean1 - vMean2);
    double wm12 = fabs(wMean1 - wMean2);

    const double thMean = .25;

    return uCor12>thCor && 
           vCor12>thCor && 
           wCor12>thCor &&
           um12<thMean &&
           vm12<thMean &&
           wm12<thMean;
}

// For squish measurement
float Implant::GetMeasurement()
{
    return viewWidget ? viewWidget->GetActiveViewMeasure() : 0;
}

//=============================== configuration work (some of it is overridden in derived Implant class)

bool Implant::LoadConfig(QString const &tableName, QString const &versionToCheck)
{
   Configuration *pC = getOrAddConfiguration(tableName);
   return pC ? pC->Load(versionToCheck) : false;
}


template<class T> T extractTfromConfig(Configuration* pC, QString const &name, T def)
{
   T val;
   if (pC && pC->GetValue(name, val))
      return val;
   else
      return def;
}

template<class T> T extractTfromConfigAndWarnIfMissing(Configuration* pC, QString const &name, T def)
{
   T val;
   if (pC)
   {
      if (pC->GetValue(name, val))
        return val;
      else
      {
         QString text("Variable '%1' is missing.\nTriathlonF1 software needs to be updated.");
         QMessageBox::warning(mainWindow, "Missing Config Variable", text.arg(name), QMessageBox::Ok);
      }
   }

   return def;
}

Configuration* Implant::getOrAddConfiguration(QString const &tableName)
{
   config_t::iterator it = m_configs.find(tableName);
   Configuration *pConfig = nullptr;
   if (it != m_configs.end())
   {
      pConfig = it->second;
   }
   else
   {
      // create one!
      pConfig = createConfig(tableName);
      if (pConfig)
      {
         m_configs[tableName] = pConfig;
      }
   }

   return pConfig;
}

Implant::ExecutionMode Implant::doGetExecutionMode()
{
   int xm = extractTfromConfig(getOrAddConfiguration("default"), "EXECUTION_MODE", int (ExM_UNSET));
   if (xm <= ExM_UNSET || xm >= ExM_ENDOFRANGE) // we didn't find it there (or it's set to wrong value)
   {
      QMessageBox::warning(mainWindow, "Missing parameter",
         "Parameter 'EXECUTION_MODE' is missing from config file\n"
         "or set to invalid value.  Presuming 1 (Development).", QMessageBox::Ok);
      xm = ExM_DEVELOPMENT;
   }
   m_executionMode = (ExecutionMode)xm;
   return m_executionMode;
}


int Implant::getConfigInt(QString const &tableName, QString const &name, int def/*= 0*/)
{
   return extractTfromConfig(getOrAddConfiguration(tableName), name, def);
}

bool Implant::getConfigBoolean(QString const &tableName, QString const &name, bool def/*= false*/)
{
   return extractTfromConfig(getOrAddConfiguration(tableName), name, def);
}

double Implant::getConfigDouble(QString const &tableName, QString const &name, double def/*= DBL_MAX*/)
{
   return extractTfromConfig(getOrAddConfiguration(tableName), name, def);
}

QString Implant::getConfigString(QString const &tableName, QString const &name, QString const &def /*= ""*/)
{
   return extractTfromConfig(getOrAddConfiguration(tableName), name, def);
}

double Implant::getConfigDoubleWarn(QString const &tableName, QString const &name, double def/*= DBL_MAX*/)
{
   return extractTfromConfigAndWarnIfMissing(getOrAddConfiguration(tableName), name, def);
}

QString Implant::getConfigStringWarn(QString const &tableName, QString const &name, QString const &def /*= ""*/)
{
   return extractTfromConfigAndWarnIfMissing(getOrAddConfiguration(tableName), name, def);
}

QString Implant::strProdFolder()
{
	static QString prodFolder = getConfigStringWarn("default", "PRODUCTION FOLDER", "X:/UNI");

	return prodFolder;
}

bool Implant::isInProdFolder()
{
	QString myFolder = GetInfo("ImplantDir");
	QString prodFolder = strProdFolder();
	if (QFileInfo(myFolder).canonicalFilePath().startsWith(prodFolder, Qt::CaseInsensitive))
	{
		// what's the next symbol?  If it's a backslash or slash, we're there!
		myFolder.remove(0, prodFolder.length());
		return (myFolder[0] == '\\' || myFolder[0] == '/');
	}
	return false;
}