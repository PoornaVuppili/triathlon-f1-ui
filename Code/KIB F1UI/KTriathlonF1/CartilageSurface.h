#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

///////////////////////////////////////////////////////////////////////
// There are significant changes in iTW6. There exists cartilage boundary
// to define the cartilage region near weight-bearing area. In iTW6,
// there is no such cartilage boundary. The whole femur surface will
// be offset 3mm, plus the extra offset near the trochlear groove. 
///////////////////////////////////////////////////////////////////////

class TEST_EXPORT_TW CCartilageSurface : public CPart
{
public:
	CCartilageSurface( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_CARTILAGE_SURFACE, const QString& sFileName="", int nEntId=-1 );
	~CCartilageSurface();
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );

	bool			IsMemberDataChanged(IwTArray<IwPoint3d>& troPoints);
	IwBSplineCurve* GetTrochlearGrooveXYZCurve() {return m_trochlearGrooveXYZCurve;};
	IwBSplineCurve* GetTrochlearGrooveUVCurve() {return m_trochlearGrooveUVCurve;};
	void			SetTrochlearGroovePoints(IwTArray<IwPoint3d>& troPoints);
	void			GetTrochlearGroovePoints(IwTArray<IwPoint3d>& troPoints);
	void			SetTrochlearGrooveRefPoints(IwTArray<IwPoint3d>& troRefPoints);
	void			GetTrochlearGrooveRefPoints(IwTArray<IwPoint3d>& troRefPoints);
	void			ConvertUVToXYZ(IwTArray<IwPoint3d>& uvPoints, IwTArray<IwPoint3d>& xyzPoints, IwTArray<IwPoint3d> *xyzNormals);
	IwBSplineSurface* GetSinglePatchSurface(bool smooth=false);
	void			GetSideSurfaces(IwBSplineSurface*& leftSurface, IwBSplineSurface*& rightSurface);
	virtual void	SetIwBrep( IwBrep* pBrep );
	static void		UpdateSurfacebyCtrlPoints(CTotalDoc* pDoc, IwBSplineSurface*& surface, IwTArray<ULONG>& ctrlPntIndexes, IwTArray<IwPoint3d>& ctrlPnts);

private:
	void			SetCSModifiedFlag( bool bModified );
	void			CreateTrochlearGrooveCurve();
	void			DisplayTrochlearGrooveCurve();
	void			DisplayTrochlearGroovePoints();

private:
	bool			m_bCSModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bCSDataModified;// Geometric entities have not been saved yet to the file. 

	long					m_glTrochlearCurveList;
	
	IwTArray<IwPoint3d>		m_trochlearGroovePoints;// (x,y,z)=(u,v,0)
	IwBSplineCurve*			m_trochlearGrooveXYZCurve;
	IwBSplineCurve*			m_trochlearGrooveUVCurve;
	IwTArray<IwPoint3d>		m_trochlearGrooveRefPoints;// Represent good locations for control points. For validation purpose;
	IwBSplineSurface*		m_smoothMainSurface;// the single surface in the main area
};