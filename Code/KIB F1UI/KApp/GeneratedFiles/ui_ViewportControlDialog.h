/********************************************************************************
** Form generated from reading UI file 'ViewportControlDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWPORTCONTROLDIALOG_H
#define UI_VIEWPORTCONTROLDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_ViewportControlDialog
{
public:
    QPushButton *pushButtonOK;
    QLabel *label;
    QTableWidget *tableWidget;

    void setupUi(QDialog *ViewportControlDialog)
    {
        if (ViewportControlDialog->objectName().isEmpty())
            ViewportControlDialog->setObjectName(QString::fromUtf8("ViewportControlDialog"));
        ViewportControlDialog->resize(392, 317);
        pushButtonOK = new QPushButton(ViewportControlDialog);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));
        pushButtonOK->setGeometry(QRect(280, 280, 97, 25));
        label = new QLabel(ViewportControlDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 11, 309, 21));
        tableWidget = new QTableWidget(ViewportControlDialog);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(20, 32, 353, 241));

        retranslateUi(ViewportControlDialog);

        pushButtonOK->setDefault(true);


        QMetaObject::connectSlotsByName(ViewportControlDialog);
    } // setupUi

    void retranslateUi(QDialog *ViewportControlDialog)
    {
        ViewportControlDialog->setWindowTitle(QCoreApplication::translate("ViewportControlDialog", "Control viewport display", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("ViewportControlDialog", "OK", nullptr));
        label->setText(QCoreApplication::translate("ViewportControlDialog", "Viewports:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ViewportControlDialog: public Ui_ViewportControlDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWPORTCONTROLDIALOG_H
