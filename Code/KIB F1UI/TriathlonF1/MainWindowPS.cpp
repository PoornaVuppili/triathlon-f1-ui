#include "MainWindowPS.h"
#include "SelectImplant.h"
#include "SelectImplantSimple.h"
#include "..\KUtility\KUtility.h"
#include "ImplantTotalPS.h"

#include "..\KDataExtractor\F1Helper.h"

#include <assert.h>
#include <qsettings.h>
#include <qtoolbar.h>
#include <qactiongroup.h>
#include <QTimer>

MainWindowPS::MainWindowPS(bool reviewmode, bool skipct, QWidget *parent, Qt::WindowFlags flags)
    : MainWindow(reviewmode || skipct, parent, flags)	// skipCT always in review mode
{
    setWindowIcon( QIcon( ":/Resources/TriathlonF1.png" ) );

	skipCT = skipct;

    // initialize the modules
    implant = new ImplantTotalPS();
    implant->InitializeModules(menuTool);
	

    SetSubTitle(GetAppName() + " " + GetAppVersion());
}

MainWindowPS::~MainWindowPS()
{

}

// Open an existing implant
void MainWindowPS::OpenImplant()
{
    // select the last implant at the beginning
    QSettings settings;
    QString implantName = settings.value("ImplantName", "").toString();

	if( skipCT )
	{
		SelectImplantSimple sis(this);
		sis.open();
		sis.SetImplantName(implantName);
		if( sis.exec() == QDialog::Rejected )
			return;

		if( implant->HasImplant() )
		{
			CloseImplant(false);
			assert( !implant->HasImplant() );
		}

		ResetContext();

		implantName = sis.GetImplantName();

		// Load the implant
		if( !((ImplantTotalPS*)implant)->LoadImplant(&sis) )
			return;
	}
	else
	{
        SelectImplant si(this);
    
		si.open();
		si.SetImplantName(implantName);
		if( si.exec() == QDialog::Rejected )
			return;

		if( implant->HasImplant() )
		{
			if( QMessageBox::information(this, "Confirmation", 
				"Opening a new implant will close all currently open implant files. Continue?",
				QMessageBox::Yes | QMessageBox::No ) == QMessageBox::No )
				return;

			CloseImplant(false);
			assert(!implant->HasImplant());
		}

		QString strFileName = "";
		QString sldworksDir = si.GetSolidWorksDir();
		QString surfDir = si.GetSurfaceDir();

		QDir solidWorksDir(sldworksDir);
		if (solidWorksDir.exists())
		{
			QFileInfoList infoList = solidWorksDir.entryInfoList(QStringList() << "*.sldprt" << "*.SLDPRT", QDir::Files);
			for (int i = 0; i < infoList.size(); i++)
			{
				strFileName = infoList[i].fileName();

				if (strFileName.contains("Femoral Layout")) break;
			}
		}

		QString strFilePath = sldworksDir + QDir::separator() + strFileName;
		F1Helper objF1Helper(strFilePath.toStdString(), surfDir.toStdString());

		bool ifDataExtYes = false;
		if (QMessageBox::question(this, "Data Extraction", "Do you want to Extract/Update Input data from Stryker fem layout",
			QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
		{
			objF1Helper.Process();
			ifDataExtYes = true;
		}

		// detect lock
		QDir segDir(si.GetSegmentDir());
		QString userPath = segDir.absoluteFilePath("user.txt");
		if( !lock.Create(userPath) && QFile::exists(userPath) )
		{
			QFile file(userPath);
			if( file.open(QIODevice::ReadOnly | QIODevice::Text) )
			{
				QString name = file.readLine();
				file.close();

				if( QMessageBox::information(this, "Confirmation", 
					name + " is processing the case. Continue?",
					QMessageBox::Yes | QMessageBox::No ) == QMessageBox::No )
					return;
			}
			lock.Release();
		}

		ResetContext();

		implantName = si.GetImplantName();

		// Load the implant
		if( !((ImplantTotalPS*)implant)->LoadImplant(&si) )
			return;

		objF1Helper.CheckIfAllStrykerDataExtracted(ifDataExtYes);
	}

    settings.setValue("ImplantName", implantName);

    UpdateActions(true);
}

// Set options
void MainWindowPS::SetOption()
{
    ((ImplantTotalPS*)implant)->SetOption();
}

void MainWindowPS::ShowStatusPrompt(QString const& msg)
{
	QPalette palette;
	palette.setColor(QPalette::WindowText, Qt::black);
	statusBar()->setPalette( palette );
	statusBar()->showMessage(msg);
}

void MainWindowPS::ShowStatusResult(bool result_status, QString const& msg, int timeout)
{
	QPalette palette;
	palette.setColor(QPalette::WindowText, result_status ?  Qt::darkGreen : Qt::red);
	statusBar()->setPalette( palette );
	statusBar()->showMessage(msg, timeout);
}
