/****************************************************************************
** Meta object code from reading C++ file 'TotalViewPS.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../TotalViewPS.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TotalViewPS.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KTotalViewPS_t {
    QByteArrayData data[17];
    char stringdata0[294];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KTotalViewPS_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KTotalViewPS_t qt_meta_stringdata_KTotalViewPS = {
    {
QT_MOC_LITERAL(0, 0, 12), // "KTotalViewPS"
QT_MOC_LITERAL(1, 13, 23), // "OnAutoInitializeCBSteps"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 6), // "action"
QT_MOC_LITERAL(4, 45, 27), // "OnActivateNextReviewManager"
QT_MOC_LITERAL(5, 73, 9), // "CManager*"
QT_MOC_LITERAL(6, 83, 19), // "pManagerToBeDeleted"
QT_MOC_LITERAL(7, 103, 8), // "CEntRole"
QT_MOC_LITERAL(8, 112, 5), // "eRole"
QT_MOC_LITERAL(9, 118, 5), // "bNext"
QT_MOC_LITERAL(10, 124, 23), // "OnGoToJigsReviewManager"
QT_MOC_LITERAL(11, 148, 26), // "OnAcceptValidationReportPS"
QT_MOC_LITERAL(12, 175, 25), // "OnPrintValidationReportPS"
QT_MOC_LITERAL(13, 201, 14), // "outputFileName"
QT_MOC_LITERAL(14, 216, 29), // "GetValidationReportPSTextEdit"
QT_MOC_LITERAL(15, 246, 10), // "QTextEdit*"
QT_MOC_LITERAL(16, 257, 36) // "OnSetModifiedMarkerToFemurPSF..."

    },
    "KTotalViewPS\0OnAutoInitializeCBSteps\0"
    "\0action\0OnActivateNextReviewManager\0"
    "CManager*\0pManagerToBeDeleted\0CEntRole\0"
    "eRole\0bNext\0OnGoToJigsReviewManager\0"
    "OnAcceptValidationReportPS\0"
    "OnPrintValidationReportPS\0outputFileName\0"
    "GetValidationReportPSTextEdit\0QTextEdit*\0"
    "OnSetModifiedMarkerToFemurPSFeatures"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KTotalViewPS[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x0a /* Public */,
       1,    0,   77,    2, 0x2a /* Public | MethodCloned */,
       4,    3,   78,    2, 0x0a /* Public */,
       4,    2,   85,    2, 0x2a /* Public | MethodCloned */,
       4,    1,   90,    2, 0x2a /* Public | MethodCloned */,
       4,    0,   93,    2, 0x2a /* Public | MethodCloned */,
      10,    0,   94,    2, 0x0a /* Public */,
      11,    0,   95,    2, 0x0a /* Public */,
      12,    1,   96,    2, 0x0a /* Public */,
      12,    0,   99,    2, 0x2a /* Public | MethodCloned */,
      14,    0,  100,    2, 0x0a /* Public */,
      16,    0,  101,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Bool, QMetaType::Int,    3,
    QMetaType::Bool,
    QMetaType::Void, 0x80000000 | 5, 0x80000000 | 7, QMetaType::Bool,    6,    8,    9,
    QMetaType::Void, 0x80000000 | 5, 0x80000000 | 7,    6,    8,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void,
    0x80000000 | 15,
    QMetaType::Void,

       0        // eod
};

void KTotalViewPS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KTotalViewPS *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->OnAutoInitializeCBSteps((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 1: { bool _r = _t->OnAutoInitializeCBSteps();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 2: _t->OnActivateNextReviewManager((*reinterpret_cast< CManager*(*)>(_a[1])),(*reinterpret_cast< CEntRole(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 3: _t->OnActivateNextReviewManager((*reinterpret_cast< CManager*(*)>(_a[1])),(*reinterpret_cast< CEntRole(*)>(_a[2]))); break;
        case 4: _t->OnActivateNextReviewManager((*reinterpret_cast< CManager*(*)>(_a[1]))); break;
        case 5: _t->OnActivateNextReviewManager(); break;
        case 6: _t->OnGoToJigsReviewManager(); break;
        case 7: _t->OnAcceptValidationReportPS(); break;
        case 8: _t->OnPrintValidationReportPS((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->OnPrintValidationReportPS(); break;
        case 10: { QTextEdit* _r = _t->GetValidationReportPSTextEdit();
            if (_a[0]) *reinterpret_cast< QTextEdit**>(_a[0]) = std::move(_r); }  break;
        case 11: _t->OnSetModifiedMarkerToFemurPSFeatures(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KTotalViewPS::staticMetaObject = { {
    QMetaObject::SuperData::link<CTotalView::staticMetaObject>(),
    qt_meta_stringdata_KTotalViewPS.data,
    qt_meta_data_KTotalViewPS,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KTotalViewPS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KTotalViewPS::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KTotalViewPS.stringdata0))
        return static_cast<void*>(this);
    return CTotalView::qt_metacast(_clname);
}

int KTotalViewPS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CTotalView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
