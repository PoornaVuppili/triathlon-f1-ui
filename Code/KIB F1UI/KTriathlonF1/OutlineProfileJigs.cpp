#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "OutlineProfileJigs.h"
#include "FemoralPart.h"
#include "CartilageSurface.h"
#include "InnerSurfaceJigs.h"
#include "StylusJigs.h"
#include "AdvancedControlJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"
#include "IFemurImplant.h"


COutlineProfileJigs::COutlineProfileJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_TRANSPARENCY;

	m_color = gray;
	m_sketchSurface = NULL;
	m_glOutlineProfileCurveList = 0;
	m_outlineProfileJigsModified = false;
	m_bOPJigsDataModified = false;
	m_outlineProfileDisplaySketchSurface = false;
	for (int i=0; i<4; i++)
		m_windowCutAngles.Add(10);// all 10 degrees
	m_medialJumpCurve = NULL;
	m_lateralJumpCurve = NULL;
	m_displayedMsgForEstimatedPegPosition = 0;
}

COutlineProfileJigs::~COutlineProfileJigs()
{

}

void COutlineProfileJigs::Display()
{
	// display outline profile here

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	// Display Outline Profile
	glDisable( GL_LIGHTING );

	if( m_outlineProfileJigsModified && m_glOutlineProfileCurveList > 0 )
		DeleteDisplayList( m_glOutlineProfileCurveList );

	if( m_glOutlineProfileCurveList == 0 )
	{
		m_glOutlineProfileCurveList = NewDisplayList();
		glNewList( m_glOutlineProfileCurveList, GL_COMPILE_AND_EXECUTE );
		DisplayOutlineProfile();
		DisplayCoreHoleProfiles();
		//DisplayOutlineFeaturePoints();
		glEndList();
	}
	else
	{
		glCallList( m_glOutlineProfileCurveList );
	}

	if ( m_outlineProfileDisplaySketchSurface )
	{
		// Make the sketch surface more transparent
		double oldTransparentFactor = m_pDoc->GetTransparencyFactor();
		double thisTransparentFactor = 0; // Make it totally transparent
		m_pDoc->SetTransparencyFactor(thisTransparentFactor);
		CPart::Display();
		m_pDoc->SetTransparencyFactor(oldTransparentFactor);
	}

	glEnable( GL_LIGHTING );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}
   
	if(m_posPegPosition.IsInitialized() && m_negPegPosition.IsInitialized())
	{
	    glPointSize(6);
		glColor3ub(0, 0, 255);
		glBegin( GL_POINTS );

		     glVertex3d( m_posPegPosition.x, m_posPegPosition.y, m_posPegPosition.z );
		     glVertex3d( m_negPegPosition.x, m_negPegPosition.y, m_negPegPosition.z );

		glEnd();
	}

	m_outlineProfileJigsModified = false;

}

void COutlineProfileJigs::DisplayOutlineFeaturePoints()
{
	IwPoint3d pnt;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	for (unsigned i=0; i<m_outlineProfileFeaturePointsInfo.GetSize(); i++)
	{
		m_sketchSurface->EvaluatePoint(IwVector2d(m_outlineProfileFeaturePoints.GetAt(i).x, m_outlineProfileFeaturePoints.GetAt(i).y), pnt);

		if (i==0 || i== (m_outlineProfileFeaturePointsInfo.GetSize()-1)) // seam point
		{
			glColor3ub(0, 0, 0);
			glPointSize(8);
		}
		else if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 0) // interpolation points
		{
			{
				glColor3ub(0, 0, 255);
				glPointSize(6);
			}
		}
		else if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 1 || // arc center points
				 m_outlineProfileFeaturePointsInfo.GetAt(i) == 2)
		{
			glColor3ub( 98, 0, 255 );
			glPointSize(6);
		}
		else if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 3 || // arc start points
			m_outlineProfileFeaturePointsInfo.GetAt(i) == 4)	    // arc end points
		{
			glColor3ub( 128, 0, 255 );
			glPointSize(6);
		}

		glBegin( GL_POINTS );

			glVertex3d( pnt.x, pnt.y, pnt.z );

		glEnd();
	}

	glPointSize(1);

}

void COutlineProfileJigs::DisplayOutlineProfile()
{
	IwPoint3d pnt;
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glLineWidth(3.0);
	glColor4ub( 127, 255, 0, 64);

	if (m_outlineProfileProjectedCurve.GetSize() > 0)
	{
		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < m_outlineProfileProjectedCurve.GetSize(); k++ ) 
		{
			pnt = m_outlineProfileProjectedCurve.GetAt(k);
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}

		glEnd();
	}
	//return;//PSV
	if (m_tabProfiles.GetSize() > 0)
	{
		// Display tab profiles
		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < m_tabProfiles.GetSize(); k++ ) 
		{
			pnt = m_tabProfiles.GetAt(k);
			if ( pnt == IwPoint3d(-111,-111,-111) )// special point to indicate the end of a tab profile
			{
				glEnd();
				glBegin( GL_LINE_STRIP );
				continue;
			}
			else if ( pnt == IwPoint3d(-11111,-11111,-11111) )// special point to indicate the tab end length is 1
			{
				continue;
			}
			else if ( pnt == IwPoint3d(-33333,-33333,-33333) )// special point to indicate the tab end length is 3
			{
				continue;
			}
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}

		glEnd();

		// Display 1~3mm from the tab end
		bool drawThePoint = false;
		glLineWidth(4.0);
		glColor4ub( 64, 125, 0, 64);
		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < m_tabProfiles.GetSize(); k++ ) 
		{
			pnt = m_tabProfiles.GetAt(k);
			if ( pnt == IwPoint3d(-111,-111,-111) )// special point to indicate the end of a tab profile
			{
				glEnd();
				glBegin( GL_LINE_STRIP );
				continue;
			}
			else if ( pnt == IwPoint3d(-11111,-11111,-11111) )// special point to indicate the tab end length is 1
			{
				drawThePoint = true;
				continue;
			}
			else if ( pnt == IwPoint3d(-33333,-33333,-33333) )// special point to indicate the tab end length is 3
			{
				drawThePoint = false;
				continue;
			}
			if ( drawThePoint )
				glVertex3d( pnt.x, pnt.y, pnt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);

}

void COutlineProfileJigs::DisplayCoreHoleProfiles()
{
	IwPoint3d pnt;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(3.0);

	glColor4ub( 64, 192, 0, 64);

	//
	if ( m_coreHoleProfile.GetSize() > 0 )
	{
		glBegin( GL_LINE_STRIP );
		for( ULONG k = 0; k < m_coreHoleProfile.GetSize(); k++ ) 
		{
			pnt = m_coreHoleProfile.GetAt(k);
			if ( pnt == IwPoint3d(-1,-1,-1) )
			{
				glEnd();
				glColor4ub( 64, 192, 0, 64);
				glBegin( GL_LINE_STRIP );
			}
			else if ( pnt == IwPoint3d(-2,-2,-2) )
			{
				glEnd();
				glColor4ub( 56, 152, 0, 64);
				glBegin( GL_LINE_STRIP );
			}
			else
			{
				glVertex3d( pnt.x, pnt.y, pnt.z );
			}
		}
		glEnd();
	}

	glLineWidth(1.0);

}

bool COutlineProfileJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool				bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bOPJigsDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_op_jigs.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	// feature points
	unsigned nSize = m_outlineProfileFeaturePoints.GetSize();

	File.write( (char*) &nSize, sizeof(unsigned) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d featurePoint = m_outlineProfileFeaturePoints.GetAt(i);
		File.write( (char*) &featurePoint, sizeof( IwPoint3d ) );
	}
	for (unsigned i=0; i<nSize; i++)
	{
		int info = m_outlineProfileFeaturePointsInfo.GetAt(i);
		File.write( (char*) &info, sizeof( int ) );
	}
	
	// dropping centers
	File.write( (char*) &m_posDroppingCenter, sizeof( IwPoint3d ) );
	File.write( (char*) &m_negDroppingCenter, sizeof( IwPoint3d ) );

	// tab points
	nSize = m_outlineProfileTabPoints.GetSize();

	File.write( (char*) &nSize, sizeof(unsigned) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d tabPoint = m_outlineProfileTabPoints.GetAt(i);
		File.write( (char*) &tabPoint, sizeof( IwPoint3d ) );
	}
	// tab end points
	nSize = m_outlineProfileTabEndPoints.GetSize();

	File.write( (char*) &nSize, sizeof(unsigned) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d tabEndPoint = m_outlineProfileTabEndPoints.GetAt(i);
		File.write( (char*) &tabEndPoint, sizeof( IwPoint3d ) );
	}

	// reference points
	nSize = m_referencePoints.GetSize();

	File.write( (char*) &nSize, sizeof(unsigned) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d refPoint = m_referencePoints.GetAt(i);
		File.write( (char*) &refPoint, sizeof( IwPoint3d ) );
	}

	//posterior ref line points
	File.write((char*)&m_posReflinePnt1, sizeof(IwPoint3d));
	File.write((char*)&m_posReflinePnt2, sizeof(IwPoint3d));
	File.write((char*)&m_negReflinePnt1, sizeof(IwPoint3d));
	File.write((char*)&m_negReflinePnt2, sizeof(IwPoint3d));

	// window cut angles
	// iTW v503~v607 have 2 cut angles. But iTW v608 has 4 (2 more for F2).
	double windowCutAngle;
	for (int i=0; i<4; i++)
	{
		windowCutAngle = m_windowCutAngles.GetAt(i);
		File.write((char*) &windowCutAngle, sizeof(double) );
	}

	File.close();

	m_bOPJigsDataModified = false;

	return true;
}

bool COutlineProfileJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);

	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_op_jigs.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	unsigned nSize;
	IwTArray<IwPoint3d>		featurePoints;
	IwTArray<int>			infos;
	IwPoint3d				posDroppingCenter, negDroppingCenter;
	IwTArray<IwPoint3d>		tabPoints, tabEndPoints, refPoints;

	// Variables for TriathlonF1 version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		// feature points
		File.read( (char*) &nSize, sizeof( unsigned ) );

		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d featurePoint;
			File.read( (char*) &featurePoint, sizeof(IwPoint3d) );
			featurePoints.Add(featurePoint);
		}
		for (unsigned i=0; i<nSize; i++)
		{
			int info;
			File.read( (char*) &info, sizeof(int) );
			infos.Add(info);
		}
		// dropping centers
		File.read( (char*) &posDroppingCenter, sizeof(IwPoint3d) );
		File.read( (char*) &negDroppingCenter, sizeof(IwPoint3d) );
		// tab points
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d tabPoint;
			File.read( (char*) &tabPoint, sizeof(IwPoint3d) );
			tabPoints.Add(tabPoint);
		}
		// tab end points
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d tabEndPoint;
			File.read( (char*) &tabEndPoint, sizeof(IwPoint3d) );
			tabEndPoints.Add(tabEndPoint);
		}
		// reference points
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d refPoint;
			File.read( (char*) &refPoint, sizeof(IwPoint3d) );
			refPoints.Add(refPoint);
		}

	}

	//posterior ref line points
	File.read((char*)&m_posReflinePnt1, sizeof(IwPoint3d));
	File.read((char*)&m_posReflinePnt2, sizeof(IwPoint3d));
	File.read((char*)&m_negReflinePnt1, sizeof(IwPoint3d));
	File.read((char*)&m_negReflinePnt2, sizeof(IwPoint3d));

	// window cut angles
	// iTW v503~v607 have 2 cut angles. But iTW v608 has 4 (2 more for F2).
	double windowCutAngle = 10.0;
	IwTArray<double> windowCutAngles;
	for (int i=0; i<4; i++)
		windowCutAngles.Add(windowCutAngle); // initialized
	// Variables for TriathlonF1 version 5.0.3
	if (m_pDoc->IsOpeningFileRightVersion(5,0,3))
	{
		File.read( (char*) &windowCutAngle, sizeof(double) );
		windowCutAngles.SetAt(0, windowCutAngle);
		File.read( (char*) &windowCutAngle, sizeof(double) );
		windowCutAngles.SetAt(1, windowCutAngle);
	}
	else if (m_pDoc->IsOpeningFileRightVersion(6,0,8))
	{
		for (int i=0; i<4; i++)
		{
			File.read( (char*) &windowCutAngle, sizeof(double) );
			windowCutAngles.SetAt(i, windowCutAngle);
		}
	}

	File.close();

	// Actions required by TriathlonF1 version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		// Get its sketch surface from IwBrep
		IwBrep* Brep = this->GetIwBrep();
		IwTArray<IwFace*> faces;
		Brep->GetFaces(faces);
		m_sketchSurface = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();

		// Set feature points
		SetOutlineProfileFeaturePoints(featurePoints, infos);
		// Set dropping center
		SetDroppingCenters(posDroppingCenter, negDroppingCenter);
		// Set tab points
		SetOutlineProfileTabPoints(tabPoints);
		SetOutlineProfileTabEndPoints(tabEndPoints);
		// Set reference points
		SetOutlineProfileReferencePoints(refPoints);
		SetOutlineProfileReferencePoints(refPoints);
		// Set window cut angles
		SetWindowCutAngles(windowCutAngles);
	}

	m_bOPJigsDataModified = false;


	return true;
}

void COutlineProfileJigs::SetRefLinePoints(IwPoint3d line1Pnt1, IwPoint3d line1Pnt2, IwPoint3d line2Pnt1, IwPoint3d line2Pnt2)
{
	m_posReflinePnt1 = line1Pnt1;
	m_posReflinePnt2 = line1Pnt2;
	m_negReflinePnt1 = line2Pnt1;
	m_negReflinePnt2 = line2Pnt2;
}

void COutlineProfileJigs::GetRefLinePoints(IwPoint3d &line1Pnt1, IwPoint3d &line1Pnt2, IwPoint3d &line2Pnt1, IwPoint3d &line2Pnt2)
{
	line1Pnt1 = m_posReflinePnt1;
	line1Pnt2 = m_posReflinePnt2;
	line2Pnt1 = m_negReflinePnt1;
	line2Pnt2 = m_negReflinePnt2;
}

void COutlineProfileJigs::GetSelectableEntities
(
	IwTArray<IwBrep*>& breps,		// O:
	IwTArray<IwCurve*>& curves,		// O:
	IwTArray<IwPoint3d>& points		// O:
)
{
	// Outline profile jigs sketch surface represented in brep in CPart is not selectable.
	// Therefore, we do not want to append to breps.
	IwTArray<IwBrep*> myBrep;
	CPart::GetSelectableEntities(myBrep, curves, points);
}

void COutlineProfileJigs::SetOPJigsModifiedFlag( bool bModified )
{
	m_outlineProfileJigsModified = bModified;
	if (bModified)
	{
		m_bOPJigsDataModified = true;
		m_pDoc->SetModified(m_bOPJigsDataModified);
	}
}

void COutlineProfileJigs::SetSketchSurface
(
	IwBSplineSurface* sketchSurface
)
{

	if (sketchSurface == NULL) return;

	// delete the previous Brep
	IwBrep* prevBrep = GetIwBrep();
	if (prevBrep)
	{
		IwObjDelete delBrep(prevBrep);
	}

	m_sketchSurface = sketchSurface;
	// Create Brep
	IwContext& iwContext = m_pDoc->GetIwContext();
	IwBrep* skchBrep = new (iwContext) IwBrep();
	IwFace* pFace;
	skchBrep->CreateFaceFromSurface(sketchSurface, sketchSurface->GetNaturalUVDomain(), pFace);

	if (pFace == NULL) return;
	
	SetIwBrep(skchBrep);
	MakeTess();

	m_bOPJigsDataModified = true;

	return;
}

double COutlineProfileJigs::GetVLengthRatio()
{
	if ( m_sketchSurface == NULL )
		return 0;

	IwExtent2d surfDom = m_sketchSurface->GetNaturalUVDomain();
	IwBSplineCurve*isoUCurve;
	m_sketchSurface->CreateIsoParametricCurve(m_pDoc->GetIwContext(), IW_SP_U, surfDom.GetMin().x, 0.01, isoUCurve);
	double length;
	isoUCurve->Length(isoUCurve->GetNaturalInterval(), 0.01, length);

	double oldLength = surfDom.GetSize().y;
	double ratio = length / oldLength;

	if ( isoUCurve )
		IwObjDelete(isoUCurve);

	return ratio;
}

bool COutlineProfileJigs::GetSketchSurface
(
	IwBSplineSurface*& sketchSurface
)
{

	sketchSurface = m_sketchSurface;

	return true;
}

void COutlineProfileJigs::SetOutlineProfileFeaturePoints
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo
)
{
	IwTArray<IwPoint3d> inputFeaturePoints;
	inputFeaturePoints.Append(featurePoints);
	IwTArray<int> inputFeaturePointsInfo;
	inputFeaturePointsInfo.Append(featurePointsInfo);

	// To ensure the arc ending points are in correct positions.
	UpdateOutlineProfileArcEndingPoints(inputFeaturePoints, inputFeaturePointsInfo);

	m_outlineProfileFeaturePoints.RemoveAll();
	m_outlineProfileFeaturePointsInfo.RemoveAll();

	// Note, Outline profile defines a closed curve.
	// The last element should be always identical to the first one.
	for (unsigned i=0; i<(inputFeaturePoints.GetSize()-1); i++)
	{
		m_outlineProfileFeaturePoints.Add(inputFeaturePoints.GetAt(i));
		m_outlineProfileFeaturePointsInfo.Add(inputFeaturePointsInfo.GetAt(i));
	}
	m_outlineProfileFeaturePoints.Add(inputFeaturePoints.GetAt(0));
	m_outlineProfileFeaturePointsInfo.Add(inputFeaturePointsInfo.GetAt(0));

	// Determine the projected curve points
	DetermineOutlineProfileJigsProjectedCurvePoints();

	SetOPJigsModifiedFlag(true);

}

void COutlineProfileJigs::DetermineOutlineProfileJigsProjectedCurvePoints
(
	CInnerSurfaceJigs* innerSurfaceJigs
)
{
	// Update m_outlineProfileProjectedCurve
	m_outlineProfileProjectedCurve.RemoveAll();

	// Outline profile jigs share the same parametric domain as the outline profile implant
	IwBSplineCurve *UVCurve = GetUVCurveOnSketchSurface(true, innerSurfaceJigs); // true=get square-off posterior tips, if applied.

	CFemoralPart* pOsteophyteSurf = GetTotalDoc()->GetOsteophyteSurface();
	CCartilageSurface* pCartilageSurf = GetTotalDoc()->GetCartilageSurface();
	// if either one is missing, display outline profile on sketch surface
	if ( pOsteophyteSurf == NULL || pCartilageSurf == NULL ) 
	{
		IwCrvOnSurf* crvOnSurf = new (GetTotalDoc()->GetIwContext()) IwCrvOnSurf(*UVCurve, *m_sketchSurface);
		IwTArray<double> breakParams;
		breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
		breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
		double aTol;
		IwBSplineCurve* outlineProfileXYZCurve;
		crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, outlineProfileXYZCurve, FALSE, FALSE);

		IwExtent1d sIvl = outlineProfileXYZCurve->GetNaturalInterval();
		double	dChordHeightTol = 0.75;
		double	dAngleTolInDegrees = 10.0;
		double min3DDistance = 0.0;
		IwPoint3d sPData[64];
		IwTArray<IwPoint3d> sPnts( 64, sPData );
		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, min3DDistance, 0.001 );
		sCrvTess.TessellateCurve( *outlineProfileXYZCurve, sIvl, NULL, &sPnts );
		m_outlineProfileProjectedCurve.Append(sPnts);
	}
	else // project onto both cartilage surface and osteophyte surface
	{
		IwBSplineSurface* cartiSurf = pCartilageSurf->GetSinglePatchSurface(true);
		IwBSplineSurface* osteoSurf = pOsteophyteSurf->GetSinglePatchSurface();
		IwTArray<IwSurface*> surfaces;
		surfaces.Add(cartiSurf);
		surfaces.Add(osteoSurf);

		IwExtent1d crvDom = UVCurve->GetNaturalInterval();
		IwTArray<IwPoint3d> uvPoints;
		UVCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), 200, 0.01, &uvPoints, NULL);
		IwPoint3d uv0;
		IwPoint2d uv;
		IwPoint3d xyz, normal, awayPnt;
		IwPoint3d intPnt;
		bool bInt;
		for (unsigned i=0; i<uvPoints.GetSize(); i++)
		{
			uv0 = uvPoints.GetAt(i);
			uv = IwPoint2d(uv0.x, uv0.y);
			m_sketchSurface->EvaluatePoint(uv, xyz);
			m_sketchSurface->EvaluateNormal(uv, FALSE, FALSE, normal);
			awayPnt = xyz + 30*normal;
			bInt = IntersectSurfacesByLine(surfaces, awayPnt, normal, intPnt);
			if ( bInt )
			{
				m_outlineProfileProjectedCurve.Add(intPnt);
			}
		}
	}


	IwBrep * pSketchSurfaceBrep = new (m_pDoc->GetIwContext())IwBrep();

	// Let's delete m_glOutlineProfileCurveList in the end of m_outlineProfileProjectedCurve creation
	DeleteDisplayList( m_glOutlineProfileCurveList );// to force update display

}

void COutlineProfileJigs::GetOutlineProfileFeaturePoints
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo
)
{
	featurePoints.RemoveAll();
	featurePointsInfo.RemoveAll();

	featurePoints.Append(m_outlineProfileFeaturePoints);
	featurePointsInfo.Append(m_outlineProfileFeaturePointsInfo);
}

void COutlineProfileJigs::UpdateOutlineProfileArcEndingPoints
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo
)
{
	for (unsigned i=0; i<featurePointsInfo.GetSize(); i++)
	{
		if ( featurePointsInfo.GetAt(i) == 4 )// arc ending point
		{
			double radius = featurePoints.GetAt(i-1).DistanceBetween(featurePoints.GetAt(i-2));
			IwVector3d endingVec = featurePoints.GetAt(i) - featurePoints.GetAt(i-1);
			endingVec.Unitize();
			IwPoint3d rightPosition = featurePoints.GetAt(i-1) + radius*endingVec;
			featurePoints.SetAt(i, rightPosition);
		}
	}
}

void COutlineProfileJigs::SetOutlineProfileTabPoints
(
	IwTArray<IwPoint3d>& tabPoints
)
{
	m_outlineProfileTabPoints.RemoveAll();
	m_outlineProfileTabPoints.Append(tabPoints);

	DetermineOutlineProfileJigsTabProfilePoints();
	SetOPJigsModifiedFlag(true);
}

void COutlineProfileJigs::SetOutlineProfileTabEndPoints
(
	IwTArray<IwPoint3d>& tabEndPoints
)
{
	m_outlineProfileTabEndPoints.RemoveAll();
	m_outlineProfileTabEndPoints.Append(tabEndPoints);

	DetermineOutlineProfileJigsTabProfilePoints();
	SetOPJigsModifiedFlag(true);

	// Determine jump curves
	if ( m_medialJumpCurve )
		IwObjDelete(m_medialJumpCurve);
	m_medialJumpCurve = NULL;
	if ( m_lateralJumpCurve )
		IwObjDelete(m_lateralJumpCurve);
	m_lateralJumpCurve = NULL;

	if ( !m_outlineProfileTabEndPoints.GetAt(0).IsInitialized() )
		return;

	// Get axis
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();

	// medial curve
	IwTArray<IwPoint3d> medialPnts;
	medialPnts.Add(m_outlineProfileTabEndPoints.GetAt(0));
	medialPnts.Add(0.5*m_outlineProfileTabEndPoints.GetAt(0)+0.5*m_outlineProfileTabEndPoints.GetAt(1)); // To make the anterior extension more predictable.
	medialPnts.Add(m_outlineProfileTabEndPoints.GetAt(1));
	medialPnts.Add(m_outlineProfileTabEndPoints.GetAt(4));
	medialPnts.Add(0.5*m_outlineProfileTabEndPoints.GetAt(4)+0.5*m_outlineProfileTabEndPoints.GetAt(5)); // To make the posterior extension more predictable.
	medialPnts.Add(m_outlineProfileTabEndPoints.GetAt(5));
	IwBSplineCurve *mCrv0, *mCrv1;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), medialPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, mCrv0);
	mCrv0->CreateExtendedCurve(m_pDoc->GetIwContext(), 9.5, 1, IW_CT_G1, mCrv1);
	mCrv1->CreateExtendedCurve(m_pDoc->GetIwContext(), 9.5, 2, IW_CT_G1, m_medialJumpCurve);
if (0)
	ShowCurve(m_pDoc, m_medialJumpCurve, red);
	// lateral curve
	IwTArray<IwPoint3d> lateralPnts;
	double lateralTabWidth = m_outlineProfileTabEndPoints.GetAt(2).DistanceBetween(m_outlineProfileTabEndPoints.GetAt(3));
	double middlePntBendingRatio = lateralTabWidth/17.5;
	IwPoint3d lateralMiddlePnt = 0.5*(m_outlineProfileTabEndPoints.GetAt(2) + m_outlineProfileTabEndPoints.GetAt(3));
	lateralMiddlePnt -= 1.0*middlePntBendingRatio*sweepAxes.GetZAxis();// move middle point down a lit to make the m_lateralJumpCurve bending, kind of similar to femur profile.
	lateralPnts.Add(m_outlineProfileTabEndPoints.GetAt(2));
	lateralPnts.Add(lateralMiddlePnt);
	lateralPnts.Add(m_outlineProfileTabEndPoints.GetAt(3));
	IwBSplineCurve *lCrv0, *lCrv1;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), lateralPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, lCrv0);
	lCrv0->CreateExtendedCurve(m_pDoc->GetIwContext(), 9.5, 1, IW_CT_G1, lCrv1);
	lCrv1->CreateExtendedCurve(m_pDoc->GetIwContext(), 9.5, 2, IW_CT_G1, m_lateralJumpCurve);
if (0)
	ShowCurve(m_pDoc, m_lateralJumpCurve, blue);

	if (mCrv0)
		IwObjDelete(mCrv0);
	if (mCrv1)
		IwObjDelete(mCrv1);

	SetOPJigsModifiedFlag(true);
}

//////////////////////////////////////////////////////////////
// Two curves. First one is in positive side. Second one is
// in negative side.
//////////////////////////////////////////////////////////////
bool COutlineProfileJigs::GetJumpCurves(IwTArray<IwBSplineCurve*>& jumpCurves)
{
	if ( m_medialJumpCurve == NULL || m_lateralJumpCurve == NULL )
		return false;

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		jumpCurves.Add(m_lateralJumpCurve);
		jumpCurves.Add(m_medialJumpCurve);
	}
	else
	{
		positiveSideIsLateral = false;
		jumpCurves.Add(m_medialJumpCurve);
		jumpCurves.Add(m_lateralJumpCurve);
	}

	return true;
}

void COutlineProfileJigs::GetOutlineProfileTabPoints
(
	IwTArray<IwPoint3d>& tabPoints
)
{
	tabPoints.RemoveAll();
	tabPoints.Append(m_outlineProfileTabPoints);
}

void COutlineProfileJigs::GetOutlineProfileTabEndPoints
(
	IwTArray<IwPoint3d>& tabEndPoints
)
{
	tabEndPoints.RemoveAll();
	tabEndPoints.Append(m_outlineProfileTabEndPoints);
}

void COutlineProfileJigs::SetOutlineProfileReferencePoints
(
	IwTArray<IwPoint3d>& refPoints
)
{
	m_referencePoints.RemoveAll();
	m_referencePoints.Append(refPoints);
	SetOPJigsModifiedFlag(true);
}

void COutlineProfileJigs::GetOutlineProfileReferencePoints
(
	IwTArray<IwPoint3d>& refPoints
)
{
	refPoints.RemoveAll();
	refPoints.Append(m_referencePoints);
}

void COutlineProfileJigs::ConvertUVToXYZ
(
	IwTArray<IwPoint3d>& uvPoints, 
	IwTArray<IwPoint3d>& xyzPoints
)
{
	xyzPoints.RemoveAll();

	if ( m_sketchSurface == NULL )
		return;

	IwPoint3d uvPnt, pnt;
	IwPoint2d uv;

	for (unsigned i=0; i<uvPoints.GetSize(); i++)
	{
		uvPnt = uvPoints.GetAt(i);
		uv = IwPoint2d(uvPnt.x, uvPnt.y);
		m_sketchSurface->EvaluatePoint(uv, pnt);
		xyzPoints.Add(pnt);
	}

	return;
}

void COutlineProfileJigs::ConvertUVToXYZ
(
	IwPoint3d& uvPoint, 
	IwPoint3d& xyzPoint, 
	IwVector3d& normal
)
{
	IwPoint2d uv;

	uv = IwPoint2d(uvPoint.x, uvPoint.y);
	m_sketchSurface->EvaluatePoint(uv, xyzPoint);
	m_sketchSurface->EvaluateNormal(uv, FALSE, FALSE, normal);

}

/////////////////////////////////////////////////////////////
// This function displays the F1 and F2 window cuts and pin holes
void COutlineProfileJigs::DetermineCoreHolesProfiles()
{
	m_coreHoleProfile.RemoveAll();

	CFemoralPart* pOsteophyteSurf = GetTotalDoc()->GetOsteophyteSurface();
	CCartilageSurface* pCartilageSurf = GetTotalDoc()->GetCartilageSurface();
	// if either one is missing, display outline profile on sketch surface
	if ( pOsteophyteSurf == NULL || pCartilageSurf == NULL ) 
		return;

	IwBSplineSurface* cartiSurf = pCartilageSurf->GetSinglePatchSurface(true);
	IwBSplineSurface* osteoSurf = pOsteophyteSurf->GetSinglePatchSurface();
	IwTArray<IwSurface*> surfaces;
	surfaces.Add(cartiSurf);
	surfaces.Add(osteoSurf);

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Get peg position
	IwPoint3d posPegPosition, negPegPosition;
	EstimatePegPositions(posPegPosition, negPegPosition);

	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();

	// get axis
	IwPoint3d pnt, normal = sweepAxes.GetZAxis();
	IwPoint3d intPnt;
	bool bInt;

	////// Positive side //////////
	//// Get F1CoreHole.crv
	
	IwTArray<IwPoint3d> posPoints;
	//// Transformation matrix
	double posRotRadian = m_windowCutAngles.GetAt(0)/(180.0/IW_PI);
	//// matrix for rotating along z-axis
	//IwAxis2Placement posRotZMat;//
	//posRotZMat.RotateAboutAxis(posRotRadian, IwVector3d(0,0,1));
	////
	//IwAxis2Placement posTransMatrix = IwAxis2Placement(posPegPosition-20.0*normal, sweepAxes.GetXAxis(), sweepAxes.GetYAxis());

	//if ( posCoreHoleSegs.GetSize() > 0 )
	//{
	//	IwBSplineCurve* seg=NULL;
	//	double len;
	//	int nSize;
	//	IwExtent1d intv;
	//	for (unsigned i=0; i<posCoreHoleSegs.GetSize(); i++)
	//	{
	//		seg = (IwBSplineCurve*)posCoreHoleSegs.GetAt(i);
	//		// transform to the right location
	//		seg->Transform(posRotZMat);
	//		seg->Transform(posTransMatrix);
	//		intv = seg->GetNaturalInterval();
	//		seg->Length(intv, 0.01, len);
	//		if ( len > 20 )
	//			nSize = (int)(len/2.0);
	//		else
	//			nSize = (int)len + 2;
	//		posPoints.RemoveAll();
	//		seg->EquallySpacedPoints(intv.GetMin(), intv.GetMax(), nSize, 0.01, &posPoints, NULL);
	//		if ( m_coreHoleProfile.GetSize() > 0 )
	//			m_coreHoleProfile.Add(IwPoint3d(-1,-1,-1));
	//		for (unsigned j=0; j<posPoints.GetSize(); j++)
	//		{
	//			pnt = posPoints.GetAt(j);
	//			bInt = IntersectSurfacesByLine(surfaces, pnt, normal, intPnt);
	//			if ( bInt )
	//			{
	//				m_coreHoleProfile.Add(intPnt);
	//			}
	//		}
	//	}
	//}

	////// Negative side //////////
	//// Get F1CoreHole.crv
	//IwTArray<IwCurve*> negCoreHoleSegs;
	//QString	negCoreHoleSegsFileName = QString("F1CoreHole.crv");
	//ReadCurves(negCoreHoleSegsFileName, negCoreHoleSegs);
	IwTArray<IwPoint3d> negPoints;
	//// Transformation matrix
	double negRotRadian = -m_windowCutAngles.GetAt(1)/(180.0/IW_PI);
	
	//// Positive side F2 window cut //////////
	// Get F2WindowCutMedial/Lateral.crv
	IwTArray<IwCurve*> posF2WindowCutSegs;
	QString	posF2WindowCutSegsFileName;
	if ( positiveSideIsLateral )
	{
		// lateral
		posF2WindowCutSegsFileName = QString("F2WindowCutLateral.crv");
		posRotRadian = m_windowCutAngles.GetAt(2)/(180.0/IW_PI);
	}
	else
	{
		// medial
		posF2WindowCutSegsFileName = QString("F2WindowCutMedial.crv");
		posRotRadian = -(m_windowCutAngles.GetAt(2)+23.4)/(180.0/IW_PI);// why 23.4? it's a weird start part
	}
	ReadCurves(posF2WindowCutSegsFileName, posF2WindowCutSegs);
	// matrix for rotating along z-axis
	IwAxis2Placement posF2RotZMat;//
	posF2RotZMat.RotateAboutAxis(posRotRadian, IwVector3d(0,0,1));
	IwAxis2Placement posF2TransMatrix = IwAxis2Placement(posPegPosition-20.0*normal, sweepAxes.GetXAxis(), sweepAxes.GetYAxis());
	if ( posF2WindowCutSegs.GetSize() > 0 )
	{
		IwBSplineCurve* seg=NULL;
		double len;
		int nSize;
		IwExtent1d intv;
		for (unsigned i=0; i<posF2WindowCutSegs.GetSize(); i++)
		{
			seg = (IwBSplineCurve*)posF2WindowCutSegs.GetAt(i);
			// transform to the location
			seg->Transform(posF2RotZMat);
			seg->Transform(posF2TransMatrix);
			intv = seg->GetNaturalInterval();
			seg->Length(intv, 0.01, len);
			if ( len > 20 )
				nSize = (int)(len/2.0);
			else
				nSize = (int)len + 2;
			posPoints.RemoveAll();
			seg->EquallySpacedPoints(intv.GetMin(), intv.GetMax(), nSize, 0.01, &posPoints, NULL);
			if ( m_coreHoleProfile.GetSize() > 0 )
				m_coreHoleProfile.Add(IwPoint3d(-2,-2,-2));// to seperate with others
			for (unsigned j=0; j<posPoints.GetSize(); j++)
			{
				pnt = posPoints.GetAt(j);
				bInt = IntersectSurfacesByLine(surfaces, pnt, normal, intPnt);
				if ( bInt )
				{
					m_coreHoleProfile.Add(intPnt);
				}
			}
		}
	}

	//// Negative side F2 window cut //////////
	// Get F2WindowCutMedial/Lateral.crv
	IwTArray<IwCurve*> negF2WindowCutSegs;
	QString	negF2WindowCutSegsFileName;
	if ( positiveSideIsLateral )
	{
		// medial
		negF2WindowCutSegsFileName = QString("F2WindowCutMedial.crv");
		negRotRadian = (m_windowCutAngles.GetAt(3)+23.4)/(180.0/IW_PI);// why 23.4? it's a weird start part
	}
	else
	{
		// lateral
		negF2WindowCutSegsFileName = QString("F2WindowCutLateral.crv");
		negRotRadian = -m_windowCutAngles.GetAt(3)/(180.0/IW_PI);
	}
	ReadCurves(negF2WindowCutSegsFileName, negF2WindowCutSegs);
	// matrix for rotating along z-axis
	IwAxis2Placement negF2RotZMat;//
	negF2RotZMat.RotateAboutAxis(negRotRadian, IwVector3d(0,0,1));
	//
	IwAxis2Placement negF2TransMatrix = IwAxis2Placement(negPegPosition-20.0*normal, sweepAxes.GetXAxis(), sweepAxes.GetYAxis());

	if ( negF2WindowCutSegs.GetSize() > 0 )
	{
		IwBSplineCurve* seg=NULL;
		double len;
		int nSize;
		IwExtent1d intv;
		for (unsigned i=0; i<negF2WindowCutSegs.GetSize(); i++)
		{
			seg = (IwBSplineCurve*)negF2WindowCutSegs.GetAt(i);
			// transform to the location
			seg->Transform(negF2RotZMat);
			seg->Transform(negF2TransMatrix);
			intv = seg->GetNaturalInterval();
			seg->Length(intv, 0.01, len);
			if ( len > 20 )
				nSize = (int)(len/2.0);
			else
				nSize = (int)len + 2;
			negPoints.RemoveAll();
			seg->EquallySpacedPoints(intv.GetMin(), intv.GetMax(), nSize, 0.01, &negPoints, NULL);
			if ( m_coreHoleProfile.GetSize() > 0 )
				m_coreHoleProfile.Add(IwPoint3d(-2,-2,-2));// to seperate with others
			for (unsigned j=0; j<negPoints.GetSize(); j++)
			{
				pnt = negPoints.GetAt(j);
				bInt = IntersectSurfacesByLine(surfaces, pnt, normal, intPnt);
				if ( bInt )
				{
					m_coreHoleProfile.Add(intPnt);
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////
// The posProfile is the sketch profile in the positive side.
// The negProfile is the sketch profile in the negative side.
// Both profiles are extended anteriorly and posteriorly such that
// the lines connected the end points of both profiles enclose
// the entire sketch profile.
//////////////////////////////////////////////////////////////////////
void COutlineProfileJigs::GetBoundingProfiles
(
	double addOffset,				// I: additional offset to make the main contact region wider 
	IwBSplineCurve* &posUVProfile,	// O: Note, the direction follows from anterior to posterior
	IwBSplineCurve* &negUVProfile,	// O: Note, the direction follows from anterior to posterior
	IwBSplineCurve* &notchUVProfile // O:
)
{
	if ( m_outlineProfileFeaturePointsInfo.GetSize() == 0 )
		return;

	// Get uv curve
	IwBSplineCurve* posUVCurve = GetUVCurveOnSketchSurface(false); // false=Get the circular posterior tips
	// Copy uv curve
	IwCurve* copyCurve;
	posUVCurve->Copy(m_pDoc->GetIwContext(), copyCurve);
	IwBSplineCurve* negUVCurve = (IwBSplineCurve*)copyCurve;
	posUVCurve->Copy(m_pDoc->GetIwContext(), copyCurve);
	IwBSplineCurve* notchUVCurve = (IwBSplineCurve*)copyCurve;

	// Determine the starting/center/ending points of the 4 un-collapable CCW arcs
	IwPoint3d startingPoint0, centerPoint0, endingPoint0;
	IwPoint3d startingPoint1, centerPoint1, endingPoint1;
	IwPoint3d startingPoint2, centerPoint2, endingPoint2;
	IwPoint3d startingPoint3, centerPoint3, endingPoint3;
	int encounteredArc = 0;
	for (unsigned i=0; i<m_outlineProfileFeaturePointsInfo.GetSize(); i++)
	{
		if ( m_outlineProfileFeaturePointsInfo.GetAt(i) == 1 ) //un-collapable CCW arc
		{
			if ( encounteredArc == 0 )// positive anterior arc
			{
				startingPoint0 = m_outlineProfileFeaturePoints.GetAt(i-1);
				centerPoint0 = m_outlineProfileFeaturePoints.GetAt(i);
				endingPoint0 = m_outlineProfileFeaturePoints.GetAt(i+1);
			}
			else if ( encounteredArc == 1 )// positive posterior arc
			{
				startingPoint1 = m_outlineProfileFeaturePoints.GetAt(i-1);
				centerPoint1 = m_outlineProfileFeaturePoints.GetAt(i);
				endingPoint1 = m_outlineProfileFeaturePoints.GetAt(i+1);
			}
			else if ( encounteredArc == 2 )// negative posterior arc
			{
				startingPoint2 = m_outlineProfileFeaturePoints.GetAt(i-1);
				centerPoint2 = m_outlineProfileFeaturePoints.GetAt(i);
				endingPoint2 = m_outlineProfileFeaturePoints.GetAt(i+1);
			}
			else if ( encounteredArc == 3 )// negative anterior arc
			{
				startingPoint3 = m_outlineProfileFeaturePoints.GetAt(i-1);
				centerPoint3 = m_outlineProfileFeaturePoints.GetAt(i);
				endingPoint3 = m_outlineProfileFeaturePoints.GetAt(i+1);
			}
			encounteredArc++;
		}
	}
	// Anterior seam point
	IwPoint3d antSeamPoint = m_outlineProfileFeaturePoints.GetAt(0);

	// Determine anterior bounding line by endingPoint3 and startingPoint0
	IwVector3d antBoundingLineVec = startingPoint0 - endingPoint3;
	antBoundingLineVec.Unitize();
	IwPoint3d antBoundingLinePnt;
	IwVector3d tempVec = antSeamPoint - endingPoint3;
	IwVector3d crossVec = antBoundingLineVec*tempVec;
	if ( crossVec.z > 0 ) // antSeamPoint is below
		antBoundingLinePnt = 0.5*startingPoint0 + 0.5*endingPoint3;
	else // antSeamPoint is above
		antBoundingLinePnt = antSeamPoint;
	// Move antBoundingLinePnt anteriorly as allowance
	antBoundingLinePnt = antBoundingLinePnt + IwPoint3d(0, -3, 0);

	// Determine posterior bounding line by both posterior arcs
	double posPostArcRadius = startingPoint1.DistanceBetween(centerPoint1);
	double negPostArcRadius = startingPoint2.DistanceBetween(centerPoint2);
	double posteriorTipExtraCoverage = 5.0 + GetTotalDoc()->GetAdvancedControlJigs()->GetInnerSurfaceAdditionalPosteriorCoverage();// 5.0 as default extra coverage
	IwPoint3d posPostTip = centerPoint1 + IwPoint3d(0,posPostArcRadius+posteriorTipExtraCoverage, 0);
	IwPoint3d negPostTip = centerPoint2 + IwPoint3d(0,negPostArcRadius+posteriorTipExtraCoverage, 0);
	IwVector3d postBoundingLineVec = posPostTip - negPostTip;
	postBoundingLineVec.Unitize();
	IwPoint3d postBoundingLinePnt = 0.5*posPostTip + 0.5*negPostTip;

	//// posProfile ///////////////////////////////////////////////////////////
	// Now trim posUVCurve by endingPoint0 and startingPoint1
	IwPoint3d cPnt;
	double param0, param1;
	DistFromPointToCurve(endingPoint0, posUVCurve, cPnt, param0);
	DistFromPointToCurve(startingPoint1, posUVCurve, cPnt, param1);
	IwExtent1d trimDom = IwExtent1d(param0, param1);
	posUVCurve->Trim(trimDom);
	// Extend 2*posPostArcRadius anteriorly
	IwBSplineCurve* posAntExtCurve;
	posUVCurve->CreateExtendedCurve(m_pDoc->GetIwContext(), 2*posPostArcRadius, 1, IW_CT_G1, posAntExtCurve); 
	// Extend 2*posPostArcRadius posterior
	IwBSplineCurve* posPostExtCurve;
	posAntExtCurve->CreateExtendedCurve(m_pDoc->GetIwContext(), 2*posPostArcRadius, 2, IW_CT_G1, posPostExtCurve); 
	// Trim by both antBoundingLine and postBoundingLine
	IwVector3d tangVec;
	IntersectCurveByVector(posPostExtCurve, posPostExtCurve->GetNaturalInterval(), antBoundingLinePnt, antBoundingLineVec, NULL, 10, cPnt, param0, tangVec); 
	IntersectCurveByVector(posPostExtCurve, posPostExtCurve->GetNaturalInterval(), postBoundingLinePnt, postBoundingLineVec, NULL, 10, cPnt, param1, tangVec); 
	trimDom = IwExtent1d(param0, param1);
	posPostExtCurve->Trim(trimDom);
	// offset
	IwBSplineCurve* posOffsetCurve=NULL;
	double aTol;
	if ( addOffset > 0 )
	{
		posPostExtCurve->CreateSimpleOffset(m_pDoc->GetIwContext(), 0.01, IwVector3d(0,0,1), addOffset, posOffsetCurve, aTol);
	}
	else
	{
		posOffsetCurve = posPostExtCurve;
	}
	// assign
	posUVProfile = posOffsetCurve;
	if (0)
	{
		// Convert to xyz curve
		IwBSplineCurve* XYZCurve;
		IwCrvOnSurf* crvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*posUVProfile, *m_sketchSurface);
		if ( crvOnSurf )
		{
			IwTArray<double> breakParams;
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
			double aTol;
			crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, XYZCurve, FALSE, FALSE);
			ShowCurve(m_pDoc, XYZCurve, magenta);
		}
	}

	//// negProfile ///////////////////////////////////////////////////////////
	// Now trim negUVCurve by endingPoint2 and startingPoint3
	DistFromPointToCurve(endingPoint2, negUVCurve, cPnt, param0);
	DistFromPointToCurve(startingPoint3, negUVCurve, cPnt, param1);
	trimDom = IwExtent1d(param0, param1);
	negUVCurve->Trim(trimDom);
	// Extend 2*negPostArcRadius anteriorly
	IwBSplineCurve* negAntExtCurve;
	negUVCurve->CreateExtendedCurve(m_pDoc->GetIwContext(), 2*negPostArcRadius, 1, IW_CT_G1, negAntExtCurve); 
	// Extend 2*negPostArcRadius posteriorly
	IwBSplineCurve* negPostExtCurve;
	negAntExtCurve->CreateExtendedCurve(m_pDoc->GetIwContext(), 2*negPostArcRadius, 2, IW_CT_G1, negPostExtCurve); 
	// Trim by both antBoundingLine and postBoundingLine
	IntersectCurveByVector(negPostExtCurve, negPostExtCurve->GetNaturalInterval(), postBoundingLinePnt, postBoundingLineVec, NULL, 10, cPnt, param0, tangVec); 
	IntersectCurveByVector(negPostExtCurve, negPostExtCurve->GetNaturalInterval(), antBoundingLinePnt, antBoundingLineVec, NULL, 10, cPnt, param1, tangVec); 
	trimDom = IwExtent1d(param0, param1);
	negPostExtCurve->Trim(trimDom);
	// Need to reverse negPostExtCurve
	IwExtent1d newItv;
	negPostExtCurve->ReverseParameterization(negPostExtCurve->GetNaturalInterval(), newItv);
	// offset
	IwBSplineCurve* negOffsetCurve=NULL;
	if ( addOffset > 0 )
	{
		negPostExtCurve->CreateSimpleOffset(m_pDoc->GetIwContext(), 0.01, IwVector3d(0,0,1), -addOffset, negOffsetCurve, aTol);// negative offset value
	}
	else
	{
		negOffsetCurve = negPostExtCurve;
	}
	// assign
	negUVProfile = negOffsetCurve;
	if (0)
	{
		// Convert to xyz curve
		IwBSplineCurve* XYZCurve;
		IwCrvOnSurf* crvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*negUVProfile, *m_sketchSurface);
		if ( crvOnSurf )
		{
			IwTArray<double> breakParams;
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
			double aTol;
			crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, XYZCurve, FALSE, FALSE);
			ShowCurve(m_pDoc, XYZCurve, cyan);
		}
	}

	//// notchProfile ///////////////////////////////////////////////////////////
	// Now trim notchUVCurve by endingPoint1 and startingPoint2
	DistFromPointToCurve(endingPoint1, notchUVCurve, cPnt, param0);
	DistFromPointToCurve(startingPoint2, notchUVCurve, cPnt, param1);
	trimDom = IwExtent1d(param0, param1);
	notchUVCurve->Trim(trimDom);
	// Extend 2*posPostArcRadius posteriorly
	IwBSplineCurve* notchPosExtCurve;
	notchUVCurve->CreateExtendedCurve(m_pDoc->GetIwContext(), 2*posPostArcRadius, 1, IW_CT_G1, notchPosExtCurve); 
	// Extend 2*negPostArcRadius posteriorly
	IwBSplineCurve* notchNegExtCurve;
	notchPosExtCurve->CreateExtendedCurve(m_pDoc->GetIwContext(), 2*negPostArcRadius, 2, IW_CT_G1, notchNegExtCurve); 
	// No need to trim notch profile by both antBoundingLine and postBoundingLine
	// No offset for notch profile
	// assign
	notchUVProfile = notchNegExtCurve;
	if (0)
	{
		// Convert to xyz curve
		IwBSplineCurve* XYZCurve;
		IwCrvOnSurf* crvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*notchUVProfile, *m_sketchSurface);
		if ( crvOnSurf )
		{
			IwTArray<double> breakParams;
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
			double aTol;
			crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, XYZCurve, FALSE, FALSE);
			ShowCurve(m_pDoc, XYZCurve, green);
		}
	}

}

//////////////////////////////////////////////////////////////////////
// The posProfile is the sketch profile in the positive side.
// The negProfile is the sketch profile in the negative side.
// Both profiles are extended anteriorly and posteriorly such that
// the lines connected the end points of both profiles enclose
// the entire sketch profile.
//////////////////////////////////////////////////////////////////////
void COutlineProfileJigs::GetAnteriorBoundingProfiles
(
	double extendDistance,				// I: anterior extension distance
	IwBSplineCurve* &posAntUVProfile,	// O: Note, the direction follows from anterior to posterior
	IwBSplineCurve* &negAntUVProfile,	// O: Note, the direction follows from anterior to posterior
	IwBSplineCurve* &notchUVProfile		// O: 
)
{
	IwContext & iwContext = m_pDoc->GetIwContext();

	double F3APWidth = m_pDoc->GetVarTableValue("JIGS F3 SKETCH AP WIDTH") + 6.0; // "6mm" as extra extension. Anterior needs to cover more for small distal cut cases.

	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Get cuts info
	IwTArray<IwPoint3d> cutPoints, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);

	// Get lateralDistalCutPnt
	IwPoint3d lateralDistalCutPnt;
	if ( positiveSideIsLateral )
	{
		lateralDistalCutPnt = cutPoints.GetAt(6);
	}
	else
	{
		lateralDistalCutPnt = cutPoints.GetAt(7);
	}
	//
	IwPoint3d F3AnteriorEdgePoint = lateralDistalCutPnt + F3APWidth*sweepAxes.GetZAxis();

	// Get sketch surface
	IwBSplineSurface *sketchSurf=NULL;
	GetSketchSurface(sketchSurf);
	IwExtent2d skchDom = sketchSurf->GetNaturalUVDomain();

	// Get the BoundingProfiles
	IwBSplineCurve *posUVProfile, *negUVProfile;
	this->GetBoundingProfiles(0.0, posUVProfile, negUVProfile, notchUVProfile);
	// Copy the curves
	IwCurve* copyCurve;
	IwBSplineCurve *posAntUVCurve, *negAntUVCurve;
	posUVProfile->Copy(iwContext, copyCurve);
	posAntUVCurve = (IwBSplineCurve*)copyCurve;
	IwExtent1d posDom = posAntUVCurve->GetNaturalInterval();
	negUVProfile->Copy(iwContext, copyCurve);
	negAntUVCurve = (IwBSplineCurve*)copyCurve;
	IwExtent1d negDom = negAntUVCurve->GetNaturalInterval();

	// Trim posAntUVCurve & negAntUVCurve in the posterior portion (75%)
	IwExtent1d posTrimDom = IwExtent1d(posDom.Evaluate(0), posDom.Evaluate(0.75));
	posAntUVCurve->Trim(posTrimDom);
	IwExtent1d negTrimDom = IwExtent1d(negDom.Evaluate(0), negDom.Evaluate(0.75));
	negAntUVCurve->Trim(negTrimDom);

	// Extend/trim anteriorly
	IwPoint3d posAntUVPnt, negAntUVPnt;
	IwPoint3d posAntUVPnt3d, negAntUVPnt3d;
	// Get the anterior end points
	posAntUVCurve->EvaluatePoint(posTrimDom.Evaluate(0), posAntUVPnt);
	negAntUVCurve->EvaluatePoint(negTrimDom.Evaluate(0), negAntUVPnt);
	sketchSurf->EvaluatePoint(IwPoint2d(posAntUVPnt.x, posAntUVPnt.y), posAntUVPnt3d);
	sketchSurf->EvaluatePoint(IwPoint2d(negAntUVPnt.x, negAntUVPnt.y), negAntUVPnt3d);
	// Determine the needed extension
	double posExtension=1.0, negExtension=1.0;
	IwVector3d tempVec = F3AnteriorEdgePoint - posAntUVPnt3d;
	posExtension = tempVec.Dot(sweepAxes.GetZAxis()) + extendDistance;// This is the overall extension
	if ( posExtension < 1.0 )// minimum 1mm extension
		posExtension = 1.0;
	tempVec = F3AnteriorEdgePoint - negAntUVPnt3d;
	negExtension = tempVec.Dot(sweepAxes.GetZAxis()) + extendDistance;// This is the overall extension
	if ( negExtension < 1.0 )// minimum 1mm extension
		negExtension = 1.0;

	// Extend the profiles
	IwPoint3d posAntExtendedUVPnt, negAntExtendedUVPnt;
	posAntExtendedUVPnt = posAntUVPnt - IwPoint3d(0, posExtension, 0);
	if ( posAntExtendedUVPnt.y < skchDom.GetVMin() )
		posAntExtendedUVPnt.y = skchDom.GetVMin();
	negAntExtendedUVPnt = negAntUVPnt - IwPoint3d(0, negExtension, 0);
	if ( negAntExtendedUVPnt.y < skchDom.GetVMin() )
		negAntExtendedUVPnt.y = skchDom.GetVMin();

	// create a line segment between posAntExtendedUVPnt and posAntUVPnt
	IwBSplineCurve *posAntExtended, *negAntExtended;
	IwBSplineCurve::CreateLineSegment(iwContext, 3, posAntExtendedUVPnt, posAntUVPnt, posAntExtended);
	IwBSplineCurve::CreateLineSegment(iwContext, 3, negAntExtendedUVPnt, negAntUVPnt, negAntExtended);

	// Use posAntExtended and posAntUVCurve to define the new posAntUVProfile
	// Convert to sampling points
	IwTArray<IwPoint3d> pnts, posProfilePnts;
	double length, tol = 0.05;
	posDom = posAntExtended->GetNaturalInterval();
	posAntExtended->Length(posDom, 0.01, length);
	int size = (int)(length/4.0);
	if ( size < 2 )// minimum 2 points
	{
		size = 2;
		posAntExtended->EquallySpacedPoints(posDom.Evaluate(0), posDom.Evaluate(1), size, 0.01, &pnts, NULL);
		// We got 3 pnts. Remove the last 2 since this is a short segment
		pnts.RemoveLast();// remove the last one
		pnts.RemoveLast();// remove the last one
	}
	else
	{
		posAntExtended->EquallySpacedPoints(posDom.Evaluate(0), posDom.Evaluate(1), size, 0.01, &pnts, NULL);
		pnts.RemoveLast();// remove the last one
	}
	posProfilePnts.Append(pnts);
	// 
	posDom = posAntUVCurve->GetNaturalInterval();
	posAntUVCurve->Length(posDom, 0.01, length);
	size = (int)(length/4.0);
	posAntUVCurve->EquallySpacedPoints(posDom.Evaluate(0), posDom.Evaluate(1), size, 0.01, &pnts, NULL);
	pnts.RemoveAt(0);// remove the 0th one
	posProfilePnts.Append(pnts);
	// Create posAntUVProfile
	IwBSplineCurve::ApproximatePoints(iwContext, posProfilePnts, 3, NULL, NULL, FALSE, &tol, posAntUVProfile);
	if (0)
	{
		// Convert to xyz curve
		IwBSplineCurve* XYZCurve;
		IwCrvOnSurf* crvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*posAntUVProfile, *m_sketchSurface);
		if ( crvOnSurf )
		{
			IwTArray<double> breakParams;
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
			double aTol;
			crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, XYZCurve, FALSE, FALSE);
			ShowCurve(m_pDoc, XYZCurve, cyan);
		}
	}
	//////
	// Use negAntExtended and negAntUVCurve to define the new negAntUVProfile
	// Convert to sampling points
	IwTArray<IwPoint3d> negProfilePnts;
	negDom = negAntExtended->GetNaturalInterval();
	negAntExtended->Length(negDom, 0.01, length);
	size = (int)(length/4.0);
	if ( size < 2 )// minimum 2 points
	{
		size = 2;
		negAntExtended->EquallySpacedPoints(negDom.Evaluate(0), negDom.Evaluate(1), size, 0.01, &pnts, NULL);
		// We got 3 pnts. Remove the last 2 since this is a short segment
		pnts.RemoveLast();// remove the last one
		pnts.RemoveLast();// remove the last one
	}
	else
	{
		negAntExtended->EquallySpacedPoints(negDom.Evaluate(0), negDom.Evaluate(1), size, 0.01, &pnts, NULL);
		pnts.RemoveLast();// remove the last one
	}
	negProfilePnts.Append(pnts);
	// 
	negDom = negAntUVCurve->GetNaturalInterval();
	negAntUVCurve->Length(negDom, 0.01, length);
	size = (int)(length/4.0);
	negAntUVCurve->EquallySpacedPoints(negDom.Evaluate(0), negDom.Evaluate(1), size, 0.01, &pnts, NULL);
	pnts.RemoveAt(0);// remove the 0th one
	negProfilePnts.Append(pnts);
	// Create negAntUVProfile
	IwBSplineCurve::ApproximatePoints(iwContext, negProfilePnts, 3, NULL, NULL, FALSE, &tol, negAntUVProfile);
	if (0)
	{
		// Convert to xyz curve
		IwBSplineCurve* XYZCurve;
		IwCrvOnSurf* crvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*negAntUVProfile, *m_sketchSurface);
		if ( crvOnSurf )
		{
			IwTArray<double> breakParams;
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
			double aTol;
			crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, XYZCurve, FALSE, FALSE);
			ShowCurve(m_pDoc, XYZCurve, cyan);
		}
	}

}

//////////////////////////////////////////////////////////////////////////
// When Inner Surface Jigs is updated, this function will also be called 
// to update something related to the inner surface.
void COutlineProfileJigs::DetermineOutlineProfileJigsTabProfilePoints
(
	CInnerSurfaceJigs* innerSurfaceJigs
)
{
	if ( m_outlineProfileTabEndPoints.GetSize() > 0 )
	{
		m_tabProfiles.RemoveAll();

		CInnerSurfaceJigs* pInnerSurfaceJigs = innerSurfaceJigs;
		if ( pInnerSurfaceJigs == NULL )// if input innerSurfaceJigsis NULL, get it from m_pDoc
			pInnerSurfaceJigs = GetTotalDoc()->GetInnerSurfaceJigs();
		if ( pInnerSurfaceJigs == NULL )
			return;

		if ( pInnerSurfaceJigs )
		{
			IwBrep* innerSurfaceJigsBrep = pInnerSurfaceJigs->GetIwBrep();
			if (innerSurfaceJigsBrep)
			{
				IwTArray<IwFace*> faces;
				innerSurfaceJigsBrep->GetFaces(faces);
				IwBSplineSurface* innerSurf = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
				IwPoint2d delta, param0, param1, param2, param3;
				IwPoint3d tempVec;
				IwPoint3d tempPnt, pnt;
				IwPoint3d cPnt0, cPnt1, cPnt2, cPnt3;
				double prevDist, distSoFar;
				double oneMM = 0.85, threeMM = 3.15;
				for (unsigned i=0; i<m_outlineProfileTabPoints.GetSize(); i+=2)// 2 steps
				{
					// distance to inner surface
					DistFromPointToSurface(m_outlineProfileTabPoints.GetAt(i), innerSurf, cPnt0, param0);
					DistFromPointToSurface(m_outlineProfileTabEndPoints.GetAt(i), innerSurf, cPnt1, param1);
					DistFromPointToSurface(m_outlineProfileTabEndPoints.GetAt(i+1), innerSurf, cPnt2, param2);
					DistFromPointToSurface(m_outlineProfileTabPoints.GetAt(i+1), innerSurf, cPnt3, param3);
					// Side products - update tab end points which are preferred on the inner surface jigs
					m_outlineProfileTabEndPoints.SetAt(i, cPnt1);
					m_outlineProfileTabEndPoints.SetAt(i+1, cPnt2);
					// first tab profile
					distSoFar = 0;
					prevDist = 0;
					delta = (param0-param1)/70;
					for (int j=0;j<71;j++)
					{
						innerSurf->EvaluatePoint(param1 + j*delta, pnt);
						if ( j>0 ) // Calculate the profile distance
						{
							distSoFar += pnt.DistanceBetween(m_tabProfiles.GetLast());
							if ( prevDist < oneMM && distSoFar >= oneMM )
								m_tabProfiles.Add(IwPoint3d(-11111,-11111,-11111));// special point to indicate the tab end length is 1
							else if ( prevDist < threeMM && distSoFar >= threeMM )
								m_tabProfiles.Add(IwPoint3d(-33333,-33333,-33333));// special point to indicate the tab end length is 3
							prevDist = distSoFar;
						}
						m_tabProfiles.Add(pnt);
					}
					m_tabProfiles.Add(IwPoint3d(-111,-111,-111));// special point to indicate the end of a tab profile
					// second tab profile (tab end profile)
					delta = (param2-param1)/30;
					for (int j=0;j<31;j++)
					{
						innerSurf->EvaluatePoint(param1 + j*delta, pnt);
						m_tabProfiles.Add(pnt);
					}
					m_tabProfiles.Add(IwPoint3d(-111,-111,-111));// special point to indicate the end of a tab profile
					// third tab profile
					distSoFar = 0;
					prevDist = 0;
					delta = (param3-param2)/70;
					for (int j=0;j<71;j++)
					{
						innerSurf->EvaluatePoint(param2 + j*delta, pnt);
						if ( j>0 ) // Calculate the profile distance
						{
							distSoFar += pnt.DistanceBetween(m_tabProfiles.GetLast());
							if ( prevDist < oneMM && distSoFar >= oneMM )
								m_tabProfiles.Add(IwPoint3d(-11111,-11111,-11111));// special point to indicate the tab end length is 1
							else if ( prevDist < threeMM && distSoFar >= threeMM )
								m_tabProfiles.Add(IwPoint3d(-33333,-33333,-33333));// special point to indicate the tab end length is 3
							prevDist = distSoFar;
						}
						m_tabProfiles.Add(pnt);
					}
					m_tabProfiles.Add(IwPoint3d(-111,-111,-111));// special point to indicate the end of a tab profile
				}
			}
		}
		// Let's delete m_glOutlineProfileCurveList in the end of m_tabProfiles creation
		DeleteDisplayList( m_glOutlineProfileCurveList );// to force update display
	}

}

bool COutlineProfileJigs::IsMemberDataChanged
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo, 
	IwTArray<IwPoint3d>& tabPoints, 
	IwTArray<IwPoint3d>& tabEndPoints, 
	IwPoint3d& posDroppingCenter, 
	IwPoint3d& negDroppingCenter,
	IwTArray<double> &windowCutAngles, 
	bool keyDataOnly
)
{
	bool changed = false;

	if ( featurePoints.GetSize() != m_outlineProfileFeaturePoints.GetSize() )
	{
		changed = true;
		return changed;
	}
	for ( unsigned i=0; i<featurePoints.GetSize(); i++)
	{
		if ( featurePoints.GetAt(i) != m_outlineProfileFeaturePoints.GetAt(i) )
		{
			changed = true;
			break;
		}
	}
	if ( changed )
		return changed;
	////////////////////////////////////////////////////
	if ( featurePointsInfo.GetSize() != m_outlineProfileFeaturePointsInfo.GetSize() )
	{
		changed = true;
		return changed;
	}
	for ( unsigned i=0; i<featurePointsInfo.GetSize(); i++)
	{
		if ( featurePointsInfo.GetAt(i) != m_outlineProfileFeaturePointsInfo.GetAt(i) )
		{
			changed = true;
			break;
		}
	}
	if ( changed )
		return changed;
	////////////////////////////////////////////////////
	if ( posDroppingCenter != m_posDroppingCenter )
	{
		changed = true;
		return changed;
	}
	if ( negDroppingCenter != m_negDroppingCenter )
	{
		changed = true;
		return changed;
	}

	////////////////////////////////////////////////////
	if ( tabPoints.GetSize() != m_outlineProfileTabPoints.GetSize() )
	{
		changed = true;
		return changed;
	}
	for ( unsigned i=0; i<tabPoints.GetSize(); i++)
	{
		if ( tabPoints.GetAt(i) != m_outlineProfileTabPoints.GetAt(i) )
		{
			changed = true;
			break;
		}
	}
	if ( changed )
		return changed;

	////////////////////////////////////////////////////
	if ( tabEndPoints.GetSize() != m_outlineProfileTabEndPoints.GetSize() )
	{
		changed = true;
		return changed;
	}
	for ( unsigned i=0; i<tabEndPoints.GetSize(); i++)
	{
		if ( tabEndPoints.GetAt(i) != m_outlineProfileTabEndPoints.GetAt(i) )
		{
			changed = true;
			break;
		}
	}
	if ( changed )
		return changed;

	////////////////////////////////////////////////////
	if ( keyDataOnly )
		return changed;

	//////////////////////////////////////////////////////
	if ( windowCutAngles.GetSize() != m_windowCutAngles.GetSize() )
	{
		changed = true;
		return changed;
	}

	for ( unsigned i=0; i<windowCutAngles.GetSize(); i++)
	{
		if ( !IS_EQ_TOL6(windowCutAngles.GetAt(i), m_windowCutAngles.GetAt(i)) )
			changed = true;
	}
	if ( changed )
		return changed;

	return changed;
}

///////////////////////////////////////////////////////////////
// This function returns the notch point on the sketch surface
IwPoint3d COutlineProfileJigs::GetJigsNotchPoint()
{
	IwPoint3d notchPnt, normal;

	for (unsigned i=0; i<m_outlineProfileFeaturePointsInfo.GetSize(); i++)
	{
		if ( m_outlineProfileFeaturePointsInfo.GetAt(i) == 2 ) // CW arc center, notch arc center
		{
			IwPoint3d stPnt = m_outlineProfileFeaturePoints.GetAt(i-1);
			IwPoint3d ctrPnt = m_outlineProfileFeaturePoints.GetAt(i);
			IwPoint3d edPnt = m_outlineProfileFeaturePoints.GetAt(i+1);
			IwVector3d stVec = stPnt - ctrPnt;
			double radius = stVec.Length();
			stVec.Unitize();
			IwVector3d edVec = edPnt - ctrPnt;
			edVec.Unitize();
			IwVector3d avgVec = 0.5*stVec + 0.5*edVec;
			avgVec.Unitize();
			if ( avgVec.y > 0 )// vector should be against v direction
				avgVec = -avgVec;
			IwPoint3d midArcPnt = ctrPnt + radius*avgVec;
			ConvertUVToXYZ( midArcPnt, notchPnt, normal);
			break;
		}
	}

	return notchPnt;
}

void COutlineProfileJigs::GetWindowCutAngles
(
	IwTArray<double>& windowCutAngles,	// O:
	IwTArray<IwPoint3d>* windowCutTips,	// O: window cut tips (center)
	IwPoint3d* posCenterPoint,			// O: core hole center
	IwPoint3d* negCenterPoint			// O: core hole center
)
{
	windowCutAngles.RemoveAll();
	windowCutAngles.Append(m_windowCutAngles);

	//
	if ( windowCutTips )
	{
		windowCutTips->RemoveAll();
		windowCutTips->Append(m_windowCutTips);
	}
	if ( posCenterPoint )
		*posCenterPoint = m_posCoreCenterPoint;
	if ( negCenterPoint )
		*negCenterPoint = m_negCoreCenterPoint;

}

void COutlineProfileJigs::SetWindowCutAngles(IwTArray<double> windowCutAngles)
{
	m_windowCutAngles.RemoveAll();
	m_windowCutAngles.Append(windowCutAngles);

	// Get peg position
	IwPoint3d posPegPosition, negPegPosition;
	EstimatePegPositions(posPegPosition, negPegPosition);

	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();
	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Update CoreCenterPoint
	m_posCoreCenterPoint = posPegPosition;
	m_negCoreCenterPoint = negPegPosition;

	m_windowCutTips.RemoveAll();
	// Update F1 window tips
	double radius = 11.25;
	double posWindowCutAngle = m_windowCutAngles.GetAt(0);
	IwPoint3d posWindowCutTip = m_posCoreCenterPoint - radius*sin(posWindowCutAngle/180.0*IW_PI)*sweepAxes.GetXAxis() + radius*cos(posWindowCutAngle/180.0*IW_PI)*sweepAxes.GetYAxis();
	m_windowCutTips.Add(posWindowCutTip);
	double negWindowCutAngle = m_windowCutAngles.GetAt(1);
	IwPoint3d negWindowCutTip = m_negCoreCenterPoint - radius*sin(-negWindowCutAngle/180.0*IW_PI)*sweepAxes.GetXAxis() + radius*cos(-negWindowCutAngle/180.0*IW_PI)*sweepAxes.GetYAxis();
	m_windowCutTips.Add(negWindowCutTip);
	
	// update F2 window tips
	double radiusF2 = 9.0;
	double posWindowCutAngleF2;
	if ( positiveSideIsLateral )
		posWindowCutAngleF2 = m_windowCutAngles.GetAt(2);
	else
		posWindowCutAngleF2 = -(m_windowCutAngles.GetAt(2)+23.4);// why 23.4? it's a weird start part
	IwPoint3d posWindowCutTipF2 = m_posCoreCenterPoint - radiusF2*sin(posWindowCutAngleF2/180.0*IW_PI)*sweepAxes.GetXAxis() + radiusF2*cos(posWindowCutAngleF2/180.0*IW_PI)*sweepAxes.GetYAxis();
	m_windowCutTips.Add(posWindowCutTipF2);

	double negWindowCutAngleF2;
	if ( positiveSideIsLateral )
		negWindowCutAngleF2 = m_windowCutAngles.GetAt(3)+23.4;// why 23.4? it's a weird start part
	else
		negWindowCutAngleF2 = -m_windowCutAngles.GetAt(3);
	IwPoint3d negWindowCutTipF2 = m_negCoreCenterPoint - radiusF2*sin(negWindowCutAngleF2/180.0*IW_PI)*sweepAxes.GetXAxis() + radiusF2*cos(negWindowCutAngleF2/180.0*IW_PI)*sweepAxes.GetYAxis();
	m_windowCutTips.Add(negWindowCutTipF2);

	// Update core hole profiles
	DetermineCoreHolesProfiles();
}

IwBSplineCurve* COutlineProfileJigs::GetUVCurveOnSketchSurface
(
	bool bSquareOffPosteriorTips,			// I: this flag allows caller to control whether return the squared tips or circular tips.
	CInnerSurfaceJigs* innerSurfaceJigs,	// I: optional input
	IwTArray<IwPoint3d>* squaredOffCornerArcs		// O: the 4 squared-off corner arcs (represent in sketch surface UV domain)
)
{
	IwBSplineCurve* uvCurve = NULL;

	IwTArray<IwPoint3d>		outlineProfileFeaturePoints;		
	IwTArray<int>			outlineProfileFeaturePointsInfo;
	outlineProfileFeaturePoints.Append(m_outlineProfileFeaturePoints);
	outlineProfileFeaturePointsInfo.Append(m_outlineProfileFeaturePointsInfo);
	IwTArray<int> squaredOffCornerCenterIndexes;

	// If square off posterior tips, modify posterior tip profile here.
	// But this function also needs inner surface jig to be existed.
	if ( innerSurfaceJigs == NULL )
		innerSurfaceJigs = GetTotalDoc()->GetInnerSurfaceJigs();

	if ( bSquareOffPosteriorTips && // bSquareOffPosteriorTips is an extra flag to allow caller to control whether return the squared tips or circular tips.
		 GetTotalDoc()->GetAdvancedControlJigs()->GetOutlineSquareOffPosteriorTip() && 
		 innerSurfaceJigs )
	{
		SquareOffPosteriorTips(innerSurfaceJigs, outlineProfileFeaturePoints, outlineProfileFeaturePointsInfo, &squaredOffCornerCenterIndexes);
		if (squaredOffCornerArcs)
		{
			int cornerArcCenter;
			squaredOffCornerArcs->RemoveAll();
			for (unsigned i=0; i<squaredOffCornerCenterIndexes.GetSize(); i++)
			{
				cornerArcCenter = squaredOffCornerCenterIndexes.GetAt(i);
				squaredOffCornerArcs->Add(outlineProfileFeaturePoints.GetAt(cornerArcCenter-1));// arc start point
				squaredOffCornerArcs->Add(outlineProfileFeaturePoints.GetAt(cornerArcCenter));// arc center point
				squaredOffCornerArcs->Add(outlineProfileFeaturePoints.GetAt(cornerArcCenter+1));// arc end point
			}
		}
	}

	// Create uvCurve by calling IFemurImplant module since outlineProfileJig and outlineProfile share the same parametric domain.
	uvCurve = IFemurImplant::OutlineProfile_CreateUVCurveOnSketchSurface(outlineProfileFeaturePoints, outlineProfileFeaturePointsInfo);
	
	return uvCurve;
}

void COutlineProfileJigs::SquareOffPosteriorTips
(
	CInnerSurfaceJigs* pInnerSurfaceJigs,				// I:
	IwTArray<IwPoint3d>& outlineProfileFeaturePoints,	// I/O:
	IwTArray<int>& outlineProfileFeaturePointsInfo,		// I/O:
	IwTArray<int>* squaredOffCornerCenterIndexes		// O: The position indexes for the 4 squared-off corner centers
)
{
	if ( outlineProfileFeaturePoints.GetSize() == 0 )
		return;
	if ( pInnerSurfaceJigs == NULL )
		return;
	IwBrep* innerSurfBrep = pInnerSurfaceJigs->GetIwBrep();
	if ( innerSurfBrep == NULL )
		return;
	IwTArray<IwFace*> faces;
	innerSurfBrep->GetFaces(faces);
	if ( faces.GetSize() == 0 )
		return;
	IwBSplineSurface* innerSurf = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();

	IwAxis2Placement femAxis = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	IwVector3d zAxis = femAxis.GetZAxis();

	IwTArray<IwPoint3d> cutPnts, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPnts, cutOrientations);
	IwPoint3d posPostCutPnt = cutPnts.GetAt(0);
	IwPoint3d posPostCutNormal = cutOrientations.GetAt(0);
	IwPoint3d negPostCutPnt = cutPnts.GetAt(1);
	IwPoint3d negPostCutNormal = cutOrientations.GetAt(1);

	// Looking for the posterior tips
	int posArcCtrIndex, negArcCtrIndex, encounteredCCWArcNo=0;
	for (unsigned i=0; i<outlineProfileFeaturePointsInfo.GetSize(); i++)
	{
		if (outlineProfileFeaturePointsInfo.GetAt(i) == 1) // CCW arc
		{
			if ( encounteredCCWArcNo == 1 ) // the 2nd encountered CCW arc is the positive side posterior arc center
				posArcCtrIndex = i;
			else if ( encounteredCCWArcNo == 2 ) // the 2nd encountered CCW arc is the negative side posterior arc center
				negArcCtrIndex = i;
			encounteredCCWArcNo++;
		}
	}
	// posterior arc radii
	double posPostArcRadius = outlineProfileFeaturePoints.GetAt(posArcCtrIndex).DistanceBetween(outlineProfileFeaturePoints.GetAt(posArcCtrIndex+1));
	double negPostArcRadius = outlineProfileFeaturePoints.GetAt(negArcCtrIndex).DistanceBetween(outlineProfileFeaturePoints.GetAt(negArcCtrIndex+1));

	// Determine posterior arc start/end points in 3d space
	IwPoint3d posStartPntUV = outlineProfileFeaturePoints.GetAt(posArcCtrIndex-1);
	IwPoint3d posCenterPntUV = outlineProfileFeaturePoints.GetAt(posArcCtrIndex);
	IwPoint3d posEndPntUV = outlineProfileFeaturePoints.GetAt(posArcCtrIndex+1);
	IwPoint3d negStartPntUV = outlineProfileFeaturePoints.GetAt(negArcCtrIndex-1);
	IwPoint3d negCenterPntUV = outlineProfileFeaturePoints.GetAt(negArcCtrIndex);
	IwPoint3d negEndPntUV = outlineProfileFeaturePoints.GetAt(negArcCtrIndex+1);
	// Convert to XYZ space
	IwPoint3d posStartPnt3d, posEndPnt3d, negStartPnt3d, negEndPnt3d, normal;
	ConvertUVToXYZ(posStartPntUV, posStartPnt3d, normal);
	ConvertUVToXYZ(posEndPntUV, posEndPnt3d, normal);
	ConvertUVToXYZ(negStartPntUV, negStartPnt3d, normal);
	ConvertUVToXYZ(negEndPntUV, negEndPnt3d, normal);
	// Project onto posterior cut planes
	IwPoint3d posStartPnt3dProj, posEndPnt3dProj, negStartPnt3dProj, negEndPnt3dProj;
	posStartPnt3dProj = posStartPnt3d.ProjectPointToPlane(posPostCutPnt, posPostCutNormal);
	posEndPnt3dProj = posEndPnt3d.ProjectPointToPlane(posPostCutPnt, posPostCutNormal);
	negStartPnt3dProj = negStartPnt3d.ProjectPointToPlane(negPostCutPnt, negPostCutNormal);
	negEndPnt3dProj = negEndPnt3d.ProjectPointToPlane(negPostCutPnt, negPostCutNormal);
	// Now determine the inner surface intersection (discrete points) with posterior cuts 
	IwTArray<IwPoint3d> posPostCutIntersectPnts, negPostCutIntersectPnts;
	IwTArray<IwPoint3d> posPostCutIntersectPntsUV, negPostCutIntersectPntsUV;
	int steps = 15, extend = 3;
	bool bInt;
	IwPoint3d pnt, intPnt, cPnt;
	IwPoint2d paramUV;
	// Positive side
	IwVector3d deltaVec = (posEndPnt3dProj - posStartPnt3dProj)/(steps-1);
	for (int i=-extend; i<(steps+extend); i++)
	{
		pnt = posStartPnt3dProj + i*deltaVec;
		bInt = IntersectSurfaceByLine(innerSurf, pnt, zAxis, intPnt); 
		if ( bInt )
		{
			posPostCutIntersectPnts.Add(intPnt);
			// map to sketch UV domain
			DistFromPointToSurface(intPnt, m_sketchSurface, cPnt, paramUV);
			posPostCutIntersectPntsUV.Add(IwPoint3d(paramUV.x, paramUV.y, 0));
		}
	}
	IwBSplineCurve* posPostCutCurveUV;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), posPostCutIntersectPntsUV, NULL, 3, NULL, NULL, false, IW_IT_CHORDLENGTH, posPostCutCurveUV);
if (0)
{
	IwBSplineCurve* posPostCutCurve;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), posPostCutIntersectPnts, NULL, 3, NULL, NULL, false, IW_IT_CHORDLENGTH, posPostCutCurve);
	ShowCurve(m_pDoc, posPostCutCurve, red);
}
	// Negative side
	deltaVec = (negEndPnt3dProj - negStartPnt3dProj)/(steps-1);
	for (int i=-extend; i<(steps+extend); i++)
	{
		pnt = negStartPnt3dProj + i*deltaVec;
		bInt = IntersectSurfaceByLine(innerSurf, pnt, zAxis, intPnt); 
		if ( bInt )
		{
			negPostCutIntersectPnts.Add(intPnt);
			// map to sketch UV domain
			DistFromPointToSurface(intPnt, m_sketchSurface, cPnt, paramUV);
			negPostCutIntersectPntsUV.Add(IwPoint3d(paramUV.x, paramUV.y, 0));
		}
	}
	IwBSplineCurve* negPostCutCurveUV;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), negPostCutIntersectPntsUV, NULL, 3, NULL, NULL, false, IW_IT_CHORDLENGTH, negPostCutCurveUV);
if (0)
{
	IwBSplineCurve* negPostCutCurve;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), negPostCutIntersectPnts, NULL, 3, NULL, NULL, false, IW_IT_CHORDLENGTH, negPostCutCurve);
	ShowCurve(m_pDoc, negPostCutCurve, blue);
}
	// Determine the square corner arcs
	double cornerRadiusRatio = GetTotalDoc()->GetAdvancedControlJigs()->GetOutlineSquareOffCornerRadiusRatio();
	double posCornerRadius = cornerRadiusRatio*posPostArcRadius;
	double negCornerRadius = cornerRadiusRatio*negPostArcRadius;
	// Offset with cornerRadius
	double aTol;
	IwBSplineCurve *posPostCutCurveUVOffset, *negPostCutCurveUVOffset;
	IwVector3d domainNormal = IwVector3d(0,0,1);// normal of UV domain
	posPostCutCurveUV->CreateSimpleOffset(m_pDoc->GetIwContext(), 0.01, domainNormal, -posCornerRadius, posPostCutCurveUVOffset, aTol);
	negPostCutCurveUV->CreateSimpleOffset(m_pDoc->GetIwContext(), 0.01, domainNormal, -negCornerRadius, negPostCutCurveUVOffset, aTol);

	//////////////////////////////////////////////////////////////////////
	// Determine the corners
	// For Negative side. Need to handle negative side first
	// The corner center is the intersection of the negPostCutCurveUVOffset and the offset tangent vectors at arc start/end points.
	IwVector3d vec = negStartPntUV  - negCenterPntUV; // outward vector
	vec.Unitize();
	pnt = negStartPntUV - negCornerRadius*vec;
	IwVector3d negPosTangVec = IwVector3d(vec.y, -vec.x, 0);// swap x and y components becomes tangent vector
	// Intersect with negPostCutCurveUVOffset -> negPosCornerCenterPntUV
	double param, startParam, endParam;
	double maxDist = 20.0;
	IwVector3d intVec;
	IwPoint3d negPosCornerStartPntUV, negPosCornerCenterPntUV, negPosCornerEndPntUV;
	IntersectCurveByVector(negPostCutCurveUVOffset, negPostCutCurveUVOffset->GetNaturalInterval(), pnt, negPosTangVec, NULL, maxDist, negPosCornerCenterPntUV, param, intVec);
	negPosCornerStartPntUV = negPosCornerCenterPntUV + negCornerRadius*vec;
	DistFromPointToCurve(negPosCornerCenterPntUV, negPostCutCurveUV, negPosCornerEndPntUV, startParam);
if (0)
{
	IwPoint3d xyzPnt;
	ConvertUVToXYZ(negPosCornerStartPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, brown);
	ConvertUVToXYZ(negPosCornerCenterPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, red);
	ConvertUVToXYZ(negPosCornerEndPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, magenta);
}
	
	// negative-negative side
	vec = negEndPntUV  - negCenterPntUV; // outward vector
	vec.Unitize();
	pnt = negEndPntUV - negCornerRadius*vec;
	IwVector3d negNegTangVec = IwVector3d(vec.y, -vec.x, 0);// swap x and y components becomes tangent vector
	// Intersect with negPostCutCurveUVOffset -> negNegCornerCenterPntUV
	IwPoint3d negNegCornerStartPntUV, negNegCornerCenterPntUV, negNegCornerEndPntUV;
	IntersectCurveByVector(negPostCutCurveUVOffset, negPostCutCurveUVOffset->GetNaturalInterval(), pnt, negNegTangVec, NULL, maxDist, negNegCornerCenterPntUV, param, intVec);
	negNegCornerEndPntUV = negNegCornerCenterPntUV + negCornerRadius*vec;
	DistFromPointToCurve(negNegCornerCenterPntUV, negPostCutCurveUV, negNegCornerStartPntUV, endParam);
if (0)
{
	IwPoint3d xyzPnt;
	ConvertUVToXYZ(negNegCornerStartPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, brown);
	ConvertUVToXYZ(negNegCornerCenterPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, red);
	ConvertUVToXYZ(negNegCornerEndPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, magenta);
}
	// negPostCutCurveUVPnts the cut curve points in the posterior tip
	IwTArray<IwPoint3d> negPostCutCurveUVPnts;
	IwTArray<int> negPostCutCurveUVPntsInfo;
	int negSteps = 6;
	double deltaParam = (endParam - startParam)/negSteps;
	for (int i=1; i<negSteps; i++)
	{
		param = startParam + i*deltaParam;
		negPostCutCurveUV->EvaluatePoint(param, pnt);
		negPostCutCurveUVPnts.Add(pnt);
		negPostCutCurveUVPntsInfo.Add(0);// interpolation points
	}
if (0)
{
	IwPoint3d xyzPnt;
	for ( unsigned i=0; i<negPostCutCurveUVPnts.GetSize(); i++ )
	{
		pnt = negPostCutCurveUVPnts.GetAt(i);
		ConvertUVToXYZ(pnt, xyzPnt, normal);
		ShowPoint(m_pDoc, xyzPnt, darkGreen);
	}
}
	// Convert negative posterior semi-circular arc into 2 corner arcs and many interpolation points
	int negPrevArcIndex = negArcCtrIndex - 2; // the index prior to the negative arc
	// remove arc end/center/start points
	outlineProfileFeaturePoints.RemoveAt(negPrevArcIndex+3);
	outlineProfileFeaturePointsInfo.RemoveAt(negPrevArcIndex+3);
	outlineProfileFeaturePoints.RemoveAt(negPrevArcIndex+2);
	outlineProfileFeaturePointsInfo.RemoveAt(negPrevArcIndex+2);
	outlineProfileFeaturePoints.RemoveAt(negPrevArcIndex+1);
	outlineProfileFeaturePointsInfo.RemoveAt(negPrevArcIndex+1);
	// Add the negative-negative side corner arc
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, negNegCornerEndPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, 4);// end point
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, negNegCornerCenterPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, 21);// squared-off arc center point
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, negNegCornerStartPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, 3);// start point
	// Add the negative negPostCutCurveUVPnts
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, &negPostCutCurveUVPnts);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, &negPostCutCurveUVPntsInfo);
	// Add the negative-positive side corner arc
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, negPosCornerEndPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, 4);// end point
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, negPosCornerCenterPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, 21);// squared-off arc center point
	outlineProfileFeaturePoints.InsertAt(negPrevArcIndex+1, negPosCornerStartPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(negPrevArcIndex+1, 3);// start point


	////////////////////////////////////////////////////////////////////////
	// For Positive side
	// The corner center is the intersection of the posPostCutCurveUVOffset and the offset tangent vectors at arc start/end points.
	vec = posStartPntUV  - posCenterPntUV; // outward vector
	vec.Unitize();
	pnt = posStartPntUV - posCornerRadius*vec;
	IwVector3d posPosTangVec = IwVector3d(vec.y, -vec.x, 0);// swap x and y components becomes tangent vector
	// Intersect with posPostCutCurveUVOffset -> posPosCornerCenterPntUV
	IwPoint3d posPosCornerStartPntUV, posPosCornerCenterPntUV, posPosCornerEndPntUV;
	IntersectCurveByVector(posPostCutCurveUVOffset, posPostCutCurveUVOffset->GetNaturalInterval(), pnt, posPosTangVec, NULL, maxDist, posPosCornerCenterPntUV, param, intVec);
	posPosCornerStartPntUV = posPosCornerCenterPntUV + posCornerRadius*vec;
	DistFromPointToCurve(posPosCornerCenterPntUV, posPostCutCurveUV, posPosCornerEndPntUV, startParam);
if (0)
{
	IwPoint3d xyzPnt;
	ConvertUVToXYZ(posPosCornerStartPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, brown);
	ConvertUVToXYZ(posPosCornerCenterPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, red);
	ConvertUVToXYZ(posPosCornerEndPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, magenta);
}
	
	// positive-negative side
	vec = posEndPntUV  - posCenterPntUV; // outward vector
	vec.Unitize();
	pnt = posEndPntUV - posCornerRadius*vec;
	IwVector3d posNegTangVec = IwVector3d(vec.y, -vec.x, 0);// swap x and y components becomes tangent vector
	// Intersect with posPostCutCurveUVOffset -> posNegCornerCenterPntUV
	IwPoint3d posNegCornerStartPntUV, posNegCornerCenterPntUV, posNegCornerEndPntUV;
	IntersectCurveByVector(posPostCutCurveUVOffset, posPostCutCurveUVOffset->GetNaturalInterval(), pnt, posNegTangVec, NULL, maxDist, posNegCornerCenterPntUV, param, intVec);
	posNegCornerEndPntUV = posNegCornerCenterPntUV + posCornerRadius*vec;
	DistFromPointToCurve(posNegCornerCenterPntUV, posPostCutCurveUV, posNegCornerStartPntUV, endParam);
if (0)
{
	IwPoint3d xyzPnt;
	ConvertUVToXYZ(posNegCornerStartPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, brown);
	ConvertUVToXYZ(posNegCornerCenterPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, red);
	ConvertUVToXYZ(posNegCornerEndPntUV, xyzPnt, normal);
	ShowPoint(m_pDoc, xyzPnt, magenta);
}
	// posPostCutCurveUVPnts the cut curve points in the posterior tip
	IwTArray<IwPoint3d> posPostCutCurveUVPnts;
	IwTArray<int> posPostCutCurveUVPntsInfo;
	int posSteps = 6;
	deltaParam = (endParam - startParam)/posSteps;
	for (int i=1; i<posSteps; i++)
	{
		param = startParam + i*deltaParam;
		posPostCutCurveUV->EvaluatePoint(param, pnt);
		posPostCutCurveUVPnts.Add(pnt);
		posPostCutCurveUVPntsInfo.Add(0);// interpolation points
	}
if (0)
{
	IwPoint3d xyzPnt;
	for ( unsigned i=0; i<posPostCutCurveUVPnts.GetSize(); i++ )
	{
		pnt = posPostCutCurveUVPnts.GetAt(i);
		ConvertUVToXYZ(pnt, xyzPnt, normal);
		ShowPoint(m_pDoc, xyzPnt, darkGreen);
	}
}
	// Convert positive posterior semi-circular arc into 2 corner arcs and many interpolation points
	int posPrevArcIndex = posArcCtrIndex - 2; // the index prior to the positive arc
	// remove arc end/center/start points
	outlineProfileFeaturePoints.RemoveAt(posPrevArcIndex+3);
	outlineProfileFeaturePointsInfo.RemoveAt(posPrevArcIndex+3);
	outlineProfileFeaturePoints.RemoveAt(posPrevArcIndex+2);
	outlineProfileFeaturePointsInfo.RemoveAt(posPrevArcIndex+2);
	outlineProfileFeaturePoints.RemoveAt(posPrevArcIndex+1);
	outlineProfileFeaturePointsInfo.RemoveAt(posPrevArcIndex+1);
	// Add the positive-negative side corner arc
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, posNegCornerEndPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, 4);// end point
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, posNegCornerCenterPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, 21);// squared-off arc center point
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, posNegCornerStartPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, 3);// start point
	// Add the positive posPostCutCurveUVPnts
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, &posPostCutCurveUVPnts);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, &posPostCutCurveUVPntsInfo);
	// Add the positive-positive side corner arc
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, posPosCornerEndPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, 4);// end point
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, posPosCornerCenterPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, 21);// squared-off arc center point
	outlineProfileFeaturePoints.InsertAt(posPrevArcIndex+1, posPosCornerStartPntUV);
	outlineProfileFeaturePointsInfo.InsertAt(posPrevArcIndex+1, 3);// start point

	// determine squaredOffCornerCenterIndexes
	if (squaredOffCornerCenterIndexes)
	{
		squaredOffCornerCenterIndexes->RemoveAll();
		for (int i=0; i<outlineProfileFeaturePointsInfo.GetSize(); i++)
		{
			if ( outlineProfileFeaturePointsInfo.GetAt(i) == 21 || outlineProfileFeaturePointsInfo.GetAt(i) == 22 )//squared-off corner arc center point
			{
				squaredOffCornerCenterIndexes->Add(i);
			}
		}
	}
}

//////////////////////////////////////////////////////////
// This is a temporarily function for alpha testing. 
// Using this function to estimate peg positions,
// users still can run positioning jig testing without 
// creating PS features.
void COutlineProfileJigs::EstimatePegPositions(IwPoint3d& posPegPosition, IwPoint3d& negPegPosition)
{

	IFemurImplant::PegPointsFromStrykerFemLayout(posPegPosition, negPegPosition);

	m_posPegPosition = posPegPosition;	
	m_negPegPosition = negPegPosition;	
	return;
}
