#pragma once
#include <qstring.h>
#pragma warning( disable : 4996 4805 )
#ifdef GetObject
#undef GetObject
#endif

#include <iwtopologyTraverser.h>
//#include <IwFilletSolver.h>
//#include <IwFilletStandardSolver.h>
//#include <IwFilletExecutive.h>
#include <IwTrimmingTools.h>
#include <IwInterface.h>
#include <IwCutter.h>
#include <IwBrepCutting.h>
#include <HwTSLibIges.h>
#include <iwtslib_all.h>
#include <IwMerge.h>
#pragma warning( default : 4996 4805 )

#define		HUGE_DOUBLE		1e+12
#define		HUGE_FLOAT		1e+10
#define		HUGE_INT		1000000000
#define		IS_EQ_TOL6(a,b) (fabs((a)-(b))<0.000001)

////////////////////////////////TYPEDEFs///////////////////////////////
typedef IwTArray< int >				CIntArray;
typedef IwTArray< double >			CDoubleArray;
typedef IwTArray< IwPoint3d >		CPointArray;
typedef IwTArray< IwVector3d >		CVectorArray;
typedef IwTArray< IwCurve* >		CCurveArray;
typedef IwTArray< IwBSplineCurve* >	CSplineArray;
typedef IwTArray< IwSurface* >		CSurfArray;
typedef IwTArray< IwVertex* >		CVertArray;
typedef IwTArray< IwEdge* >			CEdgeArray;
typedef IwTArray< IwFace* >			CFaceArray;
typedef IwTArray< IwBrep* >			CBrepArray;

typedef bool (*CCompareFunc)( double a, double b );

template < class T >
void Swap( T& a, T& b )
{
	T c;

	c = a;
	a = b;
	b = c;
}

template < class T >
bool IsInArray( T& a, IwTArray< T >& array )
{
	for( ULONG i = 0; i < array.GetSize(); i++ )
	{
		if( a == array[i] )
			return true;
	}

	return false;
}

template < class T >
bool IsInArray( T& a, int nElems, T* array )
{
	for( int i = 0; i < nElems; i++ )
	{
		if( a == array[i] )
			return true;
	}

	return false;
}

template < class T >
void ArraysIntersection( IwTArray< T >& ar1, IwTArray< T >& ar2, IwTArray< T >& result )
{
	IwTArray< T >		ar;

	for( ULONG i = 0; i < ar1.GetSize(); i++ )
	{
		if( IsInArray( ar1[i], ar2 ) )
			ar.Add( ar1[i] );
	}

	result.ReSet();
	result.Append( ar );
}

template < class T >
int IdxInArray( T& a, IwTArray< T >& array )
{
	for( unsigned long i = 0; i < array.GetSize(); i++ )
	{
		if( a == array[i] )
			return i;
	}

	return -1;
}

template < class T >
int IdxInArray( T& a, int nElems, T* array )
{
	for( int i = 0; i < nElems; i++ )
	{
		if( a == array[i] )
			return i;
	}

	return -1;
}

namespace SMLibHelpers
{
	///////////////////////////////////ENUMs////////////////////////////////
	enum CCompVal { MinVal, MaxVal };

	enum CEdgeConvexity
	{
		EDGE_UNDEFINED,
		EDGE_TANGENT,
		EDGE_CONVEX,
		EDGE_CONCAVE
	};

	enum CCurveEnd
	{
		START_POINT,
		END_POINT
	};

	struct CCurve
	{
		IwCurve*		pCurve;
		IwPoint3d		pt0;
		IwPoint3d		pt1;
		bool			bTaken;

		CCurve()
		{
			pCurve = NULL;
			bTaken = false;
		}
	};

	/////////////////////////////////////Inline-s/////////////////////////////
	inline bool fBigger( double a, double b )
	{
		return a > b;
	}

	inline bool fSmaller( double a, double b )
	{
		return a < b;
	}

	/////////////////////////////////
	//functions
	/////////////////////////////////
	IwBrep* NewBrep( double tol );
	IwBrep* CopyBrep( IwBrep* pBrep );
	IwBSplineCurve* CopyCurve( IwBSplineCurve* pCurve, bool bInvertCurve = false);
	IwBSplineSurface* CopySurface( IwBSplineSurface* pSurf );
	bool PierceBrep( IwBrep* pBrep, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, IwFace** pFace = NULL);
	bool PierceSurface( IwSurface* pSurface, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, double* uv );

	//////////////////////////////////////////////////////////////////////////////
	// If mutiple intersections occur, this function returns
	// the point that is closest to the input pt.
	/////////////////////////////////////////////////////////////////////////////
	bool IntersectBrepByLine( IwBrep*						pBrep,	// I:
								const IwPoint3d&			pt,		// I:
								const IwVector3d&			vec,	// I:
								IwPoint3d&					pnt,	// O:
								IwFace** face						// O:
							);

	//////////////////////////////////////////////////////////////////////////////
	// If mutiple intersections occur, this function returns
	// the point that is closest to the input pt.
	/////////////////////////////////////////////////////////////////////////////
	bool IntersectFaceByLine( 
			IwFace*						pFace, 
			const IwPoint3d&			pt, 
			const IwVector3d&			vec, 
			IwPoint3d&					pnt);

	//////////////////////////////////////////////////////////////////////////////
	// If mutiple intersections occur, this function returns
	// the point that is closest to the input pt.
	/////////////////////////////////////////////////////////////////////////////
	bool IntersectSurfaceByLine( IwSurface* pSurface, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, double* uv );

	/////////////////////////////////////////////////////////////////////////////
	// This function determines local intersection point.
	/////////////////////////////////////////////////////////////////////////////
	bool IntersectSurfaceByLine( IwSurface* pSurface,	// I:
								 IwPoint3d& pt,			// I:
								 IwVector3d& vec,		// I:
								 IwPoint2d& guessUV,		// I:
								 IwPoint3d& pnt,			// O:
								 IwVector3d&	normal,		// O:
								 IwPoint2d& intUV		// O:
								);

	/////////////////////////////////////////////////////////////////////////
	// If there exist multiple intersection points, the closest one 
	// will be returned.
	bool IntersectSurfacesByLine(	IwTArray<IwSurface*>		surfaces,	// I:
									IwPoint3d&					pt,			// I:
									IwVector3d&					vec,		// I:
									IwPoint3d&					pnt,		// O:
									int*						surfIndex,	// O:
									double*						uv			// O:
								);

		////////////////////////////////////////////////////////////////
	bool IntersectSurfaceByCurve(	const IwSurface*			pSurface,	// I:
									const IwCurve*				pCurve,		// I:
									IwTArray<IwPoint3d>&		intPnts,	// O: intersection points
									IwTArray<IwPoint2d>&		intUVParams,// O: surface intersection parameters
									IwTArray<double>&			intParams	// O: curve intersection parameters
								);

	bool GetBrepNormalAtPoint( IwBrep* pBrep, const IwPoint3d& pt, IwVector3d& nm, double tol, double* uv );

	IwVector3d GetSurfNormalAtPoint( IwSurface* pSurf, const IwPoint3d& pt );

	IwPoint3d DropPointOnSurface( IwSurface* pSurf, const IwPoint3d& pt, double* uv );

	IwPoint3d DropPointOnPlane(	const IwPoint3d&				planeOrigin, 
								const IwVector3d&				planeNormal,	
								const IwPoint3d&				pointToBeDropped);

	void CollectSmoothSurfaceFaces( IwFace* pStartFace, IwTArray< IwFace* >& Faces );

	void SetFacesId( IwBrep* pBrep, const char* filename );

	void SetEdgesId( IwBrep* pBrep );

	IwPoint2d GetSegmentIntersectionPoint(const IwPoint2d &P0, const IwPoint2d &P1, 
										  const IwPoint2d &Q0, const IwPoint2d &Q1);

	void CollectLaminaEdgeCurves( IwBrep* pBrep, IwTArray< IwCurve* >& rCurves );

	IwBrep* MakeBox(	const IwPoint3d&		center,
						const IwVector3d&		vx,
						const IwVector3d&		vy,
						double					l,
						double					w,
						double					h );

	IwBrep* MakeCylinder(	const IwPoint3d&		orig,
							const IwVector3d&		vx,
							const IwVector3d&		vy,
							double					r,
							double					h );

	IwBrep* MakeCylinder(	const IwPoint3d&		orig,
							IwVector3d				axis,
							double					r,
							double					h );

	IwBrep* Boolean_ByIteration(	IwBooleanOperationType	eOperation,
									IwBrep*					pBrep1, 
									IwBrep*					pBrep2,
									double					tol,
									double					angTol);

	IwBSplineCurve*	ComputeBestFitCurve(CPointArray& pts);

	IwBrep* Boolean(	IwBooleanOperationType	eOperation,
						IwBrep*					pBrep1, 
						IwBrep*					pBrep2,
						double					tol = 0.0,
						double					angTol = 0.0,
						bool					bCopyInputBreps = false);

	void FindSmoothAreaBorder( const CFaceArray& Faces, double angle, CEdgeArray& Edges );

	void FindFacesBorder( const CFaceArray& Faces, CEdgeArray& BorderEdges );

	int FindFirstCurve( CCurveArray& Curves, int ic, CCompVal eComp, IwAxis2Placement& trf );

	void SortSectionCurves( CCurveArray& Curves, int iStart );

	void SortCurvesChain( CCurveArray& Curves, int iStart = 0);

	int SortCurves( CCurveArray& Curves, CIntArray& nCurves );

	IwPoint3d GetCurveStartPoint( IwCurve* pCurve );

	IwPoint3d GetCurveEndPoint( IwCurve* pCurve );

	IwVector3d GetCurveStartTangent( IwCurve* pCurve, bool bNomalize );

	IwVector3d GetCurveEndTangent( IwCurve* pCurve, bool bNomalize );

	IwBSplineCurve* MakeLine(const IwPoint3d& p0, const IwPoint3d& p1);

	IwBSplineCurve* MakeArc(	const IwPoint3d&			cnt, 
								const IwPoint3d&			p0, 
								const IwPoint3d&			p1 );

	IwBSplineCurve* AddLine(	const IwPoint3d&			p0, 
								const IwPoint3d&			p1, 
								CCurveArray&			Curves );

	IwBSplineCurve* AddArc(		const IwPoint3d&			cnt, 
								const IwPoint3d&			p0, 
								const IwPoint3d&			p1, 
								CCurveArray&			Curves );

	IwBSplineCurve* AddLine(	const IwPoint3d&			p0, 
								const IwPoint3d&			p1, 
								CSplineArray&			Curves );

	IwBSplineCurve* AddArc(		const IwPoint3d&			cnt, 
								const IwPoint3d&			p0, 
								const IwPoint3d&			p1, 
								CSplineArray&			Curves );

	IwBrep* MakeLinearSweep(	CCurveArray&			Curves,
								const IwVector3d&		vec,
								double					height,
								bool					bMakeCaps );

	IwBrep* MakeRotationalSweep(	CCurveArray&			Curves,
									const IwPoint3d&		pt,			
									const IwVector3d&		vec,
									double					angle );

	long MakePlanarFace( IwBrep* pBrep, CCurveArray& curves, IwFace** pNewFace = NULL );

	bool IsCurvePlanar( IwCurve* pCurve, IwPoint3d& pt, IwVector3d& nm );

	void FindCommonEdges( const CFaceArray& Faces1, const CFaceArray& Faces2, CEdgeArray& CommonEdges );

	int GetFaceIndex( IwBrep* pBrep, IwFace* pFace );

	int GetEdgeIndex( IwBrep* pBrep, IwEdge* pEdge );

	IwBSplineCurve* GetEdgeCurve( IwEdge* pEdge );

	IwFace* GetFace( IwBrep* pBrep, int iFace );

	IwEdge* GetEdge( IwBrep* pBrep, int iEdge );

	IwBSplineCurve*	ApproximateWithBSpline( CCurveArray& Curves  );

	void ApproximateChainsWithSplines(	const CIntArray&		nc,
										const CCurveArray&		curves,
										CSplineArray&			splines );

	IwBSplineCurve* CrossSectBrep(	IwBrep*					pBrep,
									const IwPoint3d&		pt,
									const IwVector3d&		nm,
									int						ic, 
									CCompVal				eComp, 
									IwAxis2Placement&		trf );

	IwBSplineCurve* CrossSectBrep(	IwBrep*					pBrep,
									const IwPoint3d&		pt,
									const IwVector3d&		nm );

	void GetCrossSectSplines(IwBrep* pBrep, const IwPoint3d &pt, const IwVector3d &nm, CSplineArray &result);

	IwBSplineCurve* CutWithPlane(CCurveArray &curves, const IwPoint3d &pt, const IwVector3d &nm);

	IwBSplineCurve* MakeSplineFromCurves(CCurveArray curves, const IwPoint3d& pt);

	IwBSplineCurve* MakeBrepSection(IwBrep* pBrep, const IwPoint3d& pt, const IwVector3d& nm);

	int CrossSectSurface(IwBSplineSurface* pSurface, const IwPoint3d& pt, const IwVector3d& nm, CCurveArray& curves);

	double DropPointOnCurve( IwCurve* pCurve, const IwPoint3d& pt, IwPoint3d* pnt = NULL);

	void OrientCurve( IwCurve* pCurve, int icoord, const IwAxis2Placement& trf );

	void GetCurveEndPoints( IwCurve* pCurve, IwPoint3d& pt0, IwPoint3d& pt1 );

	void GetCurveEndParams(  IwCurve* pCurve, double& t0, double& t1 );

	void GetEdgeEndParams( IwEdge* pEdge, double& t0, double& t1 );

	void GetEdgeEndPoints( IwEdge* pEdge, IwPoint3d& p0, IwPoint3d& p1 );

	IwBSplineCurve* SplitAndOrientCurve(IwBSplineCurve* pCurve, int ic, CCompVal eComp, IwAxis2Placement& trf);

	int IntersectCurveByPlane(	IwCurve*				pCurve, 
								const IwPoint3d&		pt, 
								const IwVector3d&		nm,
								CPointArray&			pts,
								IwTArray<double>*		params = NULL);

	bool IntersectCurveByPlane(	IwCurve*				pCurve, 
								const IwPoint3d&		pnt, 
								const IwVector3d&		nrm,
								IwPoint3d&				pt,
								double*					t );

	int IntersectCurveByPlane2(	IwCurve*				pCurve, 
								const IwPoint3d&		pt,
								const IwVector3d&		nm,
								CPointArray&			pts );

	bool IntersectCurveByVector(
	    IwCurve*					pCurve,
		IwExtent1d					crvIntval,
		IwPoint3d					point,
		IwVector3d					vector,
		double*						guessParam,
		double						maxDist,
		IwPoint3d&					intersectPnt,
		double&						intersectParam,
		IwVector3d&					intersectTangent);
	
	bool IntersectCurvesByVector(
	    IwTArray<IwCurve*>			curves,
		IwPoint3d					point,
		IwVector3d					vector,
		IwPoint3d&					intersectPnt,
		double&						intersectParam);

	bool IntersectVectorByVector(
		IwPoint3d pnt0,
		IwVector3d vector0,
		IwPoint3d pnt1,
		IwVector3d vector1,
		IwPoint3d& intersectPnt);

	bool IntersectPolylinesByLine
	(
		IwTArray<IwPoint3d> polylines,	// I: define polylines
		IwTArray<IwPoint3d> line,		// I: two points define line
		IwVector3d* normalVector		// I: polylines and line plane normal
	);

	void ComputePointsOnCurve( IwCurve* pCurve, double t0, double t1, int np, IwTArray<IwPoint3d>& pts, bool bReset = false);

	void ComputePointsAndTangentsOnCurve( IwCurve* pCurve, double t0, double t1, int np, CPointArray& pts, CVectorArray& tns, bool bReset );

	void ComputePointsOnCurve( IwCurve* pCurve, int np, IwTArray<IwPoint3d>& pts, bool bReset );

	void ComputePointsAndTangentsOnCurve( IwCurve* pCurve, int np, CPointArray& pts, CVectorArray& tns, bool bReset );

	void ComputePointsOnCurve( IwEdge* pEdge, int np, IwTArray<IwPoint3d>& pts );

	void ComputePointsAndTangentsOnCurve( IwEdge* pEdge, int np, CPointArray& pts, CVectorArray& tns );

	void EvalCurvePoint( IwCurve* pCurve, double t, IwPoint3d& pt );

	void EvalCurveTangent( IwCurve* pCurve, double t, IwVector3d& tn, bool bNormalize );

	void EvalCurve( IwCurve* pCurve, double t, IwPoint3d& pt, IwVector3d& tn );

	IwVector3d EvalSurfacePoint( IwSurface* pSurf, double u, double v );

	IwVector3d EvalSurfacePoint( IwSurface* pSurf, double uv[2] );

	IwVector3d EvalSurfaceNormal( IwSurface* pSurf, double uv[2] );

	IwVector3d EvalSurfaceNormal( IwSurface* pSurf, double u, double v );

	void EvalSurface( IwSurface* pSurf, double uv[2], IwPoint3d& pt, IwVector3d& nm );

	void EvalSurface( IwSurface* pSurf, double u, double v, IwPoint3d& pt, IwVector3d& nm );

	void EvalSurfacePrincipalCurvature( IwSurface* pSurf, double uv[2], double& crv1, double& crv2 );

	void EvalSurfacePrincipalCurvature( IwSurface* pSurf, double u, double v, double& crv1, double& crv2 );

	bool FilletBrep( IwBrep* pBrep, CEdgeArray& Edges, double rad );

	void HighlightEdges( CEdgeArray& Edges, bool bHighlight );

	IwBSplineCurve*	ApproximatePoints( const CPointArray& pts, double tol );

	IwBSplineCurve*	InterpolatePoints( const CPointArray& pts, const IwVector3d& tn0, const IwVector3d& tn1);

	IwBSplineCurve*	InterpolatePoints( const CPointArray& pts, const CVectorArray& tns);

	IwBSplineCurve*	InterpolatePoints( const CPointArray& pts, const CDoubleArray& params);

	IwBSplineCurve*	InterpolatePoints( const CPointArray& pts, const CDoubleArray& params, IwVector3d& tn0, IwVector3d& tn1);

	IwBSplineCurve*	InterpolatePoints( const CPointArray& pts, const CVectorArray& tns, const CDoubleArray& knots );

	IwBSplineCurve*	MakeUniformSpline( const CPointArray& pts, IwVector3d tn0, IwVector3d tn1);

	IwBSplineCurve*	MakeUniformSpline( const CPointArray& pts);

	IwBSplineCurve*	MakeHermiteUniformSpline( const CPointArray& pts );

	IwBSplineSurface* MakeLoftSurface( 
		CSplineArray			Curves, 
		double					tol,
		IwBSplineCurve*			pRail1, 
		IwBSplineCurve*			pRail2, 
		CDoubleArray*			params,
		bool					bRemoveKnots );

	IwBrep* MakeLoftBrep( 
		CSplineArray			Curves, 
		double					tol,
		IwBSplineCurve*			pRail1, 
		IwBSplineCurve*			pRail2, 
		CDoubleArray*			params,
		bool					bRemoveKnots );

	IwBSplineSurface* MakeGordonSurface( CSplineArray& uCurves, CSplineArray& vCurves );

	IwBrep* MakeGordonBrep( CSplineArray& uCurves, CSplineArray& vCurves );

	IwBSplineSurface* MakeRuledSurface(IwBSplineCurve* pCurve0, IwBSplineCurve* pCurve1 );

	IwBrep* MakeRuledBrep( IwBSplineCurve* pCurve0, IwBSplineCurve* pCurve1 );

	IwBSplineSurface* ApproximateSurfaceByBSplineSurface( IwBSplineSurface* pSurf, int nu, int nv, bool bDelete = true);

	void CutBrepByPlane( IwBrep* pBrep, const IwPoint3d& pt, const IwVector3d& nm, bool bRemoveCut );

	////////////////////////////////////////////////////////////////////
	// This function does the boolean-cut the pBrep
	////////////////////////////////////////////////////////////////////
	IwBrep* CutBrepByPlane
	( 
		IwBrep*						pBrep,		// I:
		const IwPoint3d&			pt,			// I:
		const IwVector3d&			nm,			// I: direction to be trimmed
		double						*refSize	// I:
	);

	IwPlane* MakePlaneSurf(const IwPoint3d& pt, const IwVector3d& vec0, const IwVector3d& vec1, double w, double h);

	IwBrep* MakePlane( 
		const IwPoint3d&		pt,
		const IwVector3d&		vx,
		const IwVector3d&		vy,
		double					w,
		double					h );

	bool WriteBrep( IwBrep* pBrep, const char* filename );

	bool WriteBrep( IwBrep* pBrep, const QString& sFileName );

	IwBrep* ReadBrep( const char* filename );

	IwBrep* ReadBrep( const QString& sFileName );

	bool WriteSurfaces( CSurfArray& surfaces, const char* filename );

	bool WriteSurfaces( CSurfArray& surfaces, const QString& sFileName );

	bool ReadSurfaces( const char* filename, CSurfArray& surfaces );

	bool ReadSurfaces( const QString& sFileName, CSurfArray& surfaces );

	bool WriteCurves( CCurveArray& curves, const char* filename );

	bool WriteCurves( CCurveArray& curves, const QString& sFileName );

	bool ReadCurves( const char* filename, CCurveArray& curves );

	bool ReadCurves( const QString& sFileName, CCurveArray& curves );

	bool WriteIwp( IwBrep* pBrep, CCurveArray& curves, CSurfArray& surfaces, const char* filename );

	bool WriteIwp( IwBrep* pBrep, CCurveArray& curves, CSurfArray& surfaces, const QString& sFileName );

	bool ReadIwp( IwBrep** ppBrep, CCurveArray& curves, CSurfArray& surfaces, const char* filename );

	bool ReadIwp( IwBrep** ppBrep, CCurveArray& curves, CSurfArray& surfaces, const QString& sFileName );

	void RenameIwbToIwp( const char* filename );

	IwPoint3d ComputeArcCenter( const IwPoint3d& pt0, const IwPoint3d& pt1, const IwPoint3d& pt2 );

	/////////////////////////////////////////////////////////////////////////////////////
	void WriteIges( const char* filename, IwBrep* pBrepTrimmedSurf, IwBrep* pBrepSolid );

	void WriteIges(	
		const char*				filename, 
		CPointArray*			pPointArray, 
		CCurveArray*			pCurveArray, 
		CSurfArray*				pSurfArray, 
		CBrepArray*				pBrepTrimmedSurfArray,
		CBrepArray*				pBrepSolidArray);

	IwFace* GetBrepFace( IwBrep* pBrep, int iUserIndex2 );

	void CloseBrepHoles( IwBrep* pBrep );

	bool IsConvexEdge( IwEdge* pEdge );

	bool IsConcaveEdge( IwEdge* pEdge );

	CEdgeConvexity GetEdgeConvexity( IwEdge* pEdge );

	bool SewBrep( IwBrep* pBrep, double tol );

	IwBSplineCurve* JoinCurves( IwTArray<IwCurve*>& Curves, bool bDelete );

	IwBSplineSurface* JoinSurfaces(IwBSplineSurface* Surface1, IwBSplineSurface* Surface2);

	IwFace*	MakeSimpleFaceFromCurves( IwBrep* pBrep, IwSurface* pSurface, const CCurveArray& Curves );

	double ApproxEdgeLength( IwEdge* pEdge );

	void OrientCurve( IwCurve* pCurve, CCurveEnd eStartEnd, const IwPoint3d& pt );

	void TrimCurve( IwCurve* pCurve, double t0, double t1 );

	void TrimCurve( IwCurve* pCurve, const IwPoint3d& pt0, const IwPoint3d& pt1 );

	bool FilletEdges( IwBrep* pBrep, CEdgeArray& edges, CDoubleArray& rads );

	bool FilletEdges( IwBrep* pBrep, CEdgeArray& edges, double rad );

	double MarchAlongCurve( IwCurve* pCurve, double t0, double dt, double d_needed );

	double ComputeCircleByThreePoints(
		const IwPoint3d&			p0,
		const IwPoint3d&			p1,
		const IwPoint3d&			p2,
		IwPoint3d&				cnt );

	void InvertCurve( IwCurve* pCurve );

	double FindExtremePointOnCurve( 
		IwCurve*				pCurve, 
		int						ic, 
		CCompVal				eComp, 
		IwAxis2Placement&		trf, 
		IwPoint3d*				pnt );

	void GetFaceBounds( IwFace* pFace, IwPoint3d& pt0, IwPoint3d& pt1 );

	int FindCurvePointsParallelToVector( 
		IwCurve*				pCurve, 
		double					t0,
		double					t1,
		const IwVector3d&		vec,
		CPointArray&			pts );

	int FindCurvePointsPerpendicularToVector( 
		IwCurve*				pCurve, 
		double					t0,
		double					t1,
		const IwVector3d&		vec,
		CPointArray&			pts );

	bool IntersectCurves( 
		IwCurve*				pCurve0, 
		IwCurve*				pCurve1, 
		const IwPoint3d&		pt, 
		IwPoint3d&				pnt,
		double&					t0,
		double&					t1 );

	bool IntersectCurves( 
		IwCurve*				pCurve0, 
		IwCurve*				pCurve1, 
		IwExtent1d*				interval0,
		IwExtent1d*				interval1,
		const IwPoint3d&		pt, 
		IwPoint3d&				pnt,
		double&					t0,
		double&					t1 );

	IwBSplineSurface* GetFaceSurface( IwFace* pFace );

	IwBrep* MakeBrepFromSurface( double tol, IwSurface* pSurface );

	double DistFromEdgeToSurface( IwEdge* pEdge, IwSurface* pSurf );

	double DistFromCurveToSurface( IwBSplineCurve* pCurve, IwExtent1d* paramInterval, IwSurface* pSurf);

	double DistFromCurveToSurface( IwBSplineCurve* pCurve, IwSurface* pSurf );

	double DistFromCurveToSurface
	(
		IwBSplineCurve*				pCurve, 
		IwExtent1d					searchParamInterval, 
		IwSurface*					pSurf,
		IwVector2d*					guessSParam,
		IwPoint3d*					closestPointOnCurve,
		IwPoint3d*					closestPointOnSurface
	);

	/////////////////////////////////////////////////////////
	// This function determines the global minimum distance.
	double DistFromPointToBrep
	(
			IwPoint3d					point,			// I:
			IwBrep*						brep,			// I:
			IwPoint3d					&closestPoint	// O:
	);


	/////////////////////////////////////////////////////////
	// This function determines the local minimum distance.
	double DistFromPointToBrepLocal
	(
			IwPoint3d					point,			// I:
			IwBrep*						brep,			// I:
			IwPoint3d					initGuess,		// I: guessing point on brep
			IwPoint3d					&closestPoint	// O:
	);

	/////////////////////////////////////////////////////////
	// This function determines the global minimum distance.
	double DistFromPlaneToBrep
	(
			IwPoint3d					point,
			IwVector3d					normal,
			IwVector3d					*xAxis,
			IwExtent2d					*domain,
			IwBrep*						brep,
			IwPoint3d					&closestPoint
	);

	double DistFromPointToSurface(
			IwPoint3d					point,
			IwBSplineSurface*			surface,
			IwPoint3d					&closestPoint,
			IwPoint2d					&param);

	double DistFromPointToSurface(
			IwPoint3d					point,
			IwBSplineSurface*			surface,
			IwPoint2d					guessParam,
			IwPoint3d					&closestPoint,
			IwPoint2d					&param);

	double DistFromPointToEdge(
			IwPoint3d					point,
			IwEdge*						edge,
			IwPoint3d					&closestPoint);

	double DistFromPointToCurve
	(
			IwPoint3d					point,				  // I:
			IwBSplineCurve*				curve,				  // I:
			IwPoint3d					&closestPoint,		  // O:
			double						&param,				  // O:
			IwExtent1d*					searchInterval = NULL // I:
	);

	double DistFromPointToCurve
	(
			IwPoint3d					point,			// I:
			IwBSplineCurve*				curve,			// I:
			double						guessParam,		// I:
			IwPoint3d					&closestPoint,	// O:
			double						&param			// O:
	);

	double DistFromCurveToCurve
	(
			IwBSplineCurve*				firstCurve,			// I:
			IwBSplineCurve*				secondCurve,		// I:
			IwPoint3d					&firstClosestPoint,	// O:
			double						&firstParam,		// O:
			IwPoint3d					&secondClosestPoint,// O:
			double						&secondParam		// O:
	);

	bool DistFromCurveToCurve(
	    IwBSplineCurve*				firstCurve,			// I:
		IwBSplineCurve*				secondCurve,		// I:
		IwTArray<double>			&distances,			// O:
		IwTArray<IwPoint3d>			&firstClosestPoints,// O:
		IwTArray<double>			&firstParams,		// O:
		IwTArray<IwPoint3d>			&secondClosestPoints,// O:
		IwTArray<double>			&secondParams,		// O:
		double						*targetDist		// I:
		);

	double DistFromCurveToCurve
	(
		IwBSplineCurve*				firstCurve,
		IwExtent1d&					firstGuessInterval,
		IwBSplineCurve*				secondCurve,
		IwExtent1d&					secondGuessInterval,
		IwPoint3d					&firstClosestPoint,	// O:
		double						&firstParam,		// O:
		IwPoint3d					&secondClosestPoint,// O:
		double						&secondParam		// O:
	);

	double DistFromFaceToFace
	(
		IwFace* face0,		// I:
		IwFace* face1,		// I:
		IwPoint3d& pnt0,	// O: 
		IwPoint3d& pnt1		// O:
	);

	double DistFromShapeToShape
	(
		IwBrep* brep0,		// I:
		IwBrep* brep1,		// I:
		IwFace* face0,		// I:
		IwFace* face1,		// I:
		IwEdge* edge0,		// I:
		IwEdge* edge1,		// I:
		IwPoint3d& pnt0,	// O: 
		IwPoint3d& pnt1		// O:
	);


	double DistFromShapeToCurve
	(
		IwBrep* brep0,		// I:
		IwFace* face0,		// I:
		IwEdge* edge0,		// I:
		IwCurve* curve,		// I:
		IwExtent1d intv,	// I:
		IwPoint3d& pnt0,	// O: 
		IwPoint3d& pnt1		// O:
	);

	double DistFromPointToIwTArray(
			IwPoint3d					point,			// I:
			IwTArray<IwPoint3d>			points,			// I:
			IwVector3d*					normal,			// I: distance measured when viewing along this vector
			IwPoint3d					&closestPoint,	// O:
			int*						index);			// O:

	IwBSplineCurve*	ExtendCurve( 
		IwBSplineCurve*			pSpline, 
		int						iside,
		double					d );

	void SetColorAttr( IwAObject* pObject, const IwVector3d& color );

	void SetLabelAttr( IwAObject* pObject, const QString& sLabel );

	void SetNameAttr( IwAObject* pObject, const QString& sName );

	void MarkFaceEdges( IwFace* pFace, int flag );

	IwBSplineCurve* ApproximateCurve( 
		IwBSplineCurve*			pCurve, 
		double					tol, 
		bool					bDelete );

	IwBSplineCurve* ApproximateCurves( 
	    IwContext&					iwContext,
		IwTArray<IwBSplineCurve*>	curves, 
		double						tol);

	IwBSplineCurve* ProjectCurveOnPlane( 
		IwBSplineCurve*			pCurve, 
		const IwPoint3d&		pt,
		const IwVector3d&		nm,
		const IwVector3d&		vec,
		bool					bDelete );

	IwBSplineSurface* GetBrepSurface( IwBrep* pBrep );

	void CopyFaces( IwBrep* pBrepFrom, IwBrep* pBrepTo );

	void SplitBrepFaces( 
		IwBrep*					pBrep, 
		CFaceArray&				Faces, 
		const IwPoint3d&		pt,
		const IwVector3d&		nm );

	void SplitBrepFace(
		IwBrep*					pBrep, 
		IwFace*					pFace, 
		const IwPoint3d&		pt,
		const IwVector3d&		nm );

	bool IntersectSurfaces(
		IwBSplineSurface*		pSurf1,				// I:
		IwBSplineSurface*		pSurf2,				// I:
		CCurveArray&			Curves,				// O:
		double					tol,				// I:
		double					angtol,				// I: radian
		IwTArray<IwCurve*>		*surface1UVCurves,	// O:
		IwTArray<IwCurve*>		*surface2UVCurves	// O:
	);

	bool IntersectSurfaceByPlane
	(
		IwBSplineSurface*		pSurf,					// I:
		IwPoint3d				pnt,					// I:
		IwVector3d				normal,					// I:
		CCurveArray&			Curves,					// O:
		double					tol = 0.001,			// I:
		double					angtol = 0.01,			// I: radian
		IwTArray<IwCurve*>		*surfaceUVCurves = NULL // O:
	);

	double ComputePointOnCurveAtDist(
		IwBSplineCurve*			pCurve,
		double					tStart,
		double					dist,
		IwPoint3d*				pt );

	void ComputeEquallySpacedPoints(
		IwBSplineCurve*			pCurve,
		double					t0,	
		double					t1,
		int						np,
		IwTArray<IwPoint3d>&	pts,
		IwTArray<double>*		params );

	IwFace* GetEdgeOtherFace( IwEdge* pEdge, IwFace* pFace );

	// This function only handle 4 sides of face
	IwEdge* GetFaceOtherEdge( IwFace* pFace, IwEdge*pEdge );

	// This function only handle 4 sides of face
	// The posEdge and negEdge are connected to the pEdge
	void GetFaceConnectedEdges( 
		IwFace*						pFace,		// I:
		IwEdge*						pEdge,		// I:
		IwEdge*&					posEdge,	// O:
		IwEdge*&					negEdge		// O:
	);

	void TraceAdjacentFaces
	(
		IwFace*						pFace,		// I:
		IwEdge*						pEdge,		// I:
		int							traceSteps,	// I:
		IwTArray<IwFace*>&			adjFaces	// O:
	);

	void GetFaceNeighbors( IwFace* pFace, CFaceArray& NeighborFaces );

	IwEdge* GetFacesCommonEdge( IwFace* pFace0, IwFace* pFace1 );

	int FindCurveInflectionPoints( 
		IwCurve*				pCurve, 
		double					t0,
		double					t1,
		CDoubleArray&			params,
		CPointArray&			pts );

	void ComputeBestCubicSeg(
		IwPoint3d&				pt0,
		IwPoint3d&				pt1,
		IwVector3d&				tn0,
		IwVector3d&				tn1 );

	IwPoint3d DropPointOnBrep( IwBrep* pBrep, IwPoint3d& pt, double* uv, IwFace*& pFace );

	IwPoint3d DropPointOnBrep( IwBrep* pBrep, IwPoint3d& pt );

	void FindPointOnRayAtHeightAboveFemur(
		IwBrep*					pBrep,
		IwPoint3d				p0,
		IwVector3d				vec,
		double					height,
		IwPoint3d&				pt );

	bool ThickenSurface
	(
		IwBSplineSurface*			baseSurface,
		double						thickenDist,
		IwBrep*&					rBrep
	);

	///////////////////////////////////////////////////////////////////
	IwBSplineCurve* CreateArc
	(
		IwContext&					iwContext,	// I:
		IwPoint3d&					startPoint, // I:
		IwVector3d&					startVector,// I:
		IwPoint3d&					endPoint,	// I:
		IwPoint3d&					arcCenter,	// O:
		double&						radius		// O:
	);

	///////////////////////////////////////////////////////////////////////
	// startVector & upVector define the arc plane
	IwBSplineCurve* CreateArc(
			IwContext&					iwContext,		// I:
			IwPoint3d&					startPoint,		// I:
			IwVector3d&					startVector,	// I:
			IwVector3d&					upVector,		// I:
			double						radius,			// I:
			double						angleDegrees	// I:
	);

	IwBSplineCurve* CreateArc3(
			IwContext&					iwContext,	// I:
			IwPoint3d&					startPoint, // I:
			IwPoint3d&					midPoint,	// I:
			IwPoint3d&					endPoint,	// I:
			IwPoint3d&					arcCenter,	// O:
			double&						radius		// O:
	);

	void SortIwTArray(
		IwTArray<double>& iwArray,		// I/O:
		bool ascending);				// I:

	void SortIwTArray
	(
		IwTArray<IwPoint3d>& iwArray,	// I/O:
		int elementToSort,				// I: 0/1/2 element to sort
		bool ascending					// I:
	);

	void ChainLoopConvexOffset
	(
		IwContext& iwContext,					// I:
		IwTArray<IwBSplineCurve*> chainLoop,	// I:
		IwVector3d offsetNormal,				// I:
		double offsetDistance,					// I:
		IwTArray<IwBSplineCurve*>& offsetCurves	// O:
	);

	//////////////////////////////////////////////////////////////////
	// This function assumes the chain loop is pure convex. The curves
	// restrictedly follow the order (start-end-start-end). The new 
	// intersection points are close to the original ones. Only one 
	// intersection point exists between two curves.
	//////////////////////////////////////////////////////////////////
	void ChainLoopConvexCleanUp
	(
		IwTArray<IwBSplineCurve*>& chainLoop // I/O:
	);

	bool IsPointInsideTriangle(IwPoint3d point, IwPoint3d t0, IwPoint3d t1, IwPoint3d t2);
	IwPoint3d IntersectRayWithTriangle(IwPoint3d rayAnchor, IwVector3d rayDir, IwPoint3d A, IwPoint3d B, IwPoint3d C);

	IwBSplineCurve* OffsetCurveAlongSurfaceNormal
	(
		IwContext& iwContext,	// I:
		IwBSplineSurface* surf, // I:
		IwBSplineCurve* uvCrv,	// I: UV curve on surface
		double dOffset,			// I:
		int intervalSize,		// I: if == 0 , using chord tol
		double decay			// I: [0~1] percentage at both ends to gradually reduce dOffset value
	);

	//////////////////////////////////////////////////
	// The return curve is in XYZ space
	IwBSplineCurve* OffsetCurveAlongSurfaceAndNormal
	(
		IwContext& iwContext,	// I:
		IwBSplineSurface* surf, // I:
		IwBSplineCurve* uvCrv,	// I: UV curve on surface
		double sideOffsetStart,	// I: side distance in UV domain at start 
		double sideOffsetEnd,	// I: side distance in UV domain at end 
		double normalOffset,	// I: normal distance
		int intervalSize		// I: if == 0 , using chord tol
	);

	bool IntersectIwTArrayPointsByPlane( 
		IwTArray<IwPoint3d>			pnts, 
		IwPoint3d&					pt, 
		IwVector3d&					nm,
		double						distTol,
		IwPoint3d&					intPnt,		// O:
		int *index);							// O:
}