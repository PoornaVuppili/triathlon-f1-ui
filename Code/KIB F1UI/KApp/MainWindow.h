#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
namespace Ui {class MainWindowClass;};

class Module;
class Implant;
class QToolButton;
class QProgressBar;
class QDockWidget;
class QSplitter;
class QUndoStack;
class QActionGroup;
class QTextEditEx;

#include "..\KUtility\KLockFile.h"

class TEST_EXPORT MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(bool reviewmode, QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~MainWindow();

    // Update the undo stack
    void UpdateUndoStack(QUndoStack* stack);

    // Set the editing control with either a widget or toolbar
    void SetEditControl(QWidget* w, bool enable);
    void SetEditControl(QToolBar* b, bool enable);

    // Get the implant
    Implant* GetImplant() { return implant; }

    // Get the application name & version
    virtual QString GetAppName()    const = 0;
    virtual QString GetAppVersion() const = 0;

	QDockWidget* GetDockWidget() { return dockWidget; }

    // Close an implant immediately & exit application
	void CloseImplantImmediately(QString& msg);

	// Cancel the measurement tool
	void CancelMeasure();

   bool IsReviewMode() const { return appReviewMode || caseReviewMode; }
   void SetReviewMode(bool rm);

   void SetSubTitle(QString const &middle);
   void RefreshTitle();

   void ContextHelp(QString const& helpContent);

   bool ReviewerFeedbackAvailable() const;
   void ShowReviewerFeedback() { ReviewerFeedback(); }

protected:
    // Set up the menu bar
    void SetupMenuBar();

    // - New, open, close implant
    void AddFileMenu();
    // - Undo, redo
    void AddEditMenu();
    // - View
    void AddViewMenu();
    // - Tool
    void AddToolMenu();
    // - Help 
    void AddHelpMenu();

    // Set up the tool bar
    void SetupToolBar();

    // Set up the central widget
    void SetupCentralWidget();

    // Set up the status bar
    void SetupStatusBar();

    // Set up the dock widgets 
    void SetupDockWidget();

    virtual void closeEvent ( QCloseEvent * event );

    // Put the application in the edit mode
    void SetEditMode(bool edit = false);

    // Enable/disable the actions depend on if there is an open implant and edit status
    void UpdateActions(bool hasImplant, bool edit=false);

protected slots:
    // Display the program information
    void About();

    // Display the context-sensitive modeless window
    void ContextHelp();

    // Display the reviewer comments, if available
    void ReviewerFeedback();

    // Called when the dock location is changed
    void OnDockLocationChanged(Qt::DockWidgetArea area);

    // Open an existing implant
    virtual void OpenImplant() {}

    // Save an implant
    void SaveImplant();

    // Close an implant
    void CloseImplant(bool confirm=true);

    // Capture the screen
    void CaptureScreen();

    // Undo/Redo
    void Undo();
    void Redo();

    // Viewport control
    void ControlViewport();

    // Set options
    virtual void SetOption();

    // Trigger the measure action
	void TriggerMeasure(QAction*);

protected:
	virtual void customEvent(QEvent *evt);

protected:
    Ui::MainWindowClass* ui;

    // undo stack
    QUndoStack* undoStack;

    // menu bar
    QMenu *menuFile,*menuHelp;
    QAction *actOpenImplant, *actSaveImplant, *actCloseImplant, *actScreenCapture, *actReviewerFeedback;

    QMenu *menuEdit;
    QAction *actUndo,*actRedo;

    QMenu *menuView;
    QAction *actViewport,*actZoomIn,*actZoomOut,*actZoomFit,*actRefresh;

    QMenu *menuTool, *menuMeasure;
    QActionGroup* measureGroup;
    QAction *actOption,*actMeasureEntityDistance,*actMeasureEntityAngle,*actMeasureEntityRadius;
    QAction *actMeasureScreenDistance, *actMeasureScreenAngle, *actMeasureScreenRadius;

    // tool bar
    QToolBar *wndToolbar;
    QToolBar* editToolbar;  // toolbar used for editing manager
    QToolButton* toolMeasure;

    // dock widget
    QDockWidget* dockWidget;
    QSplitter *splitter;

    // status bar
    QProgressBar* progress;

    QString contextHelpContent;

    // single file lock: used to warn when opening a case that's already opened by someone else
    KLockSingleFile lock;

    // Implant
    Implant* implant;

    QString  subTitle; // what goes in the middle
    // review mode indicator
    bool     appReviewMode;
    bool     caseReviewMode;
};

// global instance
extern MainWindow* mainWindow;

#endif // MAINWINDOW_H
