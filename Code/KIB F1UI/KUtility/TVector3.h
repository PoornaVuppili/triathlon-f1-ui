#ifndef TVECTOR3_H
#define TVECTOR3_H

#include "TPoint3.h"

#define TVector3 TPoint3
//switch to template alias in VS2013
//template<typename T> using TVector3 = TPoint3<T>;

typedef Point3Int    Vector3Int;
typedef Point3Float  Vector3Float;
typedef Point3Double Vector3Double;

#endif //TVECTOR3_H
