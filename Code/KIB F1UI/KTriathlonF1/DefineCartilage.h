#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Entity.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

struct CDefineCartilageUndoData
{
	IwTArray<IwPoint3d>			troGroovePoints;
	IwTArray<ULONG>				ctrlPntIndexes;
	IwTArray<IwPoint3d>			ctrlPnts;
};

///////////////////////////////////////////////////////////////////////
// There are significant changes in iTW6. There exists cartilage boundary
// to define the cartilage region near weight-bearing area. In iTW6,
// there is no such cartilage boundary. The whole femur surface will
// be offset 3mm, plus the extra offset near the trochlear groove. 
///////////////////////////////////////////////////////////////////////

class TEST_EXPORT_TW CDefineCartilage : public CManager
{
    Q_OBJECT

public:
					CDefineCartilage( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CDefineCartilage();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

	virtual void	Display(Viewport* vp=NULL);
	void			DisplayTrochlearControlPoints(IwVector3d offset);
    virtual bool	MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp  ( const QPoint& cursor, Viewport* vp=NULL );

	void			ConstructCartilageSurface();
	bool			DetermineTrochlearGroove(IwTArray<IwPoint3d> &troPoints, IwTArray<IwPoint3d> &troRefPoints);
	void			GetDefineCartilageUndoData(CDefineCartilageUndoData& undoData, IwTArray<ULONG>* ctrlPntIndexes=NULL, IwTArray<IwPoint3d>* ctrlPnts=NULL);
	void			SetDefineCartilageUndoData(CDefineCartilageUndoData& undoData);
	void			SetCartilageSurfaceUpToDate(bool flag);
	static void		SmoothARegion(CTotalDoc*& pDoc, IwTArray<QPoint>& screenPoints, IwVector3d& viewingVector, IwBSplineSurface*& mainSurf, IwTArray<ULONG>& ctrlPntIndexes, IwTArray<IwPoint3d>& prevCtrlPnts, IwTArray<IwPoint3d>& postCtrlPnts);
	static CEntValidateStatus Validate(CTotalDoc* pDoc, QString &message);
	static bool		ValidateTrochlearCurveDeviation(CTotalDoc* pDoc, double& maxDeviation, bool &pastAntRefCurve);

private:
	void			Reset(bool activateUI=true, bool constructSurf=true);
	IwBSplineSurface*	CreateTrochlearGrooveSurface(IwBSplineCurve*& troGroUVCurve);	
	void			DisplaySmoothRegionLoop(IwVector3d offset=IwVector3d(0,0,0));
	void			DetermineTroGrooveAnteriorReferencePoints(IwTArray<IwPoint3d> troUVPoints);
	void			DisplayTrochlearGrooveReferencePoints(IwVector3d offset=IwVector3d(0,0,0));
	void			SetValidateIcon(CEntValidateStatus vStatus, QString message);

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnUpdate();
	void			OnCancel();
	void			OnSmooth();
	void			OnValidate();
	virtual bool	OnReviewBack();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actDefineCartilage;
	QAction*					m_actReset;
	QAction*					m_actUpdate;
	QAction*					m_actSmooth;
	QAction*					m_actValidate;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewRework;

	IwBSplineSurface*			m_femurMainSurfRef;	// for easy referencing
	IwBSplineSurface*			m_femurLeftSurfRef;	// for easy referencing
	IwBSplineSurface*			m_femurRightSurfRef;	// for easy referencing

	bool						m_cartilageSurfaceUpToDate;
	bool						m_leftButtonDown;
	int							m_addDeleteMode;			// 0: none, 1: add, 2: delete, 3: smooth
	int							m_predefinedViewIndex;

	IwTArray<QPoint>			m_smoothRegionLoop;
	IwTArray<IwPoint3d>			m_smoothRegionLoop3d;		// for display purpose only

	IwTArray<IwPoint3d>			m_currTroGroovePoints;// (x,y,z)=(u,v,0)
	IwTArray<IwPoint3d>			m_oldTroGroovePoints;// (x,y,z)=(u,v,0)
	IwTArray<IwPoint3d>			m_currTroGrooveXYZPoints;

	IwTArray<IwPoint3d>			m_troGrooveRefPoints;// For validation purpose;
	IwTArray<IwPoint3d>			m_troGrooveAntRefPoints;// To indicate the F3 anterior profile (21mm from distal cut);

	// Run time data to speed up performance
	double						m_troCurveMaxDeviation;
	//// For Validation ///////////////////////////////////////////////////////////////
	CEntValidateStatus			m_validateStatus;
	QString						m_validateMessage;
};
