#ifndef TPICKRAY_H
#define TPICKRAY_H

// A ray for picking objects in the viewports
#include "TLine3.h"

template <typename T>
class TPickRay
{
protected:
    TPoint3<T>  rayCen; // ray center
    TVector3<T> rayDir; // ray direction

public:
    TPickRay() : rayCen(TPoint3<T>(0,0,0)), rayDir(TVector3<T>(0,0,-1)) {}
    TPickRay(TPoint3<T>& cen, TVector3<T>& dir) : rayCen(cen), rayDir(dir) {}
    TPickRay(const TPickRay& ray) : rayCen(ray.rayCen), rayDir(ray.rayDir) {}

    // Get values
    inline TPoint3<T> GetRayCen() const { return rayCen; }
    inline TVector3<T> GetRayDir() const { return rayDir; }

    // Set values
    inline void SetRayCen(TPoint3<T>& p) { rayCen = p; }
    inline void SetRayDir(TVector3<T>& v) { rayDir = v; }

    // assignment
    TPickRay& operator= (const TPickRay& ray)
    {
        if( this != &ray )
        {
            rayCen = ray.GetRayCen();
            rayDir = ray.GetRayDir();
        }
        return *this;
    }

    TLine3<T> GetRayLine() const { return TLine3<T>(rayCen, rayCen+rayDir); }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TPickRay<int> PickRayInt;
typedef TPickRay<float> PickRayFloat;
typedef TPickRay<double> PickRayDouble;


#endif //TPICKRAY_H
