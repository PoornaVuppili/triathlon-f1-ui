#pragma once

#include <QtGui>
#pragma warning( disable : 4996 4805 )
#include <hwtranslatorgeneric.h>
#pragma warning( default : 4996 4805 )

class CPart;
class CProgressWidget;
class IwBrep;

class CIgesReader
	{
public:
	CIgesReader( CPart* pPart, const QString& sFileName );
	~CIgesReader();

	bool				LoadFile();
	bool				LoadFemurAndTabcyl( IwBrep*& pTabcylBrep );
	IwBrep*				GetTabcylBrep();

private:
	bool				ReadIgesFile( const char* filename  );
	bool				ReadFemurAndTabcyl( const char* filename, IwBrep*& pTabcylBrep );
	void				SewFaces();
	bool				MakeTessellation();

	CPart*				m_pPart;
	QString				m_sFileName;
	IwBrep*				m_pBrep;
	};
