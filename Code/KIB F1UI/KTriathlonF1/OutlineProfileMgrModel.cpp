#include "DefineOutlineProfile.h"
#include "OutlineProfileMgrModel.h"
#include "TotalDoc.h"
#include "OutlineProfile.h"
#include "FemoralCuts.h"
#include "FemoralAxes.h"
#include "FemoralPart.h"
//#include "JCurves.h"
#include "AdvancedControl.h"
#include "ValidateOutlineProfile.h"
//#include "SolidImplant.h"
//#include "AnalyzeImplant.h"
#include "..\KAppTotal\Utilities.h"
#include "IFemurImplant.h"

#include <QApplication>

using namespace Validation::OutlineProfile;

OutlineProfileMgrModel::OutlineProfileMgrModel(CTotalDoc *doc)
	: m_pDoc(doc)
{
	// Get cut feature points to speed up computation
	CFemoralCuts*		femoralCuts = m_pDoc->GetFemoralCuts();
	if ( femoralCuts )
	{
		femoralCuts->GetFeaturePoints(m_cutFeatPnts, &m_antOuterPnts);
		femoralCuts->DetermineEdgeFeaturePoints(&m_cutFeatPnts, m_edgeMiddlePoints);
	}
}

OutlineProfileMgrModel::~OutlineProfileMgrModel()
{
}

void OutlineProfileMgrModel::InitializeFeaturePoints()
{
	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();
	if(	outlineProfile != NULL )
	{
		outlineProfile->GetOutlineProfileFeaturePoints(m_oldFeaturePoints, m_oldFeaturePointsInfo, m_oldFeaturePointsRemap);
		outlineProfile->GetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
	}
}

bool OutlineProfileMgrModel::IsMemberDataChanged()
{
	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();
	if(	outlineProfile != NULL )
		return outlineProfile->IsMemberDataChanged(m_oldFeaturePoints);
	return false;
}

void OutlineProfileMgrModel::GetOutlineProfileFeaturePoints()
{
	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();
	if(	outlineProfile != NULL )
		outlineProfile->GetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
}

void OutlineProfileMgrModel::SetOutlineProfileFeaturePoints()
{
	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();
	if(	outlineProfile != NULL )
		outlineProfile->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
}

///////////////////////////////////////////////////////////////////////
// Re-arrange the input cutFeatPnts, antOuterPnts, and edgeMiddlePoints into 
// UVFeatPnts, UVFeatPntsInfo, and UVFeatPntsRemap.
// This function initializes UVFeatPnts, UVFeatPntsInfo, and UVFeatPntsRemap
// based on cutFeatPnts, antOuterPnts, and edgeMiddlePoints.
// PLEASE NOTE - these data are all based on femoral cut geometric info.
// Once they are determined in outline profile managers, they do not need 
// to be re-calculated again (femoral cut won't change during profile editing).
///////////////////////////////////////////////////////////////////////
bool OutlineProfileMgrModel::ConvertCutFeatPntsToOutlineFeatPnts
(
	IwTArray<IwPoint3d>& cutFeatPnts,			// I:
	IwTArray<IwPoint3d>& antOuterPnts,			// I:
	IwTArray<IwPoint3d>* edgeMiddlePoints,		// I:
	IwTArray<IwPoint3d>& UVFeatPnts,			// O:
	IwTArray<int>& UVFeatPntsInfo,				// O:
	IwTArray<IwPoint3d>& UVFeatPntsRemap		// O:
)
{
	UVFeatPnts.RemoveAll();
	UVFeatPntsInfo.RemoveAll();
	UVFeatPntsRemap.RemoveAll();

	// PLEASE NOTE - these data are all based on femoral cut geometric info.
	// Once they are determined in outline profile managers, they do not need 
	// to be re-calculated again (femoral cut won't change during profile editing).
	if ( m_idealUVFeatPnts.GetSize() > 0 && m_idealUVFeatPntsInfo.GetSize() > 0 && m_idealUVFeatPntsRemap.GetSize() > 0 )
	{
		UVFeatPnts.Append(m_idealUVFeatPnts);
		UVFeatPntsInfo.Append(m_idealUVFeatPntsInfo);
		UVFeatPntsRemap.Append(m_idealUVFeatPntsRemap);
		return true;
	}

	// Determine UVFeatPnts, UVFeatPntsInfo, and UVFeatPntsRemap from scratch.
	double cutToEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");
	IwAxis2Placement wslAxes;
	m_pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
	IwVector3d xAxis = wslAxes.GetXAxis();

	IwPoint3d pnt;
	IwTArray<IwPoint3d> posOuterPnts;
	IwTArray<IwPoint3d> posOuterPntsRemap;
	IwTArray<IwPoint3d> posInnerPnts;
	IwTArray<IwPoint3d> posInnerPntsRemap;
	IwTArray<IwPoint3d> negOuterPnts;
	IwTArray<IwPoint3d> negOuterPntsRemap;
	IwTArray<IwPoint3d> negInnerPnts;
	IwTArray<IwPoint3d> negInnerPntsRemap;

	IwTArray<IwPoint3d> anteriorOuterPnts;
	IwPoint3d posPostTipPnt;
	IwPoint3d negPostTipPnt;
	IwTArray<IwPoint3d> inputAntOuterPnts;

	posPostTipPnt = cutFeatPnts.GetAt(0);
	negPostTipPnt = cutFeatPnts.GetAt(1);

	// Note the sequence
	// positive side outer points
	double factor=0.0;
	pnt = cutFeatPnts.GetAt(18) + factor*cutToEdge*xAxis;// make it closer
	posOuterPnts.Add(pnt);
	posOuterPntsRemap.Add(IwPoint3d(0,0,18));// remap reference point (18th cut edge point) and its delta
	if ( edgeMiddlePoints )
	{
		if ( edgeMiddlePoints->GetAt(6) != IwPoint3d(0,0,0) )
		{
			posOuterPnts.Add(edgeMiddlePoints->GetAt(6));
			posOuterPntsRemap.Add(IwPoint3d(0,0,14.75));// this is an edge middle point between 14th~18th.
		}
		if ( edgeMiddlePoints->GetAt(7) != IwPoint3d(0,0,0) )
		{
			posOuterPnts.Add(edgeMiddlePoints->GetAt(7));
			posOuterPntsRemap.Add(IwPoint3d(0,0,14.5));// this is an edge middle point between 14th~18th.
		}
		if ( edgeMiddlePoints->GetAt(8) != IwPoint3d(0,0,0) )
		{
			posOuterPnts.Add(edgeMiddlePoints->GetAt(8));
			posOuterPntsRemap.Add(IwPoint3d(0,0,14.25));// this is an edge middle point between 14th~18th.
		}
	}
	posOuterPnts.Add(cutFeatPnts.GetAt(14));
	posOuterPntsRemap.Add(IwPoint3d(0,0,14));
	if ( edgeMiddlePoints )
	{
		if ( edgeMiddlePoints->GetAt(0) != IwPoint3d(0,0,0) )
		{
			posOuterPnts.Add(edgeMiddlePoints->GetAt(0));
			posOuterPntsRemap.Add(IwPoint3d(0,0,10.75));// this is an edge middle point between 10th~14th.
		}
		if ( edgeMiddlePoints->GetAt(1) != IwPoint3d(0,0,0) )
		{
			posOuterPnts.Add(edgeMiddlePoints->GetAt(1));
			posOuterPntsRemap.Add(IwPoint3d(0,0,10.5));// this is an edge middle point between 10th~14th.
		}
		if ( edgeMiddlePoints->GetAt(2) != IwPoint3d(0,0,0) )
		{
			posOuterPnts.Add(edgeMiddlePoints->GetAt(2));
			posOuterPntsRemap.Add(IwPoint3d(0,0,10.25));// this is an edge middle point between 10th~14th.
		}
	}
	posOuterPnts.Add(cutFeatPnts.GetAt(10));
	posOuterPntsRemap.Add(IwPoint3d(0,0,10));
	posOuterPnts.Add(cutFeatPnts.GetAt(6));
	posOuterPntsRemap.Add(IwPoint3d(0,0,6));
	posOuterPnts.Add(cutFeatPnts.GetAt(2));
	posOuterPntsRemap.Add(IwPoint3d(0,0,2));

	// positive side inner points
	posInnerPnts.Add(cutFeatPnts.GetAt(3));
	posInnerPntsRemap.Add(IwPoint3d(0,0,3));
	posInnerPnts.Add(cutFeatPnts.GetAt(7));
	posInnerPntsRemap.Add(IwPoint3d(0,0,7));
	posInnerPnts.Add(cutFeatPnts.GetAt(11));
	posInnerPntsRemap.Add(IwPoint3d(0,0,11.0));

	// negative side inner points
	negInnerPnts.Add(cutFeatPnts.GetAt(12));
	negInnerPntsRemap.Add(IwPoint3d(0,0,12.0));
	negInnerPnts.Add(cutFeatPnts.GetAt(8));
	negInnerPntsRemap.Add(IwPoint3d(0,0,8));
	negInnerPnts.Add(cutFeatPnts.GetAt(4));
	negInnerPntsRemap.Add(IwPoint3d(0,0,4));

	// negative side outer points
	negOuterPnts.Add(cutFeatPnts.GetAt(5));
	negOuterPntsRemap.Add(IwPoint3d(0,0,5));
	negOuterPnts.Add(cutFeatPnts.GetAt(9));
	negOuterPntsRemap.Add(IwPoint3d(0,0,9));
	negOuterPnts.Add(cutFeatPnts.GetAt(13));
	negOuterPntsRemap.Add(IwPoint3d(0,0,13));
	if ( edgeMiddlePoints )
	{
		if ( edgeMiddlePoints->GetAt(3) != IwPoint3d(0,0,0) )
		{
			negOuterPnts.Add(edgeMiddlePoints->GetAt(3));
			negOuterPntsRemap.Add(IwPoint3d(0,0,13.25));// this is an edge middle point between 13th~17th.
		}
		if ( edgeMiddlePoints->GetAt(4) != IwPoint3d(0,0,0) )
		{
			negOuterPnts.Add(edgeMiddlePoints->GetAt(4));
			negOuterPntsRemap.Add(IwPoint3d(0,0,13.5));// this is an edge middle point between 13th~17th.
		}
		if ( edgeMiddlePoints->GetAt(5) != IwPoint3d(0,0,0) )
		{
			negOuterPnts.Add(edgeMiddlePoints->GetAt(5));
			negOuterPntsRemap.Add(IwPoint3d(0,0,13.75));// this is an edge middle point between 13th~17th.
		}
	}
	negOuterPnts.Add(cutFeatPnts.GetAt(17));
	negOuterPntsRemap.Add(IwPoint3d(0,0,17));
	if ( edgeMiddlePoints )
	{
		if ( edgeMiddlePoints->GetAt(9) != IwPoint3d(0,0,0) )
		{
			negOuterPnts.Add(edgeMiddlePoints->GetAt(9));
			negOuterPntsRemap.Add(IwPoint3d(0,0,17.25));// this is an edge middle point between 17th~19th.
		}
		if ( edgeMiddlePoints->GetAt(10) != IwPoint3d(0,0,0) )
		{
			negOuterPnts.Add(edgeMiddlePoints->GetAt(10));
			negOuterPntsRemap.Add(IwPoint3d(0,0,17.5));// this is an edge middle point between 17th~19th.
		}
		if ( edgeMiddlePoints->GetAt(11) != IwPoint3d(0,0,0) )
		{
			negOuterPnts.Add(edgeMiddlePoints->GetAt(11));
			negOuterPntsRemap.Add(IwPoint3d(0,0,17.75));// this is an edge middle point between 17th~19th.
		}
	}
	pnt = cutFeatPnts.GetAt(19) - factor*cutToEdge*xAxis;// make it closer
	negOuterPnts.Add(pnt);
	negOuterPntsRemap.Add(IwPoint3d(0,0,19));

	inputAntOuterPnts.Append(antOuterPnts);
	for (unsigned i=1; i<(inputAntOuterPnts.GetSize()-1); i++)
	{
		anteriorOuterPnts.Add(inputAntOuterPnts.GetAt(i));
	}

	bool rOK;
	IwTArray<IwPoint3d> posOuterUVPnts;
	IwTArray<IwPoint3d> posInnerUVPnts;
	IwTArray<IwPoint3d> negOuterUVPnts;
	IwTArray<IwPoint3d> negInnerUVPnts;
	IwTArray<IwPoint3d> anteriorOuterUVPnts;
	IwPoint3d posPostTipUVPnt;
	IwPoint3d negPostTipUVPnt;
	IwPoint3d antTipUVPnt;

	// Project the points from XYZ to UV on sketch surface
	rOK = ProjectXYZtoSketchSurfaceUV(posOuterPnts, posOuterUVPnts);
	if ( !rOK ) return false;
	rOK = ProjectXYZtoSketchSurfaceUV(posInnerPnts, posInnerUVPnts);
	if ( !rOK ) return false;
	rOK = ProjectXYZtoSketchSurfaceUV(negOuterPnts, negOuterUVPnts);
	if ( !rOK ) return false;
	rOK = ProjectXYZtoSketchSurfaceUV(negInnerPnts, negInnerUVPnts);
	if ( !rOK ) return false;
	rOK = ProjectXYZtoSketchSurfaceUV(anteriorOuterPnts, anteriorOuterUVPnts);
	if ( !rOK ) return false;

	rOK = ProjectXYZtoSketchSurfaceUV(posPostTipPnt, posPostTipUVPnt);
	if ( !rOK ) return false;
	rOK = ProjectXYZtoSketchSurfaceUV(negPostTipPnt, negPostTipUVPnt);
	if ( !rOK ) return false;

	// For positive posterior tip profile
	IwTArray<IwPoint3d> posPostTipUVPnts;
	IwTArray<int> posPostTipPntsInfo;
	IwTArray<IwPoint3d> posPostTipPntsRemap;
	
	double posPostProfileAdjustDist=0, negPostProfileAdjustDist=0;
	IwTArray<IwPoint3d> posPnts, negPnts;
	if ( /*m_pDoc->GetJCurves() && m_pDoc->GetJCurves()->GetCoPlanarJPoints(posPnts, negPnts)*/ 1 )/*Existence of CoPlanarJPoints ensures J-Curves has been designed, rathern than an empty object.*/
	{
		double safeZone = 0.1; 
		double maxFlaringDist = m_pDoc->GetVarTableValue("JCURVE POSTERIOR MAX FLARING DISTANCE TO CUT EDGE");
		double minFlaringDist = m_pDoc->GetVarTableValue("JCURVE POSTERIOR MIN FLARING DISTANCE TO CUT EDGE");
		m_pDoc->GetOutlineProfile()->GetPostProfileAdjustDistance(posPostProfileAdjustDist, negPostProfileAdjustDist);
		double vertAllowance = 0.25;
		if (m_pDoc->isPosteriorStabilized())
			vertAllowance = 0.65;
		double posVerDist = 2, negVerDist = 2;//psv---
		//m_pDoc->GetJCurves()->GetJCurvesFlaringDistancesToPosteriorCutEdges(posVerDist, negVerDist);//PSV---
		if ( posVerDist > maxFlaringDist-safeZone )
			posPostProfileAdjustDist += maxFlaringDist - posVerDist;
		else if ( posVerDist < minFlaringDist+safeZone )
			posPostProfileAdjustDist += minFlaringDist - posVerDist;
		// But we do not want to move more than 0.25/0.65mm
		if (posPostProfileAdjustDist < -vertAllowance)
			posPostProfileAdjustDist = -vertAllowance;
		else if (posPostProfileAdjustDist > vertAllowance)
			posPostProfileAdjustDist = vertAllowance;

		if ( negVerDist > maxFlaringDist-safeZone )
			negPostProfileAdjustDist += maxFlaringDist - negVerDist;
		else if ( negVerDist < minFlaringDist+safeZone )
			negPostProfileAdjustDist += minFlaringDist - negVerDist;
		// But we do not want to move more than 0.25/0.65mm
		if (negPostProfileAdjustDist < -vertAllowance)
			negPostProfileAdjustDist = -vertAllowance;
		else if (negPostProfileAdjustDist > vertAllowance)
			negPostProfileAdjustDist = vertAllowance;
		// Set them back to OutlineProfile
		m_pDoc->GetOutlineProfile()->SetPostProfileAdjustDistance(posPostProfileAdjustDist, negPostProfileAdjustDist);
	}

	// 
	double posPosteriorTiltAngle, negPosteriorTiltAngle;
	if (m_pDoc->IsPositiveSideLateral())
	{
		posPosteriorTiltAngle = 0;//m_pDoc->GetAdvancedControl()->GetLateralPosteriorTipTiltAngle();
		negPosteriorTiltAngle = 0;//m_pDoc->GetAdvancedControl()->GetMedialPosteriorTipTiltAngle();
	}
	else
	{
		negPosteriorTiltAngle = 0;// m_pDoc->GetAdvancedControl()->GetLateralPosteriorTipTiltAngle();
		posPosteriorTiltAngle = 0;//m_pDoc->GetAdvancedControl()->GetMedialPosteriorTipTiltAngle();
	}

	rOK = false;
	int index = 0;
	IwTArray<IwFace*> cutFaces;
	if ( m_pDoc->GetFemoralCuts()->SearchCutFaces(index, cutFaces) )
	{// Define an outline as close to posterior cut as possible
		//IwFace* cutFace = cutFaces.GetAt(0);
		//rOK = DefinePostProfile(cutFace, cutFeatPnts.GetAt(2), cutFeatPnts.GetAt(3), posPostProfileAdjustDist, posPosteriorTiltAngle, posPostTipUVPnts, posPostTipPntsInfo, posPostTipPntsRemap);
	}
	
	if ( 1/*!rOK*/ )
	{// Just define an arc
		DefinePostArc(posOuterUVPnts[posOuterUVPnts.GetSize()-1], 
							 posPostTipUVPnt, posInnerUVPnts[0], 
							 posPostTipUVPnts, posPostTipPntsInfo, posPostTipPntsRemap);
	}

	IwTArray<IwPoint3d> negPostTipUVPnts;
	IwTArray<int> negPostTipPntsInfo;
	IwTArray<IwPoint3d> negPostTipPntsRemap;

	rOK = false;
	index = 1;
	if ( m_pDoc->GetFemoralCuts()->SearchCutFaces(index, cutFaces) )
	{// Define an outline as close to posterior cut as possible
		//IwFace* cutFace = cutFaces.GetAt(0);
		//rOK = DefinePostProfile(cutFace, cutFeatPnts.GetAt(4), cutFeatPnts.GetAt(5), negPostProfileAdjustDist, negPosteriorTiltAngle, negPostTipUVPnts, negPostTipPntsInfo, negPostTipPntsRemap);
	}

	if ( 1/*!rOK*/ )
	{
		rOK = DefinePostArc(negInnerUVPnts[negInnerUVPnts.GetSize()-1], 
								 negPostTipUVPnt, negOuterUVPnts[0], 
								 negPostTipUVPnts, negPostTipPntsInfo, negPostTipPntsRemap);
	}

	IwTArray<IwPoint3d> notchUVArcPnts;
	IwTArray<int> notchArcPntsInfo;
	IwTArray<IwPoint3d> notchArcPntsRemap;
	rOK = DefineNotchArc(posInnerUVPnts, negInnerUVPnts, 
					notchUVArcPnts, notchArcPntsInfo, notchArcPntsRemap);

	unsigned antMiddleSize = (unsigned)(anteriorOuterUVPnts.GetSize()/2.0);
	for (unsigned i=antMiddleSize; i<anteriorOuterUVPnts.GetSize(); i++)
	{
		UVFeatPnts.Add(anteriorOuterUVPnts.GetAt(i));
		UVFeatPntsInfo.Add(0); // Interpolation point
		if ( i == (anteriorOuterUVPnts.GetSize()-1) ) // last one
			UVFeatPntsRemap.Add(IwPoint3d(0,0,18.25));// this is the positive side 1st anterior side point above the ant/ant-chamfer cut edge
		else if ( i == (anteriorOuterUVPnts.GetSize()-2) ) // last 2nd one
			UVFeatPntsRemap.Add(IwPoint3d(0,0,18.5));// this is the positive side 2nd anterior side point above the ant/ant-chamfer cut edge
		else
			UVFeatPntsRemap.Add(IwPoint3d(0,0,-1));// no remap reference points for anterior points
	}

	for (unsigned i=0; i<posOuterUVPnts.GetSize(); i++)
	{
		UVFeatPnts.Add(posOuterUVPnts.GetAt(i));
		UVFeatPntsInfo.Add(0); // Interpolation point
		UVFeatPntsRemap.Add(posOuterPntsRemap.GetAt(i));
	}

	UVFeatPnts.Append(posPostTipUVPnts);
	UVFeatPntsInfo.Append(posPostTipPntsInfo);
	UVFeatPntsRemap.Append(posPostTipPntsRemap);

	for (unsigned i=0; i<posInnerUVPnts.GetSize(); i++)
	{
		UVFeatPnts.Add(posInnerUVPnts.GetAt(i));
		UVFeatPntsInfo.Add(0); // Interpolation point
		UVFeatPntsRemap.Add(posInnerPntsRemap.GetAt(i));
	}

	UVFeatPnts.Append(notchUVArcPnts);
	UVFeatPntsInfo.Append(notchArcPntsInfo);
	UVFeatPntsRemap.Append(notchArcPntsRemap);

	for (unsigned i=0; i<negInnerUVPnts.GetSize(); i++)
	{
		UVFeatPnts.Add(negInnerUVPnts.GetAt(i));
		UVFeatPntsInfo.Add(0); // Interpolation point
		UVFeatPntsRemap.Add(negInnerPntsRemap.GetAt(i)); 
	}

	UVFeatPnts.Append(negPostTipUVPnts);
	UVFeatPntsInfo.Append(negPostTipPntsInfo);
	UVFeatPntsRemap.Append(negPostTipPntsRemap);

	for (unsigned i=0; i<negOuterUVPnts.GetSize(); i++)
	{
		UVFeatPnts.Add(negOuterUVPnts.GetAt(i));
		UVFeatPntsInfo.Add(0);// Interpolation point
		UVFeatPntsRemap.Add(negOuterPntsRemap.GetAt(i));
	}

	for (unsigned i=0; i<(antMiddleSize+1); i++)
	{
		UVFeatPnts.Add(anteriorOuterUVPnts.GetAt(i));
		UVFeatPntsInfo.Add(0);// Interpolation point
		if ( i==0 )
			UVFeatPntsRemap.Add(IwPoint3d(0,0,19.25)); // this is the negative side 1st anterior side point above the ant/ant-chamfer cut edge
		else if ( i==1 )
			UVFeatPntsRemap.Add(IwPoint3d(0,0,19.5)); // this is the negative side 2nd anterior side point above the ant/ant-chamfer cut edge
		else
			UVFeatPntsRemap.Add(IwPoint3d(0,0,-1)); // no reference points for remap
	}

	// Set to m_idealUVFeatPnts, m_idealUVFeatPntsInfo, and m_idealUVFeatPntsRemap
	m_idealUVFeatPnts.RemoveAll();
	m_idealUVFeatPntsInfo.RemoveAll();
	m_idealUVFeatPntsRemap.RemoveAll();
	m_idealUVFeatPnts.Append(UVFeatPnts);
	m_idealUVFeatPntsInfo.Append(UVFeatPntsInfo);
	m_idealUVFeatPntsRemap.Append(UVFeatPntsRemap);

	return true;
}

bool OutlineProfileMgrModel::ProjectXYZtoSketchSurfaceUV
(
	IwTArray<IwPoint3d>& inputXYZPnts,		// I:
	IwTArray<IwPoint3d>& outputUVPnts		// O: z is not useful
)
{
	IwTArray<IwPoint3d> resultUVPnts;
	// Project the inputXYZPnts to the sketch surface
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile == NULL) return false;

	IwBSplineSurface* sketchSurface = NULL;
	outlineProfile->GetSketchSurface(sketchSurface);
	if (sketchSurface == NULL) return false;

	IwExtent2d surfDomain = sketchSurface->GetNaturalUVDomain();
	IwSolutionArray sols;
	for (unsigned i=0; i<inputXYZPnts.GetSize(); i++)
	{
		sketchSurface->GlobalPointSolve(surfDomain, IW_SO_MINIMIZE, 
							inputXYZPnts.GetAt(i), 0.01, NULL, IW_SR_SINGLE, sols);
		if ( sols.GetSize() != 1 ) return false;

		IwPoint3d UVPnt(sols[0].m_vStart[0], sols[0].m_vStart[1], 0.0);
		resultUVPnts.Add(UVPnt);
	}

	outputUVPnts.Append(resultUVPnts);

	return true;
}

bool OutlineProfileMgrModel::DefinePostProfile
(
	IwFace*& cutFace,						// I:
	IwPoint3d posEdgePnt,					// I:
	IwPoint3d negEdgePnt,					// I:
	double moveFurtherPosteriorly,			// I: move the profile further posteriorly to the edge
	double posteriorTiltAngle,				// I: degree 
	IwTArray<IwPoint3d>& profileUVPnts,		// O:
	IwTArray<int>& profilePntsInfo,			// O:
	IwTArray<IwPoint3d>& profilePntsRemap	// O:
)
{
	// clean up data if any
	profileUVPnts.RemoveAll();
	profilePntsInfo.RemoveAll();
	profilePntsRemap.RemoveAll();

	// get posterior edge info
	IwTArray<IwLoop*> loops;
	cutFace->GetLoops(loops);
	IwLoop* outerLoop = loops.GetAt(0);
	IwLoopuse *pLU0, *pLU1;
	outerLoop->GetLoopuses(pLU0,pLU1);
	IwTArray<IwEdgeuse*> edgeUses;
	pLU0->GetEdgeuses(edgeUses);

	IwVector3d pnt, faceNormal;
	cutFace->GetSurface()->IsPlanar(0.01, &pnt, &faceNormal);
	IwFaceuse *faceuse1, *faceuse2;
	cutFace->GetFaceuses(faceuse1, faceuse2);
	IwOrientationType oType = faceuse1->GetOrientation();
	if ( oType != IW_OT_SAME )
		faceNormal = -faceNormal;// flip

	// Searching for the curvy edge
	bool curveDirCCW;
	IwTArray<IwCurve*> outerCurves;
	IwCurve *linearCurve, *crv;
	double lenCutEdge=10, r, radius=1.0;
	int crvNo = 0;
	for (unsigned i=0; i<edgeUses.GetSize(); i++)
	{
		IwEdgeuse* edgeuse = edgeUses.GetAt(i);
		IwEdge* edge = edgeuse->GetEdge();
		crv = edge->GetCurve();
		if ( crv->IsLinear(0.01) )
		{
			crv = edge->GetCurve();
			linearCurve = crv;
			linearCurve->Length(edge->GetInterval(), 0.01, lenCutEdge);
			if ( lenCutEdge < 10.0)
				lenCutEdge = 10.0;// min 10mm
			r = m_pDoc->GetAdvancedControl()->GetOutlineProfilePosteriorCutFilterRadiusRatio()*lenCutEdge;
			if ( r > radius )
				radius = r;// take the max one, if multiple occur.
		}
		else
		{
			edge->GetCurve()->Copy(m_pDoc->GetIwContext(), crv);
			crv->Trim(edge->GetInterval());// trim with edge interval
			if ( crvNo == 0 )// first curve
			{
				// curveDirCCW is the first curve direction
				oType = edgeuse->GetOrientation();
				if ( oType == IW_OT_SAME )
					curveDirCCW = true;
				else
					curveDirCCW = false;
			}
			else
			{
				// whenever the crv is in different direction with the first curve (curveDirCCW)
				IwExtent1d dom;
				if ( edgeuse->GetOrientation() == IW_OT_SAME && curveDirCCW == false )
				{
					crv->ReverseParameterization(crv->GetNaturalInterval(), dom);
				}
				else if ( edgeuse->GetOrientation() == IW_OT_OPPOSITE && curveDirCCW == true )
				{
					crv->ReverseParameterization(crv->GetNaturalInterval(), dom);
				}
			}
			outerCurves.Add(crv);
			crvNo++;
		}
	}

	// merge all curve tegether
	IwBSplineCurve *outerCurve;
	if ( outerCurves.GetSize() == 1 )
	{
		outerCurve = (IwBSplineCurve *)outerCurves.GetAt(0);
	}
	else
	{
		IwBSplineCurve* oCurve = JoinCurves(outerCurves);
		// smapling points
		IwExtent1d crvDom = oCurve->GetNaturalInterval();
		IwTArray<IwPoint3d> samplingPnts;
		oCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), 100, 0.01, &samplingPnts, NULL);
		// interpolate the points into a curve
		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), samplingPnts, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, outerCurve);
	}

	// offset outerCurve inward
	int offsetDir;
	if ( curveDirCCW )
		offsetDir = 1;
	else
		offsetDir = 2;

	double tol = 0.001;
	IwTArray<IwBSplineCurve*> trimmedOffsets;
	IwCompositeCurve::CreateOffsetsOfCurve(m_pDoc->GetIwContext(), outerCurve, offsetDir, tol, faceNormal, TRUE, radius, trimmedOffsets, FALSE);
	int trimmedOffsetsNo = trimmedOffsets.GetSize();
if (0)
{
	for (unsigned i=0; i<trimmedOffsets.GetSize(); i++)
		ShowCurve(m_pDoc, trimmedOffsets.GetAt(i), green);
}
	// Get edgeDist
	double edgeDist = m_pDoc->GetVarTableValue("OUTLINE PROFILE POSTERIOR DESIRED DISTANCE TO CUT EDGE") + 0.5;
	double outwardRadius = radius-edgeDist;
	// offset curve outward
	IwTArray<IwCompositeCurve*> composisteCurves;
	IwCompositeCurve::BuildCompositesFromCurves(m_pDoc->GetIwContext(), trimmedOffsets, FALSE, tol, 0.01, 0.0, 0.0, 0.0, 0.0, composisteCurves);
	IwCompositeCurve* compCurve = composisteCurves.GetAt(0);
	IwTArray<IwBSplineCurve*> offsetCurvesMinus;
	compCurve->CreateTrimmedOffsets(m_pDoc->GetIwContext(), tol, faceNormal, IW_OC_FILLET_CORNER,
						IW_OD_RIGHT_HAND_SIDE, TRUE, outwardRadius, offsetCurvesMinus);
if (0)
{
	for (unsigned i=0; i<offsetCurvesMinus.GetSize(); i++)
		ShowCurve(m_pDoc, offsetCurvesMinus.GetAt(i), CColor(i*50, i*50, i*50));
}

	if ( (int)offsetCurvesMinus.GetSize() < (2*trimmedOffsetsNo-1) )// something wrong, offset again with a very smaller radius
	{
		radius = edgeDist;
		trimmedOffsets.RemoveAll();
		IwCompositeCurve::CreateOffsetsOfCurve(m_pDoc->GetIwContext(), outerCurve, offsetDir, tol, faceNormal, TRUE, radius, trimmedOffsets, FALSE);
		outwardRadius = radius-edgeDist;
		IwCompositeCurve::BuildCompositesFromCurves(m_pDoc->GetIwContext(), trimmedOffsets, FALSE, tol, 0.01, 0.0, 0.0, 0.0, 0.0, composisteCurves);
		compCurve = composisteCurves.GetAt(0);
		offsetCurvesMinus.RemoveAll();
		compCurve->CreateTrimmedOffsets(m_pDoc->GetIwContext(), tol, faceNormal, IW_OC_FILLET_CORNER,
							IW_OD_RIGHT_HAND_SIDE, TRUE, outwardRadius, offsetCurvesMinus);
	}

	// Sort curves
	IwTArray<IwCurve*> sortedCurves;
	for (unsigned i=0; i<offsetCurvesMinus.GetSize(); i++)
		sortedCurves.Add((IwCurve*)offsetCurvesMinus.GetAt(i));

	int noBeforeSorted = sortedCurves.GetSize();
	SortCurvesChain(sortedCurves);

	if ( noBeforeSorted != sortedCurves.GetSize() )// something wrong, offset again with a very smaller radius
	{
		radius = edgeDist;
		trimmedOffsets.RemoveAll();
		IwCompositeCurve::CreateOffsetsOfCurve(m_pDoc->GetIwContext(), outerCurve, offsetDir, tol, faceNormal, TRUE, radius, trimmedOffsets, FALSE);
		outwardRadius = radius-edgeDist;
		IwCompositeCurve::BuildCompositesFromCurves(m_pDoc->GetIwContext(), trimmedOffsets, FALSE, tol, 0.01, 0.0, 0.0, 0.0, 0.0, composisteCurves);
		compCurve = composisteCurves.GetAt(0);
		offsetCurvesMinus.RemoveAll();
		compCurve->CreateTrimmedOffsets(m_pDoc->GetIwContext(), tol, faceNormal, IW_OC_FILLET_CORNER,
							IW_OD_RIGHT_HAND_SIDE, TRUE, outwardRadius, offsetCurvesMinus);
		// Sort curves
		sortedCurves.RemoveAll();
		for (unsigned i=0; i<offsetCurvesMinus.GetSize(); i++)
			sortedCurves.Add((IwCurve*)offsetCurvesMinus.GetAt(i));

		noBeforeSorted = sortedCurves.GetSize();
		SortCurvesChain(sortedCurves);
	}

	if ( noBeforeSorted != sortedCurves.GetSize() )
		return false;

	IwTArray<IwBSplineCurve*> sortedBSplineCurves;
	for (unsigned i=0; i<sortedCurves.GetSize(); i++)
		sortedBSplineCurves.Add((IwBSplineCurve*)sortedCurves.GetAt(i));

	// Make the curve order and direction following CCW
	if ( !curveDirCCW )
	{
		sortedBSplineCurves.ReverseArray(0, sortedBSplineCurves.GetSize());
		for (unsigned i=0; i<sortedBSplineCurves.GetSize(); i++)
		{
			IwBSplineCurve* crv = sortedBSplineCurves.GetAt(i);
			IwExtent1d nDom;
			crv->ReverseParameterization(crv->GetNaturalInterval(), nDom);
			sortedBSplineCurves.SetAt(i, crv);
		}
	}

	// Convert to arc and interpolation points
	// iTW v6 does not use arcs in posterior profiles anymore 
	int useArc = 0;
	IwTArray<IwPoint3d> profPnts;
	IwPoint3d sPnt, ePnt, midPnt;
	double dist;

	// get start point of the first curve
	sortedBSplineCurves.GetAt(0)->GetEnds(sPnt, ePnt);
	profPnts.Add(sPnt);
	profilePntsInfo.Add(0);// always considered as interpolation point
	profilePntsRemap.Add(IwPoint3d(0,0,-1));// do not remap
	double accumulatedLenth=0;
	unsigned segNo = sortedBSplineCurves.GetSize();
	for (unsigned i=0; i<segNo; i++)
	{
		IwBSplineCurve* crv = sortedBSplineCurves.GetAt(i);
		IwAxis2Placement arcAxis;
		double arcR, sDeg, eDeg, arcLength;
		IwBoolean bArc = crv->IsArc(15, 0.0025, arcAxis, arcR, sDeg, eDeg);
		// the first segment and the last segment can not be the arc (too complicated to handle) and arc should not be too narrow
		if ( useArc && bArc && fabs(outwardRadius-arcR) < 0.0025  && i!=0 && i!=(segNo-1) && fabs(sDeg-eDeg)>35.0)
		{// it's an arc
			crv->GetEnds(sPnt, ePnt);
			IwExtent1d crvDom = crv->GetNaturalInterval();
			crv->Length(crvDom, 0.01, arcLength);
			crv->EvaluatePoint(crvDom.Evaluate(0.5), midPnt);
			// We need to shorten the arc a little bit to avoid dramatically tangent change at adjacent connection
			if ( arcLength > 4.0 )
			{
				double ratio = crvDom.GetLength()/arcLength;
				crv->EvaluatePoint(crvDom.GetMin()+1.0*ratio, sPnt);// now the sPnt is 1.0mm away from real start point
				crv->EvaluatePoint(crvDom.GetMax()-1.0*ratio, ePnt);// now the ePnt is 1.0mm away from real end point
			}
			IwPoint3d centerPnt = arcAxis.GetOrigin();
			dist = sPnt.DistanceBetween(profPnts.GetLast());
			if ( dist < 1.05  && profilePntsInfo.GetLast() == 0) // if identical with profPnts.GetLast(), remove profPnts.GetLast()
			{
				profPnts.RemoveLast();
				profilePntsInfo.RemoveLast();
				profilePntsRemap.RemoveLast();
			}
			if ( useArc )
			{
				// Add start/center/end points
				profPnts.Add(sPnt);// arc start point
				profPnts.Add(centerPnt);// arc center point
				profPnts.Add(ePnt);// arc end point

				profilePntsInfo.Add(3);// arc start point
				profilePntsInfo.Add(11);// arc center point, collapsible
				profilePntsInfo.Add(4);// arc end point

				profilePntsRemap.Add(IwPoint3d(0,0,-1));// do not remap
				profilePntsRemap.Add(IwPoint3d(0,0,-1));// do not remap
				profilePntsRemap.Add(IwPoint3d(0,0,-1));// do not remap
			}

			// accumulatedLenth=0
			accumulatedLenth=0;
		}
		else
		{// it's a curve
			double len;
			crv->Length(crv->GetNaturalInterval(), 0.01, len);
			accumulatedLenth+=len;
			if ( len > 5*lenCutEdge ) // too long, someting wrong.
			{
				accumulatedLenth = 0;
			}
			else if ( len > 7.0 || accumulatedLenth> 6.0)// skip if the curve is too short.
			{// convert to points
				int nSeg = (int)(len/6.0) + 1;
				if ( nSeg%2 == 1 )// we want the even number in the for-loop below.
					nSeg += 1;
				IwTArray<IwPoint3d> samplingPnts;
				crv->EquallySpacedPoints(crv->GetNaturalInterval().GetMin(), crv->GetNaturalInterval().GetMax(), nSeg, 0.01, &samplingPnts, NULL); 
				if ( samplingPnts.GetSize() < 3 )// crv is too short, but accumulatedLenth> 6.0, use its middle point
				{
					crv->EvaluatePoint(crv->GetNaturalInterval().Evaluate(0.5), midPnt);
					profPnts.Add(midPnt);
					profilePntsInfo.Add(0);// interpolation point
					profilePntsRemap.Add(IwPoint3d(0,0,-1));// do not remap
					accumulatedLenth = 0;
				}
				else
				{
					for (unsigned i=1; i<(samplingPnts.GetSize()-1); i+=2)
					{
						dist = samplingPnts.GetAt(i).DistanceBetween(profPnts.GetLast());
						if ( dist < 3.05 ) // if very close to the profPnts.GetLast(), do not add to.
						{}
						else
						{
							profPnts.Add(samplingPnts.GetAt(i));
							profilePntsInfo.Add(0);// interpolation point
							profilePntsRemap.Add(IwPoint3d(0,0,-1));// do not remap
							accumulatedLenth -= dist;
						}
					}
				}
			}
		}
	}

	// Convert all points to UV domain
	ProjectXYZtoSketchSurfaceUV(profPnts, profileUVPnts);

	// Remove the first point and the last point, which are on the posterior cut edge
	if ( profilePntsInfo.GetLast() == 0 )// if it is not an arc point, but an interpolation point
	{
		IwPoint3d negEdgeUVPnt;
		ProjectXYZtoSketchSurfaceUV(negEdgePnt, negEdgeUVPnt);
		if ( negEdgeUVPnt.DistanceBetween(profileUVPnts.GetLast()) < 3.5 )
		{
			profileUVPnts.RemoveLast();
			profilePntsInfo.RemoveLast();
			profilePntsRemap.RemoveLast();
		}
	}
	// Do it again, in case the new last point is also close. Remove the first point and the last point, which are on the posterior cut edge
	if ( profilePntsInfo.GetLast() == 0 )// if it is not an arc point, but an interpolation point
	{
		IwPoint3d negEdgeUVPnt;
		ProjectXYZtoSketchSurfaceUV(negEdgePnt, negEdgeUVPnt);
		if ( negEdgeUVPnt.DistanceBetween(profileUVPnts.GetLast()) < 3.5 )
		{
			profileUVPnts.RemoveLast();
			profilePntsInfo.RemoveLast();
			profilePntsRemap.RemoveLast();
		}
	}

	if ( profilePntsInfo.GetAt(0) == 0 )// if it is not an arc point, but an interpolation point
	{
		IwPoint3d posEdgeUVPnt;
		ProjectXYZtoSketchSurfaceUV(posEdgePnt, posEdgeUVPnt);
		if ( posEdgeUVPnt.DistanceBetween(profileUVPnts.GetAt(0)) < 3.5 )
		{
			profileUVPnts.RemoveAt(0);
			profilePntsInfo.RemoveAt(0);
			profilePntsRemap.RemoveAt(0);
		}
	}
	// Do it again
	if ( profilePntsInfo.GetAt(0) == 0 )// if it is not an arc point, but an interpolation point
	{
		IwPoint3d posEdgeUVPnt;
		ProjectXYZtoSketchSurfaceUV(posEdgePnt, posEdgeUVPnt);
		if ( posEdgeUVPnt.DistanceBetween(profileUVPnts.GetAt(0)) < 3.5 )
		{
			profileUVPnts.RemoveAt(0);
			profilePntsInfo.RemoveAt(0);
			profilePntsRemap.RemoveAt(0);
		}
	}

	// if side surface near posterior tip is tilt, the posterior coverage needs to be adjusted.
	double tiltAngleRadian = fabs(posteriorTiltAngle)/180.0*IW_PI;
	double compenDist = m_pDoc->GetVarTableValue("IMPLANT POSTERIOR TIP DESIRED THICKNESS")*tan(tiltAngleRadian); // Assume j-curve at posterior tip is 45 degree.
	// edgeDist was added 0.5mm previously. 
	// Shift all the profileUVPnts posteriorly such that it is distant to the desired posterior distance.
	double edgeDistCloser = 0.5 + compenDist + moveFurtherPosteriorly;
	if ( m_pDoc->isPosteriorStabilized() ) // we want to leave underhang more (about 2mm) near posterior tip for PS 
		edgeDistCloser -= 0.5;

	if ( edgeDistCloser > 0 )
	{
		IwPoint3d uvPoint;
		IwPoint3d uvDelta = IwPoint3d(0, edgeDistCloser, 0);
		for ( unsigned i=0; i< profileUVPnts.GetSize(); i++ )
		{
			uvPoint = profileUVPnts.GetAt(i);
			uvPoint = uvPoint + uvDelta;
			profileUVPnts.SetAt(i, uvPoint);
		}
	}

	return true;
}

bool OutlineProfileMgrModel::ProjectXYZtoSketchSurfaceUV
(
	IwPoint3d& inputXYZPnt, 
	IwPoint3d& outputUVPnt
)
{
	// Project the inputXYZPnt to the sketch surface
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile == NULL) return false;

	IwBSplineSurface* sketchSurface = NULL;
	outlineProfile->GetSketchSurface(sketchSurface);
	if (sketchSurface == NULL) return false;

	IwExtent2d surfDomain = sketchSurface->GetNaturalUVDomain();
	IwSolutionArray sols;
	sketchSurface->GlobalPointSolve(surfDomain, IW_SO_MINIMIZE, 
						inputXYZPnt, 0.01, NULL, IW_SR_SINGLE, sols);
	if ( sols.GetSize() != 1 ) return false;

	IwPoint3d UVPnt(sols[0].m_vStart[0], sols[0].m_vStart[1], 0.0);
	outputUVPnt = UVPnt;

	return true;
}

/////////////////////////////////////////////////////////////////////
// Note the sequence and how the arc is defined. 
/////////////////////////////////////////////////////////////////////
bool OutlineProfileMgrModel::DefinePostArc
(
	IwPoint3d& prevEdgeUVPnt,			// I:
	IwPoint3d& arcTipUVPnt,				// I:
	IwPoint3d& postEdgeUVPnt,			// I:
	IwTArray<IwPoint3d>& arcUVPnts,		// O:
	IwTArray<int>& arcPntsInfo,			// O:
	IwTArray<IwPoint3d>& arcPntsRemap	// O:
)
{
	IwContext& iwContext = m_pDoc->GetIwContext();
	double arcRadius = fabs(prevEdgeUVPnt.x - postEdgeUVPnt.x)/2.0;
	IwPoint3d prevPostMiddlePnt = 0.5*(prevEdgeUVPnt+postEdgeUVPnt);
	IwPoint3d modifiedArcTipUVPnt = IwPoint3d(prevPostMiddlePnt.x, arcTipUVPnt.y, 0.0);
	double sign = (prevPostMiddlePnt.y > modifiedArcTipUVPnt.y) ? 1.0 : -1.0;
	IwPoint3d arcCenter = modifiedArcTipUVPnt + sign*IwPoint3d(0, arcRadius, 0);
	IwVector3d xAxis, yAxis;
	// x/y axes define a ccw circle
	if (sign>0) 
	{
		xAxis = IwVector3d(0, 1, 0);
		yAxis = IwVector3d(-1, 0, 0);
	}
	else
	{
		xAxis = IwVector3d(0, -1, 0);
		yAxis = IwVector3d(1, 0, 0);
	}
	IwAxis2Placement circleAxes(arcCenter, xAxis, yAxis);
	IwBSplineCurve* circleSeg=NULL;
	IwExtent1d circleDomain;
	double prevArcParam, postArcParam;

	IwBSplineCurve::CreateCircleSegment(iwContext, 2, circleAxes, arcRadius, 1, 359, IW_CO_QUADRATIC, circleSeg);
	circleDomain = circleSeg->GetNaturalInterval();
	double deltaT = (circleDomain.GetLength()/358.0);
	double t = circleDomain.GetMin();
	double dot0, dot1;
	IwPoint3d pnt0, pnt1;
	IwVector3d vec00, vec01, vec10, vec11, cross0;
	bool gotPrev = false, gotPost = false;
	for (int i=1; i<358; t+=deltaT,i++)
	{
		circleSeg->EvaluatePoint(t, pnt0);
		circleSeg->EvaluatePoint(t+deltaT, pnt1);
		vec00 = pnt0-prevEdgeUVPnt;
		vec01 = arcCenter-pnt0;
		vec10 = pnt1-prevEdgeUVPnt;
		vec11 = arcCenter-pnt1;
		dot0 = vec00.Dot(vec01);
		dot1 = vec10.Dot(vec11);
		cross0 = vec00*vec01;
		if (dot0*dot1 <=0 && sign*cross0.z<0)
		{
			prevArcParam = t;
			gotPrev = true;
		}

		vec00 = pnt0-postEdgeUVPnt;
		vec01 = arcCenter-pnt0;
		vec10 = pnt1-postEdgeUVPnt;
		vec11 = arcCenter-pnt1;
		dot0 = vec00.Dot(vec01);
		dot1 = vec10.Dot(vec11);
		cross0 = vec00*vec01;
		if (dot0*dot1 <=0 && sign*cross0.z>0)
		{
			postArcParam = t;
			gotPost = true;
		}
		if (gotPrev && gotPost) break;
	}

	if ( !(gotPrev && gotPost) ) return false;

	circleSeg->Trim(IwExtent1d(prevArcParam, postArcParam));
	circleDomain = circleSeg->GetNaturalInterval();
	IwPoint3d arcStart, arcEnd;
	circleSeg->GetEnds(arcStart, arcEnd);
	arcUVPnts.Add(arcStart);
	arcUVPnts.Add(arcCenter);
	arcUVPnts.Add(arcEnd);

	arcPntsInfo.Add(3);	// arc start point
	arcPntsInfo.Add(11);// arc center point, collapsible
	arcPntsInfo.Add(4); // arc end point

	arcPntsRemap.Add(IwPoint3d(0,0,-1));// Do not remap
	arcPntsRemap.Add(IwPoint3d(0,0,-1));// Do not remap
	arcPntsRemap.Add(IwPoint3d(0,0,-1));// Do not remap

	return true;
}

/////////////////////////////////////////////////////////////////////
// Note the sequence and how the arc is defined. 
/////////////////////////////////////////////////////////////////////
bool OutlineProfileMgrModel::DefineNotchArc
(
	IwTArray<IwPoint3d>& prevEdgeUVPnts, // I:
	IwTArray<IwPoint3d>& postEdgeUVPnts, // I:
	IwTArray<IwPoint3d>& arcUVPnts,		// O: 
	IwTArray<int>& arcPntsInfo,			// O:
	IwTArray<IwPoint3d>& arcPntsRemap	// O:
)
{
	IwContext& iwContext = m_pDoc->GetIwContext();
	double refSize = 76.0;
	if ( m_pDoc->GetFemoralAxes() )
		refSize = m_pDoc->GetFemoralAxes()->GetEpiCondyleSize();

	IwPoint3d prevEdgeUVPnt = prevEdgeUVPnts.GetAt(prevEdgeUVPnts.GetSize()-1);
	IwPoint3d postEdgeUVPnt = postEdgeUVPnts.GetAt(0);
	double arcRadius = 7.0*refSize/76.0;
	IwPoint3d prevPostMiddlePnt = 0.5*(prevEdgeUVPnt+postEdgeUVPnt);
	double sign = (postEdgeUVPnts.GetAt(1).y > postEdgeUVPnts.GetAt(0).y) ? 1.0 : -1.0;
	IwPoint3d arcCenter = prevPostMiddlePnt - sign*IwPoint3d(0, arcRadius, 0);
	IwVector3d xAxis, yAxis;
	// x/y axes define a ccw circle
	if (sign>0)
	{
		xAxis = IwVector3d(0, 1, 0);
		yAxis = IwVector3d(1, 0, 0);
	}
	else
	{
		xAxis = IwVector3d(0, -1, 0);
		yAxis = IwVector3d(-1, 0, 0);
	}
	IwAxis2Placement circleAxes(arcCenter, xAxis, yAxis);
	IwBSplineCurve* circleSeg=NULL;
	IwExtent1d circleDomain;
	double prevArcParam, postArcParam;

	IwBSplineCurve::CreateCircleSegment(iwContext, 2, circleAxes, arcRadius, 1, 359, IW_CO_QUADRATIC, circleSeg);
	circleDomain = circleSeg->GetNaturalInterval();
	double deltaT = (circleDomain.GetLength()/358.0);
	double t = circleDomain.GetMin();
	double dot0, dot1;
	IwPoint3d pnt0, pnt1;
	IwVector3d vec00, vec01, vec10, vec11, cross0;
	bool gotPrev = false, gotPost = false;
	for (int i=1; i<358; t+=deltaT,i++)
	{
		circleSeg->EvaluatePoint(t, pnt0);
		circleSeg->EvaluatePoint(t+deltaT, pnt1);
		vec00 = pnt0-prevEdgeUVPnt;
		vec01 = arcCenter-pnt0;
		vec10 = pnt1-prevEdgeUVPnt;
		vec11 = arcCenter-pnt1;
		dot0 = vec00.Dot(vec01);
		dot1 = vec10.Dot(vec11);
		cross0 = vec00*vec01;
		if (dot0*dot1 <=0 && sign*cross0.z<0)
		{
			prevArcParam = t;
			gotPrev = true;
		}

		vec00 = pnt0-postEdgeUVPnt;
		vec01 = arcCenter-pnt0;
		vec10 = pnt1-postEdgeUVPnt;
		vec11 = arcCenter-pnt1;
		dot0 = vec00.Dot(vec01);
		dot1 = vec10.Dot(vec11);
		cross0 = vec00*vec01;
		if (dot0*dot1 <=0 && sign*cross0.z>0)
		{
			postArcParam = t;
			gotPost = true;
		}
		if (gotPrev && gotPost) break;
	}

	if ( !(gotPrev && gotPost) ) return false;

	circleSeg->Trim(IwExtent1d(prevArcParam, postArcParam));
	circleDomain = circleSeg->GetNaturalInterval();
	IwPoint3d arcStart, arcEnd;
	circleSeg->GetEnds(arcStart, arcEnd);
	arcUVPnts.Add(arcStart);
	arcUVPnts.Add(arcCenter);
	arcUVPnts.Add(arcEnd);

	arcPntsInfo.Add(3); // arc start point
	arcPntsInfo.Add(2); // arc center point, NOT collapsible
	arcPntsInfo.Add(4); // arc end point
	
	arcPntsRemap.Add(IwPoint3d(0,0,-1)); // Do not remap
	arcPntsRemap.Add(IwPoint3d(0,0,-1)); // Do not remap
	arcPntsRemap.Add(IwPoint3d(0,0,-1)); // Do not remap

	return true;
}


//////////////////////////////////////////////////////////////
// This function makes the anterior profile in a better shape.
// Note when call this function, the two-ear shape arcs have 
// not been added in yet.
//////////////////////////////////////////////////////////////
void OutlineProfileMgrModel::ModifyAnteriorShape
(
	IwTArray<IwPoint3d>& UVFeatPnts,	// I/O:
	IwTArray<int>& UVFeatPntsInfo,		// I/O:
	IwTArray<IwPoint3d>& UVFeatPntsRemap// I/O:
)
{
	// Get the anterior points
	IwPoint3d seamPoint = UVFeatPnts.GetAt(0);
	IwPoint3d posEarPoint = UVFeatPnts.GetAt(1);
	IwPoint3d posMiddlePoint = UVFeatPnts.GetAt(2);
	IwPoint3d posLowerPoint = UVFeatPnts.GetAt(3);
	IwPoint3d posEdgePoint = UVFeatPnts.GetAt(4);
	int no = (int)UVFeatPnts.GetSize() - 1;
	IwPoint3d negEarPoint = UVFeatPnts.GetAt(no-1);
	IwPoint3d negMiddlePoint = UVFeatPnts.GetAt(no-2);
	IwPoint3d negLowerPoint = UVFeatPnts.GetAt(no-3);
	IwPoint3d negEdgePoint = UVFeatPnts.GetAt(no-4);

	//
	IwPoint3d medialEarPoint;

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	// 
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		medialEarPoint = negEarPoint;
	}
	else
	{
		positiveSideIsLateral = false;
		medialEarPoint = posEarPoint;
	}
	// convert medialEarPoint to XYZ
	IwVector3d medialEarPointXYZ, medialEarPointXYZNormal;
	m_pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(medialEarPoint, medialEarPointXYZ, medialEarPointXYZNormal);

	// determine how much overhanging it is
	IwBSplineSurface* femurSurf = m_pDoc->GetFemur()->GetSinglePatchSurface();
	IwPoint3d cPnt;
	IwPoint2d param2d;
	double overhangDist = DistFromPointToSurface(medialEarPointXYZ, femurSurf, cPnt, param2d);
	IwVector3d tempVec = cPnt - medialEarPointXYZ;
	if ( tempVec.Dot(medialEarPointXYZNormal) < 0 )
		overhangDist = -overhangDist;

	// adjust medialEarPoint if overhanging over 1.0mm
	if ( overhangDist < -1.0 )
	{
		if ( overhangDist < -3.0 )
			medialEarPoint = medialEarPoint + 2.0*IwVector3d(0, 1, 0);// shorten the medial ear 1mm
		else 
			medialEarPoint = medialEarPoint + IwVector3d(0, 1, 0);// shorten the medial ear 1mm
		if ( positiveSideIsLateral )
			negEarPoint = medialEarPoint;
		else
			posEarPoint = medialEarPoint;
	}

	// Construct a curve representing the anterior shape
	IwTArray<IwPoint3d> antPoints;
	antPoints.Add(negEdgePoint);
	antPoints.Add(negLowerPoint);
	antPoints.Add(negEarPoint);
	antPoints.Add(seamPoint);
	antPoints.Add(posEarPoint);
	antPoints.Add(posLowerPoint);
	antPoints.Add(posEdgePoint);
	IwBSplineCurve* antCrv=NULL;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), antPoints, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, antCrv);

	// adjust posMiddlePoint & negMiddlePoint
	double param;
	DistFromPointToCurve(posMiddlePoint, antCrv, cPnt, param);
	tempVec = cPnt - posMiddlePoint;
	if ( tempVec.Dot(IwVector3d(1,0,0))< 0 )// cPnt is in the inner side of original anterior profile
	{
		posMiddlePoint = 0.5*posMiddlePoint + 0.5*cPnt;
	}
	// negative side
	DistFromPointToCurve(negMiddlePoint, antCrv, cPnt, param);
	tempVec = cPnt - negMiddlePoint;
	if ( tempVec.Dot(IwVector3d(1,0,0))> 0 )// cPnt is in the inner side of original anterior profile
	{
		negMiddlePoint = 0.5*negMiddlePoint + 0.5*cPnt;
	}

	// Construct another curve representing the anterior shape
	antPoints.RemoveAll();
	antPoints.Add(negEdgePoint);
	antPoints.Add(negMiddlePoint);
	antPoints.Add(negEarPoint);
	antPoints.Add(seamPoint);
	antPoints.Add(posEarPoint);
	antPoints.Add(posMiddlePoint);
	antPoints.Add(posEdgePoint);
	IwBSplineCurve* antCrv2=NULL;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), antPoints, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, antCrv2);

	// adjust posLowerPoint & negLowerPoint
	DistFromPointToCurve(posLowerPoint, antCrv2, cPnt, param);
	tempVec = cPnt - posLowerPoint;
	if ( tempVec.Dot(IwVector3d(1,0,0))< 0 )// cPnt is in the inner side of original anterior profile
	{
		posLowerPoint = 0.5*posLowerPoint + 0.5*cPnt;
	}
	// negative side
	DistFromPointToCurve(negLowerPoint, antCrv2, cPnt, param);
	tempVec = cPnt - negLowerPoint;
	if ( tempVec.Dot(IwVector3d(1,0,0))> 0 )// cPnt is in the inner side of original anterior profile
	{
		negLowerPoint = 0.5*negLowerPoint + 0.5*cPnt;
	}

	// set back to UVFeatPnts
	UVFeatPnts.SetAt(1, posEarPoint);
	UVFeatPnts.SetAt(2, posMiddlePoint);
	UVFeatPnts.SetAt(3, posLowerPoint);
	UVFeatPnts.SetAt(no-1, negEarPoint);
	UVFeatPnts.SetAt(no-2, negMiddlePoint);
	UVFeatPnts.SetAt(no-3, negLowerPoint);

	if ( antCrv)
		IwObjDelete(antCrv);
	if ( antCrv2)
		IwObjDelete(antCrv2);
}

//////////////////////////////////////////////////////////
// This function makes the notch arc in a better shape.
//////////////////////////////////////////////////////////
void OutlineProfileMgrModel::ModifyNotchShape
(
	IwTArray<IwPoint3d>& UVFeatPnts,	// I/O:
	IwTArray<int>& UVFeatPntsInfo,		// I/O:
	IwTArray<IwPoint3d>& UVFeatPntsRemap// I/O:
)
{
	// Determine DetermineNotchEighteenWidthPoints()
	DetermineNotchEighteenWidthPoints();
	if ( m_notchEighteenWidthPoints.GetSize() == 0 )
		return;

	// Looking for the notch index
	int notchIndex;
	for (unsigned i=0; i<UVFeatPntsInfo.GetSize(); i++)
	{
		if ( UVFeatPntsInfo.GetAt(i) == 2 )
			notchIndex = i;
		// notchIndex is the notch arc center point
	}

	// get the preferred notch tip
	IwPoint3d notchTip = m_notchEighteenWidthPoints.GetAt(0);

	// project notch tip into sketch UV domain
	IwPoint3d notchTipUV;
	ProjectXYZtoSketchSurfaceUV(notchTip, notchTipUV);

	// we want to create an arc which passes the notchTipUV with preferred radius.
	double refSize = 76.0;
	if ( m_pDoc->GetFemoralAxes() )
		refSize = m_pDoc->GetFemoralAxes()->GetEpiCondyleSize();
	double arcRadius = 8.0;
	if ( refSize > 90 )// Ensure arc diameter not larger than 18.0mm and inversely proportional to femur size
		arcRadius = 5.5;
	else if ( refSize > 80 )
		arcRadius = 6.25;
	else if ( refSize > 70 )
		arcRadius = 7.0;

	// notch arc center, move down radius from notchTipUV
	IwPoint3d arcCenterUV = notchTipUV + arcRadius*IwPoint3d(0, 1, 0);

	// Determine the arc angle by notchIndex-3, notchTipUV, and notchIndex+3
	IwVector3d xVec = IwVector3d(1,0,0);
	// start angle
	IwVector3d tempVec = UVFeatPnts.GetAt(notchIndex-3) - notchTipUV;
	double startAngleRadian;
	xVec.AngleBetween(tempVec, startAngleRadian);
	startAngleRadian = 0.5*IW_PI - startAngleRadian;
	// Determine the arc start point
	IwPoint3d startArcPointUV = arcCenterUV + arcRadius*IwPoint3d(cos(startAngleRadian),-sin(startAngleRadian), 0);
	// end angle
	tempVec = UVFeatPnts.GetAt(notchIndex+3) - notchTipUV;
	double endAngleRadian;
	xVec.AngleBetween(tempVec, endAngleRadian);
	endAngleRadian = endAngleRadian - 0.5*IW_PI;
	// Determine the arc end point
	IwPoint3d endArcPointUV = arcCenterUV + arcRadius*IwPoint3d(-cos(endAngleRadian), -sin(endAngleRadian), 0);

	IwPoint3d arcCenterUV_;
	double radius_;
	IwBSplineCurve* notchArc = CreateArc3(m_pDoc->GetIwContext(), startArcPointUV, notchTipUV, endArcPointUV, arcCenterUV_, radius_);

	// Modify the notch arc
	UVFeatPnts.SetAt(notchIndex-1, startArcPointUV);
	UVFeatPnts.SetAt(notchIndex, arcCenterUV);
	UVFeatPnts.SetAt(notchIndex+1, endArcPointUV);

	/////////////////////////////////////////////////////////////////////////////
	// Modify the notchIndex-2 and notchIndex+2 points (18mm notch width points)
	// sampling arc into points
	IwTArray<IwPoint3d> arcPoints;
	IwExtent1d arcDom = notchArc->GetNaturalInterval();
	ComputeEquallySpacedPoints(notchArc, arcDom.GetMin(), arcDom.GetMax(), 15, arcPoints);

	// use notchArc and -4, -3, +3, +4 to construct a notch curve
	IwTArray<IwPoint3d> notchPointsUV;
	notchPointsUV.Add(UVFeatPnts.GetAt(notchIndex-4));
	notchPointsUV.Add(UVFeatPnts.GetAt(notchIndex-3));
	notchPointsUV.Append(arcPoints);
	notchPointsUV.Add(UVFeatPnts.GetAt(notchIndex+3));
	notchPointsUV.Add(UVFeatPnts.GetAt(notchIndex+4));

	IwBSplineCurve* notchCurve2;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), notchPointsUV, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, notchCurve2);

	IwAxis2Placement wslAxes;
	m_pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);

	// find the closest point from the 18mm width points to the notch curve
	IwPoint3d posEighteenWidthPoint = m_notchEighteenWidthPoints.GetAt(1) - 0.075*wslAxes.GetXAxis();
	// Convert to sketch UV domain
	IwPoint3d posEighteenWidthPointUV;
	ProjectXYZtoSketchSurfaceUV(posEighteenWidthPoint, posEighteenWidthPointUV);

	IwPoint3d closestPoint;
	double param;
	DistFromPointToCurve(posEighteenWidthPointUV, notchCurve2, closestPoint, param);
	tempVec = posEighteenWidthPointUV - closestPoint;
	if ( tempVec.x > 0 )
		UVFeatPnts.SetAt(notchIndex-2, closestPoint);
	else
		UVFeatPnts.SetAt(notchIndex-2, posEighteenWidthPointUV);

	IwPoint3d negEighteenWidthPoint = m_notchEighteenWidthPoints.GetAt(2) + 0.075*wslAxes.GetXAxis();
	// Convert to sketch UV domain
	IwPoint3d negEighteenWidthPointUV;
	ProjectXYZtoSketchSurfaceUV(negEighteenWidthPoint, negEighteenWidthPointUV);

	DistFromPointToCurve(negEighteenWidthPointUV, notchCurve2, closestPoint, param);
	tempVec = negEighteenWidthPointUV - closestPoint;
	if ( tempVec.x < 0 )
		UVFeatPnts.SetAt(notchIndex+2, closestPoint);
	else
		UVFeatPnts.SetAt(notchIndex+2, negEighteenWidthPointUV);

	/////////////////////////////////////////////////////////////////////////////
	// Modify the notchIndex+-3 (1st/2nd chamfer cut edge inner points)
	IwPoint3d relaxPoint;
	int positionIndex;
	bool goodToMove = RelaxOutlineProfileAtPoint(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap, 7, 5.0, false, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		UVFeatPnts.SetAt(positionIndex, relaxPoint);
	}

	goodToMove = RelaxOutlineProfileAtPoint(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap, 8, 5.0, false, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		UVFeatPnts.SetAt(positionIndex, relaxPoint);
	}

	if ( notchCurve2 ) 
		IwObjDelete(notchCurve2);
	if ( notchArc ) 
		IwObjDelete(notchArc);
}

/////////////////////////////////////////////////////////////////////
// Determine the notch tip point and 2 points for 18mm notch width 
// definition. 
// The formula here should be the same as EstimateNotchOutlineProfileFeaturePoints()
/////////////////////////////////////////////////////////////////////
void OutlineProfileMgrModel::DetermineNotchEighteenWidthPoints()
{
	m_notchEighteenWidthPoints.RemoveAll();

	CAdvancedControl* advCtrl = m_pDoc->GetAdvancedControl();

	// Determine notch profile tip point
	// Get trochlear Curve
	//if ( m_pDoc->GetJCurves() == NULL )
		return;
	IwCurve* trochlearCurve = IFemurImplant::JCurvesInp_GetJCurve(2);//m_pDoc->GetJCurves()->GetJCurve(2);//jcurve erik inp
	if ( trochlearCurve == NULL )
		return;
	IwCurve* tCurve;
	// make a copy because we wnat to change tCurve to ReparametrizeWithArcLength()
	trochlearCurve->Copy(m_pDoc->GetIwContext(), tCurve);
	IwBSplineCurve *ttCurve = (IwBSplineCurve *)tCurve;
	ttCurve->ReparametrizeWithArcLength();

	// Get axes 
	IwAxis2Placement wslAxes;
	m_pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
	IwPoint3d tPoint, closestPoint, downPoint, downDownPoint;
	double tParam;
	// get the closest point on trochlear curve first
	DistFromPointToCurve(wslAxes.GetOrigin(), ttCurve, tPoint, tParam);

	// Get femoral size
	double femoralSize = m_pDoc->GetFemoralAxes()->GetEpiCondyleSize();
	// Here we try to map femoralSize [60~90] to overhangDist [5.75~8.75], (hard coded value)
	// The distance is along the free-form outer surface, therefore distance [5.75~8.75] could be very close to 
	// the distance [7~10] in distal view.
	double overhangDist = 5.75 + 3.0*(femoralSize-60.0)/30.0;
	//// Here we try to map EighteenWidthDist, (hard coded value)  
	//double EighteenWidthChord = 3.25 + 4.5*(femoralSize-60.0)/30.0;

	// Move down along trochlear curve with overhangDist distance
	double downParam = tParam - overhangDist;
	ttCurve->EvaluatePoint(downParam, downPoint);
	// downPoint should distant to origin >=7.0mm along Y direction
	// 0.125 as allowance
	double desiredDist = 0.125 + m_pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MIN DISTANCE TO ORIGIN");
	IwVector3d Vec7 = downPoint - wslAxes.GetOrigin();
	double extraParam = desiredDist - Vec7.Dot(-wslAxes.GetYAxis());
	if ( extraParam > 0 )
	{
		overhangDist = overhangDist + 1.5*extraParam;
		ttCurve->EvaluatePoint(tParam - overhangDist, downPoint);
	}

	// get the closest point on sketchSurface
	IwBSplineSurface* sketchSurface = NULL;
	m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
	IwPoint2d downPointParam;
	DistFromPointToSurface(downPoint, sketchSurface, closestPoint, downPointParam);

	IwPoint2d notchTipParam, posEighteenWidthParam, negEighteenWidthParam;

	// get side info 
	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	// 
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		notchTipParam = downPointParam - IwPoint2d(1.0, 0.0);// move to medial side 1.0mm
	}
	else
	{
		positiveSideIsLateral = false;
		notchTipParam = downPointParam - IwPoint2d(-1.0, 0.0);// move to medial side 1.0mm
	}

	IwPoint3d notchTipPointPreferred, posEighteenPoint, negEighteenPoint;
	sketchSurface->EvaluatePoint(notchTipParam, notchTipPointPreferred);

	///////////////////////////////////////////////////////////////
	// Determine 18mm width points
	///////////////////////////////////////////////////////////////
	// Get mechanical axes
	IwAxis2Placement axes120, mechanicalAxes, sweepAxes;
	m_pDoc->GetFemoralAxes()->GetMechanicalAxes(mechanicalAxes);
	m_pDoc->GetFemoralAxes()->GetMechanicalAxes(sweepAxes);
	// rotate -30 degrees
	double degree30Radian = 30.0/180.0*IW_PI;
	IwAxis2Placement rotMat;
	rotMat.RotateAboutAxis(-degree30Radian, IwVector3d(1,0,0));
	rotMat.TransformAxis2Placement(mechanicalAxes, axes120);

	IwVector3d vec120 = -axes120.GetZAxis();
	// Need to use the algorithm in ValidateNotchProfile()
	// get sketch profile
	IwPoint3d farDownPoint = sweepAxes.GetOrigin() + 2500*vec120;
	IwBSplineCurve* sketchProfile = CDefineOutlineProfile::DetermineSketchProfile(m_pDoc);
	IwPoint3d point120;
	double param;
	DistFromPointToCurve(farDownPoint, sketchProfile, point120, param);

	// project onto sketch UV domain
	IwVector2d point120Param;
	DistFromPointToSurface(point120, sketchSurface, closestPoint, point120Param);
	// use the notchTipParam.x as U and point120Param.y as V for the point120Param
	point120Param = IwVector2d(notchTipParam.x, point120Param.y);
	posEighteenWidthParam = point120Param - IwPoint2d(-9.0, 0.0);
	negEighteenWidthParam = point120Param - IwPoint2d(9.0, 0.0);
	// convert to xyz space
	sketchSurface->EvaluatePoint(posEighteenWidthParam, posEighteenPoint);
	sketchSurface->EvaluatePoint(negEighteenWidthParam, negEighteenPoint);

	// Determine the pointa at 7/12mm limitation
	double dist7 = m_pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MIN DISTANCE TO ORIGIN");
	double dist12 = m_pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MAX DISTANCE TO ORIGIN");
	IwPoint3d pnt7 = wslAxes.GetOrigin() - dist7*wslAxes.GetYAxis();
	IwPoint3d pnt12 = wslAxes.GetOrigin() - dist12*wslAxes.GetYAxis();
	IwPoint3d cPnt;
	IntersectCurveByPlane(ttCurve, pnt7, wslAxes.GetYAxis(), cPnt);
	double uv[2];
	IntersectSurfaceByLine(sketchSurface, cPnt, wslAxes.GetZAxis(), cPnt, uv); 
	IwPoint2d intPnt7 = IwPoint2d(uv[0], uv[1]);
	if ( positiveSideIsLateral )
		intPnt7 = intPnt7 - IwPoint2d(1.0, 0.0);// move to medial side 1.0mm
	else
		intPnt7 = intPnt7 - IwPoint2d(-1.0, 0.0);// move to lateral side 1.0mm
	IwPoint3d notchTipPoint7, notchTipPoint12;
	sketchSurface->EvaluatePoint(intPnt7, notchTipPoint7);
	// for 12mm point
	IntersectCurveByPlane(ttCurve, pnt12, wslAxes.GetYAxis(), cPnt);
	IntersectSurfaceByLine(sketchSurface, cPnt, wslAxes.GetZAxis(), cPnt, uv); 
	IwPoint2d intPnt12 = IwPoint2d(uv[0], uv[1]);
	if ( positiveSideIsLateral )
		intPnt12 = intPnt12 - IwPoint2d(1.0, 0.0);// move to medial side 1.0mm
	else
		intPnt12 = intPnt12 - IwPoint2d(-1.0, 0.0);// move to lateral side 1.0mm
	sketchSurface->EvaluatePoint(intPnt12, notchTipPoint12);

	// output
	m_notchEighteenWidthPoints.Add(notchTipPointPreferred);
	m_notchEighteenWidthPoints.Add(posEighteenPoint);
	m_notchEighteenWidthPoints.Add(negEighteenPoint);
	m_notchEighteenWidthPoints.Add(notchTipPoint7);
	m_notchEighteenWidthPoints.Add(notchTipPoint12);

	if ( tCurve )
		IwObjDelete(tCurve);
	if ( sketchProfile )
		IwObjDelete(sketchProfile);
}

bool OutlineProfileMgrModel::RelaxOutlineProfileAtPoint
(
	IwTArray<IwPoint3d>& UVFeatPnts,	// I:
	IwTArray<int>& UVFeatPntsInfo,		// I:
	IwTArray<IwPoint3d>& UVFeatPntsRemap,// I:
	double pointIndex,					// I: indicates which edge point
	double maxDistToMove,				// I:
	bool bothSideToMove,				// I:
	IwPoint3d& relaxPoint,				// O: output point which relaxes the outline profile
	int& positionIndex					// O: indicate the corresponding position index of the pointIndex in UVFeatPnts array
)
{
	positionIndex = -1;
	// Convert the pointIndex (edge point index) to the position index 
	for (unsigned i=0; i<UVFeatPntsRemap.GetSize(); i++)
	{
		// on Remap (_,_,z) = reference edge point position
		double edgeIndexI = UVFeatPntsRemap.GetAt(i).z;
		if ( edgeIndexI >= 0 )// > -1
		{
			if ( IS_EQ_TOL6(edgeIndexI, pointIndex) )
			{
				if ( UVFeatPntsInfo.GetAt(i) == 0 )// this point has to be an interpolation point (cannot be an arc point)
				{
					positionIndex = i;
					break;
				}
			}
		}
	} 
	// if could not find the position index, return;
	if ( positionIndex < 0 )
		return false;

	// the edge point position (in uv domain)
	IwPoint3d originalEdgePoint = UVFeatPnts.GetAt(positionIndex);
	IwPoint3d originalEdgePointRemap = IwPoint3d(UVFeatPntsRemap.GetAt(positionIndex).x, UVFeatPntsRemap.GetAt(positionIndex).y, 0);

	COutlineProfile* pOutlineProfile = m_pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		return false;

	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return false;
	IwAxis2Placement sweepAxes;
	pFemoralAxes->GetSweepAxes(sweepAxes);

	// Create a curve on sketch surface without using the position index point
	// remove the positionIndex point from the UVFeatPnts & UVFeatPntsInfo
	IwTArray<IwPoint3d> UVFeatPntsRelax;
	UVFeatPntsRelax.Append(UVFeatPnts);
	UVFeatPntsRelax.RemoveAt(positionIndex, 1);
	IwTArray<int> UVFeatPntsInfoRelax;
	UVFeatPntsInfoRelax.Append(UVFeatPntsInfo);
	UVFeatPntsInfoRelax.RemoveAt(positionIndex, 1);

	IwBSplineCurve *crvOnSurf=NULL, *uvCurve=NULL;
	pOutlineProfile->CreateCurveOnSketchSurface(UVFeatPntsRelax, UVFeatPntsInfoRelax, false, crvOnSurf, uvCurve);

	IwPoint3d cPnt;
	double param;
	double dist = DistFromPointToCurve(originalEdgePoint, uvCurve, cPnt, param);
	if ( dist > 10.0 )// something wrong
		return false;

	// When relax a point and bothSideToMove=false, only move the point outward to provide more coverage
	int indexInteger = (int)floor(pointIndex);
	bool isEdgePoint = !IS_EQ_TOL3(indexInteger-pointIndex, 0.0);
	IwPoint3d outwardDir = IwPoint3d(0,0,0);
	if ( indexInteger == 2 || 
		 indexInteger == 4 ||
		 indexInteger == 6 ||
		 indexInteger == 8 ||
		 indexInteger == 10 ||
		 indexInteger == 12 ||
		 indexInteger == 14 ||
		 indexInteger == 16 ||
		 indexInteger == 18 )
		 outwardDir = sweepAxes.GetXAxis();
	else if ( indexInteger == 3 || 
		 indexInteger == 5 ||
		 indexInteger == 7 ||
		 indexInteger == 9 ||
		 indexInteger == 11 ||
		 indexInteger == 13 ||
		 indexInteger == 15 ||
		 indexInteger == 17 ||
		 indexInteger == 19 )
		 outwardDir = -sweepAxes.GetXAxis();

	IwPoint3d tempVec = cPnt - originalEdgePoint;
	tempVec = tempVec.ProjectToPlane(sweepAxes.GetYAxis());
	tempVec = tempVec.ProjectToPlane(sweepAxes.GetZAxis());
	// after project to Y and Z planes, now tempVec is along x-axis (ML) of sketch surface.
	tempVec.Unitize();
	if ( tempVec.Dot(outwardDir) > 0 )// 1. if move outward, always make it more conservative.
	{												
		if ( maxDistToMove > 0.45 )
			maxDistToMove = maxDistToMove - 0.45;
	}
	else if (isEdgePoint)// 2. if is an edge point, move less.
	{
		maxDistToMove = 0.25;
	}

	if ( dist > maxDistToMove)
		dist = maxDistToMove;

	if ( tempVec.Dot(outwardDir) > 0 || bothSideToMove )
		relaxPoint = originalEdgePoint + originalEdgePointRemap + dist*tempVec;
	else
		relaxPoint = originalEdgePoint + originalEdgePointRemap;

	if ( crvOnSurf )
		IwObjDelete(crvOnSurf);
	if ( uvCurve )
		IwObjDelete(uvCurve);

	return true;
}

bool OutlineProfileMgrModel::ConvertToArc
(
	int index,									// I:
	IwTArray<IwPoint3d>& featurePoints,			// I/O: 
	IwTArray<int>& featurePointsInfo,			// I/O:
	IwTArray<IwPoint3d>& featurePointsRemap,	// I/O:
	double minRadius							// I:
)
{
	int statusInfo = featurePointsInfo.GetAt(index);
	if ( statusInfo != 0 ) // only interpolation points (and their nearby curves) can be converted to arcs
		return false;
	// can not convert the first and last points (seam point) into an arc.
	if ( index == 0 || index == (featurePointsInfo.GetSize()-1) )
		return false;

	// Only the non remap-able points can be converted.
	int zValue = (int)featurePointsRemap.GetAt(index).z;
	if ( zValue != -1 ) // (,,-1) indicates it is not a remap-able point.
		return false;

	COutlineProfile* pOutlineProfile = m_pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		return false;
	// Get UVCurve
	IwBSplineCurve *crvOnSurf, *UVCurve;
	pOutlineProfile->GetCurveOnSketchSurface(crvOnSurf, UVCurve, false);
	if ( UVCurve == NULL )
		return false;
	IwExtent1d tDom = UVCurve->GetNaturalInterval();

	// get the previous, current, and post points in UV domain.
	IwPoint3d prevPoint, postPoint, currPoint;
	currPoint = featurePoints.GetAt(index);
	prevPoint = featurePoints.GetAt(index-1);
	postPoint = featurePoints.GetAt(index+1);
	// determine these 3 points on UVCurve parametric domain
	double prevParam, postParam, currParam;
	IwPoint3d cPnt;
	DistFromPointToCurve(prevPoint, UVCurve, cPnt, prevParam);
	if ( index == 1 )// modify prevParam since DistFromPointToCurve() may return either end parameter
		prevParam = tDom.GetMin();
	DistFromPointToCurve(currPoint, UVCurve, cPnt, currParam);
	DistFromPointToCurve(postPoint, UVCurve, cPnt, postParam);
	if ( index == (featurePointsInfo.GetSize()-2) )// modify prevParam since DistFromPointToCurve() may return either end parameter
		postParam = tDom.GetMax();

	double prevMidParam = 0.5*(prevParam+currParam);
	double postMidParam = 0.5*(postParam+currParam);
	IwVector3d prevVec = prevPoint - currPoint;
	prevVec.Unitize();
	IwVector3d postVec = postPoint - currPoint;
	postVec.Unitize();
	// search the point on UVCurve where tangent is parallel to the prevVec
	IwPoint3d prevMinUV;
	IwVector3d crossVec;
	double crossValue, minCrossValue = 100000;
	double deltaParam = (prevMidParam - currParam)/25.0;
	double param;
	IwVector3d evals[2];
	IwVector3d tangentVec;
	for (int step=0; step<25; step++)
	{
		param = currParam + step*deltaParam;
		UVCurve->Evaluate(param, 1, FALSE, evals);
		tangentVec = evals[1];
		tangentVec.Unitize();
		crossVec = tangentVec*prevVec;
		crossValue = crossVec.Length();
		if ( crossValue < minCrossValue )
		{
			prevMinUV = evals[0];
			minCrossValue = crossValue;
		}
	}
	// For post side
	IwPoint3d postMinUV;
	minCrossValue = 100000;
	deltaParam = (postMidParam - currParam)/25.0;
	for (int step=0; step<25; step++)
	{
		param = currParam + step*deltaParam;
		UVCurve->Evaluate(param, 1, FALSE, evals);
		tangentVec = evals[1];
		tangentVec.Unitize();
		crossVec = tangentVec*postVec;
		crossValue = crossVec.Length();
		if ( crossValue < minCrossValue )
		{
			postMinUV = evals[0];
			minCrossValue = crossValue;
		}
	}
	// Use prevMinUV, currPoint, and postMinUV to create an arc
	IwPoint3d arcCenter;
	double radius;
	prevVec = currPoint - prevPoint;
	postVec = postPoint - currPoint;
	crossVec = prevVec*postVec;
	int arcCenterInfo;
	if ( crossVec.z > 0 )
		arcCenterInfo = 11;// CCW
	else
		arcCenterInfo = 12;// CW
	IwBSplineCurve* arc = CreateArc3(m_pDoc->GetIwContext(), prevMinUV, currPoint, postMinUV, arcCenter, radius);

	IwPoint2d arcCenter2d = IwPoint2d(arcCenter.x, arcCenter.y);
	IwExtent2d uvDomain = pOutlineProfile->GetSketchSurfaceUVDomain();

	IwVector3d sVec = prevMinUV - arcCenter;
	IwVector3d eVec = postMinUV - arcCenter;
	double arcAngleRadian;
	sVec.AngleBetween(eVec, arcAngleRadian);
	// Check if the center is out of sketch domain (cannot display center) or
	// radius is too big (does not worth to use arcs) or
	// angle is too small < 15 degrees (does not worth to use arcs)
	if ( !uvDomain.ContainsPoint2d(arcCenter2d) || radius > 30.0 || arcAngleRadian<(15.0/180.0*IW_PI) )
	{
		if (arc)
			IwObjDelete(arc);
		return false;
	}
	// Check if the radius is too small
	if ( radius < minRadius )
	{
		IwPoint3d midPnt = 0.5*(prevMinUV + postMinUV);
		IwVector3d VecM2Center = arcCenter - midPnt;
		VecM2Center.Unitize();
		double HalfChord = radius*sin(0.5*arcAngleRadian);
		double height = sqrt(minRadius*minRadius - HalfChord*HalfChord);
		arcCenter = midPnt + height*VecM2Center;
	}

	// However, the arc in the prevMinUV and postMinUV is not necessarily tangent to the UVCurve.
	// the prevMinUV and postMinUV need to be adjusted to the place where is tangent to prevVec and postVec
	tDom = arc->GetNaturalInterval();
	double minCrossValue0 = 100000, minCrossValue1 = 100000;
	deltaParam = tDom.GetLength()/50.0;
	for (int step=0; step<51; step++)
	{
		param = tDom.GetMin() + step*deltaParam;
		arc->Evaluate(param, 1, FALSE, evals);
		tangentVec = evals[1];
		tangentVec.Unitize();
		crossVec = tangentVec*prevVec;
		crossValue = crossVec.Length();
		if ( crossValue < minCrossValue0 )
		{
			prevMinUV = evals[0];
			minCrossValue0 = crossValue;
		}
		crossVec = tangentVec*postVec;
		crossValue = crossVec.Length();
		if ( crossValue < minCrossValue1 )
		{
			postMinUV = evals[0];
			minCrossValue1 = crossValue;
		}
	}

	// replace featurePoints.GetAt(index) by prevMinUV, arcCenter, and postMinUV
	// remove first
	featurePoints.RemoveAt(index);
	featurePointsInfo.RemoveAt(index);
	featurePointsRemap.RemoveAt(index);
	// Insert prevMinUV, arcCenter, and postMinUV
	// **** Notice the sequence ****
	featurePoints.InsertAt(index, postMinUV);// arc end point
	featurePointsInfo.InsertAt(index, 4);// arc end point
	featurePointsRemap.InsertAt(index, IwPoint3d(0,0,-1));// arc end point

	featurePoints.InsertAt(index, arcCenter);// arc center point
	featurePointsInfo.InsertAt(index, arcCenterInfo);// arc center point
	featurePointsRemap.InsertAt(index, IwPoint3d(0,0,-1));// arc center point

	featurePoints.InsertAt(index, prevMinUV);// arc start point
	featurePointsInfo.InsertAt(index, 3);// arc start point
	featurePointsRemap.InsertAt(index, IwPoint3d(0,0,-1));// arc start point

	if (arc)
		IwObjDelete(arc);

	return true;
}

void OutlineProfileMgrModel::DetermineInitialOutlineProfile() 
{
	IwTArray<IwPoint3d> idealUVFeatPnts;
	IwTArray<int> idealUVFeatPntsInfo;
	IwTArray<IwPoint3d> idealUVFeatPntsRemap;

	bool rOK = ConvertCutFeatPntsToOutlineFeatPnts(m_cutFeatPnts, m_antOuterPnts, NULL /*&m_edgeMiddlePoints*/, idealUVFeatPnts, idealUVFeatPntsInfo, idealUVFeatPntsRemap);
	if ( !rOK ) return;

	IwTArray<IwPoint3d> UVFeatPnts;
	IwTArray<int> UVFeatPntsInfo;
	IwTArray<IwPoint3d> UVFeatPntsRemap;
	UVFeatPnts.Append(idealUVFeatPnts);
	UVFeatPntsInfo.Append(idealUVFeatPntsInfo);
	UVFeatPntsRemap.Append(idealUVFeatPntsRemap);

	// Set to outline profile to create sketch surface and outline UVCurve.
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);

	/////////////////////////////////////////////////////
	// Modify outline profile
	/////////////////////////////////////////////////////

	// We need to modify the shape near anterior.
	ModifyAnteriorShape(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);

	// We need to modify the shape near notch.
	ModifyNotchShape(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);

	// We need to add two arcs in the two anterior ears.
	// This can only be doen after having called SetOutlineProfileFeaturePoints()
	// to create outline UVCurve 
	int index;
	if ( COutlineProfile::IsAsymmetricEarShaped(m_pDoc) ) // For AsymmetricEarShaped, only convert lateral ear to an arc.
	{
		if ( m_pDoc->IsPositiveSideLateral() )
			index = 1;
		else
			index = UVFeatPnts.GetSize() - 2;
		ConvertToArc(index, UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
	}
	else // For symmetricEarShaped, convert 2 ears to arcs.
	{
		index = 1;
		ConvertToArc(index, UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
		index = UVFeatPnts.GetSize() - 2;
		ConvertToArc(index, UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
	}

	// Set to outline profile again to reflect the new UVFeatPnts, UVFeatPntsInfo
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
	// Get the current data
	m_pDoc->GetOutlineProfile()->GetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
}

void OutlineProfileMgrModel::UpdateValidationPoints()
{
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePointsValidation(m_currFeaturePoints, m_currFeaturePointsInfo);
}

/////////////////////////////////////////////////////////////////
// This function will remap the control points on the cut edges.
// New position = new cut edge location + remap(x,y,_)
// Also reset the posterior profiles, and adjust the 18mm notch 
// profile.
// If manActType == MAN_ACT_TYPE_REFINE, refine to the ideal outline profile based on the cut profile
// if MAN_ACT_TYPE_EDIT, make the ideal profile but let condylar profile center to jcurves if possible.
// if MAN_ACT_TYPE_REFINE_CONVERGE, only update profile on cut edge points and satisfy minimum thickness. 
/////////////////////////////////////////////////////////////////
void OutlineProfileMgrModel::Refine(CManagerActivateType manActType)
{
	if ( manActType == MAN_ACT_TYPE_REFINE_CONVERGE )
		Refine_Converge(manActType);
	else if (manActType == MAN_ACT_TYPE_REFINE)
		Refine_Refine(manActType);
	else if (manActType == MAN_ACT_TYPE_EDIT)
	{
		double allowableDistMax = m_pDoc->GetVarTableValue("OUTLINE PROFILE MAX DISTANCE TO CUT EDGE");
		double allowableDistMin = m_pDoc->GetVarTableValue("OUTLINE PROFILE MIN DISTANCE TO CUT EDGE");
		IwTArray<IwPoint3d> unQualifiedLocations;
		IwTArray<double> unQualifiedDistances;
		IwTArray<int> overhangings;

		if ( ValidateDistanceToBothPosteriorCutProfiles(m_pDoc, allowableDistMax, allowableDistMin, unQualifiedLocations, unQualifiedDistances, overhangings) )
			Refine_Converge(manActType);// if both posterior cut profile are good, just converge it.
		else
			Refine_Refine(manActType);// Need significant refine.
	}
}

////////////////////////////////////////////////////////////////////////////
// Refine_Refine shall move the outline profile to the ideal position. 
void OutlineProfileMgrModel::Refine_Refine(CManagerActivateType manActType)
{

	bool activateUI = (manActType == MAN_ACT_TYPE_EDIT);

	// Get the femoral axes
	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL ) return;

	IwAxis2Placement wslAxes;
	pFemoralAxes->GetWhiteSideLineAxes(wslAxes);

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral = (implantSide == IMPLANT_SIDE_RIGHT);

	// Resample feature points first
	ResampleFeaturePoints();
	//
	RefineAnteriorOuterPoints(m_cutFeatPnts, m_antOuterPnts);

	IwTArray<IwPoint3d> idealUVFeatPnts;
	IwTArray<int> idealUVFeatPntsInfo;
	IwTArray<IwPoint3d> idealUVFeatPntsRemap;
	bool rOK = ConvertCutFeatPntsToOutlineFeatPnts(m_cutFeatPnts, m_antOuterPnts, &m_edgeMiddlePoints, idealUVFeatPnts, idealUVFeatPntsInfo, idealUVFeatPntsRemap);
	if ( !rOK ) return;

	////////////////////////////////////////////////////////////////////////////////
	// Remap the control points on the cut edges and along the edges (middle points) 
	for (unsigned i=0; i<m_currFeaturePointsRemap.GetSize(); i++)
	{
		// on Remap (_,_,z) = reference edge point position
		double edgeIndexI = m_currFeaturePointsRemap.GetAt(i).z;

		if ( edgeIndexI >= 0 )// (,,-1) indicates it is not a remap-able point
		{
			// Skip for the condylar inner points Points 7, 8, 11 & 12. Their positions are not related to cut edge vertexes.
			if ( IS_EQ_TOL6(edgeIndexI, 7.0) || IS_EQ_TOL6(edgeIndexI, 8.0) ||
				 IS_EQ_TOL6(edgeIndexI, 11.0) || IS_EQ_TOL6(edgeIndexI, 12.0) )
				 continue;
			for (unsigned j=0; j<idealUVFeatPntsRemap.GetSize(); j++)
			{
				double edgeIndexJ = idealUVFeatPntsRemap.GetAt(j).z;
				if ( IS_EQ_TOL6(edgeIndexI, edgeIndexJ) )
				{
					// UVFeatPnts (x,y,_) = (U,V,_) position on sketch surface
					// m_currFeaturePointsRemap (x,y,_) = delta (U,V,_) to map
					double U = idealUVFeatPnts.GetAt(j).x + m_currFeaturePointsRemap.GetAt(i).x;
					double V = idealUVFeatPnts.GetAt(j).y + m_currFeaturePointsRemap.GetAt(i).y;
					IwPoint3d UVPoint = IwPoint3d(U,V,0);
					m_currFeaturePoints.SetAt(i, UVPoint);
					break;
				}
			}
		}
	} 

	////////////////////////////////////////////////////////////////////
	// Here replace the posterior profile
	int indexForCutEdge2=-1, indexForCutEdge3=-1, indexForCutEdge4=-1, indexForCutEdge5=-1;
	for (unsigned i=0; i<idealUVFeatPntsRemap.GetSize(); i++)
	{
		// on Remap (_,_,z) = reference edge point position
		double edgeIndexI = idealUVFeatPntsRemap.GetAt(i).z;
		if ( IS_EQ_TOL6(edgeIndexI, 2) )
			indexForCutEdge2 = i;
		else if ( IS_EQ_TOL6(edgeIndexI, 3) )
			indexForCutEdge3 = i;
		else if ( IS_EQ_TOL6(edgeIndexI, 4) )
			indexForCutEdge4 = i;
		else if ( IS_EQ_TOL6(edgeIndexI, 5) )
			indexForCutEdge5 = i;
	}
	int indexForCurrCutEdge2=-1, indexForCurrCutEdge3=-1;
	for (unsigned i=0; i<m_currFeaturePointsRemap.GetSize(); i++)
	{
		// on Remap (_,_,z) = reference edge point position
		double edgeIndexI = m_currFeaturePointsRemap.GetAt(i).z;
		if ( IS_EQ_TOL6(edgeIndexI, 2) )
			indexForCurrCutEdge2 = i;
		else if ( IS_EQ_TOL6(edgeIndexI, 3) )
			indexForCurrCutEdge3 = i;
	}
	if ( indexForCurrCutEdge2 != -1 && indexForCurrCutEdge3 != -1 &&
		 indexForCutEdge2 != -1 && indexForCutEdge3 != -1 )
	{
		// remove the curr data m_currFeaturePoints m_currFeaturePointsInfo m_currFeaturePointsRemap
		for (int i=(indexForCurrCutEdge2+1); i<indexForCurrCutEdge3; i++)
		{
			m_currFeaturePoints.RemoveAt(indexForCurrCutEdge2+1);
			m_currFeaturePointsInfo.RemoveAt(indexForCurrCutEdge2+1);
			m_currFeaturePointsRemap.RemoveAt(indexForCurrCutEdge2+1);
		}
		for (int i=(indexForCutEdge3-1); i>indexForCutEdge2; i--)
		{
			m_currFeaturePoints.InsertAt(indexForCurrCutEdge2+1, idealUVFeatPnts.GetAt(i));
			m_currFeaturePointsInfo.InsertAt(indexForCurrCutEdge2+1, idealUVFeatPntsInfo.GetAt(i));
			m_currFeaturePointsRemap.InsertAt(indexForCurrCutEdge2+1, idealUVFeatPntsRemap.GetAt(i));
		}
	}
	// update index 
	int indexForCurrCutEdge4=-1, indexForCurrCutEdge5=-1;
	for (unsigned i=0; i<m_currFeaturePointsRemap.GetSize(); i++)
	{
		// on Remap (_,_,z) = reference edge point position
		double edgeIndexI = m_currFeaturePointsRemap.GetAt(i).z;
		if ( IS_EQ_TOL6(edgeIndexI, 4) )
			indexForCurrCutEdge4 = i;
		else if ( IS_EQ_TOL6(edgeIndexI, 5) )
			indexForCurrCutEdge5 = i;
	}
	if ( indexForCurrCutEdge4 != -1 && indexForCurrCutEdge5 != -1 &&
		 indexForCutEdge4 != -1 && indexForCutEdge5 != -1 )
	{
		// remove the curr data m_currFeaturePoints m_currFeaturePointsInfo m_currFeaturePointsRemap
		for (int i=(indexForCurrCutEdge4+1); i<indexForCurrCutEdge5; i++)
		{
			m_currFeaturePoints.RemoveAt(indexForCurrCutEdge4+1);
			m_currFeaturePointsInfo.RemoveAt(indexForCurrCutEdge4+1);
			m_currFeaturePointsRemap.RemoveAt(indexForCurrCutEdge4+1);
		}
		for (int i=(indexForCutEdge5-1); i>indexForCutEdge4; i--)
		{
			m_currFeaturePoints.InsertAt(indexForCurrCutEdge4+1, idealUVFeatPnts.GetAt(i));
			m_currFeaturePointsInfo.InsertAt(indexForCurrCutEdge4+1, idealUVFeatPntsInfo.GetAt(i));
			m_currFeaturePointsRemap.InsertAt(indexForCurrCutEdge4+1, idealUVFeatPntsRemap.GetAt(i));
		}
	}

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
	
	/////////////////////////////////////////////////////////////////
	// Check anterior overhanging in the lateral side 
	RefineByValidation_LateralAnteriorEar();
	
	/////////////////////////////////////////////////////////////////
	// Check asymmetric ear in the medial side 
	RefineByValidation_MedialAnteriorEar();

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
	
	/////////////////////////////////////////////////////////////////////////////
	double cutToEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");
	IwPoint3d relaxPoint;
	int positionIndex;
	bool goodToMove;
	// Relatx anterior cut edge points
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 18.0, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 19.0, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	// Relax the anterior side points
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 18.25, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 19.25, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	// Relatx anterior chamfer side points
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 14.75, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 14.25, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 14.5, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 17.75, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 17.25, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 17.5, 1.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	// Relax the anterior higher side points
	double posMaxDistToMove, negMaxDistToMove;
	if ( positiveSideIsLateral )
	{
		posMaxDistToMove = 1.0; // lateral side profile still needs to follow bone cut
		negMaxDistToMove = 3.0; // medial side profile does not need to follow bone cut
	}
	else
	{
		posMaxDistToMove = 3.0; // medial side profile does not need to follow bone cut
		negMaxDistToMove = 1.0; // lateral side profile still needs to follow bone cut
	}

	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 18.5, posMaxDistToMove, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 19.5, negMaxDistToMove, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	// Relax the posterior points 3 & 4 
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 3.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		if ( 0 && m_pDoc->isPosteriorStabilized() && !activateUI ) // PS
		{
			// project to posRefPnt, but in sketch surface domain
			IwPoint3d posRefPnt, negRefPnt;
			bool bHasRef = m_pDoc->GetOutlineProfile()->GetBoxCutReferencePoints(posRefPnt, negRefPnt);
			IwBSplineSurface* sketchSurface = NULL;
			m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			if ( bHasRef && sketchSurface != NULL )
			{
				IwPoint3d cPnt, param;
				IwPoint2d uvParam;
				DistFromPointToSurface(posRefPnt, sketchSurface, cPnt, uvParam);
				param = IwPoint3d(uvParam.x, uvParam.y, relaxPoint.z);
				IwVector3d vec = relaxPoint - param;
//				if ( vec.Dot(IwVector3d(1,0,0)) > 0 )// relaxPoint is in outer side of box cut wall
					relaxPoint = relaxPoint.ProjectPointToPlane(param, IwVector3d(1,0,0));
			}
		}
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 4.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		if ( 0 && m_pDoc->isPosteriorStabilized() && !activateUI ) // PS
		{
			// project to negRefPnt, but in sketch surface domain
			IwPoint3d posRefPnt, negRefPnt;
			bool bHasRef = m_pDoc->GetOutlineProfile()->GetBoxCutReferencePoints(posRefPnt, negRefPnt);
			IwBSplineSurface* sketchSurface = NULL;
			m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			if ( bHasRef && sketchSurface != NULL )
			{
				IwPoint3d cPnt, param;
				IwPoint2d uvParam;
				DistFromPointToSurface(negRefPnt, sketchSurface, cPnt, uvParam);
				param = IwPoint3d(uvParam.x, uvParam.y, relaxPoint.z);
				IwVector3d vec = relaxPoint - param;
//				if ( vec.Dot(IwVector3d(1,0,0)) < 0 )// relaxPoint is in outer side of box cut wall
					relaxPoint = relaxPoint.ProjectPointToPlane(param, IwVector3d(1,0,0));
			}
		}
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	// handle 2, 6, 10 together
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 2.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 6.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 10.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 10.5, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	// handle 5, 9, 13 together
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 5.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 9.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 13.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 13.5, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	// 14 & 17
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 14.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 17.0, cutToEdge, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	/////////////////////////////////////////////////////////////////
	// Update notch profile at 18mm gap

	// Relax both 18mm control points
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 11.0, 5.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 12.0, 5.0, true, relaxPoint, positionIndex);
	if ( goodToMove )
	{
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	////////////////////////////////////////////////////////////////
	// Refine notch profile to satisfy 7~12mm to origin and 18mm width
	RefineNotchProfile();

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	////////////////////////////////////////////////////
	// Relax the condylar inner points after the notch 18mm points are updated.
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 7.0, 5.0, true, relaxPoint, positionIndex);// condylar inner point can move freely.
	if ( goodToMove )
	{
		if ( !m_pDoc->isPosteriorStabilized() ) // CR
		{
			if ( activateUI )
			{
				IwPoint3d existingPnt = m_currFeaturePoints.GetAt(positionIndex);
				IwVector3d vec = relaxPoint - existingPnt;
				if ( vec.Dot(wslAxes.GetXAxis()) < 0 /*make implant wider*/ || fabs(vec.Dot(wslAxes.GetXAxis())) < 0.4/*move slightly*/ )
				{  
					// if activateUI, the existing point is already close to the relaxPoint, skip it.
					relaxPoint = existingPnt;
				}
			}
		}
		else // PS
		{
			if ( 0 && !activateUI )
			{
				// project to posRefPnt, but in sketch surface domain
				IwPoint3d posRefPnt, negRefPnt;
				bool bHasRef = m_pDoc->GetOutlineProfile()->GetBoxCutReferencePoints(posRefPnt, negRefPnt);
				IwBSplineSurface* sketchSurface = NULL;
				m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
				if ( bHasRef && sketchSurface != NULL )
				{
					IwPoint3d cPnt, param;
					IwPoint2d uvParam;
					DistFromPointToSurface(posRefPnt, sketchSurface, cPnt, uvParam);
					param = IwPoint3d(uvParam.x, uvParam.y, relaxPoint.z);
					IwVector3d vec = relaxPoint - param;
//					if ( vec.Dot(IwVector3d(1,0,0)) > 0 )// relaxPoint is in outer side of box cut wall
						relaxPoint = relaxPoint.ProjectPointToPlane(param, IwVector3d(1,0,0));
				}
			}
		}
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	goodToMove = RelaxOutlineProfileAtPoint(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, 8.0, 5.0, true, relaxPoint, positionIndex);// condylar inner point can move freely.
	if ( goodToMove )
	{
		if ( !m_pDoc->isPosteriorStabilized() )// CR
		{
			if ( activateUI )
			{
				IwPoint3d existingPnt = m_currFeaturePoints.GetAt(positionIndex);
				IwVector3d vec = relaxPoint - existingPnt;
				if ( vec.Dot(wslAxes.GetXAxis()) > 0 /*make implant wider*/ || fabs(vec.Dot(wslAxes.GetXAxis())) < 0.4/*move slightly*/ )
				{  
					// if activateUI and the existing point is already close to the relaxPoint, skip it.
					relaxPoint = existingPnt;
				}
			}
		}
		else // PS
		{
			if ( 0 && !activateUI )
			{
				// project to negRefPnt, but in sketch surface domain
				IwPoint3d posRefPnt, negRefPnt;
				bool bHasRef = m_pDoc->GetOutlineProfile()->GetBoxCutReferencePoints(posRefPnt, negRefPnt);
				IwBSplineSurface* sketchSurface = NULL;
				m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
				if ( bHasRef && sketchSurface != NULL )
				{
					IwPoint3d cPnt, param;
					IwPoint2d uvParam;
					DistFromPointToSurface(negRefPnt, sketchSurface, cPnt, uvParam);
					param = IwPoint3d(uvParam.x, uvParam.y, relaxPoint.z);
					IwVector3d vec = relaxPoint - param;
//					if ( vec.Dot(IwVector3d(1,0,0)) < 0 )// relaxPoint is in outer side of box cut wall
						relaxPoint = relaxPoint.ProjectPointToPlane(param, IwVector3d(1,0,0));
				}
			}
		}
		m_currFeaturePoints.SetAt(positionIndex, relaxPoint);
	}
	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	// Run validation to see any further modification is needed.
	RefineByValidation_Overall();

	// update data for validation
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePointsValidation(m_currFeaturePoints, m_currFeaturePointsInfo);
}

bool OutlineProfileMgrModel::RefineByValidation_Overall()
{
	IwVector3d xDir = IwVector3d(1,0,0);

	COutlineProfile* pOutlineProfile = m_pDoc->GetOutlineProfile();

	double allowableDistMin = m_pDoc->GetVarTableValue("OUTLINE PROFILE MIN DISTANCE TO CUT EDGE");
	double allowableDistMax = m_pDoc->GetVarTableValue("OUTLINE PROFILE MAX DISTANCE TO CUT EDGE");
	double allowableDistMaxPreferred = allowableDistMax - 0.25;
	IwTArray<IwPoint3d> thisUnQualifiedLocations;
	IwTArray<double> thisUnQualifiedDistances;
	IwTArray<int> thisOverhangings;

	Validation::OutlineProfile::ValidateDistanceToCutProfile(m_pDoc, allowableDistMax, allowableDistMin, thisUnQualifiedLocations, thisUnQualifiedDistances, thisOverhangings);
	if ( thisUnQualifiedLocations.GetSize() == 0 )
		return false;

	// Get femoral cuts information
	CFemoralCuts*		femoralCuts = m_pDoc->GetFemoralCuts();
	if (femoralCuts == NULL) return false;
	// Get feature points at vertexes
	IwTArray<IwPoint3d> cutFeatPntsAtVertexes;
	femoralCuts->GetFeaturePointsAtVertexes(cutFeatPntsAtVertexes);

	double effectDistance = 10.0;

	double moveDist2=0, moveDist3=0, moveDist4=0, moveDist5=0
		 , moveDist6=0, moveDist9=0, moveDist10=0, moveDist13=0
		 , moveDist1025=0, moveDist105=0, moveDist1075=0
		 , moveDist1325=0, moveDist135=0, moveDist1375=0
		 , moveDist14=0, moveDist17=0
		 , moveDist1425=0, moveDist145=0, moveDist1475=0
		 , moveDist1725=0, moveDist175=0, moveDist1775=0
		 , moveDist18=0, moveDist19=0;

	bool move2=false, move3=false, move4=false, move5=false;
	bool move6=false, move9=false, move10=false, move13=false;
	bool move1025=false, move105=false, move1075=false;
	bool move1325=false, move135=false, move1375=false;
	bool move14=false, move17=false;
	bool move1425=false, move145=false, move1475=false;
	bool move1725=false, move175=false, move1775=false;
	bool move18=false, move19=false;

	IwVector3d normal;
	IwPoint3d point2, point3, point4, point5, point6, point9, point10, point1025, point105, point1075,
		point13, point1325, point135, point1375, point14, point1425, point145, point1475,
		point17, point1725, point175, point1775, point18, point19;

	int index2=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(2);
	if (index2!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index2), point2, normal);
	int index3=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(3);
	if (index3!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index3), point3, normal);	
	int index4=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(4);
	if (index4!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index4), point4, normal);	
	int index5=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(5);
	if (index5!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index5), point5, normal);		
	int index6=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(6);
	if (index6!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index6), point6, normal);		
	int index9=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(9);
	if (index9!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index9), point9, normal);		
	int index10=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(10);
	if (index10!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index10), point10, normal);		
	int index1025=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(10.25);
	if ( index1025 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1025), point1025, normal);				
	int index105=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(10.5);
	if ( index105 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index105), point105, normal);				
	int index1075=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(10.75);
	if ( index1075 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1075), point1075, normal);				
	int index13=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(13);
	if (index13!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index13), point13, normal);
	int index1325=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(13.25);
	if ( index1325 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1325), point1325, normal);
	int index135=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(13.5);
	if ( index135 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index135), point135, normal);
	int index1375=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(13.75);
	if ( index1375 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1375), point1375, normal);
	int index14=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(14);
	if (index14!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index14), point14, normal);
	int index1425=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(14.25);
	if ( index1425 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1425), point1425, normal);
	int index145=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(14.5);
	if ( index145 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index145), point145, normal);
	int index1475=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(14.75);
	if ( index1475 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1475), point1475, normal);
	int index17=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(17);
	if (index17!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index17), point17, normal);
	int index1725=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(17.25);
	if ( index1725 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1725), point1725, normal);
	int index175=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(17.5);
	if ( index175 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index175), point175, normal);
	int index1775=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(17.75);
	if ( index1775 != -1 )
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index1775), point1775, normal);
	int index18=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(18);
	if (index18!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index18), point18, normal);
	int index19=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(19);
	if (index19!= -1)
		pOutlineProfile->ConvertSketchSurfaceUVtoXYZ((IwVector3d)m_currFeaturePoints.GetAt(index19), point19, normal);

	IwPoint3d pnt;
	double dist, distToVertex;
	double extraMove = -0.25;
	double safeDist = 0.1;
	for (unsigned i=0; i<thisUnQualifiedLocations.GetSize(); i++)
	{
		pnt = thisUnQualifiedLocations.GetAt(i);
		dist = thisUnQualifiedDistances.GetAt(i); // overhangging is negative value 
		if ( point2.DistanceBetween(pnt) < effectDistance )
		{
			move2 = true;
			if ( dist < moveDist2 )
				moveDist2 = dist + extraMove;// We generally only care the min distance, in which the profile is overhangging.
											 // It is too complicate to handle the max distance, in which the profile is underhangging,
											 // unless the max distance is larger than allowableDistMaxPreferred.
			else if ( dist > allowableDistMax )
			{
				moveDist2 = dist - allowableDistMaxPreferred;
				distToVertex = point2.DistanceBetween(cutFeatPntsAtVertexes.GetAt(2));
				if ( moveDist2 > distToVertex )
					moveDist2 = distToVertex - safeDist;
			}
		}
		if ( point3.DistanceBetween(pnt) < effectDistance )
		{
			move3 = true;
			if ( dist < moveDist3 )
				moveDist3 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist3 = dist - allowableDistMaxPreferred;
				distToVertex = point3.DistanceBetween(cutFeatPntsAtVertexes.GetAt(3));
				if ( moveDist3 > distToVertex )
					moveDist3 = distToVertex - safeDist;
			}
		}
		if ( point4.DistanceBetween(pnt) < effectDistance )
		{
			move4 = true;
			if ( dist < moveDist4 )
				moveDist4 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist4 = dist - allowableDistMaxPreferred;
				distToVertex = point4.DistanceBetween(cutFeatPntsAtVertexes.GetAt(4));
				if ( moveDist4 > distToVertex )
					moveDist4 = distToVertex - safeDist;
			}
		}
		if ( point5.DistanceBetween(pnt) < effectDistance )
		{
			move5 = true;
			if ( dist < moveDist5 )
				moveDist5 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist5 = dist - allowableDistMaxPreferred;
				distToVertex = point5.DistanceBetween(cutFeatPntsAtVertexes.GetAt(5));
				if ( moveDist5 > distToVertex )
					moveDist5 = distToVertex - safeDist;
			}
		}
		if ( point6.DistanceBetween(pnt) < effectDistance )
		{
			move6 = true;
			if ( dist < moveDist6 )
				moveDist6 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist6 = dist - allowableDistMaxPreferred;
				distToVertex = point6.DistanceBetween(cutFeatPntsAtVertexes.GetAt(6));
				if ( moveDist6 > distToVertex )
					moveDist6 = distToVertex - safeDist;
			}
		}
		if ( point9.DistanceBetween(pnt) < effectDistance )
		{
			move9 = true;
			if ( dist < moveDist9 )
				moveDist9 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist9 = dist - allowableDistMaxPreferred;
				distToVertex = point9.DistanceBetween(cutFeatPntsAtVertexes.GetAt(9));
				if ( moveDist9 > distToVertex )
					moveDist9 = distToVertex - safeDist;
			}
		}
		if ( point10.DistanceBetween(pnt) < effectDistance )
		{
			move10 = true;
			if ( dist < moveDist10 )
				moveDist10 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist10 = dist - allowableDistMaxPreferred;
				distToVertex = point10.DistanceBetween(cutFeatPntsAtVertexes.GetAt(10));
				if ( moveDist10 > distToVertex )
					moveDist10 = distToVertex - safeDist;
			}
		}
		if ( point1025.IsInitialized() && point1025.DistanceBetween(pnt) < effectDistance )
		{
			move1025 = true;
			if ( dist < moveDist1025 )
				moveDist1025 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1025 = dist - allowableDistMaxPreferred;
		}
		if ( point105.IsInitialized() && point105.DistanceBetween(pnt) < effectDistance )
		{
			move105 = true;
			if ( dist < moveDist105 )
				moveDist105 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist105 = dist - allowableDistMaxPreferred;
		}
		if ( point1075.IsInitialized() && point1075.DistanceBetween(pnt) < effectDistance )
		{
			move1075 = true;
			if ( dist < moveDist1075 )
				moveDist1075 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1075 = dist - allowableDistMaxPreferred;
		}
		if ( point13.DistanceBetween(pnt) < effectDistance )
		{
			move13 = true;
			if ( dist < moveDist13 )
				moveDist13 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist13 = dist - allowableDistMaxPreferred;
				distToVertex = point13.DistanceBetween(cutFeatPntsAtVertexes.GetAt(13));
				if ( moveDist13 > distToVertex )
					moveDist13 = distToVertex - safeDist;
			}
		}
		if ( point1325.IsInitialized() && point1325.DistanceBetween(pnt) < effectDistance )
		{
			move1325 = true;
			if ( dist < moveDist1325 )
				moveDist1325 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1325 = dist - allowableDistMaxPreferred;
		}
		if ( point135.IsInitialized() && point135.DistanceBetween(pnt) < effectDistance )
		{
			move135 = true;
			if ( dist < moveDist135 )
				moveDist135 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist135 = dist - allowableDistMaxPreferred;
		}
		if ( point1375.IsInitialized() && point1375.DistanceBetween(pnt) < effectDistance )
		{
			move1375 = true;
			if ( dist < moveDist1375 )
				moveDist1375 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1375 = dist - allowableDistMaxPreferred;
		}
		if ( point14.DistanceBetween(pnt) < effectDistance )
		{
			move14 = true;
			if ( dist < moveDist14 )
				moveDist14 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist14 = dist - allowableDistMaxPreferred;
				distToVertex = point14.DistanceBetween(cutFeatPntsAtVertexes.GetAt(14));
				if ( moveDist14 > distToVertex )
					moveDist14 = distToVertex - safeDist;
			}
		}
		if ( point1425.IsInitialized() && point1425.DistanceBetween(pnt) < effectDistance )
		{
			move1425 = true;
			if ( dist < moveDist1425 )
				moveDist1425 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1425 = dist - allowableDistMaxPreferred;
		}
		if ( point145.IsInitialized() && point145.DistanceBetween(pnt) < effectDistance )
		{
			move145 = true;
			if ( dist < moveDist145 )
				moveDist145 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist145 = dist - allowableDistMaxPreferred;
		}
		if ( point1475.IsInitialized() && point1475.DistanceBetween(pnt) < effectDistance )
		{
			move1475 = true;
			if ( dist < moveDist1475 )
				moveDist1475 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1475 = dist - allowableDistMaxPreferred;
		}
		if ( point17.DistanceBetween(pnt) < effectDistance )
		{
			move17 = true;
			if ( dist < moveDist17 )
				moveDist17 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist17 = dist - allowableDistMaxPreferred;
				distToVertex = point17.DistanceBetween(cutFeatPntsAtVertexes.GetAt(17));
				if ( moveDist17 > distToVertex )
					moveDist17 = distToVertex - safeDist;
			}
		}
		if ( point1725.IsInitialized() && point1725.DistanceBetween(pnt) < effectDistance )
		{
			move1725 = true;
			if ( dist < moveDist1725 )
				moveDist1725 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1725 = dist - allowableDistMaxPreferred;
		}
		if ( point175.IsInitialized() && point175.DistanceBetween(pnt) < effectDistance )
		{
			move175 = true;
			if ( dist < moveDist175 )
				moveDist175 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist175 = dist - allowableDistMaxPreferred;
		}
		if ( point1775.IsInitialized() && point1775.DistanceBetween(pnt) < effectDistance )
		{
			move1775 = true;
			if ( dist < moveDist1775 )
				moveDist1775 = dist + extraMove;
			else if ( dist > allowableDistMax )
				moveDist1775 = dist - allowableDistMaxPreferred;
		}
		if ( point18.DistanceBetween(pnt) < effectDistance )
		{
			move18 = true;
			if ( dist < moveDist18 )
				moveDist18 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist18 = dist - allowableDistMaxPreferred;
				distToVertex = point18.DistanceBetween(cutFeatPntsAtVertexes.GetAt(18));
				if ( moveDist18 > distToVertex )
					moveDist18 = distToVertex - safeDist;
			}
		}
		if ( point19.DistanceBetween(pnt) < effectDistance )
		{
			move19 = true;
			if ( dist < moveDist19 )
				moveDist19 = dist + extraMove;
			else if ( dist > allowableDistMax )
			{
				moveDist19 = dist - allowableDistMaxPreferred;
				distToVertex = point19.DistanceBetween(cutFeatPntsAtVertexes.GetAt(19));
				if ( moveDist19 > distToVertex )
					moveDist19 = distToVertex - safeDist;
			}
		}
	}

	//
	if ( move2 )
	{
		m_currFeaturePoints.SetAt(index2, m_currFeaturePoints.GetAt(index2) - (-moveDist2)*xDir);
	}
	if ( move3 )
	{
		m_currFeaturePoints.SetAt(index3, m_currFeaturePoints.GetAt(index3) + (-moveDist3)*xDir);
	}
	if ( move4 )
	{
		m_currFeaturePoints.SetAt(index4, m_currFeaturePoints.GetAt(index4) - (-moveDist4)*xDir);
	}
	if ( move5 )
	{
		m_currFeaturePoints.SetAt(index5, m_currFeaturePoints.GetAt(index5) + (-moveDist5)*xDir);
	}
	if ( move6 )
	{
		m_currFeaturePoints.SetAt(index6, m_currFeaturePoints.GetAt(index6) - (-moveDist6)*xDir);
	}
	if ( move9 )
	{
		m_currFeaturePoints.SetAt(index9, m_currFeaturePoints.GetAt(index9) + (-moveDist9)*xDir);
	}
	if ( move10 )
	{
		m_currFeaturePoints.SetAt(index10, m_currFeaturePoints.GetAt(index10) - (-moveDist10)*xDir);
	}
	if ( move1025 )
	{
		m_currFeaturePoints.SetAt(index1025, m_currFeaturePoints.GetAt(index1025) - (-moveDist1025)*xDir);
	}
	if ( move105 )
	{
		m_currFeaturePoints.SetAt(index105, m_currFeaturePoints.GetAt(index105) - (-moveDist105)*xDir);
	}
	if ( move1075 )
	{
		m_currFeaturePoints.SetAt(index1075, m_currFeaturePoints.GetAt(index1075) - (-moveDist1075)*xDir);
	}
	if ( move13 )
	{
		m_currFeaturePoints.SetAt(index13, m_currFeaturePoints.GetAt(index13) + (-moveDist13)*xDir);
	}
	if ( move1325 )
	{
		m_currFeaturePoints.SetAt(index1325, m_currFeaturePoints.GetAt(index1325) + (-moveDist1325)*xDir);
	}
	if ( move135 )
	{
		m_currFeaturePoints.SetAt(index135, m_currFeaturePoints.GetAt(index135) + (-moveDist135)*xDir);
	}
	if ( move1375 )
	{
		m_currFeaturePoints.SetAt(index1375, m_currFeaturePoints.GetAt(index1375) + (-moveDist1375)*xDir);
	}
	if ( move14 )
	{
		m_currFeaturePoints.SetAt(index14, m_currFeaturePoints.GetAt(index14) - (-moveDist14)*xDir);
	}
	if ( move1425 )
	{
		m_currFeaturePoints.SetAt(index1425, m_currFeaturePoints.GetAt(index1425) - (-moveDist1425)*xDir);
	}
	if ( move145 )
	{
		m_currFeaturePoints.SetAt(index145, m_currFeaturePoints.GetAt(index145) - (-moveDist145)*xDir);
	}
	if ( move1475 )
	{
		m_currFeaturePoints.SetAt(index1475, m_currFeaturePoints.GetAt(index1475) - (-moveDist1475)*xDir);
	}
	if ( move17 )
	{
		m_currFeaturePoints.SetAt(index17, m_currFeaturePoints.GetAt(index17) + (-moveDist17)*xDir);
	}
	if ( move1725 )
	{
		m_currFeaturePoints.SetAt(index1725, m_currFeaturePoints.GetAt(index1725) + (-moveDist1725)*xDir);
	}
	if ( move175 )
	{
		m_currFeaturePoints.SetAt(index175, m_currFeaturePoints.GetAt(index175) + (-moveDist175)*xDir);
	}
	if ( move1775 )
	{
		m_currFeaturePoints.SetAt(index1775, m_currFeaturePoints.GetAt(index1775) + (-moveDist1775)*xDir);
	}
	if ( move18 )
	{
		m_currFeaturePoints.SetAt(index18, m_currFeaturePoints.GetAt(index18) - (-moveDist18)*xDir);
	}
	if ( move19 )
	{
		m_currFeaturePoints.SetAt(index19, m_currFeaturePoints.GetAt(index19) + (-moveDist19)*xDir);
	}

	bool moved = move2 || move3 || move4 || move5 ||
				 move6 || move9 || 
				 move10 || move1025 || move105 || move1075 ||
				 move13 || move1325 || move135 || move1375 ||
				 move14 || move1425 || move145 || move1475 ||
				 move17 || move1725 || move175 || move1775 ||
				 move18 || move19;

	// Update to outline profile
	if ( moved )
		m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	return moved;
}

bool OutlineProfileMgrModel::RefineByValidation_LateralAnteriorEar()
{
	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	// 
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	double distanceToCutEdgeMax = m_pDoc->GetVarTableValue("OUTLINE PROFILE MAX DISTANCE TO CUT EDGE");
	double distanceToCutEdgeDesired = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");

	// Check anterior overhanging in the lateral side 
	IwTArray<IwPoint3d> unqualifiedLocations;
	IwTArray<double> unqualifiedDistances;
	IwPoint3d pnt, lateralArc2ndPnt;
	double lateral2ndArcDist=0;
	bool earOnly = true;
	ValidateDistanceToLateralAntCutProfile(m_pDoc, unqualifiedLocations, unqualifiedDistances, earOnly, &lateralArc2ndPnt, &lateral2ndArcDist);
	bool moved = false;
	double distToMove = 0.0;
	if ( unqualifiedLocations.GetSize() > 0 ) // profile is outside of bone cut
	{
		SortIwTArray(unqualifiedDistances, false);
		distToMove = unqualifiedDistances.GetAt(0) + distanceToCutEdgeDesired;
	}
	else if ( lateral2ndArcDist < -distanceToCutEdgeMax || lateral2ndArcDist > -0.5) // lateral arc 2nd point is too far away from or too close to bone cut
	{
		distToMove = lateral2ndArcDist + distanceToCutEdgeDesired;
	}
	// to prevent too big movements, which may indicate something wrong.
	if ( distToMove > 5.0 )
		distToMove = 5.0;
	else if ( distToMove < -5.0 )
		distToMove = -5.0;

	// Also check whether 2 ears have already too close.
	double minEarsDistance = 8.0*m_pDoc->GetFemoralAxes()->GetEpiCondyleSize()/76.0;
	IwAxis2Placement swlAxes;
	m_pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(swlAxes);
	unsigned no = m_currFeaturePoints.GetSize() - 1;
	IwPoint3d earPoint0, earPoint1;
	earPoint0 = m_currFeaturePoints.GetAt(1);
	earPoint1 = m_currFeaturePoints.GetAt(no-1);
	//
	double earsDist = fabs((earPoint1-earPoint0).Dot(swlAxes.GetXAxis()));
	if ( earsDist < minEarsDistance )
		distToMove = 0;

	// Move lateral ear
	if ( distToMove != 0 )
	{
		if ( positiveSideIsLateral )
		{
			if ( m_currFeaturePointsInfo.GetAt(1) == 3 &&
				 m_currFeaturePointsInfo.GetAt(2) == 11 &&
				 m_currFeaturePointsInfo.GetAt(3) == 4 ) // anterior ear by an arc
			{
				IwVector3d moveVec = IwPoint3d(-distToMove,0.125,0);
				AdjustLateralEarArc(m_currFeaturePoints, 2, moveVec);
				moved = true;
			}
			else // an interpolation point
			{
				pnt = m_currFeaturePoints.GetAt(1) - IwPoint3d(distToMove,0,0);
				m_currFeaturePoints.SetAt(1, pnt);
				moved = true;
			}
		}
		else // lateral is in the negative side
		{
			unsigned no = m_currFeaturePoints.GetSize() - 1;
			if ( m_currFeaturePointsInfo.GetAt(no-1) == 4 &&
				 m_currFeaturePointsInfo.GetAt(no-2) == 11 &&
				 m_currFeaturePointsInfo.GetAt(no-3) == 3 ) // anterior ear by an arc
			{
				IwVector3d moveVec = IwPoint3d(distToMove,0.125,0);
				AdjustLateralEarArc(m_currFeaturePoints, no-2, moveVec);
				moved = true;
			}
			else // an interpolation point
			{
				pnt = m_currFeaturePoints.GetAt(no-1) + IwPoint3d(distToMove,0,0);
				m_currFeaturePoints.SetAt(no-1, pnt);
				moved = true;
			}
		}
	}

	return moved;
}


bool OutlineProfileMgrModel::RefineByValidation_MedialAnteriorEar()
{
	bool moved = false;

	// Check whether validate
	IwPoint3d recommendedPoint;
	bool passRefCircle = ValidateMedialAnteriorProfile(m_pDoc, &recommendedPoint);

	// Move medial ear
	if ( !passRefCircle )
	{
		if ( m_pDoc->IsPositiveSideLateral() )// medial in negative side
		{
			unsigned no = m_currFeaturePoints.GetSize() - 1;
			if ( m_currFeaturePointsInfo.GetAt(no-1) == 0 ) // anterior ear by an interpolation point
			{
				IwPoint3d medialRefPoint;
				ProjectXYZtoSketchSurfaceUV(recommendedPoint, medialRefPoint);
				m_currFeaturePoints.SetAt(no-1, medialRefPoint);
				moved = true;
			}
			// We do not handle if medial ear is an arc
		}
		else // medial is in the positive side
		{
			if ( m_currFeaturePointsInfo.GetAt(1) == 0 ) // anterior ear by an interpolation point
			{
				IwPoint3d medialRefPoint;
				ProjectXYZtoSketchSurfaceUV(recommendedPoint, medialRefPoint);
				m_currFeaturePoints.SetAt(1, medialRefPoint);
				moved = true;
			}
			// We do not handle if medial ear is an arc
		}
	}

	return moved;
}

/////////////////////////////////////////////////////////////////////////
// This function adjusts not only lateral arc, but also seam point.
void OutlineProfileMgrModel::AdjustLateralEarArc
(
	IwTArray<IwPoint3d>& featurePoints, // I/O:
	int arcCenterIndex,					// I: arc center index
	IwVector3d moveVec					// I: move vector in uv space. (x,y,0)=(u,v)
)
{
	IwPoint3d centerPnt, startPnt, endPnt, connectPnt, seamPnt, otherSidePnt;
	IwPoint3d midPnt;
	int nSize = (int)featurePoints.GetSize();
	int midSize = (int)(nSize/2.0);
	IwVector3d vec0, vec1;
	double width, chord;
	double radius, angle, adjustDistForAngle;

	// Ensure the angle defined by centerPnt, endPnt, connectPnt is 80 degrees for CR
	double eightyDeg = 80.0/180.0*IW_PI;
	double eightyFiveDeg = 85.0/180.0*IW_PI;
	double seventyFiveDeg = 75.0/180.0*IW_PI;
	// For AsymmetricEarShaped, the angle is 85
	if (COutlineProfile::IsAsymmetricEarShaped(m_pDoc))
	{
		eightyDeg = (80.0+5.0)/180.0*IW_PI;
		eightyFiveDeg = (85.0+5.0)/180.0*IW_PI;
		seventyFiveDeg = (75.0+5.0)/180.0*IW_PI;
	}

	// Lateral is in positive side (right knee)
	if ( arcCenterIndex < midSize )
	{
		otherSidePnt = m_currFeaturePoints.GetAt(nSize-2);
		seamPnt = m_currFeaturePoints.GetAt(0);
		startPnt = m_currFeaturePoints.GetAt(arcCenterIndex-1);
		centerPnt = m_currFeaturePoints.GetAt(arcCenterIndex);
		endPnt = m_currFeaturePoints.GetAt(arcCenterIndex+1);
		connectPnt = m_currFeaturePoints.GetAt(arcCenterIndex+2);
		radius = centerPnt.DistanceBetween(startPnt);

		// Move the arc
		startPnt += moveVec;
		centerPnt += moveVec;
		endPnt += moveVec;

		// Ensure the angle defined by centerPnt, endPnt, connectPnt is 80 degrees
		vec0 = endPnt - centerPnt;
		vec0.Unitize();
		vec1 = endPnt - connectPnt;
		vec1.Unitize();
		vec0.AngleBetween(vec1, angle);
		if ( angle > eightyFiveDeg )
		{
			adjustDistForAngle = (angle-eightyDeg)*radius;
			endPnt = endPnt + adjustDistForAngle*vec1;
		}
		else if ( angle < seventyFiveDeg )
		{
			adjustDistForAngle = (eightyDeg-angle)*radius;
			endPnt = endPnt - adjustDistForAngle*vec1;
		}
		// Ensure endPnt is on the arc
		vec0 = endPnt - centerPnt;
		vec0.Unitize();
		endPnt = centerPnt + radius*vec0;

		// Ensure the angle defined by centerPnt, startPnt, seamPnt is 80 degrees
		vec0 = startPnt - centerPnt;
		vec0.Unitize();
		vec1 = startPnt - seamPnt;
		vec1.Unitize();
		vec0.AngleBetween(vec1, angle);
		if ( angle > eightyFiveDeg )
		{
			adjustDistForAngle = (angle-eightyDeg)*radius;
			startPnt = startPnt + adjustDistForAngle*vec1;
		}
		else if ( angle < seventyFiveDeg )
		{
			adjustDistForAngle = (eightyDeg-angle)*radius;
			startPnt = startPnt - adjustDistForAngle*vec1;
		}
		// Ensure startPnt is on the arc
		vec0 = startPnt - centerPnt;
		vec0.Unitize();
		startPnt = centerPnt + radius*vec0;

		// Move seamPnt, which is defined by the indentation width (from startPnt to otherSidePnt) and chord height.
		// seamPnt is in the middle of startPnt and otherSidePnt with a chord = 0.0875*width.
		midPnt = 0.5*(startPnt + otherSidePnt);
		width = startPnt.DistanceBetween(otherSidePnt);
		chord = 0.0875*width;
		seamPnt = midPnt + IwVector3d(0,chord,0);

		// Set back to m_currFeaturePoints
		m_currFeaturePoints.SetAt(0, seamPnt);
		m_currFeaturePoints.SetAt(arcCenterIndex-1, startPnt);
		m_currFeaturePoints.SetAt(arcCenterIndex, centerPnt);
		m_currFeaturePoints.SetAt(arcCenterIndex+1, endPnt);
	}
	else // Lateral is in negative side (left knee)
	{
		otherSidePnt = m_currFeaturePoints.GetAt(1);
		seamPnt = m_currFeaturePoints.GetAt(0);
		startPnt = m_currFeaturePoints.GetAt(arcCenterIndex-1);
		centerPnt = m_currFeaturePoints.GetAt(arcCenterIndex);
		endPnt = m_currFeaturePoints.GetAt(arcCenterIndex+1);
		connectPnt = m_currFeaturePoints.GetAt(arcCenterIndex-2);
		radius = centerPnt.DistanceBetween(startPnt);

		// Move the arc
		startPnt += moveVec;
		centerPnt += moveVec;
		endPnt += moveVec;

		// Ensure the angle defined by centerPnt, startPnt, connectPnt is 80 degrees
		vec0 = startPnt - centerPnt;
		vec0.Unitize();
		vec1 = startPnt - connectPnt;
		vec1.Unitize();
		vec0.AngleBetween(vec1, angle);
		if ( angle > eightyFiveDeg )
		{
			adjustDistForAngle = (angle-eightyDeg)*radius;
			startPnt = startPnt + adjustDistForAngle*vec1;
		}
		else if ( angle < seventyFiveDeg )
		{
			adjustDistForAngle = (eightyDeg-angle)*radius;
			startPnt = startPnt - adjustDistForAngle*vec1;
		}
		// Ensure startPnt is on the arc
		vec0 = startPnt - centerPnt;
		vec0.Unitize();
		startPnt = centerPnt + radius*vec0;

		// Ensure the angle defined by centerPnt, endPnt, seamPnt is 80 degrees
		vec0 = endPnt - centerPnt;
		vec0.Unitize();
		vec1 = endPnt - seamPnt;
		vec1.Unitize();
		vec0.AngleBetween(vec1, angle);
		if ( angle > eightyFiveDeg )
		{
			adjustDistForAngle = (angle-eightyDeg)*radius;
			endPnt = endPnt + adjustDistForAngle*vec1;
		}
		else if ( angle < seventyFiveDeg )
		{
			adjustDistForAngle = (eightyDeg-angle)*radius;
			endPnt = endPnt - adjustDistForAngle*vec1;
		}
		// Ensure endPnt is on the arc
		vec0 = endPnt - centerPnt;
		vec0.Unitize();
		endPnt = centerPnt + radius*vec0;

		// Move seamPnt, which is defined by the indentation width (from startPnt to otherSidePnt) and chord height.
		// seamPnt is in the middle of startPnt and otherSidePnt with a chord = 0.0875*width.
		midPnt = 0.5*(endPnt + otherSidePnt);
		width = endPnt.DistanceBetween(otherSidePnt);
		chord = 0.0875*width;
		seamPnt = midPnt + IwVector3d(0,chord,0);

		// Set back to m_currFeaturePoints
		m_currFeaturePoints.SetAt(0, seamPnt);
		m_currFeaturePoints.SetAt(arcCenterIndex-1, startPnt);
		m_currFeaturePoints.SetAt(arcCenterIndex, centerPnt);
		m_currFeaturePoints.SetAt(arcCenterIndex+1, endPnt);
	}


}

//////////////////////////////////////////////////////////////
// Refine all the vertexes based on the thicknesses, and 
// the posterior profiles based on the tip thicknesses.
bool OutlineProfileMgrModel::RefineByVertexThickness()
{
	return true;//PSV--- Solidimplant code removal trial
	IwVector3d xDir = IwVector3d(1,0,0);
	IwVector3d yDir = IwVector3d(0,1,0);// from anterior to posterior
	COutlineProfile* pOutlineProfile = m_pDoc->GetOutlineProfile();

	double criticalThickness = /*CAnalyzeImplant::DetermineCriticalThickness(m_pDoc) +*/ 0.015;// "0.015" as allowance
	double minTipThickness = m_pDoc->GetVarTableValue("IMPLANT POSTERIOR TIP MIN THICKNESS");
	double maxTipThickness = m_pDoc->GetVarTableValue("IMPLANT POSTERIOR TIP MAX THICKNESS");
	double minAntTipThickness = m_pDoc->GetVarTableValue("IMPLANT ANTERIOR TIP MIN THICKNESS");
	double maxAntTipThickness = m_pDoc->GetVarTableValue("IMPLANT ANTERIOR TIP MAX THICKNESS");

	IwTArray<double> featureThickness;
	if ( featureThickness.GetSize() == 0 )
		return false;

	bool moved = false;
	double extraMove = 0.1;
	double maxMove = 1.0;
	// Handle the vertex one by one
	int index2 = 2;
	double thickness2 = featureThickness.GetAt(index2);
	int featIndex2=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index2);
	if ( featIndex2 != -1 && thickness2 < criticalThickness )
	{
		double moveDist2 = 4.0*(criticalThickness - thickness2);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist2 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex2, m_currFeaturePoints.GetAt(featIndex2) - (moveDist2+extraMove)*xDir);
			moved = true;
		}
	}
	int index3 = 3;
	double thickness3 = featureThickness.GetAt(index3);
	int featIndex3=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index3);
	if ( featIndex3 != -1 && thickness3 < criticalThickness )
	{
		double moveDist3 = 4.0*(criticalThickness - thickness3);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist3 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex3, m_currFeaturePoints.GetAt(featIndex3) + (moveDist3+extraMove)*xDir);
			moved = true;
		}
	}
	int index4 = 4;
	double thickness4 = featureThickness.GetAt(index4);
	int featIndex4=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index4);
	if ( featIndex4 != -1 && thickness4 < criticalThickness )
	{
		double moveDist4 = 4.0*(criticalThickness - thickness4);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist4 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex4, m_currFeaturePoints.GetAt(featIndex4) - (moveDist4+extraMove)*xDir);
			moved = true;
		}
	}
	int index5 = 5;
	double thickness5 = featureThickness.GetAt(index5);
	int featIndex5=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index5);
	if ( featIndex5 != -1 && thickness5 < criticalThickness )
	{
		double moveDist5 = 4.0*(criticalThickness - thickness5);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist5 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex5, m_currFeaturePoints.GetAt(featIndex5) + (moveDist5+extraMove)*xDir);
			moved = true;
		}
	}
	int index6 = 6;
	double thickness6 = featureThickness.GetAt(index6);
	int featIndex6=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index6);
	if ( featIndex6 != -1 && thickness6 < criticalThickness )
	{
		double moveDist6 = 4.0*(criticalThickness - thickness6);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist6 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex6, m_currFeaturePoints.GetAt(featIndex6) - (moveDist6+extraMove)*xDir);
			moved = true;
		}
	}
	int index7 = 7;
	double thickness7 = featureThickness.GetAt(index7);
	int featIndex7=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index7);
	if ( featIndex7 != -1 && thickness7 < criticalThickness )
	{
		double moveDist7 = 4.0*(criticalThickness - thickness7);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist7 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex7, m_currFeaturePoints.GetAt(featIndex7) + (moveDist7+extraMove)*xDir);
			moved = true;
		}
	}
	int index8 = 8;
	double thickness8 = featureThickness.GetAt(index8);
	int featIndex8=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index8);
	if ( featIndex8 != -1 && thickness8 < criticalThickness )
	{
		double moveDist8 = 4.0*(criticalThickness - thickness8);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist8 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex8, m_currFeaturePoints.GetAt(featIndex8) - (moveDist8+extraMove)*xDir);
			moved = true;
		}
	}
	int index9 = 9;
	double thickness9 = featureThickness.GetAt(index9);
	int featIndex9=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index9);
	if ( featIndex9 != -1 && thickness9 < criticalThickness )
	{
		double moveDist9 = 4.0*(criticalThickness - thickness9);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist9 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex9, m_currFeaturePoints.GetAt(featIndex9) + (moveDist9+extraMove)*xDir);
			moved = true;
		}
	}
	int index10 = 10;
	double thickness10 = featureThickness.GetAt(index10);
	int featIndex10=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index10);
	if ( featIndex10 != -1 && thickness10 < criticalThickness )
	{
		double moveDist10 = 4.0*(criticalThickness - thickness10);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist10 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex10, m_currFeaturePoints.GetAt(featIndex10) - (moveDist10+extraMove)*xDir);
			moved = true;
		}
	}
	int index13 = 13;
	double thickness13 = featureThickness.GetAt(index13);
	int featIndex13=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index13);
	if ( featIndex13 != -1 && thickness13 < criticalThickness )
	{
		double moveDist13 = 4.0*(criticalThickness - thickness13);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist13 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex13, m_currFeaturePoints.GetAt(featIndex13) + (moveDist13+extraMove)*xDir);
			moved = true;
		}
	}
	int index14 = 14;
	double thickness14 = featureThickness.GetAt(index14);
	int featIndex14=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index14);
	if ( featIndex14 != -1 && thickness14 < criticalThickness )
	{
		double moveDist14 = 4.0*(criticalThickness - thickness14);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist14 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex14, m_currFeaturePoints.GetAt(featIndex14) - (moveDist14+extraMove)*xDir);
			moved = true;
		}
	}
	int index17 = 17;
	double thickness17 = featureThickness.GetAt(index17);
	int featIndex17=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index17);
	if ( featIndex17 != -1 && thickness17 < criticalThickness )
	{
		double moveDist17 = 4.0*(criticalThickness - thickness17);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist17 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex17, m_currFeaturePoints.GetAt(featIndex17) + (moveDist17+extraMove)*xDir);
			moved = true;
		}
	}
	int index18 = 18;
	double thickness18 = featureThickness.GetAt(index18);
	int featIndex18=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index18);
	if ( featIndex18 != -1 && thickness18 < criticalThickness )
	{
		double moveDist18 = 4.0*(criticalThickness - thickness18);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist18 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex18, m_currFeaturePoints.GetAt(featIndex18) - (moveDist18+extraMove)*xDir);
			moved = true;
		}
	}
	int index19 = 19;
	double thickness19 = featureThickness.GetAt(index19);
	int featIndex19=pOutlineProfile->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(index19);
	if ( featIndex19 != -1 && thickness19 < criticalThickness )
	{
		double moveDist19 = 4.0*(criticalThickness - thickness19);// estimated based on outer surface slope. About 4 times of thickness
		if ( moveDist19 < maxMove )
		{
			m_currFeaturePoints.SetAt(featIndex19, m_currFeaturePoints.GetAt(featIndex19) + (moveDist19+extraMove)*xDir);
			moved = true;
		}
	}

	// Update to outline profile
	if ( moved )
		m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	///////////////////////////////////////////////
	// Now handle the posterior profiles
	double posTan = 0.7265; // j-curve around posterior tip is about 36 degree related to posterior cut plane.
	double negTan = 0.7265; // j-curve around posterior tip is about 36 degree related to posterior cut plane.
	double antTan = 0.2493; // j-curve around anterior tip is about 14 degree related to anterior cut plane.

	double posTipTangentAngle, negTipTangentAngle, antTipTangentAngle;
	//bool gotTipTangentAngles = m_pDoc->GetJCurves()->GetAntPostTipPointsTangentAngles(posTipTangentAngle, negTipTangentAngle, antTipTangentAngle);
	//if ( gotTipTangentAngles )
	//{
	//	posTan = tan(posTipTangentAngle);
	//	negTan = tan(negTipTangentAngle);
	//	antTan = tan(antTipTangentAngle);
	//}

	IwTArray<IwPoint3d> locations;
	IwTArray<double> distances;

	double extraThickness = 0.075;
	double minDistToPosteriorEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE MIN POSTERIOR DISTANCE TO CUT EDGE");
	int index0 = 0;
	double thickness0 = featureThickness.GetAt(index0);
	double posPosteriorDistanceToCutProfile = ValidatePosteriorDistanceToPosteriorCutProfile(m_pDoc, index0, locations, distances);
	if ( thickness0 < minTipThickness )
	{
		// Move posterior profile anteriorly (decrease V)
		double moveDist0 = (minTipThickness-thickness0+extraThickness)/posTan; 
		for (int i=featIndex2+1; i<featIndex3; i++)
		{
			m_currFeaturePoints.SetAt(i, m_currFeaturePoints.GetAt(i) - moveDist0*yDir);
		}
		moved = true;
	}
	else if ( thickness0 > maxTipThickness )
	{
		// Move posterior profile posteriorly (increase V)
		double moveDist0 = (thickness0-maxTipThickness+extraThickness)/posTan; 
		if ( (posPosteriorDistanceToCutProfile - moveDist0) < minDistToPosteriorEdge )
			moveDist0 = posPosteriorDistanceToCutProfile - minDistToPosteriorEdge;
		for (int i=featIndex2+1; i<featIndex3; i++)
		{
			m_currFeaturePoints.SetAt(i, m_currFeaturePoints.GetAt(i) + moveDist0*yDir);
		}
		moved = true;
	}
	//
	int index1 = 1;
	double negPosteriorDistanceToCutProfile = ValidatePosteriorDistanceToPosteriorCutProfile(m_pDoc, index1, locations, distances);
	double thickness1 = featureThickness.GetAt(index1);
	if ( thickness1 < minTipThickness )
	{
		// Move posterior profile anteriorly (decrease V)
		double moveDist1 = (minTipThickness-thickness1+extraThickness)/negTan; 
		for (int i=featIndex4+1; i<featIndex5; i++)
		{
			m_currFeaturePoints.SetAt(i, m_currFeaturePoints.GetAt(i) - moveDist1*yDir);
		}
		moved = true;
	}
	else if ( thickness1 > maxTipThickness )
	{
		// Move posterior profile posteriorly (increase V)
		double moveDist1 = (thickness1-maxTipThickness+extraThickness)/negTan; 
		if ( (negPosteriorDistanceToCutProfile - moveDist1) < minDistToPosteriorEdge )
			moveDist1 = negPosteriorDistanceToCutProfile - minDistToPosteriorEdge;
		for (int i=featIndex4+1; i<featIndex5; i++)
		{
			m_currFeaturePoints.SetAt(i, m_currFeaturePoints.GetAt(i) + moveDist1*yDir);
		}
		moved = true;
	}

	///////////////////////////////////////////////
	// Now handle the anterior profile
	int nSize = m_currFeaturePoints.GetSize();
	int index20 = 20;
	double thickness20 = featureThickness.GetAt(index20);
	if ( thickness20 < minAntTipThickness )
	{
		// Move anterior profile posteriorly (increase V)
		double moveDist20 = (minAntTipThickness-thickness20+extraThickness)/antTan; 
		if (moveDist20 > 3.5) // in case too big or too small
			moveDist20 = 3.5;
		else if ( moveDist20 < -3.5 )
			moveDist20 = -3.5;
		if ( m_currFeaturePointsInfo.GetAt(1) == 0 ) // interpolation point
		{
			m_currFeaturePoints.SetAt(0, m_currFeaturePoints.GetAt(0) + moveDist20*yDir);
			m_currFeaturePoints.SetAt(1, m_currFeaturePoints.GetAt(1) + moveDist20*yDir);
		}
		else
		{
			m_currFeaturePoints.SetAt(0, m_currFeaturePoints.GetAt(0) + moveDist20*yDir);
			m_currFeaturePoints.SetAt(1, m_currFeaturePoints.GetAt(1) + moveDist20*yDir);
			m_currFeaturePoints.SetAt(2, m_currFeaturePoints.GetAt(2) + moveDist20*yDir);
			m_currFeaturePoints.SetAt(3, m_currFeaturePoints.GetAt(3) + moveDist20*yDir);
		}
		if ( m_currFeaturePointsInfo.GetAt(nSize-2) == 0 ) // interpolation point
		{
			m_currFeaturePoints.SetAt(nSize-2, m_currFeaturePoints.GetAt(nSize-2) + moveDist20*yDir);
		}
		else
		{
			m_currFeaturePoints.SetAt(nSize-2, m_currFeaturePoints.GetAt(nSize-2) + moveDist20*yDir);
			m_currFeaturePoints.SetAt(nSize-3, m_currFeaturePoints.GetAt(nSize-3) + moveDist20*yDir);
			m_currFeaturePoints.SetAt(nSize-4, m_currFeaturePoints.GetAt(nSize-4) + moveDist20*yDir);
		}
		moved = true;
	}
	else if ( thickness20 > maxAntTipThickness )
	{
		// Move posterior profile anteriorly (decrease V)
		double moveDist20 = (thickness20-maxAntTipThickness+extraThickness)/antTan; 
		if (moveDist20 > 3.5) // in case too big or too small
			moveDist20 = 3.5;
		else if ( moveDist20 < -3.5 )
			moveDist20 = -3.5;
		if ( m_currFeaturePointsInfo.GetAt(1) == 0 ) // interpolation point
		{
			m_currFeaturePoints.SetAt(0, m_currFeaturePoints.GetAt(0) - moveDist20*yDir);
			m_currFeaturePoints.SetAt(1, m_currFeaturePoints.GetAt(1) - moveDist20*yDir);
		}
		else
		{
			m_currFeaturePoints.SetAt(0, m_currFeaturePoints.GetAt(0) - moveDist20*yDir);
			m_currFeaturePoints.SetAt(1, m_currFeaturePoints.GetAt(1) - moveDist20*yDir);
			m_currFeaturePoints.SetAt(2, m_currFeaturePoints.GetAt(2) - moveDist20*yDir);
			m_currFeaturePoints.SetAt(3, m_currFeaturePoints.GetAt(3) - moveDist20*yDir);
		}
		if (m_currFeaturePointsInfo.GetAt(nSize-2) == 0 ) // interpolation point
		{
			m_currFeaturePoints.SetAt(nSize-2, m_currFeaturePoints.GetAt(nSize-2) - moveDist20*yDir);
		}
		else
		{
			m_currFeaturePoints.SetAt(nSize-2, m_currFeaturePoints.GetAt(nSize-2) - moveDist20*yDir);
			m_currFeaturePoints.SetAt(nSize-3, m_currFeaturePoints.GetAt(nSize-3) - moveDist20*yDir);
			m_currFeaturePoints.SetAt(nSize-4, m_currFeaturePoints.GetAt(nSize-4) - moveDist20*yDir);
		}
		moved = true;
	}

	// Update to outline profile
	if ( moved )
		m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	return true;
}

///////////////////////////////////////////////////////////////////////////////////
// This function will move the feature points along the input outline profile
// to the cut edges. Therefore, the thickness along the cut edges will first be  
// kept the same, and the outline feature points are on the preferred cut edges.
// However, if the feature points are not within 0~3mm away from cut edges, 
// this function will adjust the feature points in ML direction.
///////////////////////////////////////////////////////////////////////////////////
void OutlineProfileMgrModel::ResampleFeaturePoints()
{

	double minDistToEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE MIN DISTANCE TO CUT EDGE") + 0.15;// 0.15 as tolerance
	double maxDistToEdge = m_pDoc->GetVarTableValue("OUTLINE PROFILE MAX DISTANCE TO CUT EDGE") - 0.05;// 0.05 as tolerance

	// Determine the cut feature points
	// Get femoral cuts information
	CFemoralCuts*		femoralCuts = m_pDoc->GetFemoralCuts();
	if (femoralCuts == NULL) return;

	// Get the femoral axes
	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL ) return;
	IwAxis2Placement wslAxes;
	pFemoralAxes->GetWhiteSideLineAxes(wslAxes);

	// Get sketch surface
	IwBSplineSurface* sketchSurface;
	m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);

	// Get outlineProfileXYZCurve
	IwBSplineCurve* outlineProfileXYZCurve=NULL;
	IwCurve* copyCurve;
	m_pDoc->GetOutlineProfile()->GetOutlineProfileXYZCurve()->Copy(m_pDoc->GetIwContext(), copyCurve);
	outlineProfileXYZCurve = (IwBSplineCurve*)copyCurve;

	// Get feature points at vertexes
	IwTArray<IwPoint3d> cutFeatPntsAtVertexes;
	femoralCuts->GetFeaturePointsAtVertexes(cutFeatPntsAtVertexes);
	// Get the cut edges
	IwTArray<IwCurve*> cutEdgeCurves;
	femoralCuts->GetCutEdgeCurves(cutEdgeCurves);

	////////////////////////////////////////////////////////////////////////
	// Let's update the feature points one by one
	IwPoint3d cPnt, edgePnt, surfPnt;
	double param;
	IwPoint2d uvParam;
	IwVector3d tempVec;
	// positive side outer point 2
	int inedx2 = 2;
	int outlineProfileFeatureIndex2 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx2);
	if ( outlineProfileFeatureIndex2 != -1 )
	{
		int remapZValue2 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex2).z;
		if ( remapZValue2 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt2 = m_cutFeatPnts.GetAt(inedx2);
			IwPoint3d featurePntAtVectex2 = cutFeatPntsAtVertexes.GetAt(inedx2);
			DistFromPointToCurve(cutFeaturePnt2, outlineProfileXYZCurve, cPnt, param);// the corresponding point on outlineProfileXYZCurve
			IwBSplineCurve* cutEdge0 = (IwBSplineCurve*)cutEdgeCurves.GetAt(0);
			DistFromPointToCurve(cPnt, cutEdge0, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex2;
			double dist2 = -tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist2 < minDistToEdge )
				edgePnt = featurePntAtVectex2 - minDistToEdge*wslAxes.GetXAxis();
			else if ( dist2 > maxDistToEdge )
				edgePnt = featurePntAtVectex2 - maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex2, newFeaturePnt);
		}
	}

	// positive side inner point 3
	int inedx3 = 3;
	int outlineProfileFeatureIndex3 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx3);
	if ( outlineProfileFeatureIndex3 != -1 )
	{
		int remapZValue3 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex3).z;
		if ( remapZValue3 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt3 = m_cutFeatPnts.GetAt(inedx3);
			IwPoint3d featurePntAtVectex3 = cutFeatPntsAtVertexes.GetAt(inedx3);
			DistFromPointToCurve(cutFeaturePnt3, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
			IwBSplineCurve* cutEdge0 = (IwBSplineCurve*)cutEdgeCurves.GetAt(0);
			DistFromPointToCurve(cPnt, cutEdge0, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex3;
			double dist3 = tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist3 < minDistToEdge )
				edgePnt = featurePntAtVectex3 + minDistToEdge*wslAxes.GetXAxis();
			else if ( dist3 > maxDistToEdge )
				edgePnt = featurePntAtVectex3 + maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex3, newFeaturePnt);
		}
	}

	// negative side inner point 4
	int inedx4 = 4;
	int outlineProfileFeatureIndex4 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx4);
	if ( outlineProfileFeatureIndex4 != -1 )
	{
		int remapZValue4 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex4).z;
		if ( remapZValue4 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt4 = m_cutFeatPnts.GetAt(inedx4);
			IwPoint3d featurePntAtVectex4 = cutFeatPntsAtVertexes.GetAt(inedx4);
			DistFromPointToCurve(cutFeaturePnt4, outlineProfileXYZCurve, cPnt, param);// the corresponding point on outlineProfileXYZCurve
			IwBSplineCurve* cutEdge1 = (IwBSplineCurve*)cutEdgeCurves.GetAt(1);
			DistFromPointToCurve(cPnt, cutEdge1, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex4;
			double dist4 = -tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist4 < minDistToEdge )
				edgePnt = featurePntAtVectex4 - minDistToEdge*wslAxes.GetXAxis();
			else if ( dist4 > maxDistToEdge )
				edgePnt = featurePntAtVectex4 - maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex4, newFeaturePnt);
		}
	}

	// negative side outer point 5
	int inedx5 = 5;
	int outlineProfileFeatureIndex5 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx5);
	if ( outlineProfileFeatureIndex5 != -1 )
	{
		int remapZValue5 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex5).z;
		if ( remapZValue5 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt5 = m_cutFeatPnts.GetAt(inedx5);
			IwPoint3d featurePntAtVectex5 = cutFeatPntsAtVertexes.GetAt(inedx5);
			DistFromPointToCurve(cutFeaturePnt5, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
			IwBSplineCurve* cutEdge1 = (IwBSplineCurve*)cutEdgeCurves.GetAt(1);
			DistFromPointToCurve(cPnt, cutEdge1, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex5;
			double dist5 = tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist5 < minDistToEdge )
				edgePnt = featurePntAtVectex5 + minDistToEdge*wslAxes.GetXAxis();
			else if ( dist5 > maxDistToEdge )
				edgePnt = featurePntAtVectex5 + maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex5, newFeaturePnt);
		}
	}

	// positive side outer point 6
	int inedx6 = 6;
	int outlineProfileFeatureIndex6 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx6);
	if ( outlineProfileFeatureIndex6 != -1 )
	{
		int remapZValue6 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex6).z;
		if ( remapZValue6 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt6 = m_cutFeatPnts.GetAt(inedx6);
			IwPoint3d featurePntAtVectex6 = cutFeatPntsAtVertexes.GetAt(inedx6);
			DistFromPointToCurve(cutFeaturePnt6, outlineProfileXYZCurve, cPnt, param);// the corresponding point on outlineProfileXYZCurve
			IwBSplineCurve* cutEdge2 = (IwBSplineCurve*)cutEdgeCurves.GetAt(2);
			DistFromPointToCurve(cPnt, cutEdge2, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex6;
			double dist6 = -tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist6 < minDistToEdge )
				edgePnt = featurePntAtVectex6 - minDistToEdge*wslAxes.GetXAxis();
			else if ( dist6 > maxDistToEdge )
				edgePnt = featurePntAtVectex6 - maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex6, newFeaturePnt);
		}
	}

	// positive side inner point 7
	int inedx7 = 7;
	int outlineProfileFeatureIndex7 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx7);
	if ( outlineProfileFeatureIndex7 != -1 ) 
	{
		IwPoint3d cutFeaturePnt7 = m_cutFeatPnts.GetAt(inedx7);
		DistFromPointToCurve(cutFeaturePnt7, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
		IwBSplineCurve* cutEdge2 = (IwBSplineCurve*)cutEdgeCurves.GetAt(2);
		DistFromPointToCurve(cPnt, cutEdge2, edgePnt, param);// the corresponding point on cut edge
		//
		DistFromPointToCurve(edgePnt, outlineProfileXYZCurve, cPnt, param);
		DistFromPointToSurface(cPnt, sketchSurface, surfPnt, uvParam);
		// update the feature point
		IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
		m_currFeaturePoints.SetAt(outlineProfileFeatureIndex7, newFeaturePnt);
	}

	// negative side inner point 8
	int inedx8 = 8;
	int outlineProfileFeatureIndex8 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx8);
	if ( outlineProfileFeatureIndex8 != -1 ) 
	{
		IwPoint3d cutFeaturePnt8 = m_cutFeatPnts.GetAt(inedx8);
		DistFromPointToCurve(cutFeaturePnt8, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
		IwBSplineCurve* cutEdge3 = (IwBSplineCurve*)cutEdgeCurves.GetAt(3);
		DistFromPointToCurve(cPnt, cutEdge3, edgePnt, param);// the corresponding point on cut edge
		//
		DistFromPointToCurve(edgePnt, outlineProfileXYZCurve, cPnt, param);
		DistFromPointToSurface(cPnt, sketchSurface, surfPnt, uvParam);
		// update the feature point
		IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
		m_currFeaturePoints.SetAt(outlineProfileFeatureIndex8, newFeaturePnt);
	}

	// negative side outer point 9
	int inedx9 = 9;
	int outlineProfileFeatureIndex9 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx9);
	if ( outlineProfileFeatureIndex9 != -1 )
	{
		int remapZValue9 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex9).z;
		if ( remapZValue9 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt9 = m_cutFeatPnts.GetAt(inedx9);
			IwPoint3d featurePntAtVectex9 = cutFeatPntsAtVertexes.GetAt(inedx9);
			DistFromPointToCurve(cutFeaturePnt9, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
			IwBSplineCurve* cutEdge3 = (IwBSplineCurve*)cutEdgeCurves.GetAt(3);
			DistFromPointToCurve(cPnt, cutEdge3, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex9;
			double dist9 = tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist9 < minDistToEdge )
				edgePnt = featurePntAtVectex9 + minDistToEdge*wslAxes.GetXAxis();
			else if ( dist9 > maxDistToEdge )
				edgePnt = featurePntAtVectex9 + maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex9, newFeaturePnt);
		}
	}

	// positive side outer point 10
	int inedx10 = 10;
	IwPoint3d point10;
	int outlineProfileFeatureIndex10 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx10);
	if ( outlineProfileFeatureIndex10 != -1 )
	{
		int remapZValue10 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex10).z;
		if ( remapZValue10 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt10 = m_cutFeatPnts.GetAt(inedx10);
			IwPoint3d featurePntAtVectex10 = cutFeatPntsAtVertexes.GetAt(inedx10);
			DistFromPointToCurve(cutFeaturePnt10, outlineProfileXYZCurve, cPnt, param);// the corresponding point on outlineProfileXYZCurve
			IwBSplineCurve* cutEdge4 = (IwBSplineCurve*)cutEdgeCurves.GetAt(4);
			DistFromPointToCurve(cPnt, cutEdge4, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex10;
			double dist10 = -tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist10 < minDistToEdge )
				edgePnt = featurePntAtVectex10 - minDistToEdge*wslAxes.GetXAxis();
			else if ( dist10 > maxDistToEdge )
				edgePnt = featurePntAtVectex10 - maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			point10 = surfPnt;
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex10, newFeaturePnt);
		}
	}

	// Skip points 11 & 12

	// negative side outer point 13
	int inedx13 = 13;
	IwPoint3d point13;
	int outlineProfileFeatureIndex13 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx13);
	if ( outlineProfileFeatureIndex13 != -1 )
	{
		int remapZValue13 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex13).z;
		if ( remapZValue13 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt13 = m_cutFeatPnts.GetAt(inedx13);
			IwPoint3d featurePntAtVectex13 = cutFeatPntsAtVertexes.GetAt(inedx13);
			DistFromPointToCurve(cutFeaturePnt13, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
			IwBSplineCurve* cutEdge5 = (IwBSplineCurve*)cutEdgeCurves.GetAt(5);
			DistFromPointToCurve(cPnt, cutEdge5, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex13;
			double dist13 = tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist13 < minDistToEdge )
				edgePnt = featurePntAtVectex13 + minDistToEdge*wslAxes.GetXAxis();
			else if ( dist13 > maxDistToEdge )
				edgePnt = featurePntAtVectex13 + maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			point13 = surfPnt;
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex13, newFeaturePnt);
		}
	}

	// positive side outer point 14
	int inedx14 = 14;
	IwPoint3d point14;
	int outlineProfileFeatureIndex14 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx14);
	if ( outlineProfileFeatureIndex14 != -1 )
	{
		int remapZValue14 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex14).z;
		if ( remapZValue14 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt14 = m_cutFeatPnts.GetAt(inedx14);
			IwPoint3d featurePntAtVectex14 = cutFeatPntsAtVertexes.GetAt(inedx14);
			DistFromPointToCurve(cutFeaturePnt14, outlineProfileXYZCurve, cPnt, param);// the corresponding point on outlineProfileXYZCurve
			IwBSplineCurve* cutEdge6 = (IwBSplineCurve*)cutEdgeCurves.GetAt(6);
			DistFromPointToCurve(cPnt, cutEdge6, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex14;
			double dist14 = -tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist14 < minDistToEdge )
				edgePnt = featurePntAtVectex14 - minDistToEdge*wslAxes.GetXAxis();
			else if ( dist14 > maxDistToEdge )
				edgePnt = featurePntAtVectex14 - maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			point14 = surfPnt;
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex14, newFeaturePnt);
		}
	}

	// Skip points 15 & 16

	// negative side outer point 17
	int inedx17 = 17;
	IwPoint3d point17;
	int outlineProfileFeatureIndex17 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx17);
	if ( outlineProfileFeatureIndex17 != -1 )
	{
		int remapZValue17 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex17).z;
		if ( remapZValue17 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt17 = m_cutFeatPnts.GetAt(inedx17);
			IwPoint3d featurePntAtVectex17 = cutFeatPntsAtVertexes.GetAt(inedx17);
			DistFromPointToCurve(cutFeaturePnt17, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
			IwBSplineCurve* cutEdge7 = (IwBSplineCurve*)cutEdgeCurves.GetAt(7);
			DistFromPointToCurve(cPnt, cutEdge7, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex17;
			double dist17 = tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist17 < minDistToEdge )
				edgePnt = featurePntAtVectex17 + minDistToEdge*wslAxes.GetXAxis();
			else if ( dist17 > maxDistToEdge )
				edgePnt = featurePntAtVectex17 + maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			point17 = surfPnt;
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex17, newFeaturePnt);
		}
	}

	// positive side outer point 18
	int inedx18 = 18;
	IwPoint3d point18;
	int outlineProfileFeatureIndex18 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx18);
	if ( outlineProfileFeatureIndex18 != -1 )
	{
		int remapZValue18 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex18).z;
		if ( remapZValue18 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt18 = m_cutFeatPnts.GetAt(inedx18);
			IwPoint3d featurePntAtVectex18 = cutFeatPntsAtVertexes.GetAt(inedx18);
			DistFromPointToCurve(cutFeaturePnt18, outlineProfileXYZCurve, cPnt, param);// the corresponding point on outlineProfileXYZCurve
			IwBSplineCurve* cutEdge8 = (IwBSplineCurve*)cutEdgeCurves.GetAt(8);
			DistFromPointToCurve(cPnt, cutEdge8, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex18;
			double dist18 = -tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist18 < minDistToEdge )
				edgePnt = featurePntAtVectex18 - minDistToEdge*wslAxes.GetXAxis();
			else if ( dist18 > maxDistToEdge )
				edgePnt = featurePntAtVectex18 - maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			point18 = surfPnt;
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex18, newFeaturePnt);
		}
	}

	// negative side outer point 19
	int inedx19 = 19;
	IwPoint3d point19;
	int outlineProfileFeatureIndex19 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx19);
	if ( outlineProfileFeatureIndex19 != -1 )
	{
		int remapZValue19 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex19).z;
		if ( remapZValue19 != -1 ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d cutFeaturePnt19 = m_cutFeatPnts.GetAt(inedx19);
			IwPoint3d featurePntAtVectex19 = cutFeatPntsAtVertexes.GetAt(inedx19);
			DistFromPointToCurve(cutFeaturePnt19, outlineProfileXYZCurve, cPnt, param);// the corresponding point on sideOuterCurve
			IwBSplineCurve* cutEdge8 = (IwBSplineCurve*)cutEdgeCurves.GetAt(8);
			DistFromPointToCurve(cPnt, cutEdge8, edgePnt, param);// the corresponding point on cut edge
			// check the distance to vertex
			tempVec = edgePnt - featurePntAtVectex19;
			double dist19 = tempVec.Dot(wslAxes.GetXAxis());// inner is a positive value
			if ( dist19 < minDistToEdge )
				edgePnt = featurePntAtVectex19 + minDistToEdge*wslAxes.GetXAxis();
			else if ( dist19 > maxDistToEdge )
				edgePnt = featurePntAtVectex19 + maxDistToEdge*wslAxes.GetXAxis();
			//
			DistFromPointToSurface(edgePnt, sketchSurface, surfPnt, uvParam);
			point19 = surfPnt;
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex19, newFeaturePnt);
		}
	}

	//////////////////////////////////////////////////////////
	// edge inner points
	// point 1025
	double inedx1025 = 10.25;
	int outlineProfileFeatureIndex1025 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1025);
	if ( outlineProfileFeatureIndex1025 != -1 )
	{
		int remapZValue1025 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1025).z;
		if ( remapZValue1025 != -1 && point10.IsInitialized() && point14.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt10, pnt14, pnt1025;
			double param10, param14;
			DistFromPointToCurve(point10, outlineProfileXYZCurve, pnt10, param10);
			DistFromPointToCurve(point14, outlineProfileXYZCurve, pnt14, param14);
			// Synchronize parameters 0.72 & 0.28 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.72*param10+0.28*param14, pnt1025);
			DistFromPointToSurface(pnt1025, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1025, newFeaturePnt);
		}
	}
	// point 105
	double inedx105 = 10.5;
	int outlineProfileFeatureIndex105 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx105);
	if ( outlineProfileFeatureIndex105 != -1 )
	{
		int remapZValue105 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex105).z;
		if ( remapZValue105 != -1 && point10.IsInitialized() && point14.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt10, pnt14, pnt105;
			double param10, param14;
			DistFromPointToCurve(point10, outlineProfileXYZCurve, pnt10, param10);
			DistFromPointToCurve(point14, outlineProfileXYZCurve, pnt14, param14);
			// Synchronize parameters 0.5 & 0.5 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.5*param10+0.5*param14, pnt105);
			DistFromPointToSurface(pnt105, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex105, newFeaturePnt);
		}
	}
	// point 1075
	double inedx1075 = 10.75;
	int outlineProfileFeatureIndex1075 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1075);
	if ( outlineProfileFeatureIndex1075 != -1 )
	{
		int remapZValue1075 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1075).z;
		if ( remapZValue1075 != -1 && point10.IsInitialized() && point14.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt10, pnt14, pnt1075;
			double param10, param14;
			DistFromPointToCurve(point10, outlineProfileXYZCurve, pnt10, param10);
			DistFromPointToCurve(point14, outlineProfileXYZCurve, pnt14, param14);
			// Synchronize parameters 0.28 & 0.72 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.28*param10+0.72*param14, pnt1075);
			DistFromPointToSurface(pnt1075, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1075, newFeaturePnt);
		}
	}

	// point 1425
	double inedx1425 = 14.25;
	int outlineProfileFeatureIndex1425 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1425);
	if ( outlineProfileFeatureIndex1425 != -1 )
	{
		int remapZValue1425 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1425).z;
		if ( remapZValue1425 != -1 && point14.IsInitialized() && point18.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt14, pnt18, pnt1425;
			double param14, param18;
			DistFromPointToCurve(point14, outlineProfileXYZCurve, pnt14, param14);
			DistFromPointToCurve(point18, outlineProfileXYZCurve, pnt18, param18);
			// Synchronize parameters 0.72 & 0.28 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.72*param14+0.28*param18, pnt1425);
			DistFromPointToSurface(pnt1425, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1425, newFeaturePnt);
		}
	}
	// point 145
	double inedx145 = 14.5;
	int outlineProfileFeatureIndex145 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx145);
	if ( outlineProfileFeatureIndex145 != -1 )
	{
		int remapZValue145 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex145).z;
		if ( remapZValue145 != -1 && point14.IsInitialized() && point18.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt14, pnt18, pnt145;
			double param14, param18;
			DistFromPointToCurve(point14, outlineProfileXYZCurve, pnt14, param14);
			DistFromPointToCurve(point18, outlineProfileXYZCurve, pnt18, param18);
			// Synchronize parameters 0.5 & 0.5 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.5*param14+0.5*param18, pnt145);
			DistFromPointToSurface(pnt145, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex145, newFeaturePnt);
		}
	}
	// point 1475
	double inedx1475 = 14.75;
	int outlineProfileFeatureIndex1475 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1475);
	if ( outlineProfileFeatureIndex1475 != -1 )
	{
		int remapZValue1475 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1475).z;
		if ( remapZValue1475 != -1 && point14.IsInitialized() && point18.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt14, pnt18, pnt1475;
			double param14, param18;
			DistFromPointToCurve(point14, outlineProfileXYZCurve, pnt14, param14);
			DistFromPointToCurve(point18, outlineProfileXYZCurve, pnt18, param18);
			// Synchronize parameters 0.28 & 0.72 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.28*param14+0.72*param18, pnt1475);
			DistFromPointToSurface(pnt1475, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1475, newFeaturePnt);
		}
	}

	// point 1325
	double inedx1325 = 13.25;
	int outlineProfileFeatureIndex1325 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1325);
	if ( outlineProfileFeatureIndex1325 != -1 )
	{
		int remapZValue1325 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1325).z;
		if ( remapZValue1325 != -1 && point13.IsInitialized() && point17.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt13, pnt17, pnt1325;
			double param13, param17;
			DistFromPointToCurve(point13, outlineProfileXYZCurve, pnt13, param13);
			DistFromPointToCurve(point17, outlineProfileXYZCurve, pnt17, param17);
			// Synchronize parameters 0.72 & 0.28 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.72*param13+0.28*param17, pnt1325);
			DistFromPointToSurface(pnt1325, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1325, newFeaturePnt);
		}
	}
	// point 135
	double inedx135 = 13.5;
	int outlineProfileFeatureIndex135 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx135);
	if ( outlineProfileFeatureIndex135 != -1 )
	{
		int remapZValue135 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex135).z;
		if ( remapZValue135 != -1 && point13.IsInitialized() && point17.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt13, pnt17, pnt135;
			double param13, param17;
			DistFromPointToCurve(point13, outlineProfileXYZCurve, pnt13, param13);
			DistFromPointToCurve(point17, outlineProfileXYZCurve, pnt17, param17);
			// Synchronize parameters 0.5 & 0.5 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.5*param13+0.5*param17, pnt135);
			DistFromPointToSurface(pnt135, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex135, newFeaturePnt);
		}
	}
	// point 1375
	double inedx1375 = 13.75;
	int outlineProfileFeatureIndex1375 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1375);
	if ( outlineProfileFeatureIndex1375 != -1 )
	{
		int remapZValue1375 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1375).z;
		if ( remapZValue1375 != -1 && point13.IsInitialized() && point17.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt13, pnt17, pnt1375;
			double param13, param17;
			DistFromPointToCurve(point13, outlineProfileXYZCurve, pnt13, param13);
			DistFromPointToCurve(point17, outlineProfileXYZCurve, pnt17, param17);
			// Synchronize parameters 0.28 & 0.72 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.28*param13+0.72*param17, pnt1375);
			DistFromPointToSurface(pnt1375, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1375, newFeaturePnt);
		}
	}

	// point 1725
	double inedx1725 = 17.25;
	int outlineProfileFeatureIndex1725 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1725);
	if ( outlineProfileFeatureIndex1725 != -1 )
	{
		int remapZValue1725 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1725).z;
		if ( remapZValue1725 != -1 && point17.IsInitialized() && point19.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt17, pnt19, pnt1725;
			double param17, param19;
			DistFromPointToCurve(point17, outlineProfileXYZCurve, pnt17, param17);
			DistFromPointToCurve(point19, outlineProfileXYZCurve, pnt19, param19);
			// Synchronize parameters 0.72 & 0.28 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.72*param17+0.28*param19, pnt1725);
			DistFromPointToSurface(pnt1725, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1725, newFeaturePnt);
		}
	}
	// point 175
	double inedx175 = 17.5;
	int outlineProfileFeatureIndex175 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx175);
	if ( outlineProfileFeatureIndex175 != -1 )
	{
		int remapZValue175 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex175).z;
		if ( remapZValue175 != -1 && point17.IsInitialized() && point19.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt17, pnt19, pnt175;
			double param17, param19;
			DistFromPointToCurve(point17, outlineProfileXYZCurve, pnt17, param17);
			DistFromPointToCurve(point19, outlineProfileXYZCurve, pnt19, param19);
			// Synchronize parameters 0.5 & 0.5 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.5*param17+0.5*param19, pnt175);
			DistFromPointToSurface(pnt175, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex175, newFeaturePnt);
		}
	}
	// point 1775
	double inedx1775 = 17.75;
	int outlineProfileFeatureIndex1775 = m_pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(inedx1775);
	if ( outlineProfileFeatureIndex1775 != -1 )
	{
		int remapZValue1775 = (int)m_currFeaturePointsRemap.GetAt(outlineProfileFeatureIndex1775).z;
		if ( remapZValue1775 != -1 && point17.IsInitialized() && point19.IsInitialized() ) // a remapable point, whose position is relative to cut feature point
		{
			IwPoint3d pnt17, pnt19, pnt1775;
			double param17, param19;
			DistFromPointToCurve(point17, outlineProfileXYZCurve, pnt17, param17);
			DistFromPointToCurve(point19, outlineProfileXYZCurve, pnt19, param19);
			// Synchronize parameters 0.28 & 0.72 with "CFemoralCuts::DetermineEdgeFeaturePoints()"
			outlineProfileXYZCurve->EvaluatePoint(0.28*param17+0.72*param19, pnt1775);
			DistFromPointToSurface(pnt1775, sketchSurface, surfPnt, uvParam);
			// update the feature point
			IwPoint3d newFeaturePnt = IwPoint3d(uvParam.x, uvParam.y, 0);
			m_currFeaturePoints.SetAt(outlineProfileFeatureIndex1775, newFeaturePnt);
		}
	}

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	if ( outlineProfileXYZCurve )
		IwObjDelete(outlineProfileXYZCurve);

	return;
}

/////////////////////////////////////////////////////////////////
// If manActType == MAN_ACT_TYPE_REFINE_CONVERGE, execute ResampleFeaturePoints(), which
// does not really "modify" outline profile but move the feature points
// along the profile to the cut edges, and adjust to be 0~3mm away from cut edge verteces.
/////////////////////////////////////////////////////////////////
void OutlineProfileMgrModel::Refine_Converge(CManagerActivateType manActType)
{
	//
	ResampleFeaturePoints();

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	/////////////////////////////////////////////////////////////////
	// Check anterior overhanging in the lateral side 
	RefineByValidation_LateralAnteriorEar();
	
	/////////////////////////////////////////////////////////////////
	// Check asymmetric ear in the medial side 
	RefineByValidation_MedialAnteriorEar();

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	// Run validation to see any further modification is needed.
	RefineByValidation_Overall();

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);

	////////////////////////////////////////////////////////////////
	// Refine notch profile to satisfy 7~12mm to origin and 18mm width
	RefineNotchProfile();

	/////////////////////////////////////////////////////////////////
	// Update to outline profile
	m_pDoc->GetOutlineProfile()->SetOutlineProfileFeaturePoints(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);


}

void OutlineProfileMgrModel::CancelChanges()
{
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();

	if (outlineProfile)
		outlineProfile->SetOutlineProfileFeaturePoints(m_oldFeaturePoints, m_oldFeaturePointsInfo, m_oldFeaturePointsRemap);
}

bool OutlineProfileMgrModel::ConvertPoint(int caughtIndex)
{
	// If there is any caught point, we need to check 
	// whether it is a deletable arc center point (info=11 or 12), 
	// or interpolation point (info=0).
	if ( caughtIndex == 0 )// seam point
		return false;
	int statusInfo = m_currFeaturePointsInfo.GetAt(caughtIndex);
	int remapZValue0 = (int)m_currFeaturePointsRemap.GetAt(caughtIndex).z;
	int remapZValue2 = (int)(100*(m_currFeaturePointsRemap.GetAt(caughtIndex).z-remapZValue0));
	if ( statusInfo != 0 && statusInfo != 11 && statusInfo != 12 ) // see OutlineProfile.h for the meanings of statusInfo = 0, 11, & 12.
		return false;
	bool changed = false;

	if ( statusInfo == 0 )// a interpolation point
	{
		if ( remapZValue0 == -1 && remapZValue2 == 0 ) // A pure non remap-able point. Can be converted into an arc.
		{
			changed = ConvertToArc(caughtIndex, m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
		}
		else if ( remapZValue0 == -1 && remapZValue2 != 0) // a non remap-able point now, but can be converted into remap-able point
		{
			changed = ConvertToRemapablePoint(caughtIndex, true, m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
		}
		else if ( remapZValue0 != -1 ) // a remap-able point now
		{
			changed = ConvertToRemapablePoint(caughtIndex, false, m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
		}
	}
	else if ( statusInfo == 11 || statusInfo == 12 ) // a collapsible arc
	{
		changed = ConvertToPoint(caughtIndex, m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap);
	}

	return changed;
}

bool OutlineProfileMgrModel::ConvertToRemapablePoint
(
	int index,								// I:
	bool bConvertToRemapable,				// I: if true, convert it to remap-able point. otherwise, convert it to non remap-able point
	IwTArray<IwPoint3d>& featurePoints,		// I/O: 
	IwTArray<int>& featurePointsInfo,		// I/O:
	IwTArray<IwPoint3d>& featurePointsRemap // I/O:
)
{
	bool changed = false;

	int statusInfo = featurePointsInfo.GetAt(index);
	if ( statusInfo != 0 ) // only interpolation point can be converted into (non) remap-able point
		return false;

	IwPoint3d remap = featurePointsRemap.GetAt(index);
	int remapZValue0 = (int)remap.z;
	int remapZValue2 = (int)(-100*(remap.z-remapZValue0));

	if ( bConvertToRemapable )
	{
		// the original input point should be non remap-able (remapZValue0 == -1), but can be converted (remapZValue2 != 0).
		if ( remapZValue0 == -1 && remapZValue2 != 0 )
		{
			IwPoint3d newRemap = IwPoint3d(remap.x, remap.y, remapZValue2);
			featurePointsRemap.SetAt(index, newRemap);
			changed = true;
		}
	}
	else // convert to non remap-able
	{
		// the original input point should be remap-able (remapZValue0 != -1).
		if ( remapZValue0 != -1 )
		{
			double newZValue = -(1.0 + remap.z/100.0);
			IwPoint3d newRemap = IwPoint3d(remap.x, remap.y, newZValue);
			featurePointsRemap.SetAt(index, newRemap);
			changed = true;
		}
	}

	return changed;
}

bool OutlineProfileMgrModel::ConvertToPoint
(
	int index,							// I:
	IwTArray<IwPoint3d>& featurePoints,	// I/O: 
	IwTArray<int>& featurePointsInfo,	// I/O:
	IwTArray<IwPoint3d>& featurePointsRemap // I/O:
)
{
	int statusInfo = featurePointsInfo.GetAt(index);
	if ( statusInfo != 11 && statusInfo != 12 ) // only collapsible arcs can be converted to points (and curves nearby)
		return false;

	COutlineProfile* pOutlineProfile = m_pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		return false;

	// Get UVCurve
	IwBSplineCurve *crvOnSurf, *UVCurve;
	pOutlineProfile->GetCurveOnSketchSurface(crvOnSurf, UVCurve, false);
	IwExtent1d tDom = UVCurve->GetNaturalInterval();

	// get the start and end points of the arc.
	IwPoint3d startPoint, endPoint;
	startPoint = featurePoints.GetAt(index-1);
	endPoint = featurePoints.GetAt(index+1);

	// determine these 2 points on UVCurve parametric domain
	double startParam, endParam;
	IwPoint3d cPnt;
	DistFromPointToCurve(startPoint, UVCurve, cPnt, startParam);
	DistFromPointToCurve(endPoint, UVCurve, cPnt, endParam);
	// middle point is the new interpolation point
	double midParam = 0.5*(startParam+endParam);
	IwPoint3d intPoint;
	UVCurve->EvaluatePoint(midParam, intPoint);

	// Remove start/center/end points of the arc
	featurePoints.RemoveAt(index+1);
	featurePointsInfo.RemoveAt(index+1);
	featurePointsRemap.RemoveAt(index+1);
	featurePoints.RemoveAt(index);
	featurePointsInfo.RemoveAt(index);
	featurePointsRemap.RemoveAt(index);
	featurePoints.RemoveAt(index-1);
	featurePointsInfo.RemoveAt(index-1);
	featurePointsRemap.RemoveAt(index-1);

	// Insert the new interpolation point at (index-1) position
	featurePoints.InsertAt(index-1, intPoint);
	featurePointsInfo.InsertAt(index-1, 0);
	featurePointsRemap.InsertAt(index-1, IwPoint3d(0,0,-1));

	return true;
}

void OutlineProfileMgrModel::MovePoint(int caughtIndex, IwPoint2d surfParam)
{
	Qt::KeyboardModifiers keyModifier = QApplication::keyboardModifiers();
	IwPoint3d prevUVPnt = m_currFeaturePoints.GetAt(caughtIndex);
	// update the UVPnt
	IwPoint3d interUVPnt = IwPoint3d(surfParam.x, surfParam.y, 0);
	m_currFeaturePoints.SetAt(caughtIndex, interUVPnt);
	// if it is an arc center point
	if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 1 || m_currFeaturePointsInfo.GetAt(caughtIndex) == 2 ||
			m_currFeaturePointsInfo.GetAt(caughtIndex) == 11 || m_currFeaturePointsInfo.GetAt(caughtIndex) == 12 )
	{
		if ( keyModifier == Qt::ControlModifier ) // control key is down
		{
			IwVector3d deltaVec = interUVPnt - prevUVPnt;
			IwPoint3d arcSatrt = m_currFeaturePoints.GetAt(caughtIndex-1);
			m_currFeaturePoints.SetAt(caughtIndex-1, arcSatrt+deltaVec);
			IwPoint3d arcEnd = m_currFeaturePoints.GetAt(caughtIndex+1);
			m_currFeaturePoints.SetAt(caughtIndex+1, arcEnd+deltaVec);
		}
	}
	// if it is the anterior indentation point
	else if ( caughtIndex == 0 ) 
	{
		if ( keyModifier == Qt::ControlModifier ) // control key is down
		{
			IwVector3d deltaVec = interUVPnt - prevUVPnt;
			IwPoint3d pnt;
			if ( m_currFeaturePointsInfo.GetAt(caughtIndex+2) == 11 ) // arc
			{
				pnt = m_currFeaturePoints.GetAt(caughtIndex+1);// arc start point
				m_currFeaturePoints.SetAt(caughtIndex+1, pnt+deltaVec);
				pnt = m_currFeaturePoints.GetAt(caughtIndex+2);// arc center point
				m_currFeaturePoints.SetAt(caughtIndex+2, pnt+deltaVec);
				pnt = m_currFeaturePoints.GetAt(caughtIndex+3);// arc end point
				m_currFeaturePoints.SetAt(caughtIndex+3, pnt+deltaVec);
			}
			else // interpolation point
			{
				pnt = m_currFeaturePoints.GetAt(caughtIndex+1);
				m_currFeaturePoints.SetAt(caughtIndex+1, pnt+deltaVec);
			}
			//
			unsigned nsize = m_currFeaturePointsInfo.GetSize();
			if ( m_currFeaturePointsInfo.GetAt(nsize-3) == 11 ) // arc
			{
				pnt = m_currFeaturePoints.GetAt(nsize-4);// arc start point
				m_currFeaturePoints.SetAt(nsize-4, pnt+deltaVec);
				pnt = m_currFeaturePoints.GetAt(nsize-3);// arc center point
				m_currFeaturePoints.SetAt(nsize-3, pnt+deltaVec);
				pnt = m_currFeaturePoints.GetAt(nsize-2);// arc end point
				m_currFeaturePoints.SetAt(nsize-2, pnt+deltaVec);
			}
			else// interpolation point
			{
				pnt = m_currFeaturePoints.GetAt(nsize-2);
				m_currFeaturePoints.SetAt(nsize-2, pnt+deltaVec);
			}
		}
	}
	// if it is an arc starting point
	else if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 3 )
	{
		if ( m_currFeaturePointsInfo.GetAt(caughtIndex+1) == 1 || m_currFeaturePointsInfo.GetAt(caughtIndex+1) == 2 )// not collapsible
		{
			IwPoint3d arcEnd = m_currFeaturePoints.GetAt(caughtIndex+2);
			IwVector3d deltaVec = interUVPnt - arcEnd;
			if ( deltaVec.Length() < 3.5 )// too close to define an arc
			{
				interUVPnt = prevUVPnt;
				m_currFeaturePoints.SetAt(caughtIndex, interUVPnt);// set it back to original position
			}
		}
	}
	// if it is an arc ending point
	else if ( m_currFeaturePointsInfo.GetAt(caughtIndex) == 4 )
	{
		if ( m_currFeaturePointsInfo.GetAt(caughtIndex-1) == 1 || m_currFeaturePointsInfo.GetAt(caughtIndex-1) == 2 )// not collapsible
		{
			IwPoint3d arcStart = m_currFeaturePoints.GetAt(caughtIndex-2);
			IwVector3d deltaVec = interUVPnt - arcStart;
			if ( deltaVec.Length() < 3.5 )// too close to define an arc
			{
				interUVPnt = prevUVPnt;
				m_currFeaturePoints.SetAt(caughtIndex, interUVPnt);// set it back to original position
			}
		}
	}

	// Get cut feature points to update remap
	IwTArray<IwPoint3d> idealUVFeatPnts;
	IwTArray<int> idealUVFeatPntsInfo;
	IwTArray<IwPoint3d> idealUVFeatPntsRemap;
	bool rOK = ConvertCutFeatPntsToOutlineFeatPnts(m_cutFeatPnts, m_antOuterPnts, &m_edgeMiddlePoints, idealUVFeatPnts, idealUVFeatPntsInfo, idealUVFeatPntsRemap);
	if ( !rOK ) return;

	// update the Remap, only the remap-able points need such info.
	// When users move a ctrl point, the remap is always the movement from ideal to cursor released location. 
	int zValue = (int)m_currFeaturePointsRemap.GetAt(caughtIndex).z;
	if ( zValue != -1 ) // Remap (,,-1) indicates it is not a reamp-able point.
	{
		IwPoint3d idealUVPnt = idealUVFeatPnts.GetAt(caughtIndex);
		IwVector3d tempVec = interUVPnt - idealUVPnt; // move from ideal to the cursor location
		double remapU = tempVec.x;
		double remapV = tempVec.y;
		double referPnt = m_currFeaturePointsRemap.GetAt(caughtIndex).z;
		m_currFeaturePointsRemap.SetAt(caughtIndex, IwPoint3d(remapU, remapV, referPnt));
	}
}

int OutlineProfileMgrModel::AddPoint(IwPoint2d surfParam)
{
	IwPoint3d interUVPnt = IwPoint3d(surfParam.x, surfParam.y, 0);
	return InsertAFeaturePointByOrder(m_currFeaturePoints, m_currFeaturePointsInfo, m_currFeaturePointsRemap, interUVPnt);
}

int OutlineProfileMgrModel::InsertAFeaturePointByOrder
(
	IwTArray<IwPoint3d>& featPoints,		// I/O:
	IwTArray<int>& featPointsInfo,			// I/O:
	IwTArray<IwPoint3d>& featPointsRemap,	// I/O:
	IwPoint3d& insertPoint					// I:
)
{
	IwPoint3d prevPnt, postPnt;
	IwVector3d baseVec, prevVec, postVec, crossVec;
	double minDist = 100000, dist;
	int insertPos = -1;

	for (unsigned i=0; i<(featPointsInfo.GetSize()-1); i++) 
	{
		// The insert should not be in an arc 
		// First, check i and i+1 are arc start/center points
		if ( featPointsInfo.GetAt(i) == 3 &&
			 (featPointsInfo.GetAt(i+1)==1 || featPointsInfo.GetAt(i+1)==2 || featPointsInfo.GetAt(i+1)==11 || featPointsInfo.GetAt(i+1)==12 ) ) 
			 continue;
		// Second, check i and i+1 are arc center/end points
		if ( (featPointsInfo.GetAt(i)==1 || featPointsInfo.GetAt(i)==2 || featPointsInfo.GetAt(i)==11 || featPointsInfo.GetAt(i)==12 ) &&
			 featPointsInfo.GetAt(i+1) == 4 ) 
			 continue;
		
		prevPnt = featPoints.GetAt(i);
		postPnt = featPoints.GetAt(i+1);
		baseVec = postPnt - prevPnt;
		baseVec.Unitize();
		prevVec = insertPoint - prevPnt;
		postVec = insertPoint - postPnt;

		if ( baseVec.Dot(prevVec)*baseVec.Dot(postVec) < 0 ) // the insert point is near the segment by the i and i+1 points.
		{
			crossVec = baseVec*prevVec;
			dist = crossVec.Length();
			if (minDist > dist)
			{
				minDist = dist;
				insertPos = i+1;
			}
		}
	}

	if (insertPos != -1 && minDist < 10)// found the best insert position
	{
		featPoints.InsertAt(insertPos, insertPoint);
		featPointsInfo.InsertAt(insertPos, 0); // only interpolation point can be inserted.
		featPointsRemap.InsertAt(insertPos, IwPoint3d(0,0,-1)); // "-1" indicates this is not a remap-able point.

		return insertPos;
	}

	return -1;
}

bool OutlineProfileMgrModel::DeletePoint(int caughtIndex)
{
	// Check whether the caught point is delete-able
	if ( caughtIndex == 0 )// the first point can NOT be deleted. 
		return false; 
	// only interpolation points can be deleted. 
	if ( m_currFeaturePointsInfo.GetAt(caughtIndex) != 0 ) // interpolation point
		return false;
	// Edge points can NOT be deleted
	IwPoint3d remap = m_currFeaturePointsRemap.GetAt(caughtIndex);
	int zValue = (int)remap.z;
	// If it is a cut edge vertex point, its z will be an integer.
	if ( zValue > 0 && IS_EQ_TOL6(zValue,remap.z) ) 
		return false;

	// Delete the point
	m_currFeaturePoints.RemoveAt(caughtIndex);
	m_currFeaturePointsInfo.RemoveAt(caughtIndex);
	m_currFeaturePointsRemap.RemoveAt(caughtIndex);

	return true;
}

/////////////////////////////////////////////////////////////////
// This function currently only refines the anterior outer points
// the 2 points above the ant/ant-chamfer cut edge.
/////////////////////////////////////////////////////////////////
void OutlineProfileMgrModel::RefineAnteriorOuterPoints
(
	IwTArray<IwPoint3d>& cutFeatPnts,	// I:
	IwTArray<IwPoint3d>& antOuterPnts	// I/O:
)
{
	CFemoralCuts* pFemoralCuts = m_pDoc->GetFemoralCuts();
	if ( pFemoralCuts == NULL )
		return;

	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	IwAxis2Placement sweepAxes;
	pFemoralAxes->GetSweepAxes(sweepAxes);

	double cutToEdgeDist = m_pDoc->GetVarTableValue("OUTLINE PROFILE DESIRED DISTANCE TO CUT EDGE");

	IwTArray<IwFace*> cutFaces;
	int antIndex = 9;
	pFemoralCuts->SearchCutFaces(antIndex, cutFaces);

	//////////////////////////////////////////////////////////////
	// determine the positive side 
	IwPoint3d posEdgePnt = cutFeatPnts.GetAt(18);
	IwPoint3d posAntSidePnt = antOuterPnts.GetAt(6);
	IwPoint3d posMidPnt = 0.5*posEdgePnt + 0.5*posAntSidePnt;
	// Determine the closest point on the face edges
	double dist, minDist = HUGE_DOUBLE;
	IwPoint3d cPnt, closestPnt;
	double param;
	for (unsigned i=0; i<cutFaces.GetSize(); i++)
	{
		IwTArray<IwEdge*> edges;
		cutFaces.GetAt(i)->GetEdges(edges);
		for (unsigned j=0; j<edges.GetSize(); j++)
		{
			IwBSplineCurve* crv = (IwBSplineCurve*)edges.GetAt(j)->GetCurve();
			IwExtent1d crvDom = edges.GetAt(j)->GetInterval();
			dist = DistFromPointToCurve(posMidPnt, crv, cPnt, param, &crvDom);
			if ( dist < minDist )
			{
				minDist = dist;
				closestPnt = cPnt;
			}
		}
	}

	if ( minDist < 1000 )// got the closest point
	{
		IwPoint3d pnt = closestPnt - cutToEdgeDist*sweepAxes.GetXAxis();
		antOuterPnts.SetAt(7, pnt);
	}

	//////////////////////////////////////////////////////////////
	// determine the negative side 
	IwPoint3d negEdgePnt = cutFeatPnts.GetAt(19);
	IwPoint3d negAntSidePnt = antOuterPnts.GetAt(2);
	IwPoint3d negMidPnt = 0.5*negEdgePnt + 0.5*negAntSidePnt;
	// Determine the closest point on the face edges
	minDist = HUGE_DOUBLE;
	for (unsigned i=0; i<cutFaces.GetSize(); i++)
	{
		IwTArray<IwEdge*> edges;
		cutFaces.GetAt(i)->GetEdges(edges);
		for (unsigned j=0; j<edges.GetSize(); j++)
		{
			IwBSplineCurve* crv = (IwBSplineCurve*)edges.GetAt(j)->GetCurve();
			IwExtent1d crvDom = edges.GetAt(j)->GetInterval();
			dist = DistFromPointToCurve(negMidPnt, crv, cPnt, param, &crvDom);
			if ( dist < minDist )
			{
				minDist = dist;
				closestPnt = cPnt;
			}
		}
	}

	if ( minDist < 1000 )// got the closest point
	{
		IwPoint3d pnt = closestPnt + cutToEdgeDist*sweepAxes.GetXAxis();
		antOuterPnts.SetAt(1, pnt);
	}

	return;
}

void OutlineProfileMgrModel::SetModelData(const IwTArray<IwPoint3d>& featurePoints, const IwTArray<int>& pointsInfo, const IwTArray<IwPoint3d>& pointsRemap)
{
	m_currFeaturePoints.RemoveAll();
	m_currFeaturePointsInfo.RemoveAll();
	m_currFeaturePointsRemap.RemoveAll();

	m_currFeaturePoints.Append(featurePoints);
	m_currFeaturePointsInfo.Append(pointsInfo);
	m_currFeaturePointsRemap.Append(pointsRemap);
}


void OutlineProfileMgrModel::RefineNotchProfile()
{
	//COutlineProfile* pOutlineProfile = m_pDoc->GetOutlineProfile();

	//// get side info
	//bool positiveSideIsLateral = m_pDoc->IsPositiveSideLateral();

	////// notch arc 7~12 mm ////////////////////////////////////////////////////////
	//// Search for the notch arc
	//int notchArcCenterIndex = -1;
	//for (unsigned i=0; i<m_currFeaturePointsInfo.GetSize(); i++)
	//{
	//	if ( m_currFeaturePointsInfo.GetAt(i) == 2 ) // notch arc is the only arc going CW=2 direction
	//	{
	//		notchArcCenterIndex = i;
	//	}
	//}
	//if ( notchArcCenterIndex == -1 )
	//	return;

	//double maxDist = m_pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MAX DISTANCE TO ORIGIN") - 0.1;// 0.1 as tolerance
	//double minDist = m_pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MIN DISTANCE TO ORIGIN") + 0.1;// 0.1 as tolerance

	//// determine coordinate
	//IwAxis2Placement wslAxes;
	//m_pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
	//// determine the outline curve
	//IwBSplineCurve *crvOnSurf, *UVCurve;
	//m_pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, UVCurve, false);
	//// Get trochlear curve
	//IwBSplineCurve *tCurve = (IwBSplineCurve*)m_pDoc->GetJCurves()->GetJCurve(2);

	//// Make notch arc center point on tCurve medially 1.0mm
	//IwPoint3d arcCenterUV = m_currFeaturePoints.GetAt(notchArcCenterIndex);
	//IwPoint3d arcCenter3d;
	//IwVector3d normal;
	//pOutlineProfile->ConvertSketchSurfaceUVtoXYZ(arcCenterUV, arcCenter3d, normal);
	//IwPoint3d cPnt;
	//double param;
	//DistFromPointToCurve(arcCenter3d, tCurve, cPnt, param);
	//if ( positiveSideIsLateral )
	//	cPnt = cPnt - wslAxes.GetXAxis(); // away from tCurve 1mm medially
	//else
	//	cPnt = cPnt + wslAxes.GetXAxis(); // away from tCurve 1mm medially
	//IwVector3d tempVec = cPnt - arcCenter3d;
	//double distToMoveML = tempVec.Dot(wslAxes.GetXAxis());
	//// NOTE - distToMoveML is 3d distance, which is the distance from (cPnt - arcCenter3d) projected onto sketch surface
	////        Since outline profile sketch surface is parametrization with arc length, it is OK to use
	////        this 3d distance in the UV space to move notch arc center.

	//// determine the notch tip distance to origin
	//IwPoint3d farAntPoint = wslAxes.GetOrigin() + 2500*wslAxes.GetYAxis();
	//IwExtent1d crvDom = crvOnSurf->GetNaturalInterval();
	//IwExtent1d searchDom = IwExtent1d(crvDom.Evaluate(0.4), crvDom.Evaluate(0.6));
	//IwPoint3d notchPoint;
	//double notchParam;
	//DistFromPointToCurve(farAntPoint, crvOnSurf, notchPoint, notchParam, &searchDom);
	//IwVector3d vec = notchPoint - wslAxes.GetOrigin();
	//double dotValue = vec.Dot(wslAxes.GetYAxis());
	//double distAlongY = fabs(dotValue);

	//// 
	//double distToMove = 0.0;
	//if ( distAlongY < minDist )
	//{
	//	distToMove = minDist - distAlongY;// positive value, move posteriorly
	//}
	//else if ( distAlongY > maxDist)
	//{
	//	distToMove = maxDist - distAlongY;// negative value, move anteriorly
	//}

	////
	//IwPoint3d arcStart = m_currFeaturePoints.GetAt(notchArcCenterIndex-1) + IwPoint3d(distToMoveML,distToMove,0);// move along u and v direction on sketch surface
	//IwPoint3d arcCenter = m_currFeaturePoints.GetAt(notchArcCenterIndex) + IwPoint3d(distToMoveML,distToMove,0);// move along u and v direction on sketch surface
	//IwPoint3d arcEnd = m_currFeaturePoints.GetAt(notchArcCenterIndex+1) + IwPoint3d(distToMoveML,distToMove,0);// move along u and v direction on sketch surface
	//
	////
	//m_currFeaturePoints.SetAt(notchArcCenterIndex-1, arcStart);
	//m_currFeaturePoints.SetAt(notchArcCenterIndex, arcCenter);
	//m_currFeaturePoints.SetAt(notchArcCenterIndex+1, arcEnd);


	//// Notch 18mm width ////////////////////////////////////////////////
	//// DetermineNotchEighteenWidthPoints()
	//DetermineNotchEighteenWidthPoints();
	//if ( m_notchEighteenWidthPoints.GetSize() == 0 )// Something wrong, skip refine notch profile.
	//	return;

	//// If both control point are outside of 18mm, move them in.
	//// the positive 18mm width point
	//IwPoint3d posEighteenWidthPoint = m_notchEighteenWidthPoints.GetAt(1) - 0.075*wslAxes.GetXAxis();
	//// Convert to sketch UV domain
	//IwPoint3d posEighteenWidthPointUV;
	//ProjectXYZtoSketchSurfaceUV(posEighteenWidthPoint, posEighteenWidthPointUV);
	//// looking for the positive control point
	//int posIndex = -1;
	//// Convert the pointIndex (edge point index) to the position index 
	//for (unsigned i=0; i<m_currFeaturePointsRemap.GetSize(); i++)
	//{
	//	// on Remap (_,_,z) = reference edge point position
	//	double edgeIndexI = m_currFeaturePointsRemap.GetAt(i).z;
	//	if ( edgeIndexI >= 0 )// (,,-1) indicates it is not a remap-able point
	//	{
	//		if ( IS_EQ_TOL6(edgeIndexI, 11.0) )// positive side control point for 18mm is "11.0" in remap
	//		{
	//			if ( m_currFeaturePointsInfo.GetAt(i) == 0 )// this point has to be an interpolation point (cannot be an arc point)
	//			{
	//				posIndex = i;
	//				break;
	//			}
	//		}
	//	}
	//} 
	//// Got positive side 18mm control point
	//if ( posIndex != -1 )
	//{
	//	// Find the closest point to the UVCurve
	//	IwPoint3d closestPoint;
	//	double param;
	//	DistFromPointToCurve(posEighteenWidthPointUV, UVCurve, closestPoint, param);
	//	IwVector3d tempVec = posEighteenWidthPointUV - closestPoint;
	//	double length = tempVec.Length();
	//	tempVec.Unitize();
	//	if ( tempVec.x < 0 )// if the posEighteenWidthPointUV is inside of UVCurve
	//	{
	//		IwPoint3d insidePnt =  closestPoint + (length + 0.15)*tempVec;// 0.15 as safe factor
	//		m_currFeaturePoints.SetAt(posIndex, insidePnt);
	//	}
	//}

	//// For negative side 18mm width point
	//IwPoint3d negEighteenWidthPoint = m_notchEighteenWidthPoints.GetAt(2) + 0.075*wslAxes.GetXAxis();
	//// Convert to sketch UV domain
	//IwPoint3d negEighteenWidthPointUV;
	//ProjectXYZtoSketchSurfaceUV(negEighteenWidthPoint, negEighteenWidthPointUV);
	//// looking for the positive control point
	//int negIndex = -1;
	//// Convert the pointIndex (edge point index) to the position index 
	//for (unsigned i=0; i<m_currFeaturePointsRemap.GetSize(); i++)
	//{
	//	// on Remap (_,_,z) = reference edge point position
	//	double edgeIndexI = m_currFeaturePointsRemap.GetAt(i).z;
	//	if ( edgeIndexI >= 0 )// (,,-1) indicates it is not a remap-able point.
	//	{
	//		if ( IS_EQ_TOL6(edgeIndexI, 12.0) )// negative side control point for 18mm is "12.0" in remap
	//		{
	//			if ( m_currFeaturePointsInfo.GetAt(i) == 0 )// this point has to be an interpolation point (cannot be an arc point)
	//			{
	//				negIndex = i;
	//				break;
	//			}
	//		}
	//	}
	//} 
	//// if got the point
	//if ( negIndex != -1 )
	//{
	//	// Find the closest point to the UVCurve
	//	IwPoint3d closestPoint;
	//	double param;
	//	DistFromPointToCurve(negEighteenWidthPointUV, UVCurve, closestPoint, param);
	//	IwVector3d tempVec = negEighteenWidthPointUV - closestPoint;
	//	double length = tempVec.Length();
	//	tempVec.Unitize();
	//	if ( tempVec.x > 0 )// if the negEighteenWidthPointUV is inside of UVCurve
	//	{
	//		IwPoint3d insidePnt =  closestPoint + (length + 0.15)*tempVec;// 0.15 as safe factor
	//		m_currFeaturePoints.SetAt(negIndex, insidePnt);
	//	}
	//}

}

//////////////////////////////////////////////////////////
// This function checks whether the caught control point 
// represents a cut edge vertex point. 
//////////////////////////////////////////////////////////
bool OutlineProfileMgrModel::IsCriticalThicknessControlPoint(int caughtIndex)
{
	bool criticalThicknessPoint = false;

	if ( caughtIndex < 0 || caughtIndex > (m_currFeaturePointsRemap.GetSize()-1) )
		return false;

	IwPoint3d remap = m_currFeaturePointsRemap.GetAt(caughtIndex);
	int zValue = (int)remap.z;

	// remap z represents the corresponding cut edge vertex point or intermediate point.  
	// If it is a cut edge vertex point, its z will be an integer.
	if ( zValue > 0 && IS_EQ_TOL6(zValue,remap.z) ) 
		criticalThicknessPoint = true;

	return criticalThicknessPoint;
}

double OutlineProfileMgrModel::GetControlPointReferenceEdgeInfo(int index)
{
	IwPoint3d remap = m_currFeaturePointsRemap.GetAt(index);
	double zValue = remap.z;

	return zValue;
}