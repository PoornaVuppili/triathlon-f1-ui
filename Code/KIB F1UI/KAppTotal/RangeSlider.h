#pragma once

#include <QWidget>

class CRangeSlider : public QWidget
	{
	Q_OBJECT

public:
	CRangeSlider( QWidget *parent );
	~CRangeSlider();

	void				SetRange( double left, double right );

	virtual void		paintEvent( QPaintEvent* event );
    virtual void		mousePressEvent(QMouseEvent *event);
    virtual void		mouseMoveEvent(QMouseEvent *event);
    virtual void		mouseReleaseEvent(QMouseEvent *event);

signals:
	void				sigRangeChanged( double, double );

protected:
	
	enum _TrackMode 
		{
		TRACK_NONE,			// No tracking
		TRACK_LEFT,			// Left Arrow is being slided
		TRACK_RIGHT,		// Right Arrow is being slided
		TRACK_MIDDLE,		// Middle Area is being slided
		} 
	m_eTrackMode; 

	void				PaintArrowButton( QRect& rect, QPoint poly[3], bool bDeflate );
	void				DrawButton( QRect rect, bool bDeflate, bool bLeft  );
	

	double		m_left;
	double		m_right;
	int			m_min;
	int			m_max;
	int			m_xLast;

	int			m_left_x0;
	int			m_left_x1;
	int			m_right_x0;
	int			m_right_x1;
	
	int			m_nButtonWidth;

	int			m_dx;

	int			m_width;
	int			m_height;

	QPainter*	m_pPainter;
	};
