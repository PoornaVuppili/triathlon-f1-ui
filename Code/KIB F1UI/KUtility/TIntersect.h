///////////////////////////////////////////////////////////////////////
//	File:    TIntersect.h
//	Purpose: A template class for finding the intersection between objects
//     of different types.
//
//	Author:	 Tong Zhang
//  Copyright 2008 Stryker, Inc.
///////////////////////////////////////////////////////////////////////

#ifndef TINTERSECT_H
#define TINTERSECT_H

#include <limits>
#include "TLine3.h"
#include "TLineSegment3.h"
#include "TPlane3.h"

///////////////////////////////////////////////////////////////////////
// Function name:    Intersect
// Function purpose: Find the point of intersection between a line and a line segment
// Input:
//   lineSeg: the line segment
//   line: the line
// Output:
//   intersectPoint: the point of intersection if exists
// Return: an intersect flag
//   IntersectFlag::Coincident, intersectPoint is undefined
//   IntersectFlag::Parallel, intersectPoint is undefined
//   IntersectFlag::Intersect, intersectPoint is the point of intersection
//   IntersectFlag::NonIntersect, intersectPoint is the point that is
//                                the closest to the other line
///////////////////////////////////////////////////////////////////////
template <typename T>
int Intersect(const TLineSegment3<T>& lineSeg, const TLine3<T>& line, TPoint3<T>& intersectPoint, T distTolerance = NumTraits<T>::dummy_precision())
{
    TPoint3<T> p1 = lineSeg.GetP1();
    TPoint3<T> p2 = lineSeg.GetP2();

    if( lineSeg.IsParallel(line) )
    {
        return line.Distance(p1)<distTolerance ?
            IntersectFlag::Coincident : IntersectFlag::Parallel;
    }

    TPoint3<T> p3 = line.GetOrigin();
    TPoint3<T> p4 = p3 + line.GetDirection();

    TPoint3<T> p13 = p1 - p3;
    TPoint3<T> p43 = p4 - p3;
    TPoint3<T> p21 = p2 - p1;

    double d1343 = p13.dot(p43);
    double d4321 = p43.dot(p21);
    double d1321 = p13.dot(p21);
    double d4343 = p43.dot(p43);
    double d2121 = p21.dot(p21);

    double denom = d2121 * d4343 - d4321 * d4321;
    double numer = d1343 * d4321 - d1321 * d4343;

    double mua = numer / denom;
    double mub = (d1343 + d4321 * mua) / d4343;

    const double mtol = distTolerance/p1.Distance(p2);

    if( mua>=-mtol && mua<=1+mtol )
    {
        TPoint3<T> pmua = p1 + ((T)mua) * p21;
        TPoint3<T> pmub = p3 + ((T)mub) * p43;

        if( pmua.Distance(pmub) < distTolerance )
        {
            intersectPoint = (pmua+pmub)/2;
            return IntersectFlag::Intersect;
        }
        else
            intersectPoint = pmua;
    }
    else if( mua<0 )
        intersectPoint = p1;
    else
        intersectPoint = p2;
    return IntersectFlag::NonIntersect;
}

template <typename T>
int Intersect(const TLine3<T>& line, const TLineSegment3<T>& lineSeg, TPoint3<T>& intersectPoint, T distTolerance = NumTraits<T>::dummy_precision())
{
    return Intersect(lineSeg, line, intersectPoint, distTolerance);
}

///////////////////////////////////////////////////////////////////////
// Function name:    Intersect
// Function purpose: Determine if the line intersects the plane, if so
//   find the point of intersection
// Input:
//   plane: the plane object
//   line : the line object
//   tol  : tolerance for coincident/parallel detection
// Output:
//   point : the point of intersection if exists
// Return:
//   IntersectFlag::Intersect: there is intersection, and the 'point' 
//      contains the coordinate of the point of intersection;
//   IntersectFlag::Parallel: the line and plane are paralle, and the 
//      'point' is undefined;
//   IntersectFlag::Coincident: the line is on the plane, and the 
//      'point' is undefined.
///////////////////////////////////////////////////////////////////////
template <typename T>
int Intersect(const TPlane3<T>& plane, const TLine3<T>& line, TPoint3<T>& point, T tol = NumTraits<T>::dummy_precision())
{
    TVector3<T> n = plane.GetPlaneNormal();
    T v = plane.GetPlaneValue();

    TPoint3<T> o = line.GetOrigin();
    TVector3<T> d = line.GetDirection();

    T nom = n.dot(o) + v;
    T denom = n.dot(d);

    if( fabs(denom) <= tol )
    {
        return ( fabs(nom) <= tol ) ? IntersectFlag::Coincident : IntersectFlag::Parallel;
    }
    else
    {
        point = o - (nom/denom) * d;
        return IntersectFlag::Intersect;
    }
}

template <typename T>
int Intersect(const TLine3<T>& line, const TPlane3<T>& plane, TPoint3<T>& point, T tol = NumTraits<T>::dummy_precision())
{
    return Intersect(plane, line, point, tol);
}

///////////////////////////////////////////////////////////////////////
// Function name:    Intersect
// Function purpose: Determine if the line segment intersects the plane, if so
//   find the point of intersection
// Input:
//   plane: the plane object
//   lineSeg : the line segment object
//   tol  : tolerance for coincident/parallel detection
// Output:
//   point : the point of intersection if exists
// Return:
//   IntersectFlag::Intersect: there is intersection, and the 'point' 
//      contains the coordinate of the point of intersection;
//   IntersectFlag::Parallel: the line segment and plane are parallel, and the 
//      'point' is undefined;
//   IntersectFlag::Coincident: the line segment is on the plane, and the 
//      'point' is undefined.
//   IntersectFlag::NonIntersect: the line segment doesn't intersect the plane
///////////////////////////////////////////////////////////////////////
template <typename T>
int Intersect(const TPlane3<T>& plane, const TLineSegment3<T>& lineSeg, TPoint3<T>& point, T tol = NumTraits<T>::dummy_precision())
{
    TVector3<T> n = plane.GetPlaneNormal();
    T v = plane.GetPlaneValue();

    TPoint3<T> o = lineSeg.GetP1();
    TPoint3<T> e = lineSeg.GetP2();

    TVector3<T> d = (e-o).normalized();
    T len = e.Distance(o);

    T nom = n.dot(o) + v;
    T denom = n.dot(d);

    if( fabs(denom) <= tol )
        return ( fabs(nom) <= tol ) ? IntersectFlag::Coincident : IntersectFlag::Parallel;

    T w = - nom/denom;
    if( w < 0 )
    {
        point = o;
        return IntersectFlag::NonIntersect;
    }
    else if( w > len )
    {
        point = e;
        return IntersectFlag::NonIntersect;
    }

    point = o + w * d;
    return IntersectFlag::Intersect;
}

template <typename T>
int Intersect(const TLineSegment3<T>& lineSeg, const TPlane3<T>& plane, TPoint3<T>& point, T tol = NumTraits<T>::dummy_precision())
{
    return Intersect(plane, lineSeg, point, tol);
}

// Ray-Triangle intersection
//  org, dir: ray origin and direction
//  vert0, vert1, vert2: triangle vertices
//  point : point of intersection
//  dist: signed distance from 'org' along 'dir' to the point of intersection
// Return:
//   IntersectFlag::Intersect: intersects at 'point'
//   IntersectFlag::Coincident: the ray is on the triangle plane, and 'point' is undefined.
//   IntersectFlag::NonIntersect: the ray doesn't intersect the triangle plan
// Ref: http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/raytri/
template <typename T>
int Intersect(const TPoint3<T>& org, const TPoint3<T>& dir, 
              const TPoint3<T>& vert0, const TPoint3<T>& vert1, const TPoint3<T>& vert2,
              T& dist, T tol = NumTraits<T>::dummy_precision() )
{
    // find vectors for two edges sharing vert0
    TPoint3<T> edge1 = vert1 - vert0;
    TPoint3<T> edge2 = vert2 - vert0;

    TPoint3<T> pvec = dir.cross(edge2);

    T det = edge1.dot(pvec);

    // if near zero, ray lies in plane of triangle
    if (det > -tol && det < tol)
        return IntersectFlag::Coincident;

    T inv_det = ((T)1) / det;

    // calculate distance from vert0 to ray origin 
    TPoint3<T> tvec = orig - vert0;

    // calculate U parameter and test bounds
    T u = tvec.dot(pvec) * inv_det;
    if (u < ((T)0) || u > ((T)1))
        return IntersectFlag::NonIntersect;

    // prepare to test V parameter 
    TPoint3<T> qvec = tvec.cross(edge1);

    // calculate V parameter and test bounds
    T v = DOT(dir, qvec) * inv_det;
    if (v < ((T)0) || u + v > ((T)1))
        return IntersectFlag::NonIntersect;

   // calculate dist, ray intersects triangle
   dist = edge2.dot(qvec) * inv_det;

   return IntersectFlag::Intersect;
}

template <typename T>
int Intersect(const TPoint3<T>& org, const TPoint3<T>& dir, 
              const TPoint3<T>& vert0, const TPoint3<T>& vert1, const TPoint3<T>& vert2,
              TPoint3<T>& point, T tol = NumTraits<T>::dummy_precision() )
{
    T dist;
    int ret = Intersect(org, dir, vert0, vert1, vert2, dist, tol);

    if( ret==IntersectFlag::Intersect )
        point = org + dir * dist;

    return ret;
}

// Triangle-Triangle intersection (Tong:Not used and tested yet)
//  u0, u1, u2: one triangle vertices
//  v0, v1, v2: the other triangle vertices
// Return:
//   IntersectFlag::Intersect: intersects
//   IntersectFlag::NonIntersect: not intersects
// Ref: http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/opttritri.txt

/* this edge to edge test is based on Franlin Antonio's gem:
   "Faster Line Segment Intersection", in Graphics Gems III,
   pp. 199-202 */
//#define EDGE_EDGE_TEST(V0,U0,U1)                      \
//  Bx=U0[i0]-U1[i0];                                   \
//  By=U0[i1]-U1[i1];                                   \
//  Cx=V0[i0]-U0[i0];                                   \
//  Cy=V0[i1]-U0[i1];                                   \
//  f=Ay*Bx-Ax*By;                                      \
//  d=By*Cx-Bx*Cy;                                      \
//  if((f>0 && d>=0 && d<=f) || (f<0 && d<=0 && d>=f))  \
//  {                                                   \
//    e=Ax*Cy-Ay*Cx;                                    \
//    if(f>0)                                           \
//    {                                                 \
//      if(e>=0 && e<=f) return IntersectFlag::Intersect;\
//    }                                                 \
//    else                                              \
//    {                                                 \
//      if(e<=0 && e>=f) return IntersectFlag::Intersect;\
//    }                                                 \
//  }
//
//#define EDGE_AGAINST_TRI_EDGES(V0,V1,U0,U1,U2) \
//{                                              \
//  T Ax,Ay,Bx,By,Cx,Cy,e,d,f;               \
//  Ax=V1[i0]-V0[i0];                            \
//  Ay=V1[i1]-V0[i1];                            \
//  /* test edge U0,U1 against V0,V1 */          \
//  EDGE_EDGE_TEST(V0,U0,U1);                    \
//  /* test edge U1,U2 against V0,V1 */          \
//  EDGE_EDGE_TEST(V0,U1,U2);                    \
//  /* test edge U2,U1 against V0,V1 */          \
//  EDGE_EDGE_TEST(V0,U2,U0);                    \
//}
//
//#define POINT_IN_TRI(V0,U0,U1,U2)           \
//{                                           \
//  T a,b,c,d0,d1,d2;                     \
//  /* is T1 completly inside T2? */          \
//  /* check if V0 is inside tri(U0,U1,U2) */ \
//  a=U1[i1]-U0[i1];                          \
//  b=-(U1[i0]-U0[i0]);                       \
//  c=-a*U0[i0]-b*U0[i1];                     \
//  d0=a*V0[i0]+b*V0[i1]+c;                   \
//                                            \
//  a=U2[i1]-U1[i1];                          \
//  b=-(U2[i0]-U1[i0]);                       \
//  c=-a*U1[i0]-b*U1[i1];                     \
//  d1=a*V0[i0]+b*V0[i1]+c;                   \
//                                            \
//  a=U0[i1]-U2[i1];                          \
//  b=-(U0[i0]-U2[i0]);                       \
//  c=-a*U2[i0]-b*U2[i1];                     \
//  d2=a*V0[i0]+b*V0[i1]+c;                   \
//  if(d0*d1>0)                             \
//  {                                         \
//    if(d0*d2>0) return IntersectFlag::Intersect;                 \
//  }                                         \
//}
//
//template <typename T>
//int CoplanarIntersect(const TPoint3<T>& N,
//              const TPoint3<T>& U0, const TPoint3<T>& U1, const TPoint3<T>& U2,
//              const TPoint3<T>& V0, const TPoint3<T>& V1, const TPoint3<T>& V2,
//              T tol = NumTraits<T>::dummy_precision() )
//{
//   T A[3];
//   int i0,i1;
//   /* first project onto an axis-aligned plane, that maximizes the area */
//   /* of the triangles, compute indices: i0,i1. */
//   A[0]=fabs(N[0]);
//   A[1]=fabs(N[1]);
//   A[2]=fabs(N[2]);
//   if(A[0]>A[1])
//   {
//      if(A[0]>A[2])
//      {
//          i0=1;      /* A[0] is greatest */
//          i1=2;
//      }
//      else
//      {
//          i0=0;      /* A[2] is greatest */
//          i1=1;
//      }
//   }
//   else   /* A[0]<=A[1] */
//   {
//      if(A[2]>A[1])
//      {
//          i0=0;      /* A[2] is greatest */
//          i1=1;
//      }
//      else
//      {
//          i0=0;      /* A[1] is greatest */
//          i1=2;
//      }
//    }
//
//    /* test all edges of triangle 1 against the edges of triangle 2 */
//    EDGE_AGAINST_TRI_EDGES(V0,V1,U0,U1,U2);
//    EDGE_AGAINST_TRI_EDGES(V1,V2,U0,U1,U2);
//    EDGE_AGAINST_TRI_EDGES(V2,V0,U0,U1,U2);
//
//    /* finally, test if tri1 is totally contained in tri2 or vice versa */
//    POINT_IN_TRI(V0,U0,U1,U2);
//    POINT_IN_TRI(U0,V0,V1,V2);
//
//    return IntersectFlag::NonIntersect;
//}
//
//#define NEWCOMPUTE_INTERVALS(VV0,VV1,VV2,D0,D1,D2,D0D1,D0D2,A,B,C,X0,X1) \
//{ \
//        if(D0D1>0)  { \
//            /* here we know that D0D2<=0.0 */ \
//            /* that is D0, D1 are on the same side, D2 on the other or on the plane */ \
//            A=VV2; B=(VV0-VV2)*D2; C=(VV1-VV2)*D2; X0=D2-D0; X1=D2-D1; \
//        } \
//        else if(D0D2>0.0f) { \
//            /* here we know that d0d1<=0.0 */ \
//            A=VV1; B=(VV0-VV1)*D1; C=(VV2-VV1)*D1; X0=D1-D0; X1=D1-D2; \
//        } \
//        else if(D1*D2>0.0f || D0!=0.0f) { \
//            /* here we know that d0d1<=0.0 or that D0!=0.0 */ \
//            A=VV0; B=(VV1-VV0)*D0; C=(VV2-VV0)*D0; X0=D0-D1; X1=D0-D2; \
//        } \
//        else if(D1!=0.0f) { \
//            A=VV1; B=(VV0-VV1)*D1; C=(VV2-VV1)*D1; X0=D1-D0; X1=D1-D2; \
//        } \
//        else if(D2!=0.0f) { \
//            A=VV2; B=(VV0-VV2)*D2; C=(VV1-VV2)*D2; X0=D2-D0; X1=D2-D1; \
//        } \
//        else { \
//            /* triangles are coplanar */ \
//            return CoplanarIntersect(N1,V0,V1,V2,U0,U1,U2,tol); \
//        } \
//}
//
//template <typename T>
//int Intersect(const TPoint3<T>& U0, const TPoint3<T>& U1, const TPoint3<T>& U2,
//              const TPoint3<T>& V0, const TPoint3<T>& V1, const TPoint3<T>& V2,
//              T tol = NumTraits<T>::dummy_precision() )
//{
//    // compute plane equation of triangle(V0,V1,V2)
//    TPoint3<T> N1 = (V1 - V0).cross(V2 - V0);
//    T d1=-N1.dot(V0);// plane equation 1: N1.X+d1=0 
//
//    // put U0,U1,U2 into plane equation 1 to compute signed distances to the plane
//    T du0=N1.dot(U0)+d1;
//    T du1=N1.dot(U1)+d1;
//    T du2=N1.dot(U2)+d1;
//
//    // coplanarity robustness check 
//    if(fabs(du0)<tol) du0=(T)0;
//    if(fabs(du1)<tol) du1=(T)0;
//    if(fabs(du2)<tol) du2=(T)0;
//
//    T du0du1=du0*du1;
//    T du0du2=du0*du2;
//
//    if(du0du1>0 && du0du2>0) // same sign on all of them + not equal 0 ?
//        return IntersectFlag::NonIntersect;
//
//    // compute plane of triangle (U0,U1,U2)
//    TPoint3<T> N2 = (U1 - U0).cross(U2 - U0);
//    T d2=-N2.dot(U0);// plane equation 2: N2.X+d2=0
//
//    // put V0,V1,V2 into plane equation 2 
//    T dv0=N2.dot(V0)+d2;
//    T dv1=N2.dot(V1)+d2;
//    T dv2=N2.dot(V2)+d2;
//
//    if(fabs(dv0)<tol) dv0=(T)0;
//    if(fabs(dv1)<tol) dv1=(T)0;
//    if(fabs(dv2)<tol) dv2=(T)0;
//                 
//    T dv0dv1=dv0*dv1;
//    T dv0dv2=dv0*dv2;
//
//    if(dv0dv1>0.0f && dv0dv2>0.0f) // same sign on all of them + not equal 0 ?
//        return IntersectFlag::NonIntersect;
//
//    // compute direction of intersection line
//    TPoint3<T> D = N1.cross(N2);
//  
//    // compute and index to the largest component of D 
//    T maxv=fabs(D[0]);
//    int index=0;
//    T bb=fabs(D[1]);
//    T cc=fabs(D[2]);
//    if(bb>maxv) maxv=bb,index=1;
//    if(cc>maxv) maxv=cc,index=2;
//
//    // this is the simplified projection onto L
//    T vp0=V0[index];
//    T vp1=V1[index];
//    T vp2=V2[index];
//
//    T up0=U0[index];
//    T up1=U1[index];
//    T up2=U2[index];
//
//    // compute interval for triangle 1
//    T a,b,c,x0,x1;
//    NEWCOMPUTE_INTERVALS(vp0,vp1,vp2,dv0,dv1,dv2,dv0dv1,dv0dv2,a,b,c,x0,x1);
//
//    // compute interval for triangle 2
//    T d,e,f,y0,y1;
//    NEWCOMPUTE_INTERVALS(up0,up1,up2,du0,du1,du2,du0du1,du0du2,d,e,f,y0,y1);
//
//    T xx,yy,xxyy,tmp;
//    xx=x0*x1;
//    yy=y0*y1;
//    xxyy=xx*yy;
//
//    tmp=a*xxyy;
//    T isect1[]={tmp+b*x1*yy, tmp+c*x0*yy};
//    if( isect1[0] > isect1[1] )
//    {
//        tmp = isect1[0];
//        isect1[0] = isect1[1];
//        isect1[1] = tmp;
//    }
//    
//    tmp=d*xxyy;
//    T isect2[]={tmp+e*xx*y1, tmp+f*xx*y0};
//    if( isect2[0] > isect2[1] )
//    {
//        tmp = isect2[0];
//        isect2[0] = isect2[1];
//        isect2[1] = tmp;
//    }
//
//    if(isect1[1]<isect2[0] || isect2[1]<isect1[0])
//        return IntersectFlag::NonIntersect;
//    return IntersectFlag::Intersect;
//}

// Triangle-AxisAlignedBox intersection test
//  triverts0/1/2: triangle vertices
//  boxcenter: box center
//  boxhalfsize : half box size along x,y,z
// Return:
//   IntersectFlag::Intersect: intersects
//   IntersectFlag::NonIntersect: not intersects
// Ref: http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/tribox3.txt

#define FINDMINMAX(x0,x1,x2,min,max) \
  min = max = x0;   \
  if(x1<min) min=x1;\
  if(x1>max) max=x1;\
  if(x2<min) min=x2;\
  if(x2>max) max=x2;

/*======================== X-tests ========================*/
#define AXISTEST_X01(a, b, fa, fb)			   \
	p0 = a*v0.y() - b*v0.z();			       	   \
	p2 = a*v2.y() - b*v2.z();			       	   \
    if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
	rad = fa * boxhalfsize.y() + fb * boxhalfsize.z();   \
	if(min>rad || max<-rad) return IntersectFlag::NonIntersect;

#define AXISTEST_X2(a, b, fa, fb)			   \
	p0 = a*v0.y() - b*v0.z();			           \
	p1 = a*v1.y() - b*v1.z();			       	   \
    if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxhalfsize.y() + fb * boxhalfsize.z();   \
	if(min>rad || max<-rad) return IntersectFlag::NonIntersect;

/*======================== Y-tests ========================*/
#define AXISTEST_Y02(a, b, fa, fb)			   \
	p0 = -a*v0.x() + b*v0.z();		      	   \
	p2 = -a*v2.x() + b*v2.z();	       	       	   \
    if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
	rad = fa * boxhalfsize.x() + fb * boxhalfsize.z();   \
	if(min>rad || max<-rad) return IntersectFlag::NonIntersect;

#define AXISTEST_Y1(a, b, fa, fb)			   \
	p0 = -a*v0.x() + b*v0.z();		      	   \
	p1 = -a*v1.x() + b*v1.z();	     	       	   \
    if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxhalfsize.x() + fb * boxhalfsize.z();   \
	if(min>rad || max<-rad) return IntersectFlag::NonIntersect;

/*======================== Z-tests ========================*/
#define AXISTEST_Z12(a, b, fa, fb)			   \
	p1 = a*v1.x() - b*v1.y();			           \
	p2 = a*v2.x() - b*v2.y();			       	   \
    if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;} \
	rad = fa * boxhalfsize.x() + fb * boxhalfsize.y();   \
	if(min>rad || max<-rad) return IntersectFlag::NonIntersect;

#define AXISTEST_Z0(a, b, fa, fb)			   \
	p0 = a*v0.x() - b*v0.y();				   \
	p1 = a*v1.x() - b*v1.y();			           \
    if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxhalfsize.x() + fb * boxhalfsize.y();   \
	if(min>rad || max<-rad) return IntersectFlag::NonIntersect;


template <typename T>
int Intersect(const TPoint3<T>& triverts0, const TPoint3<T>& triverts1, const TPoint3<T>& triverts2,
              const TPoint3<T>& boxcenter, const TPoint3<T>& boxhalfsize, 
              T tol = NumTraits<T>::dummy_precision())
{
    /*    use separating axis theorem to test overlap between triangle and box */
    /*    need to test for overlap in these directions: */
    /*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
    /*       we do not even need to test these) */
    /*    2) normal of the triangle */
    /*    3) crossproduct(edge from tri, {x,y,z}-directin) */
    /*       this gives 3x3=9 more tests */

    T min,max,p0,p1,p2,rad;		// -NJMP- "d" local variable removed

    // move everything so that the boxcenter is in (0,0,0)
    TPoint3<T> v0 = triverts0 - boxcenter;
    TPoint3<T> v1 = triverts1 - boxcenter;
    TPoint3<T> v2 = triverts2 - boxcenter;

    // compute triangle edges
    TPoint3<T> e0 = v1 - v0;
    TPoint3<T> e1 = v2 - v1;
    TPoint3<T> e2 = v0 - v2;

    // Bullet 3:
    //  test the 9 tests first (this was faster)
    T fex = fabs(e0.x());
    T fey = fabs(e0.y());
    T fez = fabs(e0.z());

    AXISTEST_X01(e0.z(), e0.y(), fez, fey);
    AXISTEST_Y02(e0.z(), e0.x(), fez, fex);
    AXISTEST_Z12(e0.y(), e0.x(), fey, fex);

    fex = fabs(e1.x());
    fey = fabs(e1.y());
    fez = fabs(e1.z());

    AXISTEST_X01(e1.z(), e1.y(), fez, fey);
    AXISTEST_Y02(e1.z(), e1.x(), fez, fex);
    AXISTEST_Z0 (e1.y(), e1.x(), fey, fex);

    fex = fabs(e2.x());
    fey = fabs(e2.y());
    fez = fabs(e2.z());

    AXISTEST_X2 (e2.z(), e2.y(), fez, fey);
    AXISTEST_Y1 (e2.z(), e2.x(), fez, fex);
    AXISTEST_Z12(e2.y(), e2.x(), fey, fex);

    /* Bullet 1: */
    /*  first test overlap in the {x,y,z}-directions */
    /*  find min, max of the triangle each direction, and test for overlap in */
    /*  that direction -- this is equivalent to testing a minimal AABB around */
    /*  the triangle against the AABB */

    /* test in X-direction */
    FINDMINMAX(v0.x(),v1.x(),v2.x(),min,max);

    if(min>boxhalfsize.x() || max<-boxhalfsize.x()) return IntersectFlag::NonIntersect;

    /* test in Y-direction */
    FINDMINMAX(v0.y(),v1.y(),v2.y(),min,max);
    if(min>boxhalfsize.y() || max<-boxhalfsize.y()) return IntersectFlag::NonIntersect;

    /* test in Z-direction */
    FINDMINMAX(v0.z(),v1.z(),v2.z(),min,max);
    if(min>boxhalfsize.z() || max<-boxhalfsize.z()) return IntersectFlag::NonIntersect;

    /* Bullet 2: */
    /*  test if the box intersects the plane of the triangle */
    /*  compute plane equation of triangle: normal*x+d=0 */
    TPoint3<T> normal = e0.cross(e1);

    TPoint3<T> vmin,vmax;
    for(int q=0;q<=2;q++)
    {
        T v=v0[q];

        if(normal[q]>0)
        {
            vmin[q]=-boxhalfsize[q] - v;
            vmax[q]= boxhalfsize[q] - v;
        }
        else
        {
            vmin[q]= boxhalfsize[q] - v;
            vmax[q]=-boxhalfsize[q] - v;
        }
    }

    if(normal.dot(vmin)>0) return IntersectFlag::NonIntersect;
    if(normal.dot(vmax)>=0) return IntersectFlag::Intersect;
    return IntersectFlag::NonIntersect;
}


#endif //TINTERSECT_H
