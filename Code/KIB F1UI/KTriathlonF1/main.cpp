#include <QApplication>
#include "MainWindow.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	CMainWindow		MainWindow( &app );

	if ( argc == 2 )// run from command line to create  the new implant
	{
		MainWindow.hide();

		char* cCaseNumber = argv[1];
		QString caseNumber = QString(cCaseNumber);

		MainWindow.CreateImplantSilent(caseNumber);

		return 0;
	}
	else
	{
		MainWindow.showMaximized();// run GUI
		return app.exec();
	}

}
