/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../MainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CMainWindow_t {
    QByteArrayData data[15];
    char stringdata0[195];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CMainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CMainWindow_t qt_meta_stringdata_CMainWindow = {
    {
QT_MOC_LITERAL(0, 0, 11), // "CMainWindow"
QT_MOC_LITERAL(1, 12, 9), // "OnFileNew"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 10), // "OnFileOpen"
QT_MOC_LITERAL(4, 34, 8), // "QString&"
QT_MOC_LITERAL(5, 43, 4), // "elem"
QT_MOC_LITERAL(6, 48, 11), // "OnFileClose"
QT_MOC_LITERAL(7, 60, 10), // "OnFileSave"
QT_MOC_LITERAL(8, 71, 8), // "bWarning"
QT_MOC_LITERAL(9, 80, 20), // "OnFileOpenAsReadOnly"
QT_MOC_LITERAL(10, 101, 15), // "OnFileSaveACopy"
QT_MOC_LITERAL(11, 117, 21), // "OnEnableMakingActions"
QT_MOC_LITERAL(12, 139, 16), // "OnFileSaveAutoOn"
QT_MOC_LITERAL(13, 156, 17), // "OnImportCADEntity"
QT_MOC_LITERAL(14, 174, 20) // "OnCleanUpTempDisplay"

    },
    "CMainWindow\0OnFileNew\0\0OnFileOpen\0"
    "QString&\0elem\0OnFileClose\0OnFileSave\0"
    "bWarning\0OnFileOpenAsReadOnly\0"
    "OnFileSaveACopy\0OnEnableMakingActions\0"
    "OnFileSaveAutoOn\0OnImportCADEntity\0"
    "OnCleanUpTempDisplay"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CMainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x0a /* Public */,
       3,    1,   75,    2, 0x0a /* Public */,
       3,    0,   78,    2, 0x2a /* Public | MethodCloned */,
       6,    0,   79,    2, 0x0a /* Public */,
       7,    1,   80,    2, 0x0a /* Public */,
       7,    0,   83,    2, 0x2a /* Public | MethodCloned */,
       9,    0,   84,    2, 0x0a /* Public */,
      10,    0,   85,    2, 0x0a /* Public */,
      11,    0,   86,    2, 0x0a /* Public */,
      12,    1,   87,    2, 0x0a /* Public */,
      13,    0,   90,    2, 0x0a /* Public */,
      14,    0,   91,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CMainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnFileNew(); break;
        case 1: _t->OnFileOpen((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->OnFileOpen(); break;
        case 3: _t->OnFileClose(); break;
        case 4: _t->OnFileSave((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->OnFileSave(); break;
        case 6: _t->OnFileOpenAsReadOnly(); break;
        case 7: _t->OnFileSaveACopy(); break;
        case 8: _t->OnEnableMakingActions(); break;
        case 9: _t->OnFileSaveAutoOn((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->OnImportCADEntity(); break;
        case 11: _t->OnCleanUpTempDisplay(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CMainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<Module::staticMetaObject>(),
    qt_meta_stringdata_CMainWindow.data,
    qt_meta_data_CMainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CMainWindow.stringdata0))
        return static_cast<void*>(this);
    return Module::qt_metacast(_clname);
}

int CMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Module::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
