#include "EntityView.h"
#include "..\KUtility\KUtility.h"
#include <assert.h>

EntityView::EntityView(QObject *parent)
    : QObject(parent)
{
    show = true;                // default: show
    showIntersection = false;   // default: no intersection
    opaque = true;              // default: opaque and no translucency
}

EntityView::~EntityView()
{
}

// Set the show property
void EntityView::SetShow(bool b)
{
    if( b==show )
        return;

    show = b;
    emit showChanged(b);
}

// Set the intersection show property
void EntityView::SetShowIntersection(bool b)
{
    if( showIntersection == b )
        return;

    showIntersection = b;
    emit showIntersectionChanged(b);
}

void EntityView::SetShowOpaque(bool b) 
{
    if( opaque == b )
        return;

    opaque = b;
}
