#ifndef TPOINT3_H
#define TPOINT3_H

#include "MathOp.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
using namespace Eigen;

#pragma warning( disable : 4996 )
#include "IwVector3d.h"
#pragma warning( default : 4996)

template <typename T>
class TPoint3 : public Matrix<T,3,1>
{
public:
    TPoint3() : Matrix(0,0,0) {}
    TPoint3(T a, T b, T c) : Matrix(a,b,c) {}
    TPoint3(IwPoint3d& pt) : Matrix((T)pt.x, (T)pt.y, (T)pt.z) {}

    // construct TPoint3 from Matrix expressions
    template<typename Derived>
    TPoint3(const DenseBase<Derived>& other)
        : Matrix(other) { }

    // Assign Matrix expressions to TPoint3
    template<typename Derived>
    TPoint3& operator = (const MatrixBase<Derived>& other)
    {
        Matrix::operator=(other);
        return *this;
    }

    inline operator IwPoint3d() const { return IwPoint3d(x(),y(),z());}

    // Get values
    inline T GetX() const { return x(); }
    inline T GetY() const { return y(); }
    inline T GetZ() const { return z(); }
    void GetXYZ(T& xv, T& yv, T& zv) const { xv=x(); yv=y(); zv=z(); }

    // Set values
    inline void SetX(T xv) { x() = xv; }
    inline void SetY(T yv) { y() = yv; }
    inline void SetZ(T zv) { z() = zv; }
    inline void SetXYZ(T xv, T yv, T zv) { x()=xv; y()=yv; z()=zv; }

    // Distance to a point
    T Distance(T xv, T yv, T zv) const
    {
        xv -= x();
        yv -= y();
        zv -= z();
        return std::sqrt(xv*xv + yv*yv + zv*zv);
    }

    template<typename Derived>
    T Distance(const MatrixBase<Derived>& pt) const
    {
        T xv = x() - pt.x();
        T yv = y() - pt.y();
        T zv = z() - pt.z();
        return std::sqrt(xv*xv + yv*yv + zv*zv);
    }

    // Cross product and normalize to unit vector
    template<typename Derived>
    TPoint3 UnitCross (const MatrixBase<Derived>& pt) const
    {
        return cross(pt).normalized();
    }

    // Rotation
    void RotateX(T degree)
    {
        T rad = degree * PI / 180;
        T cosa = std::cos(rad);
        T sina = std::sin(rad);

        T yp = cosa * y() - sina * z();
        T zp = sina * y() + cosa * z();

        y() = yp;
        z() = zp;
    }

    void RotateY(T degree)
    {
        T rad = degree * PI / 180;
        T cosa = std::cos(rad);
        T sina = std::sin(rad);

        T xp =  cosa * x() + sina * z();
        T zp = -sina * x() + cosa * z();

        x() = xp;
        z() = zp;
    }

    void RotateZ(T degree)
    {
        T rad = degree * PI / 180;
        T cosa = std::cos(rad);
        T sina = std::sin(rad);

        T xp =  cosa * x() - sina * y();
        T yp =  sina * x() + cosa * y();

        x() = xp;
        y() = yp;
    }

    // comparison
    template<typename Derived>
    bool operator> (const MatrixBase<Derived>& pt) { return x()>pt.x() && y()>pt.y() && z()>pt.z(); }

    template<typename Derived>
    bool operator< (const MatrixBase<Derived>& pt) { return x()<pt.x() && y()<pt.y() && z()<pt.z(); }

    template<typename Derived>
    bool operator>= (const MatrixBase<Derived>& pt) { return x()>=pt.x() && y()>=pt.y() && z()>=pt.z(); }

    template<typename Derived>
    bool operator<= (const MatrixBase<Derived>& pt) { return x()<=pt.x() && y()=<pt.y() && z()<=pt.z(); }

    // conversion
    TPoint3<int>    Round()    { return TPoint3<int>(round(x()), round(y()), round(z())); }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TPoint3<int>    Point3Int;
typedef TPoint3<float>  Point3Float;
typedef TPoint3<double> Point3Double;

#endif //TPOINT3_H
