#pragma once

#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Entity.h"

struct CDefineOutlineProfileUndoData
{
	IwTArray<IwPoint3d>			featurePoints;			
	IwTArray<int>				featurePointsInfo;		
	IwTArray<IwPoint3d>			featurePointsRemap;			
};

class CTotalDoc;
class OutlineProfileMgrModel;
///////////////////////////////////////////////////////////////
// This class is used by DefineOutlineProfile to keep track
// of the current active UI manager either 2D or 3D
// It implements common functionality between 2D and 3D UI handlers
// See DefineOutlineProfile.h for more information
///////////////////////////////////////////////////////////////
class OutlineProfileMgrUIBase : public QObject
{
	Q_OBJECT

public:
				 OutlineProfileMgrUIBase(CTotalDoc* doc, CManagerActivateType manActType, CManager *owner, OutlineProfileMgrModel *model);
				 ~OutlineProfileMgrUIBase();

	virtual void Display(Viewport* vp=NULL);
	void DisplayNotchEighteenWidthPoints(IwVector3d viewVec=IwVector3d(0,0,0));
	void DisplayNonQualifiedLocations(IwVector3d viewVec=IwVector3d(0,0,0));

	//////////////////////////////////////////////////////////
	virtual void CalculateSketchSurfaceNormalInfo(IwBSplineSurface*& skchSurface) {};
	virtual void DetermineViewingNormalRegion() {};
	void RefineAnteriorOuterPoints(IwTArray<IwPoint3d>& cutFeatPnts, IwTArray<IwPoint3d>& antOuterPnts);
	//////////////////////////////////////////////////////////

    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
	virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp=NULL );
	virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL ){ return false;};
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* vp=NULL )=0;

	virtual void	SetMouseMoveSearchingList() = 0;

	void DetermineOutlineProfile();

	void GetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData);
	virtual void SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData);

	void OnAccept();
	void OnCancel();	

signals:
	void	SwitchToCoordSystem(QString name);

public slots:
	virtual void	OnDispRuler();
	virtual void	OnValidate();

	void	OnReset();
	void	OnRefine();
	void	OnAdd();
	void	OnDel();

protected: //methods
	virtual void	UpdateSketchSurface();
	virtual void	Reset();
	virtual void	DetermineNotchEighteenWidthPoints();
	void	Refine();

	void	MovePoint(int caughtIndex, IwPoint2d surfParam);
	bool	AddPoint(IwPoint2d surfParam);
	void	DeletePoint(int caughtIndex);

protected:	//data
	CTotalDoc*					m_pDoc;
	CManagerActivateType		m_manActType;

	bool						m_oldOutlineProfileExist;
	bool						m_leftButtonDown;
	bool						m_displayRuler;
	int							m_addDeleteMode;			// 0: none, 1: add, 2: delete

	OutlineProfileMgrModel*		m_mgrModel;
	CManager*					m_owner;

	//// For Validation ///////////////////////////////////////////////////////////////
	CEntValidateStatus			m_validateStatus;
	QString						m_validateMessage;
};
