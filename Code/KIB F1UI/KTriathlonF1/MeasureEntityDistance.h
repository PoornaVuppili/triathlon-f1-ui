#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"
#include "SelectedEntity.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class QLineEdit;
class QCheckBox;

class TEST_EXPORT_TW CMeasureEntityDistance : public CManager
{
    Q_OBJECT

public:
					CMeasureEntityDistance( CTotalView* pView );
	virtual			~CMeasureEntityDistance();
    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseRight ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* viewport=NULL );
	bool			Reject() {OnAccept();return true;};

	virtual void	Display(Viewport* vp=NULL);

	bool			GetPoints(double &x1, double &y1, double &z1, double &x2, double &y2, double &z2);

	static bool		HitPoints(IwTArray<IwPoint3d>& pointList, IwPoint3d& origin, IwVector3d& dir, double tol, IwPoint3d& hitPoint, double& height, IwPoint3d* exclusivePoint=NULL);
	static bool		HitVertexes(IwTArray<IwBrep*>& brepList, IwPoint3d& origin, IwVector3d& dir, double tol, IwPoint3d& hitVertex, double& height, IwPoint3d* exclusivePoint=NULL);
	static bool		HitCurves(IwTArray<IwCurve*>& curveList, IwPoint3d& origin, IwVector3d& dir, double tol, IwCurve*& hitCurve, IwPoint3d& hitPoint, double& hitParam, double& height, IwCurve* exclusiveCurve=NULL);
	static bool		HitEdges(IwTArray<IwBrep*>& brepList, IwPoint3d& origin, IwVector3d& dir, double tol, IwEdge*& hitEdge, IwPoint3d& hitPoint, double& hitParam, double& height, IwEdge* exclusiveEdge=NULL);
	static bool		HitFaces(IwTArray<IwBrep*>& brepList, IwPoint3d& origin, IwVector3d& dir, double tol, IwFace*& hitFace, IwPoint3d& hitPoint, IwPoint2d& hitParam, double& height, IwFace* exclusiveFace=NULL);

private:
	void			SelectEntities(const QPoint& cursor, CSelectedEntity& hitEntity, CSelectedEntity* exclusiveEntity=NULL);

public slots:
	void			OnAccept();

private slots:

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;
	QCheckBox*					m_checkPointVertex;
	QCheckBox*					m_checkCurveEdge;
	QCheckBox*					m_checkSurfaceFace;
	QCheckBox*					m_checkLocalResult;

	QLineEdit*					m_textMinMax;
	QLineEdit*					m_textMeasuredDistance;

	bool						m_mouseLeftDown;
	IwTArray<IwBrep*>			m_selectableBreps;
	IwTArray<bool>				m_selectableBrepsAreOpaque;
	IwTArray<IwCurve*>			m_selectableCurves;
	IwTArray<IwPoint3d>			m_selectablePoints;

	CSelectedEntity				m_firstHitEntity;
	CSelectedEntity				m_secondHitEntity;

	// there 3 members are for for hitpoint only
	QPoint						m_prevCursorClicked;
	IwCurve*					m_prevCurveClicked;
	IwFace*						m_prevFaceClicked;

	long						m_glEdgeDisplayList;
	bool						m_bEdgeModified;
};
