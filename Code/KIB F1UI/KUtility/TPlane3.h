#ifndef TPLANE3_H
#define TPLANE3_H

#include "TPoint3.h"
#include "TLine3.h"

template <typename T>
class TPlane3
{
public:
    // Construct the plane from 3 points
    TPlane3(const TPoint3<T>& v1, const TPoint3<T>& v2, const TPoint3<T>& v3)
    {
        TPoint3<T> e1 = v2 - v1;
        TPoint3<T> e2 = v3 - v1;

        n = e1.UnitCross(e2);
        v = - n.dot(v1);
    }

    // Construct the plane from a point and the normal
    TPlane3(const TPoint3<T>& pt, const TVector3<T>& nor)
    {
        n = nor.normalized();
        v = - n.dot(pt);
    }

    // Get values
    const TVector3<T>& GetPlaneNormal() const { return n; }
    T GetPlaneValue() const { return v; }

    // Set values
    void SetPlaneNormal(const TVector3<T>& nor) { n=nor.normalized(); }
    void SetPlaneValue(T c) { v=c; }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    SignedDistance
    // Function purpose: Compute the signed distance from a point to the plane
    // Input: the point coordinate
    // Output: the signed distance
    ///////////////////////////////////////////////////////////////////////
    T SignedDistance(const TPoint3<T>& pt) const
    {
        return n.dot(pt) + v;
    }
    T SignedDistance(T x, T y, T z) const
    {
        return SignedDistance(TPoint3<T>(x,y,z));
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    SignedDistance
    // Function purpose: Compute the Euclidean distance from a point to the plane (>=0)
    // Input: the point coordinate
    // Output: the Euclidean distance
    ///////////////////////////////////////////////////////////////////////
    T EuclideanDistance(const TPoint3<T>& pt) const
    {
        return fabs( SignedDistance(pt) );
    }
    T EuclideanDistance(T x, T y, T z) const
    {
        return EuclideanDistance(TPoint3<T>(x,y,z));
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    WhichSide
    // Function purpose: Determine which side is the given point relative
    //    to the line
    // Input: the point coordinate
    // Output: 0 (on the plane) 1 (positive side) -1 (negative side)
    ///////////////////////////////////////////////////////////////////////
    int WhichSide(const TPoint3<T>& pt) const
    { 
        return Sign( SignedDistance(pt) );
    }
    int WhichSide(T x, T y, T z) const
    {
        return WhichSide(TPoint3<T>(x,y,z));
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    IsParallel
    // Function purpose: Determine if two planes are parallel by testing if 
    //   the two plane normals are in the same or opposite direction
    // Input: the plane object
    // Output: true (yes) false (no)
    ///////////////////////////////////////////////////////////////////////
    bool IsParallel(const TPlane3<T>& plane, T tol = NumTraits<T>::dummy_precision()) const
    { 
        return fabs(n.dot(plane.GetPlaneNormal()))>=1-tol;
    }

    bool IsPerpendicular(const TPlane3<T>& plane, T tol=NumTraits<T>::dummy_precision()) const
    { 
        return fabs(n.dot(plane.GetPlaneNormal()))<=tol;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    IsCoincident
    // Function purpose: Determine if two planes are coincident
    // Input: the plane object
    // Output: true (yes) false (no)
    ///////////////////////////////////////////////////////////////////////
    bool IsCoincident(const TPlane3<T>& plane, T tol = NumTraits<T>::dummy_precision()) const
    { 
        // test parallel
        T dotv = n.dot( plane.GetPlaneNormal() );
        if( fabs(dotv)>=1-tol )
        {
            // test coincident
            if( dotv>0 )
                // normal in the same direction
                return fabs(v-plane.GetPlaneValue())<tol;
            else
                // normal in the opposite direction
                return fabs(v+plane.GetPlaneValue())<tol;
        }
        return false;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    IsParallel
    // Function purpose: Determine if the line is parallel to the plane by
    //   testing if the plane normal and line direction are perpendicular
    // Input: the line object
    // Output: true (yes) false (no)
    ///////////////////////////////////////////////////////////////////////
    bool IsParallel(const TLine3<T>& line, T tol=NumTraits<T>::dummy_precision()) const
    { 
        return fabs(n.dot(line.GetDirection()))<=tol;
    }

    bool IsPerpendicular(const TLine3<T>& line, T tol=NumTraits<T>::dummy_precision()) const
    { 
        return fabs(n.dot(line.GetDirection()))>=1-tol;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    IsCoincident
    // Function purpose: Determine if the line is coincident with the plane
    // Input: the line object
    // Output: true (yes) false (no)
    ///////////////////////////////////////////////////////////////////////
    bool IsCoincident(const TLine3<T>& line, T tol=NumTraits<T>::dummy_precision()) const
    { 
        return fabs(n.dot(line.GetDirection())) <= tol &&
               fabs(n.dot(line.GetOrigin())+v) <= tol;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Intersect
    // Function purpose: Determine if the plane intersects another plane, if so
    //   find the line of intersection
    // Input:
    //   plane : the other plane
    // Output:
    //   line : the line of intersection if exists
    // Return:
    //   IntersectFlag::Intersect: there is intersection, and the 'line' 
    //      contains the line of intersection;
    //   IntersectFlag::Parallel: the planes are paralle, and the 
    //      'line' is undefined;
    //   IntersectFlag::Coincident: the planes are coincident, and the 
    //      'line' is undefined.
    ///////////////////////////////////////////////////////////////////////
    int Intersect(const TPlane3<T>& plane, TLine3<T>& line, T tol = NumTraits<T>::dummy_precision()) const
    {
        TVector3<T> nn = plane.GetPlaneNormal();
        T vv = plane.GetPlaneValue();

        T dotv = n.dot(nn);
        if( fabs(dotv)>=1-tol )
        {
            // test coincident
            if( dotv>0 )
                // normal in the same direction
                return fabs(v-vv)<tol ? IntersectFlag::Coincident : IntersectFlag::Parallel;
            else
                // normal in the opposite direction
                return fabs(v+vv)<tol ? IntersectFlag::Coincident : IntersectFlag::Parallel;
        }

        T invDet = 1.0/(1 - dotv*dotv);
        T c = (dotv*vv - v)*invDet;
        T cn =(dotv*v - vv)*invDet;
        line.SetOrigin( c*n + cn*nn );
        line.SetDirection(n.UnitCross(nn));

        return IntersectFlag::Intersect;
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Project
    // Function purpose: Find the point projection along the plane normal
    // Input: the point coordinate
    // Output: the projected point
    ///////////////////////////////////////////////////////////////////////
    TPoint3<T> Project(const TPoint3<T>& pt) const
    { 
        return pt - n * SignedDistance(pt);
    }

protected:
    // plane parameter: n.x * x + n.y * y + n.z * z + v = 0
    TVector3<T> n;  // plane normal
    T v;            // plane value
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TPlane3<int>    Plane3Int;
typedef TPlane3<float>  Plane3Float;
typedef TPlane3<double> Plane3Double;

#endif //TPLANE3_H

