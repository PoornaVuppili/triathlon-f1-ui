#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "OutlineProfile.h"
#include "DefineOutlineProfile.h"
//#include "JCurves.h"
#include "FemoralAxes.h"
#include "FemoralCuts.h"
//#include "InnerSurface.h"
#include "AdvancedControl.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"
#include "IFemurImplant.h"

COutlineProfile::COutlineProfile( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_TRANSPARENCY;

	m_color = gray;
	m_sketchSurface = NULL;
	m_uvCurve = NULL;
	m_outlineProfileXYZCurve = NULL;
	m_outlineProfileXYZCurveValidation = NULL;
	m_outlineProfileModified = false;
	m_bOPDataModified = false;
	m_outlineProfileRulerModified = false;
	m_displayOutlineProfileRuler = false;
	m_displayOutlineProfileBoth = false;
	m_displayBoxCut = true;
	m_glOutlineProfileCurveList = 0;
	m_glOutlineProfileProjectedCurveList = 0;
	m_glOutlineProfileRulerCurveList = 0;
	m_glOutlineProfileBoxCutList = 0;
	m_estimatedImplantMLSize = 0;
	m_b2DEditing = false;
	m_bEver2DEditing = false;
	m_posPostProfileAdjustDist = 0;
	m_negPostProfileAdjustDist = 0;
}

COutlineProfile::~COutlineProfile()
{

}

void COutlineProfile::Display()
{
	if(m_b2DEditing)
		return;
	
	// display outline profile here
	glDisable( GL_LIGHTING );

	if( m_outlineProfileModified && m_glOutlineProfileCurveList > 0 )
		DeleteDisplayList( m_glOutlineProfileCurveList );

	if( m_outlineProfileModified && m_glOutlineProfileProjectedCurveList > 0 )
		DeleteDisplayList( m_glOutlineProfileProjectedCurveList );

	if ( m_displayOutlineProfileBoth ) // This is the editing mode
	{
		if( m_glOutlineProfileCurveList == 0 )
		{
			m_glOutlineProfileCurveList = NewDisplayList();
			glNewList( m_glOutlineProfileCurveList, GL_COMPILE_AND_EXECUTE );
			DisplayOutlineProfile();
			glEndList();
		}
		else
		{
			glCallList( m_glOutlineProfileCurveList );
		}
		// Do not use display list for DisplayOutlineFeaturePoints(). 
		// The feature points need to be offset to be closer to the viewers.
		DisplayOutlineFeaturePoints();
	}
	else
	{
		if( 1 /*m_glOutlineProfileProjectedCurveList == 0*/ )
		{
			m_glOutlineProfileProjectedCurveList = NewDisplayList();
			glNewList( m_glOutlineProfileProjectedCurveList, GL_COMPILE_AND_EXECUTE );
			DisplayOutlineProfileProjectedCurve();
			glEndList();
		}
		else
		{
			glCallList( m_glOutlineProfileProjectedCurveList );
		}
	}

	// Only sketch surface at CPart and Ruler can be transparent.
	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}
	// Make the sketch surface more transparent
	double oldTransparentFactor = m_pDoc->GetTransparencyFactor();
	double thisTransparentFactor = 0.0;// make it totally transparent
	m_pDoc->SetTransparencyFactor(thisTransparentFactor);
	CPart::Display();
	m_pDoc->SetTransparencyFactor(oldTransparentFactor);
	// Display Ruler
	DisplayRulerCurvesGLlist();
	// Display box cut
	if ( m_pDoc->isPosteriorStabilized() )
		DisplayBoxCutGLlist();

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	glEnable( GL_LIGHTING );


	//PSV RENDER----
	if(m_arrOfArrOfIntPoints.GetSize() > 0)
	{
		IwBSplineSurface * pSketchSurf = NULL;
		GetSketchSurface(pSketchSurf);
		IwTArray<IwPoint3d> arrOfPoints = m_arrOfArrOfIntPoints[0];

		for(int idx = 0; idx < arrOfPoints.GetSize(); idx++)
		{
			IwPoint3d point = arrOfPoints[idx];

			IwPoint2d point2D(point.x, point.y);

			IwPoint3d point3D;
			pSketchSurf->EvaluatePoint(point2D, point3D);

			glPointSize(8);
			glColor3ub( 250, 0, 0 );

			glBegin( GL_POINTS );
			glVertex3d( point3D.x, point3D.y, point3D.z );
			glEnd();
		}
	}

	m_outlineProfileModified = false;

}

void COutlineProfile::DisplayRulerCurvesGLlist()
{
	if (m_displayOutlineProfileRuler)
	{
		glDisable( GL_LIGHTING );

		if( m_outlineProfileRulerModified && m_glOutlineProfileRulerCurveList > 0 )
			DeleteDisplayList( m_glOutlineProfileRulerCurveList );

		if( m_glOutlineProfileRulerCurveList == 0 )
		{
			m_glOutlineProfileRulerCurveList = NewDisplayList();
			glNewList( m_glOutlineProfileRulerCurveList, GL_COMPILE_AND_EXECUTE );
			DisplayRulerCurves();
			glEndList();
		}
		else
		{
			glCallList( m_glOutlineProfileRulerCurveList );
		}

		m_outlineProfileRulerModified = false;
	}
}

void COutlineProfile::DisplayBoxCutGLlist()
{
	if (m_displayBoxCut)
	{
		glDisable( GL_LIGHTING );

		if( m_glOutlineProfileBoxCutList == 0 )
		{
			m_glOutlineProfileBoxCutList = NewDisplayList();
			glNewList( m_glOutlineProfileBoxCutList, GL_COMPILE_AND_EXECUTE );
			DisplayBoxCut();
			glEndList();
		}
		else
		{
			glCallList( m_glOutlineProfileBoxCutList );
		}
	}
}

bool COutlineProfile::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool				bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bOPDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_op.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	unsigned nSize = m_outlineProfileFeaturePoints.GetSize();

	File.write( (char*) &nSize, sizeof(unsigned) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d featurePoint = m_outlineProfileFeaturePoints.GetAt(i);
		File.write( (char*) &featurePoint, sizeof( IwPoint3d ) );
	}
	for (unsigned i=0; i<nSize; i++)
	{
		int info = m_outlineProfileFeaturePointsInfo.GetAt(i);
		File.write( (char*) &info, sizeof( int ) );
	}
	////////////////////////////////////////////
	// Parameter for v4.0
	////////////////////////////////////////////
	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d featurePointRemap = m_outlineProfileFeaturePointsRemap.GetAt(i);
		File.write( (char*) &featurePointRemap, sizeof( IwPoint3d ) );
	}

	////////////////////////////////////////////
	// Parameter for v6.0.10
	File.write( (char*) &m_posPostProfileAdjustDist, sizeof( double ) );
	File.write( (char*) &m_negPostProfileAdjustDist, sizeof( double ) );

	// Parameter for v6.0.17
	File.write( (char*) &m_bEver2DEditing, sizeof( bool ) );

	File.close();

	m_bOPDataModified = false;

	//// For validation ////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_op_validation.dat~";

	QFile				FileValidation( sFileName );

	bOK = FileValidation.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	nSize = m_outlineProfileFeaturePointsValidation.GetSize();

	FileValidation.write( (char*) &nSize, sizeof(unsigned) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d featurePoint = m_outlineProfileFeaturePointsValidation.GetAt(i);
		FileValidation.write( (char*) &featurePoint, sizeof( IwPoint3d ) );
	}
	for (unsigned i=0; i<nSize; i++)
	{
		int info = m_outlineProfileFeaturePointsInfoValidation.GetAt(i);
		FileValidation.write( (char*) &info, sizeof( int ) );
	}
	FileValidation.close();


	return true;
}

bool COutlineProfile::Load( const QString& sDir )
{
	CPart::Load(sDir);


	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_op.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	unsigned nSize;
	IwTArray<IwPoint3d>		featurePoints;
	IwTArray<int>			infos;
	IwTArray<IwPoint3d>		featurePointsRemap;

	// Variables for TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{

		File.read( (char*) &nSize, sizeof( unsigned ) );

		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d featurePoint;
			File.read( (char*) &featurePoint, sizeof(IwPoint3d) );
			featurePoints.Add(featurePoint);
		}
		for (unsigned i=0; i<nSize; i++)
		{
			int info;
			File.read( (char*) &info, sizeof(int) );
			infos.Add(info);
		}
	}

	////////////////////////////////////////////
	// Parameter for v4.0.0
	////////////////////////////////////////////
	if ( m_pDoc->IsOpeningFileRightVersion(4,0,0) )
	{
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d featurePointRemap;
			File.read( (char*) &featurePointRemap, sizeof(IwPoint3d) );
			featurePointsRemap.Add(featurePointRemap);
		}
	}
	else
	{
		for (unsigned i=0; i<nSize; i++)
			featurePointsRemap.Add(IwPoint3d(0,0,0));
	}

	// Variables for TriathlonF1 version 6.0.10
	if (m_pDoc->IsOpeningFileRightVersion(6,0,10))
	{
		File.read( (char*) &m_posPostProfileAdjustDist, sizeof(double) );
		File.read( (char*) &m_negPostProfileAdjustDist, sizeof(double) );
	}

	// Variables for TriathlonF1 version 6.0.17
	if (m_pDoc->IsOpeningFileRightVersion(6,0,17))
	{
		File.read( (char*) &m_bEver2DEditing, sizeof(bool) );
	}

	File.close();


	// Actions required by TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{
		// Set the data
		SetOutlineProfileFeaturePoints(featurePoints, infos, featurePointsRemap);
		// determine the ruler surves
		DetermineRulerCurves();
		// determine the box cut curves
		if ( m_pDoc->isPosteriorStabilized() )
			DetermineBoxCutCurves();
		// Load sketch surface from Brep
		IwBSplineSurface* sketchSurf = NULL;
		GetSketchSurfaceFromBrep(sketchSurf);
	}

	m_bOPDataModified = false;

	///////////////////////////////////////////////////////////
	//// For validation ///////////////////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_op_validation.dat";

	QFile				FileValidation( sFileName );

	bOK = FileValidation.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	IwTArray<IwPoint3d>		featurePointsValidation;
	IwTArray<int>			infosValidation;

	FileValidation.read( (char*) &nSize, sizeof( unsigned ) );

	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d featurePoint;
		FileValidation.read( (char*) &featurePoint, sizeof(IwPoint3d) );
		featurePointsValidation.Add(featurePoint);
	}
	for (unsigned i=0; i<nSize; i++)
	{
		int info;
		FileValidation.read( (char*) &info, sizeof(int) );
		infosValidation.Add(info);
	}
	FileValidation.close();

	SetOutlineProfileFeaturePointsValidation(featurePointsValidation, infosValidation);



	return true;
}

bool COutlineProfile::IsMemberDataChanged(IwTArray<IwPoint3d>& featurePoints)
{
	bool changed = true;

	if (featurePoints.GetSize() != m_outlineProfileFeaturePoints.GetSize())
		return changed;

	for (unsigned i=0; i<featurePoints.GetSize(); i++)
	{
		if (featurePoints.GetAt(i) != m_outlineProfileFeaturePoints.GetAt(i))
			return changed;
	}

	return false;
}

void COutlineProfile::SetSketchSurface
(
	IwBSplineSurface* sketchSurface
)
{

	if (sketchSurface == NULL) return;

	// First, we need to update m_outlineProfileFeaturePoints & m_outlineProfileXYZCurve
	// Get the old m_outlineProfileFeaturePoints in XYZ space
	IwTArray<IwPoint3d> oldXYZPoints;
	IwTArray<int> oldOutlineProfileFeaturePointsInfo;
	oldOutlineProfileFeaturePointsInfo.Append(m_outlineProfileFeaturePointsInfo);
	if (m_sketchSurface != NULL && m_outlineProfileFeaturePoints.GetSize() > 0)
	{
		ConvertSketchSurfaceUVtoXYZ(m_outlineProfileFeaturePoints, oldXYZPoints);
		m_outlineProfileFeaturePoints.RemoveAll();
		m_outlineProfileFeaturePointsInfo.RemoveAll();
	}

	// delete the previous Brep
	IwBrep* prevBrep = GetIwBrep();
	if (prevBrep)
	{
		IwObjDelete delBrep(prevBrep);
	}

	m_sketchSurface = sketchSurface;

	// Second, map the old m_outlineProfileFeaturePoints in XYZ space back to UV surface space
	IwSolutionArray sols;
	IwSolution sol;
	if ( oldXYZPoints.GetSize() > 0 )
	{		
		for (unsigned i=0; i<oldXYZPoints.GetSize(); i++)
		{
			m_sketchSurface->GlobalPointSolve(m_sketchSurface->GetNaturalUVDomain(), IW_SO_MINIMIZE, 
								oldXYZPoints.GetAt(i), 0.001, NULL, IW_SR_ALL, sols);
			if (sols.GetSize() > 0)
			{
				sol = sols.GetAt(0); 
				IwPoint3d paramUVW = IwPoint3d(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1], 0);
				m_outlineProfileFeaturePoints.Add(paramUVW);
				m_outlineProfileFeaturePointsInfo.Add(oldOutlineProfileFeaturePointsInfo.GetAt(i));
			}
		}
		// update the feature points
		SetOutlineProfileFeaturePoints(m_outlineProfileFeaturePoints, m_outlineProfileFeaturePointsInfo, m_outlineProfileFeaturePointsRemap);
		SetOPModifiedFlag(true);
	}

	IwContext& iwContext = m_pDoc->GetIwContext();
	IwBrep* skchBrep = new (iwContext) IwBrep();
	IwFace* pFace;
	skchBrep->CreateFaceFromSurface(sketchSurface, sketchSurface->GetNaturalUVDomain(), pFace);
	
	//psv---
	CBrepArray arrOfBrep;
	arrOfBrep.Add(skchBrep);
	WriteIges( "C:\\PS\\Ops\\17LExported_SketchSurf_TriathlonF1.igs", NULL, NULL, NULL, NULL, &arrOfBrep );	

	if (pFace == NULL) return;
	
	SetIwBrep(skchBrep);
	MakeTess();

	m_bOPDataModified = true;

	return;
}

bool COutlineProfile::GetSketchSurface
(
	IwBSplineSurface*& sketchSurface
)
{
	IwBSplineSurface* skchSurf = NULL;
	if (m_sketchSurface == NULL)
	{
		bool gotIt = GetSketchSurfaceFromBrep(skchSurf);
		if ( !gotIt ) return false;
	}

	sketchSurface = m_sketchSurface;

	return true;
}

bool COutlineProfile::GetSketchSurfaceFromBrep
(
	IwBSplineSurface*& sketchSurface
)
{
	sketchSurface = NULL;

	if (m_pBrep == NULL) return false;

	IwTArray<IwFace*> faces;
	m_pBrep->GetFaces(faces);

	if (faces.GetSize() != 1) return false;

	IwFace* face = NULL;
	IwSurface* skchSurf = NULL;

	face = faces.GetAt(0);

	if (face == NULL) return false;

	skchSurf = face->GetSurface();

	if (skchSurf == NULL) return false;

	sketchSurface = (IwBSplineSurface*)skchSurf;

	m_sketchSurface = sketchSurface;

	return true;
}

void COutlineProfile::SetOutlineProfileFeaturePoints
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo,
	IwTArray<IwPoint3d>& featurePointsRemap
)
{
	IwTArray<IwPoint3d> inputFeaturePoints;
	inputFeaturePoints.Append(featurePoints);
	IwTArray<int> inputFeaturePointsInfo;
	inputFeaturePointsInfo.Append(featurePointsInfo);
	IwTArray<IwPoint3d> inputFeaturePointsRemap;
	inputFeaturePointsRemap.Append(featurePointsRemap);

	m_outlineProfileFeaturePoints.RemoveAll();
	m_outlineProfileFeaturePointsInfo.RemoveAll();
	m_outlineProfileFeaturePointsRemap.RemoveAll();

	// Note, Outline profile defines a closed curve.
	// The last element should be always identical to the first one.
	for (unsigned i=0; i<(inputFeaturePoints.GetSize()-1); i++)
	{
		m_outlineProfileFeaturePoints.Add(inputFeaturePoints.GetAt(i));
		m_outlineProfileFeaturePointsInfo.Add(inputFeaturePointsInfo.GetAt(i));
		m_outlineProfileFeaturePointsRemap.Add(inputFeaturePointsRemap.GetAt(i));
	}
	m_outlineProfileFeaturePoints.Add(inputFeaturePoints.GetAt(0));
	m_outlineProfileFeaturePointsInfo.Add(inputFeaturePointsInfo.GetAt(0));
	m_outlineProfileFeaturePointsRemap.Add(inputFeaturePointsRemap.GetAt(0));

	// Update m_outlineProfileXYZCurve
	if (m_outlineProfileXYZCurve)
		IwObjDelete delCurve(m_outlineProfileXYZCurve);

	bool updateFeaturePoints = true;
	CreateCurveOnSketchSurface(m_outlineProfileFeaturePoints, m_outlineProfileFeaturePointsInfo, updateFeaturePoints, m_outlineProfileXYZCurve, m_uvCurve);
	
	// Project onto inner surface
	//ProjectOutlineProfileCurveOntoInnerCutPlanes(NULL);

	if ( m_pDoc->isPosteriorStabilized() )
		DetermineBoxCutCurves();

	SetOPModifiedFlag(true);

	//**** Do not delete UVCurve. It is refered by m_outlineProfileXYZCurve.
	//if (UVCurve)
	//	IwObjDelete delUVCrv(UVCurve);

}

void COutlineProfile::GetOutlineProfileFeaturePoints
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo,
	IwTArray<IwPoint3d>& featurePointsRemap
)
{
	featurePoints.RemoveAll();
	featurePointsInfo.RemoveAll();
	featurePointsRemap.RemoveAll();

	featurePoints.Append(m_outlineProfileFeaturePoints);
	featurePointsInfo.Append(m_outlineProfileFeaturePointsInfo);
	featurePointsRemap.Append(m_outlineProfileFeaturePointsRemap);
}

void COutlineProfile::SetOutlineProfileFeaturePointsValidation
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo
)
{
	if ( featurePoints.GetSize() == 0 )
		return;

	IwTArray<IwPoint3d> inputFeaturePoints;
	inputFeaturePoints.Append(featurePoints);
	IwTArray<int> inputFeaturePointsInfo;
	inputFeaturePointsInfo.Append(featurePointsInfo);

	m_outlineProfileFeaturePointsValidation.RemoveAll();
	m_outlineProfileFeaturePointsInfoValidation.RemoveAll();

	// Note, Outline profile defines a closed curve.
	// The last element should be always identical to the first one.
	for (unsigned i=0; i<(inputFeaturePoints.GetSize()-1); i++)
	{
		m_outlineProfileFeaturePointsValidation.Add(inputFeaturePoints.GetAt(i));
		m_outlineProfileFeaturePointsInfoValidation.Add(inputFeaturePointsInfo.GetAt(i));
	}
	m_outlineProfileFeaturePointsValidation.Add(inputFeaturePoints.GetAt(0));
	m_outlineProfileFeaturePointsInfoValidation.Add(inputFeaturePointsInfo.GetAt(0));

	IwBSplineCurve *UVCurve = NULL;
	bool updateFeaturePoints = true;
	CreateCurveOnSketchSurface(m_outlineProfileFeaturePointsValidation, m_outlineProfileFeaturePointsInfoValidation, updateFeaturePoints, m_outlineProfileXYZCurveValidation, UVCurve);
}

void COutlineProfile::GetOutlineProfileFeaturePointsValidation
(
	IwTArray<IwPoint3d>& featurePoints, 
	IwTArray<int>& featurePointsInfo
)
{
	featurePoints.RemoveAll();
	featurePointsInfo.RemoveAll();

	featurePoints.Append(m_outlineProfileFeaturePointsValidation);
	featurePointsInfo.Append(m_outlineProfileFeaturePointsInfoValidation);
}

int COutlineProfile::GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex
(
	double femoralCutsFeatureIndex	// I:
)
{
	int outlineProfileFeatureIndex = -1;
	if ( m_outlineProfileFeaturePointsRemap.GetSize() == 0 )
		return outlineProfileFeatureIndex;

	for (unsigned i=0; i<m_outlineProfileFeaturePointsRemap.GetSize(); i++)
	{
		double positionIndex = m_outlineProfileFeaturePointsRemap.GetAt(i).z;
		if ( IS_EQ_TOL6(positionIndex, femoralCutsFeatureIndex) )
		{
			outlineProfileFeatureIndex = i;
			return outlineProfileFeatureIndex;
		}
	}

	return outlineProfileFeatureIndex;
}

void COutlineProfile::DisplayOutlineProfileProjectedCurve()
{
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glLineWidth(3.0);

	IwPoint3d	pt;
	IwPoint3d sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );
	IwExtent1d sIvl;
	double	dChordHeightTol = 0.3;
	double	dAngleTolInDegrees = 5.0;
	int nPoints;

	glColor4ub( 128, 255, 0, 64);

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < m_outlineProfileXYZCurvePoints.GetSize(); k++ ) 
	{
		pt = m_outlineProfileXYZCurvePoints.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();

	glLineWidth(1.0);

	///////////////////////////////////////////////////////////
	// for validation
	//if ( !m_pDoc->DisplayValidationData() )//PSV--- to display outline profile
	//	return;

	if (m_outlineProfileXYZCurveValidation == NULL) return;

	glLineWidth(2.0);

	glColor4ub( 0, 127, 0, 64);

	sIvl = m_outlineProfileXYZCurveValidation->GetNaturalInterval();

	IwCurveTessDriver sCrvTessValidation( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

	sPnts.RemoveAll();

	sCrvTessValidation.TessellateCurve( *m_outlineProfileXYZCurveValidation, sIvl, NULL, &sPnts );

	nPoints = sPnts.GetSize();

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
	{
		pt = sPnts.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();

	glLineWidth(1.0);

}

void COutlineProfile::DisplayOutlineProfile()
{

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glLineWidth(3.0);

	IwPoint3d	pt;
	IwPoint3d sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );
	IwExtent1d sIvl;
	double	dChordHeightTol = 0.3;
	double	dAngleTolInDegrees = 5.0;
	int nPoints;

	if (m_outlineProfileXYZCurve)
	{
		sIvl = m_outlineProfileXYZCurve->GetNaturalInterval();

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *m_outlineProfileXYZCurve, sIvl, NULL, &sPnts );

		nPoints = sPnts.GetSize();

		glColor4ub( 92, 212, 0, 64);

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);
	glColor4ub( 128, 255, 0, 64);

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < m_outlineProfileXYZCurvePoints.GetSize(); k++ ) 
	{
		pt = m_outlineProfileXYZCurvePoints.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();

}

void COutlineProfile::DisplayRulerCurves()
{
	IwPoint3d	pt;
	IwPoint3d sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glEnable(GL_BLEND);
	glDepthMask(GL_TRUE);

	for (unsigned i=0; i<m_posRulerCurves.GetSize(); i++)
	{
		glLineWidth(1.0);
		if ( (i-5)%10 == 0 )
			glLineWidth(2.0);

		if (i%5 == 0)
			glColor4ub( black[0], black[1], black[2], 128);
		else
			glColor4ub( darkGray[0], darkGray[1], darkGray[2], m_pDoc->GetTransparencyFactor());

		IwCurve* curve = m_posRulerCurves.GetAt(i);

		IwExtent1d sIvl = curve->GetNaturalInterval();

		double	dChordHeightTol = 0.75;
		double	dAngleTolInDegrees = 8.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

		sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	for (unsigned i=0; i<m_negRulerCurves.GetSize(); i++)
	{
		glLineWidth(1.0);
		if ( (i-5)%10 == 0 )
			glLineWidth(2.0);

		if (i%5 == 0)
			glColor4ub( black[0], black[1], black[2], 128);
		else
			glColor4ub( darkGray[0], darkGray[1], darkGray[2], m_pDoc->GetTransparencyFactor());

		IwCurve* curve = m_negRulerCurves.GetAt(i);

		IwExtent1d sIvl = curve->GetNaturalInterval();

		double	dChordHeightTol = 0.75;
		double	dAngleTolInDegrees = 8.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

		sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);
}


void COutlineProfile::DisplayBoxCut()
{
	IwPoint3d	pt;
	IwPoint3d sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glEnable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(2.0);
	glColor4ub( 96, 96, 48, 128);

	for (unsigned i=0; i<m_boxCutCurves.GetSize(); i++)
	{
		IwCurve* curve = m_boxCutCurves.GetAt(i);

		IwExtent1d sIvl = curve->GetNaturalInterval();

		double	dChordHeightTol = 0.75;
		double	dAngleTolInDegrees = 8.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

		sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);
}

void COutlineProfile::DisplayOutlineFeaturePoints()
{
	IwPoint3d pnt;

	IwVector3d offsetVec = -5*m_pDoc->GetView()->GetViewingVector();

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	for (unsigned i=0; i<m_outlineProfileFeaturePointsInfo.GetSize(); i++)
	{
		m_sketchSurface->EvaluatePoint(IwVector2d(m_outlineProfileFeaturePoints.GetAt(i).x, m_outlineProfileFeaturePoints.GetAt(i).y), pnt);
		pnt = pnt + offsetVec; // make it closer to the viewers.
		if (i==0 || i== (m_outlineProfileFeaturePointsInfo.GetSize()-1)) // seam point
		{
			glColor3ub(0, 0, 0);
			glPointSize(8);
		}
		else if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 0) // interpolation points
		{
			// remap z value (,,z). if z=-1 -> pure non remap-able point. if z=-1.xxxx (xxxx=some values), non remap-able point now, but can be converted into a remap-able point.
			IwPoint3d remap = m_outlineProfileFeaturePointsRemap.GetAt(i);
			int zValue0 = (int)remap.z;
			int zValue2 = (int)(100*(remap.z-zValue0));
			if ( zValue0 == -1 ) // non remap-able points
			{
				if ( zValue2 == 0 ) // pure non remap-able point
				{
					glColor3ub(98, 98, 255);
					glPointSize(6);
				}
				else  // non remap-able point now, but can be converted into remap-able point.
				{
					glColor3ub(152, 0, 255);
					glPointSize(6);
				}
			}
			else // remap-able points (cut edge points & edge middle points)
			{
				glColor3ub(0, 0, 128);
				glPointSize(6);
			}
		}
		else if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 1 || // arc center points
				 m_outlineProfileFeaturePointsInfo.GetAt(i) == 2)
		{
			glColor3ub( 98, 0, 255 );
			glPointSize(6);
		}
		else if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 3 || // arc start points
			m_outlineProfileFeaturePointsInfo.GetAt(i) == 4)	    // arc end points
		{
			glColor3ub( 128, 0, 255 );
			glPointSize(6);
		}

		glBegin( GL_POINTS );

			glVertex3d( pnt.x, pnt.y, pnt.z );

		glEnd();
	}

	///////// For validation
	if ( !m_pDoc->DisplayValidationData() )
		return;

	glPointSize(5);

	for (unsigned i=0; i<m_outlineProfileFeaturePointsInfoValidation.GetSize(); i++)
	{
		m_sketchSurface->EvaluatePoint(IwVector2d(m_outlineProfileFeaturePointsValidation.GetAt(i).x, m_outlineProfileFeaturePoints.GetAt(i).y), pnt);
		glColor3ub( 0, 64, 0 );

		glBegin( GL_POINTS );

			glVertex3d( pnt.x, pnt.y, pnt.z );

		glEnd();
	}


	glPointSize(1);

}

bool COutlineProfile::GetCurveOnSketchSurface
(
	IwBSplineCurve*& crvOnSurf, // O:
	IwBSplineCurve*& UVCurve,	// O:
	bool trueCurveOnSurf		// I:
)
{
	crvOnSurf = NULL;
	UVCurve = NULL;
	bool updateFeaturePoints = false;
	CreateCurveOnSketchSurface(m_outlineProfileFeaturePoints, m_outlineProfileFeaturePointsInfo, updateFeaturePoints, crvOnSurf, UVCurve, trueCurveOnSurf);

	if (crvOnSurf == NULL) return false;

	return true;

}
///////////////////////////////////////////////////////////////////
// If updateFeaturePoints=true, the arc start/end points in 
// featurePoints will be modified based on arc calculation.
///////////////////////////////////////////////////////////////////
bool COutlineProfile::CreateCurveOnSketchSurface
(
	IwTArray<IwPoint3d>& featurePoints,		// I/O:
	IwTArray<int>& featurePointsInfo,		// I:
	bool updateFeaturePoints,				// I:
	IwBSplineCurve*& crvOnSurf,				// O:
	IwBSplineCurve*& UVCurve,				// O:
	bool trueCurveOnSurf					// I: if true, create a true curve on surface, otherwise, just approximate.
)
{
    m_arrOfArrOfIntPoints.Add(featurePoints);

	crvOnSurf = NULL;
	IwBSplineSurface* skchSurf = NULL;

	if ( !GetSketchSurface(skchSurf) ) return false;

	IwTArray<IwPoint3d> interPnts, arcInterPnts;
	for (unsigned i=0; i<featurePointsInfo.GetSize(); )
	{
		if (featurePointsInfo.GetAt(i) == 0) // Interpolation point
		{
			interPnts.Add(featurePoints.GetAt(i));
			i += 1;// move forward one element
		}
		else if (featurePointsInfo.GetAt(i) == 3) // arc start point
		{
			IwPoint3d stPnt, edPnt;
			if ( CreateArcInterpolatingPoints(featurePoints, i, arcInterPnts, stPnt, edPnt) )
			{
				if (updateFeaturePoints)
				{
					featurePoints.SetAt(i, stPnt);
					featurePoints.SetAt(i+2, edPnt);
				}
				interPnts.Append(arcInterPnts);
			}
			i += 3;// move forward 3 elements, no matter succeed or fail
		}
	}


	IwContext& iwContext = m_pDoc->GetIwContext();
	IwBSplineCurve::InterpolatePoints(iwContext, interPnts, NULL, 2, NULL, NULL, TRUE, IW_IT_CHORDLENGTH, UVCurve);
	if (UVCurve == NULL) return false;

	IwCrvOnSurf* myCrvOnSurf = new (iwContext) IwCrvOnSurf(*UVCurve, *skchSurf);
	if ( trueCurveOnSurf )
	{
		crvOnSurf = (IwBSplineCurve*)myCrvOnSurf;
		return true;
	}

	// the myCrvOnSurf is a curve on surface. It refers to the surface when evaluate. When the surface is updated
	// (the original surface is deleted), the curve on surface has a problem to evaluate itself.
	// To be simple, just create a 3D curve here. 
	if ( myCrvOnSurf )
	{
		IwTArray<double> breakParams;
		breakParams.Add(myCrvOnSurf->GetNaturalInterval().GetMin());
		breakParams.Add(myCrvOnSurf->GetNaturalInterval().GetMax());
		double aTol;
		myCrvOnSurf->ApproximateCurve(iwContext, IW_AA_HERMITE, breakParams, 0.001, aTol, crvOnSurf, FALSE, FALSE);
	}

	return true;
}

IwBSplineCurve* COutlineProfile::ConvertUVCurveToXYZCurve(IwBSplineCurve* UVCurve, bool trueCurveOnSurf)
{
	IwBSplineCurve* crvOnSurf = NULL;
	IwBSplineSurface* skchSurf = NULL;

	if ( !GetSketchSurface(skchSurf) ) return NULL;

	IwCrvOnSurf* myCrvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*UVCurve, *skchSurf);
	if ( trueCurveOnSurf )
	{
		crvOnSurf = (IwBSplineCurve*)myCrvOnSurf;
		return crvOnSurf;
	}

	// the myCrvOnSurf is a curve on surface. It refers to the surface when evaluate. When the surface is updated
	// (the original surface is deleted), the curve on surface has a problem to evaluate itself.
	// To be simple, just create a 3D curve here. 
	if ( myCrvOnSurf )
	{
		IwTArray<double> breakParams;
		breakParams.Add(myCrvOnSurf->GetNaturalInterval().GetMin());
		breakParams.Add(myCrvOnSurf->GetNaturalInterval().GetMax());
		double aTol;
		myCrvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, crvOnSurf, FALSE, FALSE);
	}

	return crvOnSurf;
}

/////////////////////////////////////////////////////////////////
// This function uses the 3 points (start from the posIndex) to
// define an arc and output arc interpolating points
/////////////////////////////////////////////////////////////////
bool COutlineProfile::CreateArcInterpolatingPoints
(
	IwTArray<IwPoint3d>& featurePoints, // I:
	int posIndex,						// I:
	IwTArray<IwPoint3d>& interPoints,	// O:
	IwPoint3d &arcStPnt,				// O:
	IwPoint3d &arcEdPnt					// O:
)
{
	interPoints.RemoveAll();

	IwPoint3d arcStartPnt, arcCenterPnt, arcEndPnt;
	IwPoint3d prevPnt, postPnt;
	// We do not check whether featurePoints has the following elements.
	// If not, just let it error out.
	prevPnt = featurePoints.GetAt(posIndex-1);
	arcStartPnt = featurePoints.GetAt(posIndex);
	arcCenterPnt = featurePoints.GetAt(posIndex+1);
	arcEndPnt = featurePoints.GetAt(posIndex+2);
	postPnt = featurePoints.GetAt(posIndex+3);

	IwVector3d vecPrevToStart = arcStartPnt - prevPnt;
	vecPrevToStart.Unitize();
	IwVector3d vecStartToCenter = arcCenterPnt - arcStartPnt;
	double radius = vecStartToCenter.Length();
	vecStartToCenter.Unitize();
	IwVector3d vecCenterToEnd =  arcEndPnt - arcCenterPnt;
	vecCenterToEnd.Unitize();
	IwVector3d zAxis = vecPrevToStart*vecStartToCenter;
	IwVector3d yAxis = -zAxis*vecPrevToStart;
	IwVector3d xAxis = yAxis*zAxis;

	double startAngle, endAngle, deltaAngle;
	xAxis.AngleBetween(-vecStartToCenter, startAngle);
	startAngle = startAngle*180.0/3.1415926;
	vecCenterToEnd.AngleBetween(-vecStartToCenter, deltaAngle);
	if (deltaAngle < 1.5707963)
	{
		endAngle = startAngle + deltaAngle*180.0/3.1415926;
	}
	else
	{
		xAxis.AngleBetween(vecCenterToEnd, endAngle);
		endAngle = 360 - endAngle*180.0/3.1415926;
	}

	IwContext& iwContext = m_pDoc->GetIwContext();
	IwAxis2Placement axes(arcCenterPnt, xAxis, yAxis);
	IwBSplineCurve* arcSeg=NULL;
	IwBSplineCurve::CreateCircleSegment(iwContext, 2, axes, radius, startAngle, endAngle, IW_CO_QUADRATIC, arcSeg);

	if (arcSeg == NULL) return false;
	// Get end points
	arcSeg->GetEnds(arcStPnt, arcEdPnt);

	IwExtent1d segDomain = arcSeg->GetNaturalInterval();
	double deltaT = (segDomain.GetLength()/16.0);
	double t = segDomain.GetMin();	
	IwPoint3d intPnt;
	for (int i=0; i<17; t+=deltaT,i++)
	{
		arcSeg->EvaluatePoint(t, intPnt);
		interPoints.Add(intPnt);
	}

	return true;
}

void COutlineProfile::GetSelectableEntities
(
	IwTArray<IwBrep*>& breps,		// O:
	IwTArray<IwCurve*>& curves,		// O:
	IwTArray<IwPoint3d>& points		// O:
)
{
	curves.Add(m_outlineProfileXYZCurve);

	// Outline profile sketch surface represented in brep in CPart is not selectable.
	// Therefore, we do not want to append to breps.
	IwTArray<IwBrep*> myBrep;
	CPart::GetSelectableEntities(myBrep, curves, points);
}

void COutlineProfile::GetRulerCurves(IwTArray<IwCurve*> &posCurves, IwTArray<IwCurve*> &negCurves)
{
	DetermineRulerCurves();

	posCurves.RemoveAll();
	negCurves.RemoveAll();

	posCurves.Append(m_posRulerCurves);
	negCurves.Append(m_negRulerCurves);
}

void COutlineProfile::DetermineRulerCurves()
{

}

void COutlineProfile::DetermineBoxCutCurves()
{

}

//////////////////////////////////////////////////////////////////////////
// The weight bearing center is defined as the average center 
// at 3 locations -> 0, 45, 90 flexion.
IwPoint3d COutlineProfile::DetermineOutlineProfileWeightBearingMLCenter
(
	IwTArray<IwPoint3d> *MProfilePoints,	// medial side profile points at 0, 45, and 90 degrees
	IwTArray<IwPoint3d> *LProfilePoints		// lateral side profile points at 0, 45, and 90 degrees
)
{
	IwPoint3d weightBearingCenter = IwPoint3d();
	if (MProfilePoints)
		MProfilePoints->RemoveAll();
	if (LProfilePoints)
		LProfilePoints->RemoveAll();

	// Get xyz curve
	IwBSplineCurve* profileXYZCurve = GetOutlineProfileXYZCurve();
	if ( profileXYZCurve == NULL )
		return IwPoint3d();

	// Get mechanical axis
	CFemoralAxes* femoralAxes = GetTotalDoc()->GetFemoralAxes();
	if (femoralAxes == NULL) 
		return IwPoint3d();
	IwAxis2Placement mechAxes;
	femoralAxes->GetMechanicalAxes(mechAxes);

	// Get J-curve
	//CJCurves* pJCurves = GetTotalDoc()->GetJCurves();
	//if ( pJCurves == NULL )
	//	return IwPoint3d();
	IwBSplineCurve* medialJCurve = IFemurImplant::JCurvesInp_GetJCurve(0);//(IwBSplineCurve*)pJCurves->GetJCurve(0);//jcurve erik inp
	if (medialJCurve == NULL)
		return IwPoint3d();

	// Determine the jcurve M-Point (0 flexion)
	IwPoint3d farDownPnt = mechAxes.GetOrigin() - 10000*mechAxes.GetZAxis();
	IwPoint3d MPoint;
	double param;
	DistFromPointToCurve(farDownPnt, medialJCurve, MPoint, param);
	IwVector3d MNormal = mechAxes.GetYAxis();
	// Determine the jcurve posterior point (90 flexion)
	IwPoint3d farPostPnt = mechAxes.GetOrigin() - 10000*mechAxes.GetYAxis();
	IwPoint3d PPoint;
	DistFromPointToCurve(farPostPnt, medialJCurve, PPoint, param);
	IwVector3d PNormal = -mechAxes.GetZAxis();
	// Determine the jcurve mid-flexion point (45 flexion)
	IwPoint3d farMidFlexionPnt = mechAxes.GetOrigin() - 10000*mechAxes.GetYAxis() - 10000*mechAxes.GetZAxis();
	IwPoint3d midFlexionPoint;
	DistFromPointToCurve(farMidFlexionPnt, medialJCurve, midFlexionPoint, param);
	IwVector3d midFlexionNormal = MNormal + PNormal;
	midFlexionNormal.Unitize();

if (0)
{
	ShowPoint(m_pDoc, MPoint, magenta);
	ShowPoint(m_pDoc, midFlexionPoint, red);
	ShowPoint(m_pDoc, PPoint, brown);
}

	// Determine the outline profile width at 0, 45, 90 flexion
	IwPoint3d posFlexionPnt0, negFlexionPnt0, avgFlexionPnt0;
	IntersectCurveByPlane(profileXYZCurve, MPoint+200*mechAxes.GetXAxis(), MNormal, posFlexionPnt0);
	IntersectCurveByPlane(profileXYZCurve, MPoint-200*mechAxes.GetXAxis(), MNormal, negFlexionPnt0);
	avgFlexionPnt0 = 0.5*posFlexionPnt0 + 0.5*negFlexionPnt0;
	IwPoint3d posFlexionPnt45, negFlexionPnt45, avgFlexionPnt45;
	IntersectCurveByPlane(profileXYZCurve, midFlexionPoint+200*mechAxes.GetXAxis(), midFlexionNormal, posFlexionPnt45);
	IntersectCurveByPlane(profileXYZCurve, midFlexionPoint-200*mechAxes.GetXAxis(), midFlexionNormal, negFlexionPnt45);
	avgFlexionPnt45 = 0.5*(posFlexionPnt45+negFlexionPnt45);
	IwPoint3d posFlexionPnt90, negFlexionPnt90, avgFlexionPnt90;
	IntersectCurveByPlane(profileXYZCurve, PPoint+200*mechAxes.GetXAxis(), PNormal, posFlexionPnt90);
	IntersectCurveByPlane(profileXYZCurve, PPoint-200*mechAxes.GetXAxis(), PNormal, negFlexionPnt90);
	avgFlexionPnt90 = 0.5*(posFlexionPnt90+negFlexionPnt90);
	// Add to output
	if (MProfilePoints && LProfilePoints)
	{
		if ( m_pDoc->IsPositiveSideLateral() )
		{
			MProfilePoints->Add(negFlexionPnt0);
			MProfilePoints->Add(negFlexionPnt45);
			MProfilePoints->Add(negFlexionPnt90);
			LProfilePoints->Add(posFlexionPnt0);
			LProfilePoints->Add(posFlexionPnt45);
			LProfilePoints->Add(posFlexionPnt90);
		}
		else
		{
			MProfilePoints->Add(posFlexionPnt0);
			MProfilePoints->Add(posFlexionPnt45);
			MProfilePoints->Add(posFlexionPnt90);
			LProfilePoints->Add(negFlexionPnt0);
			LProfilePoints->Add(negFlexionPnt45);
			LProfilePoints->Add(negFlexionPnt90);
		}
	}
if (0)
{
	ShowPoint(m_pDoc, posFlexionPnt0, magenta);
	ShowPoint(m_pDoc, negFlexionPnt0, magenta);
	ShowPoint(m_pDoc, posFlexionPnt45, red);
	ShowPoint(m_pDoc, negFlexionPnt45, red);
	ShowPoint(m_pDoc, posFlexionPnt90, brown);
	ShowPoint(m_pDoc, negFlexionPnt90, brown);
}

	// All project onto MPoint x-axis line
	avgFlexionPnt0 = avgFlexionPnt0.ProjectPointToPlane(MPoint, mechAxes.GetYAxis());
	avgFlexionPnt0 = avgFlexionPnt0.ProjectPointToPlane(MPoint, mechAxes.GetZAxis());
	avgFlexionPnt45 = avgFlexionPnt45.ProjectPointToPlane(MPoint, mechAxes.GetYAxis());
	avgFlexionPnt45 = avgFlexionPnt45.ProjectPointToPlane(MPoint, mechAxes.GetZAxis());
	avgFlexionPnt90 = avgFlexionPnt90.ProjectPointToPlane(MPoint, mechAxes.GetYAxis());
	avgFlexionPnt90 = avgFlexionPnt90.ProjectPointToPlane(MPoint, mechAxes.GetZAxis());

	// average overall center
	double oneThird = 1.0/3.0;
	weightBearingCenter = oneThird*avgFlexionPnt0 + oneThird*avgFlexionPnt45 + oneThird*avgFlexionPnt90;
if (0)
{
	ShowPoint(m_pDoc, weightBearingCenter, green);
}

	return weightBearingCenter;
}

/////////////////////////////////////////////////////////////////
bool COutlineProfile::ConvertSketchSurfaceUVtoXYZ
(
	IwPoint3d& inputUVPnt,		// I:
	IwPoint3d& outputXYZPnt,	// O:
	IwVector3d& outputXYZNormal	// O:
)
{

	IwBSplineSurface* sketchSurface = NULL;
	GetSketchSurface(sketchSurface);
	if (sketchSurface == NULL) return false;

	IwPoint2d uv = IwPoint2d(inputUVPnt.x, inputUVPnt.y);
	sketchSurface->EvaluatePoint(uv, outputXYZPnt);
	sketchSurface->EvaluateNormal(uv, FALSE, FALSE, outputXYZNormal);

	return true;
}

bool COutlineProfile::ConvertSketchSurfaceUVtoXYZ
(
	IwTArray<IwPoint3d>& inputUVPnts,		// I:
	IwTArray<IwPoint3d>& outputXYZPnts		// O:
)
{
	if (m_sketchSurface == NULL) return false;

	IwPoint3d uvPnt, xyzPnt;
	IwPoint2d uv;
	for (unsigned i=0; i<inputUVPnts.GetSize(); i++)
	{
		uvPnt = inputUVPnts.GetAt(i);
		uv = IwPoint2d(uvPnt.x, uvPnt.y);
		m_sketchSurface->EvaluatePoint(uv, xyzPnt);
		outputXYZPnts.Add(xyzPnt);
	}

	return true;
}

void COutlineProfile::SetOPModifiedFlag( bool bModified )
{
	m_outlineProfileModified = bModified;
	if (m_outlineProfileModified)
	{
		m_bOPDataModified = true;
		m_pDoc->SetModified(m_outlineProfileModified);
	}
}

bool COutlineProfile::GetNotchArcMiddlePoint
(
	IwPoint3d& middlePnt,	// O:
	IwPoint2d& middleUVPnt	// O:
)
{
	IwTArray<IwPoint3d> arcInterPnts;
	IwPoint3d stPnt, edPnt;
	for (unsigned i=0; i<m_outlineProfileFeaturePointsInfo.GetSize(); i++)
	{
		if (m_outlineProfileFeaturePointsInfo.GetAt(i) == 2)// CW , notch arc is CW
		{
			CreateArcInterpolatingPoints(m_outlineProfileFeaturePoints, i-1, arcInterPnts, stPnt, edPnt);
			break;
		}
	}

	int nSize = arcInterPnts.GetSize();
	if (nSize == 0) return false;

	int mIndex = (int)(nSize/2.0);
	
	IwPoint3d uvPnt = arcInterPnts.GetAt(mIndex);

	middleUVPnt = IwPoint2d(uvPnt.x, uvPnt.y);
	m_sketchSurface->EvaluatePoint(middleUVPnt, middlePnt);

	return true;
}

///////////////////////////////////////////////////////////////////
// This function returns the posterior tip points.
///////////////////////////////////////////////////////////////////
bool COutlineProfile::GetPosteriorTipPoints
(
	IwPoint3d& posTipPnt,	// O:
	IwPoint3d& negTipPnt	// O:
)
{
	if ( GetTotalDoc()->GetFemoralCuts() == NULL )
		return false;

	if ( m_outlineProfileXYZCurve == NULL )
		return false;
	
	if ( GetTotalDoc()->GetFemoralAxes() == NULL )
		return false;
	IwAxis2Placement sweepAxes;
	GetTotalDoc()->GetFemoralAxes()->GetSweepAxes(sweepAxes);
	IwVector3d zAxis = sweepAxes.GetZAxis();

	IwTArray<IwPoint3d> featurePoints;
	GetTotalDoc()->GetFemoralCuts()->GetFeaturePoints(featurePoints, NULL);

	if ( featurePoints.GetSize() == 0 )
		return false;

	// Determine positive side tip point
	IwPoint3d posFarZPnt = featurePoints.GetAt(0) + 30.0*zAxis;
	IwPoint3d cPntOutlineProfile;
	double param;
	DistFromPointToCurve(posFarZPnt, m_outlineProfileXYZCurve, cPntOutlineProfile, param);
	posTipPnt = cPntOutlineProfile;

	// For nagative side
	IwPoint3d negFarZPnt = featurePoints.GetAt(1) + 30.0*zAxis;
	DistFromPointToCurve(negFarZPnt, m_outlineProfileXYZCurve, cPntOutlineProfile, param);
	negTipPnt = cPntOutlineProfile;

	return true;

}

//////////////////////////////////////////////////////////////////////////////////
// They are on anterior cut plane.
// 0th element: central point, 1st: positive side point, 2nd: negative side point
// These 3 point define the anterior "ear" shape.
//////////////////////////////////////////////////////////////////////////////////
bool COutlineProfile::GetAnteriorEarFeaturePoints
(
	IwTArray<IwPoint3d>& antFeaturePoints	// O:
)
{
	m_pDoc->AppendLog( QString("COutlineProfile::GetAnteriorEarFeaturePoints()") );

	antFeaturePoints.RemoveAll();

	//
	IwTArray<IwPoint3d> featurePoints, featurePointsRemap;
	IwTArray<int> featurePointsInfo;
	GetOutlineProfileFeaturePoints(featurePoints, featurePointsInfo, featurePointsRemap);
	unsigned nSize = featurePoints.GetSize();
	if ( nSize == 0 )
		return false;

	// centralIndentPoint
	IwPoint3d uvPnt = featurePoints.GetAt(0);
	IwPoint3d centralIndentPoint;
	IwVector3d normal;
	ConvertSketchSurfaceUVtoXYZ(uvPnt, centralIndentPoint, normal);
	// posEarPoint
	IwPoint3d posEarPoint;
	if ( featurePointsInfo.GetAt(1) == 0 )// interpolation point
	{
		uvPnt = featurePoints.GetAt(1);
		ConvertSketchSurfaceUVtoXYZ(uvPnt, posEarPoint, normal);
	}
	else // arc
	{
		double radius = featurePoints.GetAt(1).DistanceBetween(featurePoints.GetAt(2));
		uvPnt = featurePoints.GetAt(2) + radius*IwVector3d(0,-1,0);// move anteriorly up
		ConvertSketchSurfaceUVtoXYZ(uvPnt, posEarPoint, normal);
	}
	// negEarPoint
	IwPoint3d negEarPoint;
	if ( featurePointsInfo.GetAt(nSize-2) == 0 )// interpolation point
	{
		uvPnt = featurePoints.GetAt(nSize-2);
		ConvertSketchSurfaceUVtoXYZ(uvPnt, negEarPoint, normal);
	}
	else // arc
	{
		double radius = featurePoints.GetAt(nSize-2).DistanceBetween(featurePoints.GetAt(nSize-3));
		uvPnt = featurePoints.GetAt(nSize-3) + radius*IwVector3d(0,-1,0);// move anteriorly up
		ConvertSketchSurfaceUVtoXYZ(uvPnt, negEarPoint, normal);
	}

	antFeaturePoints.Add(centralIndentPoint);
	antFeaturePoints.Add(posEarPoint);
	antFeaturePoints.Add(negEarPoint);


if (0)
{
ShowPoint(m_pDoc, centralIndentPoint, blue);
ShowPoint(m_pDoc, posEarPoint, blue);
ShowPoint(m_pDoc, negEarPoint, darkGreen);
}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////
// They are on femoral (sweeping) x-z plane.
// 0th element: central point, 1st: positive side point, 2nd: negative side point
// These 3 point define the anterior "ear" shape.
//////////////////////////////////////////////////////////////////////////////////
bool COutlineProfile::GetInitialAnteriorEarFeaturePoints
(
	CTotalDoc* pDoc,
	IwTArray<IwPoint3d>& antFeaturePoints	// O:
)
{
	pDoc->AppendLog( QString("COutlineProfile::GetInitialAnteriorFeaturePoints()") );

	antFeaturePoints.RemoveAll();

	// get needed info
	CFemoralAxes* femoralAxes = pDoc->GetFemoralAxes();
	if ( !femoralAxes ) return false;
	IwAxis2Placement femAxes;
	femoralAxes->GetSweepAxes(femAxes);
	double refSize = femoralAxes->GetEpiCondyleSize();
	double ratio = refSize/76.0;

	// Get ant plane info
	IwPoint3d antPlaneCenter, antPlaneRotationCenter, antPlaneOrientation;
	femoralAxes->GetAntCutPlaneInfo(antPlaneCenter, antPlaneOrientation, &antPlaneRotationCenter);
	
	if(0)
	{
		antPlaneCenter = IwPoint3d(-99.11, 13.04, -517.13);//psv---
	}

if(0)
ShowPoint(pDoc, antPlaneCenter, magenta);

	QString sideInfo;
	CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral = false;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}

	// centralIndentPoint
	IwPoint3d centralIndentPoint;

	IwBSplineCurve * pTJCurve = IFemurImplant::JCurvesInp_GetJCurve(2);

	if(0)
	{
		//pTJCurve = (IwBSplineCurve *)pDoc->GetJCurves()->GetJCurve(2);
	}

	if ( /*pDoc->GetJCurves() && pDoc->GetJCurves()->GetJCurve(2)*/  pTJCurve  )
	{
		// find the closest point on trochlear curve then project onto ant cut plane
		IwBSplineCurve* tCurve = pTJCurve/*(IwBSplineCurve*)pDoc->GetJCurves()->GetJCurve(2)*/;
		IwPoint3d cPnt;
		double param;
		DistFromPointToCurve(antPlaneCenter, tCurve, cPnt, param);
		// project
		centralIndentPoint = cPnt.ProjectPointToPlane(antPlaneCenter, antPlaneOrientation);
	}
	else
	{
		centralIndentPoint = antPlaneCenter;
	}
if(0)
ShowPoint(pDoc, centralIndentPoint, yellow);

	// Ear points
	double earLateralXShift = fabs((antPlaneCenter-antPlaneRotationCenter).Dot(femAxes.GetXAxis()));
	earLateralXShift = earLateralXShift + 1.5;// Always make it bigger. If it is really too big (overhanging), it will be push back inside of cut profile.
	// Double check to prevent too big and too small
	if ( earLateralXShift > 1.75*10.6*ratio )
		earLateralXShift = 1.75*10.6*ratio;
	if ( earLateralXShift < 0.70*10.6*ratio )
		earLateralXShift = 0.70*10.6*ratio;

	double posXShift, posZShift, negXShift, negZShift;
	IwPoint3d posEarPoint, negEarPoint;
	if ( positiveSideIsLateral )
	{
		// for lateral ear
		posXShift = earLateralXShift;//10.6*ratio;
		posZShift = 2.4*ratio;//2.4*ratio;
		// for medial ear
		negXShift = -8.7*ratio;//-8.7*ratio;
		if ( IsAsymmetricEarShaped(pDoc) ) // AsymmetricEarShaped is almost a slope line.
			negZShift = -2.25*ratio;//-2.25*ratio;
		else
			negZShift = 2.25*ratio;//2.25*ratio;
	}
	else
	{
		// for medial ear
		posXShift = 8.7*ratio;
		if ( IsAsymmetricEarShaped(pDoc) ) // AsymmetricEarShaped is almost a slope line.
			posZShift = -2.25*ratio;
		else
			posZShift = 2.25*ratio;
		// for lateral ear
		negXShift = -earLateralXShift;
		negZShift = 2.4*ratio;
	}

	posEarPoint = centralIndentPoint + posXShift*femAxes.GetXAxis() + posZShift*femAxes.GetZAxis();
	negEarPoint = centralIndentPoint + negXShift*femAxes.GetXAxis() + negZShift*femAxes.GetZAxis();
	// project onto ant plane
	posEarPoint = posEarPoint.ProjectPointToPlane(antPlaneCenter, antPlaneOrientation);
	negEarPoint = negEarPoint.ProjectPointToPlane(antPlaneCenter, antPlaneOrientation);

	antFeaturePoints.Add(centralIndentPoint);
	antFeaturePoints.Add(posEarPoint);
	antFeaturePoints.Add(negEarPoint);

if (0)
{
ShowPoint(pDoc, centralIndentPoint, blue);
ShowPoint(pDoc, posEarPoint, blue);
ShowPoint(pDoc, negEarPoint, darkGreen);
}

	return true;
}

//////////////////////////////////////////////////////////////////////
// This function determines outline profile width. 
// Note - CSolidImplant::DetermineSolidImplantSizeML() determines 
//        implant ML width based on actual implant size. 
//////////////////////////////////////////////////////////////////////
double COutlineProfile::GetImplantMLSize
(
	IwPoint3d* posPoint,	// O: postive side extreme point
	IwPoint3d* negPoint		// O: negative side extreme point
)
{
	double MLWidth = -1;

	if ( m_outlineProfileXYZCurve == NULL ) return -1;

	IwAxis2Placement wslAxes;
	GetTotalDoc()->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);

	IwPoint3d posFarPoint = wslAxes.GetOrigin() + 10000*wslAxes.GetXAxis();
	IwPoint3d negFarPoint = wslAxes.GetOrigin() - 10000*wslAxes.GetXAxis();

	IwPoint3d closestPoint;
	double param;
	
	double posDist = DistFromPointToCurve(posFarPoint, m_outlineProfileXYZCurve, closestPoint, param);
	if ( posPoint )
		*posPoint = closestPoint;

	double negDist = DistFromPointToCurve(negFarPoint, m_outlineProfileXYZCurve, closestPoint, param);
	if ( negPoint )
		*negPoint = closestPoint;

	MLWidth = 20000 - posDist - negDist;

	return MLWidth;
}

double COutlineProfile::GetEstimatedImplantMLSize() 
{
	if ( m_estimatedImplantMLSize != 0 )
		return m_estimatedImplantMLSize;
	else
	{
		m_estimatedImplantMLSize = GetImplantMLSize();
		return m_estimatedImplantMLSize;
	}
}

IwExtent2d COutlineProfile::GetSketchSurfaceUVDomain()
{
	IwExtent2d uvDomain(0,0,0,0);
	if ( m_sketchSurface )
		uvDomain = m_sketchSurface->GetNaturalUVDomain();

	return uvDomain;
}

void COutlineProfile::SetEstimatedNotchOutlineProfileFeatures
(
	IwTArray<IwBSplineCurve*>& curves, 
	IwTArray<IwPoint3d>& points
) 
{
	m_estimatedNotchOutlineProfileCurves.RemoveAll();
	m_estimatedNotchOutlineProfileCurves.Append(curves);
	m_estimatedNotchOutlineProfileFeaturePoints.RemoveAll();
	m_estimatedNotchOutlineProfileFeaturePoints.Append(points);
}

void COutlineProfile::GetEstimatedNotchOutlineProfileFeatures
(
	IwTArray<IwBSplineCurve*>& curves, 
	IwTArray<IwPoint3d>& points
) 
{
	curves.RemoveAll(); 
	points.RemoveAll();

	//if ( m_estimatedNotchOutlineProfileFeaturePoints.GetSize() == 0 )
	//	CDefineOutlineProfile::InitializeNotchFeatures(GetTotalDoc());

	curves.Append(m_estimatedNotchOutlineProfileCurves);
	points.Append(m_estimatedNotchOutlineProfileFeaturePoints);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// This function returns the center of the condylar profile around the posterior 1st/2nd cut edge. 
bool COutlineProfile::GetCondylarProfileCenters
(
	IwPoint3d& posCenter,	// O: the profile center in positive condyle
	IwPoint3d& negCenter	// O: the profile center in negative condyle
)
{
	if ( m_outlineProfileXYZCurve == NULL )
		return false;

	IwBSplineSurface* sketchSurface = NULL;
	bool gotSketchSurf = GetSketchSurface(sketchSurface);
	if ( !gotSketchSurf )
		return false;

	if ( GetTotalDoc()->GetFemoralCuts() == NULL )
		return false;

	IwTArray<IwPoint3d> cutFeaturePoints;
	GetTotalDoc()->GetFemoralCuts()->GetFeaturePoints(cutFeaturePoints, NULL);

	if (cutFeaturePoints.GetSize() != 21) return false;

	IwPoint3d pnt2, pnt3, pnt4, pnt5, pnt6, pnt7, pnt8, pnt9;
	IwPoint2d param2d;
	DistFromPointToSurface(cutFeaturePoints.GetAt(2), sketchSurface, pnt2, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(3), sketchSurface, pnt3, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(4), sketchSurface, pnt4, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(5), sketchSurface, pnt5, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(6), sketchSurface, pnt6, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(7), sketchSurface, pnt7, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(8), sketchSurface, pnt8, param2d);
	DistFromPointToSurface(cutFeaturePoints.GetAt(9), sketchSurface, pnt9, param2d);

	IwVector3d vec = pnt7 - pnt6;
	vec.Unitize();

	IwPoint3d pnt22, pnt33, pnt44, pnt55, pnt66, pnt77, pnt88, pnt99;
	IwVector3d intTangVec;
	IwExtent1d dom = m_outlineProfileXYZCurve->GetNaturalInterval();
	double param;
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt2, vec, NULL, 10.0, pnt22, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt3, vec, NULL, 10.0, pnt33, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt4, vec, NULL, 10.0, pnt44, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt5, vec, NULL, 10.0, pnt55, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt6, vec, NULL, 10.0, pnt66, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt7, vec, NULL, 10.0, pnt77, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt8, vec, NULL, 10.0, pnt88, param, intTangVec);
	IntersectCurveByVector(m_outlineProfileXYZCurve, dom, pnt9, vec, NULL, 10.0, pnt99, param, intTangVec);
if (0)
{
	ShowPoint(m_pDoc, pnt22, red);
	ShowPoint(m_pDoc, pnt33, magenta);
	ShowPoint(m_pDoc, pnt44, blue);
	ShowPoint(m_pDoc, pnt55, cyan);
	ShowPoint(m_pDoc, pnt66, red);
	ShowPoint(m_pDoc, pnt77, magenta);
	ShowPoint(m_pDoc, pnt88, blue);
	ShowPoint(m_pDoc, pnt99, cyan);
}
	// For CR, and the backup for PS
	posCenter = 0.5*(pnt66 + pnt77); 
	negCenter = 0.5*(pnt88 + pnt99);

	if ( GetTotalDoc()->isPosteriorStabilized() ) // for PS
	{
		IwPoint3d posRefPnt, negRefPnt;
		bool bHasRef = GetBoxCutReferencePoints(posRefPnt, negRefPnt);
		if ( bHasRef )
		{
			posCenter = 0.5*pnt22 + 0.5*posRefPnt; 
			negCenter = 0.5*pnt55 + 0.5*negRefPnt; 
		}
	}


	return true;
}

///////////////////////////////////////////////////////////////////////////////////
// This function checks the temporary non remapable points and return their indexes.
///////////////////////////////////////////////////////////////////////////////////
bool COutlineProfile::GetNonRemapablePointNowIndexes
(
	IwTArray<int>& indexes	// O: 
)
{
	bool nonRemapableExist = false;
	indexes.RemoveAll();

	int statusInfo, remapZValue0, remapZValue2;
	for (unsigned i=0; i<m_outlineProfileFeaturePointsRemap.GetSize(); i++)
	{
		statusInfo = m_outlineProfileFeaturePointsInfo.GetAt(i);
		remapZValue0 = (int)m_outlineProfileFeaturePointsRemap.GetAt(i).z;
		remapZValue2 = (int)(100*(m_outlineProfileFeaturePointsRemap.GetAt(i).z-remapZValue0));
		if ( statusInfo == 0 )// a interpolation point
		{
			if ( remapZValue0 == -1 && remapZValue2 != 0) // a temporary non remap-able point now, but can be converted into remap-able point
			{
				indexes.Add(i);
			}
		}
	}

	if ( indexes.GetSize() > 0 )
		nonRemapableExist = true;

	return nonRemapableExist;
}

bool COutlineProfile::MoveAnteriorProfile
(
	IwPoint3d earIndentation // I: new ear indentation
)
{
	IwBSplineSurface* sketchSurf=NULL;
	GetSketchSurface(sketchSurf);
	if ( sketchSurf == NULL )
		return false;

	// 
	IwTArray<IwPoint3d> featurePoints, featurePointsRemap;
	IwTArray<int> featurePointsInfo;
	GetOutlineProfileFeaturePoints(featurePoints, featurePointsInfo, featurePointsRemap);
	unsigned nSize = featurePoints.GetSize();
	if ( nSize == 0 )
		return false;

	// Convert earIndentation into uv domain
	IwPoint3d cPnt, pnt;
	IwPoint2d param2d;
	DistFromPointToSurface(earIndentation, sketchSurf, cPnt, param2d);
	IwPoint3d earIndenetationXY = IwPoint3d(param2d.x, param2d.y, 0);
	// Determine delta movement
	IwPoint3d oldEarIndentation = featurePoints.GetAt(0);
	IwVector3d moveVecXY = earIndenetationXY - oldEarIndentation;
	// only move along AP direction.
	IwVector3d moveVecY = IwVector3d(0, moveVecXY.y, 0); 

	IwPoint3d newEarIndenetation = oldEarIndentation + moveVecY;
	// Move anterior ear shape
	featurePoints.SetAt(0, newEarIndenetation);
	// positive side ear shape 
	if ( featurePointsInfo.GetAt(1) == 0 ) // interpolation point
	{
		pnt = featurePoints.GetAt(1);
		featurePoints.SetAt(1, pnt + moveVecY);
	}
	else // arc
	{
		pnt = featurePoints.GetAt(1); // arc starting point
		featurePoints.SetAt(1, pnt + moveVecY);
		pnt = featurePoints.GetAt(2); // arc center point
		featurePoints.SetAt(2, pnt + moveVecY);
		pnt = featurePoints.GetAt(3); // arc ending point
		featurePoints.SetAt(3, pnt + moveVecY);
	}
	// negative side ear shape 
	if ( featurePointsInfo.GetAt(nSize-2) == 0 ) // interpolation point
	{
		pnt = featurePoints.GetAt(nSize-2);
		featurePoints.SetAt(nSize-2, pnt + moveVecY);
	}
	else // arc
	{
		pnt = featurePoints.GetAt(nSize-2); // arc ending point
		featurePoints.SetAt(nSize-2, pnt + moveVecY);
		pnt = featurePoints.GetAt(nSize-3); // arc center point
		featurePoints.SetAt(nSize-3, pnt + moveVecY);
		pnt = featurePoints.GetAt(nSize-4); // arc starting point
		featurePoints.SetAt(nSize-4, pnt + moveVecY);
	}
	//
	SetOutlineProfileFeaturePoints(featurePoints, featurePointsInfo, featurePointsRemap);

	return true;
}

bool COutlineProfile::GetBoxCutReferencePoints
(
	IwPoint3d& posRefPnt,	// O: Box cut outer wall ref point;
	IwPoint3d& negRefPnt	// O: Box cut outer wall ref point;
)
{
	if ( !m_posBoxCutRefPoint.IsInitialized() || !m_negBoxCutRefPoint.IsInitialized() )
		return false;

	posRefPnt = m_posBoxCutRefPoint;
	negRefPnt = m_negBoxCutRefPoint;

	return true;
}

bool COutlineProfile::IsAsymmetricEarShaped(CTotalDoc* pDoc) 
{
	// Here we use the "OUTLINE PROFILE MEDIAL ANTERIOR PROFILE STATUS" to define is symmetric (=-1)
	// or asymmetric (>-1) anterior ear shape.
	return pDoc->GetValidateTableStatus("OUTLINE PROFILE MEDIAL ANTERIOR PROFILE STATUS")>-1;
}