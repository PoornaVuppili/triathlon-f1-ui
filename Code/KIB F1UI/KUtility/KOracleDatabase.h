#pragma once

#include <qstring.h>

class KOracleDatabase
{
public:
    KOracleDatabase();
    ~KOracleDatabase();

    // Set connection parameters. 
    //  name (I) : database name
    //  host (I): ........ host name
    //  port (I): ........ port number
    //  user (I): ........ user name
    //  password (I): .... password
    // Return:
    //  true : the parameters are set successfully.
    //  false: there is no valid driver and the parameters cannot be set.
    bool SetParameters(QString& name, QString& host, int port, QString& user, QString& password);

    // Test connection
    // Return: true - successful, false - failed
    bool TestConnection();

    // Query based on the implant number. The function returns an implant description string, country code,
    // catalog number and its revision, kit ED number and its revision.
    // From the description string, the implant type, left/right knee, medial/lateral information can be extracted.
    //   
    // Parameters:
    //  implantNum          (I) : serial number
    //  desc                (O) : implant description 
    //  country             (O) : country code
	//  lastName,firstName  (O) : name (optional)
    // Return:
    //  true - successful; false: failed.
    bool Query(const QString& implantNum, QString& desc, QString& country, QString* lastName=NULL, QString* firstName=NULL);
};

