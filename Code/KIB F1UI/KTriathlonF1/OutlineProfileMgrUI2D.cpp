#include "OutlineProfileMgrUI2D.h"
#include "OutlineProfile.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "UndoRedoCmd.h"
//#include "InnerSurface.h"
#include "ValidateOutlineProfile.h"
#include "OutlineProfileMgrModel.h"

#include <QApplication>

using namespace Validation::OutlineProfile;

OutlineProfileMgrUI2D::OutlineProfileMgrUI2D(CTotalDoc* doc, CManagerActivateType manActType, CManager *owner, OutlineProfileMgrModel *model )
	: OutlineProfileMgrUIBase(doc, manActType, owner, model),
	m_glOutlineProfileFlatList(0),
	m_glRulerGLlist(0)
{
	m_pDoc->AppendLog("OutlineProfileMgrUI2D, use 2D outline profile.");

	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT );
	//////////////////////////////////////////////////////////////////////////////////
	///////////////////ACTIONS taken in the base class
	//Before any computation can happen there needs to be an updated SketchSurface
	//That update happens in the base class
	//
	//If the activation type is INITIALIZE
	//mgrModel (aka the geometry to work on) is initialized in the base class
	//
	//If the activation type is RESET or outlineProfile object does exist
	//	then the outlineProfile Entity is created and the reset action 
	//	happens in the base class
	////ATTENTION: If the actions in the base class change, please update this comment
	//////////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////
	//After the base class prepares the data
	// the following happens
	if ( activateUI )
	{
		m_pDoc->StartTimeLog("DefineOutlineProfile2D");
		DetermineNotchEighteenWidthPoints();
	}

	// Get the runtime data
	if( manActType == MAN_ACT_TYPE_EDIT ||
		manActType == MAN_ACT_TYPE_REFINE ||
		manActType == MAN_ACT_TYPE_REFINE_CONVERGE ) 
	{
		// convert feature points from UV domain to xyz space
		SetMouseMoveSearchingList();

		if ( manActType == MAN_ACT_TYPE_REFINE )
			Refine();

		//OnValidate();
	}

	if ( m_pDoc->GetOutlineProfile() )
		m_pDoc->GetOutlineProfile()->SetEver2DEditing();
}

OutlineProfileMgrUI2D::~OutlineProfileMgrUI2D()
{
	if(m_manActType == MAN_ACT_TYPE_EDIT)
	{
		//m_pDoc->GetInnerSurface()->Enable2DView(false);
		m_pDoc->GetOutlineProfile()->Set2DEditing(false);
	}

	DeleteGLlist(m_glOutlineProfileFlatList);
	DeleteGLlist(m_glRulerGLlist);
}

void OutlineProfileMgrUI2D::SetMouseMoveSearchingList()
{
	IwTArray<IwPoint3d> xyzPoints;
	m_pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(const_cast<IwTArray<IwVector3d>&>(m_mgrModel->FeaturePoints()), xyzPoints);
	xyzPoints.RemoveLast();// Note, the last one is never selectable.
	ConvertSketchSurfaceTo2D(xyzPoints);
	ConvertSketchSurfaceTo2D(m_mgrModel->NotchEighteenWidthPoints());
	ConvertSketchSurfaceTo2D(m_mgrModel->ValidateUnQualifiedLocations());
	m_owner->SetMouseMoveSearchingList(xyzPoints);
}

bool OutlineProfileMgrUI2D::MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	IwPoint3d caughtPoint;
	int caughtIndex;

	if(m_owner->GetMouseMoveCaughtPoint(caughtPoint, caughtIndex))
		DeleteGLlist(m_glOutlineProfileFlatList);

	return OutlineProfileMgrUIBase::MouseLeft(cursor, keyModifier, vp);
}

bool OutlineProfileMgrUI2D::MouseUp    ( const QPoint& cursor, Viewport* vp)
{
	if ( m_leftButtonDown )
	{
		int caughtIndex;
		IwPoint3d caughtPoint;
		bool gotCaughtPoint = m_owner->GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);

		//// Update the mouse move caught point /////////////////
		if ( m_addDeleteMode == 0 && gotCaughtPoint)
		{
			m_pDoc->AppendLog( QString("OutlineProfileMgrUI2D::MouseUp(), m_addDeleteMode = move, caughtIndex: %1, caughtPoint: %2 %3 %4").arg(caughtIndex).arg(caughtPoint.x).arg(caughtPoint.y).arg(caughtPoint.z) );

			// if the clicked-down is the same as the button-up point, do nothing
			// **** NOTE **** DO NOT CHANGE THIS CONDITION UNLESS YOU SEE THE MouseDoubleClickLeft().
			if (m_owner->GetPositionStart() == cursor)
			{
				m_leftButtonDown = false;
				return false;
			}

			IwPoint2d surfParam;
			Convert2DToSketchSurface(caughtPoint, surfParam);
			MovePoint(caughtIndex, surfParam);

			DeleteGLlist(m_glOutlineProfileFlatList);
		}
		//// Update the adding point /////////////////
		else if (m_addDeleteMode == 1)
		{
			m_pDoc->AppendLog( QString("OutlineProfileMgrUI2D::MouseUp(), m_addDeleteMode = add, cursor: %1 %2").arg(cursor.x()).arg(cursor.y()) );

			IwPoint2d surfParam;
			IwPoint3d curPnt;
			m_owner->GetView()->UVtoXYZ(cursor, curPnt);

			Convert2DIntersectionPtToSketchSurface(curPnt, m_owner->GetView()->GetViewingVector(), surfParam);

			if(AddPoint(surfParam))
				DeleteGLlist(m_glOutlineProfileFlatList);
		}
		//// Update the deleting point /////////////////
		else if (m_addDeleteMode == 2)
		{
			m_pDoc->AppendLog( QString("OutlineProfileMgrUI2D::MouseUp(), m_addDeleteMode = delete, cursor: %1 %2").arg(cursor.x()).arg(cursor.y()) );

			DeletePoint(caughtIndex);
		}
	}

	return OutlineProfileMgrUIBase::MouseUp(cursor, vp);
}

bool OutlineProfileMgrUI2D::MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp)
{
	IwPoint3d caughtPoint;
	int caughtIndex;

	if(m_owner->GetMouseMoveCaughtPoint(caughtPoint, caughtIndex))
		DeleteGLlist(m_glOutlineProfileFlatList);

	return OutlineProfileMgrUIBase::MouseDoubleClickLeft(cursor, vp);
}

void OutlineProfileMgrUI2D::Display(Viewport* vp)
{
	glDisable(GL_BLEND);
	glDisable( GL_LIGHTING );
	glDepthMask(GL_TRUE);
	glDisable(GL_DEPTH_TEST);

	if( m_glOutlineProfileFlatList == 0 )
	{
		m_glOutlineProfileFlatList = glGenLists(1);
		glNewList( m_glOutlineProfileFlatList, GL_COMPILE_AND_EXECUTE );
		Display2DOutline();
		DisplayOutlineFeaturePoints();
		glEndList();
	}
	else
	{
		glCallList( m_glOutlineProfileFlatList );
	}

	if(glIsList(m_glRulerGLlist))
		glCallList(m_glRulerGLlist);

	glDisable(GL_DEPTH_TEST);

	IwVector3d viewVec = m_owner->GetView()->GetViewingVector();

	if (m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( m_owner->GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			glPointSize(7);
			glColor3ub( 128, 0, 128);

			m_owner->DisplayCaughtPoint(-50*viewVec);

			glPointSize(1);
		}
	}
	else
	{
		glPointSize(6);
		glColor3ub( 255, 0, 255);

		m_owner->DisplayCaughtPoint(-50*viewVec);
		glPointSize(1);
	}

	OutlineProfileMgrUIBase::Display(vp);
}

void OutlineProfileMgrUI2D::Display2DOutline()
{
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile)
	{
		glLineWidth(3.0);
		glColor3ub( 128, 255, 0);
		glBegin( GL_LINE_STRIP );
			for(int i = 0; i < outlineProfile->Get2DDisplayPoints().GetSize(); i++ ) 
				glVertex3d(	outlineProfile->Get2DDisplayPoints().GetAt(i).x, 
							outlineProfile->Get2DDisplayPoints().GetAt(i).y, 
							outlineProfile->Get2DDisplayPoints().GetAt(i).z );
		glEnd();
		glLineWidth(1.0);
	}
}

void OutlineProfileMgrUI2D::DisplayOutlineFeaturePoints()
{
	IwBSplineSurface* sketchSurface = NULL;
	m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);

	IwPoint3d pnt;

	glPointSize(6);

	for (unsigned i=0; i<m_mgrModel->FeaturePointsInfo().GetSize(); i++)
	{
		sketchSurface->EvaluatePoint(IwVector2d(m_mgrModel->FeaturePoints().GetAt(i).x, m_mgrModel->FeaturePoints().GetAt(i).y), pnt);

		if (i==0 || i== (m_mgrModel->FeaturePointsInfo().GetSize()-1)) // seam point
		{
			glColor3ub(0, 0, 0);
			glPointSize(8);
		}
		else if (m_mgrModel->FeaturePointsInfo().GetAt(i) == 0) // interpolation points
		{
			// remap z value (,,z). if z=-1 -> pure non remap-able point. if z=-1.xxxx (xxxx=some values), non remap-able point now, but can be converted into a remap-able point.
			IwPoint3d remap = m_mgrModel->FeaturePointsRemap().GetAt(i);
			int zValue0 = (int)remap.z;
			int zValue2 = (int)(100*(remap.z-zValue0));
			if ( zValue0 == -1 ) // non remap-able points
			{
				if ( zValue2 == 0 ) // pure non remap-able point
				{
					glColor3ub(98, 98, 255);
					glPointSize(6);
				}
				else  // non remap-able point now, but can be converted into remap-able point.
				{
					glColor3ub(152, 0, 255);
					glPointSize(6);
				}
			}
			else // remap-able points (cut edge points & edge middle points)
			{
				glColor3ub(0, 0, 128);
				glPointSize(6);
			}
		}
		else if (m_mgrModel->FeaturePointsInfo().GetAt(i) == 1 || // arc center points
				 m_mgrModel->FeaturePointsInfo().GetAt(i) == 2)
		{
			glColor3ub( 98, 0, 255 );
			glPointSize(6);
		}
		else if (m_mgrModel->FeaturePointsInfo().GetAt(i) == 3 || // arc start points
				 m_mgrModel->FeaturePointsInfo().GetAt(i) == 4)	    // arc end points
		{
			glColor3ub( 128, 0, 255 );
			glPointSize(6);
		}

		ConvertSketchSurfaceTo2D(pnt);

		glBegin( GL_POINTS );
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}

	///////// For validation
	if ( !m_pDoc->DisplayValidationData() )
		return;

	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile)
	{
		glPointSize(5);
			for (unsigned i=0; i<outlineProfile->GetFeaturePointsValidation().GetSize(); i++)
			{
				sketchSurface->EvaluatePoint(IwVector2d(outlineProfile->GetFeaturePointsValidation().GetAt(i).x, outlineProfile->GetFeaturePointsValidation().GetAt(i).y), pnt);
				ConvertSketchSurfaceTo2D(pnt);
				glColor3ub( 0, 64, 0 );
				glBegin( GL_POINTS );
					glVertex3d( pnt.x, pnt.y, pnt.z );
				glEnd();
			}
		glPointSize(1);
	}
}

void OutlineProfileMgrUI2D::OnDispRuler()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUI2D::OnDispRuler()") );

	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();

	QApplication::setOverrideCursor( Qt::WaitCursor );

	OutlineProfileMgrUIBase::OnDispRuler();

	if(!m_displayRuler)
	{
		DeleteGLlist(m_glRulerGLlist);
		QApplication::restoreOverrideCursor();
		return;
	}

	IwTArray<IwCurve*> curves[2];

	outlineProfile->GetRulerCurves(curves[0], curves[1]);

	m_glRulerGLlist = glGenLists(1);
	glNewList(m_glRulerGLlist, GL_COMPILE);

	for(int k=0; k<2; k++)
	{
		for(int i=0; i<curves[k].GetSize(); i++)
		{
			IwExtent1d extent = curves[k][i]->GetNaturalInterval();
			IwTArray<IwPoint3d> pnts;
			curves[k][i]->Tessellate(extent, 0.75, 8, 6, NULL, &pnts);

			ConvertSketchSurfaceTo2D(pnts);

			glLineWidth(1.0);
			if ( (i-5)%10 == 0 )
				glLineWidth(2.0);

			if (i%5 == 0)
				glColor4ub( black[0], black[1], black[2], 128);
			else
				glColor4ub( darkGray[0], darkGray[1], darkGray[2], m_pDoc->GetTransparencyFactor());

			glBegin(GL_LINE_STRIP);
				for(int j=0; j<pnts.GetSize(); j++)
					if(pnts[j].IsInitialized())
						glVertex3f(pnts[j].x, pnts[j].y, pnts[j].z);
			glEnd();
		}
	}
	glEndList();

	m_owner->GetView()->Redraw();

	QApplication::restoreOverrideCursor();
}

void OutlineProfileMgrUI2D::SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData)
{
	OutlineProfileMgrUIBase::SetDefineOutlineProfileUndoData(undoData);

	// Also set the m_currFeaturePoints to the SetMouseMoveSearchingList() for mouse searching;
	// convert feature points from UV domain to xyz space
	COutlineProfile*		outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile)
	{
		// We need to call SetOutlineProfileFeaturePoints() and GetOutlineProfileFeaturePoints()
		// such that all the m_currFeaturePoints are in the correct positions, especially for
		// the arc end points.
		//The SetOutlineProfileFeaturePoints()
		//Happens in the base class
		m_mgrModel->GetOutlineProfileFeaturePoints();
		IwTArray<IwPoint3d> xyzPoints;
		outlineProfile->ConvertSketchSurfaceUVtoXYZ(const_cast<IwTArray<IwVector3d>&>(m_mgrModel->FeaturePoints()), xyzPoints);
		xyzPoints.RemoveLast();// Note, the last one is never selectable.
		ConvertSketchSurfaceTo2D(xyzPoints);
		ConvertSketchSurfaceTo2D(m_mgrModel->NotchEighteenWidthPoints());
		ConvertSketchSurfaceTo2D(m_mgrModel->ValidateUnQualifiedLocations());
		m_owner->SetMouseMoveSearchingList(xyzPoints);

		DeleteGLlist(m_glOutlineProfileFlatList);
	}
	m_owner->GetView()->Redraw();
}

void OutlineProfileMgrUI2D::DeleteGLlist(long &list)
{
	if( glIsList(list) )
		glDeleteLists(list, 1);

	list = 0;
}

void OutlineProfileMgrUI2D::DetermineNotchEighteenWidthPoints()
{
	OutlineProfileMgrUIBase::DetermineNotchEighteenWidthPoints();

	ConvertSketchSurfaceTo2D(m_mgrModel->NotchEighteenWidthPoints());
}

void OutlineProfileMgrUI2D::ConvertSketchSurfaceTo2D(IwTArray<IwPoint3d> &arr)
{
	for(int i=0; i<arr.GetSize(); i++)
		ConvertSketchSurfaceTo2D(arr[i]);
}

void OutlineProfileMgrUI2D::ConvertSketchSurfaceTo2D(IwPoint3d &pt)
{
	if(!pt.IsInitialized())
		return;

	IwBSplineSurface* sketchSurface = NULL;
	m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);

	return;

	IwPoint3d onSurf;
	IwPoint2d surfParam;
	IwVector3d nm;
	IwPoint3d ptRaw;
	IwFace* intFace = NULL;

	DistFromPointToSurface(pt, sketchSurface, onSurf, surfParam);
	sketchSurface->EvaluateNormal(surfParam, FALSE, FALSE, nm);
	//IntersectBrepByLine(innerBrep, onSurf, nm, ptRaw, &intFace);
	//pt = pInnerSurface->Get3DTo2D(intFace, ptRaw);
}

void OutlineProfileMgrUI2D::Convert2DIntersectionPtToSketchSurface
	(	const IwPoint3d &anchor,  // I: the point of the ray
		const IwVector3d &vec,	  // I: ray direction
		IwPoint2d &uv			  // O: UV coordinates on the sketch surface
	)
{
	
}

void OutlineProfileMgrUI2D::Convert2DToSketchSurface(IwTArray<IwPoint3d> &arr)
{
	for(int i=0; i<arr.GetSize(); i++)
		Convert2DToSketchSurface(arr[i], IwPoint2d());
}

void OutlineProfileMgrUI2D::Convert2DToSketchSurface(IwPoint3d &pt, IwPoint2d &uv)
{

		return;

	IwBSplineSurface* sketchSurface = NULL;
	m_pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
	if(sketchSurface == NULL)
		return;

	//pt = pInnerSurface->Get2DTo3D(pt);
	DistFromPointToSurface(pt, sketchSurface, pt, uv);
}

void OutlineProfileMgrUI2D::UpdateSketchSurface()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	OutlineProfileMgrUIBase::UpdateSketchSurface();

	QApplication::restoreOverrideCursor();
}

void OutlineProfileMgrUI2D::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUI2D::Reset()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	OutlineProfileMgrUIBase::Reset();

	// update search list
	IwTArray<IwPoint3d> xyzPoints;
	m_pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(const_cast<IwTArray<IwVector3d>&>(m_mgrModel->FeaturePoints()), xyzPoints);
	xyzPoints.RemoveLast();// Note, the last one is never selectable.
	
	ConvertSketchSurfaceTo2D(xyzPoints);
	ConvertSketchSurfaceTo2D(m_mgrModel->NotchEighteenWidthPoints());
	ConvertSketchSurfaceTo2D(m_mgrModel->ValidateUnQualifiedLocations());
	m_owner->SetMouseMoveSearchingList(xyzPoints);

	QApplication::restoreOverrideCursor();
}

void OutlineProfileMgrUI2D::OnValidate()
{
	OutlineProfileMgrUIBase::OnValidate();
	ConvertSketchSurfaceTo2D(m_mgrModel->ValidateUnQualifiedLocations());
}