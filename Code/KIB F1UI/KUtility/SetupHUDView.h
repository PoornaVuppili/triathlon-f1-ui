#pragma once

#include "TRigid3.h"
#include <IwVector3d.h>

class IwAxis2Placement;
class IwExtent3d;
class Viewport;

//This class is used to setup a Head Up Display style view in the upper right corner of the 3D viewport

class SetupHUDView
{
public:
	SetupHUDView(int width, int height, Viewport *vp, IwAxis2Placement *axes, IwExtent3d *viewBox, bool defaultRotation=true);
	~SetupHUDView();
											//zoomIn zoomRatio < 1, zoomOut zoomRation >1
	void Zoom(int viewX, int viewY, float zoomRatio);
	//void ZoomIn(int viewX, int viewY, float zoomRatio = .98f);
	//void ZoomOut(int viewX, int viewY, float zoomRatio = 1.02f);

	void ApplyView();
	void RestoreView();

	IwPoint3d GetProjectedPointToViewport(IwPoint3d pt);

private:
	void AdjustAspectRatio();

private:

	int minX, maxX, minY, maxY;
	int w, h;

	float left, right, bottom, top, ne, fa;

	Rigid3Float transform;
	Viewport*	_vp;
};