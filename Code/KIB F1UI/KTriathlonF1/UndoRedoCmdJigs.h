#pragma once

#include <QtGui>
#include <QUndoStack>
#include <QUndoCommand>
#include "DefineCartilage.h"
#include "DefineOutlineProfileJigs.h"
#include "MakeOuterSurfaceJigs.h"
#include "DefineStylusJigs.h"
#include "..\KAppTotal\Manager.h"

class DefineCartilageCmd : public QUndoCommand
{
	public:
		DefineCartilageCmd(CManager* manager, 
		CDefineCartilageUndoData &prevUndoData, 
		CDefineCartilageUndoData &postUndoData, 
		QUndoCommand *parent = 0);
    void undo();
    void redo();

private:
	CManager*			m_pManager;
	CDefineCartilageUndoData	m_prevUndoData;
	CDefineCartilageUndoData	m_postUndoData;	
};

class DefineOutlineProfileJigsCmd : public QUndoCommand
{
	public:
		DefineOutlineProfileJigsCmd(CManager* manager, 
		CDefineOutlineProfileJigsUndoData &prevUndoData, 
		CDefineOutlineProfileJigsUndoData &postUndoData, 
		QUndoCommand *parent = 0);
    void undo();
    void redo();

private:
	CManager*						m_pManager;
	CDefineOutlineProfileJigsUndoData	m_prevUndoData;
	CDefineOutlineProfileJigsUndoData	m_postUndoData;
	
};

class MakeOuterSurfaceJigsCmd : public QUndoCommand
{
	public:
		MakeOuterSurfaceJigsCmd(CManager* manager, 
		CMakeOuterSurfaceJigsUndoData &prevUndoData, 
		CMakeOuterSurfaceJigsUndoData &postUndoData, 
		QUndoCommand *parent = 0);
    void undo();
    void redo();

private:
	CManager*			m_pManager;
	CMakeOuterSurfaceJigsUndoData	m_prevUndoData;
	CMakeOuterSurfaceJigsUndoData	m_postUndoData;	
};

class DefineStylusJigsCmd : public QUndoCommand
{
	public:
		DefineStylusJigsCmd(CManager* manager, 
		CDefineStylusJigsUndoData &prevUndoData, 
		CDefineStylusJigsUndoData &postUndoData, 
		QUndoCommand *parent = 0);
    void undo();
    void redo();

private:
	CManager*						m_pManager;
	CDefineStylusJigsUndoData		m_prevUndoData;
	CDefineStylusJigsUndoData		m_postUndoData;
	
};