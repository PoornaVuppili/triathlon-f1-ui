#pragma once

#include "Valgebra.h"

class IwAxis2Placement;

class CBounds3d
	{
public:
	bool			bInitiated;		// Initialization flag
	CPoint3d		pt0;			// Lower left corner
	CPoint3d		pt1;			// Upper right corner

	// Default constructor
	CBounds3d();

	// Copy constructor
	CBounds3d( 
		const CBounds3d& bounds		// Other box
		);

	// Destructor
	~CBounds3d();

	// Reset to uninitialized state
	void   Reset(); 

	// Assignment operator
	CBounds3d&   operator = (		
		const CBounds3d& bounds		// Other box
		);

	// Expand by point
	void   Expand( 
		const CPoint3d& pt			// Point to be added to the box
		);

	// Expand by other box
	void   Expand( 
		const CBounds3d& bounds		// Other box to be added to the cuurent one
		);

	// Expand by value
	void   Expand( 
		double delta
		);

	// Check if the current box intersects other box
	bool   Intersects( 
		const CBounds3d& bounds		// Other box to check for intersection with the current one
		) const;

	// Check if the current box contains a given point
	bool	Contains( const CPoint3d& pt );

	// Check if the current box contains a given point
	bool   Contains( 
		const	CPoint3d& pt,		// The tested point
		int		viewPlane,			// Index of coord plane in which the test should be performed
		bool	bCheckIn2d			// true means test only in 2d-plane, given by viewPlane param.
		);	
	};



class CBounds2d
	{
private:
	bool			bInitiated;		// Intialization flag

public:
	CPoint2d		pt0;			// Lower left corner	
	CPoint2d		pt1;			// Upper right corner


	// Default constructor
	CBounds2d();

	
	// Constructor by corners coordinates
	CBounds2d ( 
		double x0,					// x of lower left corner
		double y0,					// y of lower left corner
		double x1,					// x of upper right corner
		double y1					// y of upper right corner
		);

	
	// Copy constructor
	CBounds2d ( 
		const CBounds2d& bounds		// other CBounds2d
		);

	
	// Destructor
	~CBounds2d();
	
	
	// Assignment operator
	CBounds2d&   operator = ( 
		const CBounds2d& bounds		// other CBounds2d
		);

	
	// Reset to uninitiated state
	void   Reset(); 

	
	// Set to given coordinates
	CBounds2d&   Resize ( 
		double x0,					// x of lower left corner
		double y0,					// y of lower left corner
		double x1,					// x of upper right corner
		double y1					// y of upper right corner
		);

	
	// Expand by point
	void   Expand ( 
		const CPoint2d& pt			// Point to add to the box
		);

	
	// Expand by other box
	void   Expand ( 
		const CBounds2d& bounds		// An other bounds to add to the box
		);

	// Extend in both directions by epsX and epsY
	void Extend( 
		double epsX,
		double epsY
		);
	
	// Check intersection with another box
	bool   Intersects ( 
		const CBounds2d& bounds		// An other bounds to check intersection with
		) const;

	
	// Check if the current box contains a given point
	bool   Contains ( 
		const CPoint2d& pt			// A point to check if it is contained in the current box
		);
	};

//**************************************************************

struct CCoordSys
	{
	CPoint3d	origin;
	CVector3d	x_vec;
	CVector3d	y_vec;
	CVector3d	z_vec;
	};


class CTransform
	{
private:
	CMatrix			matrix;			// Rotation matrix
	CVector3d		shift;			// Translation vector

public:
	// Default constructor
	CTransform();

	// Constructor by rotation matrix and shift vector
	CTransform( const CMatrix& _matrix, const CVector3d& _shift );
	
	// Constructor by real matrix
	CTransform( double matr[3][4] );

	// Destructor
	~CTransform();

	
	// Applying transformation to a point
	inline CPoint3d   operator * ( 
		const CPoint3d& pt			// A point to be transformed
		) const;

	
	// Applying transformation to a vector
	inline CVector3d   operator * ( 
		const CVector3d& pt			// A vector to be transformed
		) const;

	
	// Composing (multiplication) two transformations;
	// First apply the argument, then this transformation
	CTransform   operator * ( 
		const CTransform& trf		// Parameter trf will be applied first;
									// then the current transformation.
		) const;

	
	// Follow the current trf by translation
	void   Translate( 
		double x,					// x coord of translation vector
		double y, 					// y coord of translation vector
		double z 					// z coord of translation vector
		);

	// Follow the current trf by translation
	void   Translate( 
		const CVector3d& vec		// After the current transform is applied
									// translate by this vector
		);

	// Follow the current trf by scaling
	void   Scale( 
		double factor				// After the current transform is applied
									// scale by this factor
		);

	void	ScaleMatrix(
		double factor
		);


	// Follow the current trf by rotation
	void   Rotate( 
		double angle,				// Rotation angle in degrees
		double x,					// x coord of the rotation axis
		double y,					// y coord of the rotation axis
		double z					// z coord of the rotation axis
		);

	// Follow the current trf by rotation
	void   Rotate( 
		double angle,				// Rotation angle in degrees
		const CVector3d& vec		// Vector of rotation axis
		);

	// Update shift of Anisotropic Scaling Trf so that
	// the scaling is applied with respect to given center
	void	SetCenter( 
		CPoint3d center				// The center of scaling
		);

	// Mirror with respect to coord plane 
	void	Mirror( 
		int idx
		);

	bool IsMirrored();

	// Make 4x4 (homogeneous coord) matrix for OpenGL
	void   MakeGLMatrix( 
		double glMatrix[16]			// Memory for OpenGL matrix; should be allocated by the caller
		) const;


	// Sets transform by real 3x4 matrix
	void   SetTransform( 
		double matr[3][4]			// Transformation matrix
		);


	// Gets 3x3 rotation matrix
	void   GetRotationMatrix( 
		CMatrix& matr			// Rotation matrix
		);


	// Get reference to translation vector
	CVector3d&		GetShiftVector();

	void			SetRotationMatrix( CMatrix& matr );
	void			SetShiftVector( CVector3d& vec );

	static CTransform	IwAxis2Placement2CTransform(IwAxis2Placement const& iwxform);

	// Computes inverse transformation
	bool ComputeInverseTrf( 
		CTransform& InverseTrf
		);

	bool	operator == ( 
		const CTransform& trf
		) const;

	bool	operator != ( 
		const CTransform& trf
		) const;
	};


CPoint3d CTransform::operator * ( const CPoint3d& pt ) const
	{
	return matrix * pt + shift;
	}


CVector3d CTransform::operator * ( const CVector3d& pt ) const
	{
	return matrix * pt;
	}
