#ifndef VIEWPORTCONTROLDIALOG_H
#define VIEWPORTCONTROLDIALOG_H

#include <QDialog>
namespace Ui {class ViewportControlDialog;};

class ViewWidget;

#include <vector>
using namespace std;

class ViewportControlDialog : public QDialog
{
    Q_OBJECT

public:
    ViewportControlDialog(QWidget *parent = 0);
    ~ViewportControlDialog();

    bool Init(ViewWidget* vw);

private slots:
    void on_pushButtonOK_clicked();
    void onCellChanged(int,int);

private:
    // update the checkboxes on the specified row
    void UpdateRow(int row);

    Ui::ViewportControlDialog *ui;

    ViewWidget* viewWidget;
    int num2DView;
};

#endif // VIEWPORTCONTROLDIALOG_H
