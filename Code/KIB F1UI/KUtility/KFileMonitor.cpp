#include "KFileMonitor.h"
#include <QFileInfo>

KFileMonitor::KFileMonitor(QObject *parent)
    : QThread(parent)
{
    handle = INVALID_HANDLE_VALUE;
}

KFileMonitor::~KFileMonitor()
{
    if( handle != INVALID_HANDLE_VALUE )
    {
        FindCloseChangeNotification( handle );
        handle = INVALID_HANDLE_VALUE;
    }
}

bool KFileMonitor::Monitor(QString& file)
{
    filePath = file;

    QString str = QFileInfo(file).absolutePath();

    QFileInfo info(filePath);
    handle = FindFirstChangeNotification(info.absolutePath().toUtf8(), false, FILE_NOTIFY_CHANGE_LAST_WRITE);

    if( handle==INVALID_HANDLE_VALUE )
        return false;

    lastModified = info.lastModified();
    return true;
}

void KFileMonitor::run()
{
    static int count = 0;
    while( handle != INVALID_HANDLE_VALUE )
    {
        if( WaitForSingleObject(handle, INFINITE) == WAIT_OBJECT_0 )
        {
            if( (++count)==2 )  // due to being notified twice for each change
            {
                QDateTime modified = QFileInfo(filePath).lastModified();
                
                if( modified != lastModified )
                {
                    lastModified = modified;
                    sleep(1);
                    emit fileModified();
                }
                count = 0;
            }
        }

        if( !FindNextChangeNotification( handle ) )
            break;
    }
}