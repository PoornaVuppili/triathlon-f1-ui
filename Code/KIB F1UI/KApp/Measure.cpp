#include "Measure.h"
#include "..\KUtility\SMLibInterface.h"
#pragma warning( disable : 4996 )
#include <IwLine.h>
#include <IwFace.h>
#include <IwTess.h>
#include <IwLoop.h>
#pragma warning( default : 4996 )

double MeasureCloseTolerance = 0.005;

///////////////////////////////////////////
// Entity for measurement
///////////////////////////////////////////

// Default constructor
MeasureEntity::MeasureEntity()
{
    selectType = SelectNone;
}

// Constructor point from IwPoint3d
MeasureEntity::MeasureEntity(const IwPoint3d& pt)
{
    selectType = SelectPoint;
    selectValue.point.x = pt.x;
    selectValue.point.y = pt.y;
    selectValue.point.z = pt.z;
    hitPoint.SetXYZ(pt.x, pt.y, pt.z);
}

// Constructor from IwCurve*
MeasureEntity::MeasureEntity(IwCurve* c, double hitU, const IwPoint3d& hitpoint) 
{
    selectType = SelectCurve;
    selectValue.curve.curve = c;
    selectValue.curve.hitU= hitU;
    selectValue.curve.dl = 0;
	selectValue.curve.originalCurvePointer = nullptr;
    hitPoint.SetXYZ(hitpoint.x, hitpoint.y, hitpoint.z);
}

// Constructor from IwEdge*
MeasureEntity::MeasureEntity(IwEdge* e, double hitU, const IwPoint3d& hitpoint)
{
    selectType = SelectEdge;
    selectValue.edge.edge = e;
    selectValue.edge.hitU = hitU;
    selectValue.edge.dl = 0;
	selectValue.edge.originalEdgePointer = nullptr;
    hitPoint.SetXYZ(hitpoint.x, hitpoint.y, hitpoint.z);
}

// Constructor from IwSurface*
MeasureEntity::MeasureEntity(IwSurface* s, double hitU, double hitV, const IwPoint3d& hitpoint)
{
    selectType = SelectSurface;
    selectValue.surface.surface = s;
    selectValue.surface.hitU = hitU;
    selectValue.surface.hitV = hitV;
    selectValue.surface.dl = 0;
	selectValue.surface.originalSurfacePointer = nullptr;
    hitPoint.SetXYZ(hitpoint.x, hitpoint.y, hitpoint.z);
}

// Constructor from IwFace*
MeasureEntity::MeasureEntity(IwFace* f, double hitU, double hitV, const IwPoint3d& hitpoint)
{
    selectType = SelectFace;
    selectValue.face.face = f;
    selectValue.face.hitU = hitU;
    selectValue.face.hitV = hitV;
    selectValue.face.dl = 0;
	selectValue.face.originalFacePointer = nullptr;
    hitPoint.SetXYZ(hitpoint.x, hitpoint.y, hitpoint.z);
}

// Copy constructor and assignment operator
// - transfer the points and display lists
MeasureEntity::MeasureEntity(const MeasureEntity& e)
{
    CopyTransfer(const_cast<MeasureEntity&>(e));
}

MeasureEntity::~MeasureEntity()
{
    Reset();
}

void MeasureEntity::Reset()
{
    switch(selectType)
    {
    case SelectCurve:
        if( selectValue.curve.originalCurvePointer != nullptr && 
            selectValue.curve.curve != nullptr && 
			selectValue.curve.curve != selectValue.curve.originalCurvePointer )
            IwObjDelete( selectValue.curve.curve );
        if( glIsList(selectValue.curve.dl) )
            glDeleteLists(selectValue.curve.dl, 1);
        break;
    case SelectEdge:
        if( selectValue.edge.originalEdgePointer != nullptr &&  
            selectValue.edge.edge != nullptr &&  
			selectValue.edge.edge != selectValue.edge.originalEdgePointer )
			IwObjDelete( selectValue.edge.edge->GetBrep() );
        if( glIsList(selectValue.edge.dl) )
            glDeleteLists(selectValue.edge.dl, 1);
        break;
    case SelectSurface:
        if( selectValue.surface.originalSurfacePointer != nullptr && 
            selectValue.surface.surface != nullptr && 
			selectValue.surface.surface != selectValue.surface.originalSurfacePointer )
            IwObjDelete( selectValue.surface.surface );
        if( glIsList(selectValue.surface.dl) )
            glDeleteLists(selectValue.surface.dl, 1);
        break;
    case SelectFace:
    case SelectFaceTransparent:
        if( selectValue.face.originalFacePointer != nullptr && 
            selectValue.face.face != nullptr && 
			selectValue.face.face != selectValue.face.originalFacePointer )
			IwObjDelete( selectValue.face.face->GetBrep() );
        if( glIsList(selectValue.face.dl) )
            glDeleteLists(selectValue.face.dl, 1);
        break;
    }
    selectType = SelectNone;
	bUseHitPointToMeasure = false;
}

void MeasureEntity::CopyTransfer(MeasureEntity& e)
{
    selectType = e.selectType;
    hitPoint = e.hitPoint;
	bUseHitPointToMeasure = e.bUseHitPointToMeasure;
    selectValue = e.selectValue;

    switch(selectType)
    {
    case SelectCurve:
        if( selectValue.curve.originalCurvePointer != nullptr && 
            selectValue.curve.curve != nullptr && 
			selectValue.curve.curve != selectValue.curve.originalCurvePointer )
            e.selectValue.curve.curve = nullptr;
        e.selectValue.curve.dl = 0;
        break;
    case SelectEdge:
        if( selectValue.edge.originalEdgePointer != nullptr &&  
            selectValue.edge.edge != nullptr &&  
			selectValue.edge.edge != selectValue.edge.originalEdgePointer )
            e.selectValue.edge.edge = nullptr;
        e.selectValue.edge.dl = 0;
        break;
    case SelectSurface:
        if( selectValue.surface.originalSurfacePointer != nullptr && 
            selectValue.surface.surface != nullptr && 
			selectValue.surface.surface != selectValue.surface.originalSurfacePointer )
	        e.selectValue.surface.surface = nullptr;
        e.selectValue.surface.dl = 0;
        break;
    case SelectFace:
    case SelectFaceTransparent:
        if( selectValue.face.originalFacePointer != nullptr && 
            selectValue.face.face != nullptr && 
			selectValue.face.face != selectValue.face.originalFacePointer )
            e.selectValue.face.face = nullptr;
        e.selectValue.face.dl = 0;
        break;
    }
}

MeasureEntity& MeasureEntity::operator= (const MeasureEntity& e)
{
    if( this != &e )
    {
        Reset();
        CopyTransfer(const_cast<MeasureEntity&>(e));
    }
    return *this;
}

// operator == (close-enough point/line/circle/plane or the same curve/edge/face)
bool MeasureEntity::operator==(const MeasureEntity& ent)
{
    if( selectType == ent.selectType )
    {
        switch(selectType)
        {
        case SelectNone:
            return true;
        case SelectPoint:
            {
                // points : the coordinates are close
                Map<const Vector3d> p(&selectValue.point.x);
                Map<const Vector3d> q(&ent.selectValue.point.x);
                return (p - q).norm() <= MeasureCloseTolerance;
            }
        case SelectLine:
            {
                // line   : two end points are close (allow flipping)
                Map<const Vector3d> ps(&selectValue.line.xs);
                Map<const Vector3d> pe(&selectValue.line.xe);
                Map<const Vector3d> qs(&ent.selectValue.line.xs);
                Map<const Vector3d> qe(&ent.selectValue.line.xe);
                if( (ps - qs).norm() <= MeasureCloseTolerance )
                    return (pe - qe).norm() <= MeasureCloseTolerance;
                else if( (ps - qe).norm() <= MeasureCloseTolerance )
                    return (pe - qs).norm() <= MeasureCloseTolerance;
                return false;
            }
        case SelectCircle:
            {
                // circle : the center and radius are close + normal in the same/opposite direction
                Map<const Vector3d> p(&selectValue.circle.x);
                Map<const Vector3d> q(&ent.selectValue.circle.x);
                return (p - q).norm() <= MeasureCloseTolerance &&
                       fabs(selectValue.circle.rad - ent.selectValue.circle.rad) <= MeasureCloseTolerance &&
                       fabs( Map<const Vector3d>(&selectValue.circle.nx).dot( 
                             Map<const Vector3d>(&ent.selectValue.circle.nx)) ) >= 1-MeasureCloseTolerance;
            }
        case SelectPlane:
            {
                // plane  : four corners are all close
                Map<const Vector3d> p1(&selectValue.plane.cx);
                Map<const Vector3d> p2(&selectValue.plane.ux);
                Map<const Vector3d> p3(&selectValue.plane.vx);
                Map<const Vector3d> q1(&ent.selectValue.plane.cx);
                Map<const Vector3d> q2(&ent.selectValue.plane.ux);
                Map<const Vector3d> q3(&ent.selectValue.plane.vx);
                Vector3d q4 = q2 - q1 + q3;
                return (((p1-q1).norm() <= MeasureCloseTolerance || (p1-q2).norm() <= MeasureCloseTolerance ||
                         (p1-q3).norm() <= MeasureCloseTolerance || (p1-q4).norm() <= MeasureCloseTolerance ) &&
                        ((p2-q1).norm() <= MeasureCloseTolerance || (p2-q2).norm() <= MeasureCloseTolerance ||
                         (p2-q3).norm() <= MeasureCloseTolerance || (p2-q4).norm() <= MeasureCloseTolerance ) &&
                        ((p3-q1).norm() <= MeasureCloseTolerance || (p3-q2).norm() <= MeasureCloseTolerance ||
                         (p3-q3).norm() <= MeasureCloseTolerance || (p3-q4).norm() <= MeasureCloseTolerance ));
            }
        case SelectCurve:    // curve
			{
				return selectValue.curve.curve == ent.selectValue.curve.curve || selectValue.curve.originalCurvePointer == ent.selectValue.curve.curve;
			}
        case SelectEdge:     // edge
			{
				return selectValue.edge.edge == ent.selectValue.edge.edge || selectValue.edge.originalEdgePointer == ent.selectValue.edge.edge;
			}
        case SelectSurface:  // surface
			{
				return selectValue.surface.surface == ent.selectValue.surface.surface || selectValue.surface.originalSurfacePointer == ent.selectValue.surface.surface;
			}
        case SelectFace:     // face
			{
				return selectValue.face.face == ent.selectValue.face.face || selectValue.face.originalFacePointer == ent.selectValue.face.face;
			}
        }
    }
    return false;
}

void MeasureEntity::Transform(IwAxis2Placement& transMatrix)
{
	// point
	if ( selectType == SelectPoint )
	{
		IwPoint3d point = IwPoint3d(selectValue.point.x,selectValue.point.y,selectValue.point.z);
		transMatrix.TransformPoint(point, point);
		selectValue.point.x = point.x;
		selectValue.point.y = point.y;
		selectValue.point.z = point.z;
	}
	// Line
	else if ( selectType == SelectLine )
	{
		IwPoint3d startPoint, endPoint;
		startPoint = IwPoint3d(selectValue.line.xs,selectValue.line.ys,selectValue.line.zs);
		endPoint = IwPoint3d(selectValue.line.xe,selectValue.line.ye,selectValue.line.ze);
		transMatrix.TransformPoint(startPoint, startPoint);
		transMatrix.TransformPoint(endPoint, endPoint);
		selectValue.line.xs = startPoint.x;
		selectValue.line.ys = startPoint.y;
		selectValue.line.zs = startPoint.z;
		selectValue.line.xe = endPoint.x;
		selectValue.line.ye = endPoint.y;
		selectValue.line.ze = endPoint.z;
	}
	// circle
	else if ( selectType == SelectCircle )
	{
		IwPoint3d center;
		IwVector3d normal;
		center = IwPoint3d(selectValue.circle.x,selectValue.circle.y,selectValue.circle.z);
		normal = IwVector3d(selectValue.circle.nx,selectValue.circle.ny,selectValue.circle.nz);
		transMatrix.TransformPoint(center, center);
		transMatrix.TransformPoint(normal, normal);
		selectValue.circle.x = center.x;
		selectValue.circle.y = center.y;
		selectValue.circle.z = center.z;
		selectValue.circle.nx = normal.x;
		selectValue.circle.ny = normal.y;
		selectValue.circle.nz = normal.z;
	}
	// plane
	else if ( selectType == SelectPlane )
	{
		IwPoint3d origin, pointU, pointV;
		origin = IwPoint3d(selectValue.plane.cx,selectValue.plane.cy,selectValue.plane.cz);
		pointU = IwPoint3d(selectValue.plane.ux,selectValue.plane.uy,selectValue.plane.uz);
		pointV = IwPoint3d(selectValue.plane.vx,selectValue.plane.vy,selectValue.plane.vz);
		transMatrix.TransformPoint(origin, origin);
		transMatrix.TransformPoint(pointU, pointU);
		transMatrix.TransformPoint(pointV, pointV);
		selectValue.plane.cx = origin.x;
		selectValue.plane.cy = origin.y;
		selectValue.plane.cz = origin.z;
		selectValue.plane.ux = pointU.x;
		selectValue.plane.uy = pointU.y;
		selectValue.plane.uz = pointU.z;
		selectValue.plane.vx = pointV.x;
		selectValue.plane.vy = pointV.y;
		selectValue.plane.vz = pointV.z;
	}
	// curve
	else if ( selectType == SelectCurve )
	{
		IwCurve* copyCurve=NULL;
		IwCurve* originalCurve = selectValue.curve.curve;
		originalCurve->Copy(*iwContext, copyCurve);
		copyCurve->Transform(transMatrix);
		selectValue.curve.originalCurvePointer=originalCurve;
		selectValue.curve.curve=copyCurve;
	}
	// surface
	else if ( selectType == SelectSurface )
	{
		IwSurface* copySurface=NULL;
		IwSurface* originalSurface = selectValue.surface.surface;
		originalSurface->Copy(*iwContext, copySurface);
		copySurface->Transform(transMatrix);
		selectValue.surface.originalSurfacePointer=originalSurface;
		selectValue.surface.surface=copySurface;
	}
	// Edge
	else if ( selectType == SelectEdge )
	{
		IwEdge* originalEdge = selectValue.edge.edge;
		IwBrep* copyBrep = new ( *iwContext ) IwBrep();
		IwTArray<IwEdge*> edges, copyEdges;
		edges.Add(originalEdge);
		originalEdge->GetBrep()->CopyEdges(edges, copyBrep, &copyEdges);
		copyBrep->Transform(transMatrix);
		selectValue.edge.originalEdgePointer = originalEdge;
		selectValue.edge.edge = copyEdges.GetAt(0);
	}
	// Face
	else if (selectType == SelectFace || selectType == SelectFaceTransparent)
	{
		IwFace* originalFace = selectValue.face.face;
		IwBrep* copyBrep = new ( *iwContext ) IwBrep();
		IwTArray<IwFace*> faces, copyFaces;
		faces.Add(originalFace);
		originalFace->GetBrep()->CopyFaces(faces, copyBrep, &copyFaces);
		copyBrep->Transform(transMatrix);
		selectValue.face.originalFacePointer = originalFace;
		selectValue.face.face = copyFaces.GetAt(0);
	}

    // hit point
    IwPoint3d hit = hitPoint;
    transMatrix.TransformPoint(hit, hit);
    hitPoint = hit;
}

////////////////////////////////////
// Entity measurement result
////////////////////////////////////

void MeasureResult::Reset()
{
    valid = false;
    mode = None;
}

// Compute the measurement result based on mode and entities
MeasureResult MeasureResult::Compute(MeasureMode measureMode, std::vector<MeasureEntity>& entities)
{
    size_t n = entities.size();
    switch( measureMode )
    {
    case EntityDistance:
        if( n==2 ) return ComputeEntityDistance(entities[0], entities[1]);
        break;

    case EntityAngle:
        if( n==2 )
            return ComputeEntityAngle(entities[0], entities[1]);
        else if( n==3 )
            return ComputeEntityAngle(entities[0], entities[1], entities[2]);
        else if( n==4 )
            return ComputeEntityAngle(entities[0], entities[1], entities[2], entities[3]);
        break;

    case EntityRadius:
        if( n==1 ) return ComputeEntityRadius(entities[0]);
        break;

    case ScreenDistance:
        if( n==2 ) return ComputeScreenDistance(entities[0], entities[1]);
        break;

    case ScreenAngle:
        if( n==4 ) return ComputeScreenAngle(entities[0], entities[1], entities[2], entities[3]);
        break;

    case ScreenRadius:  // 3 points to fit a circle for radius
        if( n==3 ) return ComputeScreenRadius(entities[0], entities[1], entities[2]);
        break;
    }
    return MeasureResult();
}

// Get the measurement result if valid. NaN otherwise.
double MeasureResult::GetResult()
{
    if( valid )
    {
        switch( mode )
        {
        case EntityDistance:
        case ScreenDistance:
            return value.distance.distance;

        case EntityAngle:
        case ScreenAngle:
            return value.angle.degree;
    
        case EntityRadius:
        case ScreenRadius:
            return value.radius.radius;
        }
    }
    throw std::exception("Invalid measurement result");
}
