#pragma once
#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\EntityEnums.h"
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include <QtGui>
#include <map>

class TEST_EXPORT_TW IFemurImplant
{
public:
	IFemurImplant();
	~IFemurImplant();

	// FemoralPart
	static void FemoralPart_GetSurfaces(bool copy, IwBSplineSurface*& mainSurface, IwBSplineSurface*& leftSideSurface, IwBSplineSurface*& rightSideSurface);

	// FemoralAxes
	static void FemoralAxes_GetFeaturePoints(IwPoint3d& notchProximalPoint, IwPoint3d& leftEpiCondylarPoint, IwPoint3d& rightEpiCondylarPoint, double& antCutFlexionAngle, double& antCutObliqueAngle, double& femoralAxesFlexionAngle);
	static IwAxis2Placement FemoralAxes_GetMechanicalAxes();
	static IwAxis2Placement FemoralAxes_GetWhiteSideLineAxes();
	static IwAxis2Placement FemoralAxes_GetSweepAxes();
	static double FemoralAxes_GetFemurEpiCondylarSize();
	static double FemoralAxes_GetFemurEpiCondylarSizeRatio();
	static void FemoralAxes_GetAntCutPlaneInfo(IwPoint3d& antCutPlaneCenter, IwVector3d& antCutPlaneOrientation);
	static void FemoralAxes_GetMechAxis3Planes(double refSizeRatio, IwBSplineSurface*& coronalPlane, IwBSplineSurface*& sagittalPlane, IwBSplineSurface*& distalPlane);

	static std::map<QString, IwPoint3d> FemoralAxesInp_PointsDataFromStrykerLayout();
	static void FemoralAxes_AntCenterPointAndAntRotCenterPoint(IwPoint3d &antPlaneCenter, IwPoint3d &antPlaneRotationCenter);
	static IwBrep* GetStrykerFemCutBrep();

	// FanningPlanes

	// JCurves

	static std::map<QString, IwPoint3d> GetStrykerFemLayoutCutPointsMap();
	static std::map<QString, IwPoint3d> GetStrykerImplantFeaturePointsMap();

	//JCurve Inputs By Erik
	static IwBSplineCurve * JCurvesInp_GetJCurve(int index);
	static void JCurvesInp_GetJCurves(IwTArray<IwCurve*>& curves);
	static IwPoint3d JCurvesInp_GetWeightBearingMLCenterPoint();
	static IwPoint3d JCurvesInp_GetMedialJPoint();

	//Peg Point by erik
	static void PegPointsFromStrykerFemLayout(IwPoint3d &posPegPosition, IwPoint3d &negPegPosition);

	// Outline Profile
	static IwBSplineSurface* OutlineProfile_GetSketchSurface(bool bCopy);
	static IwBSplineCurve* OutlineProfile_GetSketchCurve(bool bCopy, IwBSplineCurve** uvCurve=NULL);
	static void OutlineProfile_GetFeaturePoints(IwTArray<IwPoint3d>& featurePointsUV, IwTArray<int>& featurePointsInfo);
	static int OutlineProfile_InsertAFeaturePointByOrder(IwTArray<IwPoint3d>& featurePointsUV, IwTArray<int>& featurePointsInfo, IwPoint3d insertPoint);
	static IwBSplineCurve* OutlineProfile_CreateUVCurveOnSketchSurface(IwTArray<IwPoint3d>& featurePointsUV, IwTArray<int>& featurePointsInfo, IwBSplineCurve** xyzCurve=NULL);
	static IwPoint3d OutlineProfile_GetWeightBearingMLCenter(IwTArray<IwPoint3d>& MedialProfilePoints, IwTArray<IwPoint3d>& LateralProfilePoints);

	// Femoral Cuts
	static void FemoralCuts_GetCutsInfo(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations);
	static void FemoralCuts_GetCutsFeaturePoints(IwTArray<IwPoint3d>& cutFeaturePoints, IwTArray<IwPoint3d>& antOuterPoints);
	static void FemoralCuts_GetCondylarCutWidths(double& posWidth, double& negWidth);
	static IwBrep* FemoralCuts_GetIwBrep(bool bCopy);

	//FemCut input given by Erik
	static void FemoralCutsInp_GetCutsInfo(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations);
	static IwTArray<IwPoint3d> IFemurImplant::FemoralCutsInp_GetCutsFeaturePoints();

	//Implant cut Feautre points shared by erik
	static IwTArray<IwPoint3d> ImplantInp_GetCutsFeaturePointInfo();

	static IwVector3d PosteriorFaceNormalVector();
	
	
	// Display/Hide an entity
	static void DisplayHideEntity(CEntRole entRole, CDisplayMode dispMode);
	static void HideAllFemurImplantEntities();

	// femur implant info
	static bool IsFemurImplantComplete();
	static bool IsFemurImplantWithValidAPMLSizes();

	// For Review Rework dialog
	static void Review_DisplayReviewCommentDialog(CEntRole eRole);

	// For Review
	static void Review_StartReviewFemur();

private:
	static void	CloseImplantImmediately(QString& errorIFunctionName);

private:

};