@echo off
rem --- this is the command file that copies 3rd party dlls
rem     from their respective locations to the target dir
rem     supposedly will be called with parameters that enumerate
rem     the folders from which to copy DLL and PDB files
rem
rem     collect_dlls  <targetdir> <dir1> <dir2> ...

rem :DEBUG1 echo %0 %1 %2 %3 %4 %5 %6 %7 %8 %9
rem :DEBUG2 goto end

if "%9"=="" goto check8
xcopy /Y /R %9\*.dll %1
xcopy /Y /R %9\*.pdb %1
:check8
if "%8"=="" goto check7
xcopy /Y /R %8\*.dll %1
xcopy /Y /R %8\*.pdb %1
:check7
if "%7"=="" goto check6
xcopy /Y /R %7\*.dll %1
xcopy /Y /R %7\*.pdb %1
:check6
if "%6"=="" goto check5
xcopy /Y /R %6\*.dll %1
xcopy /Y /R %6\*.pdb %1
:check5
if "%5"=="" goto check4
xcopy /Y /R %5\*.dll %1
xcopy /Y /R %5\*.pdb %1
:check4
if "%4"=="" goto check3
xcopy /Y /R %4\*.dll %1
xcopy /Y /R %4\*.pdb %1
:check3
if "%3"=="" goto check2
xcopy /Y /R %3\*.dll %1
xcopy /Y /R %3\*.pdb %1
:check2
if "%2"=="" goto end
xcopy /Y /R %2\*.dll %1
xcopy /Y /R %2\*.pdb %1

:end
