#include "TotalEntPanelPS.h"
#include "TotalDocPS.h"
#include "TotalViewPS.h"
#include "EntityForTwin.h"
#include "IFemurImplantPS.h"
#include "..\KApp\EntityWidget.h"
#include "..\KApp\SwitchBoard.h"
#include "..\KTriathlonF1\TotalEntPanelJigs.h"
#include "../KTriathlonF1/IFemurImplant.h"

KTotalEntPanelPS::KTotalEntPanelPS(QTreeView* view) : CTotalEntPanel(view)
{
	SwitchBoard::addSignalSender(SIGNAL(FemoralJigsNeedToChange()), this);
}

KTotalEntPanelPS::~KTotalEntPanelPS()
{
	SwitchBoard::removeSignalSender(this);
}

void KTotalEntPanelPS::Add( int id, const QString& sName, CEntRole eRole )
{
	// For Femoral Implant Basis & Jigs
	if ( eRole >= ENT_ROLE_NONE &&
		 eRole <= ENT_ROLE_JIGS_END )
	{
		CTotalEntPanel::Add(id, sName, eRole);
		return;
	}

	// For PS Features
    // create the 'PS Features' root item
	//PSV
    if( items.size()==0 )
    {
        QStandardItem* item = NewItem("Femoral PS Features", false, false);
	    item->setData( QVariant(),Qt::CheckStateRole);//PSV
        QFont font = item->font();
        font.setBold(true);
        font.setItalic(true);
        item->setFont(font);

        items.append( item );

		if ( !entityWidget->IsItemExist(this) )
			entityWidget->AddItem( this );
    }

	CPanelElem		el;

	el.id			= id;
	el.eEntRole		= eRole;
	el.sEntName		= sName;

	m_list.append( el );

	QStandardItem*		itemId = NewItem("", true, false, true);

	CEntity*	pEnt = m_pDoc->GetEntityById( id );

	if ( pEnt )
		itemId->setFlags( Qt::ItemIsEnabled );
	else
		itemId->setFlags( Qt::NoItemFlags );

	QString sEntName = sName;
	if ( pEnt && !pEnt->GetUpToDateStatus())
	{
		sEntName = "*" + sEntName;// Add "*" if entity is out of date
	}

	itemId->setData( sEntName, 0 );
	itemId->setFlags( Qt::ItemIsEnabled );
	itemId->setSelectable(false);

	if(pEnt && pEnt->Has2ndItem())
		items[0]->appendRow( QList<QStandardItem*>() << itemId << pEnt->Get2ndItem());
	else
		items[0]->appendRow( QList<QStandardItem*>() << itemId << NewItem(""));

	if ( pEnt )
	{
		if( pEnt->GetDisplayMode() != DISP_MODE_NONE && pEnt->GetDisplayMode() != DISP_MODE_HIDDEN )
			itemId->setCheckState( Qt::Checked );
		else
			itemId->setCheckState( Qt::Unchecked );
	}

}


void KTotalEntPanelPS::OnItemClicked(const QModelIndex& index)
{
    if( items[0]->index() == index )
        return; // root node

    // sub node
    int row = index.row();
    assert( row >= 0 && row < m_list.size() );

	CEntity*	pEnt = m_pDoc->GetEntityById( m_list[row].id );
	CEntRole eRole = pEnt->GetEntRole();

	// For Femoral Implant Basis & Jigs
	if ( eRole >= ENT_ROLE_NONE &&
		 eRole <= ENT_ROLE_JIGS_END )
	{
		CTotalEntPanel::OnItemClicked(index);
		return;
	}

	ItemClickedPS( index );
}

void KTotalEntPanelPS::OnItemRightClicked(const QModelIndex& index)
{
    if( items[0]->index() == index )
        return; // root node

    // sub node
    int row = index.row();
    assert( row >= 0 && row < m_list.size() );

	m_id = m_list[row].id;
	CEntity*	pEntity = m_pDoc->GetEntityById( m_id );
	if ( pEntity == NULL )
		return;

	// the items for femoral implant basic feature
	if ( pEntity->GetEntRole() >= ENT_ROLE_NONE &&
		 pEntity->GetEntRole() <= ENT_ROLE_IMPLANT_END )
	{
		DisplayContextMenu( row );
	}

	// for jigs
	else if ( pEntity->GetEntRole() >= ENT_ROLE_JIGS_NONE &&
		      pEntity->GetEntRole() <= ENT_ROLE_JIGS_END )
	{
		GetTotalDocPS()->GetEntPanelJigs()->DisplayContextMenuJigs( row );
	}
}

void KTotalEntPanelPS::ItemClickedPS(const QModelIndex& index)
{
	int row = index.row();

	CEntity*	pEnt = m_pDoc->GetEntityById( m_list[row].id );
	if( pEnt == NULL )
		return;

	CTotalEntPanel::ItemClicked(index);
}

void KTotalEntPanelPS::DisplayContextMenuPS( int row ) 
{
	// initial data
	m_actDisplay = NULL;
	m_actRedefine = NULL;
	m_actRegenerateAStep = NULL;
	m_actRegenerateMultipleSteps = NULL;
	m_actRetessellate = NULL;
	m_actRegenerateAStep = NULL;

	CEntity*	pEntity;

	m_id = m_list[row].id;
	pEntity = m_pDoc->GetEntityById( m_id );

	if ( pEntity == NULL )
		return;

	QMenu		Menu;
	QMenu*		pMenuDisplayMode( &Menu );

	// Check whether any manager is active.
	bool managerExists = false;
	if ( m_pDoc->GetView()->GetManager() || m_pDoc->GetView()->GetGlobalManager() )
		managerExists = true;

	// Use environment variable to activate ExportIGES
	bool enableExportIGES = false;
	char *envExportIGES = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (envExportIGES != NULL && QString("1") == envExportIGES)
	{
		enableExportIGES = true;
	}
	IwAxis2Placement transM;
	bool bMatesUpToDate ;

	// Open, Regenerate depend on the the edit/review mode
	if ( mainWindow->IsReviewMode() )
	{
		// disable all entities belong to jigs design
		if ( m_actRedefine )
			m_actRedefine->setEnabled(false);
		if ( m_actRegenerateAStep )
			m_actRegenerateAStep->setEnabled(false);
		if ( m_actRegenerateMultipleSteps ) 
			m_actRegenerateMultipleSteps->setEnabled(false);
	}

	QPoint		pos = QCursor::pos();

	pos.setY( pos.y() + 10 );

	Menu.exec( pos );
}

void KTotalEntPanelPS::OnRedefinePS()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	KTotalViewPS*	pView = (KTotalViewPS*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("KTotalEntPanelPS::OnRedefinePS(), entity name: %1").arg(pEnt->GetEntName()) );
}

void KTotalEntPanelPS::OnDeleteAllPS()
{
	m_pDoc->AppendLog( QString("KTotalEntPanelPS::OnDeleteAllPS()") );

	int		button;
	button = QMessageBox::warning( NULL, 
									tr("Warning Message!"), 
									tr("All PS steps will be deleted.\nDo you want to proceed deletion?"),
									QMessageBox::Yes, QMessageBox::No);	

	if ( button == QMessageBox::No ) return;

	GetTotalDocPS()->DeleteEntitiesBelongToPSFeatures();

	m_pDoc->GetView()->Redraw();

}

void KTotalEntPanelPS::OnRegenerateAStepPS()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	KTotalViewPS*	pView = GetView();

	m_pDoc->AppendLog( QString("KTotalEntPanelPS::OnRegenerateAStepPS(), entity name: %1").arg(pEnt->GetEntName()) );
}

void KTotalEntPanelPS::OnRegenerateMultipleStepsPS()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	KTotalViewPS*	pView = GetView();

	m_pDoc->AppendLog( QString("KTotalEntPanelPS::OnRegenerateMultipleStepsPS(), entity name: %1").arg(pEnt->GetEntName()) );

	bool regenerateOnly = true;
}

void KTotalEntPanelPS::NotifyOfPSFeaturesChange()
{
   emit FemoralJigsNeedToChange();
}