#include "Manager.h"
#include "..\KUtility\KUtility.h"
#include <QBitmap>
#include <QUndoStack>
#include "ViewWidget.h"

QCursor* Manager::rotateCursor;
QCursor* Manager::arrowBox;
QCursor* Manager::arrowBoxPlus;
QCursor* Manager::arrowBoxMinus;
QCursor* Manager::arrowLasso;
QCursor* Manager::arrowLassoPlus;
QCursor* Manager::arrowLassoMinus;

Manager::Manager(QObject *parent, QUndoStack* stack)
    : QObject(parent)
    , m_reporter(this)
{
    widget      = NULL;
    m_toolbar   = NULL;
    undoStack   = stack;           // default: no undo, not shared
    sharedStack = (undoStack != NULL);

    if( rotateCursor == NULL )
    {
        rotateCursor = new QCursor(QBitmap(":/Resources/Rotate.png"), 
            QBitmap(":/Resources/RotateMask.png"), 15,16);
        arrowBox = new QCursor(QBitmap(":/Resources/ArrowSquare.png"), 
            QBitmap(":/Resources/ArrowSquareMask.png"), 0,0);
        arrowBoxPlus = new QCursor(QBitmap(":/Resources/ArrowSquarePlus.png"), 
            QBitmap(":/Resources/ArrowSquarePlusMask.png"), 0,0);
        arrowBoxMinus = new QCursor(QBitmap(":/Resources/ArrowSquareMinus.png"), 
            QBitmap(":/Resources/ArrowSquareMinusMask.png"), 0,0);
        arrowLasso = new QCursor(QBitmap(":/Resources/ArrowLasso.png"), 
            QBitmap(":/Resources/ArrowLassoMask.png"), 0,0);
        arrowLassoPlus = new QCursor(QBitmap(":/Resources/ArrowLassoPlus.png"), 
            QBitmap(":/Resources/ArrowLassoPlusMask.png"), 0,0);
        arrowLassoMinus = new QCursor(QBitmap(":/Resources/ArrowLassoMinus.png"), 
            QBitmap(":/Resources/ArrowLassoMinusMask.png"), 0,0);
    }
}

Manager::~Manager()
{
    DeletePtr( widget );
    //DeletePtr( toolbar ); // lead to crash, let qt take care of the deletion
    if( !sharedStack )
        DeletePtr( undoStack );
}

// enable the undostack
void Manager::EnableUndoStack(bool enable)
{
    if( enable )
    {
        if( !undoStack )
            undoStack = new QUndoStack(this);
    }
    else if( sharedStack )
    {
        undoStack = NULL;
        sharedStack = false;
    }
    else
        DeletePtr( undoStack );
}

void Manager::doDismiss()
{
   viewWidget->RemoveManager(this);
   viewWidget->update();
   deleteLater();
}

void Manager::dismissOnAccept()
{
   m_reporter.setReason(ExitSignaller::DismissedByAccept);
   doDismiss();
}

void Manager::dismissOnCancel()
{
   m_reporter.setReason(ExitSignaller::DismissedByCancel);
   doDismiss();
}

void Manager::dismissOnReviewNext()
{
   m_reporter.setReason(ExitSignaller::DismissedByReviewNext);
   doDismiss();
}

void Manager::dismissOnReviewPrev()
{
   m_reporter.setReason(ExitSignaller::DismissedByReviewPrev);
   doDismiss();
}

void Manager::dismissOnRework()
{
   m_reporter.setReason(ExitSignaller::DismissedByRework);
   doDismiss();
}

void Manager::noExitReasonYet()
{
   m_reporter.setReason(ExitSignaller::ExceptionOrOtherError);
}
