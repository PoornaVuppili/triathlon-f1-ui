#include "KUtility.h"


const char* formatDateTime = "MMM d,yyyy h:m:s";

// get the current user name
QString CurrentUserName()
{
    char *pValue;
    size_t len;
    errno_t err = _dupenv_s( &pValue, &len, "USERNAME" );
    if ( err )
        return "";

    QString str(pValue);
    free( pValue );
    return str;
}

QString GetSerialNumberFromFolderName(QString const& folderName)
{
    // the folder name can contain prefix, suffix and some non-alphanumeric symbols after it
    // Example:  "A1234567BC, a case of a lost balance"

    // find first alphanumeric character, start skipping non-digits.
    int firstDigit = folderName.indexOf(QRegExp("[0-9]"));
    if (firstDigit < 0) // not found
        return "";

    QString rv = folderName.mid(firstDigit, 7);

    return rv;
}

bool gt(double a, double b)	{	return a > b; }
bool lt(double a, double b)	{	return a < b; }

double ClosestDistanceError(MatrixXd& movMat, MatrixXd& fixMat, Vector3d& directionError)
{
	double errorSum = 0;
	IwVector3d sumDirError(0,0,0);
	int numPts = movMat.rows();
	for(int idxM=0; idxM < numPts; idxM++)
	{
		double minDist = 1.0e10;
		int minIdx = 0;
		IwVector3d dirError(0,0,0);
		for(int idxF=0; idxF < fixMat.rows(); idxF++)
		{
			IwVector3d pt1(movMat.row(idxM).x(), movMat.row(idxM).y(),movMat.row(idxM).z());
			IwVector3d pt2(fixMat.row(idxF).x(), fixMat.row(idxF).y(),fixMat.row(idxF).z());
			double dist = pt1.DistanceBetween(pt2);
			if (dist < minDist)
			{
				minDist = dist;
				dirError = pt1-pt2;

			}
				
		}
	errorSum += minDist;
	sumDirError += dirError;
	}

	directionError[0] = sumDirError.x/numPts;
	directionError[1] = sumDirError.y/numPts;
	directionError[2] = sumDirError.z/numPts;
	return errorSum;
}