#include <QtGui>
#include <QtOpenGL>
#include <qfile.h>
#include <gl\gl.h>
#pragma warning( disable : 4996 4805 )
#include <IwTSLib_all.h>
#include <IwTess.h>
#include <IwInterface.h>
//#include "IwFilletSolver.h"
//#include "IwFilletStandardSolver.h"
//#include <IwFilletExecutive.h>
#include <IwTrimmingTools.h>
#pragma warning( default : 4996 4805 )
#include "Part.h"
#include "DataDoc.h"
#include "STLReader.h"
#include "IgesReader.h"
#include "FakeXmlReader.h"
#include "Tess.h"
#include "OpenGLView.h"
#include "Globals.h"
#include "Utilities.h"


CPart::CPart( CDataDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
		CEntity( pDoc, eEntRole, sFileName, nEntId )
{
	m_eEntType = ENT_TYPE_PART;
	m_ePartType = PART_TYPE_BREP;

	if( m_sFileName.endsWith( "stl", Qt::CaseInsensitive ) )
		m_ePartType = PART_TYPE_STL;

	m_glFaceList = 0;
	m_glEdgeList = 0;
    m_glIsoCurveList = 0;
	m_glIsoCurveListWithNormal = 0;

	m_pBrep	= NULL;
	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = white;

	m_bModified = false;
	m_bDataModified = false;
}


CPart::CPart( const CPart* pPart )
{
	if( m_pBrep )
		delete m_pBrep;
}


CPart::~CPart()
{
}


void CPart::Init()
{
}


void CPart::SetPartType( CPartType ePartType )
{
	m_ePartType = ePartType;
	m_pDoc->SetModified( true );
}


CPartType CPart::GetPartType()
{
	return m_ePartType;
}


bool CPart::ImportPart( const QString& sFileName )
{
	bool		bOK = false;

	switch( m_ePartType )
	{
		case PART_TYPE_STL:
			bOK = LoadSTL( sFileName );
			break;

		case PART_TYPE_BREP:
		{
				if (sFileName.endsWith(".igs", Qt::CaseInsensitive) || sFileName.endsWith(".iges", Qt::CaseInsensitive))
					bOK = LoadIGES( sFileName );
				else if (sFileName.endsWith(".xml", Qt::CaseInsensitive))
					bOK = LoadXML( sFileName );
			break;
		}
	}

	SetModifiedFlag(true);

	return bOK;
}


bool CPart::LoadSTL( const QString& sFileName )
{
	CSTLReader*	pReader = new CSTLReader( this, sFileName );

	bool bOK = pReader->LoadFile();

	delete pReader;

	return bOK;
}


bool CPart::LoadIGES( const QString& sFileName )
{
	CIgesReader*	pReader = new CIgesReader( this, sFileName );

	bool bOK = pReader->LoadFile();

	delete pReader;

	return bOK;
}


bool CPart::LoadXML( const QString& sFileName )
{
	CFakeXmlReader xmlReader = CFakeXmlReader( sFileName );

	bool bOK = false;

	IwPoint3d pnt;

	if ( m_eEntRole == ENT_ROLE_HIP )
	{
		QString strID =xmlReader.GetString(QString("ID"));
		if ( strID == "Hip") 
		{
			bOK = xmlReader.GetIwPoint3d(QString("FemHeadCenter"), pnt);
			if (bOK) 
			{
				m_points.Add(pnt);
				SetModifiedFlag(true);
			}
		}
	}
	
	// Expand part bounds
	m_Bounds.Expand(pnt);

	return bOK;
}

bool CPart::ImportBrep( const QString& sFileName )
{
	IwContext&			iwContext = m_pDoc->GetIwContext();
	QByteArray			byteArray;
	int					idx;

	m_pBrep = NewBrep( 0.0001 );
	
	m_pBrep = ReadBrep( sFileName );

	if( m_pBrep == NULL )
		return false;

	m_sEntName = GetFileNameWithExtension( sFileName );

	idx = m_sEntName.lastIndexOf( "." );
	m_sEntName = m_sEntName.left( idx );

	MakeTess();

	return true;
}


void CPart::SetIwBrep( IwBrep* pBrep )
{
	m_pBrep = pBrep;

	SetModifiedFlag(true);
}


IwBrep* CPart::GetIwBrep()
{
	return m_pBrep;
}

void CPart::GetPoints(IwTArray<IwPoint3d>& points)
{
	points.Append(m_points);
}

void CPart::SetIwCurves( IwTArray<IwCurve*> const& curves )
{
	for (unsigned i=0; i<m_curves.GetSize(); i++)
	{
		IwCurve* crv = m_curves.GetAt(i);
		if (crv) IwObjDelete(crv);
	}
	m_curves.RemoveAll();

	m_curves.Append(curves);

	DeleteDisplayList( m_glFaceList );// need to delete m_glFaceList to force update screen
}

IwTArray<IwCurve*> CPart::GetIwCurves()
{
	return m_curves;
}

void CPart::SetIwPoints( IwTArray<IwPoint3d>const & points )
{
	m_points.RemoveAll();

	m_points.Append(points);

	for (unsigned i=0; i<points.GetSize(); i++)
		m_Bounds.Expand(points.GetAt(i));

}

IwTArray<IwPoint3d> CPart::GetIwPoints()
{
	return m_points;
}

void CPart::GetIwData
(
	IwBrep*& pBrep, 
	IwTArray<IwCurve*>& curves, 
	IwTArray<IwPoint3d>& points 
)
{
	pBrep = m_pBrep;
	curves.Append(m_curves);
	points.Append(m_points);
}

int CPart::NewDisplayList()
{
	return m_pDoc->GetAvlDisplayList();
}


void CPart::DeleteDisplayList( long& glList )
{
	if( glList != 0 )
	{
		if( glIsList( glList ) )
			glDeleteLists( glList, 1 );

		glList = 0;
	}
}


void CPart::SetModifiedFlag( bool bModified )
{
	m_bModified = bModified;
	if (m_bModified)
	{
		m_bDataModified = true;
		m_pDoc->SetModified(m_bModified);
	}
}


bool CPart::IsModified()
{
	return m_bModified;
}


void CPart::InvalidateEdges()
{
	DeleteDisplayList( m_glEdgeList );
}


void CPart::Display()
{
	if( m_eDisplayMode == DISP_MODE_DISPLAY ||
		m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
		m_eDisplayMode == DISP_MODE_DISPLAY_NET ||
		m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET
	  )
	{

		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
			 m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthMask(GL_FALSE);
		}
        else if (m_eDisplayMode == DISP_MODE_DISPLAY ||
                 m_eDisplayMode == DISP_MODE_DISPLAY_NET)
        {
            glDisable(GL_BLEND);
            glDepthMask(GL_TRUE);
        }

		// Display surfaces
		glEnable( GL_DEPTH_TEST );

		glEnable( GL_POLYGON_OFFSET_FILL );
		glPolygonOffset( 3.0, 100.0 );

		if( m_bModified && m_glFaceList > 0 )
			DeleteDisplayList( m_glFaceList );

		glColor4ub( m_color[0], m_color[1], m_color[2], m_pDoc->GetTransparencyFactor());

		if( m_glFaceList == 0 )
		{
			m_glFaceList = NewDisplayList();
			glNewList( m_glFaceList, GL_COMPILE_AND_EXECUTE );
			DisplayFaces();
			DisplayCurves();
			DisplayPoints();
			glEndList();
		}
		else
		{
			glCallList( m_glFaceList );
		}

		glDisable( GL_POLYGON_OFFSET_FILL );

		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
			 m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
		}

	}

	if( m_ePartType == PART_TYPE_STL )
		return;

	{
		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
			 m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthMask(GL_FALSE);
		}

		// Display edges
		glDisable( GL_LIGHTING );

		if( m_bModified && m_glEdgeList > 0 )
			DeleteDisplayList( m_glEdgeList );

		if( m_glEdgeList == 0 )
		{
			m_glEdgeList = NewDisplayList();
			glNewList( m_glEdgeList, GL_COMPILE_AND_EXECUTE );
			DisplayEdges();
			glEndList();
		}
		else
		{
			glCallList( m_glEdgeList );
		}

		glEnable( GL_LIGHTING );

		// Display the iso curves only for osteophyte/cartilage/merged/inner/outer surfaces.
		// Their faces are untrimmed (therefore, easy to display iso curves).
		// I prefer to hard code these 6 types here.
		if ( (m_eEntRole == ENT_ROLE_FEMUR ||
			  m_eEntRole == ENT_ROLE_OSTEOPHYTE_SURFACE ||
			  m_eEntRole == ENT_ROLE_CARTILAGE_SURFACE ||
			  m_eEntRole == ENT_ROLE_INNER_SURFACE_JIGS ||
			  m_eEntRole == ENT_ROLE_OUTER_SURFACE_JIGS ||
			  m_eEntRole == ENT_ROLE_ANTERIOR_SURFACE_JIGS) )
        {
			if ( m_eDisplayMode == DISP_MODE_WIREFRAME || 
				 m_eDisplayMode == DISP_MODE_DISPLAY_NET || 
				 m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )
			{
				if( m_bModified && m_glIsoCurveList > 0 )
				{
					DeleteDisplayList( m_glIsoCurveList );
					DeleteDisplayList( m_glIsoCurveListWithNormal );
				}

				glColor4ub( m_color[0], m_color[1], m_color[2], m_pDoc->GetTransparencyFactor());

				if( m_glIsoCurveList == 0 )
				{
					m_glIsoCurveList = NewDisplayList();
					glNewList( m_glIsoCurveList, GL_COMPILE_AND_EXECUTE );
					DisplayIsoCurves(false);
					glEndList();

					m_glIsoCurveListWithNormal = NewDisplayList();
					glNewList( m_glIsoCurveListWithNormal, GL_COMPILE_AND_EXECUTE );
					DisplayIsoCurves(true);
					glEndList();
				}
				else
				{
					if ( m_eDisplayMode == DISP_MODE_WIREFRAME )
						glCallList( m_glIsoCurveListWithNormal );
					else
						glCallList( m_glIsoCurveList );
				}
			}
		}

		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
			 m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
		}

	}

	SetModifiedFlag(false);
}

void CPart::DisplayFaces()
{
	int			nFacets, iFacet;
	IwPoint3d	pts[3], pt;
	IwVector3d	norms[3], norm;

	if (m_pBrep == NULL) return;

	glEnable( GL_COLOR_MATERIAL );
	glColorMaterial( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );

	nFacets = m_Facets.count();

	if ( nFacets == 0 )
	{
		MakeTess();
		nFacets = m_Facets.count();
	}

   if (nFacets > 0)
   {
	   glBegin( GL_TRIANGLES );
	
	   for( iFacet = 0; iFacet < nFacets; iFacet++ )
	   {
		   CFacet&		f = m_Facets[iFacet];
		
		   if( m_ePartType == PART_TYPE_STL )
		   {
			   norm = NormalizedCross( f.points[1] - f.points[0], f.points[2] - f.points[1] );
			   glNormal3d( norm.x, norm.y, norm.z );

			   glVertex3d( f.points[0].x, f.points[0].y, f.points[0].z );
			   glVertex3d( f.points[1].x, f.points[1].y, f.points[1].z );
			   glVertex3d( f.points[2].x, f.points[2].y, f.points[2].z );
		   }
		   else
		   {
			   glNormal3d( f.normals[0].x, f.normals[0].y, f.normals[0].z);
			   glVertex3d( f.points[0].x, f.points[0].y, f.points[0].z);

			   glNormal3d( f.normals[1].x, f.normals[1].y, f.normals[1].z );
			   glVertex3d( f.points[1].x, f.points[1].y, f.points[1].z );

			   glNormal3d( f.normals[2].x, f.normals[2].y, f.normals[2].z );
			   glVertex3d( f.points[2].x, f.points[2].y, f.points[2].z );
		   }
	   }

	   glEnd();
   }
}

void CPart::DisplayEdges()
{
	IwPoint3d	pt;
	CColor		color;
	CColor		black( 0, 0, 0 );
	CColor		red( 255, 0, 0 );

	if (m_pBrep == NULL) return;

    IW_PTR_ARRAY( sEdges, IwEdge, 256 );
    
	m_pBrep->GetEdges( sEdges );
	
	IwPoint3d sPData[64];
	
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glLineWidth(1.0);

	for( ULONG i = 0; i < sEdges.GetSize(); i++ ) 
	{
		IwEdge *pEdge = sEdges[i];

		// Do not display the smooth inner edges for femur/femoraCuts/osteophyte 
		if ( (m_eEntRole == ENT_ROLE_FEMUR ||
			  m_eEntRole == ENT_ROLE_FEMORAL_CUTS ||
			  m_eEntRole == ENT_ROLE_OSTEOPHYTE_SURFACE) &&
			pEdge->IsTangentEdge(10.0) )
			continue;

		if( pEdge->IsSeam() )
			continue;

		glColor4ub( color[0], color[1], color[2], m_pDoc->GetTransparencyFactor());
		
		IwCurve *pCurve = pEdge->GetCurve();

		IwExtent1d sIvl = pEdge->GetInterval();

		double	dChordHeightTol = 0.03;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.00001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}
}

//////////////////////////////////////////////////////////////////////////////
// So far only osteophyte surface, cartilage surface, and mergaed surface
// are qualified to display iso-curve.
//////////////////////////////////////////////////////////////////////////////
void CPart::DisplayIsoCurves(bool useNormal)
{
	IwPoint3d	pt;

	if (m_pBrep == NULL) return;

    IW_PTR_ARRAY( sFaces, IwFace, 32 );
    
	m_pBrep->GetFaces( sFaces );
	
	IwPoint3d sPData[64];
	
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glLineWidth(1.0);

	for( ULONG i = 0; i < sFaces.GetSize(); i++ ) 
	{
		IwFace *pFace = sFaces[i];
		IwBSplineSurface *pSurface = (IwBSplineSurface*)pFace->GetSurface();

        IwTArray<double> sUKnots;
        IwTArray<double> sVKnots;
        pSurface->GetKnots(IW_SP_U,sUKnots);
        pSurface->GetKnots(IW_SP_V,sVKnots);
		
		double u, v;
		IwPoint3d pt;
		IwVector3d normal;
		// Display each iso curve
		for ( ULONG j=0; j<sUKnots.GetSize(); j++)
		{
			u = sUKnots.GetAt(j);

			glBegin( GL_LINE_STRIP );

				for( ULONG k = 0; k < sVKnots.GetSize(); k++ ) 
				{
					v = sVKnots.GetAt(k);
					pSurface->EvaluatePoint(IwPoint2d(u,v), pt);
					glVertex3d( pt.x, pt.y, pt.z );
					if ( useNormal )
					{
						pSurface->EvaluateNormal(IwPoint2d(u,v), FALSE, FALSE, normal);
						glNormal3d( normal.x, normal.y, normal.z );
					}
				}

			glEnd();
		}
		// Display each iso curve in the other direction
		for ( ULONG j=0; j<sVKnots.GetSize(); j++)
		{
			v = sVKnots.GetAt(j);

			glBegin( GL_LINE_STRIP );

				for( ULONG k = 0; k < sUKnots.GetSize(); k++ ) 
				{
					u = sUKnots.GetAt(k);
					pSurface->EvaluatePoint(IwPoint2d(u,v), pt);
					glVertex3d( pt.x, pt.y, pt.z );
					if ( useNormal )
					{
						pSurface->EvaluateNormal(IwPoint2d(u,v), FALSE, FALSE, normal);
						glNormal3d( normal.x, normal.y, normal.z );
					}
				}

			glEnd();
		}
	}
}	

void CPart::DisplayCurves()
{
	IwPoint3d	pt;

	if (m_curves.GetSize() == 0) return;

	glLineWidth(2.0);

	glEnable( GL_COLOR_MATERIAL );

	glColorMaterial( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );

    IW_PTR_ARRAY( sEdges, IwEdge, 256 );
    	
	IwPoint3d sPData[64];
	
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	for( ULONG i = 0; i < m_curves.GetSize(); i++ ) 
	{
		
		IwCurve *pCurve = m_curves.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.05;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

		glLineWidth(1.0);

}	

void CPart::DisplayPoints()
{
	if (m_points.GetSize() == 0) return;

	glPointSize(7);

	glDisable( GL_LIGHTING );

	IwPoint3d pnt;
	glBegin( GL_POINTS );
		for (unsigned i=0; i<m_points.GetSize(); i++)
		{
			pnt = m_points.GetAt(i);
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}
	glEnd();

	glEnable( GL_LIGHTING );

	glPointSize(1);
}

////////////////////////////////////////////////////
// This is a function with a very specific purpose.
// Caller should know what he is doing.
////////////////////////////////////////////////////
void CPart::GenerateIsoCurvesOnlyDisplayList()
{
	// Display iso curves
	if( m_bModified && m_glIsoCurveList > 0 )
		DeleteDisplayList( m_glIsoCurveList );

	if( m_glIsoCurveList == 0 )
	{
		m_glIsoCurveList = NewDisplayList();
		glNewList( m_glIsoCurveList, GL_COMPILE_AND_EXECUTE );
		DisplayIsoCurves();
		glEndList();
	}	
}

CBounds3d CPart::GetBounds()
{
	return m_Bounds;
}

	
void CPart::UpdateBounds( CBounds3d& Bounds )
{
	m_Bounds.Expand( Bounds );
}


void CPart::UpdateBounds( IwPoint3d& pt )
{
	m_Bounds.Expand( pt );
}


void CPart::AddFacet( CFacet& Facet )
{
	m_Facets.append( Facet );
}

int CPart::GetNumFacets()
{
	return m_Facets.count();
}


void CPart::GetFacetPoints( int iFacet, IwPoint3d pts[3] )
{
	CFacet&		Facet = m_Facets[iFacet];

	pts[0] = Facet.points[0];
	pts[1] = Facet.points[1];
	pts[2] = Facet.points[2];
}


CBounds3d CPart::ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds )
{
	int			i, nFacets, iFacet;
	CPoint3d	pt;
	CBounds3d	Bounds;

	nFacets = m_Facets.count();

	// Determine the Bounding box for Brep
	for( iFacet = 0; iFacet < nFacets; iFacet++ )
	{
		CFacet&		f = m_Facets[iFacet];

		for( i = 0; i < 3; i++ )
		{
			CPoint3d&	p = (CPoint3d)f.points[i];

			if( pClipBounds && !pClipBounds->Contains( p ) )
				continue;

			pt = Trf * p;

			Bounds.Expand( pt );
		}
	}
	// Expand the Bounding box for curves
	IwExtent3d crvBounds;
	for (unsigned i=0; i<m_curves.GetSize(); i++)
	{
		IwCurve* crv = m_curves.GetAt(i);
		crv->CalculateBoundingBox(crv->GetNaturalInterval(), &crvBounds);
		CPoint3d minPt = crvBounds.GetMin();
		pt = Trf * minPt;
		Bounds.Expand( pt );
		CPoint3d maxPt = crvBounds.GetMax();
		pt = Trf * maxPt;
		Bounds.Expand( pt );
	}
	// Expand the bounding box for points
	for (unsigned i=0; i<m_points.GetSize(); i++)
	{
		CPoint3d p = m_points.GetAt(i);
		pt = Trf * p;
		Bounds.Expand( pt );
	}

	return Bounds;
}


bool CPart::WriteTess( const QString& sFileName )
{
	int			nFacets, iFacet;
	QFile		File( sFileName );
	bool		bOK;

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	nFacets = m_Facets.count();

	File.write( (char*) &nFacets, sizeof(int) );

	for( iFacet = 0; iFacet < nFacets; iFacet++ )
	{
		CFacet&		f = m_Facets[iFacet];

		int n = File.write( (char*) &f, sizeof(CFacet) );
		int a = 1;
	}

	File.close();

	return true;
}


bool CPart::ReadTess( const QString& sFileName )
{
	QFile		File( sFileName );
	CFacet		f;
	int			nFacets, iFacet;
	bool		bOK;

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	File.read( (char*) &nFacets, sizeof(int) );

	m_Facets.clear();

	m_Bounds.Reset();

	for( iFacet = 0; iFacet < nFacets; iFacet++ )
	{
		File.read( (char*) &f, sizeof(CFacet) );

		m_Facets.append( f );

		UpdateBounds( f.points[0] );
		UpdateBounds( f.points[1] );
		UpdateBounds( f.points[2] );
	}

	File.close();

	return true;
}


void CPart::MakeTess(bool updateView)
{
	// Tessellation is only for Brep. If m_pBrep does not exist, clear everything and return;
	if ( !m_pBrep )
	{
		m_pDoc->AppendLog( QString("CPart::MakeTess(), m_pBrep == NULL ") );
		ClearTess();

		if ( updateView )
			SetModifiedFlag(true);

		return;
	}

	IwContext&		iwContext = m_pDoc->GetIwContext();
	IwTArray<IwFace*> faces;
	m_pBrep->GetFaces(faces);
    IwBoolean		bFailedFaces;

	double  tessFactor = m_pDoc->GetTessellationFactor();

#ifndef SM_VERSION_STRING
	// Based onthe default values of SMLib 8.6.1
	ULONG lMinimumNumberOfSegments = 0;
	double dChordHeight = 0.0;
	double dAngleToleranceDegrees = 8.0;
	double dMaximum3DDistanceBetweenPoints = 0.0;
	double dMinimumParametricRatio = 0.00001;
#else
	// Based on the values recommended by SMLib support
	ULONG lMinimumNumberOfSegments = 0;
	double dChordHeight = 0.1;
	double dAngleToleranceDegrees = 0;
	double dMaximum3DDistanceBetweenPoints = 0;
	double dMinimumParametricRatio = 0;
#endif
	IwBoolean bEvaluatorBasedTessellation = FALSE;
	IwCurveTessDriver		sCrvTess = IwCurveTessDriver(lMinimumNumberOfSegments, dChordHeight, dAngleToleranceDegrees, dMaximum3DDistanceBetweenPoints, dMinimumParametricRatio, bEvaluatorBasedTessellation);

#ifndef SM_VERSION_STRING
	// Based on the default values of SMLib 8.6.1
	double dChordHeightSurface    = 0.1*tessFactor;
	double dAngleToleranceDegreesSurface = 0.0;
	double dMaxEdgeLength3D       = 0.0;
	double dMinEdgeLength3D       = 0.0;
	double dMinEdgeLengthRatioUV  = 0.0;												
	double dMaxAspectRatio        = 0.0;
#else
	// Based on the values recommended by SMLib support
	double dChordHeightSurface    = 0.1*tessFactor;
	double dAngleToleranceDegreesSurface = 0;
	double dMaxEdgeLength3D       = 0;
	double dMinEdgeLength3D       = 0;
	double dMinEdgeLengthRatioUV  = 0;												
	double dMaxAspectRatio        = 0;
#endif
	IwSurfaceTessDriver		sSrfTess = IwSurfaceTessDriver(dChordHeightSurface, dAngleToleranceDegreesSurface, dMaxEdgeLength3D, dMinEdgeLength3D, dMinEdgeLengthRatioUV, dMaxAspectRatio);
	
    IwTess iwTess( iwContext, sCrvTess, sSrfTess );
      
    // Note that the Brep sent into the Tessellator gets modified
    // so you usually will want to make a copy of it is follows:
	IwBrep *pBrepCopy = new ( iwContext ) IwBrep( *m_pBrep );

    // Now do the tessellation process to produce triangles in UV space of the surfaces.
    iwTess.DoTessellation( pBrepCopy, bFailedFaces );  
	IW_ASSERT( bFailedFaces == false );

	ClearTess();

   	CTess		Tess( this );

	iwTess.OutputPolygons( Tess );

	if ( updateView )
		SetModifiedFlag(true);

    // Here we are done.  Note that if you declare IwTess on the stack
    // it will automatically clean up the pCopy IwBrep and all of it's internal
    // structures when it goes out of scope.
	if (pBrepCopy) IwObjDelete(pBrepCopy);
}


bool CPart::Save( const QString& sDir, bool incrementalSave )
{
	QString				sBrepFileName;
	bool				bOK = true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bDataModified ) 
		return bOK;

	// Save Brep		
	sBrepFileName = sDir + "\\" + m_sFileName + ".iwp~";
	// No need to save ".tess" file. Too big.
	//sTessFileName = sDir + "\\" + m_sFileName + ".tess";

	bool brepOK = false;
	if ( m_pBrep ) 
	{
		brepOK = WriteBrep( m_pBrep, sBrepFileName );;
	}
	else // if m_pBrep==NULL, remove the existing file.
	{
		QString previousFileName = sDir + "\\" + m_sFileName + ".iwp";
		QFile previousFile(previousFileName);
		if ( previousFile.exists() )
			previousFile.remove();
	}

	bool curveOK = false;
	QString				sCurvesFileName;
	sCurvesFileName = sDir + "\\" + m_sFileName + ".crv~";
	if ( m_curves.GetSize() > 0 )
	{
		curveOK = WriteCurves( m_curves, sCurvesFileName );
	}

	bool pointOK = false;
	QString				sPointsFileName;
	sPointsFileName = sDir + "\\" + m_sFileName + "_pt.dat~";
	unsigned nSize = m_points.GetSize();
	if ( nSize > 0 )
	{
		QFile				File( sPointsFileName );

		pointOK = File.open( QIODevice::WriteOnly );

		if( pointOK )
		{
			File.write( (char*) &nSize, sizeof( int ) );

			for (unsigned i=0; i<nSize; i++)
				File.write( (char*) &m_points.GetAt(i), sizeof( IwPoint3d ) );
		}
	}

	m_bDataModified = false;

	// As long as some data is saved, it returns true.
	bOK = brepOK || curveOK || pointOK;

	return bOK;

}


bool CPart::Load( const QString& sDir )
{
	IwContext&			iwContext = m_pDoc->GetIwContext();
	QString				sBrepFileName;
	bool				bOK = true;

	// Variables for TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{

		sBrepFileName = sDir + QDir::separator() + m_sFileName + ".iwp";

		bool brepOK = false;
		m_pBrep = ReadBrep( sBrepFileName );

		if( m_pBrep )
		{
			brepOK = true;
			MakeTess();
		}

		bool curveOK = false;
		QString				sCurvesFileName;
		sCurvesFileName = sDir + QDir::separator() + m_sFileName + ".crv";
		CCurveArray			sCurves;
		curveOK = ReadCurves( sCurvesFileName, sCurves );
		for (unsigned i=0; i<sCurves.GetSize(); i++)
		{
			IwCurve* crv = sCurves.GetAt(i);
			m_curves.Add(crv);
			// Expand Bounds
			IwExtent3d box;
			crv->CalculateBoundingBox(crv->GetNaturalInterval(), &box);
			m_Bounds.Expand(box.GetMax());
			m_Bounds.Expand(box.GetMin());
		}

		bool pointOK = false;
		QString				sPointsFileName;
		sPointsFileName = sDir + QDir::separator() + m_sFileName + "_pt.dat";
		QFile				File( sPointsFileName );
		pointOK = File.open( QIODevice::ReadOnly );
		int nSize;
		if( pointOK )
		{
			File.read( (char*) &nSize, sizeof( int ) );
			IwPoint3d pnt;
			m_points.RemoveAll();
			for (int i=0; i<nSize; i++)
			{
				File.read( (char*) &pnt, sizeof( IwPoint3d ) );
				m_points.Add(pnt);
				m_Bounds.Expand(pnt);
			}
		}

		bOK = brepOK || curveOK || pointOK; // As long as some data are loaded, it returns true;
	}


	m_bDataModified = false;

	return bOK;
}


void CPart::ClearTess()
{
	m_Facets.clear();

	if( m_glFaceList )
		DeleteDisplayList( m_glFaceList );

	if( m_glEdgeList )
		DeleteDisplayList( m_glEdgeList );

    if( m_glIsoCurveList )
		DeleteDisplayList( m_glIsoCurveList );

	if( m_glIsoCurveListWithNormal )
		DeleteDisplayList( m_glIsoCurveListWithNormal );
}

bool CPart::ExportSTL( const QString& sFileName )
{
	QFile			File( sFileName );
	char			head[80];
	const int		MAX_FACETS = 10000;
	int				nFacetsInBuffer;
	CSTLFacet		STLFacetBuffer[ MAX_FACETS ];
	CSTLFacet*		pSTLFacet;
	int				i, iFacet, nFacets;
	CVector3d		nm;
	bool			bOK;

	nFacets = m_Facets.count();

	nFacetsInBuffer = 0;
	
	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;
	
    memset(head, ' ', 80);
	File.write( (char*) head, 80 );
	
	File.write( (char*) &nFacets, 4 );

	for( iFacet = 0; iFacet < nFacets; iFacet++ )
	{
		CFacet&		f = m_Facets[iFacet];

		pSTLFacet = STLFacetBuffer + nFacetsInBuffer;

		for( i = 0; i < 3; i++ )
		{
			pSTLFacet->pt[i].x = (float) f.points[i][0];
			pSTLFacet->pt[i].y = (float) f.points[i][1];
			pSTLFacet->pt[i].z = (float) f.points[i][2];
		}

		nm = NormalizedCross( f.points[1] - f.points[0], f.points[2] - f.points[1] );

		pSTLFacet->norm.x = (float) nm[0];
		pSTLFacet->norm.y = (float) nm[1];
		pSTLFacet->norm.z = (float) nm[2];

		nFacetsInBuffer++;

		if( nFacetsInBuffer == MAX_FACETS )
		{
			File.write( (char*) STLFacetBuffer, nFacetsInBuffer * 50 );
			nFacetsInBuffer = 0;
		}
	}

	File.write( (char*) STLFacetBuffer, nFacetsInBuffer * 50 );
	File.close();

	return true;
}

void CPart::ExportIGES( const QString& sFileName )
{
	CBrepArray				BrepTrimmedSurfArray;
	char					filename[2048];
	CPoint3d				pt;

	if (m_pBrep)
		BrepTrimmedSurfArray.Add( m_pBrep );

	QStringToCharArray( sFileName, filename );

	// Regardless the m_pBrep is a solid Brep or a trimmed shell,
	// Save it a trimmed surfaces.
	WriteIges( filename, NULL, NULL, NULL, &BrepTrimmedSurfArray, NULL );	
}

void CPart::GetSelectableEntities
(
	IwTArray<IwBrep*>& breps,		// O:
	IwTArray<IwCurve*>& curves,		// O: 
	IwTArray<IwPoint3d>& points		// O:
)
{

	if (m_pBrep)
	{
		breps.Add(m_pBrep);
	}
	if (m_curves.GetSize() > 0)
	{
		curves.Append(m_curves);
	}
	if (m_points.GetSize() > 0)
	{
		points.Append(m_points);
	}

}