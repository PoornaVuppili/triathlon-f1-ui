#include "Measure.h"
#include "..\KUtility\SMLibInterface.h"
#include "..\KUtility\KStatusIndicator.h"

MeasureResult MeasureResult::ComputeEntityRadius(MeasureEntity& e0)
{
    MeasureResult r;
    switch( e0.selectType )
    {
    case SelectCircle:
        r.mode = EntityRadius;
        r.value.radius.radius = e0.selectValue.circle.rad;
        r.value.radius.xc = e0.selectValue.circle.x;
        r.value.radius.yc = e0.selectValue.circle.y;
        r.value.radius.zc = e0.selectValue.circle.z;
        r.value.radius.nx = e0.selectValue.circle.nx;
        r.value.radius.ny = e0.selectValue.circle.ny;
        r.value.radius.nz = e0.selectValue.circle.nz;
        r.valid = true;
        break;

    case SelectCurve:
    case SelectEdge :
        {
            IwBSplineCurve *crv;
            double hitU;
            if(e0.selectType == SelectCurve)
            {
                crv = (IwBSplineCurve*) e0.selectValue.curve.curve;
                hitU = e0.selectValue.curve.hitU;
            }
            else
            {
                crv = (IwBSplineCurve*) e0.selectValue.edge.edge->GetCurve();
                hitU = e0.selectValue.edge.hitU;
            }
            // Determine the curve length
			IwExtent1d dom = crv->GetNaturalInterval();
			double curveLength;
			crv->Length(dom, 0.01, curveLength);
			// determine the curvature at the picked point
			IwVector3d gVectors[3];
			crv->EvaluateGeometric(hitU, 2, FALSE, gVectors);
			IwPoint3d pos = gVectors[0];
			IwVector3d tangVec = gVectors[1];
			IwVector3d curvVec = gVectors[2];
			double length, radius=0.0;
			length = curvVec.Length();
            if ( length >= 0.000001 )
			{
				radius = 1.0/length;
				tangVec.Unitize();
				curvVec.Unitize();
				IwPoint3d center = pos + radius*curvVec;
                IwVector3d nor = tangVec * curvVec;
                r.mode = EntityRadius;
                r.value.radius.radius = radius;
                r.value.radius.xc = center.x;
                r.value.radius.yc = center.y;
                r.value.radius.zc = center.z;
                r.value.radius.nx = nor.x;
                r.value.radius.ny = nor.y;
                r.value.radius.nz = nor.z;
                r.valid = true;
			}
            else
                kStatus.ShowMessage("Warning: a very large radius is found");
        }
        break;

    default:
        kStatus.ShowMessage("Error: unsupported entity type");
        break;
    }
    return r;
}
