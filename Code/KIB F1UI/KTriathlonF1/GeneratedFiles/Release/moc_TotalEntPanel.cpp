/****************************************************************************
** Meta object code from reading C++ file 'TotalEntPanel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../TotalEntPanel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TotalEntPanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CTotalEntPanel_t {
    QByteArrayData data[8];
    char stringdata0[134];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CTotalEntPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CTotalEntPanel_t qt_meta_stringdata_CTotalEntPanel = {
    {
QT_MOC_LITERAL(0, 0, 14), // "CTotalEntPanel"
QT_MOC_LITERAL(1, 15, 10), // "OnRedefine"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 17), // "OnRegenerateAStep"
QT_MOC_LITERAL(4, 45, 25), // "OnRegenerateMultipleSteps"
QT_MOC_LITERAL(5, 71, 19), // "OnSubdivMeshChanged"
QT_MOC_LITERAL(6, 91, 20), // "OnSketchSliceChanged"
QT_MOC_LITERAL(7, 112, 21) // "OnExportIges3Surfaces"

    },
    "CTotalEntPanel\0OnRedefine\0\0OnRegenerateAStep\0"
    "OnRegenerateMultipleSteps\0OnSubdivMeshChanged\0"
    "OnSketchSliceChanged\0OnExportIges3Surfaces"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CTotalEntPanel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x0a /* Public */,
       3,    0,   45,    2, 0x0a /* Public */,
       4,    0,   46,    2, 0x0a /* Public */,
       5,    1,   47,    2, 0x0a /* Public */,
       6,    1,   50,    2, 0x0a /* Public */,
       7,    0,   53,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

       0        // eod
};

void CTotalEntPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CTotalEntPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnRedefine(); break;
        case 1: _t->OnRegenerateAStep(); break;
        case 2: _t->OnRegenerateMultipleSteps(); break;
        case 3: _t->OnSubdivMeshChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->OnSketchSliceChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->OnExportIges3Surfaces(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CTotalEntPanel::staticMetaObject = { {
    QMetaObject::SuperData::link<CEntPanel::staticMetaObject>(),
    qt_meta_stringdata_CTotalEntPanel.data,
    qt_meta_data_CTotalEntPanel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CTotalEntPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CTotalEntPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CTotalEntPanel.stringdata0))
        return static_cast<void*>(this);
    return CEntPanel::qt_metacast(_clname);
}

int CTotalEntPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CEntPanel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
