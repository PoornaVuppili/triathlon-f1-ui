#version 130

out vec4 vFragColor;

smooth in vec3 vTex;

uniform vec4		vColor;
uniform sampler3D	dicom;
uniform float		threshold;
uniform ivec3		vTexSize;
uniform vec3		vViewDir;

void traverse(out vec3 totalVec, out vec4 texColor);

void main(void)
{ 
	vec4 texColor;
	vec3 totalVec;

	traverse(totalVec, texColor);

	vFragColor = texColor * vColor;
	gl_FragDepth += length(totalVec);

    // Dot product gives us diffuse intensity
    //float diff = max(0.0, dot(normalize(vVaryingNormal), normalize(vVaryingLightDir)));
    //vFragColor *= diff;

    // Add in ambient light
    //vFragColor += vec4(.01f, .01f, .01f, 1.0f);

    // Specular Light
    //vec3 vReflection = normalize(reflect(-normalize(vVaryingLightDir), normalize(vVaryingNormal)));
    //float spec = max(0.0, dot(normalize(vVaryingNormal), vReflection));
    //if(diff != 0)
	//{
    //    float fSpec = pow(spec, 128.0);
    //    vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
	//}
}

void traverse(out vec3 totalVec, out vec4 texColor)
{
	vec3 invTexSize = 1/vec3(vTexSize);
	float testValue;

	vec3 Pd;

	vec3 dir = vec3(normalize(vViewDir));
	ivec3 P1 = ivec3(vTex*vTexSize);
	ivec3 P2 = ivec3(P1 + 2000*dir);

	vec3 d = P2 - P1;
	ivec3 a = ivec3(abs(d)*2);
	ivec3 s = ivec3(sign(d));

	ivec3 sample = P1;

	if( a.x >= max(a.y, a.z) )
	{
		Pd.y = a.y - a.x/2;
		Pd.z = a.z - a.x/2;
		
		while(sample.x >= 0 && sample.x <= vTexSize.x)
		{
			if(Pd.y >= 0)
			{
				sample.y += s.y;
				Pd.y -= a.x;
			}

			if(Pd.z >= 0)
			{
				sample.z += s.z;
				Pd.z -= a.x;
			}

			sample.x += s.x;
			Pd.y += a.y;
			Pd.z += a.z;

			texColor = texture(dicom, vec3(sample*invTexSize));
			testValue = (texColor.r + texColor.g + texColor.b)/3;

			if( threshold < testValue)
				break;
		} 
	}
	else if(a.y >= max(a.x, a.z) )
	{
		Pd.x = a.x - a.y/2;
		Pd.z = a.z - a.y/2;

		while(sample.y >= 0 && sample.y <= vTexSize.y)
		{
			if( Pd.x >= 0)
			{
				sample.x += s.x;
				Pd.x -= a.y;
			}

			if(Pd.z >= 0)
			{
				sample.z += s.z;
				Pd.z -= a.y;
			}

			sample.y += s.y;
			Pd.x += a.x;
			Pd.z += a.z;

			texColor = texture(dicom, vec3(sample*invTexSize));
			testValue = (texColor.r + texColor.g + texColor.b)/3;

			if( threshold < testValue)
				break;
		}
	}
	else if(a.z >= max(a.x, a.y) )
	{
		Pd.x = a.x - a.z/2;
		Pd.y = a.y - a.z/2;

		while(sample.z >= 0 && sample.z <= vTexSize.z)
		{
			if(Pd.x >= 0)
			{
				sample.x += s.x;
				Pd.x -= a.z;
			}

			if(Pd.y >= 0)
			{
				sample.y += s.y;
				Pd.y -= a.z;
			}

			sample.z += s.z;
			Pd.x += a.x;
			Pd.y += a.y;

			texColor = texture(dicom, vec3(sample*invTexSize));
			testValue = (texColor.r + texColor.g + texColor.b)/3;

			if( threshold < testValue)
				break;
		}
	}

	if( threshold > testValue)
		discard;

	totalVec = (sample - P1)*invTexSize;
}
