#pragma once

#include <QtGui>
#pragma warning( disable : 4996 4805 )
#include <HwTranslatorGeneric.h>
#pragma warning( default : 4996 4805 )

class CProgressWidget;

class CIgesTracker : public HwProgressTrackerInterface
	{
public:
	CIgesTracker( CProgressWidget* pProgressWidget );

    virtual IwStatus Heartbeat();
    virtual IwStatus StartCountdown( unsigned int count );
    virtual IwStatus Tick ();
    virtual IwStatus EndCountdown ();

	int						m_iCount;
	int						m_nCount;
	CProgressWidget*		m_pProgressWidget;
	};
