#pragma once

#include <qobject.h>
#include <qlist.h>

class SwitchBoard : QObject
{
	Q_OBJECT

public:
    static SwitchBoard& getInstance();

	static void addSignalSender(const char *signal, QObject *sender);
	static void removeSignalSender(QObject *sender);

	static void addSignalReceiver(const char *signal, QObject *receiver, const char *slot);
	static void removeSignalReceiver(QObject *receiver);

private:

	//Methods
    SwitchBoard();
	~SwitchBoard();

    SwitchBoard(SwitchBoard const&);
    void operator=(SwitchBoard const&);

	void makeConnections();

	//Data structures
	class Receiver
	{
	public:
		Receiver(const char *sig, QObject *o, const char *s) : signal(sig), obj(o), slot(s) {};

		const char *signal;
		QObject *obj;
		const char *slot;
	};

	class Sender
	{
	public:
		Sender(const char *sig, QObject *o) : signal(sig), obj(o) {};

		const char *signal;
		QObject *obj;
	};

	QList<Sender *> senders;
	QList<Receiver *> receivers;
};
