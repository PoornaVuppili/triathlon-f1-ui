#pragma once

#include <qevent.h>

const QEvent::Type CLOSE_IMPLANT_EVENT		= static_cast<QEvent::Type>(QEvent::User + 100);
