#include "DataDoc.h"
#include "Part.h"
#include "OpenGLView.h"
#include "MainWindow.h"
#include "Globals.h"
#include "DocReader.h"
#include "DocWriter.h"
#include "DocLogWriter.h"
#include "EntPanel.h"
#include "IgesReader.h"
#include "Utilities.h"
#include "ImplantHeaderDlg.h"
#include "PartTessThread.h"
#include "..\KUtility\KUtility.h"
#include "..\KApp\EntityWidget.h"
#include "..\KApp\Implant.h"
#include "..\KApp\MainWindow.h"

#include <QMessageBox>

FILETIME ValidationReport::s_start;
LARGE_INTEGER ValidationReport::s_startCPUcounter;
bool ValidationReport::s_timeInit = ValidationReport::StartCounting();

CDataDoc::CDataDoc(QObject *parent)
{
	m_bModified			= false;
	m_bReadOnlyFile		= false;
	m_bFileSaveAuto		= false;
	m_pView				= NULL;
	m_pEntPanel			= NULL;
	m_nAvlEntId			= 1;

	m_sPatientId		= QString("");
	m_eImplantSide		= IMPLANT_SIDE_UNKNOWN;
	m_eImplantZone		= IMPLANT_ZONE_UNKNOWN;

	m_openingFileReleaseNumber	= -1;
	m_openingFileRevisionNumber = -1;
	m_openingFileBuildNumber = -1;

	m_tessellationFactor = 0.33;
	m_transparencyFactor = 64;

	// Get the user info
	char acUserName[100];
	DWORD nUserName = sizeof(acUserName);
	GetUserName(acUserName, &nUserName);
	m_currentUserName = QString(acUserName);
	m_currentUserInfo = QString("");
	char acComputerName[100];
	DWORD nComputerName = sizeof(acComputerName);
	GetComputerName(acComputerName, &nComputerName);
	m_currentComputerName = QString(acComputerName);

	vectOfFieldsUsed.clear();

}


CDataDoc::~CDataDoc()
{
}

void CDataDoc::ToggleEntPanelItemByID(int id, bool On)
{
	m_pEntPanel->ToggleById(id, On);
}

void CDataDoc::AddEntity( CEntity* pEntity, bool bAddToEntPanel )
{
	if( !EntityExists( pEntity ) )
		m_EntList.append( pEntity );

	if( bAddToEntPanel )
	{
		int				id = pEntity->GetEntId();
		QString			sName = pEntity->GetEntName();
		CEntRole		eRole = pEntity->GetEntRole();

		m_pEntPanel->Add( id, sName, eRole );
	}

	m_bModified = true;
}


void CDataDoc::AddPart( CPart* pPart, bool bAddToEntPanel )
{
	AddEntity( pPart, bAddToEntPanel );

	CBounds3d	Bounds = pPart->ComputeBounds(CTransform());
	// use CTransform() is actually not right. The transformation is in OpenGLView.
	// Here we play a trick. If users could not see the entire part, they may click on "Fit Screen".
	// Then CDataDoc::ComputeBounds() will return the right bound.

	m_Bounds.Expand( Bounds );
}


bool CDataDoc::EntityExists( CEntity* pEntity )
{
	int					iEnt, nEnts;

	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*	pEnt = m_EntList[iEnt];

		if( pEnt == pEntity )
			return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////
// This function creates CPart object.
///////////////////////////////////////////////////////////////////////////
bool CDataDoc::ImportCPart( const QString& sFileName, CEntRole eEntRole )
{
	CColor		color( 255, 255, 255 );
	CPart*		pPart;
	bool		bOK;
	
	pPart = new CPart( this, eEntRole, sFileName );
	bOK = pPart->ImportPart( sFileName );

	if( !bOK )
		return false;

	pPart->SetColor( color );

	AddPart( pPart );

	return true;
}

bool CDataDoc::ImportBrep( const QString& sFileName, const CColor& color )
{
	CPart*		pPart = new CPart( this, ENT_ROLE_NONE );

	bool		bOK = pPart->ImportBrep( sFileName );

	pPart->SetColor( color );

	if( bOK )
		{
		AddPart( pPart );
		}

	return bOK;
}

int CDataDoc::AddPreviousUserInfo(QString& prevUserInfo)
{

	m_previousUserInfo.append(prevUserInfo);
	int previousUsersNo = m_previousUserInfo.count();

	return previousUsersNo;
}


int CDataDoc::GetPreviousUserInfo(QList<QString>& prevUserInfo)
{
	prevUserInfo.append(m_previousUserInfo);
	int no = prevUserInfo.count();

	return no;
}

QString CDataDoc::GetPatientId()
{
	m_sPatientId = mainWindow->GetImplant()->GetInfo("ImplantName");
	return m_sPatientId;
}

CImplantSide CDataDoc::GetImplantSide( QString& sImplantSideName )
{
	if ( mainWindow->GetImplant()->GetInfo("KneeType") == "Left" )
	{
		m_eImplantSide = IMPLANT_SIDE_LEFT;
		sImplantSideName = "Left";
	}
	else if ( mainWindow->GetImplant()->GetInfo("KneeType") == "Right" )
	{
		m_eImplantSide = IMPLANT_SIDE_RIGHT;
		sImplantSideName = "Right";
	}

	return m_eImplantSide;
}

bool CDataDoc::IsPositiveSideLateral() // Right knee
{
	// Get side info
	QString sideInfo;
	CImplantSide implantSide = GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	return positiveSideIsLateral;
}

CImplantRegulatoryZone CDataDoc::GetImplantZone()
{
	QString zone = mainWindow->GetImplant()->GetInfo("Zone");
	if( zone == "US_FDA" )
		m_eImplantZone = IMPLANT_ZONE_US_FDA;
	else if( zone == "CE_MARK" )
		m_eImplantZone = IMPLANT_ZONE_CE_MARK;
	else if( zone == "UNKNOWN" )
		m_eImplantZone = IMPLANT_ZONE_UNKNOWN;
	else
		m_eImplantZone = IMPLANT_ZONE_NEW_ZONE;

	return m_eImplantZone;
}

int	CDataDoc::GetNumOfEntities(bool displayedOnly)
{
	int nCount = m_EntList.count();
	if ( displayedOnly == false )
		return nCount;

	int dispCount = 0;
	CEntity*		pEntity;
	CDisplayMode	eDispMode;

	for (int i=0; i<nCount; i++)
	{
		pEntity = m_EntList[i];
		eDispMode = pEntity->GetDisplayMode();
		if( eDispMode != DISP_MODE_HIDDEN && eDispMode != DISP_MODE_NONE ) // visible entity
		{
			dispCount++;
		}
	}

	return dispCount;
}


CEntity* CDataDoc::GetEntity( int iPart )
{
	return m_EntList[iPart];
}
	
CEntity* CDataDoc::GetLastEntity()
{
	int count = m_EntList.count();

	return GetEntity( count - 1 );
}

CEntity* CDataDoc::GetEntity( CEntRole eEntRole )
{
	int		iEnt, nEnts;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*	pEntity = m_EntList[iEnt];

		if( pEntity->GetEntRole() == eEntRole )
			return pEntity;
	}

	return NULL;
}


CEntity* CDataDoc::GetEntityById( int id )
{
	int		iEnt, nEnts;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		if( pEntity->GetEntId() == id )
			return pEntity;
	}

	return NULL;
}
	

CPart* CDataDoc::GetPart( CEntRole eEntRole )
{
	return (CPart*) GetEntity( eEntRole );
}


CPart* CDataDoc::GetPartById( int id )
{
	return (CPart*) GetEntityById( id );
}


// Display the opaque entities.
void CDataDoc::Display()
{
	int						iEnt, nEnts;
	
	nEnts = m_EntList.count();

	// Display the non-transparent entities first
	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];
		CDisplayMode	eDispMode = pEntity->GetDisplayMode();

		if( eDispMode == DISP_MODE_NONE || eDispMode == DISP_MODE_HIDDEN )
			continue;

		if (pEntity->GetDisplayMode() == DISP_MODE_TRANSPARENCY ||
			pEntity->GetDisplayMode() == DISP_MODE_TRANSPARENT_CT ||
			pEntity->GetDisplayMode() == DISP_MODE_TRANSPARENCY_NET)
			continue;

		pEntity->Display();
#ifndef NDEBUG
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
			AppendLog(QString("CDataDoc::Display(); GL Error after displaying: %1. Error number: %2").arg(pEntity->GetEntName()).arg(error));
#endif
	}

	DisplayTempPoints();

}

// Display the transparent entities.
void CDataDoc::DisplayTransparent()
{
    int						iEnt, nEnts;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];
		CDisplayMode	eDispMode = pEntity->GetDisplayMode();

		if( eDispMode == DISP_MODE_NONE || eDispMode == DISP_MODE_HIDDEN )
			continue;

		if (pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
			pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENT_CT &&
			pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY_NET)
			continue;

		pEntity->Display();
	}
}

CBounds3d CDataDoc::GetBounds()
{
	int				i, nEnts;
	CBounds3d		Bounds, bounds;

	nEnts = m_EntList.count();

	for( i = 0; i < nEnts; i++ )
	{
		CEntity*	pEntity = m_EntList[i];

		if( pEntity->GetDisplayMode() < DISP_MODE_WIREFRAME )
			continue;

		bounds = pEntity->GetBounds();
		Bounds.Expand( bounds );
	}

	return Bounds;
}


CBounds3d CDataDoc::ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds )
{
	int				i, nEnts;
	CBounds3d		Bounds, bounds;

	nEnts = m_EntList.count();

	for( i = 0; i < nEnts; i++ )
	{
		CEntity*	pEntity = m_EntList[i];

		if( pEntity->GetDisplayMode() < DISP_MODE_WIREFRAME )
			continue;

		bounds = pEntity->ComputeBounds( Trf, pClipBounds );

		Bounds.Expand( bounds );
	}

	int np = m_tempPoints.count();

	for( i = 0; i < np; i++ )
		Bounds.Expand( m_tempPoints[i].pt );

	return Bounds;
}


long CDataDoc::GetAvlDisplayList()
{
	return glGenLists(1);
}


IwContext& CDataDoc::GetIwContext()
{
	if ( iwContext == NULL )
		ResetContext();
	return *iwContext;
}

void CDataDoc::FileSaveAuto()
{
	// automactically save the file, if need
	bool bWarning = false;
	if ( this->IsFileSaveAuto() )
		m_pView->GetMainWindow()->OnFileSave(bWarning);

}

bool CDataDoc::IsEmpty()
{
	return m_EntList.count() == 0;
}


bool CDataDoc::IsModified()
{
	return m_bModified;
}

void CDataDoc::Clear()
{
	int			i, nEnts;
	nEnts = m_EntList.count();
	for( i = 0; i < nEnts; i++ )
	{
		delete m_EntList[i];
	}
	m_EntList.clear();

	m_tempPoints.clear();

	m_Bounds.Reset();

	entityWidget->RemoveItem(m_pEntPanel);

	m_pEntPanel->Clear();

	m_bModified			= false;
	m_bReadOnlyFile		= false;
	m_bFileSaveAuto		= false;
	m_nAvlEntId			= 1;

	m_sPatientId		= QString("");
	m_eImplantSide		= IMPLANT_SIDE_UNKNOWN;
	m_eImplantZone		= IMPLANT_ZONE_UNKNOWN;
	m_fileName			= QString("");
	m_currentUserInfo	= QString("");
	m_previousUserInfo.clear();
	m_validationReport.clear();

}


bool CDataDoc::GetImplantInfo( QString& sPatientId, CImplantSide& eImplantSide, CImplantRegulatoryZone& eImplantZone )
{
	sPatientId = m_sPatientId;
	eImplantSide = m_eImplantSide;
	eImplantZone = m_eImplantZone;

	if ( !sPatientId.isEmpty() && eImplantSide != IMPLANT_SIDE_UNKNOWN /*&& eImplantZone != IMPLANT_ZONE_UNKNOWN*/ ) // Zone info could be missing
	{
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////
// Now any modified files are first saved as "fileName~" with "~" in 
// the end. This post process function then removes "~" from the files.
// The reason to do so is because the saving processing is error-prone
// and may take a few minutes. If anything goes wrong, the file can
// not be opened anymore. By saving with "~" file names first, if 
// something goes wrong, at least users still have a chance to open 
// the previous saved files.
//////////////////////////////////////////////////////////////////////
void CDataDoc::SavePostProcessing( const QString& sDocFileName )
{
	QDir			dir( sDocFileName );
	if ( !dir.exists() ) return;
	
	int size = 0;
	QFileInfo fileInfo;
	QString   fileNameSaving, fileNameSaved;
	QFileInfoList fileInfoList = dir.entryInfoList();

	int fileNo = fileInfoList.size();
	for (int i=0; i<fileNo; i++)
	{
		fileInfo = fileInfoList.at(i);
		fileNameSaving = fileInfo.filePath(); // file name including path
		if ( fileNameSaving.endsWith("~") )
		{
			size = fileNameSaving.size();
			fileNameSaved = fileNameSaving.left(size-1);
			QFile fileSaved(fileNameSaved);
			if ( fileSaved.exists() )
				fileSaved.remove();
			QFile fileSaving(fileNameSaving);
			bool copied = fileSaving.copy(fileNameSaved);
			if ( copied )
				fileSaving.remove();
		}

	}

	// Make the header.txt hidden to prevent normal users opening/editing
	QString hearderName = sDocFileName + QDir::separator() + "header.txt";
	char filename[2048];
	QStringToCharArray( hearderName, filename );
	SetFileAttributes(filename, FILE_ATTRIBUTE_HIDDEN);

	return;
}

///////////////////////////////////////////////////////////////////
// Remove any file names ending with "~". See SavePostProcessing()
// comments.
///////////////////////////////////////////////////////////////////
bool CDataDoc::LoadPreProcessing( const QString& sDocFileName )
{
	bool			bPreviousSaveImcomplete = false;
	QDir			dir( sDocFileName );
	if ( !dir.exists() ) return bPreviousSaveImcomplete;
	
	QFileInfoList fileInfoList = dir.entryInfoList();

	///////////////////////////////////////////////////////
	// Check existance of any files ending with "~"
	QString fileName;
	QFileInfo fileInfo;
	int fileNo = fileInfoList.size();
	for (int i=0; i<fileNo; i++)
	{
		fileInfo = fileInfoList.at(i);
		fileName = fileInfo.filePath();
		if ( fileName.endsWith("~") )
		{
			QFile fileSaved(fileName);
			fileSaved.remove();
			bPreviousSaveImcomplete = true;
		}
	}

	return bPreviousSaveImcomplete;
}

void CDataDoc::TessInvisibleParts()
{
	CPartTessThread *partTessThread = new CPartTessThread(m_EntList, 2);// 2=tess invisible only

	partTessThread->start(QThread::NormalPriority);
}

void CDataDoc::InitializeLogFile(const QString logFileDir)
{
	if ( !logFileDir.isEmpty() )
		m_docLogWritter.SetFileDir(logFileDir);
	m_docLogWritter.InitializeLogFile(GetPatientId());
}

void CDataDoc::AppendLog(const QString& logString, bool newLine)
{
	m_docLogWritter.AppendLog(logString, newLine);
}

void CDataDoc::TerminateLogFile()
{
	m_docLogWritter.TerminateLogFile();
}

void CDataDoc::SetLogFileDir(const QString& logFileDir)
{
	// Copy the crash file to the new folder
	QString existingFileCrashName = m_docLogWritter.GetFileCrashName();
	QString newFileCrashName = logFileDir + QDir::separator() + m_docLogWritter.GetFileCrashName(false);
	
	QFile file(existingFileCrashName);
	file.copy(newFileCrashName);

	m_docLogWritter.SetFileDir(logFileDir);
}

void CDataDoc::StartTimeLog(string strStepname)
{
}

void CDataDoc::EndTimeLog(string strStepname)
{
}

QString CDataDoc::SaveTemp()
{
	QString		sDir = QDir::tempPath() + "\\temp.tw";

	if( QDir( sDir ).exists() )
		DeleteDocDir( sDir );

	Save( sDir );

	return sDir;
}

////////////////////////////////////////////////////////////
// Make another folder with the same contents (file list)
////////////////////////////////////////////////////////////
void CDataDoc::SaveAnotherCopy(QString sourceDir, QString targetDir)
{
	// Copy all files to target folder
	QDir originalFileDir(sourceDir);
	originalFileDir.mkdir(targetDir);

	QStringList fileList = originalFileDir.entryList(QDir::Files | QDir::Hidden);
	for (int i=0; i<fileList.count(); i++)
	{
		QString fileName(fileList.at(i));
		// Do not need to copy whoOccupiesFile.txt file, but anything else.
		if ( !fileName.endsWith("whoOccupiesFile.txt") )
		{
			QFile file(sourceDir + QDir::separator() + fileList.at(i));
			file.copy(targetDir + QDir::separator() + fileList.at(i));
		}
	}
}

void CDataDoc::DisplayOnlyPart( int id )
{
	int		iEnt, nEnts;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		if( pEntity->GetDisplayMode() != DISP_MODE_NONE )
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );

		if( pEntity->GetEntId() == id )
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
	}

	m_pView->Redraw();
}

void CDataDoc::SetEntityDisplayMode( int id, CDisplayMode eDispMode )
{
	CEntity*		pEntity = GetEntityById( id );

	pEntity->SetDisplayMode( eDispMode );

	m_pEntPanel->SetCheckState( id, eDispMode );
}


int CDataDoc::GetAvlEntId()
{
	return m_nAvlEntId++;
}


void CDataDoc::UpdateAvlEntId( int nEntId )
{
	m_nAvlEntId = max( m_nAvlEntId, nEntId + 1 );
}


void CDataDoc::SetAvlEntId( int id )
{
	m_nAvlEntId = id;
}

/* virtual */
bool CDataDoc::DeleteEntityById( int id, bool bDestroyEntity /* = true */ )
{
	int		iEnt, nEnts;
	
	nEnts = m_EntList.count();

    bool actuallyDeleted = false;

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		if( pEntity->GetEntId() == id )
		{
			m_EntList.removeAt( iEnt );
			
			if( bDestroyEntity )
				delete pEntity;
			
			iEnt--;
			nEnts--;

            actuallyDeleted = true;

			break; // id is unique. Just break out when found it
		}
	}

    if (actuallyDeleted)
    {
    	m_pEntPanel->DeleteById( id );
    	m_bModified = true;
    }

    return actuallyDeleted;
}

void CDataDoc::DeleteEntitiesStartingId( int id )
{
	int		iEnt, nEnts, idEnt;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		idEnt = pEntity->GetEntId();

		if( idEnt >= id )
		{
			m_EntList.removeAt( iEnt );
			
			delete pEntity;
			
			iEnt--;
			nEnts--;

			
			m_pEntPanel->DeleteById( idEnt );
		}
	}

	m_bModified = true;
}

void CDataDoc::DeleteAllEntities()
{
	int		iEnt, nEnts, idEnt;
	
	nEnts = m_EntList.count();

	for( iEnt = nEnts-1; iEnt > -1; iEnt-- )// reverse
	{
		CEntity*		pEntity = m_EntList[iEnt];

		idEnt = pEntity->GetEntId();

		m_EntList.removeAt( iEnt );
			
		delete pEntity;
			
		m_pEntPanel->DeleteById( idEnt );
	}

	m_pEntPanel->Clear();
	if ( entityWidget->IsItemExist(m_pEntPanel) )
		entityWidget->RemoveItem( m_pEntPanel );

	m_bModified = true;
}

void CDataDoc::RemoveEntityFromEntPanel( CEntRole eEntRole )
{
	CEntity*	pEntity = GetEntity( eEntRole );

	if( pEntity )
		m_pEntPanel->DeleteById( pEntity->GetEntId() );
}

void CDataDoc::SetEntPanelValidateStatus( CEntRole eEntRole, CEntValidateStatus status, QString message )
{
	CEntity*	pEntity;

	pEntity = GetEntity( eEntRole );

	if (pEntity == NULL ) return;

	m_pEntPanel->SetValidateStatus( pEntity->GetEntId(), status, message );
}

void CDataDoc::SetEntPanelFontColor(CEntRole entRole, CColor color)
{
	CEntity*	pEntity;

	pEntity = GetEntity( entRole );

	if (pEntity == NULL ) return;

	m_pEntPanel->SetFontColor( pEntity->GetEntId(), color );

}

void CDataDoc::SetEntPanelToolTip(CEntRole entRole, QString message)
{
	CEntity*	pEntity;

	pEntity = GetEntity( entRole );

	if (pEntity == NULL ) return;

	m_pEntPanel->SetToolTip( pEntity->GetEntId(), message );

}

int CDataDoc::ParsePatientId( const QString& sFileName )
{
	QString		sValue;
	QRegExp		rx( "000\\d\\d\\d\\d_" );
	int			id = 0;
	int			idx = sFileName.indexOf( rx );

	if( idx >= 0 )
	{
		sValue = sFileName.mid( idx, 7 );
		id = sValue.toInt();
	}

	return id;
}


void CDataDoc::DisplayTempPoints()
{
	int						i, np;
	IwPoint3d				pt;
	CColor					color;

	np = m_tempPoints.count();

	for( i = 0; i < np; i++ )
	{
		pt = m_tempPoints[i].pt;
		color = m_tempPoints[i].color;

		m_pView->DisplayPoint( pt, color );
	}
}


void CDataDoc::AddTempPoint( const IwPoint3d& pt, const CColor& color )
{
	CTempPoint		tp;

	tp.pt = pt;
	tp.color = color;

	m_tempPoints.append( tp );
}

void CDataDoc::CleanUpTempPoints()
{
	m_tempPoints.clear();
}

bool CDataDoc::FolderExists(QString docFolderName)
{
	return CDocWriter::FolderExists(docFolderName);
}

/////////////////////////////////////////////////////////////////////////
// This function returns the objects in their original coordinate system,
// regardless  transforms the scene or not.
/////////////////////////////////////////////////////////////////////////
bool CDataDoc::GetSelectableEntities
(
	IwTArray<IwBrep*>& selectableBreps,			// O:
	IwTArray<bool>& selectableBrepsAreOpaque,	// O: true=opaque, false=transparent
	IwTArray<IwCurve*>& selectableCurves,		// O:
	IwTArray<IwPoint3d>& selectablePoints		// O:
)
{
	int	iEnt, nEnts;
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];
		CDisplayMode	eDispMode = pEntity->GetDisplayMode();

		// Only the display and transparency and displaynet and transparencynet entities are selectable.
		if( eDispMode == DISP_MODE_TRANSPARENCY || eDispMode == DISP_MODE_DISPLAY ||
			eDispMode == DISP_MODE_TRANSPARENCY_NET || eDispMode == DISP_MODE_DISPLAY_NET ||
			eDispMode == DISP_MODE_WIREFRAME )
		{
			int prevBrepNumber = selectableBreps.GetSize();
			pEntity->GetSelectableEntities(selectableBreps, selectableCurves, selectablePoints);
			int postBrepNumber = selectableBreps.GetSize();
			// Opaque or transparent
			for (int i=prevBrepNumber; i<postBrepNumber; i++)
			{
				if ( eDispMode == DISP_MODE_DISPLAY || eDispMode == DISP_MODE_DISPLAY_NET )
				{
					selectableBrepsAreOpaque.Add(true);
				}
				else
				{
					selectableBrepsAreOpaque.Add(false);
				}
			}
		}
	}

	return true;

}

QString	CDataDoc::GetFileFolder()
{
	QString fileFolder = QString("");
	QString fileName = GetFileName();

	if ( fileName.isEmpty() )
		return fileFolder;
	
	int lastIndex0 = fileName.lastIndexOf("/");
	int lastIndex1 = fileName.lastIndexOf("\\");
	int lastIndex = (lastIndex0 >lastIndex1) ? lastIndex0 : lastIndex1;
	
	fileFolder = fileName.left(lastIndex );

	return fileFolder;
}

void CDataDoc::AppendValidateMessage(QString const& key, int& status, QString& message)
{
	QString keyStatus = key + QString(" STATUS");
	int st = GetValidateTableStatus(keyStatus);
	if ( st > -1 )
	{
		status = max(st, status);
		QString keyMessage = key + QString(" MESSAGE");
		QString ms = GetValidateTableString(keyMessage);
		if ( message.isEmpty() )
		{
			message += ms;
		}
		else
		{
			bool bContained = message.contains(ms);
			if ( !bContained )
				message += QString("\n") + ms;
		}
	}
}

/////////////////////////////////////////////////////////////////////
// This function check whether the opening file is equal or newer 
// than the input (Rel, Rev, dVer) version.
bool CDataDoc::IsOpeningFileRightVersion(int Rel, int Rev, int dVer)
{
	int Release, Revision, DFVersion;
	GetOpeningFileReleaseNumber(Release);
	GetOpeningFileRevisionNumber(Revision);
	GetOpeningFileDataFileVersionNumber(DFVersion);

	bool rightVersion = false;
	if ( Release > Rel )
	{
		rightVersion = true;
		return rightVersion;
	}
	else if ( Release == Rel &&
		      Revision > Rev )
	{
		rightVersion = true;
		return rightVersion;
	}
	else if ( Release == Rel &&
		      Revision == Rev &&
			  DFVersion >= dVer)
	{
		rightVersion = true;
		return rightVersion;
	}

	return rightVersion;
}

bool CDataDoc::DisplayValidationData()
{
	bool dispValData = false;
	char *displayValidationData = getenv("ITOTAL_DISPLAYVALIDATIONDATA");
	if (displayValidationData != NULL && QString("1") == displayValidationData)
	{
		if ( IsReadOnlyFile() )
			dispValData = true;
	}

	return dispValData;
}

QString	CDataDoc::GetVarTableString(QString const& key)
{
   QString		string = mainWindow->GetImplant()->getConfigString("default", key);

	if( string.isEmpty() )
	{
		int	button;
		if (mainWindow->GetImplant()->isDevelopment())
		{
			button = QMessageBox::critical( mainWindow, 
											"Configurable Table", 
											QString("Configurable key %1 is missing or not up-to-date.\nTriathlonF1 software needs to be updated.").arg(key),
											QMessageBox::Ok );
		}
		else
		{
			button = QMessageBox::critical( mainWindow, 
											"Configurable Table", 
											"Configurable table is missing or not up-to-date.\n"
											"TriathlonF1 software needs to be updated.", 
											QMessageBox::Ok );
		}
	}

	return string;
}

double	CDataDoc::GetVarTableValue(QString const& key)
{
	QString		string = mainWindow->GetImplant()->getConfigString("default", key);

	if( string.isEmpty() )
	{
		int	button;
		QString message = QString("Variable %1 is missing.\nTriathlonF1 software needs to be updated.").arg(key);
		button = QMessageBox::critical( mainWindow, 
										"Configurable Table", 
										message, 
										QMessageBox::Ok );	
	}


	if (std::find(vectOfFieldsUsed.begin(), vectOfFieldsUsed.end(), key) == vectOfFieldsUsed.end()) 
	{
		vectOfFieldsUsed.push_back(key);
	}

	double value = string.toDouble();

	return value;
}

QString	CDataDoc::GetValidateTableString(QString const& key)
{
	QString		string = mainWindow->GetImplant()->getConfigString("default", key);

	return string;
}

///////////////////////////////////////////////////////////
// Status: -1: skip checking, 0: green, 1: yellow, 2: red
///////////////////////////////////////////////////////////
int	CDataDoc::GetValidateTableStatus(QString const& key)
{
	QString		string = mainWindow->GetImplant()->getConfigString("default", key);
	// QString		string = CDocReader::GetKeyValue(m_validationTableFileName, key);

	int value = -1;

	if( string.isEmpty() )
	{
		int	button;
		if (mainWindow->GetImplant()->isDevelopment())
		{
			button = QMessageBox::critical( mainWindow, 
											"Configurable Table", 
											QString("Configurable key %1 is missing or not up-to-date.\nTriathlonF1 software needs to be updated.").arg(key),
											QMessageBox::Ok );
		}
		else
		{
			button = QMessageBox::critical( mainWindow, 
											"Configurable Table", 
											"Configurable table is missing or not up-to-date.\n"
											"TriathlonF1 software needs to be updated.", 
											QMessageBox::Ok );	
		}
	}
	else
	{
		value = string.toInt();

		if (std::find(vectOfFieldsUsed.begin(), vectOfFieldsUsed.end(), key) == vectOfFieldsUsed.end())
		{
			vectOfFieldsUsed.push_back(key);
		}
	}
	
	return value;
}

/*   static */
bool CDataDoc::isCruciateRetaining()
{
   if (Implant* pImp = mainWindow->GetImplant())
      return pImp->GetModule("TotalFemur") != nullptr;
   else
      return false;
}

/*   static */
bool CDataDoc::isPosteriorStabilized()
{
   if (Implant* pImp = mainWindow->GetImplant())
      return pImp->GetModule("TotalPS") != nullptr;
   else
      return false;
}

bool CDataDoc::isProductionSoftware()
{
   if (Implant* pImp = mainWindow->GetImplant())
      return pImp->isProduction();
   else
      return false;
}

/////////////////////////////////////////////////
int CDataDoc::GetTotalDesignTime()
{
	CEntity*		pEntity;

	int totalSeconds = 0;
	int nEnts = GetNumOfEntities();

	for( int i = 0; i < nEnts; i++ )
	{
		pEntity = GetEntity( i );
		totalSeconds += pEntity->GetDesignTime();
	}

	return totalSeconds;
}

int CDataDoc::GetFemurImplantDesignTime()
{
	CEntity*		pEntity;

	int totalSeconds = 0;
	int nEnts = GetNumOfEntities();

	for( int i = 0; i < nEnts; i++ )
	{
		pEntity = GetEntity( i );
		CEntRole entRole = pEntity->GetEntRole();
		if ( entRole >= ENT_ROLE_NONE && entRole <= ENT_ROLE_IMPLANT_END )
			totalSeconds += pEntity->GetDesignTime();
	}

	return totalSeconds;
}

int CDataDoc::GetFemurJigsDesignTime()
{
	CEntity*		pEntity;

	int totalSeconds = 0;
	int nEnts = GetNumOfEntities();

	for( int i = 0; i < nEnts; i++ )
	{
		pEntity = GetEntity( i );
		CEntRole entRole = pEntity->GetEntRole();
		if ( entRole >= ENT_ROLE_JIGS_NONE && entRole <= ENT_ROLE_JIGS_END )
			totalSeconds += pEntity->GetDesignTime();
	}

	return totalSeconds;
}

void CDataDoc::AppendEDHREntry(QString const& name, double value)
{
    m_validationReport.append(name, "DESIGN", value);
}


// this is where we initialize times for future reference
bool ValidationReport::StartCounting()
{
    QueryPerformanceCounter(&s_startCPUcounter);
    GetSystemTimeAsFileTime(&s_start);
    return true;
}

// for overall sorting - first by string, the by time
struct VRItemLess
{
    bool operator()(ValidationReport::vrrecord_t const& a, ValidationReport::vrrecord_t const& b)
    {
        // compare the strings, if the same, compare the time
        auto const& sa = std::get<0>(a);
        auto const& sb = std::get<0>(b);
        if (sa != sb) // different
        {
            return sa < sb;
        }
        else
        {
            LARGE_INTEGER aa, bb;
            aa.LowPart = std::get<2>(a).dwLowDateTime;
            aa.HighPart = std::get<2>(a).dwHighDateTime;
            bb.LowPart = std::get<2>(b).dwLowDateTime;
            bb.HighPart = std::get<2>(b).dwHighDateTime;
            return aa.QuadPart < bb.QuadPart;
        }
    }
};

// for sorting by time only - for logging and performance analysis
struct VRItemEarlier
{
    bool operator()(ValidationReport::vrrecord_t const& a, ValidationReport::vrrecord_t const& b)
    {
        // compare the time
        LARGE_INTEGER aa, bb;
        aa.LowPart = std::get<2>(a).dwLowDateTime;
        aa.HighPart = std::get<2>(a).dwHighDateTime;
        bb.LowPart = std::get<2>(b).dwLowDateTime;
        bb.HighPart = std::get<2>(b).dwHighDateTime;
        return aa.QuadPart < bb.QuadPart;
    }
};

QString& ValidationReport::append(QString const& str, QString const& task, double param)
{
    assert(s_timeInit);

    LARGE_INTEGER now;
    QueryPerformanceCounter(&now);

    LONGLONG diff_in_100nsec = (now.QuadPart - s_startCPUcounter.QuadPart); // so many 100-nanosec intervals has passed

    LARGE_INTEGER t_now, t_then;

    t_then.LowPart = s_start.dwLowDateTime;
    t_then.HighPart = s_start.dwHighDateTime;

    t_now.QuadPart = t_then.QuadPart + diff_in_100nsec;

    FILETIME ft;
    ft.dwLowDateTime = t_now.LowPart;
    ft.dwHighDateTime = t_now.HighPart;

    // TODO: properly combine 'str' and 'task'
    m_eDHR.push_back(std::make_tuple(str, param, ft));

    return std::get<0>(m_eDHR.back()); // the very first (0th) element of the last inserted tuple
}
