#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "StylusJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

/////////////////////////////////////////////////////////////////////////////////////////
// The parameters in DefineStylusJigs are related to the stylus start part in SolidWorks.
// The TipPosition is the basis reference, which is the intersection point between
// the outer edge and the sagittal plane. 
// The stylusControlPosition is a point projected from TipPosition onto the stylus top face 
// (the most-y face). The control points are displayed there or 
// related to stylusControlPosition if StylusDelta applies. 
// The TipPosition and stylusControlPosition are fixed after Reset().
// The stylus cross section x-length is 5mm, y-length is 7mm. The default z-length from
// TipPosition to the stylus end is 22mm.
// The stylus cross section profile (rectangle) location is defined related to the TipPosition.
// Its x-position is centered to the TipPosition.
// Its y-position is defined by a "stylus height" which is from the stylus rectangle bottom
// (minimum y) to the TipPosition, i.e., "stylus height" = 7.0 - distance(TipPosition,stylusControlPosition)
// However, the Reset() has determined a proper stylusControlPosition 
// such that the stylus has 2mm clearance with osteophyte. 
// The StylusDelta defines the additional shifts/lengths along x, y, and z directions.
// The delta x and delta y will NOT affect the location of stylusControlPosition. 
/////////////////////////////////////////////////////////////////////////////////////////

CStylusJigs::CStylusJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = gray;

	m_bStylusJigsModified = false;
	m_bStylusJigsDataModified = false;
	m_glStylusJigsVirtualFeaturesList = 0;
	m_glStylusJigsStylusContactRegionList = 0;
	m_stylusContactSurface = NULL;
	m_gapBetweenStylusToOuterSurface = 0.1;
}

CStylusJigs::~CStylusJigs()
{

}

void CStylusJigs::Display()
{
	// display stylus jigs here

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	glDisable( GL_LIGHTING );

	// display virtual features, if any.
	if ( m_bStylusJigsModified && m_glStylusJigsVirtualFeaturesList > 0 )
		DeleteDisplayList( m_glStylusJigsVirtualFeaturesList );

	if( m_glStylusJigsVirtualFeaturesList == 0 )
	{
		m_glStylusJigsVirtualFeaturesList = NewDisplayList();
		glNewList( m_glStylusJigsVirtualFeaturesList, GL_COMPILE_AND_EXECUTE );
		DisplayVirtualFeatures();
		glEndList();
	}
	else
	{
		glCallList( m_glStylusJigsVirtualFeaturesList );
	}

	// display stylus contact surface
	if( m_glStylusJigsStylusContactRegionList == 0 )
	{
		m_glStylusJigsStylusContactRegionList = NewDisplayList();
		glNewList( m_glStylusJigsStylusContactRegionList, GL_COMPILE_AND_EXECUTE );
		DisplayStylusContactRegion();
		glEndList();
	}
	else
	{
		glCallList( m_glStylusJigsStylusContactRegionList );
	}

	glEnable( GL_LIGHTING );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	m_bStylusJigsModified = false;

	CPart::Display();

}

void CStylusJigs::DisplayVirtualFeatures()
{
	IwPoint3d	pt;

	IwPoint3d sPData[64];
	
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	GLfloat wd = 3.0;

	glLineWidth(wd);

	glColor4ub( 225, 225, 0, m_pDoc->GetTransparencyFactor());

	for( ULONG i = 0; i < m_virtualStylus.GetSize(); i++ ) 
	{

		IwCurve *pCurve = m_virtualStylus.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	for( ULONG i = 0; i < m_virtualFillets.GetSize(); i++ ) 
	{

		IwCurve *pCurve = m_virtualFillets.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);
}

void CStylusJigs::DisplayStylusContactRegion()
{
	if (m_stylusContactSurface == NULL) 
		return;

	IwPoint3d	pt;
	IwPoint3d sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	GLfloat wd = 3.0;
	glLineWidth(wd);

	glColor4ub( m_color[0], m_color[1], m_color[2], m_pDoc->GetTransparencyFactor());

	IwBSplineCurve *isoUCurve0, *isoUCurve1, *isoVCurve0, *isoVCurve1;
	m_stylusContactSurface->CreateIsoBoundaries(m_pDoc->GetIwContext(), IW_SP_U, 0.01, isoUCurve0, isoUCurve1);
	m_stylusContactSurface->CreateIsoBoundaries(m_pDoc->GetIwContext(), IW_SP_V, 0.01, isoVCurve0, isoVCurve1);
	IwTArray<IwBSplineCurve*> boundaryCurves;
	boundaryCurves.Add(	isoUCurve0 );
	boundaryCurves.Add(	isoUCurve1 );
	boundaryCurves.Add(	isoVCurve0 );
	boundaryCurves.Add(	isoVCurve1 );

	for( ULONG i = 0; i < boundaryCurves.GetSize(); i++ ) 
	{

		IwCurve *pCurve = boundaryCurves.GetAt(i);
		if ( pCurve == NULL )
			continue;

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);

	for( ULONG i = 0; i < boundaryCurves.GetSize(); i++ ) 
	{
		if ( boundaryCurves.GetAt(i) )
			IwObjDelete(boundaryCurves.GetAt(i));
	}
}

bool CStylusJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool				bOK=true;
	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bStylusJigsDataModified ) 
		return bOK;

	/////////////////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_stylus_jigs.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	// m_stylusPosition
	File.write( (char*) &m_stylusControlPosition, sizeof(IwPoint3d) );

	// m_stylusTipPosition
	File.write( (char*) &m_stylusTipPosition, sizeof(IwPoint3d) );

	// m_stylusDelta
	File.write( (char*) &m_stylusDelta, sizeof(IwPoint3d) );

	// m_filletRadii
	unsigned nSize = m_filletRadii.GetSize();
	File.write( (char*) &nSize, sizeof(unsigned) );
	for (unsigned i=0; i<nSize; i++)
	{
		double radius = m_filletRadii.GetAt(i);
		File.write( (char*) &radius, sizeof( double ) );
	}
	// m_filletPoints
	nSize = m_filletPoints.GetSize();
	File.write( (char*) &nSize, sizeof(unsigned) );
	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d fPnt = m_filletPoints.GetAt(i);
		File.write( (char*) &fPnt, sizeof( IwPoint3d ) );
	}

	//////////////////////////////////////////////////////////////////
	// New for iTW v6014
	// m_gapBetweenStylusToOuterSurface
	File.write( (char*) &m_gapBetweenStylusToOuterSurface, sizeof( double ) );

	File.close();

	// m_virtualStylus
	bool curveOK = false;
	QString				sStylusCurvesFileName;
	sStylusCurvesFileName = sDir + "\\" + m_sFileName + "_stylus_jigs_stylus.crv~";

	curveOK = WriteCurves( m_virtualStylus, sStylusCurvesFileName );

	// m_virtualFillets
	QString				sFilletCurvesFileName;
	sFilletCurvesFileName = sDir + "\\" + m_sFileName + "_stylus_jigs_fillets.crv~";

	curveOK = WriteCurves( m_virtualFillets, sFilletCurvesFileName );

	//////////////////////////////////////////////////////////////////
	// New for iTW v606
	// m_stylusContactSurface
	sFileName = sDir + "\\" + m_sFileName + "_stylus_jigs_contact.surf~";
	QFile				FileSurface( sFileName );
	bOK = FileSurface.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	if ( m_stylusContactSurface )
	{
		CSurfArray surfaces;
		surfaces.Add(m_stylusContactSurface);
		WriteSurfaces( surfaces, sFileName );
	}
	FileSurface.close();
	/////////////////////////////////////////////////////////////////////

	m_bStylusJigsDataModified = false;

	return true;
}


bool CStylusJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);

	//////////////////////////////
	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_stylus_jigs.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	unsigned nSize;
	IwTArray<IwPoint3d>		edgePoints, tPoints;

	// Variables for Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		// m_stylusPosition
		File.read( (char*) &m_stylusControlPosition, sizeof( IwPoint3d ) );

		// m_stylusTipPosition
		File.read( (char*) &m_stylusTipPosition, sizeof( IwPoint3d ) );

		// m_stylusDelta
		File.read( (char*) &m_stylusDelta, sizeof( IwPoint3d ) );

		// m_filletRadii
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			double radius;
			File.read( (char*) &radius, sizeof(double) );
			m_filletRadii.Add(radius);
		}

		// m_filletPoints
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d fPoint;
			File.read( (char*) &fPoint, sizeof(IwPoint3d) );
			m_filletPoints.Add(fPoint);
		}
	}

	// Variables for Jigs version 6.0.14
	if (m_pDoc->IsOpeningFileRightVersion(6,0,14))
	{
		// m_gapBetweenStylusToOuterSurface
		File.read( (char*) &m_gapBetweenStylusToOuterSurface, sizeof( double ) );
	}

	File.close();

	// load m_virtualStylus
	bool curveOK = false;
	CCurveArray			sStylusCurves;
	QString				sStylusCurvesFileName;
	sStylusCurvesFileName = sDir + "\\" + m_sFileName + "_stylus_jigs_stylus.crv";

	curveOK = ReadCurves( sStylusCurvesFileName, sStylusCurves );
	
	// load m_virtualFillets
	curveOK = false;
	CCurveArray			sFilletCurves;
	CSurfArray			surfaces;
	QString				sFilletCurvesFileName;
	sFilletCurvesFileName = sDir + "\\" + m_sFileName + "_stylus_jigs_fillets.crv";

	curveOK = ReadCurves( sFilletCurvesFileName, sFilletCurves );

	// New for iTW v606
	IwBSplineSurface* stylusContactSurface = NULL;
	if (m_pDoc->IsOpeningFileRightVersion(6,0,6))
	{
		sFileName = sDir + "\\" + m_sFileName + "_stylus_jigs_contact.surf";

		bOK = ReadSurfaces( sFileName, surfaces );

		if( bOK && surfaces.GetSize() > 0 )
			stylusContactSurface = (IwBSplineSurface*)surfaces.GetAt(0);
	}


	// Actions required by Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		SetVirtualStylusNFillets(sStylusCurves, sFilletCurves);
	}

	// Actions required by Jigs version 6.0.6
	if (m_pDoc->IsOpeningFileRightVersion(6,0,6))
	{
		SetStylusContactSurface(stylusContactSurface);
	}

	m_bStylusJigsDataModified = false;

	return true;
}

void CStylusJigs::SetStylusJigsModifiedFlag( bool bModified )
{
	m_bStylusJigsModified = bModified;
	if (m_bStylusJigsModified)
	{
		m_bStylusJigsDataModified = true;
		m_pDoc->SetModified(m_bStylusJigsModified);
	}
}

void CStylusJigs::SetFilletPointsRadii
(
	IwTArray<IwPoint3d>& filletPoints,
	IwTArray<double>& filletRadii
)
{
	m_filletPoints.RemoveAll();
	m_filletRadii.RemoveAll();

	m_filletPoints.Append(filletPoints);
	m_filletRadii.Append(filletRadii);

	SetStylusJigsModifiedFlag(true);
}

void CStylusJigs::GetFilletPointsRadii
(
	IwTArray<IwPoint3d>& filletPoints,
	IwTArray<double>& filletRadii
)
{
	filletPoints.RemoveAll();
	filletRadii.RemoveAll();

	filletPoints.Append(m_filletPoints);
	filletRadii.Append(m_filletRadii);
}

void CStylusJigs::SetVirtualStylusNFillets
(
	IwTArray<IwCurve*>& virtualStylus,
	IwTArray<IwCurve*>& virtualFillets
)
{
	// delete m_virtualStylus
	for (unsigned i=0; i<m_virtualStylus.GetSize(); i++)
	{
		if ( m_virtualStylus.GetAt(i) )
			IwObjDelete(m_virtualStylus.GetAt(i));
	}
	m_virtualStylus.RemoveAll();
	// delete m_virtualFillets
	for (unsigned i=0; i<m_virtualFillets.GetSize(); i++)
	{
		if ( m_virtualFillets.GetAt(i) )
			IwObjDelete(m_virtualFillets.GetAt(i));
	}
	m_virtualFillets.RemoveAll();

	m_virtualStylus.Append(virtualStylus);
	m_virtualFillets.Append(virtualFillets);

	DeleteDisplayList( m_glStylusJigsVirtualFeaturesList ); // Force to update display

	SetStylusJigsModifiedFlag(true);
}

void CStylusJigs::GetVirtualStylusNFillets
(
	IwTArray<IwCurve*>& virtualStylus,
	IwTArray<IwCurve*>& virtualFillets
)
{
	virtualStylus.RemoveAll();
	virtualFillets.RemoveAll();

	virtualStylus.Append(m_virtualStylus);
	virtualFillets.Append(m_virtualFillets);
}

void CStylusJigs::SetStylusPosition
(
	IwPoint3d& stylusControlPosition, 
	IwPoint3d& stylusTipPosition, 
	IwPoint3d& stylusDelta,
	double& gapBetweenStylusToOuterSurface
)
{
	m_stylusControlPosition = stylusControlPosition;
	m_stylusTipPosition = stylusTipPosition;
	m_stylusDelta = stylusDelta;
	m_gapBetweenStylusToOuterSurface = gapBetweenStylusToOuterSurface;

	SetStylusJigsModifiedFlag(true);
}

void CStylusJigs::GetStylusPosition
(
	IwPoint3d& stylusControlPosition, 
	IwPoint3d& stylusTipPosition, 
	IwPoint3d& stylusDelta,
	double& gapBetweenStylusToOuterSurface
)
{
	stylusControlPosition = m_stylusControlPosition;
	stylusTipPosition = m_stylusTipPosition;
	stylusDelta = m_stylusDelta;
	gapBetweenStylusToOuterSurface = m_gapBetweenStylusToOuterSurface;
}

void CStylusJigs::SetStylusContactSurface(IwBSplineSurface* contactSurface)
{
	// Delete the existing one
	if (m_stylusContactSurface)
	{
		IwObjDelete(m_stylusContactSurface);
		m_stylusContactSurface = NULL;
		DeleteDisplayList( m_glStylusJigsStylusContactRegionList );
		m_glStylusJigsStylusContactRegionList =0;
	}
	//
	m_stylusContactSurface = contactSurface;
}

bool CStylusJigs::IsMemberDataChanged
(
	IwPoint3d& stylusControlPosition, 
	IwPoint3d& stylusDelta, 
	double& gapBetweenStylusToOuterSurface,
	IwTArray<double>& filletRadii
)
{
	if ( stylusControlPosition != m_stylusControlPosition )
		return true;

	if ( stylusDelta != m_stylusDelta )
		return true;

	if ( filletRadii.GetSize() != m_filletRadii.GetSize() )
		return true;

	if ( !IS_EQ_TOL6(gapBetweenStylusToOuterSurface, m_gapBetweenStylusToOuterSurface) )
		return true;

	for (unsigned i=0; i<filletRadii.GetSize(); i++)
	{
		if (filletRadii.GetAt(i) != m_filletRadii.GetAt(i))
		{
			return true;
		}
	}

	return false;
}