#ifndef KINPUTVALUESLIDER2_H
#define KINPUTVALUESLIDER2_H

// A dialog for entering two user-adjustable values with two sliders. It can be
// used, for example, to enter the window & level values.
// Other than the sliders, the dialog also contains two labels for the sliders, two spin boxes,
// and OK + Cancel buttons. 

#include <QDialog>

class Ui_InputValueSlider2Class;

class KInputValueSlider2 : public QDialog
{
    Q_OBJECT

public:
    KInputValueSlider2(QWidget *parent = 0);
    ~KInputValueSlider2();

    void SetLabel1Text(QString& str);
    void SetLabel2Text(QString& str);
    void SetRange1(int min, int max);
    void SetRange2(int min, int max);
    void InitValue(int v1, int v2);
    void SetValue1(int v);
    void SetValue2(int v);
    int GetValue1();
    int GetValue2();

    void ShowCheckBox(bool b);
    void SetCheckBoxText(QString& str);
    void SetChecked(bool b);
    bool IsChecked();

protected slots:
    void onSlider1Change(int v);
    void onSpinBox1Change(int v);
    void onSlider2Change(int v);
    void onSpinBox2Change(int v);

signals:
    void value1Changed(int);
    void value2Changed(int);

private:
    Ui_InputValueSlider2Class* ui;
    int initValue1,initValue2;

private slots:
    void on_pushButtonCancel_clicked();
    void on_pushButtonOK_clicked();
};

#endif // KINPUTVALUESLIDER2_H
