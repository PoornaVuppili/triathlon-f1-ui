#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

////////////////////////////////////////////////////////////////////
// This class merges femur surface, determines view profiles,
// and solidify femur Brep. When solidifing, the original surfaces 
// is from Part.h m_pBrep, then merge the patches as possible, and 
// solidify them. Set the solidified Brep back to m_pBrep.
////////////////////////////////////////////////////////////////////

class TEST_EXPORT_TW CFemoralPart : public CPart
{
public:
	CFemoralPart( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName="", int nEntId=-1 );
	virtual ~CFemoralPart();
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );
	virtual void	Display();
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);
	void			DisplayFemoralPartViewProfiles();
	bool			MakeSolidifyIwBrep( );
	IwBSplineSurface*	GetSinglePatchSurface() { return m_pMainFemurSurface;};
	void				GetSideSurfaces(IwBSplineSurface* &leftSideSurface, IwBSplineSurface* &rightSideSurface);
	static bool			DetermineSilhouetteCurves(IwContext& iwContext, IwSurface* surface, IwVector3d &viewVector, IwTArray<IwCurve*>& silhouetteCurves, double tol=0.025);
	void				EmptyViewProfiles();
	void				GetViewProfilesInfo(IwVector3d& viewVector, IwTArray<IwCurve*>& viewProfiles);
	void				SetViewProfiles_ThisIsWrongFunctionToCall(IwTArray<IwCurve*>& viewProfiles);
	bool				GetOuterLoop();
	IwBrep*				GetSolidifyIwBrep();
	void				SetDisplayViewProfiles(bool flag);
	bool				GetDisplayViewProfiles() {return m_bViewProfileDisplay;};
	static bool		CurvesPlaneOuterIntersection(IwTArray<IwCurve*>& silCurves, IwPlane*& plane, IwAxis2Placement& axes, 
											IwPoint3d& posPnt, IwPoint2d& posUVPnt, 
											IwPoint3d& negPnt, IwPoint2d& negUVPnt);
	static bool		CurvesPlaneInnerIntersection(IwTArray<IwCurve*>& silCurves, IwPlane*& plane, IwAxis2Placement& axes, 
											double& refSize, IwPoint3d& pnt, IwPoint2d& UVPnt);
	IwPoint3d		GetMostAnteriorPoint();

	////////////////////
	bool				NormalizeThreeSurfaces(IwAxis2Placement& refAxis);
	static bool			NormalizeThreeSurfaces(IwAxis2Placement& refAxis, IwBSplineSurface*& mainSurface, IwBSplineSurface*& leftSurface, IwBSplineSurface*& rightSurface);
	static void			SmoothThreeSurfaceBoundaries(IwBSplineSurface*& mainSurface, IwBSplineSurface*& leftSurface, IwBSplineSurface*& rightSurface);
	static void			SmoothMainSurfaceAtCrisps(IwBSplineSurface*& mainSurface);
	static void			MoveBoundaryControlPoint(IwPoint3d& ctrlPnt, IwVector3d& movingVector,
									ULONG uIndex, ULONG vIndex, ULONG& ucCount, ULONG& vcCount, double*& cPointers);
	static int			FindCorrespondentIndexOnSideSurface(ULONG& uIndexMain, ULONG& vIndexMain, ULONG& uCtrlCountMain, ULONG& vCtrlCountMain,
									ULONG& uCtrlCountLeft, ULONG& vCtrlCountLeft, ULONG& uCtrlCountRight, ULONG& vCtrlCountRight, 
									int& leftRight, ULONG& uIndexOut, ULONG& vIndexOut, ULONG& uIndexOutNext, ULONG& vIndexOutNext, bool& onLowerCorner);

private:
	bool				SolidifyBrepBySealLaminaEdges( IwBrep* openBrep, IwBrep*& closedBrep);
	bool				SolidifyBrepByCutPlane( IwBrep* openBrep, IwBrep*& closedBrep);
	bool				OrderFaces(IwBrep* pBrep, IwTArray<int> &dimension, IwTArray<IwFace*> &mainFaceOrder, IwTArray<IwFace*> &posSideFaceOrder, IwTArray<IwFace*> &negSideFaceOrder);
	void				MergeFemurSurfaces(IwBrep* femurBrep, IwBrep*& mergedFemurBrep);
	void				SetFPModifiedFlag(bool flag);
	static int			FindCorrespondentIndexOnSideSurface_LongerU(ULONG& uIndexMain, ULONG& vIndexMain, ULONG& uCtrlCountMain, ULONG& vCtrlCountMain,
									ULONG& uCtrlCountLeft, ULONG& vCtrlCountLeft, ULONG& uCtrlCountRight, ULONG& vCtrlCountRight, 
									int& leftRight, ULONG& uIndexOut, ULONG& vIndexOut, ULONG& uIndexOutNext, ULONG& vIndexOutNext, bool& onLowerCorner);
	static int			FindCorrespondentIndexOnSideSurface_LongerV(ULONG& uIndexMain, ULONG& vIndexMain, ULONG& uCtrlCountMain, ULONG& vCtrlCountMain,
									ULONG& uCtrlCountLeft, ULONG& vCtrlCountLeft, ULONG& uCtrlCountRight, ULONG& vCtrlCountRight, 
									int& leftRight, ULONG& uIndexOut, ULONG& vIndexOut, ULONG& uIndexOutNext, ULONG& vIndexOutNext, bool& onLowerCorner);
	

private:
	long						m_glFemoralPartCurveList;

	IwBSplineSurface*			m_pMainFemurSurface;// the femur surface in the main area. It is a single patch.
	IwBSplineSurface*			m_pLeftSideSurface;
	IwBSplineSurface*			m_pRightSideSurface;
	IwVector3d					m_viewProfileVector;
	IwTArray<IwCurve*>			m_viewProfileCurves; // These are simplified planar view profile curves. 0th for positive, 1st for negative side, 2nd inner.

	bool						m_bFPModified;	// Geometric entities in femoral part have not been updated yet to the screen.
	bool						m_bFPDataModified;	// Geometric entities in femoral part have not been saved yet to the file.
	bool						m_bViewProfileDisplay;	// Display view profile curves or not

};