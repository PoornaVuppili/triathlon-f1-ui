#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "InnerSurfaceJigs.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW COuterSurfaceJigs : public CInnerSurfaceJigs
{
public:
	COuterSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_OUTER_SURFACE_JIGS, const QString& sFileName="", int nEntId=-1 );
	~COuterSurfaceJigs();

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );

	void			SetOffsetOsteophyteBrep(IwBrep*& osteoBrep);
	void			GetOffsetOsteophyteBrep(IwBrep*& osteoBrep);
	void			SetOffsetCartilageBrep(IwBrep*& cartiBrep);
	void			GetOffsetCartilageBrep(IwBrep*& cartiBrep);

	void			SetDispOsteo(bool val) {m_bDispOsteo=val;};
	void			SetDispCarti(bool val) {m_bDispCarti=val;};
	unsigned		GetOsteophyteTimeStamp() {return m_osteophyteTimeStamp;};
	void			SetOsteophyteTimeStamp(unsigned val) {m_osteophyteTimeStamp=val;SetOSJigsModifiedFlag(true);};
	unsigned		GetCartilageTimeStamp() {return m_cartilageTimeStamp;};
	void			SetCartilageTimeStamp(unsigned val) {m_cartilageTimeStamp=val;SetOSJigsModifiedFlag(true);};

private:
	void			SetOSJigsModifiedFlag( bool bModified );

private:
	bool			m_bOSJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bOSJigsDataModified;// Geometric entities have not been saved yet to the file. 

	long			m_glOsteoIsoCurveList;
	long			m_glCartiIsoCurveList;
	
	IwBrep*			m_offsetOsteophyteBrep;
	IwBrep*			m_offsetCartilageBrep;
	bool			m_bDispOsteo;
	bool			m_bDispCarti;
	unsigned		m_osteophyteTimeStamp;	// When create m_offsetOsteophyteBrep, we need to check the time stamp
	unsigned		m_cartilageTimeStamp;	// in AdvancedControl is the same as the time stamp here.
											// If the same (indicate users never update the osteophyte surface
											// since last time create m_offsetOsteophyteBrep), just directly use
											// the m_offsetOsteophyteBrep here. Otherwise (users had updated
											//  the osteophyte surface), we need to create the m_offsetOsteophyteBrep.

};