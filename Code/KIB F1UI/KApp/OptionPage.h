#ifndef OPTIONPAGE_H
#define OPTIONPAGE_H

#include <QWidget>

class OptionPage : public QWidget
{
    Q_OBJECT

public:
    OptionPage(QWidget *parent);
    virtual ~OptionPage();

    // Called when 'OK' button is clicked -> Accept the changes.
    virtual void OnOK() = 0;
    // Called when 'Cancel' button is clicked -> Cancel the changes.
    virtual void OnCancel() = 0;
};

#endif // OPTIONPAGE_H
