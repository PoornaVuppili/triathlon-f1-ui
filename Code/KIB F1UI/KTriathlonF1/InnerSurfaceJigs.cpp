#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "InnerSurfaceJigs.h"
#include "OutlineProfileJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

CInnerSurfaceJigs::CInnerSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = gray;

	m_bISJigsModified = false;
	m_bISJigsDataModified = false;

	m_anteriorDropping = NULL;
	m_bDispAntDrop = false;
	m_bDispSurface = true;
	m_tabDropOffsetDistances.Add(0.0);// initialize 3 elements
	m_tabDropOffsetDistances.Add(0.0);// initialize 3 elements
	m_tabDropOffsetDistances.Add(0.0);// initialize 3 elements
	m_glAntDropFaceList = 0;
	m_glAntDropEdgeList = 0;
	m_offsetOsteophyteBrep = NULL;
	m_osteophyteTimeStamp = 1;
}

CInnerSurfaceJigs::~CInnerSurfaceJigs()
{
	if( glIsList( m_glAntDropFaceList ) )
		glDeleteLists( m_glAntDropFaceList, 1 );
	if( glIsList( m_glAntDropEdgeList ) )
		glDeleteLists( m_glAntDropEdgeList, 1 );

}

void CInnerSurfaceJigs::Display()
{
	if ( m_bDispSurface )
		CPart::Display();

	DisplayAntDrop();

	m_bISJigsModified = false;

}

void CInnerSurfaceJigs::DisplayAntDrop()
{
	// display inner surface jigs here
	if( m_eDisplayMode == DISP_MODE_DISPLAY ||
		m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
		m_eDisplayMode == DISP_MODE_DISPLAY_NET ||
		m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET ||
		m_eDisplayMode == DISP_MODE_WIREFRAME )
	{
		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY || m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthMask(GL_FALSE);
		}

		// Display anterior drop surface
		glEnable( GL_LIGHTING );

		glEnable( GL_DEPTH_TEST );

		glEnable( GL_POLYGON_OFFSET_FILL );
		glPolygonOffset( 3.0, 100.0 );

		if ( m_bDispAntDrop )
		{
			if( m_glAntDropFaceList == 0 )
			{
				GenerateIwBrepDisplayList(m_pDoc, m_anteriorDropping, false, blue, m_glAntDropFaceList, m_glAntDropEdgeList);
			}
			else
			{
				glColor4ub( blue[0], blue[1], blue[2], m_pDoc->GetTransparencyFactor());

				if ( m_eDisplayMode != DISP_MODE_WIREFRAME )
					glCallList( m_glAntDropFaceList );

				glCallList( m_glAntDropEdgeList );
			}
		}

		glDisable( GL_POLYGON_OFFSET_FILL );

		if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY || m_eDisplayMode == DISP_MODE_TRANSPARENCY_NET )	
		{
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
		}
	}
}

bool CInnerSurfaceJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool				bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bISJigsDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_is_jigs.iwp~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	if ( m_anteriorDropping )
	{
		WriteBrep( m_anteriorDropping, sFileName );
	}

	File.close();

	// Variables for Jigs version 6.0.26 & 5.2.2
	QString sFileNameOsteo= sDir + "\\" + m_sFileName + "_is_jigs_osteo.iwp~";

	QFile				FileOsteo( sFileNameOsteo );

	bOK = FileOsteo.open( QIODevice::WriteOnly );

	if ( bOK && m_offsetOsteophyteBrep )
	{
		WriteBrep( m_offsetOsteophyteBrep, sFileNameOsteo );
	}

	FileOsteo.close();

	//// variables ///////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_is_jigs.dat~";
	QFile				FileDat( sFileName );
	bOK = FileDat.open( QIODevice::WriteOnly );
	if( !bOK )
		return false;

	unsigned nSize = 0;// m_posVerticalEdgePoints was retired in iTW6
	FileDat.write( (char*) &nSize, sizeof(unsigned) );
	//for (unsigned i=0; i<nSize; i++)
	//{
	//	IwPoint3d edgePoint = m_posVerticalEdgePoints.GetAt(i);
	//	FileDat.write( (char*) &edgePoint, sizeof( IwPoint3d ) );
	//}

	nSize = 0;// m_negVerticalEdgePoints was retired in iTW6
	FileDat.write( (char*) &nSize, sizeof(unsigned) );
	//for (unsigned i=0; i<nSize; i++)
	//{
	//	IwPoint3d edgePoint = m_negVerticalEdgePoints.GetAt(i);
	//	FileDat.write( (char*) &edgePoint, sizeof( IwPoint3d ) );
	//}

	// Variables for TriathlonF1 version 5.0.4
	nSize = m_tabDropOffsetDistances.GetSize();
	FileDat.write( (char*) &nSize, sizeof(unsigned) );
	for (unsigned i=0; i<nSize; i++)
	{
		double offset = m_tabDropOffsetDistances.GetAt(i);
		FileDat.write( (char*) &offset, sizeof( double ) );
	}

	// Variables for TriathlonF1 version 6.0.26 & 5.2.2
	File.write( (char*) &m_osteophyteTimeStamp, sizeof(unsigned) );

	FileDat.close();


	m_bISJigsDataModified = false;

	return true;
}


bool CInnerSurfaceJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);

	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_is_jigs.iwp";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	// Variables for Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		m_anteriorDropping = ReadBrep( sFileName );
		// When the inner surface is loaded completely, we can then call 
		// DetermineOutlineProfileJigsTanProfilePoints() and DetermineOutlineProfileJigsProjectedCurvePoints().
		// Why? It's because the tab and squared-off profiles are on the inner surface jigs.
		COutlineProfileJigs* pOutlineProfileJigs = GetTotalDoc()->GetOutlineProfileJigs();
		if ( pOutlineProfileJigs )
		{
			pOutlineProfileJigs->DetermineOutlineProfileJigsTabProfilePoints(this);
			pOutlineProfileJigs->DetermineOutlineProfileJigsProjectedCurvePoints(this);//It will square off posterior tips if needed
		}
	}

	File.close();

	// Variables for Jigs version 6.0.26 & 5.2.2
	if (m_pDoc->IsOpeningFileRightVersion(5,2,2) && !m_pDoc->IsOpeningFileRightVersion(6,0,0)) // only versions [5.2.2 ~ 6.0.0) (include 5.2.2, but not include 6.0.0)
	{
		QString sFileNameOsteo = sDir + "\\" + m_sFileName + "_is_jigs_osteo.iwp";
		QFile				FileOsteo( sFileNameOsteo );
		bool bOK = FileOsteo.open( QIODevice::ReadOnly );
		if( bOK )
			m_offsetOsteophyteBrep = ReadBrep( sFileNameOsteo );
		FileOsteo.close();
	}
	else if (m_pDoc->IsOpeningFileRightVersion(6,0,26)) // only versions 6.0.26 and after
	{
		QString sFileNameOsteo = sDir + "\\" + m_sFileName + "_is_jigs_osteo.iwp";
		QFile				FileOsteo( sFileNameOsteo );
		bool bOK = FileOsteo.open( QIODevice::ReadOnly );
		if( bOK )
			m_offsetOsteophyteBrep = ReadBrep( sFileNameOsteo );
		FileOsteo.close();
	}

	///////////////////////////////////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_is_jigs.dat";
	QFile				FileDat( sFileName );
	bOK = FileDat.open( QIODevice::ReadOnly );
	if( !bOK )
		return false;
	// Variables for Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		unsigned nSize;
		FileDat.read( (char*) &nSize, sizeof( unsigned ) );
		// m_posVerticalEdgePoints was retired in iTW6
		IwTArray<IwPoint3d> posVerticalEdgePoints;
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d edgePoint;
			FileDat.read( (char*) &edgePoint, sizeof(IwPoint3d) );
			posVerticalEdgePoints.Add(edgePoint);
		}

		FileDat.read( (char*) &nSize, sizeof( unsigned ) );
		// m_negVerticalEdgePoints was retired in iTW6
		IwTArray<IwPoint3d> negVerticalEdgePoints;
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d edgePoint;
			FileDat.read( (char*) &edgePoint, sizeof(IwPoint3d) );
			negVerticalEdgePoints.Add(edgePoint);
		}
	}

	// Variables for Jigs version 5.0.4
	if ( m_pDoc->IsOpeningFileRightVersion(5,0,4) )
	{
		unsigned nSize;
		FileDat.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			double offset;
			FileDat.read( (char*) &offset, sizeof(offset) );
			m_tabDropOffsetDistances.Add(offset);
		}

	}

	// Variables for Jigs version 6.0.26 & 5.2.2
	if (m_pDoc->IsOpeningFileRightVersion(5,2,2) && !m_pDoc->IsOpeningFileRightVersion(6,0,0)) // only versions [5.2.2 ~ 6.0.0) (include 5.2.2, but not include 6.0.0)
	{
		File.read( (char*) &m_osteophyteTimeStamp, sizeof( unsigned ) );
	}
	else if (m_pDoc->IsOpeningFileRightVersion(6,0,0)) // only versions 6.0.26 and after
	{
		File.read( (char*) &m_osteophyteTimeStamp, sizeof( unsigned ) );
	}

	FileDat.close();

	m_bISJigsDataModified = false;

	return true;
}

void CInnerSurfaceJigs::SetISJigsModifiedFlag( bool bModified )
{
	m_bISJigsModified = bModified;
	if (m_bISJigsModified)
	{
		m_bISJigsDataModified = true;
		m_pDoc->SetModified(m_bISJigsModified);
	}
}

void CInnerSurfaceJigs::SetAnteriorDropping(IwBrep* antDropping) 
{
	if ( m_anteriorDropping )
	{
		IwObjDelete(m_anteriorDropping);
		m_anteriorDropping = NULL;
	}
	if( glIsList( m_glAntDropFaceList ) )
	{
		glDeleteLists( m_glAntDropFaceList, 1 );
		m_glAntDropFaceList = 0;
	}
	if( glIsList( m_glAntDropEdgeList ) )
	{
		glDeleteLists( m_glAntDropEdgeList, 1 );
		m_glAntDropEdgeList = 0;
	}

	m_anteriorDropping=antDropping;

	SetISJigsModifiedFlag(true);
}

void CInnerSurfaceJigs::SetOffsetOsteophyteBrep
(
	IwBrep*& osteoBrep
)
{
	// delete m_offsetOsteophyteBrep
	if ( m_offsetOsteophyteBrep )
	{
		IwObjDelete(m_offsetOsteophyte);
		m_offsetOsteophyte=NULL;
	}

	m_offsetOsteophyteBrep = osteoBrep;
}

bool CInnerSurfaceJigs::GetOffsetOsteophyteBrep
(
	IwBrep*& osteoBrep
)
{
	osteoBrep = m_offsetOsteophyteBrep;

	if ( m_offsetOsteophyteBrep == NULL )
		return false;
	else
		return true;
}
