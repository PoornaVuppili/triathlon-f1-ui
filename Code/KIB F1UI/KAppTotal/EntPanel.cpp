#include "EntPanel.h"
#include "Globals.h"
#include "DataDoc.h"
#include "OpenGLView.h"
#include "Globals.h"
#include <assert.h>

#include <QColorDialog>
#include <QFileDialog>
#include <QApplication>

CEntPanel::CEntPanel(QTreeView* view) : EntityItem(view)
{
	
}

CEntPanel::~CEntPanel()
{
	
}

void CEntPanel::DeleteById( int id )
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
		{
			m_list.removeAt( i );
			items[0]->removeRow( i );
			break;
		}
	}
}

void CEntPanel::ToggleById( int id, bool On )
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
		{
			QStandardItem*	pItem0 = items[0]->child( i );
			pItem0->setCheckState( On ? Qt::Checked : Qt::Unchecked);
			break;
		}
	}
}

void CEntPanel::Clear()
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = nItems - 1; i >= 0; i-- )
	{
		m_list.removeAt( i );
		//items[0]->removeRow( i );
	}

    items.clear();
}

void CEntPanel::SetModifiedMark( int id, bool removeMark/* true=upToDate */)
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
		{
			QStandardItem* itemId = items[0]->child(i);
			QString sName;
			
			if ( !removeMark )
			{
				sName = "*" + m_list[i].sEntName; 
			}
			else
				sName = m_list[i].sEntName;

			itemId->setData( sName, 0 );
			break;
		}
	}
}

void CEntPanel::SetValidateStatus( int id, CEntValidateStatus status, QString message )
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
		{
			QStandardItem* itemId = items[0]->child(i);
			if ( itemId )
			{
				if ( !message.isEmpty() )
					itemId->setToolTip(message);
				if ( status == VALIDATE_STATUS_RED )// red
					itemId->setForeground( QBrush( QColor(darkRed[0],darkRed[1],darkRed[2]) ) );
				else if ( status == VALIDATE_STATUS_YELLOW ) // yellow
					itemId->setForeground( QBrush(QColor(darkYellow[0],darkYellow[1],darkYellow[2])) ); 
				else if ( status == VALIDATE_STATUS_GREEN ) // green
					itemId->setForeground( QBrush(QColor(darkGreen[0],darkGreen[1],darkGreen[2])) );
				else	// black
					itemId->setForeground( QBrush(QColor(black[0],black[1],black[2])) );

				break;
			}
		}
	}
}

void CEntPanel::SetFontColor( int id, CColor color )
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
		{
            QStandardItem* itemId = items[0]->child(i);
			if ( itemId )
			{
				itemId->setForeground( QBrush( QColor(color[0],color[1],color[2]) ) );
				break;
			}
		}
	}
}

void CEntPanel::SetToolTip(int id, QString message )
{
	int			i, nItems;

	nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
		{
			QStandardItem* itemId = items[0]->child(i);
			if ( itemId )
			{
				if ( !message.isEmpty() )
					itemId->setToolTip(message);
				break;
			}
		}
	}
}

void CEntPanel::OnDisplay()
{
	CEntity*			pEnt = m_pDoc->GetEntityById( m_id );
	QStandardItem*	    pItem = FindItemById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnDisplay(), entity name: %1").arg(pEnt->GetEntName()) );

	pEnt->SetDisplayMode( DISP_MODE_DISPLAY );

	pItem->setCheckState( Qt::Checked );
}

void CEntPanel::OnDisplayNet()
	{
	CEntity*			pEnt = m_pDoc->GetEntityById( m_id );
	QStandardItem*	    pItem = FindItemById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnDisplayNet(), entity name: %1").arg(pEnt->GetEntName()) );

	pEnt->SetDisplayMode( DISP_MODE_DISPLAY_NET );

	pItem->setCheckState( Qt::Checked );
	}

void CEntPanel::OnTransparency()
{
	CEntity*			pEnt = m_pDoc->GetEntityById( m_id );
	QStandardItem*	    pItem = FindItemById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnTransparency(), entity name: %1").arg(pEnt->GetEntName()) );

	pEnt->SetDisplayMode( DISP_MODE_TRANSPARENCY );

	pItem->setCheckState( Qt::Checked );
}

void CEntPanel::OnTransparencyNet()
	{
	CEntity*			pEnt = m_pDoc->GetEntityById( m_id );
	QStandardItem*	    pItem = FindItemById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnTransparencyNet(), entity name: %1").arg(pEnt->GetEntName()) );

	pEnt->SetDisplayMode( DISP_MODE_TRANSPARENCY_NET );

	pItem->setCheckState( Qt::Checked );
	}

void CEntPanel::OnWireframe()
{
	CEntity*			pEnt = m_pDoc->GetEntityById( m_id );
	QStandardItem*	    pItem = FindItemById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnWireframe(), entity name: %1").arg(pEnt->GetEntName()) );

	pEnt->SetDisplayMode( DISP_MODE_WIREFRAME );

	pItem->setCheckState( Qt::Checked );
}

void CEntPanel::OnHide()
{
	CEntity*			pEnt = m_pDoc->GetEntityById( m_id );
	QStandardItem*	    pItem = FindItemById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnHide(), entity name: %1").arg(pEnt->GetEntName()) );

	pEnt->SetDisplayMode( DISP_MODE_HIDDEN );

	pItem->setCheckState( Qt::Unchecked );
}

void CEntPanel::OnColor()
{
	CPart*		pPart = m_pDoc->GetPartById( m_id );
	
	QColor		qcolor = QColorDialog::getColor();

	m_pDoc->AppendLog( QString("CEntPanel::OnColor(), entity name: %1").arg(pPart->GetEntName()) );

	if( qcolor.isValid() )
	{
		int			r, g, b;

		qcolor.getRgb( &r, &g, &b );

		CColor		color( r, g, b );

		pPart->SetColor( color );

	}
}

void CEntPanel::OnExportStl()
{
	QString					sFileTypes = "STL files (*.stl);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;
	CPart*					pPart;
	QFileDialog				Dlg;

	sDir = m_pDoc->GetFileFolder();

	sFileName = Dlg.getSaveFileName( NULL, "Export as STL", sDir, sFileTypes, 0, options  );

	if( sFileName.isNull() )
		return;

	if( !sFileName.endsWith( "stl", Qt::CaseInsensitive ) )
		sFileName += ".stl";

	sDir = GetFileDir( sFileName );

	pPart = m_pDoc->GetPartById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnExportStl(), entity name: %1").arg(pPart->GetEntName()) );

	pPart->ExportSTL( sFileName );

}


void CEntPanel::OnExportIges()
{
	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;
	CPart*					pPart;
	QFileDialog				Dlg;

	sDir = m_pDoc->GetFileFolder();

	sFileName = Dlg.getSaveFileName( NULL, "Export as IGES", sDir, sFileTypes, 0, options  );

	if( sFileName.isNull() )
		return;

	if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
		sFileName += ".igs";

	sDir = GetFileDir( sFileName );

	pPart = m_pDoc->GetPartById( m_id );

	m_pDoc->AppendLog( QString("CEntPanel::OnExportIges(), entity name: %1").arg(pPart->GetEntName()) );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	pPart->ExportIGES( sFileName );

	QApplication::restoreOverrideCursor();
}

void CEntPanel::OnRetessellate()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntType	eEntType = pEnt->GetEntType();

	m_pDoc->AppendLog( QString("CEntPanel::OnRetessellate(), entity name: %1").arg(pEnt->GetEntName()) );

	switch( eEntType )
	{

		case ENT_TYPE_PART:
		case ENT_TYPE_FEMORAL_PART:
		case ENT_TYPE_FEMORAL_CUTS:
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
				CPart* part = (CPart*)pEnt;
				part->MakeTess(true);
				m_pDoc->GetView()->Redraw();
				QApplication::restoreOverrideCursor();
				break;
			}
	}
}

void CEntPanel::OnDelete()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();

	m_pDoc->AppendLog( QString("CEntPanel::OnDelete(), entity name: %1").arg(pEnt->GetEntName()) );

	switch( eEntRole )
		{

		case ENT_ROLE_NONE:
		case ENT_ROLE_JIGS_NONE:
		case ENT_ROLE_JIGS_PS_NONE:
			{
			m_pDoc->DeleteEntityById( m_id );
			m_pDoc->GetView()->Redraw();
			break;
			}

		}

}

QStandardItem* CEntPanel::FindItemById( int id )
{
	int			i, nItems;

    nItems = m_list.size();

	for( i = 0; i < nItems; i++ )
	{
		if( m_list[i].id == id )
			break;
	}

    return items[0]->child( i, 0 );
}

void CEntPanel::SetCheckState( int id, CDisplayMode eDispMode )
{
	QStandardItem*	pItem = FindItemById( id );

	if( eDispMode == DISP_MODE_DISPLAY || 
		eDispMode == DISP_MODE_WIREFRAME ||
		eDispMode == DISP_MODE_TRANSPARENCY
	  )
		pItem->setCheckState( Qt::Checked );
	else
		pItem->setCheckState( Qt::Unchecked );
}

void CEntPanel::ItemClicked(const QModelIndex& index)
{

    bool ctrlDown = (QApplication::keyboardModifiers() == Qt::ControlModifier);
    Qt::MouseButtons button = EntityItem::ClickButton;

	int row = index.row();

	CEntity*	pEnt = m_pDoc->GetEntityById( m_list[row].id );
	if( pEnt == NULL || pEnt->GetDisplayMode() == DISP_MODE_NONE )
		return;

	// Here to handle the 2nd item
	QStandardItem *si = dynamic_cast<const QStandardItemModel*>(index.model())->itemFromIndex(index);
	if(pEnt->Get2ndItem() == si)
	{
		CDisplayMode expectedDisplayMode = DISP_MODE_DISPLAY;

		if(button == Qt::MiddleButton)
			si->setCheckState( (si->checkState() == Qt::Checked) ? Qt::Unchecked : Qt::Checked);

		if(!pEnt->Is2ndItemOn() && items[0]->child(row, 0)->checkState() == Qt::Unchecked)
			return;

		if(!pEnt->Is2ndItemOn())
		{
			if(pEnt->GetEntRole() == ENT_ROLE_FEMORAL_AXES)
				expectedDisplayMode = DISP_MODE_TRANSPARENCY;
		}
		else
			expectedDisplayMode = (button == Qt::MiddleButton) ? DISP_MODE_TRANSPARENT_CT : DISP_MODE_DISPLAY_CT;

		if(items[0]->child(row, 0)->checkState() != Qt::Checked)
			items[0]->child(row, 0)->setCheckState(Qt::Checked);

		pEnt->SetDisplayMode(expectedDisplayMode);
		m_pDoc->SetModified(true);

		return;
	}

	// Here to handle the 1st item
	CDisplayMode expectedDisplayMode;
	CDisplayMode displayedMode = pEnt->GetDisplayMode();

	if ( button == Qt::LeftButton )
	{
		if ( displayedMode == DISP_MODE_HIDDEN ) // it was hidden, it should be turned on.
		{
			expectedDisplayMode = DISP_MODE_DISPLAY; // it should be turned on.
			// However, we may have the following conditions
			if(pEnt->GetEntRole() == ENT_ROLE_FEMORAL_AXES)
				expectedDisplayMode = DISP_MODE_TRANSPARENCY;
			if ( QApplication::keyboardModifiers() == Qt::NoModifier && pEnt->Is2ndItemOn() )
				expectedDisplayMode = DISP_MODE_DISPLAY_CT;
			else if ( QApplication::keyboardModifiers() == Qt::ControlModifier )
				expectedDisplayMode = DISP_MODE_DISPLAY_NET;
			else if ( QApplication::keyboardModifiers() == Qt::ShiftModifier )
				expectedDisplayMode = DISP_MODE_WIREFRAME;
		}
		else // it was displayed, it should be turned off.
		{
			expectedDisplayMode = DISP_MODE_HIDDEN; // it should be turned off.
			// However, we may have the following conditions
			if ( QApplication::keyboardModifiers() == Qt::ControlModifier )
				expectedDisplayMode = DISP_MODE_DISPLAY_NET;
			else if ( QApplication::keyboardModifiers() == Qt::ShiftModifier )
				expectedDisplayMode = DISP_MODE_WIREFRAME;
		}
	}
	else if ( button == Qt::MiddleButton )
	{
		if ( displayedMode == DISP_MODE_HIDDEN ) // it was hidden, it should be turned on.
		{
			expectedDisplayMode = DISP_MODE_TRANSPARENCY; // it should be turned on in transparent.
			// However, we may have the following conditions
			if ( QApplication::keyboardModifiers() == Qt::NoModifier && pEnt->Is2ndItemOn() )
				expectedDisplayMode = DISP_MODE_TRANSPARENT_CT;
			else if ( QApplication::keyboardModifiers() == Qt::ControlModifier )
				expectedDisplayMode = DISP_MODE_TRANSPARENCY_NET;
			else if ( QApplication::keyboardModifiers() == Qt::ShiftModifier )
				expectedDisplayMode = DISP_MODE_WIREFRAME;
		}
		else // it was displayed
		{
			// However, we may have the following conditions
			if ( QApplication::keyboardModifiers() == Qt::NoModifier )
			{
				if ( displayedMode == DISP_MODE_TRANSPARENCY )
					expectedDisplayMode = DISP_MODE_DISPLAY;
				else
					expectedDisplayMode = DISP_MODE_TRANSPARENCY; 
			}
			else if ( QApplication::keyboardModifiers() == Qt::ControlModifier )
				expectedDisplayMode = DISP_MODE_TRANSPARENCY_NET;
			else if ( QApplication::keyboardModifiers() == Qt::ShiftModifier )
				expectedDisplayMode = DISP_MODE_WIREFRAME;
		}
	}

	// Only these entities can display NET 
	if ( pEnt->GetEntRole() == ENT_ROLE_FEMUR ||
		 pEnt->GetEntRole() == ENT_ROLE_OSTEOPHYTE_SURFACE ||
		 pEnt->GetEntRole() == ENT_ROLE_CARTILAGE_SURFACE ||
		 pEnt->GetEntRole() == ENT_ROLE_INNER_SURFACE_JIGS ||
		 pEnt->GetEntRole() == ENT_ROLE_OUTER_SURFACE_JIGS ||
		 pEnt->GetEntRole() == ENT_ROLE_ANTERIOR_SURFACE_JIGS)
	{}
	else
	{
		if ( expectedDisplayMode == DISP_MODE_DISPLAY_NET )
			expectedDisplayMode = DISP_MODE_DISPLAY;
		else if ( expectedDisplayMode == DISP_MODE_TRANSPARENCY_NET )
			expectedDisplayMode = DISP_MODE_TRANSPARENCY;
	}
	//
	if ( pEnt->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE ||
		 pEnt->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE_JIGS ) // Always display outline sketch surface in transparent
	{
		if ( expectedDisplayMode == DISP_MODE_WIREFRAME || expectedDisplayMode == DISP_MODE_DISPLAY ||
			 expectedDisplayMode == DISP_MODE_DISPLAY_NET || expectedDisplayMode == DISP_MODE_DISPLAY_CT ||
			 expectedDisplayMode == DISP_MODE_TRANSPARENCY_NET || expectedDisplayMode == DISP_MODE_TRANSPARENT_CT )
			expectedDisplayMode = DISP_MODE_TRANSPARENCY;
	}

	pEnt->SetDisplayMode( expectedDisplayMode );

	bool bDisplayed = ( expectedDisplayMode != DISP_MODE_HIDDEN );

	items[0]->child(row, 0)->setCheckState( bDisplayed ? Qt::Checked : Qt::Unchecked );
	m_pDoc->SetModified(true);

	m_pDoc->GetView()->Redraw();
}

