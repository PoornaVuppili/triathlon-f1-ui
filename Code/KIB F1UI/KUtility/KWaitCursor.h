#ifndef KWAITCURSOR_H
#define KWAITCURSOR_H

#include <QApplication>

class KWaitCursor
{
public:
    KWaitCursor(bool start = true)
	{ 
		if (start)
			Start(); 
	}

    ~KWaitCursor() { End(); }

	void Start()
	{ 
		QCursor* pC = QApplication::overrideCursor();
		if (!pC || pC->shape() != Qt::WaitCursor)
			QApplication::setOverrideCursor(Qt::WaitCursor); 
	}

	void End() { QApplication::restoreOverrideCursor(); }
};

#endif //KWAITCURSOR_H

