/********************************************************************************
** Form generated from reading UI file 'NewImplant.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWIMPLANT_H
#define UI_NEWIMPLANT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewImplantClass
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *hboxLayout;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;
    QLabel *label;
    QLineEdit *lineEditImplantNum;
    QPushButton *pushButtonQuery;
    QLabel *labelQueryResult;
    QWidget *layoutWidget2;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QRadioButton *radioButtonTotalPS;
    QRadioButton *radioButtonTotal;
    QRadioButton *radioButtonUni;
    QRadioButton *radioButtonDuo;
    QLabel *labelKnee;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *radioButtonLeft;
    QRadioButton *radioButtonRight;
    QSpacerItem *horizontalSpacer;
    QLabel *labelCompartment;
    QHBoxLayout *horizontalLayout_3;
    QRadioButton *radioButtonMedial;
    QRadioButton *radioButtonLateral;
    QSpacerItem *horizontalSpacer_2;
    QLabel *labelCountry;
    QHBoxLayout *horizontalLayout_4;
    QComboBox *comboBoxCountry;
    QComboBox *comboBoxZone;
    QSpacerItem *horizontalSpacer_3;
    QButtonGroup *buttonGroupType;
    QButtonGroup *buttonGroupKnee;
    QButtonGroup *buttonGroupCompartment;

    void setupUi(QDialog *NewImplantClass)
    {
        if (NewImplantClass->objectName().isEmpty())
            NewImplantClass->setObjectName(QString::fromUtf8("NewImplantClass"));
        NewImplantClass->resize(456, 240);
        layoutWidget = new QWidget(NewImplantClass);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(244, 200, 203, 30));
        hboxLayout = new QHBoxLayout(layoutWidget);
        hboxLayout->setSpacing(15);
        hboxLayout->setContentsMargins(11, 11, 11, 11);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        pushButtonOK = new QPushButton(layoutWidget);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        hboxLayout->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(layoutWidget);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        hboxLayout->addWidget(pushButtonCancel);

        label = new QLabel(NewImplantClass);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(11, 12, 148, 20));
        lineEditImplantNum = new QLineEdit(NewImplantClass);
        lineEditImplantNum->setObjectName(QString::fromUtf8("lineEditImplantNum"));
        lineEditImplantNum->setGeometry(QRect(165, 12, 153, 20));
        pushButtonQuery = new QPushButton(NewImplantClass);
        pushButtonQuery->setObjectName(QString::fromUtf8("pushButtonQuery"));
        pushButtonQuery->setGeometry(QRect(328, 8, 113, 25));
        pushButtonQuery->setAutoDefault(false);
        labelQueryResult = new QLabel(NewImplantClass);
        labelQueryResult->setObjectName(QString::fromUtf8("labelQueryResult"));
        labelQueryResult->setGeometry(QRect(12, 40, 433, 16));
        layoutWidget2 = new QWidget(NewImplantClass);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(20, 80, 421, 113));
        gridLayout = new QGridLayout(layoutWidget2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        radioButtonTotalPS = new QRadioButton(layoutWidget2);
        buttonGroupType = new QButtonGroup(NewImplantClass);
        buttonGroupType->setObjectName(QString::fromUtf8("buttonGroupType"));
        buttonGroupType->addButton(radioButtonTotalPS);
        radioButtonTotalPS->setObjectName(QString::fromUtf8("radioButtonTotalPS"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(radioButtonTotalPS->sizePolicy().hasHeightForWidth());
        radioButtonTotalPS->setSizePolicy(sizePolicy);
        radioButtonTotalPS->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(radioButtonTotalPS);

        radioButtonTotal = new QRadioButton(layoutWidget2);
        buttonGroupType->addButton(radioButtonTotal);
        radioButtonTotal->setObjectName(QString::fromUtf8("radioButtonTotal"));
        sizePolicy.setHeightForWidth(radioButtonTotal->sizePolicy().hasHeightForWidth());
        radioButtonTotal->setSizePolicy(sizePolicy);
        radioButtonTotal->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(radioButtonTotal);

        radioButtonUni = new QRadioButton(layoutWidget2);
        buttonGroupType->addButton(radioButtonUni);
        radioButtonUni->setObjectName(QString::fromUtf8("radioButtonUni"));
        radioButtonUni->setEnabled(false);
        sizePolicy.setHeightForWidth(radioButtonUni->sizePolicy().hasHeightForWidth());
        radioButtonUni->setSizePolicy(sizePolicy);
        radioButtonUni->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(radioButtonUni);

        radioButtonDuo = new QRadioButton(layoutWidget2);
        buttonGroupType->addButton(radioButtonDuo);
        radioButtonDuo->setObjectName(QString::fromUtf8("radioButtonDuo"));
        radioButtonDuo->setEnabled(false);
        sizePolicy.setHeightForWidth(radioButtonDuo->sizePolicy().hasHeightForWidth());
        radioButtonDuo->setSizePolicy(sizePolicy);
        radioButtonDuo->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(radioButtonDuo);


        gridLayout->addLayout(horizontalLayout, 0, 1, 1, 2);

        labelKnee = new QLabel(layoutWidget2);
        labelKnee->setObjectName(QString::fromUtf8("labelKnee"));

        gridLayout->addWidget(labelKnee, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        radioButtonLeft = new QRadioButton(layoutWidget2);
        buttonGroupKnee = new QButtonGroup(NewImplantClass);
        buttonGroupKnee->setObjectName(QString::fromUtf8("buttonGroupKnee"));
        buttonGroupKnee->addButton(radioButtonLeft);
        radioButtonLeft->setObjectName(QString::fromUtf8("radioButtonLeft"));

        horizontalLayout_2->addWidget(radioButtonLeft);

        radioButtonRight = new QRadioButton(layoutWidget2);
        buttonGroupKnee->addButton(radioButtonRight);
        radioButtonRight->setObjectName(QString::fromUtf8("radioButtonRight"));

        horizontalLayout_2->addWidget(radioButtonRight);


        gridLayout->addLayout(horizontalLayout_2, 1, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 2, 1, 1);

        labelCompartment = new QLabel(layoutWidget2);
        labelCompartment->setObjectName(QString::fromUtf8("labelCompartment"));

        gridLayout->addWidget(labelCompartment, 2, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        radioButtonMedial = new QRadioButton(layoutWidget2);
        buttonGroupCompartment = new QButtonGroup(NewImplantClass);
        buttonGroupCompartment->setObjectName(QString::fromUtf8("buttonGroupCompartment"));
        buttonGroupCompartment->addButton(radioButtonMedial);
        radioButtonMedial->setObjectName(QString::fromUtf8("radioButtonMedial"));
        radioButtonMedial->setEnabled(false);

        horizontalLayout_3->addWidget(radioButtonMedial);

        radioButtonLateral = new QRadioButton(layoutWidget2);
        buttonGroupCompartment->addButton(radioButtonLateral);
        radioButtonLateral->setObjectName(QString::fromUtf8("radioButtonLateral"));
        radioButtonLateral->setEnabled(false);

        horizontalLayout_3->addWidget(radioButtonLateral);


        gridLayout->addLayout(horizontalLayout_3, 2, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 2, 2, 1, 1);

        labelCountry = new QLabel(layoutWidget2);
        labelCountry->setObjectName(QString::fromUtf8("labelCountry"));

        gridLayout->addWidget(labelCountry, 3, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        comboBoxCountry = new QComboBox(layoutWidget2);
        comboBoxCountry->setObjectName(QString::fromUtf8("comboBoxCountry"));

        horizontalLayout_4->addWidget(comboBoxCountry);

        comboBoxZone = new QComboBox(layoutWidget2);
        comboBoxZone->setObjectName(QString::fromUtf8("comboBoxZone"));

        horizontalLayout_4->addWidget(comboBoxZone);


        gridLayout->addLayout(horizontalLayout_4, 3, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 3, 2, 1, 1);

        QWidget::setTabOrder(lineEditImplantNum, pushButtonQuery);
        QWidget::setTabOrder(pushButtonQuery, radioButtonDuo);
        QWidget::setTabOrder(radioButtonDuo, radioButtonLeft);
        QWidget::setTabOrder(radioButtonLeft, radioButtonRight);
        QWidget::setTabOrder(radioButtonRight, radioButtonMedial);
        QWidget::setTabOrder(radioButtonMedial, radioButtonLateral);
        QWidget::setTabOrder(radioButtonLateral, comboBoxCountry);
        QWidget::setTabOrder(comboBoxCountry, comboBoxZone);
        QWidget::setTabOrder(comboBoxZone, pushButtonOK);
        QWidget::setTabOrder(pushButtonOK, pushButtonCancel);

        retranslateUi(NewImplantClass);

        pushButtonCancel->setDefault(false);


        QMetaObject::connectSlotsByName(NewImplantClass);
    } // setupUi

    void retranslateUi(QDialog *NewImplantClass)
    {
        NewImplantClass->setWindowTitle(QCoreApplication::translate("NewImplantClass", "Create a new implant", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("NewImplantClass", "OK", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("NewImplantClass", "Cancel", nullptr));
        label->setText(QCoreApplication::translate("NewImplantClass", "Enter the new implant number:", nullptr));
        pushButtonQuery->setText(QCoreApplication::translate("NewImplantClass", "Query database", nullptr));
        labelQueryResult->setText(QString());
        label_2->setText(QCoreApplication::translate("NewImplantClass", "Select the implant type:", nullptr));
        radioButtonTotalPS->setText(QCoreApplication::translate("NewImplantClass", "TriathlonF1PS", nullptr));
        radioButtonTotal->setText(QCoreApplication::translate("NewImplantClass", "TriathlonF1", nullptr));
        radioButtonUni->setText(QCoreApplication::translate("NewImplantClass", "iUni", nullptr));
        radioButtonDuo->setText(QCoreApplication::translate("NewImplantClass", "iDuo", nullptr));
        labelKnee->setText(QCoreApplication::translate("NewImplantClass", "Select the knee:", nullptr));
        radioButtonLeft->setText(QCoreApplication::translate("NewImplantClass", "Left", nullptr));
        radioButtonRight->setText(QCoreApplication::translate("NewImplantClass", "Right", nullptr));
        labelCompartment->setText(QCoreApplication::translate("NewImplantClass", "Select the compartment:", nullptr));
        radioButtonMedial->setText(QCoreApplication::translate("NewImplantClass", "Medial", nullptr));
        radioButtonLateral->setText(QCoreApplication::translate("NewImplantClass", "Lateral", nullptr));
        labelCountry->setText(QCoreApplication::translate("NewImplantClass", "Select the country, zone:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NewImplantClass: public Ui_NewImplantClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWIMPLANT_H
