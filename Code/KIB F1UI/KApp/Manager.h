#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <qpoint.h>
#include <qevent.h>
#include <QFocusEvent>
#pragma warning( disable : 4996 )
#include "IwVector3d.h"
#include "IwExtent3d.h"
#pragma warning( default : 4996)
#include "Measure.h"
class Viewport;
class QUndoStack;
class QToolBar;

class Manager : public QObject
{
    Q_OBJECT

public:
    Manager(QObject *parent=NULL, QUndoStack* stack=NULL);
    virtual ~Manager();

    // Event processing
    //  cursor      : mouse cursor position in SCREEN coordinates
    //  keyModifier : key modifiers
    //  viewport    : the viewport where the mouse/keyboard event take place
    // Return:
    //  true  : the calling function should continue response
    //  false : ........................... stop passing along the message

    // Called when the mouse button is pressed
    virtual bool MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }
	virtual bool MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }
	virtual bool MouseRight ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }

    // Called when the mouse button is released
	virtual bool MouseLeftUp  ( const QPoint& cursor, Viewport* viewport=NULL )    { return MouseUp( cursor, viewport ); }
	virtual bool MouseMiddleUp( const QPoint& cursor, Viewport* viewport=NULL )    { return MouseUp( cursor, viewport ); }
	virtual bool MouseRightUp ( const QPoint& cursor, Viewport* viewport=NULL )    { return MouseUp( cursor, viewport ); }

    // Called when the mouse is moved while the left button is pressed
    virtual bool MouseLeftMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }
    // Called when the mouse is moved while the middle button is pressed
    virtual bool MouseMiddleMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }
  // Called when the mouse is moved while the right button is pressed
    virtual bool MouseRightMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }
  
	// Called when the mouse is moved while no button is pressed
	virtual bool MouseNoneMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* viewport=NULL ) { return true; }

    // Called when scrolling using the mouse wheel
	virtual bool MouseWheel ( QWheelEvent * event, Viewport* viewport=NULL ) { return true; }

    // Called when double clicking the mouse left button
	virtual bool MouseDoubleClickLeft( const QPoint& cursor, Viewport* viewport=NULL ) { return true; }

    // Called when a key is pressed
	virtual bool keyPressEvent( QKeyEvent* event, Viewport* viewport=NULL )  { return true; }

    // Called when a key is released
	virtual bool keyReleaseEvent( QKeyEvent* ev, Viewport* viewport=NULL )  { return true; }

    // Called when receiving focus
	virtual bool focusInEvent( QFocusEvent* event, Viewport* viewport=NULL )  { return true; }

    // Called when losing focus
	virtual bool focusOutEvent( QFocusEvent* ev, Viewport* viewport=NULL )  { return true; }

    // Display the content
    virtual void Display(Viewport* viewport=NULL) {}

	virtual void DisplayAfterBuffersSwap(Viewport* viewport=NULL) {}

    // Display the content
    virtual void DisplayTransparent(Viewport* viewport=NULL) {}

    virtual IwExtent3d GetShowRegion() const { return IwExtent3d(); } // nothing to report

    // Get the widget, toolbar associated with the manager
    QWidget* GetWidget() { return widget; }
    QToolBar* GetToolBar() { return m_toolbar; }

    // Pick the entity for measurement
    //  (I) ray         : a ray that picks an entity for measurement
    //  (I) tolerance   : the tolerance for picking
    //  (O) ent         : the picked entity
    //  (I, optional) mask: the candidate entity types of selection, default to all types 
	//  (I, optional) entToAvoid: the entity to avoid picking (maybe picked already)
    // Return:
    //  true        : successful and the picked entity is 'ent' with the picked point 'pt', 
    //  false       : failed
    virtual bool Measure(const PickRayFloat& ray, float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL) { return false; }

    ///////////////////////////////////
    /// Undo/redo Related
    ///////////////////////////////////

    // Check if the undostack is available
    bool HasUndoStack() const { return undoStack!=NULL; }

    // Get the undostack
    QUndoStack* GetUndoStack() { return undoStack; }

    // Enable/disable the undostack
    void EnableUndoStack(bool enable);

    // common functionality for all managers
	void SetId(QString id)	{	_id = id;	}
	QString GetId()			{	return _id;	}

    virtual bool GetHint(QString& str) const { return false; }

signals:
    void finishedByAccept();
    void finishedByCancel();
    void finishedByReviewNext();
    void finishedByReviewPrev();
    void finishedByRework();
    void finishedAbnormally();

private:
   struct ExitSignaller
   {
      enum Reason { DismissedByCancel, DismissedByAccept,
                    DismissedByReviewNext, DismissedByReviewPrev, DismissedByRework,
                    ExceptionOrOtherError } reason;
      Manager *papa;
      ExitSignaller(Manager *pM) : reason(DismissedByCancel), papa(pM) {}
      ~ExitSignaller() { Manager::signalTheEnd(papa, reason); }
      void setReason(Reason r) { reason = r; }
   };

protected:
    virtual bool MouseUp( const QPoint& cursor, Viewport* viewport=NULL ) { return true; }

    ExitSignaller m_reporter; // when destructed, emits the proper signal

    // widget/toolbar associated with the manager. Null if not exists.
    QWidget*    widget;
    QToolBar*   m_toolbar;

    // undostack
    QUndoStack* undoStack;
    bool sharedStack;    // whether the stack is shared

	QString _id;

	QPoint cursorDownPoint;

    // shared cursors for the manager and derived
    static QCursor *rotateCursor;
    static QCursor *arrowBox,   *arrowBoxMinus,  *arrowBoxPlus;
    static QCursor *arrowLasso, *arrowLassoPlus, *arrowLassoMinus;

private:

   static void signalTheEnd(Manager* pM, ExitSignaller::Reason reason)
   {
      switch (reason)
      {
      case ExitSignaller::DismissedByCancel:
         emit pM->finishedByCancel(); break;
      case ExitSignaller::DismissedByAccept:
         emit pM->finishedByAccept(); break;
      case ExitSignaller::DismissedByReviewNext:
         emit pM->finishedByReviewNext(); break;
      case ExitSignaller::DismissedByReviewPrev:
         emit pM->finishedByReviewPrev(); break;
      case ExitSignaller::DismissedByRework:
         emit pM->finishedByRework(); break;
      case ExitSignaller::ExceptionOrOtherError:
         emit pM->finishedAbnormally(); break;
      }
   }

   void doDismiss();

protected:
   void dismissOnAccept();
   void dismissOnCancel();
   void dismissOnReviewNext();
   void dismissOnReviewPrev();
   void dismissOnRework();
   void noExitReasonYet();
};

#endif // MANAGER_H
