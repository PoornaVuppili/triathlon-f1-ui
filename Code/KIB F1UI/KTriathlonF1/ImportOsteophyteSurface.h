#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

class TEST_EXPORT_TW CImportOsteophyteSurface : public CManager
{
    Q_OBJECT

public:
					CImportOsteophyteSurface( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CImportOsteophyteSurface();

	static void		CreateOsteophyteSurfaceObject(CTotalDoc* pDoc);
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

private:
	void			Reset(bool activateUI=true);

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnCancel();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actImportOsteophyteSurf;
	QAction*					m_actReset;


};
