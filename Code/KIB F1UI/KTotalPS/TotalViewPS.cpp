#include <QtGui>
#include "TotalViewPS.h"
#include "TotalDocPS.h"
#include "TotalMainWindowPS.h"
#include "..\KApp\SwitchBoard.h"
#include "../KTriathlonF1/IFemurImplant.h"
#include "../KTriathlonF1/IFemurJigs.h"

#include <QtPrintSupport\qprinter.h>
#include <QtPrintSupport\qprintdialog.h>

KTotalViewPS::KTotalViewPS( QObject *parent )
    : CTotalView( parent )
{
	m_validationReportPSDlg = NULL;
	m_validationReportPSTextEdit = NULL;
}


KTotalViewPS::~KTotalViewPS()
{
	SwitchBoard::removeSignalReceiver(this);
}

void KTotalViewPS::OnSetModifiedMarkerToFemurPSFeatures()
{
}

// action 0: create/reset all
// action 1: delete all (if exist)
// action 2: delete cast implant and reset the rest if exist
bool KTotalViewPS::OnAutoInitializeCBSteps(int action)
{

	CEntity* pEnt;
	if ( action == 0 ) // create/reset all
	{

	}
	else if ( action == 1 )
	{
		GetTotalDocPS()->DeleteEntitiesBelongToPSFeatures();
	}
	
	return true;
}

void KTotalViewPS::OnAcceptValidationReportPS()
{
	if ( m_validationReportPSDlg )
		delete m_validationReportPSDlg;
	m_validationReportPSTextEdit = NULL;// when delete m_validationReportPSDlg, m_validationReportTextEdit should also be deteted.
}

void KTotalViewPS::OnPrintValidationReportPS(QString outputFileName)
{
	bool allGoodResults=true;
	QString totalReportMessage ;
	totalReportMessage = GetTotalDocPS()->GetValidationReportPS(allGoodResults);

	QPrinter printer;
	printer.setPageSize(QPrinter::A4);
	printer.setFullPage (true);
	if ( outputFileName.isEmpty() )// print to a printer
	{
		QPrintDialog *dialog = new QPrintDialog (&printer, m_validationReportPSTextEdit);
		if (dialog->exec() == QDialog::Accepted)
		{
			m_validationReportPSTextEdit->print(&printer);
		}
	}
	else // print to a given file, default *.pdf format and *.txt format
	{
		printer.setOutputFileName( outputFileName );
		m_validationReportPSTextEdit->print(&printer);
		// delete m_validationReportPSTextEdit
		delete m_validationReportPSTextEdit;
		m_validationReportPSTextEdit = NULL;
		// Also write to a text file
		QString outputFileNameText = outputFileName;
		int sLength = outputFileNameText.length();
		if (outputFileNameText.endsWith(".pdf"))
			outputFileNameText = outputFileNameText.left(sLength-4); // remove ".pdf"
		outputFileNameText = outputFileNameText + QString(".txt");
		QFile			File( outputFileNameText );
		QTextStream		out( &File );
		bool bOK = File.open( QIODevice::WriteOnly | QIODevice::Text );
		if( !bOK )
			return;
		out << totalReportMessage;
		File.close();	
		//
		OnAcceptValidationReportPS();
	}


}

//////////////////////////////////////////////////////////////////////////////////
// This function will delete the existing manager and activate the next manager.
void KTotalViewPS::OnActivateNextReviewManager(CManager* pManagerToBeDeleted, CEntRole eRole, bool bNext)
{
	if (pManagerToBeDeleted)
	{
		pManagerToBeDeleted->SetDestroyMe(); // set the flag
		Draw(); // call Draw() to delete the manager immediately
	}

	if ( eRole == ENT_ROLE_IMPLANT_PS_NONE )
	{

	}
}

void KTotalViewPS::OnGoToJigsReviewManager()
{
	// Go to jigs review step
	IFemurJigs::Review_StartReviewJigs();

}
