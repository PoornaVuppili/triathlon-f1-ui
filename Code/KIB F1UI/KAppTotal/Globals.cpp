#include <windows.h>
#include <QtOpenGL>
#include "Globals.h"
#include "OpenGLView.h"
#pragma warning( disable : 4996 )
//#include <iwfillet_extern.h>
#pragma warning( default : 4996 )

CColor		white	= CColor( 255, 255, 255 );
CColor		red		= CColor( 255, 0, 0 );
CColor		darkRed		= CColor( 128, 0, 0 );
CColor		dDarkRed		= CColor( 64, 0, 0 );
CColor		green	= CColor( 0, 255, 0 );
CColor		darkGreen	= CColor( 42, 127, 42 );
CColor		blue	= CColor( 0, 0, 255 );
CColor		cyan	= CColor( 0, 255, 255 );
CColor		darkCyan	= CColor( 0, 96, 96 );
CColor		magenta	= CColor( 255, 0, 255 );
CColor		darkMagenta	= CColor( 48, 0, 48 );
CColor		lightYellow	= CColor( 255, 255, 128 );
CColor		yellow	= CColor( 255, 255, 0 );
CColor		darkYellow	= CColor( 152, 128, 0 );
CColor		orange	= CColor( 255, 127, 0 );
CColor		black	= CColor( 0, 0, 0 );
CColor		darkGray= CColor( 105, 105, 105 );
CColor		gray	= CColor( 192, 192, 192 );
CColor		brown	= CColor( 191,  63,  63 );
CColor		darkBrown	= CColor( 95,  30,  30 );
CColor		cPink		= CColor( 253,  171,  241 );
CColor		mPurple	= CColor( 174,  100,  135 );

CVector3d	vec_x	= CVector3d( 1.0, 0.0, 0.0 );
CVector3d	vec_y	= CVector3d( 0.0, 1.0, 0.0 );
CVector3d	vec_z	= CVector3d( 0.0, 0.0, 1.0 );
CVector3d	zero	= CVector3d( 0.0, 0.0, 0.0 );


void Msg( const QString& sMsg )
	{
	QMessageBox::information( NULL, "", sMsg );
	}


void InitStringPref( const QString& sName, const QString& sValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	QVariant		var;
	QString			sCurValue;

	var = regSettings.value( sName, var );

	if( var.isNull() )
		{
		regSettings.setValue( sName, sValue );
		}
	}


void SetStringPref( const QString& sName, const QString& sValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );

	regSettings.setValue( sName, sValue );
	}


QString GetStringPref( const QString& sName )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	QString			sValue;
	QVariant		var;

	var = regSettings.value( sName, var );

	sValue = var.toString();

	return sValue;
	}


void InitDoublePref( const QString& sName, double fValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	QVariant		var;

	var = regSettings.value( sName, var );

	if( var.isNull() )
		{
		regSettings.setValue( sName, fValue );
		}
	}


void SetDoublePref( const QString& sName, double fValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );

	regSettings.setValue( sName, fValue );
	}


double GetDoublePref( const QString& sName )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	double			fValue;
	QVariant		var;

	var = regSettings.value( sName, var );

	fValue = var.toDouble();

	return fValue;
	}


void InitIntPref( const QString& sName, int iValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	QVariant		var;

	var = regSettings.value( sName, var );

	if( var.isNull() )
		{
		regSettings.setValue( sName, iValue );
		}
	}


void SetIntPref( const QString& sName, int iValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );

	regSettings.setValue( sName, iValue );
	}


int GetIntPref( const QString& sName )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	int				iValue;
	QVariant		var;

	var = regSettings.value( sName, var );

	iValue = var.toInt();

	return iValue;
	}


void InitBoolPref( const QString& sName, bool bValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	QVariant		var;

	var = regSettings.value( sName, var );

	if( var.isNull() )
		{
		regSettings.setValue( sName, bValue );
		}
	}


void SetBoolPref( const QString& sName, bool bValue )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );

	regSettings.setValue( sName, bValue );
	}


bool GetBoolPref( const QString& sName )
	{
	QSettings		regSettings( "Stryker", "TriathlonF1" );
	bool			bValue;
	QVariant		var;

	var = regSettings.value( sName, var );

	bValue = var.toBool();

	return bValue;
	}


QString GetFileDir( const QString& sPathName )
	{
	int			idx, idx1, idx2;
	QString		sDir;

	idx1 = sPathName.lastIndexOf( "/" );
	idx2 = sPathName.lastIndexOf( "\\" );
	idx = max( idx1, idx2 );

	sDir = sPathName.left( idx );

	return sDir;
	}

// global function name should be unique (or longer) to avoid confliction
QString GetFileNameWithExtension( const QString& sPathName )
	{
	int			idx, idx1, idx2;
	QString		sName;

	idx1 = sPathName.lastIndexOf( "/" );
	idx2 = sPathName.lastIndexOf( "\\" );
	idx = max( idx1, idx2 );

	sName = sPathName.mid( idx + 1 );

	return sName;
	}


QString GetFileExt( const QString& sPathName )
	{
	int			idx;
	QString		sExt;

	idx = sPathName.lastIndexOf( "." );

	sExt = sPathName.mid( idx + 1 );

	return sExt;
	}


void QStringToCharArray( const QString& sText, char* out )
	{
	QByteArray			byteArray = sText.toLatin1();
	char*				data = byteArray.data();

	strcpy( out, data );
	}


void InvertPointArray( CPointArray& pts )
	{
	int				i, n;
	CPointArray		points( pts );

	n = pts.GetSize();

	for( i = 0; i < n; i++ )
		pts[i] = points[n-1-i];
	}


void ErrMes( const QString& sMsg )
	{
	QMessageBox::critical( NULL, "", sMsg );	
	}


bool QuestionMes( const QString& sMsg )
	{
	int button = QMessageBox::question( NULL, "", sMsg, QMessageBox::Yes, QMessageBox::No, QMessageBox::NoButton );	

	return ( button == QMessageBox::Yes );
	}


double PutInRange( double val, double min, double max )
	{
	val = std::max( val, min );
	val = std::min( val, max );

	return val;
	}

bool Solve2by2LinearSystem(
	double					a00,
	double					a01,
	double					a10,
	double					a11,
	double					b0,
	double					b1, 
	double&					x,
	double&					y )
	{
	double					det;
	double					eps = 1e-6;

	det = a00 * a11 - a01 * a10;

	if( fabs( det ) < eps )
		return false;

	x = ( b0 * a11 - b1 * a01 ) / det;
	y = ( a00 * b1 - a10 * b0 ) / det;

	return true;
	}


double Det( double m00, double m01, double m02,
			double m10, double m11, double m12,
			double m20, double m21, double m22 )
	{
	return		m00 * ( m11 * m22 - m21 * m12 ) - 
				m01 * ( m10 * m22 - m20 * m12 ) +
				m02 * ( m10 * m21 - m20 * m11 );
	}


bool Solve3by3LinearSystem(
	double					m[3][3],
	double					f[3],
	double					xyz[3] )
	{
	double					det, detX, detY, detZ;
	double					eps = 1e-8;

	det = Det(	m[0][0], m[0][1], m[0][2],
				m[1][0], m[1][1], m[1][2],
				m[2][0], m[2][1], m[2][2] );

	if( fabs( det ) < eps )
		return false;

	detX = Det(	f[0], m[0][1], m[0][2],
				f[1], m[1][1], m[1][2],
				f[2], m[2][1], m[2][2] );

	detY = Det( m[0][0], f[0], m[0][2],
				m[1][0], f[1], m[1][2],
				m[2][0], f[2], m[2][2] );

	detZ = Det(	m[0][0], m[0][1], f[0],
				m[1][0], m[1][1], f[1],
				m[2][0], m[2][1], f[2] );

	xyz[0] = detX / det;
	xyz[1] = detY / det;
	xyz[2] = detZ / det;

	return true;
	}



void DistributePointsOnSegment(
	double					d,
	double					d0,
	double					d1,
	int						n,
	CDoubleArray&			arr )
	{
	double					a, b, c, x, x2, x3, x4;
	double					x1, x12, x13, x14, x15;
	double					m[3][3], f[3], abc[3];
	bool					bOK;
	int						i;

	x1 = n + 1;
	x12 = x1 * x1;
	x13 = x12 * x1;
	x14 = x13 * x1;
	x15 = x14 * x1;

	m[0][0] = x14;		m[0][1] = x13;		m[0][2] = x12;		f[0] = d1 - d0;
	m[1][0] = 4*x12;	m[1][1] = 3*x1;		m[1][2] = 2.0;		f[1] = 0.0;
	m[2][0] = x15/5;	m[2][1] = x14/4;	m[2][2] = x13/3;	f[2] = d + ( d0 + d1 ) / 2 - d0 * x1;

	bOK = Solve3by3LinearSystem( m, f, abc );

	a = abc[0];
	b = abc[1];
	c = abc[2];

	arr.SetSize( n );

	for( i = 0; i < n; i++ )
		{
		x = 1.0 * ( i + 1 );
		x2 = x * x;
		x3 = x2 * x;
		x4 = x3 * x;

		arr[i] = a * x4 + b * x3 + c * x2 + d0;
		}
	}


bool IsValidationMode()
	{
	//char*	env = getenv( "TriathlonF1_Validation" );

	//if( env == NULL )
	//	return false;

	//QString	sEnv = QString( env );

	//return ( sEnv == "true" );

	QString		sValidation = GetStringPref( "Validation Mode" );

	return ( sValidation == "Yes" );
	}


void DeleteDocDir( const QString& sPath )
	{
	QDir				dir( sPath );
	QStringList			filters;
	QString				sFileName;

	filters += "*.*";

	foreach( QString sFile, dir.entryList( filters, QDir::Files ) )
		{
		sFileName = sPath + "\\" + sFile;
		QFile( sFileName ).remove();
		}

	dir.rmdir( sPath );
	}


bool IsProductionFolder( const QString sDir )
{
	int                                 idx;
	QString                             sRootDir, sProductionFile;
	QRegExp                             rx;
      
	rx = QRegExp( "\\b(UNI|archive)\\b" );

	idx = sDir.indexOf( rx );

	if( idx >= 0 )
	{
		sRootDir = sDir.left( idx );

		sProductionFile = sRootDir + ".production";

		if( QFile::exists( sProductionFile ) )
		{
			return true;
		}
	}

	return false;
}
