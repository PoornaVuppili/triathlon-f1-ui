#include "InfoTextReader.h"
#include <QFile>
#include <QTextStream>

CInfoTextReader::CInfoTextReader( const QString& sFileName )
{
	m_bFileIsLoaded = false;

	if( !QFile::exists(sFileName) ) return;

	QFile file( sFileName );

	QTextStream	in( &file );

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

	QString sLine;
	QString equalSign("=");
	int idx;
	do
	{
		sLine = in.readLine();
		idx = sLine.indexOf( equalSign );
		m_tags.append(sLine.left( idx ));
		m_tagContents.append( sLine.mid( idx + 1 ) );

	} while ( !in.atEnd() );

	file.close();
	
	m_bFileIsLoaded = true;

	return;
}


CInfoTextReader::~CInfoTextReader()
{
	
}

QString CInfoTextReader::GetString(const QString& tag)
{
	QString tagContent;

	for (int i=0; i<m_tags.size(); i++)
	{
		if ( tag == m_tags.at(i) )
		{
			tagContent = m_tagContents.at(i);
			break;
		}
	}

	return tagContent;
}