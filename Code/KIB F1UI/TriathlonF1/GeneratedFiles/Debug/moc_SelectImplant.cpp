/****************************************************************************
** Meta object code from reading C++ file 'SelectImplant.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../SelectImplant.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SelectImplant.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SelectImplant_t {
    QByteArrayData data[14];
    char stringdata0[320];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SelectImplant_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SelectImplant_t qt_meta_stringdata_SelectImplant = {
    {
QT_MOC_LITERAL(0, 0, 13), // "SelectImplant"
QT_MOC_LITERAL(1, 14, 27), // "on_pushButtonBrowse_clicked"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 30), // "on_pushButtonNewSerial_clicked"
QT_MOC_LITERAL(4, 74, 27), // "on_LineEdit_editingFinished"
QT_MOC_LITERAL(5, 102, 33), // "on_pushButtonOpenReformat_cli..."
QT_MOC_LITERAL(6, 136, 29), // "on_pushButtonReformat_clicked"
QT_MOC_LITERAL(7, 166, 27), // "on_pushButtonImport_clicked"
QT_MOC_LITERAL(8, 194, 32), // "on_comboImplantList_indexChanged"
QT_MOC_LITERAL(9, 227, 27), // "on_pushButtonCancel_clicked"
QT_MOC_LITERAL(10, 255, 23), // "on_pushButtonOK_clicked"
QT_MOC_LITERAL(11, 279, 21), // "on_allOptStateChanged"
QT_MOC_LITERAL(12, 301, 5), // "state"
QT_MOC_LITERAL(13, 307, 12) // "onAdjustSize"

    },
    "SelectImplant\0on_pushButtonBrowse_clicked\0"
    "\0on_pushButtonNewSerial_clicked\0"
    "on_LineEdit_editingFinished\0"
    "on_pushButtonOpenReformat_clicked\0"
    "on_pushButtonReformat_clicked\0"
    "on_pushButtonImport_clicked\0"
    "on_comboImplantList_indexChanged\0"
    "on_pushButtonCancel_clicked\0"
    "on_pushButtonOK_clicked\0on_allOptStateChanged\0"
    "state\0onAdjustSize"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SelectImplant[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    0,   71,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    0,   73,    2, 0x08 /* Private */,
       7,    0,   74,    2, 0x08 /* Private */,
       8,    1,   75,    2, 0x08 /* Private */,
       9,    0,   78,    2, 0x08 /* Private */,
      10,    0,   79,    2, 0x08 /* Private */,
      11,    1,   80,    2, 0x08 /* Private */,
      13,    0,   83,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,

       0        // eod
};

void SelectImplant::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SelectImplant *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButtonBrowse_clicked(); break;
        case 1: _t->on_pushButtonNewSerial_clicked(); break;
        case 2: _t->on_LineEdit_editingFinished(); break;
        case 3: _t->on_pushButtonOpenReformat_clicked(); break;
        case 4: _t->on_pushButtonReformat_clicked(); break;
        case 5: _t->on_pushButtonImport_clicked(); break;
        case 6: _t->on_comboImplantList_indexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_pushButtonCancel_clicked(); break;
        case 8: _t->on_pushButtonOK_clicked(); break;
        case 9: _t->on_allOptStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->onAdjustSize(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SelectImplant::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_SelectImplant.data,
    qt_meta_data_SelectImplant,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SelectImplant::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SelectImplant::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SelectImplant.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int SelectImplant::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
