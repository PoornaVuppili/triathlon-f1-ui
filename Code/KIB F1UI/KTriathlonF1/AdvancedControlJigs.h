#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Entity.h"
#include "..\KAppTotal\Basics.h"

class CTotalDoc;

class TEST_EXPORT_TW CAdvancedControlJigs : public CEntity
{
public:
	CAdvancedControlJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_ADVANCED_CONTROL_JIGS, const QString& sFileName="", int nEntId=-1 );
	
	bool			Save( const QString& sDir, bool incrementalSave=true );
	bool			Load( const QString& sDir );
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&) {};
	virtual CBounds3d		GetBounds() {return CBounds3d();};
	virtual CBounds3d		ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL ) {return CBounds3d();};

	// Auto
	bool			GetAutoAllSteps() {return m_autoAllSteps;};
	void			SetAutoAllSteps(bool flag) {m_autoAllSteps=flag;};
	bool			GetAutoDrivenSteps() {return m_autoDrivenSteps;};
	void			SetAutoDrivenSteps(bool flag) {m_autoDrivenSteps=flag;};

	// Osteophyte Surface
	unsigned		GetOsteophyteTimeStampForInner() {return m_osteophyteTimeStampForInner;};
	void			SetOsteophyteTimeStampForInner(unsigned val) {m_osteophyteTimeStampForInner=val;SetModifiedFlag(true);};
	unsigned		GetOsteophyteTimeStampForOuter() {return m_osteophyteTimeStampForOuter;};
	void			SetOsteophyteTimeStampForOuter(unsigned val) {m_osteophyteTimeStampForOuter=val;SetModifiedFlag(true);};

	// Cartilage Surface
	double			GetCartilageThickness() {return m_cartilageThickness;};
	void			SetCartilageThickness(double val) {m_cartilageThickness=val;SetModifiedFlag(true);};
	double			GetTrochlearGrooveAdditionalCartilageThickness() {return m_troGroAddCartilageThickness;};
	void			SetTrochlearGrooveAdditionalCartilageThickness(double val) {m_troGroAddCartilageThickness=val;SetModifiedFlag(true);};
	double			GetTrochlearGrooveCartilageTransitionWidth() {return m_troGroCartilageTransitionWidth;};
	void			SetTrochlearGrooveCartilageTransitionWidth(double val) {m_troGroCartilageTransitionWidth=val;SetModifiedFlag(true);};
	bool			GetCartilageInitializeEdgeOnly() {return m_cartilageInitializeEdgeOnly;};
	void			SetCartilageInitializeEdgeOnly(bool val) {m_cartilageInitializeEdgeOnly=val;};
	unsigned		GetCartilageTimeStamp() {return m_cartilageTimeStamp;};
	void			SetCartilageTimeStamp(unsigned val) {m_cartilageTimeStamp=val;SetModifiedFlag(true);};
	bool			GetCartilageUseWaterTightSurface() {return m_cartilageUseWaterTightSurface;};
	void			SetCartilageUseWaterTightSurface(bool val) {m_cartilageUseWaterTightSurface=val;};
	bool			GetCartilageDisplaySmooth() {return m_cartilageDisplaySmooth;};
	void			SetCartilageDisplaySmooth(bool val) {m_cartilageDisplaySmooth=val;};
	// Outline Profile
	bool			GetOutlineSquareOffPosteriorTip() {return m_outlineSquareOffPosteriorTip;};
	void			SetOutlineSquareOffPosteriorTip(bool val) {m_outlineSquareOffPosteriorTip=val;};
	double			GetOutlineSquareOffCornerRadiusRatio() {return m_outlineSquareOffCornerRadiusRatio;};
	void			SetOutlineSquareOffCornerRadiusRatio(double val) {m_outlineSquareOffCornerRadiusRatio=val;};
	double			GetOutlineSquareOffTransitionFactor() {return m_outlineSquareOffTransitionFactor;};
	void			SetOutlineSquareOffTransitionFactor(double val) {m_outlineSquareOffTransitionFactor=val;};

	// Inner Surface
	double			GetInnerSurfaceApproachDegree() {return m_innerSurfaceApproachDegree;};
	void			SetInnerSurfaceApproachDegree(double val) {m_innerSurfaceApproachDegree=val;SetModifiedFlag(true);};
	double			GetInnerSurfaceDropOffsetDistance() {return m_innerSurfaceDropOffsetDistance;};
	void			SetInnerSurfaceDropOffsetDistance(double val) {m_innerSurfaceDropOffsetDistance=val;SetModifiedFlag(true);};
	double			GetInnerSurfaceAdditionalDropLength() {return m_innerSurfaceAdditionalDropLength;};
	void			SetInnerSurfaceAdditionalDropLength(double val) {m_innerSurfaceAdditionalDropLength=val;SetModifiedFlag(true);};
	double			GetInnerSurfaceAdditionalPosteriorCoverage() {return m_innerSurfaceAdditionalPosteriorCoverage;};
	void			SetInnerSurfaceAdditionalPosteriorCoverage(double val) {m_innerSurfaceAdditionalPosteriorCoverage=val;SetModifiedFlag(true);};
	double			GetInnerSurfaceAnteriorDropDegree() {return m_innerSurfaceAnteriorDropDegree;};
	void			SetInnerSurfaceAnteriorDropDegree(double val) {m_innerSurfaceAnteriorDropDegree=val;SetModifiedFlag(true);};
	bool			GetInnerSurfaceDisplayAnteriorDropSurface() {return m_innerSurfaceDisplayAnteriorDropSurface;};
	void			SetInnerSurfaceDisplayAnteriorDropSurface(bool val) {m_innerSurfaceDisplayAnteriorDropSurface=val;}; // not se SetModifiedFlag(true);
	// Outer Surface
	double			GetOuterSurfaceOffsetDistance() {return m_outerSurfaceOffsetDistance;};
	void			SetOuterSurfaceOffsetDistance(double val) {m_outerSurfaceOffsetDistance=val;SetModifiedFlag(true);};
	double			GetOuterSurfaceTrochlearGrooveAdditionalThickness() {return m_outerSurfaceTroGroAddThickness;};
	void			SetOuterSurfaceTrochlearGrooveAdditionalThickness(double val) {m_outerSurfaceTroGroAddThickness=val;SetModifiedFlag(true);};
	double			GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth() {return m_outerSurfaceTroGroAddThicknessTransitionWidth;};
	void			SetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth(double val) {m_outerSurfaceTroGroAddThicknessTransitionWidth=val;SetModifiedFlag(true);};
	bool			GetOuterSurfaceDisplayAnteriorDropSurface() {return m_outerSurfaceDisplayAnteriorDropSurface;};
	void			SetOuterSurfaceDisplayAnteriorDropSurface(bool val) {m_outerSurfaceDisplayAnteriorDropSurface=val;}; // not use SetModifiedFlag(true);
	double			GetOuterSurfaceDropDegree() {return m_outerSurfaceDropDegree;};
	void			SetOuterSurfaceDropDegree(double val) {m_outerSurfaceDropDegree=val;SetModifiedFlag(true);};
	bool			GetOuterSurfaceEnforceSmooth() {return m_outerSurfaceEnforceSmooth;};
	void			SetOuterSurfaceEnforceSmooth(bool val) {m_outerSurfaceEnforceSmooth=val;};
	// Side Surface
	double			GetSideSurfaceExtendedWidth() {return m_sideSurfaceExtendedWidth;};
	void			SetSideSurfaceExtendedWidth(double val) {m_sideSurfaceExtendedWidth=val;SetModifiedFlag(true);};
	// Solid Position Jigs
	int				GetSolidPositionJigsToleranceLevel() {return m_solidPositionJigsToleranceLevel;};
	void			SetSolidPositionJigsToleranceLevel(int val) {m_solidPositionJigsToleranceLevel=val;};
	// Stylus Jigs
	bool			GetStylusJigsDisplayVirtualFeaturesOnly() {return m_stylusJigsDisplayVirtualFeaturesOnly;};
	void			SetStylusJigsDisplayVirtualFeaturesOnly(bool val) {m_stylusJigsDisplayVirtualFeaturesOnly=val;SetModifiedFlag(true);};
	bool			GetStylusJigsFullControl() {return m_stylusJigsFullControl;};
	void			SetStylusJigsFullControl(bool val) {m_stylusJigsFullControl=val;};
	double			GetStylusJigsClearance() {return m_stylusJigsClearance;};
	void			SetStylusJigsClearance(double val) {m_stylusJigsClearance=val;};
	// Anterior Surface
	double			GetAnteriorSurfaceApproachDegree() {return m_anteriorSurfaceApproachDegree;};
	void			SetAnteriorSurfaceApproachDegree(double val) {m_anteriorSurfaceApproachDegree=val;SetModifiedFlag(true);};
	double			GetAnteriorSurfaceAntExtension() {return m_anteriorSurfaceAntExtension;};
	void			SetAnteriorSurfaceAntExtension(double val) {m_anteriorSurfaceAntExtension=val;};
	double			GetAnteriorSurfaceDropExtension() {return m_anteriorSurfaceDropExtension;};
	void			SetAnteriorSurfaceDropExtension(double val) {m_anteriorSurfaceDropExtension=val;};
	// Output Results
	bool			GetOutputOsteophyteSurface() {return m_outputOsteophyteSurface;};
	void			SetOutputOsteophyteSurface(bool val) {m_outputOsteophyteSurface=val;};
	bool			GetOutputCartilageSurface() {return m_outputCartilageSurface;};
	void			SetOutputCartilageSurface(bool val) {m_outputCartilageSurface=val;};
	bool			GetOutputThreeSurfaces() {return m_outputThreeSurfaces;};
	void			SetOutputThreeSurfaces(bool val) {m_outputThreeSurfaces=val;};
	bool			GetOutputAnteriorSurfaceRegardless() {return m_outputAnteriorSurfaceRegardless;};
	void			SetOutputAnteriorSurfaceRegardless(bool val) {m_outputAnteriorSurfaceRegardless=val;};

protected:

private:
	void			SetModifiedFlag( bool bModified );

private:


	bool			m_bModified;
	bool			m_bDataModified;

	// Auto
	bool			m_autoAllSteps; 
	bool			m_autoDrivenSteps; 

	// Osteophyte Surface
	unsigned		m_osteophyteTimeStampForInner;		// for inner surface creation. when the osteophyte surface is imported. Users will not access this value.
	unsigned		m_osteophyteTimeStampForOuter;		// for cartilage surface creation. when the osteophyte surface is imported. Users will not access this value.

	// Cartilage Surface
	double			m_cartilageThickness; 
	double			m_troGroAddCartilageThickness;// trochlear groove additional cartilage thickness
	double			m_troGroCartilageTransitionWidth;
	bool			m_cartilageInitializeEdgeOnly;
	unsigned		m_cartilageTimeStamp;		// when the cartilage surface is created. Users will not access this value.
	bool			m_cartilageUseWaterTightSurface;
	bool			m_cartilageDisplaySmooth;	// Display "Smooth" button

	// Outline Profile
	bool			m_outlineSquareOffPosteriorTip;
	double			m_outlineSquareOffCornerRadiusRatio;// cornerRadius/posteriotRadius
	double			m_outlineSquareOffTransitionFactor;// times of CornerRadius

	// Inner Surface 
	double			m_innerSurfaceApproachDegree;	 // How the surgeons fit the F1 to femur. If "0", fit along mech z-axis. If >0, the approaching direction deviates (toward anterior) by the degrees.
	double			m_innerSurfaceDropOffsetDistance;// offset distance for the whole dropping surface
	double			m_innerSurfaceAdditionalDropLength;	// additional dropping down length
	double			m_innerSurfaceAdditionalPosteriorCoverage;	// additional posterior coverage
	double			m_innerSurfaceAnteriorDropDegree;// anterior dropping surface angle degrees
	bool			m_innerSurfaceDisplayAnteriorDropSurface;

	// Outer Surface
	double			m_outerSurfaceOffsetDistance; // for jig thickness
	double			m_outerSurfaceTroGroAddThickness;// trochlear groove additional offset thickness
	double			m_outerSurfaceTroGroAddThicknessTransitionWidth;// trochlear groove additional offset thickness transition width, percentage of outline profile jigs
	bool			m_outerSurfaceDisplayAnteriorDropSurface;
	double			m_outerSurfaceDropDegree;		 // dropping angle degrees
	bool			m_outerSurfaceEnforceSmooth;	 // Use inner surface as reference to remove spikes

	// Side Surface
	double			m_sideSurfaceExtendedWidth;

	// Solid Position Jigs
	int				m_solidPositionJigsToleranceLevel;

	// Stylus Jigs
	bool			m_stylusJigsDisplayVirtualFeaturesOnly;
	bool			m_stylusJigsFullControl;
	double			m_stylusJigsClearance;

	// Anterior Surface Jigs
	double			m_anteriorSurfaceApproachDegree;	 // deviate from sketch normal
	double			m_anteriorSurfaceAntExtension;
	double			m_anteriorSurfaceDropExtension;

	// Output Results
	bool			m_outputOsteophyteSurface;
	bool			m_outputCartilageSurface;
	bool			m_outputThreeSurfaces;
	bool			m_outputAnteriorSurfaceRegardless;
};