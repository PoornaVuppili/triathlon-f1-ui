#pragma once

#include "QFileDialog.h"
#include "QSortFilterProxyModel"
#include "QLineEdit.h"
#include "QPushButton.h"
#include "QFileIconProvider.h"


#define		LOAD_DOC	1
#define		SAVE_DOC	2


class CTotalDocDlg : public QFileDialog
	{
    Q_OBJECT

public:
	CTotalDocDlg( QWidget* parent, int iOpenMode, const QString& sDirectory, const QString& filter );

	 void  CenterWindow();

private slots:
	void OnDirEntered( const QString& directory );
	void OnCurrentChanged( const QString& path );
	void OnAccept();

public:
	QLineEdit*		m_pLineEdit;
	QPushButton*	m_pAcceptButton;
	};


class CTotalDocProxyModel : public QSortFilterProxyModel
	{
     Q_OBJECT

 public:
     CTotalDocProxyModel( QObject *parent = 0 );

 protected:
     bool filterAcceptsRow( int sourceRow, const QModelIndex &sourceParent ) const;
	 bool filterAcceptsColumn( int source_column, const QModelIndex & source_parent ) const;
	};


class CTotalDocIconProvider : public QFileIconProvider
	{
public:
	CTotalDocIconProvider();

	virtual QIcon icon( const QFileInfo& info ) const;
	virtual QString type( const QFileInfo& info ) const;
	};


