#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

struct CMakeOuterSurfaceJigsUndoData
{
	ULONG				ctrlPntIndex;
	IwPoint3d			ctrlPnt;
};

class TEST_EXPORT_TW CMakeOuterSurfaceJigs : public CManager
{
    Q_OBJECT

public:
					CMakeOuterSurfaceJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CMakeOuterSurfaceJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	void			GetMakeOuterSurfaceJigsUndoData(CMakeOuterSurfaceJigsUndoData& undoData, ULONG& ctrlPntIndex, IwPoint3d& ctrlPnt);
	void			SetMakeOuterSurfaceJigsUndoData(CMakeOuterSurfaceJigsUndoData& undoData);

	static void		CreateOuterSurfaceJigsObject(CTotalDoc* pDoc);

private:
	void			Reset(bool activateUI=true);
	bool			PickAControlPoint(const QPoint& cursor, ULONG& ctrlPntIndex, IwPoint3d& prevCtrlPnt, IwPoint3d& postCtrlPnt);

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnCancel();
	void			OnDispSurface();
	void			OnDispAntDrop();
	void			OnDispOsteo();
	void			OnDispCarti();

protected:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actMakeOuterSurfaceJigs;
	QAction*					m_actReset;
	QAction*					m_actDispSurface;
	QAction*					m_actDispAntDrop;
	QAction*					m_actDispOsteo;
	QAction*					m_actDispCarti;

	bool						m_bDispSurface;
	bool						m_bDispAntDrop;
	bool						m_bDispOsteo;
	bool						m_bDispCarti;
	
	bool						m_leftButtonDown;
};
