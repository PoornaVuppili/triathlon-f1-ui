#pragma once

#define NOMINMAX

#include <math.h>
#include <qlist.h>
#pragma warning( disable : 4996 4805 )

#ifdef GetObject
#undef GetObject
#endif

#include <IwVector3d.h>
#include <iwcore_types.h>
#pragma warning( default : 4996 4805 )

#include <algorithm>

#pragma warning(disable: 4244)
#pragma warning(disable: 4305)

typedef unsigned char	BYTE;

#define		HUGE_DOUBLE		1e+12
#define		HUGE_FLOAT		1e+10
#define		HUGE_INT		1000000000

template<typename T> T min3(T a, T b, T c) { return std::min( a, std::min( b, c) ); }
template<typename T> T max3(T a, T b, T c) { return std::max( a, std::max( b, c) ); }

template < class CElem >
void Swap( CElem& a, CElem& b )
	{
        std::swap(a, b);
	//CElem c;

	//c = a;
	//a = b;
	//b = c;
	}

class CVector3d;
class CPoint3d;
class CPoint2d;
class CColor;
class CMatrix;

#define ROUND_SCALE	1e+6

inline double Round( double f )
	{
	return floor( f * ROUND_SCALE + 0.5 ) / ROUND_SCALE;
	}


// Round off a single double number
inline void RoundValue( double& val )
	{
	const double ln2 = 0.69314718056;

	if( fabs(val) < 1e-8 )
		{
		val = 0.0;
		return;
		}

	double	scaler = pow( 2.0, 20L - (long)( log(fabs(val)) / ln2 ) );

	scaler *= 100;

	val = floor( val * scaler + 0.5f ) / scaler;
	}


// We have two main classes for implementing vector algebra:
// CVector3d and CPoint3d. These two classes are very similar
// (they have the same data and the same methods); we have two
// classes instead of one because of difference in applying transformation
// to points and vectors.
//
// Class CVector3d - the primary class of the vector algebra.
// Some operators are defined only for this class and not defined
// for CPoint3d; instead we have a conversion of CPoint3d to CVector3d.
// 
class CVector3d
	{
private:
	double		x;
	double		y;
	double		z;


public:

	// Default constructor
	inline CVector3d();


	// Constructor by coordinates
	inline CVector3d( 
		double x,				// X coordinate
		double y, 				// Y coordinate
		double z 				// X coordinate
		);
	

	// Copy constructor
	inline CVector3d( 
		const CVector3d& vec	// other vector
		);
	

	// Constructor by coordinates array
	inline CVector3d( 
		double _coord[3]			// array of coordinates
		);


	inline CVector3d( IwVector3d& iwVec );
	inline CVector3d( const IwVector3d& iwVec );
	
	// Destructor
	inline ~CVector3d();
	

	// Conversion operator to coordinates array;
	// it allows us to use CVector3d object where double* is expected.
	// For example in OpenGL functions.
	inline operator double* ();
	

	inline operator CPoint2d& ();


	// Assignment operator
	inline CVector3d& operator = ( 
		const CVector3d& vec	// other vector
		);
	

	// Read-only index operator
	inline double operator [] ( 
		int idx					// index of requested coordinate
		) const;
	

	// Read-write index operator
	inline double& operator [] ( 
		int idx					// index of requested coordinate
		);

	inline operator IwVector3d& () const;

	// Creates new vector by changing sign of this one
	inline CVector3d operator - () const;
	

	// Creates new vector by adding other vector to the current one
	inline CVector3d operator + ( 
		const CVector3d& vec	// vector being added
		) const;
	
	
	// Creates new vector by subtracting other vector from the current one
	inline CVector3d operator - ( 
		const CVector3d& vec	// vector being subtracted
		) const;
	
	
	// Creates new vector by multiplying the current one by scalar
	inline CVector3d operator * ( 
		double scale				// scalar to multiple by
		) const;


	// Creates new vector by dividing the current one by scalar
	inline CVector3d operator / ( 
		double scale				// scalar to divide by
		) const;


	// Changes this vector by adding other vector
	inline CVector3d& operator += (
		const CVector3d& vec	// vector being added
		);


	// Changes this vector by subtracting other vector
	inline CVector3d& operator -= ( 
		const CVector3d& vec	// vector being subtracted
		);

	inline CVector3d& operator *= ( 
		double scale 
		);

	inline CVector3d& operator /= ( 
		double scale 
		);

	// Computes vector length
	inline double Length() const;
	
	
	// Normalize current vector
	inline bool	Normalize();

	inline void SetLength( double length );

	// Normalize current vector
	inline bool	Normalize2d();


	friend class CPoint3d;

	friend class CMatrix;


	// Equality operator
	friend inline bool operator == ( 
		const CVector3d& v1,	// first vector to compare
		const CVector3d& v2		// second vector to compare
		);


	// Create new vector by multiplying a given one by scalar from left side
	friend inline CVector3d operator * ( 
		double scale,			// scalar
		const CVector3d& vec	// vector
		);

	// Normalize vector
	friend inline CVector3d Normalize( 
		CVector3d vec 
		);

	// Dot product of two vectors
	friend inline double	operator * ( 
		const CVector3d& v1,	// first vector
		const CVector3d& v2		// second vector
		);

	// Cross product of two vectors
	friend inline CVector3d Cross3d ( 
		const CVector3d& v1,	// first vector
		const CVector3d& v2		// second vector
		);

	
	friend inline double MixedProduct( 
		CVector3d& a,			// first vector
		CVector3d& b,			// second vector
		CVector3d& nm			// normalized normal to both
		);
	

	// Normalized cross product of two vectors
	friend inline CVector3d NormalizedCross ( 
		const CVector3d& v1,	// first vector
		const CVector3d& v2		// second vector
		);


	friend inline CVector3d OrientedCross( 
		const CVector3d& v1, 
		const CVector3d& v2, 
		const CVector3d& v3
		);
	};


//explicit inline operator const IwVector3d* ( const CVector3d* vec )
//	{
//	return (IwVector3d*) &vec;
//	}


inline void SwapVectors( CVector3d& a, CVector3d& b )
	{
	CVector3d	c;

	c = a; a = b; b = c;
	}


// Class CPoint3d represents a point in 3d-space. A point can be considered
// as a vector from origin to itself ("radius-vector" of a point).
// In regarding of this consideration all operators of vector algebra
// are defined for class CPoint3d.
//
class CPoint3d
	{
private:
	double	x;
	double	y;
	double	z;

public:
	// Default constructor
	inline CPoint3d();


	// Constructor by coordinates
	inline CPoint3d( 
		double x,				// X coordinate
		double y,				// Y coordinate
		double z					// Z coordinate
		);


	// Copy constructor
	inline CPoint3d( 
		const CPoint3d& pt		// other point
		);


	// Constrcutor by coordinate array
	inline CPoint3d( 
		double _coord[3]			// coordinates array
		);

	// Constructor from array of three floats
	inline CPoint3d(
		float fcoord[3]
		);

	// Cobstrictor from CVector3d
	inline CPoint3d( const CVector3d& vec );

	inline CPoint3d( IwPoint3d& iwPnt );
	inline CPoint3d( const IwPoint3d& iwPnt );


	// Destructor
	inline ~CPoint3d();


	// Conversion operator to coordinates
	// it allows us to use CPoint3d object where double* is expected.
	// For example in OpenGL functions.
	inline operator double* (); 
	inline operator double* () const; 

	inline operator CPoint2d& ();


	// Conversion operator to CVector3d
	// It allows us not to redefine global functions and operators
	// already defined for CVector3d
	inline operator CVector3d& ();


	// Assignment operator
	inline CPoint3d& operator = ( 
		const CPoint3d& pt		// other point
		);


	// Read-only index operator
	inline double operator [] ( 
		int idx					// index of requested coordinate
		) const ;


	// Read-write index operator
	inline double& operator [] ( 
		int idx					// index of requested coordinate
		);


	inline operator IwPoint3d& () const;


	// Creates new point reflecting the current one about origin
	inline CVector3d operator - () const;
	

	// Creates new point by adding a vector (or a point) to the current point
	inline CPoint3d operator + ( 
		const CVector3d& vec	// vector being added
		) const;


	// Creates new point by adding a vector (or a point) to the current point
	inline CPoint3d	operator - ( 
		const CVector3d& vec	// vector being subtracted
		) const;

	// Creates new point by multiplying the current point by scalar
	inline CPoint3d operator * ( 
		double scale				// scalar
		) const;


	// Creates new point by dividing the current point by scalar
	inline CPoint3d operator / ( 
		double scale				// scalar
		) const;


	// Changes the current point by adding a vector (or a point)
	inline CPoint3d& operator += ( 
		const CVector3d& vec	// vector being added
		);


	// Changes the current point by subtracting a vector (or a point)
	inline CPoint3d& operator -= ( 
		const CVector3d& vec	// 
		);
	

	// Changes the current point by multiplying it by scalar
	inline CPoint3d& operator *= ( 
		double scale				// scalar
		);


	// Changes the current point by dividing it by scalar
	inline CPoint3d& operator /= ( 
		double scale				// scalar
		);
				
	
	// Compares the current point with other point or vector
	inline bool	operator == ( 
		const CVector3d& vec	// vector (or point) to compare with
		) const;
	

	// Computes distance from origin to the current point
	inline double Length() const;


	// Normalize the radius-vector of the current point
	inline bool	Normalize();

	bool False();

	inline bool IsPointInsideTriangle (
		const CPoint3d&	p1,
		const CPoint3d&	p2,
		const CPoint3d&	p3
		) const;

	inline void RoundOff();


	friend class CVector3d;

	friend class CMatrix;


	friend inline CPoint3d operator + (
		const CPoint3d& pt0,
		const CPoint3d& pt1
		);

	friend inline CVector3d operator - (
		const CPoint3d& pt0,
		const CPoint3d& pt1
		);

	// Left-side multiplication of a point by scalar
	friend inline CPoint3d operator * ( 
		double scale,			// scalar
		const CPoint3d& pt		// point
		);

	// Finding distance between two points
	friend inline double dist( 
		const CPoint3d& pt0, 
		const CPoint3d& pt1
		);
	

	// Finding distance between two 2d-points
	friend inline double dist2d( 
		CPoint3d& pt0, 
		CPoint3d& pt1
		);
	};

struct CPointSeg
	{
	CPoint3d	p0;
	CPoint3d	p1;
	};


class CPointSegArray : public QList< CPointSeg >
	{
public:
	void Add( CPoint3d p[2] )
		{
		CPointSeg	Seg;

		Seg.p0 = p[0];
		Seg.p1 = p[1];

		QList< CPointSeg >::append( Seg );
		}

	void Add( CPoint3d& p0, CPoint3d& p1 )
		{
		CPointSeg	Seg;

		Seg.p0 = p0;
		Seg.p1 = p1;

		QList< CPointSeg >::append( Seg );
		}
	};

	
// Class CPoint2d represents a point on a plane.
// We do not have 2d-transformations and therefore do not need
// two different classes for point and vector.
//
class CPoint2d
	{
private:
	double	x;
	double	y;

public:
	//Default constructor
	inline CPoint2d();


	// Constructor by coordinates
	inline CPoint2d( 
		double _x,				// X-coordinate
		double _y				// Y-coordinate
		);


	// Constructor by coordinates array
	inline CPoint2d( 
		double coord[2]			// coordinates array
		);


	// Copy constructor
	inline CPoint2d( 
		CPoint2d& pt			// other point
		);


	// Destructor
	inline ~CPoint2d();


	// Conversion operator to coordinates
	inline operator double* (); 


	// Read-only index operator
	inline double operator [] ( 
		int idx					// requested index
		) const;


	// Read-write index operator
	inline double& operator [] ( 
		int idx					// requsted index
		);


	// Assignment operator
	inline CPoint2d& operator = ( 
		const CPoint2d& pt		// other point
		);


	// Checks if this point is equal to another one
	inline bool operator == ( 
		const CPoint2d& pt		// other point
		) const;


	// Ckecks if this point is not equal to another one
	inline bool operator != ( 
		const CPoint2d& pt		// other point
		) const;


	// Create new point by reflecting this one around origin
	inline CPoint2d operator - () const;


	// Create new point by adding another point to this one
	inline CPoint2d operator + ( 
		const CPoint2d& pt		// other point
		) const;


	// Create new point by subtracting another point to this one
	inline CPoint2d operator - ( 
		const CPoint2d& pt		// other point
		) const;

	// Create new point by multiplying this one by scalar
	inline CPoint2d operator * ( 
		double factor			// scalar
		) const;


	// Create new point by dividing this one by scalar
	inline CPoint2d operator / ( 
		double factor			// scalar
		) const;


	// Modifying this point by adding another one to it.
	inline CPoint2d& operator += ( 
		const CPoint2d& pt		// other point
		);

	
	// Modifying this point by adding another one to it.
	inline CPoint2d& operator -= ( 
		const CPoint2d& pt		// other point
		);


	// Modifying this point by multiplying it by scalar
	inline CPoint2d& operator *= ( 
		double factor			// scalar
		);


	// Modifying this point by dividing it by scalar
	inline CPoint2d& operator /= ( 
		double factor			// scalar
		);


	// Left-side multiplication of a point by scalar
	friend inline CPoint2d operator * ( 
		double factor,			// scalar
		const CPoint2d& pt		// point
		);
	

	// Two-dimensional cross-product
	friend inline double Cross2d( 
		CPoint2d& a, 
		CPoint2d& b
		);


	// Two-dimensional dot-product
	friend inline double Dot2d( 
		CPoint2d& a, 
		CPoint2d& b
		);


	// Finding distance between two points
	friend inline double dist( 
		CPoint2d& pt0, 
		CPoint2d& pt1
		);

	// Normalize 2d-vector
	friend inline CPoint2d Normalize2d( 
		CPoint2d vec 
		);

	bool IsPointInsideTriangle (
		const CPoint3d&	p,
		const CPoint3d&	p1,
		const CPoint3d&	p2,
		const CPoint3d&	p3
		);


	bool SolveLinearSystem2x2( 
		const CPoint2d& a, 
		const CPoint2d& b, 
		const CPoint2d& c, 
		double& x, 
		double& y 
		);
	};

// Class CColor represents a color given by rgb
// It provides interface for basic arithmetic and
// index and conversion operators.
//
class CColor
	{
private:
	BYTE rgb[3];				// Red Green Blue

public:
	// Default constructor
	inline CColor();


	// Constructor by rgb values
	inline CColor( 
		BYTE r,					// Red
		BYTE g,					// Green
		BYTE b					// Blue
		);


	// Copy constructor
	inline CColor( 
		const CColor& color		// other color
		);


	// Constrcutor by BYTE array
	inline CColor( 
		BYTE _rgb[3]			// components array
		);


	// Destructor
	inline ~CColor();


	// Conversion operator to components array
	// it allows us to use CColor object where BYTE* is expected.
	inline operator BYTE* ();


	// Assignment operator
	inline CColor& operator = ( 
		const CColor& color		// other color
		);


	// Read-only index operator
	inline BYTE operator [] ( 
		int idx					// index of requested component
		) const ;


	// Read-write index operator
	inline BYTE& operator [] ( 
		int idx					// index of requested component
		);


	// Equality operator
	inline bool operator == ( 
		const CColor& color		// other color
		);


	// NotEquality operator
	inline bool operator != ( 
		const CColor& color		// other color
		);


	// Creates new color by adding another color to the current color
	inline CColor operator + ( 
		const CColor& color		// color being added
		) const;

/*
	// Creates new color by subtracting another color to the current color
	inline CColor operator - ( 
		const CColor& color		// color being subtracted
		) const;
*/

	// Creates new color by multiplying the current color by scalar
	inline CColor operator * ( 
		double scale				// scalar
		) const;


	// Creates new color by dividing the current color by scalar
	inline CColor operator / ( 
		double scale				// scalar
		) const;


	// Changes the current color by adding another color
	inline CColor& operator += ( 
		const CColor& color		// color being added
		);

/*
	// Changes the current color by subtracting another color
	inline CColor& operator -= ( 
		const CColor& color		// color being subtracted
		);
*/	

	// Changes the current color by multiplying it by scalar
	inline CColor& operator *= ( 
		double scale				// scalar
		);


	// Changes the current color by dividing it by scalar
	inline CColor& operator /= ( 
		double scale				// scalar
		);
				
	
	// Compares the current color with other color
	inline bool	operator == ( 
		const CColor& color		// color to compare with
		) const;
	

	inline void Normalize( 
		double mag
		);


	// Left-side multiplication of a color by scalar
	friend inline CColor operator * ( 
		double scale,			// scalar
		const CColor& color		// color
		);
	

	friend inline CColor lincom( 
		double		w0, 
		CColor&		color0, 
		double		w1,	
		CColor&		color1
		);


	friend inline CColor lincom( 
		double		w0, 
		CColor&		color0, 
		double		w1, 
		CColor&		color1,
		double		w2, 
		CColor&		color2 );
	};



// Class CMatrix represents 3x3 scaling-and-rotation matrix;
// no translation component included.
//
// This matrix is not nessesarily orthonormal;
// it is rather affine transformation than euclidian one.
//
// Performance of applying matrix to a point is critical;
// that is why no array data is used.
//
class CMatrix
	{
private:
	double		m00, m01, m02;		
	double		m10, m11, m12;
	double		m20, m21, m22;
	
public:
	// Deafult constructor
	inline CMatrix();

	
	// Constructor by angle and axis of rotation
	inline CMatrix( 
		double angle,			// angle of rotation in degrees
		double x,				// X-coordinate of axis of rotation
		double y,				// Y-coordinate of axis of rotation
		double z					// Z-coordinate of axis of rotation
		);


	// Constructor by real matrix
	inline CMatrix(
		double matr[3][3]
	);


	// Destructor
	inline ~CMatrix() {}

	
	// Applying matrix to point
	inline CPoint3d operator * ( 
		const CPoint3d& pt		// point
		) const;


	// Applying matrix to vector
	inline CVector3d operator * ( 
		const CVector3d& pt		// vector
		) const;


	// Creates new matrix by adding another one to this one
	inline CMatrix operator + ( 
		const CMatrix& m		// other matrix
		) const;
	

	// Creates new matrix by subtracting another one from this one
	inline CMatrix operator - ( 
		const CMatrix& m		// other matrix
		) const;


	// Creates new matrix by multiplying this one by scalar
	inline CMatrix operator * ( 
		const double scale		// scalar
		) const;

	
	// Creates new matrix by dividing this one by scalar
	inline CMatrix operator / ( 
		const double scale		// scalar
		) const;

	
	// Modifying this matrix by multiplying it by scalar
	inline CMatrix& operator *= ( 
		const double scale		// scalar
		);

	
	// Modifying this matrix by dividing it by scalar
	inline CMatrix& operator /= ( 
		const double scale		// scalar
		);

	// Matrix multiplication: out = this * other;
	inline CMatrix operator * ( 
		const CMatrix& m
		) const;


	// Equality operator
	inline bool operator == ( 
		const CMatrix& matr
		) const;

	// Finds inverse matrix
	inline bool Inverse( CMatrix& out );

	inline double Determinant();

	// Returns true if the matrix is non-diagonal
	inline bool IsRotation();

	// Returns true if matrix does not change z coord.
	inline bool XYOnly();

	// Fills in the matrix[3][3]
	inline void GetMatrix( double matr[3][3] ) const;
	inline void SetMatrix( double matr[3][3] );


	// Makes 4x4 (homegeneous coordiantes) matrix for OpenGL
	inline void MakeGLMatrix( 
		double glMatrix[16]		// array of 16 floats; should be allocated by caller
		) const;

	friend class CTransform;
	};





//******************************************************************************//
//                        CVector3d Implementation                              //
//******************************************************************************//

CVector3d::CVector3d() 
	{
	x = y = z = 0.0;
	}


CVector3d::CVector3d( double _x, double _y, double _z )
	{ 
	x = _x;
	y = _y;
	z = _z;
	}

	
CVector3d::CVector3d( const CVector3d& pt )
	{ 
	x = pt.x;
	y = pt.y;
	z = pt.z;
	}

	
CVector3d::CVector3d( double coord[3] )
	{
	if( coord )
		{
		x = coord[0];
		y = coord[1];
		z = coord[2];
		}
	else
		{
		x = y = z = 0.0;
		}
	}

CVector3d::CVector3d( IwVector3d& iwVec )
	{
	x = iwVec.x;
	y = iwVec.y;
	z = iwVec.z;
	}

CVector3d::CVector3d( const IwVector3d& iwVec )
	{
	x = iwVec.x;
	y = iwVec.y;
	z = iwVec.z;
	}

	
CVector3d::~CVector3d()
	{
	}

	
CVector3d::operator double* ()
	{ 
	return (double*) this;
	}

	
// Conversion to reference to CPoint2d
CVector3d::operator CPoint2d& ()
	{
	return *( (CPoint2d*) this );
	}


CVector3d& CVector3d::operator = ( const CVector3d& pt )
	{
	x = pt.x;
	y = pt.y;
	z = pt.z;

	return *this;
	}


double CVector3d::operator [] ( int idx ) const
	{ 
	switch( idx )
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		}

	return 0.0;
	}


double& CVector3d::operator [] ( int idx )
	{ 
	switch( idx )
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		}

	return x;
	}


CVector3d::operator IwVector3d& () const
	{ 
	return *( (IwVector3d*) this );
	}


CVector3d CVector3d::operator - () const
	{
	return CVector3d( -x, -y, -z );
	}


CVector3d CVector3d::operator + ( const CVector3d& vec ) const
	{
	return CVector3d( x + vec.x, y + vec.y, z + vec.z );
	}
	

CVector3d CVector3d::operator - ( const CVector3d& vec ) const
	{
	return CVector3d( x - vec.x, y - vec.y, z - vec.z );
	}

	
CVector3d CVector3d::operator * ( double scale ) const
	{
	return CVector3d( x * scale, y * scale, z * scale );
	}


CVector3d CVector3d::operator / ( double scale ) const
	{
	return CVector3d( x / scale, y / scale, z / scale );
	}


CVector3d& CVector3d::operator += ( const CVector3d& vec )
	{
	x += vec.x;
	y += vec.y;
	z += vec.z;

	return *this;
	}


CVector3d& CVector3d::operator -= ( const CVector3d& vec )
	{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;

	return *this;
	}


CVector3d& CVector3d::operator *= ( double scale )
	{
	x *= scale;
	y *= scale;
	z *= scale;

	return *this;
	}


CVector3d& CVector3d::operator /= ( double scale  )
	{
	x /= scale;
	y /= scale;
	z /= scale;

	return *this;
	}


double CVector3d::Length() const
	{
	return sqrt( x * x + y * y + z * z );
	}


bool CVector3d::Normalize()
	{
	const double	eps = 1e-4;

	double length = sqrt( x * x + y * y + z * z );

	if( length < eps )
		return false;

	x /= length;
	y /= length;
	z /= length;

	return true;
	}


void CVector3d::SetLength( double length )
	{
	Normalize();

	x *= length;
	y *= length;
	z *= length;
	}


bool CVector3d::Normalize2d()
	{
	const double	eps = 1e-8;

	double length = sqrt( x * x + y * y );

	if( length < eps )
		return false;

	x /= length;
	y /= length;
	z = 0.0;

	return true;
	}


inline CVector3d Normalize( CVector3d vec )
	{
	double		length = sqrt( vec.x * vec.x + vec.y * vec.y + vec.z * vec.z );

	if( length > 1e-8 )
		{
		vec.x /= length;
		vec.y /= length;
		vec.z /= length;
		}
	else
		{
		vec.x = 0.0;
		vec.y = 0.0;
		vec.z = 0.0;
		}

	return vec;
	}


inline bool operator==	( const CVector3d& v1, const CVector3d& v2 )
	{
	//return ( v1.x == v2.x && v1.y == v2.y && v1.z == v2.z );
	double		eps = 1e-9;

	return ( fabs( v1.x - v2.x ) < eps && fabs( v1.y - v2.y ) < eps && fabs( v1.z - v2.z ) < eps ); 
	}


inline CVector3d operator * ( double scale, const CVector3d& vec )
	{
	return CVector3d( scale * vec.x, scale * vec.y, scale * vec.z );
	}


inline double operator * ( const CVector3d& v1, const CVector3d& v2 )
	{
	return ( v1.x * v2.x + v1.y * v2.y + v1.z * v2.z );
	}


inline CVector3d Cross3d( const CVector3d& v1, const CVector3d& v2 )
	{
	return CVector3d(	v1.y * v2.z - v1.z * v2.y, 
						v1.z * v2.x - v1.x * v2.z, 
						v1.x * v2.y - v1.y * v2.x );
	}


inline CVector3d NormalizedCross( const CVector3d& v1, const CVector3d& v2 )
	{
	CVector3d	vec1 = Normalize( v1 );
	CVector3d	vec2 = Normalize( v2 );

	CVector3d	vec = CVector3d( 	vec1.y * vec2.z - vec1.z * vec2.y, 
									vec1.z * vec2.x - vec1.x * vec2.z, 
									vec1.x * vec2.y - vec1.y * vec2.x );

	return Normalize( vec );
	}


inline CVector3d OrientedCross( const CVector3d& v1, const CVector3d& v2, const CVector3d& v3 )
	{
	CVector3d			vec;

	vec = NormalizedCross( v1, v2 );

	if( vec * v3 < 0.0 )
		vec *= -1.0;

	return vec;
	}


inline double MixedProduct( CVector3d& a, CVector3d& b, CVector3d& nm )
	{
	a.Normalize();
	b.Normalize();

	CVector3d	c = Cross3d( a, b );

	return c * nm;
	}

	
//******************************************************************************//
//                        CPoint3d Implementation                               //
//******************************************************************************//

// Default constructor
CPoint3d::CPoint3d()
	{
	x = y = z = 0.0;
	}


// Constructor from coordinates
CPoint3d::CPoint3d( double _x, double _y, double _z )
	{ 
	x = _x;
	y = _y;
	z = _z;
	}


// Copy constructor
CPoint3d::CPoint3d( const CPoint3d& pt )
	{ 
	x = pt.x;
	y = pt.y;
	z = pt.z;
	}


// Constructor from coordinate array
CPoint3d::CPoint3d( double coord[3] )
	{
	x = coord[0];
	y = coord[1];
	z = coord[2];
	}

// Constructor from float coordinate array
CPoint3d::CPoint3d( float fcoord[3] )
	{
	x = fcoord[0];
	y = fcoord[1];
	z = fcoord[2];
	}

// Constructor from CVector3d
CPoint3d::CPoint3d( const CVector3d& vec )
	{ 
	x = vec.x;
	y = vec.y;
	z = vec.z;
	}


CPoint3d::CPoint3d( IwPoint3d& iwPnt )
	{
	x = iwPnt.x;
	y = iwPnt.y;
	z = iwPnt.z;
	}

CPoint3d::CPoint3d( const IwPoint3d& iwPnt )
	{
	x = iwPnt.x;
	y = iwPnt.y;
	z = iwPnt.z;
	}
	
// Destructor
CPoint3d::~CPoint3d() {}

// Conversion to pointer to coordinates
CPoint3d::operator double* () 
	{ 
	return ( double*) this;
	}

// Conversion to pointer to coordinates
CPoint3d::operator double* () const
	{ 
	return (double*) this;
	}

// Conversion to reference to CVector3d
CPoint3d::operator CVector3d& ()
	{
	return *( (CVector3d*) this );
	}

// Conversion to reference to CPoint2d
CPoint3d::operator CPoint2d& ()
	{
	return *( (CPoint2d*) this );
	}

// Assignment operator
CPoint3d& CPoint3d::operator = ( const CPoint3d& pt )
	{
	x = pt.x;
	y = pt.y;
	z = pt.z;
			
	return *this;
	}

// Const indexing operator - for right side
double CPoint3d::operator [] ( int idx ) const 
	{ 
	switch( idx )
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		}

	return 0.0;
	}

// Non-const indexing operator - for left side
double& CPoint3d::operator [] ( int idx )
	{ 
	switch( idx )
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		}

	return x;
	}


CPoint3d::operator IwPoint3d& () const
	{
	return *( (IwPoint3d*) this );
	}
	

// Unary inverting sign
CVector3d CPoint3d::operator - () const
	{
	return CVector3d( -x, -y, -z );
	}


// Add vector to point operator
CPoint3d CPoint3d::operator + ( const CVector3d& vec ) const
	{
	return CPoint3d( x + vec.x, y + vec.y, z + vec.z );
	}


// Subtract vector from point
CPoint3d CPoint3d::operator - ( const CVector3d& vec ) const
	{
	return CPoint3d( x - vec.x, y - vec.y, z - vec.z );
	}

// Mulptiple point by scalar
CPoint3d CPoint3d::operator * ( double scale ) const
	{
	return CPoint3d( x * scale, y * scale, z * scale );
	}

// Divide point by scalar
CPoint3d CPoint3d::operator/ ( double scale ) const
	{
	return CPoint3d( x / scale, y / scale, z / scale );
	}

CPoint3d& CPoint3d::operator += ( const CVector3d& vec )
	{
	x += vec.x;
	y += vec.y;
	z += vec.z;

	return *this;
	}


CPoint3d& CPoint3d::operator -= ( const CVector3d& vec )
	{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;

	return *this;
	}


CPoint3d& CPoint3d::operator *= ( double scale )
	{
	x *= scale;
	y *= scale;
	z *= scale;

	return *this;
	}


CPoint3d& CPoint3d::operator /= ( double scale  )
	{
	x /= scale;
	y /= scale;
	z /= scale;

	return *this;
	}

						
// Equality operator
bool CPoint3d::operator == ( const CVector3d& vec ) const
	{
	//return ( x == vec.x && y == vec.y && z == vec.z );
	double		eps = 1e-9;

	return ( fabs( x - vec.x ) < eps && fabs( y - vec.y ) < eps && fabs( z - vec.z ) < eps ); 
	}

// Find distance from point to Zero
double CPoint3d::Length() const
	{
	return sqrt( x * x + y * y + z * z );
	}

// Normalize radius-vector of point
bool CPoint3d::Normalize()
	{
	const double	eps = 1e-10;

	double	length = sqrt( x * x + y * y + z * z );

	if( length < eps )
		return false;

	x /= length;
	y /= length;
	z /= length;

	return true;
	}


inline CPoint3d operator + ( const CPoint3d& pt0, const CPoint3d& pt1 )
	{
	return CPoint3d( pt0.x + pt1.x, pt0.y + pt1.y, pt0.z + pt1.z );
	}


inline CVector3d operator - ( const CPoint3d& pt0, const CPoint3d& pt1 )
	{
	return CPoint3d( pt0.x - pt1.x, pt0.y - pt1.y, pt0.z - pt1.z );
	}


inline CPoint3d operator * ( double scale, const CPoint3d& pt )
	{
	return CPoint3d( scale * pt.x, scale * pt.y, scale * pt.z );
	}


inline double dist( const CPoint3d& pt0, const CPoint3d& pt1 )
	{
	double		dx = pt1.x - pt0.x;
	double		dy = pt1.y - pt0.y;
	double		dz = pt1.z - pt0.z;

	return sqrt( dx * dx + dy * dy + dz * dz );
	}


inline double dist2d( CPoint3d& pt0, CPoint3d& pt1 )
	{
	double		dx = pt1.x - pt0.x;
	double		dy = pt1.y - pt0.y;

	return sqrt( dx * dx + dy * dy );
	}


// Round off a 3d-point
inline void CPoint3d::RoundOff()
	{
	RoundValue( x );
	RoundValue( y );
	RoundValue( z );
	}


///*****************************************************************************//
//                          CPoint2d Implementation                             //
//******************************************************************************//

CPoint2d::CPoint2d()
	{
	x = y = 0.0;
	}


CPoint2d::CPoint2d( double _x, double _y )
	{
	x = _x;
	y = _y;
	}


CPoint2d::CPoint2d( double coord[2] )
	{
	x = coord[0];
	y = coord[1];
	}


CPoint2d::CPoint2d( CPoint2d& pt )
	{
	x = pt.x;
	y = pt.y;
	}


CPoint2d::~CPoint2d()
	{
	}


// Conversion to pointer to coordinates
CPoint2d::operator double* () 
	{ 
	return (double*) this;
	}


// Const indexing operator - for right side
double CPoint2d::operator [] ( int idx ) const 
	{ 
	return ( idx == 0 )?	x : y;
	}

// Non-const indexing operator - for left side
double& CPoint2d::operator [] ( int idx )
	{ 
	return ( idx == 0 )?	x : y;
	}


CPoint2d& CPoint2d::operator = ( const CPoint2d& pt )
	{
	x = pt.x;
	y = pt.y;

	return *this;
	}


bool CPoint2d::operator == ( const CPoint2d& pt ) const
	{
	double		eps = 1e-9;

	return ( fabs( x - pt.x ) < eps && fabs( y - pt.y ) < eps );
	}


bool CPoint2d::operator != ( const CPoint2d& pt ) const
	{
	return ( x != pt.x || y != pt.y );
	}


// Unary inverting sign
CPoint2d CPoint2d::operator - () const
	{
	return CPoint2d( -x, -y );
	}


// Add point to point operator
CPoint2d CPoint2d::operator + ( const CPoint2d& pt ) const
	{
	return CPoint2d( x + pt.x, y + pt.y );
	}

// Subtract point from point
CPoint2d CPoint2d::operator - ( const CPoint2d& pt ) const
	{
	return CPoint2d( x - pt.x, y - pt.y );
	}

// Mulptiple point by scalar
CPoint2d CPoint2d::operator * ( double scale ) const
	{
	return CPoint2d( x * scale, y * scale );
	}

// Divide point by scalar
CPoint2d CPoint2d::operator / ( double scale ) const
	{
	return CPoint2d( x / scale, y / scale );
	}

CPoint2d& CPoint2d::operator += ( const CPoint2d& pt )
	{
	x += pt.x;
	y += pt.y;

	return *this;
	}


CPoint2d& CPoint2d::operator -= ( const CPoint2d& pt )
	{
	x -= pt.x;
	y -= pt.y;

	return *this;
	}


CPoint2d& CPoint2d::operator *= ( double scale )
	{
	x *= scale;
	y *= scale;

	return *this;
	}


CPoint2d& CPoint2d::operator /= ( double scale  )
	{
	x /= scale;
	y /= scale;

	return *this;
	}


inline CPoint2d operator * ( double scale, const CPoint2d& pt )
	{
	return CPoint2d( scale * pt.x, scale * pt.y );
	}


inline double Cross2d( CPoint2d& a, CPoint2d& b )
	{
	return a[0] * b[1] - a[1] * b[0];
	}


inline double Dot2d( CPoint2d& a, CPoint2d& b )
	{
	return a.x * b.x + a.y * b.y;
	}


inline double dist( CPoint2d& pt0, CPoint2d& pt1 )
	{
	return sqrt( ( pt1.x - pt0.x ) * ( pt1.x - pt0.x ) +
				 ( pt1.y - pt0.y ) * ( pt1.y - pt0.y ) );
	}


inline CPoint2d Normalize2d( CPoint2d vec )
	{
	double	length = sqrt( vec.x * vec.x + vec.y * vec.y );

	if( length > 1e-6 )
		{
		vec.x /= length;
		vec.y /= length;
		}
	else
		{
		vec.x = 0.0;
		vec.y = 0.0;
		}

	return vec;
	}


//******************************************************************************//
//                        CColor Implementation                                 //
//******************************************************************************//

#pragma warning(disable: 4244) // conversion from double to BYTE

CColor::CColor()
	{
	rgb[0] = 0;
	rgb[1] = 0;
	rgb[2] = 0;
	}


CColor::CColor( BYTE r, BYTE g, BYTE b )
	{
	rgb[0] = r;
	rgb[1] = g;
	rgb[2] = b;
	}


CColor::CColor( const CColor& color )
	{
	rgb[0] = color.rgb[0];
	rgb[1] = color.rgb[1];
	rgb[2] = color.rgb[2];
	}


CColor::CColor( BYTE _rgb[3] )
	{
	rgb[0] = _rgb[0];
	rgb[1] = _rgb[1];
	rgb[2] = _rgb[2];
	}


CColor::~CColor()
	{
	}


CColor::operator BYTE* ()
	{
	return rgb;
	}


CColor& CColor::operator = ( const CColor& color )
	{
	rgb[0] = color.rgb[0];
	rgb[1] = color.rgb[1];
	rgb[2] = color.rgb[2];

	return *this;
	}


BYTE CColor::operator [] ( int idx ) const
	{
	return rgb[idx];
	}


BYTE& CColor::operator [] ( int idx )
	{
	return rgb[idx];
	}


CColor CColor::operator + ( const CColor& color	) const
	{
	return CColor( rgb[0] + color.rgb[0],
				   rgb[1] + color.rgb[1],
				   rgb[2] + color.rgb[2] );
	}

/*
CColor CColor::operator - ( const CColor& color	) const
	{
	return CColor( rgb[0] - color.rgb[0],
				   rgb[1] - color.rgb[1],
				   rgb[2] - color.rgb[2] );
	}
*/

CColor CColor::operator * ( double scale ) const
	{
	return CColor( rgb[0] * scale + 0.5, rgb[1] * scale + 0.5, rgb[2] * scale + 0.5 );
	}


CColor CColor::operator / ( double scale ) const
	{
	return CColor( rgb[0] / scale + 0.5, rgb[1] / scale + 0.5, rgb[2] / scale + 0.5 );
	}


CColor& CColor::operator += ( const CColor& color )
	{
	rgb[0] += color.rgb[0];
	rgb[1] += color.rgb[1];
	rgb[2] += color.rgb[2];

	return *this;
	}

/*
CColor& CColor::operator -= ( const CColor& color )
	{
	rgb[0] -= color.rgb[0];
	rgb[1] -= color.rgb[1];
	rgb[2] -= color.rgb[2];
	
	return *this;
	}
*/

CColor& CColor::operator *= ( double scale )
	{
	rgb[0] *= scale;
	rgb[1] *= scale;
	rgb[2] *= scale;
	
	return *this;
	}


CColor& CColor::operator /= ( double scale )
	{
	rgb[0] /= scale;
	rgb[1] /= scale;
	rgb[2] /= scale;
	
	return *this;
	}


void CColor::Normalize( double new_mag )
	{
	double r = rgb[0] / 255.0;
	double g = rgb[1] / 255.0;
	double b = rgb[2] / 255.0;
	double sc;

	double mag = sqrt( r * r + g * g + b * b );

	if( mag > 0.0 )
		{
		sc = new_mag / mag;

		rgb[0] *= sc;
		rgb[1] *= sc;
		rgb[2] *= sc;
		}
	}


bool CColor::operator == ( const CColor& color )
	{
	return rgb[0] == color.rgb[0] &&
		   rgb[1] == color.rgb[1] &&
		   rgb[2] == color.rgb[2];
	}
	

bool CColor::operator != ( const CColor& color )
	{
	return rgb[0] != color.rgb[0] ||
		   rgb[1] != color.rgb[1] ||
		   rgb[2] != color.rgb[2];
	}
	

CColor operator * ( double scale, const CColor& color )
	{
	/*
	return CColor( scale * color.rgb[0] + 0.5, 
				   scale * color.rgb[1] + 0.5,
				   scale * color.rgb[2] + 0.5);
	*/
	return CColor( scale * color.rgb[0], 
				   scale * color.rgb[1],
				   scale * color.rgb[2]);
	}
	

CColor lincom( double w0, CColor& color0, double w1, CColor& color1 )
	{
	CColor	color;

	color[0] = int( w0 * color0[0] + w1 * color1[0] + 0.5 );
	color[1] = int( w0 * color0[1] + w1 * color1[1] + 0.5 );
	color[2] = int( w0 * color0[2] + w1 * color1[2] + 0.5 );

	return color;
	}


CColor lincom( 
	double		w0, 
	CColor&		color0, 
	double		w1, 
	CColor&		color1,
	double		w2, 
	CColor&		color2 )
	{
	CColor	color;

	color[0] = int( w0 * color0[0] + w1 * color1[0] + w2 * color2[0] + 0.5 );
	color[1] = int( w0 * color0[1] + w1 * color1[1] + w2 * color2[1] + 0.5 );
	color[2] = int( w0 * color0[2] + w1 * color1[2] + w2 * color2[2] + 0.5 );

	return color;
	}


extern CColor SevenColors[7];


#pragma warning(default: 4244) // conversion from double to BYTE



//******************************************************************************//
//                        CMatrix Implementation                                //
//******************************************************************************//

// Default constructor makes indentity matrix
CMatrix::CMatrix()
	{
	m00 = 1.0; m01 = 0.0; m02 = 0.0;
	m10 = 0.0; m11 = 1.0; m12 = 0.0;
	m20 = 0.0; m21 = 0.0; m22 = 1.0;
	}


CMatrix::CMatrix( double m[3][3] )
	{
	m00 = m[0][0];  m01 = m[0][1];  m02 = m[0][2];
	m10 = m[1][0];  m11 = m[1][1];  m12 = m[1][2];
	m20 = m[2][0];  m21 = m[2][1];  m22 = m[2][2];
	}


// This constructor makes rotation matrix;
// angle - rotation angle; (x,y,z) - rotation axis vector
CMatrix::CMatrix( double angle, double x, double y, double z )
	{
	// First normalize axis vector
	double length = sqrt( x * x + y * y + z * z );

	x /= length;
	y /= length;
	z /= length;


	// Then convert angle from degrees to radians
	angle *= 3.1415926535f / 180.0f;

	double sn = sin( angle );
	double cs = cos( angle );


	// Form the matrix
	m00 = x * x + cs * ( 1.0 -  x * x );
	m01 = x * y * ( 1.0 - cs ) - z * sn;
	m02 = z * x * ( 1.0 - cs ) + y * sn;

	m10 = x * y * ( 1.0 - cs ) + z * sn;
	m11 = y * y + cs * ( 1.0 - y * y );
	m12 = y * z * ( 1.0 - cs ) - x * sn;

	m20 = z * x * ( 1.0 - cs ) - y * sn;
	m21 = y * z * ( 1.0 - cs ) + x * sn;
	m22 = z * z + cs * ( 1.0 - z * z );
	}


CPoint3d CMatrix::operator * ( const CPoint3d& pt ) const
	{
	//double x = Round( m00 * pt[0] + m01 * pt[1] + m02 * pt[2] );
	//double y = Round( m10 * pt[0] + m11 * pt[1] + m12 * pt[2] );
	//double z = Round( m20 * pt[0] + m21 * pt[1] + m22 * pt[2] );

	double x = m00 * pt[0] + m01 * pt[1] + m02 * pt[2];
	double y = m10 * pt[0] + m11 * pt[1] + m12 * pt[2];
	double z = m20 * pt[0] + m21 * pt[1] + m22 * pt[2];

	return CPoint3d( x, y, z );
	}


CVector3d CMatrix::operator * ( const CVector3d& pt ) const
	{
	//double x = Round( m00 * pt[0] + m01 * pt[1] + m02 * pt[2] );
	//double y = Round( m10 * pt[0] + m11 * pt[1] + m12 * pt[2] );
	//double z = Round( m20 * pt[0] + m21 * pt[1] + m22 * pt[2] );

	double x = m00 * pt[0] + m01 * pt[1] + m02 * pt[2];
	double y = m10 * pt[0] + m11 * pt[1] + m12 * pt[2];
	double z = m20 * pt[0] + m21 * pt[1] + m22 * pt[2];

	return CVector3d( x, y, z );
	}


CMatrix CMatrix::operator + ( const CMatrix& m ) const
	{
	CMatrix	out;

	out.m00 = m00 + m.m00;
	out.m01 = m01 + m.m01;
	out.m02 = m02 + m.m02;

	out.m10 = m10 + m.m10;
	out.m11 = m11 + m.m11;
	out.m12 = m12 + m.m12;

	out.m20 = m20 + m.m20;
	out.m21 = m21 + m.m21;
	out.m22 = m22 + m.m22;

	return out;
	}


CMatrix CMatrix::operator - ( const CMatrix& m ) const
	{
	CMatrix	out;

	out.m00 = m00 - m.m00;
	out.m01 = m01 - m.m01;
	out.m02 = m02 - m.m02;

	out.m10 = m10 - m.m10;
	out.m11 = m11 - m.m11;
	out.m12 = m12 - m.m12;

	out.m20 = m20 - m.m20;
	out.m21 = m21 - m.m21;
	out.m22 = m22 - m.m22;

	return out;
	}


CMatrix CMatrix::operator * ( const double scale ) const
	{
	CMatrix	out;

	out.m00 = m00 * scale;
	out.m01 = m01 * scale;
	out.m02 = m02 * scale;

	out.m10 = m10 * scale;
	out.m11 = m11 * scale;
	out.m12 = m12 * scale;

	out.m20 = m20 * scale;
	out.m21 = m21 * scale;
	out.m22 = m22 * scale;

	return out;
	}


CMatrix CMatrix::operator / ( const double scale ) const
	{
	CMatrix	out;

	out.m00 = m00 / scale;
	out.m01 = m01 / scale;
	out.m02 = m02 / scale;

	out.m10 = m10 / scale;
	out.m11 = m11 / scale;
	out.m12 = m12 / scale;

	out.m20 = m20 / scale;
	out.m21 = m21 / scale;
	out.m22 = m22 / scale;

	return out;
	}


CMatrix& CMatrix::operator *= ( const double scale )
	{
	m00 *= scale;
	m01 *= scale;
	m02 *= scale;

	m10 *= scale;
	m11 *= scale;
	m12 *= scale;
	
	m20 *= scale;
	m21 *= scale;
	m22 *= scale;

	return *this;
	}


CMatrix& CMatrix::operator /= ( const double scale )
	{
	m00 /= scale;
	m01 /= scale;
	m02 /= scale;

	m10 /= scale;
	m11 /= scale;
	m12 /= scale;
	
	m20 /= scale;
	m21 /= scale;
	m22 /= scale;

	return *this;
	}


// Equality operator
bool CMatrix::operator == ( const CMatrix& matr ) const
	{
	if( m00 == matr.m00 && m01 == matr.m01 && m02 == matr.m02 &&
	    m10 == matr.m10 && m11 == matr.m11 && m12 == matr.m12 &&
	    m20 == matr.m20 && m21 == matr.m21 && m22 == matr.m22 )
		return true;
	
	return false;
	}


CMatrix CMatrix::operator * ( const CMatrix& m ) const
	{
	CMatrix	out;

	//out.m00 = Round( m00 * m.m00 + m01 * m.m10 + m02 * m.m20 );
	//out.m01 = Round( m00 * m.m01 + m01 * m.m11 + m02 * m.m21 );
	//out.m02 = Round( m00 * m.m02 + m01 * m.m12 + m02 * m.m22 );

	//out.m10 = Round( m10 * m.m00 + m11 * m.m10 + m12 * m.m20 );
	//out.m11 = Round( m10 * m.m01 + m11 * m.m11 + m12 * m.m21 );
	//out.m12 = Round( m10 * m.m02 + m11 * m.m12 + m12 * m.m22 );

	//out.m20 = Round( m20 * m.m00 + m21 * m.m10 + m22 * m.m20 );
	//out.m21 = Round( m20 * m.m01 + m21 * m.m11 + m22 * m.m21 );
	//out.m22 = Round( m20 * m.m02 + m21 * m.m12 + m22 * m.m22 );

	out.m00 = m00 * m.m00 + m01 * m.m10 + m02 * m.m20;
	out.m01 = m00 * m.m01 + m01 * m.m11 + m02 * m.m21;
	out.m02 = m00 * m.m02 + m01 * m.m12 + m02 * m.m22;

	out.m10 = m10 * m.m00 + m11 * m.m10 + m12 * m.m20;
	out.m11 = m10 * m.m01 + m11 * m.m11 + m12 * m.m21;
	out.m12 = m10 * m.m02 + m11 * m.m12 + m12 * m.m22;

	out.m20 = m20 * m.m00 + m21 * m.m10 + m22 * m.m20;
	out.m21 = m20 * m.m01 + m21 * m.m11 + m22 * m.m21;
	out.m22 = m20 * m.m02 + m21 * m.m12 + m22 * m.m22;

	return out;
	}


bool CMatrix::Inverse( CMatrix& out )
	{
	double	det;
	double	eps = 1e-10;

	det = m00 * ( m11 * m22 - m12 * m21 ) -
		  m01 * ( m10 * m22 - m12 * m20 ) +
		  m02 * ( m10 * m21 - m11 * m20 );

	/*
	if( fabs( det ) < eps )
		return false;
	*/

	out.m00 =  ( m11 * m22 - m12 * m21 ) / det;
	out.m10 = -( m10 * m22 - m12 * m20 ) / det;
	out.m20 =  ( m10 * m21 - m11 * m20 ) / det;

	out.m01 = -( m01 * m22 - m02 * m21 ) / det;
	out.m11 =  ( m00 * m22 - m02 * m20 ) / det;
	out.m21 = -( m00 * m21 - m01 * m20 ) / det;

	out.m02 =  ( m01 * m12 - m02 * m11 ) / det;
	out.m12 = -( m00 * m12 - m02 * m10 ) / det;
	out.m22 =  ( m00 * m11 - m01 * m10 ) / det;
	
	return true;
	}


double CMatrix::Determinant()
	{
	double	det;

	det = m00 * ( m11 * m22 - m12 * m21 ) -
		  m01 * ( m10 * m22 - m12 * m20 ) +
		  m02 * ( m10 * m21 - m11 * m20 );

	return det;
	}


bool CMatrix::IsRotation()
	{
	double eps = 1e-6;

	double sum = fabs( m01 ) + fabs( m02 ) + fabs( m12 ) +
				fabs( m10 ) + fabs( m20 ) + fabs( m21 );

	return ( sum > eps )? true : false;
	}


bool CMatrix::XYOnly()
	{
	double eps = 1e-4;

	if( fabs( m02 ) + fabs( m12 ) + fabs( m20 ) + fabs( m21 ) < eps &&
		fabs( m22 - 1.0 ) < eps )
		return true;
	else
		return false;
	}


void CMatrix::GetMatrix( double matr[3][3] ) const
	{
	matr[0][0] = m00;  matr[0][1] = m01;  matr[0][2] = m02;
	matr[1][0] = m10;  matr[1][1] = m11;  matr[1][2] = m12;
	matr[2][0] = m20;  matr[2][1] = m21;  matr[2][2] = m22;
	}


void CMatrix::SetMatrix( double matr[3][3] )
	{
	m00 = matr[0][0];  m01 = matr[0][1];  m02 = matr[0][2];
	m10 = matr[1][0];  m11 = matr[1][1];  m12 = matr[1][2];
	m20 = matr[2][0];  m21 = matr[2][1];  m22 = matr[2][2];
	}


void CMatrix::MakeGLMatrix( double glMatrix[16] ) const
	{
	glMatrix[0] = m00;	glMatrix[4] = m01;	glMatrix[8]  = m02;
	glMatrix[1] = m10;	glMatrix[5] = m11;	glMatrix[9]  = m12;
	glMatrix[2] = m20;	glMatrix[6] = m21;	glMatrix[10] = m22;
	}


//******************** Arrays of basic classes ***********************

//typedef QList< CPoint3d >		CPointArray;
//typedef QList< CVector3d >		CVectorArray;
//typedef QList< CColor >			CColorArray;


struct CIndSeg
	{
	int					i0;
	int					i1;
	bool				flag;

	CIndSeg::CIndSeg()
		{
		}

	CIndSeg( int _i0, int _i1 )
		{
		i0 = _i0;
		i1 = _i1;
		}

	inline bool operator == ( const CIndSeg& rh )
		{
		return(	i0 == rh.i0 && i1 == rh.i1 || i0 == rh.i1 && i1 == rh.i0 );
		}
	};


//******************* Global non-inline operations *********************

bool IntersectLines( 
	CPoint2d&		p0, 
	CPoint2d&		v0,
	CPoint2d&		p1, 
	CPoint2d&		v1,
	CPoint2d&		pnt );


bool IntersectLines( 
	CPoint3d&		p0, 
	CPoint3d&		p1,
	CPoint3d&		q0, 
	CPoint3d&		q1,
	CVector3d		norm,
	CPoint3d&		pnt,
	bool			bInfiniteLines = false );

bool IntersectLines( 
	CPoint3d&		p0, 
	CVector3d&		v0,
	CPoint3d&		p1, 
	CVector3d&		v1,
	CVector3d		norm,
	CPoint3d&		pnt,
	bool			bInfiniteLines = false );


bool		PierceTriangle( CPoint3d pt, CPoint3d pts[3], double& z );

CPoint3d	RotatePoint( CPoint3d& p0, CVector3d& axis, double angle, CPoint3d& p );
CVector3d	RotateVec( CVector3d& axis, double angle, CVector3d& vec );

bool		IsPointInsideTriangle( const CPoint3d& p, CPoint3d pts[3] );

double		dist( CPoint3d& pt, CPoint3d& p0, CPoint3d& p1 );

double		DistPointToSeg( CPoint3d& pt, CPoint3d& p0, CPoint3d& p1 );

double		Angle( CVector3d vec0, CVector3d vec1 );

double		TriangleArea( CPoint3d& pt0, CPoint3d& pt1, CPoint3d& pt2 );

void		ComputeBarycentricCoords( CPoint3d pts[3], CPoint3d& pt, double w[3] );

CColor		ComputeBarycentricColor( CPoint3d pts[3], CPoint3d&	pt, CColor colors[3] );

double		ProjectPointOnLine( CPoint3d& pt, CPoint3d& p0, CPoint3d& p1, CPoint3d* pnt = NULL );

CPoint3d	ProjectPointOnPlane( const CPoint3d& pt, const CPoint3d& p0, const CVector3d& nm );

int			FindNearestEdge( CPoint3d& pt, CPoint3d pts[3] );

CPoint2d	ConstructPolygonInnerPoint( int	n, CPoint2d p[] );
	
bool		SameVectors( CVector3d& a, CVector3d& b );

bool		IsPointInsideFrame( const CPoint3d&	pt, const CPoint3d&	p0, const CPoint3d&	p1 );

bool		Intersect2dSegments( 
				CPoint3d&		p00, 
				CPoint3d&		p01,
				CPoint3d&		p10, 
				CPoint3d&		p11,
				CPoint3d&		pnt );

bool		IsPointInsideTriangle (
				const CPoint3d&		pt,
				const CPoint3d&		pt1,
				const CPoint3d&		pt2,
				const CPoint3d&		pt3 );

bool		IsPointInsideQuad (
				const CPoint3d&		pt,
				const CPoint3d&		pt1,
				const CPoint3d&		pt2,
				const CPoint3d&		pt3,
				const CPoint3d&		pt4 );

int			IntersectTriangleAndQuad( 
				CPoint3d		tp[3], 
				CPoint3d		qp[4], 
				CPoint3d		pts[] );


bool		TriangleIntersectPlane( 
				CPoint3d		pts[3], 
				CPoint3d		pt, 
				CVector3d		nm,
				int				idx[2],
				CPoint3d		p[2] );

bool		TrianglesIntersectEachOther( 
				CPoint3d		p[3], 
				CPoint3d		q[3],
				int				idxFace[2],
				int				idxEdge[2],
				CPoint3d		pts[2] );

bool		SegmentsIntersectEachOther( 
				CPoint3d&		p00, 
				CPoint3d&		p01,
				CPoint3d&		p10, 
				CPoint3d&		p11,
				CPoint3d&		pnt );

bool		SegmentsIntersectEachOther( 
				CPoint2d&		p0, 
				CPoint2d&		p1,
				CPoint2d&		q0, 
				CPoint2d&		q1,
				CPoint2d&		pt );

bool		CheckForGraph( 
				int				np, 
				CPoint2d*		pts, 
				int				nsegs, 
				CIndSeg*		segs );

CVector3d	MakeNormalToVector( const CVector3d& vec );

double		MakeFillet( 
				const CPoint3d&			vert, 
				const CPoint3d&			pt0, 
				const CPoint3d&			pt1,
				CPoint3d&				center,
				CPoint3d&				p0, 
				CPoint3d&				p1 ); 

bool		MakeFillet( 
				const CPoint3d&			vert, 
				const CPoint3d&			pt0, 
				const CPoint3d&			pt1,
				double					rad,
				CPoint3d&				center,
				CPoint3d&				p0, 
				CPoint3d&				p1,
				bool					bStrict = true ); 


CVector3d	Rot90( CVector3d vec );

bool		IntersectLines( CPoint3d& pt00, CPoint3d& pt01, CPoint3d& pt10, CPoint3d& pt11, CPoint3d& pnt );

CVector3d	BisectAngle( CPoint3d& vertex, CPoint3d& pt0, CPoint3d pt1 );

CPoint3d	ArcMiddle( CPoint3d& center, CPoint3d& pt0, CPoint3d& pt1 );

CVector3d	ProjectVectorOnPlane( const CVector3d& vec, const CVector3d& nm, bool bNormalize = true );

CVector3d	OrientVector( const CVector3d& vec1, const CVector3d& vec2 );

CPoint3d LineMirror( 
				const CPoint3d&			pt0,
				const CPoint3d&			pt1,
				const CPoint3d&			pt );

bool SphereLineIntersection( 
				const CPoint3d&			center,
				double					rad,
				const CPoint3d&			pt0,	
				const CPoint3d&			pt1,
				CPoint3d				pts[2] );

bool SphereLineIntersection( 
				const CPoint3d&			center,
				double					rad,
				const CPoint3d&			pnt,	
				const CVector3d&		vec,
				const CPoint3d&			guess,
				CPoint3d&				pt );

double ComputeArc( 
				const CPoint3d&			pt0, 
				const CVector3d&		axis, 
				const CPoint3d&			pt1, 
				CPoint3d&				cnt );

bool MakeArcLineFillet( 
				const CPoint3d&			cnt,				// in
				const CPoint3d&			pt0,				// in
				const CPoint3d&			pt1,				// in
				const CPoint3d&			pt2,				// in
				double					rad,				// in
				CPoint3d&				center,				// out
				CPoint3d&				pnt0,				// out
				CPoint3d&				pnt1 );				// out

CPoint3d ArcCenterByTwoPointsAndRadius(
				const CPoint3d&			pt0,
				const CPoint3d&			pt1,
				double					rad,
				const CVector3d&		vec );				// points from mid point toward the center

bool PlaneLineIntersection( 
				const CPoint3d&			ptPlane,
				const CVector3d&		nmPlane,
				const CPoint3d&			pt,
				const CVector3d&		vec,
				CPoint3d&				pnt );

double DistPointToPlane( 
				const CPoint3d&			pt,
				const CPoint3d&			ptPlane,
				const CVector3d&		nmPlane );

bool FindCircleCenter( 
				CPoint3d				pts[3], 
				CPoint3d&				center );


CVector3d BisectChords(
				const CPoint3d&			pt0,
				const CPoint3d&			pt1,
				const CPoint3d&			pt2 );
