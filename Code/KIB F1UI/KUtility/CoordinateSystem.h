#pragma once

#pragma warning( disable : 4996 4805 )
#include <IwAxis2Placement.h>
#pragma warning( default : 4996 4805 )
#include <qmetatype.h>
#include <qstringlist.h>

class CoordSystem : public IwAxis2Placement
{
public:
	CoordSystem() : IwAxis2Placement(){}
    // o, x, y: orgin & x and y axis
    // icons (optional, refer to QIcon::QIcon(const QString & fileName))
    //   - empty string list (default) : use default icons
    //   - string list of size 8: each string corresponds to a view icon of
    //       (0) front (1) back (2) left (3) right (4) top (5) bottom (6) isoFront (7) isoBack
	CoordSystem(const QString& Name, IwPoint3d o, IwVector3d x, IwVector3d y, QStringList icons=QStringList()) 
        : IwAxis2Placement(o, x, y), name(Name), iconFiles(icons) {}

    QString GetName() const { return name; }
    void SetName(const QString& s) { name = s; }

    QStringList GetIconFiles() const { return iconFiles; }
    void SetIconFiles(QStringList strList) { iconFiles = strList; }

protected:
    QStringList iconFiles;
    QString     name;
};

Q_DECLARE_METATYPE(CoordSystem)
