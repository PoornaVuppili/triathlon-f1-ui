#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "OutputResultsJigs.h"
#include "FemoralPart.h"
#include "CartilageSurface.h"
#include "OutlineProfileJigs.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "SideSurfaceJigs.h"
#include "MakeSideSurfaceJigs.h"
#include "SolidPositionJigs.h"
#include "StylusJigs.h"
#include "AnteriorSurfaceJigs.h"
#include "AdvancedControlJigs.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "..\KAppTotal\ProgressWidget.h"
#include "../KUtility/KWaitCursor.h"
#include <QDomDocument>
#include <QTextStream>
#include <QFile>

COutputResultsJigs::COutputResultsJigs( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "COutputResultsJigs";

	m_toolbar = new QToolBar("Output Results");

	m_actOutputResults = m_toolbar->addAction("OutputJigs");
	SetActionAsTitle( m_actOutputResults );

	m_actOutput = m_toolbar->addAction("Output Results");
	EnableAction( m_actOutput, true );

	m_actOutputAuxiliaryEntities = m_toolbar->addAction("Aux Ent");
	EnableAction( m_actOutputAuxiliaryEntities, true );

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	m_toolbar->setMovable( false );	
	m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actOutput, SIGNAL( triggered() ), this, SLOT( OnOutputResults() ) );
	m_pMainWindow->connect( m_actOutputAuxiliaryEntities, SIGNAL( triggered() ), this, SLOT( OnOutputAuxiliaryEntities() ) );
	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

}

COutputResultsJigs::~COutputResultsJigs()
{
}

void COutputResultsJigs::Display(Viewport* vp)
{		

}

void COutputResultsJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("COutputResultsJigs::OnAccept()") );

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void COutputResultsJigs::OnOutputResults()
{
	m_pDoc->AppendLog( QString("COutputResultsJigs::OnOutputResults()") );

	KWaitCursor waitCursor;

    unique_ptr<OutputterOfResultsJigs> pOut(new OutputterOfResultsJigs(m_pDoc));

	QString	sFileName, sDir;
	sFileName = m_pDoc->GetFileName();
	if ( sFileName.isEmpty() )
	{
		QString msg("Need to save the file before output.");
		QMessageBox::information(NULL, "Output Results", msg, QMessageBox::Ok);

		return;
	}

	// Check "_Outputs" exist or not
	int length = sFileName.length();
	sFileName.remove(length-3, 3);

	QString outputFolder_Outputs = sFileName + "_Outputs" + QDir::separator();
	QDir dir_Outputs( outputFolder_Outputs );
	if( !dir_Outputs.exists() )
	{
		dir_Outputs.mkdir( outputFolder_Outputs );

		//QString msg("Need to output femoral implant before outputing position jigs.");
		//QMessageBox::information(NULL, "Output Results", msg, QMessageBox::Ok);
		//return;
	}

	//
	QString	sPatientId = m_pDoc->GetPatientId();
	QString outputFolder_Jigs = sFileName + "_Outputs" + QDir::separator() + sPatientId + "_Jigs_Outputs" + QDir::separator();

	// create the outputFolder_Jigs before displaying the message to the users.
	// It takes time for operating system to create the folder.
	QDir dir_Jigs( outputFolder_Jigs );

	if( !dir_Jigs.exists() )
	{
		dir_Jigs.mkdir( outputFolder_Jigs );
	}

	if( !dir_Jigs.exists() )// try again if not existing and display error message to users
	{
		bool bOK = dir_Jigs.mkdir( outputFolder_Jigs );

		if( !bOK )
		{
    		QMessageBox::information(NULL, "Output Results", "Cannot save the data. Please check permissions.", QMessageBox::Ok);
			m_pDoc->AppendLog( QString("COutputResultsJigs::OnOutputResults, Cannot save the data."));

			return;
		}
	}	

    QString msg;
    if (!pOut->DoValidate(outputFolder_Jigs, msg))
    {
        QApplication::restoreOverrideCursor();
        QMessageBox::information(NULL, "Output Results", msg, QMessageBox::Ok);

        return;
    }

    pOut->DoOutput(outputFolder_Jigs);

	EnableAction( m_actOutput, false );
}

/////////////////////////////////////////////////////////////////////////////////////////

OutputterOfResultsJigs::OutputterOfResultsJigs(CTotalDoc* pDoc)
    : m_pDoc(pDoc)
{
}

bool OutputterOfResultsJigs::DoValidate(QString const& outputFolder_Jigs, QString &msg)
{

	// Validate Jigs
	m_pDoc->GetView()->OnValidateJigs(MAN_ACT_TYPE_VALIDATE, false);
	bool allGoodResults;
	QString validationReport;
	validationReport = m_pDoc->GetValidationReportJigs(allGoodResults);

	// Print the validation report
	QString outputFileName = outputFolder_Jigs + m_pDoc->GetPatientId() + QString("_Validation_Report_Jigs.pdf");
	m_pDoc->GetView()->OnPrintValidationReportJigs( outputFileName );
	m_pDoc->GetView()->OnAcceptValidationReportJigs();


	CAnteriorSurfaceJigs *pAnteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();
	CSolidPositionJigs *pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();
	
	bool upToDateSolidPositionJigs = pSolidPositionJigs && pSolidPositionJigs->GetUpToDateStatus();
	if ( !upToDateSolidPositionJigs && !m_pDoc->GetAdvancedControlJigs()->GetOutputAnteriorSurfaceRegardless() )
	{
		msg = QString("Not all jig design steps are up-to-date.\nCan not output the results.\n");
		return false;
	}

	bool upToDateAnteriorSurfaceJigs = pAnteriorSurfaceJigs && pAnteriorSurfaceJigs->GetUpToDateStatus();
	if ( !upToDateAnteriorSurfaceJigs )
	{
		msg = QString("Not all jig design steps are up-to-date.\nCan not output the results.\n");
		return false;
	}

    return true;
}
///////////////////////////////////////////////////////////////////
// Note: need to call DoValidate() to output the validation report,
// which is part of output requirements.
bool OutputterOfResultsJigs::DoOutput(QString const& outputFolder_Jigs)
{
	// Output the jigs 
	OutputStylusSurfaceJigs(outputFolder_Jigs);
	OutputAnteriorSurfaceJigs(outputFolder_Jigs);
	OutputOsteophyteSurfaceJigs(outputFolder_Jigs);
	OutputCartilageSurfaceJigs(outputFolder_Jigs);
	OutputInnerSurfaceJigs(outputFolder_Jigs);
	OutputOuterSurfaceJigs(outputFolder_Jigs);
	OutputOuterInsetSurfaceJigs(outputFolder_Jigs);
	OutputSideSurfaceJigsUnTrimmed(outputFolder_Jigs);
	OutputSolidPositionJigs(outputFolder_Jigs);
	OutputXMLInfo(outputFolder_Jigs);

    return true;
}

void OutputterOfResultsJigs::OutputSolidPositionJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("COutputResultsJigs::OnOutputSolidPositionJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Position Jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Solid_Position_Jigs.igs";
	}

	CBrepArray				BrepSolidArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	CAdvancedControlJigs* advCtrl = m_pDoc->GetAdvancedControlJigs();

	bool haveSolidPositionJigs = false;
	// output solid implant
	if (1)
	{
		CSolidPositionJigs* pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();
		if (pSolidPositionJigs)
		{
			IwBrep* solidPositionJigsBrep = pSolidPositionJigs->GetIwBrep();
			if (solidPositionJigsBrep)
			{
				BrepSolidArray.Add(solidPositionJigsBrep);
				haveSolidPositionJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveSolidPositionJigs)
		WriteIges( filename, NULL, NULL, NULL, NULL, &BrepSolidArray );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputStylusSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("COutputResultsJigs::OnOutputStylusSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Stylus_Surface_Jigs.igs";
	}

	CSurfArray				SurfArray;
	CBrepArray				BrepSolidArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveStylusSurfaceJigs = false;

	// output stylus contact surface 
	if (1)
	{
		CStylusJigs* pStylusJigs = m_pDoc->GetStylusJigs();
		if (pStylusJigs)
		{
			IwBSplineSurface* stylusContactSurface = pStylusJigs->GetStylusContactSurface();
			if (stylusContactSurface)
			{
				// Output stylusContactSurface
				SurfArray.Add(stylusContactSurface);
				haveStylusSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveStylusSurfaceJigs)
		WriteIges( filename, NULL, NULL, &SurfArray, NULL, NULL );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputAnteriorSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("COutputResultsJigs::OnOutputAnteriorSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sSolidFileName, sDir;;
	QFileDialog				Dlg;

	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Anterior_Surface_Jigs.igs";
		sSolidFileName = outputFolder + sPatientId + "_Anterior_Surface_Jigs_Solid.igs";
	}

	CSurfArray				SurfArray;
	CBrepArray				BrepSolidArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveAnteriorSurfaceJigs = false;
	bool haveSolidAnteriorSurfaceJigs = false;

	// output anterior surface 
	if (1)
	{
		CAnteriorSurfaceJigs* pAnteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();
		if (pAnteriorSurfaceJigs)
		{
			IwBrep* anteriorSurfaceJigsBrep = pAnteriorSurfaceJigs->GetIwBrep();
			if (anteriorSurfaceJigsBrep)
			{
				IwTArray<IwFace*> faces;
				anteriorSurfaceJigsBrep->GetFaces(faces);
				if ( faces.GetSize() > 0 )
				{
					IwBSplineSurface* antSurf = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
					if ( antSurf )
					{
						// Anterior surface
						SurfArray.Add(antSurf);
						haveAnteriorSurfaceJigs = true;
					}
				}
			}
			//
			IwBrep* solidAnteriorSurfaceJigsBrep = pAnteriorSurfaceJigs->SolidifyAnteriorSurfaceForOutput();
			if (solidAnteriorSurfaceJigsBrep)
			{
				BrepSolidArray.Add(solidAnteriorSurfaceJigsBrep);
				haveSolidAnteriorSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveAnteriorSurfaceJigs)
		WriteIges( filename, NULL, NULL, &SurfArray, NULL, NULL );	

	// write to an iges file
	if (haveSolidAnteriorSurfaceJigs)
		WriteIges( sSolidFileName, NULL, NULL, NULL, NULL, &BrepSolidArray );	

}

void OutputterOfResultsJigs::OutputOsteophyteSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputOsteophyteSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Osteophyte Surface jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Osteophyte_Surface_Jigs.igs";
	}

	CBrepArray				BrepSolidArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveOsteophyteSurfaceJigs = false;
	// output osteophyte surface
	if (1)
	{
		CFemoralPart* pOsteophyteSurfaceJigs = m_pDoc->GetOsteophyteSurface();
		if (pOsteophyteSurfaceJigs)
		{
			IwBrep* osteophyteSurfaceJigsBrep = pOsteophyteSurfaceJigs->GetIwBrep();
			if (osteophyteSurfaceJigsBrep)
			{
				BrepSolidArray.Add(osteophyteSurfaceJigsBrep);
				haveOsteophyteSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveOsteophyteSurfaceJigs)
		WriteIges( filename, NULL, NULL, NULL, NULL, &BrepSolidArray );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputCartilageSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputCartilageSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Cartilage Surface jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Cartilage_Surface_Jigs.igs";
	}

	CBrepArray				BrepSolidArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveCartilageSurfaceJigs = false;
	// output cartilage surface
	if (1)
	{
		CCartilageSurface* pCartilageSurfaceJigs = m_pDoc->GetCartilageSurface();
		if (pCartilageSurfaceJigs)
		{
			IwBrep* cartilageSurfaceJigsBrep = pCartilageSurfaceJigs->GetIwBrep();
			if (cartilageSurfaceJigsBrep)
			{
				BrepSolidArray.Add(cartilageSurfaceJigsBrep);
				haveCartilageSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveCartilageSurfaceJigs)
		WriteIges( filename, NULL, NULL, NULL, NULL, &BrepSolidArray );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputInnerSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputInnerSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Inner Surface jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Inner_Surface_Jigs.igs";
	}

	CSurfArray				SurfArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveInnerSurfaceJigs = false;
	// output anterior surface
	if (1)
	{
		CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
		if (pInnerSurfaceJigs)
		{
			IwBrep* innerSurfaceJigsBrep = pInnerSurfaceJigs->GetIwBrep();
			if (innerSurfaceJigsBrep)
			{
				IwTArray<IwFace*> faces;
				IwFace* face;
				innerSurfaceJigsBrep->GetFaces(faces);
				if ( faces.GetSize() != 1 )
					return;
				face = faces.GetAt(0);
				IwBSplineSurface* innerSurface = (IwBSplineSurface*)face->GetSurface();
				SurfArray.Add(innerSurface);
				haveInnerSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveInnerSurfaceJigs)
		WriteIges( filename, NULL, NULL, &SurfArray, NULL, NULL );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputSideSurfaceJigsUnTrimmed(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputSideSurfaceJigsUnTrimmed()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Untrimmed Side Surface jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Side_Surface_Jigs_Untrimmed.igs";
	}

	CSurfArray				SurfArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveSideSurfaceJigs = false;
	// output side surface
	if (1)
	{
		IwBSplineSurface* sideSurfaceUntrimmed = CMakeSideSurfaceJigs::CreateSideSurfaceUntrimmed(m_pDoc);
		if (sideSurfaceUntrimmed)
		{
			// Split at the middle. SolidWorks prefers split surface, rather than one single surface.
			IwExtent2d dom = sideSurfaceUntrimmed->GetNaturalUVDomain();
			IwSurface *leftSurface, *rightSurface;
			sideSurfaceUntrimmed->SplitAt(m_pDoc->GetIwContext(), dom.GetUInterval().Evaluate(0.5), IW_SP_U, leftSurface, rightSurface);
			SurfArray.Add(leftSurface);
			SurfArray.Add(rightSurface);
			haveSideSurfaceJigs = true;
		}
	}

	// write to an iges file
	if (haveSideSurfaceJigs)
		WriteIges( filename, NULL, NULL, &SurfArray, NULL, NULL );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputOuterSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputOuterSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Outer Surface jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Outer_Surface_Jigs.igs";
	}

	CSurfArray				SurfArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveOuterSurfaceJigs = false;
	// output outer surface
	if (1)
	{
		COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
		if (pOuterSurfaceJigs)
		{
			IwBrep* outerSurfaceJigsBrep = pOuterSurfaceJigs->GetIwBrep();
			if (outerSurfaceJigsBrep)
			{
				// inset the surface
				IwTArray<IwFace*> faces;
				IwFace* face;
				outerSurfaceJigsBrep->GetFaces(faces);
				if ( faces.GetSize() != 1 )
					return;
				face = faces.GetAt(0);
				IwBSplineSurface* outerSurface = (IwBSplineSurface*)face->GetSurface();
				SurfArray.Add(outerSurface);
				haveOuterSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveOuterSurfaceJigs)
		WriteIges( filename, NULL, NULL, &SurfArray, NULL, NULL );	

	//m_pView->Redraw();
}

void OutputterOfResultsJigs::OutputOuterInsetSurfaceJigs(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputOuterInsetSurfaceJigs()") );

	IwContext& iwContext = m_pDoc->GetIwContext();

	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output Outer Inset Surface jigs", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
			sFileName += ".igs";
	}
	else
	{
		QString					sPatientId = m_pDoc->GetPatientId();
		sFileName = outputFolder + sPatientId + "_Outer_Inset_Surface_Jigs.igs";
	}

	CSurfArray				SurfArray;
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	bool haveOuterInsetSurfaceJigs = false;
	// output anterior surface
	if (1)
	{
		double insetDist = -1.0*m_pDoc->GetVarTableValue("JIGS LABELING SURFACE INSET DISTANCE");
		COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
		COutlineProfileJigs *pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
		if (pOuterSurfaceJigs && pOutlineProfileJigs)
		{
			IwBrep* outerSurfaceJigsBrep = pOuterSurfaceJigs->GetIwBrep();
			IwTArray<IwPoint3d> tabPoints;
			pOutlineProfileJigs->GetOutlineProfileTabPoints(tabPoints);
			if (outerSurfaceJigsBrep && tabPoints.GetSize()>0)
			{
				// inset the surface
				IwTArray<IwFace*> faces;
				IwFace* face;
				outerSurfaceJigsBrep->GetFaces(faces);
				if ( faces.GetSize() != 1 )
					return;
				face = faces.GetAt(0);
				IwBSplineSurface* outerSurface = (IwBSplineSurface*)face->GetSurface();
				IwBSplineSurface* outerInsetSurface;
				outerSurface->ApproximateOffsetSurface(m_pDoc->GetIwContext(), insetDist, 0.1, outerInsetSurface);
				// Trim the tab area (only output the main contact area).
				IwPoint3d cPnt;
				IwPoint2d param0, param2;
				DistFromPointToSurface(tabPoints.GetAt(0), outerInsetSurface, cPnt, param0);
				DistFromPointToSurface(tabPoints.GetAt(2), outerInsetSurface, cPnt, param2);
				double minV=min(param0.y, param2.y)+1.0;// make it smaller
				double maxV=max(param0.y, param2.y)-1.0;// make it smaller
				IwExtent2d surfDom = outerInsetSurface->GetNaturalUVDomain();
				IwExtent2d surfDomTrim = IwExtent2d(surfDom.GetUMin(), minV, surfDom.GetUMax()-5.0, maxV);// also trim a bit (5mm) on anterior
				outerInsetSurface->TrimWithDomain(surfDomTrim);
				outerInsetSurface->RemoveKnots(fabs(0.5*insetDist), FALSE, TRUE);// smooth a bit
				SurfArray.Add(outerInsetSurface);
				haveOuterInsetSurfaceJigs = true;
			}
		}
	}

	// write to an iges file
	if (haveOuterInsetSurfaceJigs)
		WriteIges( filename, NULL, NULL, &SurfArray, NULL, NULL );	

	//m_pView->Redraw();
}

void COutputResultsJigs::OnOutputAuxiliaryEntities()
{
	m_pDoc->AppendLog( QString("COutputResultsJigs::OnOutputAuxiliaryEntities()") );

	IwContext& iwContext = m_pDoc->GetIwContext();
	CPointArray				PointArray;
	CCurveArray				CurveArray;
	CSurfArray				SurfArray;
	CBrepArray				BrepTrimmedSurfArray;
	CBrepArray				BrepSolidArray;
	char					filename[2048];

	KWaitCursor waitCursor;

	CAdvancedControlJigs* advCtrl = m_pDoc->GetAdvancedControlJigs();

	QString entityName;
	bool haveEntities = false;

	// output osteophyte surface
	if ( advCtrl->GetOutputOsteophyteSurface() )
	{
		CFemoralPart* pOsteophyteSurface = m_pDoc->GetOsteophyteSurface();
		if (pOsteophyteSurface)
		{
			IwBrep* osteophyteSurfaceBrep = pOsteophyteSurface->GetIwBrep();
			if (osteophyteSurfaceBrep)
			{
				BrepSolidArray.Add(osteophyteSurfaceBrep);
				haveEntities = true;
				entityName = entityName + QString("Osteophyte Surface"); 
			}
		}
	}

	// output cartilage surface
	if (advCtrl->GetOutputCartilageSurface())
	{
		CCartilageSurface* pCartilageSurface = m_pDoc->GetCartilageSurface();
		if (pCartilageSurface)
		{
			IwBrep* cartilageSurfaceBrep = pCartilageSurface->GetIwBrep();
			if (cartilageSurfaceBrep)
			{
				BrepTrimmedSurfArray.Add(cartilageSurfaceBrep);
				haveEntities = true;
				if (entityName.isEmpty())
					entityName = QString("Cartilage Surface"); 
				else
					entityName = entityName + QString(" & Cartilage Surface"); 
			}
		}
	}

	// output ThreeSurfaces
	if (advCtrl->GetOutputThreeSurfaces())
	{
		CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
		COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
		CSideSurfaceJigs* pSideSurfaceJigs = m_pDoc->GetSideSurfaceJigs();
		if (pInnerSurfaceJigs && pOuterSurfaceJigs && pSideSurfaceJigs)
		{
			IwBrep* innerSurfaceJigsBrep = pInnerSurfaceJigs->GetIwBrep();
			IwBrep* outerSurfaceJigsBrep = pOuterSurfaceJigs->GetIwBrep();
			IwBrep* sideSurfaceJigsBrep = pSideSurfaceJigs->GetIwBrep();
			if (innerSurfaceJigsBrep && outerSurfaceJigsBrep && sideSurfaceJigsBrep)
			{
				BrepTrimmedSurfArray.Add(innerSurfaceJigsBrep);
				BrepTrimmedSurfArray.Add(outerSurfaceJigsBrep);
				BrepTrimmedSurfArray.Add(sideSurfaceJigsBrep);
				haveEntities = true;
				if (entityName.isEmpty())
					entityName = QString("Three Surfaces"); 
				else
					entityName = entityName + QString(" & Three Surfaces"); 
			}
		}
	}

	// write to an iges file
	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;

	sDir = GetStringPref( "Last used part folder" );
	QString sTitle = QString("Output %1").arg(entityName);

	sFileName = Dlg.getSaveFileName( NULL, sTitle, sDir, sFileTypes, 0, options  );

	if( sFileName.isNull() )
		return;

	// for position jigs, "jigs" could be part of file name in the end.
	if( sFileName.endsWith( "jigs", Qt::CaseInsensitive ) )
		sFileName += ".igs";

	if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
		sFileName += ".igs";

	QStringToCharArray( sFileName, filename );

	if (haveEntities)
		WriteIges( filename, &PointArray, &CurveArray, &SurfArray, &BrepTrimmedSurfArray, &BrepSolidArray );	

	m_pView->Redraw();

	EnableAction( m_actOutputAuxiliaryEntities, false );
}

/////////////////////////////////////////////////////////////////////////////
// Output any data that are able to saved as XML file, which includes
// outer fillet info (control points (x,y,z), radii(double), and percentage 
// of the point location.), peg info, and casting gates/vents info.
/////////////////////////////////////////////////////////////////////////////
void OutputterOfResultsJigs::OutputXMLInfo(const QString& outputFolder)
{
	m_pDoc->AppendLog( QString("OutputterOfResultsJigs::OutputXMLInfo()") );

	QString					sFileTypes = "xml files (*.xml);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;;
	QFileDialog				Dlg;
	QString					sPatientId = m_pDoc->GetPatientId();

	if (outputFolder.isEmpty())
	{
		sDir = GetStringPref( "Last used part folder" );
		sFileName = Dlg.getSaveFileName( NULL, "Output XML Info", sDir, sFileTypes, 0, options  );
		if( sFileName.isNull() )
			return;
		if( !sFileName.endsWith( "xml", Qt::CaseInsensitive ) )
			sFileName += ".xml";
	}
	else
	{
		sFileName = outputFolder + sPatientId + "_Jigs_Output_Info.xml";
	}

	// Get femoral Axes
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	// Get side info
	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Get stylus info
	IwPoint3d stylusPosition, stylusFinalPosition, stylusTipPosition, stylusDelta;
	double stylusSketchHeight, stylusSketchShift, stylusHeadLength, stylusGap;

	CStylusJigs* pStylusJigs = m_pDoc->GetStylusJigs();
	bool hasStylus = false;
	if ( pStylusJigs )
	{
		pStylusJigs->GetStylusPosition(stylusPosition, stylusTipPosition, stylusDelta, stylusGap);
		hasStylus = true;
		stylusFinalPosition = stylusPosition + stylusDelta.x*wslAxes.GetXAxis() + stylusDelta.y*wslAxes.GetYAxis();
		IwVector3d vec = stylusFinalPosition - stylusTipPosition;
		stylusSketchHeight = 7.0 - vec.Dot(wslAxes.GetYAxis());// Along Y-axis. Please see the stylus start part for why it is "7.0".
		stylusSketchShift = vec.Dot(wslAxes.GetXAxis());// Along X-axis
		if ( fabs(stylusSketchShift) < 0.001 )
			stylusSketchShift = 0.0;
		stylusHeadLength = 22.0 + stylusDelta.z;// Along Z-axis
	}

	// Get fillet info
	IwTArray<IwPoint3d> filletPoints, filletPointsCCW;
	IwTArray<double> filletRadii, filletRadiiCCW;

	bool hasFillets = false;
	if ( pStylusJigs )
	{
		pStylusJigs->GetFilletPointsRadii(filletPoints, filletRadii);
		if ( filletRadii.GetSize() > 0 )
			hasFillets = true;
		// Make them follow the CCW direction
		if ( positiveSideIsLateral )
		{
			filletRadiiCCW.Add(filletRadii.GetAt(2));
			filletRadiiCCW.Add(filletRadii.GetAt(3));
			filletRadiiCCW.Add(filletRadii.GetAt(5));
			filletRadiiCCW.Add(filletRadii.GetAt(4));
			filletRadiiCCW.Add(filletRadii.GetAt(1));
			filletRadiiCCW.Add(filletRadii.GetAt(0));
			//
			filletPointsCCW.Add(filletPoints.GetAt(2));
			filletPointsCCW.Add(filletPoints.GetAt(3));
			filletPointsCCW.Add(filletPoints.GetAt(5));
			filletPointsCCW.Add(filletPoints.GetAt(4));
			filletPointsCCW.Add(filletPoints.GetAt(1));
			filletPointsCCW.Add(filletPoints.GetAt(0));
		}
		else
		{
			filletRadiiCCW.Add(filletRadii.GetAt(0));
			filletRadiiCCW.Add(filletRadii.GetAt(1));
			filletRadiiCCW.Add(filletRadii.GetAt(4));
			filletRadiiCCW.Add(filletRadii.GetAt(5));
			filletRadiiCCW.Add(filletRadii.GetAt(3));
			filletRadiiCCW.Add(filletRadii.GetAt(2));
			//
			filletPointsCCW.Add(filletPoints.GetAt(0));
			filletPointsCCW.Add(filletPoints.GetAt(1));
			filletPointsCCW.Add(filletPoints.GetAt(4));
			filletPointsCCW.Add(filletPoints.GetAt(5));
			filletPointsCCW.Add(filletPoints.GetAt(3));
			filletPointsCCW.Add(filletPoints.GetAt(2));
		}
	}

	// Get anterior Surface info
	CAnteriorSurfaceJigs *pAnteriorSurfaceJigs= m_pDoc->GetAnteriorSurfaceJigs();
	bool hasAnteriorSurface = false;
	IwPoint3d lateralPeakPoint;
	if ( pAnteriorSurfaceJigs )
	{
		lateralPeakPoint = pAnteriorSurfaceJigs->GetLateralPeakPoint();
		hasAnteriorSurface = true;
	}

	// Get outline profile jigs notch point & window cut angles
	COutlineProfileJigs *pOutlineProfileJigs= m_pDoc->GetOutlineProfileJigs();
	bool hasOutlineProfileJigs = false;
	IwPoint3d outlineProfileJigsNotchPoint;
	IwTArray<double> outlineProfileJigsWindowCutAngles;
	double outlineProfileJigsPosWindowCutAngle, outlineProfileJigsNegWindowCutAngle;
	double outlineProfileJigsF2PosWindowCutAngle, outlineProfileJigsF2NegWindowCutAngle;
	if ( pOutlineProfileJigs )
	{
		outlineProfileJigsNotchPoint = pOutlineProfileJigs->GetJigsNotchPoint();
		pOutlineProfileJigs->GetWindowCutAngles(outlineProfileJigsWindowCutAngles);
		outlineProfileJigsPosWindowCutAngle = outlineProfileJigsWindowCutAngles.GetAt(0);
		outlineProfileJigsNegWindowCutAngle = outlineProfileJigsWindowCutAngles.GetAt(1);
		outlineProfileJigsF2PosWindowCutAngle = outlineProfileJigsWindowCutAngles.GetAt(2);
		outlineProfileJigsF2NegWindowCutAngle = outlineProfileJigsWindowCutAngles.GetAt(3);
		hasOutlineProfileJigs = true;
	}

	///////////////////////////////////////////////////////////////////////////////////////
	QFile file(sFileName);
	bool bOK = file.open( QIODevice::WriteOnly | QIODevice::Text );

	const int IndentSize = 4;

	// 
    QDomDocument doc;
	QDomElement outputInfo = doc.createElement("OutputInfoJigs");
	doc.appendChild(outputInfo);

	// TriathlonF1Jigs version
    QDomElement TriathlonF1JigsInfo = doc.createElement("TriathlonF1JigsInfo");
	outputInfo.appendChild(TriathlonF1JigsInfo);
	QDomElement VersionNumber = doc.createElement("VersionNumber");
	TriathlonF1JigsInfo.appendChild(VersionNumber);

	QString appVersion = m_pDoc->GetView()->GetMainWindow()->GetAppVersion();
	QDomText version_text = doc.createTextNode(QString::number(ITOTAL_RELEASE) + '.' + QString::number(ITOTAL_REVISION) + '.' + QString::number(ITOTAL_DATAFILE_VERSION) + '.' + appVersion);
	VersionNumber.appendChild(version_text);

	// User info
    QDomElement userInfo = doc.createElement("UserInfo");
	outputInfo.appendChild(userInfo);
	QDomElement userName = doc.createElement("UserName");
	userInfo.appendChild(userName);
	QDomText userName_text = doc.createTextNode( m_pDoc->GetCurrentUserName() );
	userName.appendChild(userName_text);
	QDomElement computerName = doc.createElement("ComputerName");
	userInfo.appendChild(computerName);
	QDomText computerName_text = doc.createTextNode( m_pDoc->GetCurrentComputerName() );
	computerName.appendChild(computerName_text);

	// IGES Files
    QDomElement IGESFiles = doc.createElement("IGES");
	outputInfo.appendChild(IGESFiles);
	// Solid iges files
	QDomElement Solid = doc.createElement("Solid");
	IGESFiles.appendChild(Solid);
	QDomElement Solid_Position_Jigs = doc.createElement("Solid_Position_Jigs");
	Solid.appendChild(Solid_Position_Jigs);
	QDomText Solid_Position_Jigs_text = doc.createTextNode( sPatientId + QString("_Solid_Position_Jigs.igs") );
	Solid_Position_Jigs.appendChild(Solid_Position_Jigs_text);
	QDomElement Anterior_Surface_Jigs_Solid = doc.createElement("Anterior_Surface_Jigs_Solid");
	Solid.appendChild(Anterior_Surface_Jigs_Solid);
	QDomText Anterior_Surface_Jigs_Solid_text = doc.createTextNode( sPatientId + QString("_Anterior_Surface_Jigs_Solid.igs") );
	Anterior_Surface_Jigs_Solid.appendChild(Anterior_Surface_Jigs_Solid_text);
	// surface/plane iges files
	QDomElement Surfaces = doc.createElement("Surfaces");
	IGESFiles.appendChild(Surfaces);
	QDomElement Anterior_Surface_Jigs = doc.createElement("Anterior_Surface_Jigs");
	Surfaces.appendChild(Anterior_Surface_Jigs);
	QDomText Anterior_Surface_Jigs_text = doc.createTextNode( sPatientId + QString("_Anterior_Surface_Jigs.igs") );
	Anterior_Surface_Jigs.appendChild(Anterior_Surface_Jigs_text);
	QDomElement Stylus_Surface_Jigs = doc.createElement("Stylus_Surface_Jigs");
	Surfaces.appendChild(Stylus_Surface_Jigs);
	QDomText Stylus_Surface_Jigs_text = doc.createTextNode( sPatientId + QString("_Stylus_Surface_Jigs.igs") );
	Stylus_Surface_Jigs.appendChild(Stylus_Surface_Jigs_text);
	QDomElement Cartilage_Surface_Jigs = doc.createElement("Cartilage_Surface_Jigs");
	Surfaces.appendChild(Cartilage_Surface_Jigs);
	QDomText Cartilage_Surface_Jigs_text = doc.createTextNode( sPatientId + QString("_Cartilage_Surface_Jigs.igs") );
	Cartilage_Surface_Jigs.appendChild(Cartilage_Surface_Jigs_text);
	QDomElement Inner_Surface_Jigs = doc.createElement("Inner_Surface_Jigs");
	Surfaces.appendChild(Inner_Surface_Jigs);
	QDomText Inner_Surface_Jigs_text = doc.createTextNode( sPatientId + QString("_Inner_Surface_Jigs.igs") );
	Inner_Surface_Jigs.appendChild(Inner_Surface_Jigs_text);
	QDomElement Osteophyte_Surface_Jigs = doc.createElement("Osteophyte_Surface_Jigs");
	Surfaces.appendChild(Osteophyte_Surface_Jigs);
	QDomText Osteophyte_Surface_Jigs_text = doc.createTextNode( sPatientId + QString("_Osteophyte_Surface_Jigs.igs") );
	Osteophyte_Surface_Jigs.appendChild(Osteophyte_Surface_Jigs_text);
	QDomElement Outer_Inset_Surface_Jigs = doc.createElement("Outer_Inset_Surface_Jigs");
	Surfaces.appendChild(Outer_Inset_Surface_Jigs);
	QDomText Outer_Inset_Surface_Jigs_text = doc.createTextNode( sPatientId + QString("_Outer_Inset_Surface_Jigs.igs") );
	Outer_Inset_Surface_Jigs.appendChild(Outer_Inset_Surface_Jigs_text);
	QDomElement Side_Surface_Jigs_Untrimmed = doc.createElement("Side_Surface_Jigs_Untrimmed");
	Surfaces.appendChild(Side_Surface_Jigs_Untrimmed);
	QDomText Side_Surface_Jigs_Untrimmed_text = doc.createTextNode( sPatientId + QString("_Side_Surface_Jigs_Untrimmed.igs") );
	Side_Surface_Jigs_Untrimmed.appendChild(Side_Surface_Jigs_Untrimmed_text);

	// Stylus Info
	if ( hasStylus )
 	{
		QDomElement stylusInfo = doc.createElement("StylusInfo");
		outputInfo.appendChild(stylusInfo);

		QDomElement basePosition = doc.createElement("BasePosition");
		stylusInfo.appendChild(basePosition);
		{
			QDomElement point = doc.createElement("Point");
			QDomElement xyz = doc.createElement("XYZ");
			QDomText xyz_text = doc.createTextNode(QString::number(stylusTipPosition.x,'g',12) + ',' + QString::number(stylusTipPosition.y,'g',12) + ',' + QString::number(stylusTipPosition.z,'g',12));

			basePosition.appendChild(point);
			point.appendChild(xyz);
			xyz.appendChild(xyz_text);
		}

		QDomElement sketchHeight = doc.createElement("StylusSketchHeight");
		stylusInfo.appendChild(sketchHeight);
		{
			QDomElement height = doc.createElement("Distance");
			QDomText height_text = doc.createTextNode(QString::number(stylusSketchHeight,'g',12) );

			sketchHeight.appendChild(height);
			height.appendChild(height_text);
		}

		QDomElement sketchShift = doc.createElement("StylusSketchShift");
		stylusInfo.appendChild(sketchShift);
		{
			QDomElement shift = doc.createElement("Distance");
			QDomText shift_text = doc.createTextNode(QString::number(stylusSketchShift,'g',12) );

			sketchShift.appendChild(shift);
			shift.appendChild(shift_text);
		}

		QDomElement stylusLength = doc.createElement("StylusLength");
		stylusInfo.appendChild(stylusLength);
		{
			QDomElement length = doc.createElement("Distance");
			QDomText length_text = doc.createTextNode(QString::number(stylusHeadLength,'g',12) );

			stylusLength.appendChild(length);
			length.appendChild(length_text);
		}

	}


	// Tab fillets
	if ( hasFillets )
	{
		QDomElement tabFillet = doc.createElement("TabFillets");
		outputInfo.appendChild(tabFillet);

		for (unsigned i=0; i<filletRadiiCCW.GetSize(); i++)
		{
			QDomElement point = doc.createElement("Point");
			QDomElement xyz = doc.createElement("XYZ");
			QDomElement radius = doc.createElement("Radius");
			QDomText xyz_text = doc.createTextNode(QString::number(filletPointsCCW.GetAt(i).x,'g',12) + ',' + QString::number(filletPointsCCW.GetAt(i).y,'g',12) + ',' + QString::number(filletPointsCCW.GetAt(i).z,'g',12));
			QDomText radius_text = doc.createTextNode(QString::number(filletRadiiCCW.GetAt(i)));

			tabFillet.appendChild(point);
			point.appendChild(xyz);
			point.appendChild(radius);
			xyz.appendChild(xyz_text);
			radius.appendChild(radius_text);
		}

	}

	// lateral peak Info
	if ( hasAnteriorSurface )
 	{
		QDomElement lateralAnteralPeak = doc.createElement("LateralAnteriorPeak");
		outputInfo.appendChild(lateralAnteralPeak);

		{
			QDomElement point = doc.createElement("Point");
			QDomElement xyz = doc.createElement("XYZ");
			QDomText xyz_text = doc.createTextNode(QString::number(lateralPeakPoint.x,'g',12) + ',' + QString::number(lateralPeakPoint.y,'g',12) + ',' + QString::number(lateralPeakPoint.z,'g',12));

			lateralAnteralPeak.appendChild(point);
			point.appendChild(xyz);
			xyz.appendChild(xyz_text);
		}

	}

 	// outline profile jigs notch point
	if ( hasOutlineProfileJigs )
 	{
		QDomElement jigsNotchPoint = doc.createElement("OutlineProfileJigsNotchPoint");
		outputInfo.appendChild(jigsNotchPoint);
		{
			QDomElement point = doc.createElement("Point");
			QDomElement xyz = doc.createElement("XYZ");
			QDomText xyz_text = doc.createTextNode(QString::number(outlineProfileJigsNotchPoint.x,'g',12) + ',' + QString::number(outlineProfileJigsNotchPoint.y,'g',12) + ',' + QString::number(outlineProfileJigsNotchPoint.z,'g',12));

			jigsNotchPoint.appendChild(point);
			point.appendChild(xyz);
			xyz.appendChild(xyz_text);
		}

		QDomElement posWindowCutAngle = doc.createElement("OutlineProfileJigsPositiveCutWindowAngle");
		outputInfo.appendChild(posWindowCutAngle);
		{
			QDomElement angle = doc.createElement("Degrees");
			QDomText angle_text = doc.createTextNode(QString::number(outlineProfileJigsPosWindowCutAngle,'g',12));

			posWindowCutAngle.appendChild(angle);
			angle.appendChild(angle_text);
		}

		QDomElement negWindowCutAngle = doc.createElement("OutlineProfileJigsNegativeCutWindowAngle");
		outputInfo.appendChild(negWindowCutAngle);
		{
			QDomElement angle = doc.createElement("Degrees");
			QDomText angle_text = doc.createTextNode(QString::number(outlineProfileJigsNegWindowCutAngle,'g',12));

			negWindowCutAngle.appendChild(angle);
			angle.appendChild(angle_text);
		}

		QDomElement posF2WindowCutAngle = doc.createElement("OutlineProfileJigsF2PositiveCutWindowAngle");
		outputInfo.appendChild(posF2WindowCutAngle);
		{
			QDomElement angle = doc.createElement("Degrees");
			QDomText angle_text = doc.createTextNode(QString::number(outlineProfileJigsF2PosWindowCutAngle,'g',12));

			posF2WindowCutAngle.appendChild(angle);
			angle.appendChild(angle_text);
		}

		QDomElement negF2WindowCutAngle = doc.createElement("OutlineProfileJigsF2NegativeCutWindowAngle");
		outputInfo.appendChild(negF2WindowCutAngle);
		{
			QDomElement angle = doc.createElement("Degrees");
			QDomText angle_text = doc.createTextNode(QString::number(outlineProfileJigsF2NegWindowCutAngle,'g',12));

			negF2WindowCutAngle.appendChild(angle);
			angle.appendChild(angle_text);
		}
	}

	QTextStream out(&file);
    doc.save(out, IndentSize);

	file.close();

}

