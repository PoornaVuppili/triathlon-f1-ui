#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "CartilageSurface.h"
#include "AdvancedControlJigs.h"
#include "IFemurImplant.h"
#include "FemoralPart.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

///////////////////////////////////////////////////////////////////////
// There are significant changes in iTW6. There exists cartilage boundary
// to define the cartilage region near weight-bearing area. In iTW6,
// there is no such cartilage boundary. The whole femur surface will
// be offset 3mm, plus the extra offset near the trochlear groove. 
///////////////////////////////////////////////////////////////////////

CCartilageSurface::CCartilageSurface( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = cPink;

	m_bCSModified = false;
	m_bCSDataModified = false;

	m_trochlearGrooveXYZCurve = NULL;
	m_trochlearGrooveUVCurve = NULL;
	m_glTrochlearCurveList = 0;
	m_smoothMainSurface = NULL;
}

CCartilageSurface::~CCartilageSurface()
{
	if ( m_smoothMainSurface )
		IwObjDelete(m_smoothMainSurface);
}

void CCartilageSurface::Display()
{

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	// Display cartilage edge curve
	glDisable( GL_LIGHTING );

	if( m_bCSModified && m_glTrochlearCurveList > 0 )
		DeleteDisplayList( m_glTrochlearCurveList );

	if( m_glTrochlearCurveList == 0 )
	{
		m_glTrochlearCurveList = NewDisplayList();
		glNewList( m_glTrochlearCurveList, GL_COMPILE_AND_EXECUTE );
		DisplayTrochlearGrooveCurve();
		glEndList();
	}
	else
	{
		glCallList( m_glTrochlearCurveList );
	}

	glEnable( GL_LIGHTING );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	m_bCSModified = false;

	CPart::Display();

}

bool CCartilageSurface::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool				bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bCSDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_cs.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	// Retired m_edgePoints in iTW v6
	unsigned nSize = 0;
	File.write( (char*) &nSize, sizeof(unsigned) );
	//for (unsigned i=0; i<nSize; i++)
	//{
	//	IwPoint3d edgePoint = m_edgePoints.GetAt(i);
	//	File.write( (char*) &edgePoint, sizeof( IwPoint3d ) );
	//}

	nSize = m_trochlearGroovePoints.GetSize();
	File.write( (char*) &nSize, sizeof(unsigned) );
	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d tPoint = m_trochlearGroovePoints.GetAt(i);
		File.write( (char*) &tPoint, sizeof( IwPoint3d ) );
	}

	// Variables for iTW 6.0.28
	// Write m_trochlearGrooveRefPoints
	nSize = m_trochlearGrooveRefPoints.GetSize();
	File.write( (char*) &nSize, sizeof(unsigned) );
	for (unsigned i=0; i<nSize; i++)
	{
		IwPoint3d tPoint = m_trochlearGrooveRefPoints.GetAt(i);
		File.write( (char*) &tPoint, sizeof( IwPoint3d ) );
	}

	File.close();

	m_bCSDataModified = false;

	return true;
}


bool CCartilageSurface::Load( const QString& sDir )
{
	CPart::Load(sDir);

	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_cs.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	unsigned nSize;
	IwTArray<IwPoint3d>		edgePoints, tPoints;

	// Variables for Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		// edgePoints were retired in iTW v6. We load here, but never set them to m_edgePoints
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d edgePoint;
			File.read( (char*) &edgePoint, sizeof(IwPoint3d) );
			edgePoints.Add(edgePoint);
		}

		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d tPoint;
			File.read( (char*) &tPoint, sizeof(IwPoint3d) );
			tPoints.Add(tPoint);
		}
	}
	// Variables for Jigs version 6.0.28
	if (m_pDoc->IsOpeningFileRightVersion(6,0,28))
	{
		// Read m_trochlearGrooveRefPoints
		File.read( (char*) &nSize, sizeof( unsigned ) );
		for (unsigned i=0; i<nSize; i++)
		{
			IwPoint3d tPoint;
			File.read( (char*) &tPoint, sizeof(IwPoint3d) );
			m_trochlearGrooveRefPoints.Add(tPoint);
		}
	}
	File.close();


	// Actions required by Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		// Do NOT set edge points in iTW6. See comments in header.
		// this->SetEdgePoints(edgePoints);
		this->SetTrochlearGroovePoints(tPoints);
	}

	m_bCSDataModified = false;

	return true;
}

void CCartilageSurface::SetCSModifiedFlag( bool bModified )
{
	m_bCSModified = bModified;
	if (m_bCSModified)
	{
		m_bCSDataModified = true;
		m_pDoc->SetModified(m_bCSModified);
	}
}

void CCartilageSurface::SetTrochlearGroovePoints(IwTArray<IwPoint3d>& troPoints)
{
	m_trochlearGroovePoints.RemoveAll();

	m_trochlearGroovePoints.Append(troPoints);

	CreateTrochlearGrooveCurve();

	SetCSModifiedFlag( true );
}

void CCartilageSurface::GetTrochlearGroovePoints(IwTArray<IwPoint3d>& troPoints)
{
	troPoints.RemoveAll();

	troPoints.Append(m_trochlearGroovePoints);
}

void CCartilageSurface::SetTrochlearGrooveRefPoints(IwTArray<IwPoint3d>& troRefPoints)
{
	m_trochlearGrooveRefPoints.RemoveAll();

	m_trochlearGrooveRefPoints.Append(troRefPoints);

	SetCSModifiedFlag( true );
}

void CCartilageSurface::GetTrochlearGrooveRefPoints(IwTArray<IwPoint3d>& troRefPoints)
{
	troRefPoints.RemoveAll();

	troRefPoints.Append(m_trochlearGrooveRefPoints);
}

////////////////////////////////////////////////////////////////
// This function create the trochlear groove XYZ curve & UV curve
void CCartilageSurface::CreateTrochlearGrooveCurve()
{
	if ( m_trochlearGrooveXYZCurve )
		IwObjDelete(m_trochlearGrooveXYZCurve);
	if ( m_trochlearGrooveUVCurve )
		IwObjDelete(m_trochlearGrooveUVCurve);

	m_trochlearGrooveXYZCurve = NULL;
	m_trochlearGrooveUVCurve = NULL;

	// Get femoral surface 
	IwBSplineSurface *mainSurf, *leftSurf, *rightSurf;
	bool bCopy = false;
	IFemurImplant::FemoralPart_GetSurfaces(bCopy, mainSurf, leftSurf, rightSurf);

	// interpolate edgePoints
	IwBSplineCurve* uvCurve;
	IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), m_trochlearGroovePoints, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, uvCurve);
	if (uvCurve == NULL) return;
	m_trochlearGrooveUVCurve = uvCurve;

	IwCrvOnSurf* crvOnSurf = new (m_pDoc->GetIwContext()) IwCrvOnSurf(*uvCurve, *mainSurf);
	// the crvOnSurf is a curve on surface. It refers to the surface when evaluate. When the surface is updated
	// (the original surface is deleted), the curve on surface has a problem to evaluate itself.
	// To be simple, just create a 3D curve here. 
	if ( crvOnSurf )
	{
		IwTArray<double> breakParams;
		breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
		breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
		double aTol;
		crvOnSurf->ApproximateCurve(m_pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.001, aTol, m_trochlearGrooveXYZCurve, FALSE, FALSE);
	}

	// Delete objects
	if ( uvCurve )
		IwObjDelete(uvCurve);
	if ( crvOnSurf )
		IwObjDelete(crvOnSurf);

	return;
}


void CCartilageSurface::DisplayTrochlearGrooveCurve()
{

	if (m_trochlearGrooveXYZCurve == NULL) return;

	IwPoint3d	pt;

	IwPoint3d sPData[64];
	
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(3.0);

	glColor4ub( 48, 96, 0, 64);

	IwExtent1d sIvl = m_trochlearGrooveXYZCurve->GetNaturalInterval();

	double	dChordHeightTol = 0.3;
	double	dAngleTolInDegrees = 5.0;

	IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

	sCrvTess.TessellateCurve( *m_trochlearGrooveXYZCurve, sIvl, NULL, &sPnts );

	int nPoints = sPnts.GetSize();

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
	{
		pt = sPnts.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();

	glLineWidth(1.0);

}

void CCartilageSurface::DisplayTrochlearGroovePoints()
{
	IwPoint3d pnt;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	IwTArray<IwPoint3d> xyzPoints;
	ConvertUVToXYZ(m_trochlearGroovePoints, xyzPoints, NULL);

	glColor3ub( 0, 48, 0 );

	for (unsigned i=0; i<xyzPoints.GetSize(); i++)
	{

		pnt = xyzPoints.GetAt(i);

		glBegin( GL_POINTS );

			glVertex3d( pnt.x, pnt.y, pnt.z );

		glEnd();
	}

	glPointSize(1);

}

/////////////////////////////////////////////////////////////////
void CCartilageSurface::ConvertUVToXYZ
(
	IwTArray<IwPoint3d>& uvPoints,		// I: the uv defined in femur surface parametric domain
	IwTArray<IwPoint3d>& xyzPoints,		// O:
	IwTArray<IwPoint3d> *xyzNormals		// O:
)
{
	xyzPoints.RemoveAll();

	// Get femoral surface 
	IwBSplineSurface *mainSurf=NULL, *leftSurf=NULL, *rightSurf=NULL;
	bool bCopy = false;
	IFemurImplant::FemoralPart_GetSurfaces(bCopy, mainSurf, leftSurf, rightSurf);

	IwPoint3d uvPnt, pnt, normal;
	IwPoint2d uv;

	for (unsigned i=0; i<uvPoints.GetSize(); i++)
	{
		uvPnt = uvPoints.GetAt(i);
		uv = IwPoint2d(uvPnt.x, uvPnt.y);
		mainSurf->EvaluatePoint(uv, pnt);
		xyzPoints.Add(pnt);
		if ( xyzNormals )
		{
			mainSurf->EvaluateNormal(uv, FALSE, FALSE, normal);
			xyzNormals->Add(normal);
		}
	}

	return;
}

//////////////////////////////////////////////////////////////////////////////
// The control points are limited to the main area only (no side surfaces).
// The "Smooth" function will smooth a selected region by recalculating
// the control points. This function will update the surface by the control
// points.
void CCartilageSurface::UpdateSurfacebyCtrlPoints
(
	CTotalDoc* pDoc,					// I:
	IwBSplineSurface*& surface,			// I/O:
	IwTArray<ULONG>& ctrlPntIndexes,	// I:
	IwTArray<IwPoint3d>& ctrlPnts		// I:
)
{
	IwBSplineSurface* Surf = surface;

	// Get control points of main surface
	ULONG ucCount, vcCount;
	double* cPointers;
	Surf->GetControlPointsPointer(ucCount, vcCount, cPointers);

#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	Surf->Notify(IW_NO_PRE_EDIT, Surf, nullptr, nullptr);
#else
	Surf->Notify(IW_NO_PRE_EDIT);
#endif

	ULONG cpIndex;
	IwPoint3d cPoint;
	for (unsigned i=0; i<ctrlPntIndexes.GetSize(); i++)
	{
		cpIndex = ctrlPntIndexes.GetAt(i);
		cPoint = ctrlPnts.GetAt(i);
		cPointers[cpIndex] = cPoint.x;
		cPointers[cpIndex+1] = cPoint.y;
		cPointers[cpIndex+2] = cPoint.z;
	}

#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	Surf->Notify(IW_NO_POST_EDIT, Surf, nullptr, nullptr);
#else
	Surf->Notify(IW_NO_POST_EDIT);
#endif
	// Smooth the Surf at crisps
	CFemoralPart::SmoothMainSurfaceAtCrisps(Surf);

}

void CCartilageSurface::SetIwBrep( IwBrep* pBrep )
{
	CPart::SetIwBrep(pBrep);

	// Delete m_smoothMainSurface and let GetSinglePatchSurface() recalculate it.
	if ( m_smoothMainSurface )
	{
		IwObjDelete(m_smoothMainSurface);
	}
	m_smoothMainSurface = NULL;
}

/////////////////////////////////////////////////////////////////////
// This function will first simplify (smooth=true) the main surface to 
// remove the crisp edges since the crisp edges are challenged to
// certain solvers (distance, normal, offset, ...). Then returns the 
// copied main surface.
IwBSplineSurface* CCartilageSurface::GetSinglePatchSurface(bool smooth)
{
	IwBrep* cartiBrep = this->GetIwBrep();
	if ( cartiBrep == NULL )
		return NULL;

	IwTArray<IwFace*> faces;
	cartiBrep->GetFaces(faces);
	if ( faces.GetSize() == 0 )
		return NULL;

	IwFace* mainFace = faces.GetAt(0);
	IwBSplineSurface* cartiSurf = (IwBSplineSurface*)mainFace->GetSurface();
	if ( cartiSurf == NULL )
		return NULL;

	if ( smooth )
	{
		if ( m_smoothMainSurface )
		{
			cartiSurf = m_smoothMainSurface;
		}
		else
		{
			IwSurface* copySurf=NULL;
			cartiSurf->Copy(m_pDoc->GetIwContext(), copySurf);
			IwBSplineSurface* bSurf = (IwBSplineSurface*)copySurf;
			bSurf->RemoveKnots(0.01, TRUE, TRUE);// to remove the crisp edges, if exist.
			m_smoothMainSurface = bSurf;
			cartiSurf = m_smoothMainSurface;
		}
	}

	return cartiSurf;
}

void CCartilageSurface::GetSideSurfaces
(
	IwBSplineSurface*& leftSurface,	// O:
	IwBSplineSurface*& rightSurface	// O:
)
{
	IwBrep* cartiBrep = this->GetIwBrep();
	if ( cartiBrep == NULL )
		return;

	IwTArray<IwFace*> faces;
	cartiBrep->GetFaces(faces);
	if ( faces.GetSize() != 3 )
		return;

	IwFace* leftFace = faces.GetAt(1);
	leftSurface = (IwBSplineSurface*)leftFace->GetSurface();
	IwFace* rightFace = faces.GetAt(2);
	rightSurface = (IwBSplineSurface*)rightFace->GetSurface();

}

bool CCartilageSurface::IsMemberDataChanged(IwTArray<IwPoint3d>& troPoints)
{
	if ( troPoints.GetSize() != m_trochlearGroovePoints.GetSize() )
		return true; // changed

	for (unsigned i=0; i<m_trochlearGroovePoints.GetSize(); i++)
	{
		if ( m_trochlearGroovePoints.GetAt(i).DistanceBetween(troPoints.GetAt(i)) > 0.001 )
			return true;
	}

	return false;
}