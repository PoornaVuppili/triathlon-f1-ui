#include "KInputValueSlider2.h"
#include "ui_KInputValueSlider2.h"

KInputValueSlider2::KInputValueSlider2(QWidget *parent)
    : QDialog(parent), ui(new Ui_InputValueSlider2Class())
{
    ui->setupUi(this);

    connect(ui->horizontalSlider1, SIGNAL(valueChanged(int)), this, SLOT(onSlider1Change(int)));
    connect(ui->horizontalSlider2, SIGNAL(valueChanged(int)), this, SLOT(onSlider2Change(int)));
    connect(ui->spinBox1, SIGNAL(valueChanged(int)), this, SLOT(onSpinBox1Change(int)));
    connect(ui->spinBox2, SIGNAL(valueChanged(int)), this, SLOT(onSpinBox2Change(int)));

    // hide the check box by default
    ui->checkBox->hide();
}

KInputValueSlider2::~KInputValueSlider2()
{
    ui->horizontalSlider1->disconnect();
    ui->horizontalSlider2->disconnect();
    ui->spinBox1->disconnect();
    ui->spinBox2->disconnect();
    delete ui;
}

void KInputValueSlider2::ShowCheckBox(bool b)
{
    ui->checkBox->setVisible(b);
}

void KInputValueSlider2::SetCheckBoxText(QString& str)
{
    ui->checkBox->setText(str);
}

void KInputValueSlider2::SetChecked(bool b)
{
    ui->checkBox->setChecked(b);
}

bool KInputValueSlider2::IsChecked()
{
    return ui->checkBox->isChecked();
}

void KInputValueSlider2::SetLabel1Text(QString& str)
{
    ui->label1->setText(str);
}

void KInputValueSlider2::SetLabel2Text(QString& str)
{
    ui->label2->setText(str);
}

void KInputValueSlider2::SetRange1(int min, int max)
{
    ui->horizontalSlider1->setRange(min, max);
    ui->spinBox1->setRange(min, max);
}

void KInputValueSlider2::SetRange2(int min, int max)
{
    ui->horizontalSlider2->setRange(min, max);
    ui->spinBox2->setRange(min, max);
}

void KInputValueSlider2::onSlider1Change(int v)
{
    if( v != ui->spinBox1->value() )
        ui->spinBox1->setValue(v);
    emit value1Changed(v);
}

void KInputValueSlider2::onSlider2Change(int v)
{
    if( v != ui->spinBox2->value() )
        ui->spinBox2->setValue(v);
    emit value2Changed(v);
}

void KInputValueSlider2::onSpinBox1Change(int v)
{
    if( v != ui->horizontalSlider1->value() )
        ui->horizontalSlider1->setValue(v);
}

void KInputValueSlider2::onSpinBox2Change(int v)
{
    if( v != ui->horizontalSlider2->value() )
        ui->horizontalSlider2->setValue(v);
}

void KInputValueSlider2::InitValue(int v1, int v2)
{
    initValue1 = v1;
    initValue2 = v2;
    SetValue1(v1);
    SetValue2(v2);
}

void KInputValueSlider2::SetValue1(int v)
{
    if( v != ui->horizontalSlider1->value() )
        ui->horizontalSlider1->setValue(v);
}

void KInputValueSlider2::SetValue2(int v)
{
    if( v != ui->horizontalSlider2->value() )
        ui->horizontalSlider2->setValue(v);
}

int KInputValueSlider2::GetValue1()
{
    return ui->spinBox1->value();
}

int KInputValueSlider2::GetValue2()
{
    return ui->spinBox2->value();
}

void KInputValueSlider2::on_pushButtonOK_clicked()
{
    accept();
}

void KInputValueSlider2::on_pushButtonCancel_clicked()
{
    if( initValue1 != GetValue1() )
        SetValue1(initValue1 );
    if( initValue2 != GetValue2() )
        SetValue2(initValue2 );

    reject();
}

