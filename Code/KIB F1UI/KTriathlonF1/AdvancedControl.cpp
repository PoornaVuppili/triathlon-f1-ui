#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#pragma warning( default : 4996 4805 )
#include "AdvancedControl.h"
#include "FemoralAxes.h"
#include "FemoralCuts.h"
#include "TotalDoc.h"
#include "TotalView.h"

CAdvancedControl::CAdvancedControl( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CEntity( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_ADVANCED_CONTROL );

	m_eDisplayMode = DISP_MODE_NONE;

	m_bModified = false;
	m_bDataModified = false;

	// m_autoJOCSteps
	m_autoJOCSteps = false; // JOC = Jcurve + Outline profile + femoral Cuts
	m_autoMajorSteps = true; // from femoal axes to solid implant
	m_autoAllSteps = false; 
	m_autoAdditionalIterations = 0;

	// Femoral Axes
	m_femoralAxesEnableRefine = false;
	m_femoralImplantCoronalRadius = 40.0;
	m_trochlearGrooveRadius = 30.0;
	m_femoralAxesDisplayMechAxes = true;

	// Femoral Cuts
	m_femoralCutsEnableReset = false;
	m_femoralCutsOnlyMoveNormal = true;
	m_viewProfileOffsetDistance = 5.5;

	CFemoralAxes* femAxes = GetTotalDoc()->GetFemoralAxes();
	if (femAxes)
	{
		// The m_viewProfileOffsetDistance, which is also the default bone cut depth,
		// should be proportional to the femoral size. "5.5" is for femoral size 70.
		double defaultValue = 5.5*femAxes->GetEpiCondyleSize()/70.0;
		m_viewProfileOffsetDistance = ((int)(10*defaultValue))/10.0;// only keep the first decimal.
	}
	m_cutLineDisplayLength = 15.0;
	m_achieveDesiredPostCuts = false;
	m_cutsExtraAllowance = 0.0;

	// Outline Profile
	m_outlineProfileEnableReset = false;
	m_outlineProfilePosteriorCutFilterRadiusRatio = 0.35;
	m_outlineProfileSketchSurfaceAntExtension = 0.0;
	m_outlineProfileSketchSurfacePostExtension = 0.0;
	m_outlineProfileAntAirBallDistance = 2.0;

	//// Output Results
	m_outputNormalized = false;
	m_importFromNormalized = false;
	m_outputWithTransformation = false;
	m_importRotationXYZ = IwVector3d(0,0,0); 
	m_importTranslationXYZ = IwVector3d(0,0,0);
	m_importTransformationX = IwVector3d(0,0,0); 
	m_importTransformationY = IwVector3d(0,0,0); 
	m_importTransformationT = IwVector3d(0,0,0);
}

CAdvancedControl::~CAdvancedControl()
{

}

bool CAdvancedControl::Save( const QString& sDir, bool incrementalSave )
{
	QString				sFileName;
	bool				bOK = true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_ac.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	// write m_autoJOCSteps, JOC = Jcurve + Outline profile + femoral Cuts
	int autoJOCSteps = m_autoJOCSteps ? 1 : 0;
	File.write( (char*) &autoJOCSteps, sizeof( int ) );
	// do not need to write m_autoMajorSteps
	// do not need to write m_autoAllSteps

	// Femoral Axes
	// Write m_femoralImplantCoronalRadius
	File.write( (char*) &m_femoralImplantCoronalRadius, sizeof( double ) );
	// Write m_trochlearGrooveRadius
	File.write( (char*) &m_trochlearGrooveRadius, sizeof( double ) );
	// Not Write m_femoralAxesDisplayMechAxes
	// File.write( (char*) &m_femoralAxesDisplayMechAxes, sizeof( bool ) );

	// Femoral Cuts variables
	// Write m_viewProfileOffsetDistance
	File.write( (char*) &m_viewProfileOffsetDistance, sizeof( double ) );
	// Write m_cutLineDisplayLength
	File.write( (char*) &m_cutLineDisplayLength, sizeof( double ) );
	// Write m_distalCutAngle (obsoleted at v4.0.0)
	double distalCutAngle=0.0;
	File.write( (char*) &distalCutAngle, sizeof( double ) );
	// Write m_postTipThickness
	double postTipThickness = 2.0; // Actually it is retired in iTW v6.0.10
	File.write( (char*) &postTipThickness, sizeof( double ) );
	
	// Output Results
	// We do not need to save output settings

	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v1.11.b
	/////////////////////////////////////////////////////////////////////////////////////
	// Write m_outlineProfileSketchSurfaceAntExtension
	File.write( (char*) &m_outlineProfileSketchSurfaceAntExtension, sizeof( double ) );
	// Write m_outlineProfileSketchSurfacePostExtension
	File.write( (char*) &m_outlineProfileSketchSurfacePostExtension, sizeof( double ) );

	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v4.0.0
	/////////////////////////////////////////////////////////////////////////////////////
	
	// No need to write m_achieveDesiredPostCuts
	// Write m_outlineProfilePosteriorCutFilterRadiusRatio
	File.write( (char*) &m_outlineProfilePosteriorCutFilterRadiusRatio, sizeof( double ) );

	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v6.0.7
	/////////////////////////////////////////////////////////////////////////////////////
	// Write m_cutsExtraAllowance
	File.write( (char*) &m_cutsExtraAllowance, sizeof( double ) );

	File.close();

	m_bDataModified = false;

	return true;
}


bool CAdvancedControl::Load( const QString& sDir )
{
	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_ac.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	// Variables for TriathlonF1 version 0.0
	if ( m_pDoc->IsOpeningFileRightVersion(0,0,0) )
	{
		// auto JOC steps
		// m_autoJOCSteps, JOC = Jcurve + Outline profile + femoral Cuts
		int autoJOCSteps;
		File.read( (char*) &autoJOCSteps, sizeof(int) );
		m_autoJOCSteps = autoJOCSteps==1 ? true : false;
		// do not need to read m_autoMajorSteps
		// do not need to read m_autoAllSteps

		// Femoral Axes
		// Read m_femoralImplantCoronalRadius
		File.read( (char*) &m_femoralImplantCoronalRadius, sizeof( double ) );
		// Read m_trochlearGrooveRadius
		File.read( (char*) &m_trochlearGrooveRadius, sizeof( double ) );
		
		// Femoral Cuts
		// Read m_viewProfileOffsetDistance
		File.read( (char*) &m_viewProfileOffsetDistance, sizeof( double ) );
		// Read m_cutLineDisplayLength
		File.read( (char*) &m_cutLineDisplayLength, sizeof( double ) );

		double distalCutAngle;
		File.read( (char*) &distalCutAngle, sizeof( double ) );
		// Read m_postTipThickness
		double postTipThickness;;
		File.read( (char*) &postTipThickness, sizeof( double ) ); // Actually it is retired in iTW v6.0.10
	}

		// Variables for TriathlonF1 version 1.11 
	if ( m_pDoc->IsOpeningFileRightVersion(1,11,0) )
	{
		// Read m_outlineProfileSketchSurfaceAntExtension
		File.read( (char*) &m_outlineProfileSketchSurfaceAntExtension, sizeof( double ) );
		// Read m_outlineProfileSketchSurfacePostExtension
		File.read( (char*) &m_outlineProfileSketchSurfacePostExtension, sizeof( double ) );
	}

	// Variables for TriathlonF1 version 4.0.0 
	if ( m_pDoc->IsOpeningFileRightVersion(4,0,0) )
	{
		// No need to read m_achieveDesiredPostCuts
		// Read m_outlineProfilePosteriorCutFilterRadiusRatio
		File.read( (char*) &m_outlineProfilePosteriorCutFilterRadiusRatio, sizeof( double ) );
	}

	// Variables for TriathlonF1 version 6.0.7 
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,7) )
	{
		// Read m_cutsExtraAllowance;
		File.read( (char*) &m_cutsExtraAllowance, sizeof( double ) );
	}


	// Variables for TriathlonF1 version 6.0.14 
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,14) )
	{
	
	}

	m_bDataModified = false;

	File.close();


	return true;
}


void CAdvancedControl::SetModifiedFlag( bool bModified )
{
	m_bModified = bModified;
	if (m_bModified)
	{
		m_bDataModified = true;
		m_pDoc->SetModified(m_bModified);
	}
}
