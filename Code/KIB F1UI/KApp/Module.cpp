#include "Module.h"
#include <qmenu.h>
#include <qtoolbar.h>
#include <assert.h>

Module::Module(QObject *parent)
    : QObject(parent)
{
}

Module::~Module()
{
    ClearAll();
}

// Clean up
void Module::ClearAll()
{
    for(int i=0; i<menus.size(); i++)
        delete menus[i];
    menus.clear();

    for(int i=0; i<toolbars.size(); i++)
        delete toolbars[i];
    toolbars.clear();
}

QMenu* Module::GetMenu(int index)
{
    assert( index>=0 && index < menus.size() );

    return menus[index];
}

QToolBar* Module::GetToolBar(int index)
{
    assert( index>=0 && index < toolbars.size() );

    return toolbars[index];
}

// Enable/disable actions on the menus or toolbars
void Module::EnableActions(bool enable)
{
    for(int i=0; i<menus.size(); i++)
        menus[i]->setEnabled(enable);

    for(int i=0; i<toolbars.size(); i++)
        toolbars[i]->setEnabled(enable);
}

void Module::ContinueToNextStep()
{
   // am I auto-capable?
   if (IsCapableOfAutoStep() /* && IsApplicationInAutoStepMode() */)
   {
      if (QAction *pNextAction = nextAction())
      {
         // how do we know whether it's enabled?
         // -- supposedly the 'nextAction' should determine that
         // and if it's not enabled, return nullptr...
         pNextAction->trigger();
      }
   }
}

void Module::ContinueToPrevStep()
{
   // am I auto-capable?
   if (IsCapableOfAutoStep() /* && IsApplicationInAutoStepMode() */)
   {
      if (QAction *pPrevAction = prevAction())
      {
         // how do we know whether it's enabled?
         // -- supposedly the 'prevAction' should determine that
         // and if it's not enabled, return nullptr...
         pPrevAction->trigger();
      }
   }
}

void Module::onManagerDismissedByAccept()
{
   ContinueToNextStep();
}

void Module::onManagerDismissedByReviewNext()
{
   ContinueToNextStep();
}

void Module::onManagerDismissedByReviewPrev()
{
   ContinueToPrevStep();
}

void Module::onManagerDismissedByRework()
{
   DoRework();
}
