#include "ImplantTotalPS.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\EntityWidget.h"
#include "..\KApp\ViewWidget.h"
#include "..\KApp\OptionDialog.h"
#include "..\KApp\OptionGeneral.h"
#include "..\KTotalPS\TotalMainWindowPS.h"
#include "..\KUtility\KUtility.h"
#include "..\KUtility\KXmlHelper.h"
#include "../KUtility/KApplicationMode.h"
#include "SelectImplant.h"
#include "SelectImplantSimple.h"
#include "../KApp/SwitchBoard.h"

#include <QMessageBox>
#include <QTextStream>

using namespace std;

ImplantTotalPS::ImplantTotalPS(QObject *parent)
    : Implant(parent)

    , totalPS(nullptr)
    , femBasis(nullptr)
    , femJigs(nullptr)
    , m_designStage(NOTYET)

{

    templateFolder = QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("Template");
    if( !QDir(templateFolder).exists() )
    {
        templateFolder = QDir::current().absoluteFilePath("Template");
        if( !QDir(templateFolder).exists() )
            templateFolder = "";
    }

    SwitchBoard::addSignalReceiver(SIGNAL(DesignStageCommenced(Module*)), this, SLOT(OnDesignStageCommenced(Module*)));
    SwitchBoard::addSignalReceiver(SIGNAL(DesignStageCommenced(Module*,int)), this, SLOT(OnDesignStageCommenced(Module*,int)));
    SwitchBoard::addSignalReceiver(SIGNAL(DesignStageCompleted(Module*)), this, SLOT(OnDesignStageCompleted(Module*)));
    SwitchBoard::addSignalReceiver(SIGNAL(DesignStageCompleted(Module*,int)), this, SLOT(OnDesignStageCompleted(Module*,int)));
}

// Initialize & release modules
bool ImplantTotalPS::InitializeModules(QMenu* before)
{
	if( totalPS==NULL )
    {
        totalPS = new KTotalMainWindowPS();
		totalPS->SetAppVersion(mainWindow->GetAppVersion());
        if( !totalPS->Initialize() || !totalPS->InitializeJigs())
		{
            return false;
		}
        AddModuleMenuToolbar( totalPS, before );
        // these should be in order, the module will report them that way
        m_designStageByModule.insert(std::make_pair(totalPS, FEMUR_BASIS));
        //m_designStageByModule.insert(std::make_pair(totalPS, PS_FEATURES));
        m_designStageByModule.insert(std::make_pair(totalPS, FEM_JIGS));
    }

    femBasis = totalPS;
    femJigs = totalPS;

    EnableActions(false);
    return true;
}

void ImplantTotalPS::ReleaseModules()
{
    m_designStageByModule.clear(); // so no reference to pointers exists in it...

    DeletePtr( totalPS );

    // don't delete femBasis or femJigs yet!
}

// Get the version info of all modules
QStringList ImplantTotalPS::GetModuleVersionInfo()
{
    QStringList sl;

    return sl;
}

// Load the implant
bool ImplantTotalPS::LoadImplant(SelectImplant* si)
{
    KWaitCursor wait;

	// Initialize data
	nameElement.clear();
	totalPSElement.clear();
	viewElement.clear();
    treeElement.clear();

    implantName = si->GetImplantName();
    implantType = si->GetImplantType();
    kneeType = si->GetKneeType();
    compartmentType = si->GetCompartmentType();
    country = si->GetCountry();
    zone = si->GetZone();

    // update the folders
    implantDir = si->GetBasePath() + QDir::separator() + si->GetImplantFolder();
    imgDir = si->GetImgDir();
    segDir = si->GetSegmentDir();
    surfDir = si->GetSurfaceDir();
    stlDir = si->GetSTLDir();
    sldworksDir = si->GetSolidWorksDir();

    Viewport* vp = viewWidget->CreateViewport();

    QString femurFile = si->GetFemuralImplantFile();
	
    if( !femurFile.isEmpty() )
    {
		if( totalPSElement.childNodes().count() != 0 )
		{
			totalPSElement.removeChild(totalPSElement.firstChild());
		}
        if( totalPSElement.childNodes().count() == 0 )
        {
            QDomElement elem = docStatus.createElement("File");
            totalPSElement.appendChild(elem);
			QStringList list = femurFile.split(QDir::separator(), QString::SkipEmptyParts);
			QString file = list.last();
			list.pop_back();
			file = list.last() + QDir::separator() + file;
			elem.appendChild( docStatus.createTextNode(file) );
		}

		totalPS->OnFileOpenWithOptions(  femurFile, "FemBasis" );

	}

    if( !femurFile.isEmpty() )
    {
        totalPS->OnFileOpenWithOptions(  femurFile, "FemJigs" );
    }

	// set the view status
    viewWidget->SetStatus(viewElement);
	viewWidget->ZoomFit();

    entityWidget->SetStatus(treeElement);

    // Set the window title
    QString title = QString(mainWindow->GetAppName())  + " " + mainWindow->GetAppVersion()  + implantType + ", " + kneeType;
    if( !implantType.startsWith("TriathlonF1",Qt::CaseInsensitive) )
        title += ", " + implantName + ", " + compartmentType;
    if( !country.isEmpty() )
        title += ", " + country + "(" + zone + ")";
    
    QString patientName = "";
    title += QString(" (%1)").arg(patientName);

    mainWindow->SetSubTitle(title);
    
    EnableActions(true);

    if (mainWindow->ReviewerFeedbackAvailable())
        mainWindow->ShowReviewerFeedback();

    return true;
}

bool ImplantTotalPS::LoadImplant(SelectImplantSimple* sis)
{
	KWaitCursor wait;

	// Initialize data
	nameElement.clear();
	totalPSElement.clear();
	viewElement.clear();

    // update the implant information
    implantName = sis->GetImplantName();
    implantType = sis->GetImplantType();
    kneeType = sis->GetKneeType();
    compartmentType = sis->GetCompartmentType();
    country = sis->GetCountry();
    zone = sis->GetZone();

    // update the folders
    implantDir = sis->GetBasePath() + QDir::separator() + sis->GetImplantFolder();
    imgDir = sis->GetImgDir();
    segDir = sis->GetSegmentDir();
    surfDir = sis->GetSurfaceDir();
    stlDir = sis->GetSTLDir();
    sldworksDir = sis->GetSolidWorksDir();

	kStatus.ProgressInit(8);
	
	kStatus.ProgressSet(1);
	kStatus.ShowMessage("Load segmentation...");
	
	kStatus.ProgressSet(2);
	kStatus.ShowMessage("Load femur basis...");

    // load the total fem basis
    QString femurFile;
	QDir femurImplantDir(sldworksDir /*+ QDir::separator() + "Femur"*/);
	if( femurImplantDir.exists() )
	{
        QFileInfoList infoList = femurImplantDir.entryInfoList(QStringList() << "*.tw", QDir::Dirs, QDir::Name);
		if( infoList.size()>0 )
			femurFile = infoList[0].absoluteFilePath();
	}
 
	kStatus.ProgressSet(4);
	kStatus.ShowMessage("Load motion ...");

	kStatus.ProgressSet(6);
	kStatus.ShowMessage("Load femur ps ...");

    // load the total fem PS features
    // - temp solution: add the file as a node
    if( !femurFile.isEmpty() )
    {
    }

	kStatus.ProgressSet(7);
	kStatus.ShowMessage("Load femur jigs ...");

    // load the fem Jigs
    // - temp solution: add the file as a node
    if( !femurFile.isEmpty() )
    {
    }

	kStatus.ProgressReset();
	kStatus.ShowMessage("");
	
	// set the view status
    viewWidget->SetStatus(viewElement);
	viewWidget->ZoomFit();

    // Set the window title
    QString title = QString(mainWindow->GetAppName())  + " " + mainWindow->GetAppVersion() + " - " + implantName + ", " + implantType + ", " + kneeType;
    if( !implantType.startsWith("TriathlonF1",Qt::CaseInsensitive) )
        title += ", " + compartmentType;
    if( !country.isEmpty() )
        title += ", " + country + "(" + zone + ")";
    
    QString patientName = "";
    title += QString(" (%1)").arg(patientName);

    mainWindow->SetSubTitle(title);
    
    EnableActions(true);
    return true;
}

// Save the implant
bool ImplantTotalPS::SaveImplant()
{
#ifndef TESTING_MODE
	if ( mainWindow->IsReviewMode()) // should not happen, but in case it does, display a message and pretend to have saved
	{
		QMessageBox::critical(mainWindow, "Not allowed to save", "Something called 'SaveImplant' when saving is not allowed.\nAlert Software Dept.");
		return true; // pretend to have saved (
	}
#endif
    if (!uHistory.empty())
    {
        if (userHistory.isNull()) // no user history in the file yet
        {
            userHistory = docStatus.createElement("UserHistory");
            userHistory.setAttribute("Version", "1.0");
        }
        else // it's there, let's remove old contents
        {
            while (userHistory.hasChildNodes())
                userHistory.removeChild(userHistory.lastChild());
        }

        uHistory.back().birth = QDateTime::currentDateTime();

        for (int i = 0, n = uHistory.size(); i < n; ++i)
        {
            QDomElement ui = docStatus.createElement("User");
            ui.setAttribute("Name", uHistory[i].name);
            ui.setAttribute("PC", uHistory[i].workstation);
            ui.setAttribute("When", uHistory[i].birth.toString());
            ui.setAttribute("Mode", QString("%1").arg(uHistory[i].exmode));
            ui.setAttribute("Version", uHistory[i].version);
            userHistory.appendChild(ui);
        }

        // replace 'Username' element with 'UserHistory'
        if (nameElement.tagName() == "Username")
            docStatus.firstChild().replaceChild(userHistory, nameElement);
        else
            docStatus.insertBefore(userHistory, docStatus.firstChild().firstChild());
    }
    else
    {
        // update the user name
        nameElement.replaceChild(docStatus.createTextNode(CurrentUserName()), nameElement.firstChild());
    }
	   
    if( totalPS->HasImplant() )
        totalPS->OnFileSave();
		
	entityWidget->GetStatus(treeElement);

    // get the view status
    viewWidget->GetStatus(viewElement);

    emit Implant::ImplantSavedSuccessfully();
    return true;
}

bool ImplantTotalPS::SaveReworkImplant()
{
    // update the user name
    nameElement.replaceChild(docStatus.createTextNode(CurrentUserName()), nameElement.firstChild());

    // get the view status
    viewWidget->GetStatus(viewElement);

    entityWidget->GetStatus(treeElement);

    // save the status file
	QString fileName = implantStatusFile;
	fileName.replace(".xml", "_rework.xml");

    QFile file( fileName );
    if (!file.open(QIODevice::WriteOnly))
    {
        return false;
    }

    //const int IndentSize = 4;
    //QTextStream out(&file);
    //docStatus.save(out, IndentSize);

	return true;
}

// Whether the implant is changed
bool ImplantTotalPS::IsImplantChanged() const
{
    if( !HasImplant() )
        return false;
	  
	if (totalPS && totalPS->IsImplantModified())
        return true; 
	  
    return false;
}

// clear the module content
void ImplantTotalPS::ClearModule()
{
     //clear the total PS data
    totalPS->FileCloseSilent();

	QString appName = mainWindow->GetAppName();
    implantStatusFile = QDir(implantDir).absoluteFilePath(implantName + "_" + appName + ".xml");
	QString reworkStatusFile = implantStatusFile;
	reworkStatusFile.replace(".xml", "_rework.xml");

	bool deleteRework = QFile::exists(reworkStatusFile);

	if(deleteRework)
	{
		QFile file(reworkStatusFile);
		file.remove();
	}

    // clear the implant information
    implantName = "";
    implantType = "";
    kneeType = "";
    compartmentType = "";
    country = "";
    zone = "";

    // clear the folders
    implantDir = "";
    imgDir = "";
    segDir = "";
    surfDir = "";
    stlDir = "";
    sldworksDir = "";
}

// Get the implant info by name
QString ImplantTotalPS::GetInfo(const QString& name) const
{
    if( name == "ImplantName" )
        return implantName;
	else if( name == "ImplantContainerName")//PSV---
        return implantContainerName;
    else if( name == "ImplantType" )
        return implantType;
    else if( name == "KneeType" )
        return kneeType;
    else if( name == "Compartment" )
        return compartmentType;
    else if( name == "Country" )
        return country;
    else if( name == "Zone" )
        return zone;
    else if( name == "ImplantDir" ) // this is where the case resides
        return implantDir;
    else if( name == "ImageDir" )
        return imgDir;
    else if( name == "SegDir" )
        return segDir;
    else if( name == "SurfDir" )
        return surfDir;
    else if( name == "STLDir" )
        return stlDir;
    else if( name == "SolidWorksDir" )
        return sldworksDir;
    else if( name == "TemplateFolder" )
        return templateFolder;
    else if (name == "DesignStage")
    {
        QString stage;
        CurrentStage(&stage);
        return stage;
    }
    else if (name.startsWith("ExpLoc"))
    {
        
        const QChar _ = QDir::separator(); // "backslash"

        QString femDir = sldworksDir +_+ "Femur";

        QString serNumSide = implantName +" "+ GetInfo("KneeType")[0]; // ######## {R|L}

        QString femBasisOut = femDir +_+ serNumSide + " Femoral Implant_Outputs";
        QString femPSOut = femBasisOut +_+ implantName + "_PS_Outputs";
        QString const& msOut = femPSOut; // motion stuio is the same as Fem PS
        QString jigsOut = femBasisOut +_+ implantName + "_Jigs_Outputs";

        if (name == "ExpLocFemur")
            return femBasisOut;
        else if (name == "ExpLocFemPS")
            return femPSOut;
        else if (name == "ExpLocMStudio")
            return msOut;
        else if (name == "ExpLocJigs")
            return jigsOut;
    }
 
    else if (name == "FeedbackLoc")
    	return implantDir + QDir::separator() + implantName + "_ReviewerFeedback.txt";

    return Implant::GetInfo(name);
}

// Get the module by name
Module* ImplantTotalPS::GetModule(const QString& name)
{
    if( name == "TotalPS" )
        return totalPS;
      
    return Implant::GetModule(name);
}

bool ImplantTotalPS::AllStepsComplete(std::string const &phase, std::string const &subphase) const
{
   return Implant::AllStepsComplete(phase, subphase);
}

bool ImplantTotalPS::OutputResults(std::string const &phase, std::string const &subphase) const // virtual -- overridden
{
    if (phase == "Implant")
	{
		return Implant::OutputResults(phase, subphase);
	}
    return false;
}

void ImplantTotalPS::HideAll()
{
    HideAllExcept(nullptr);
}

void ImplantTotalPS::HideAllExcept(Module* pMod)
{
    
    if (pMod != totalPS)
		totalPS->HideAllEntities();
  
}

// Enable/disable actions of menus and toolbars
void ImplantTotalPS::EnableActions(bool enable)
{
    totalPS->EnableActions( enable );
}

// Set options
void ImplantTotalPS::SetOption()
{
    OptionDialog dlg(mainWindow);

    // add the default general page
    OptionGeneral *pGenPage = new OptionGeneral();

    dlg.AddPage( pGenPage );

    vector<OptionPage*> pgs;

    // add the global & implant pages of each module
    QList<Module*> listMod;
    listMod << totalPS ;

    for(int i=0; i<listMod.size(); i++)
    {
        Module* m = listMod.at(i);
        
        pgs = m->GetGlobalOptionPages();
        for(auto it=pgs.begin(); it != pgs.end(); it++)
            dlg.AddPage( *it );

        if( HasImplant() )
        {
            pgs = m->GetImplantOptionPages();
            for(auto it=pgs.begin(); it != pgs.end(); it++)
                dlg.AddPage( *it, false );
        }
    }
    
    dlg.exec();
}

void ImplantTotalPS::StartProfileReview()
{
	//if ( totalPS == NULL || totalPS->IsAllProfileDesignComplete() == false )
	//{
	//	QMessageBox::critical(mainWindow, "Not complete", "Implant design is not complete yet. Cannot proceed to review.");
	//	return;
	//}

	//if ( totalPS != NULL )
	//	totalPS->OnFemoralImplantReview();
}

// actual overrider for creating configuration (connects table name and file name)
Configuration* ImplantTotalPS::createConfig(QString const &tableName)
{
   if (tableName == "default")
   {
      Configuration *pC = new Configuration("config_PS.txt", ":", "**");
      if (pC->Load(""))
      {
         return pC;
      }
      else
      {
         delete pC;
         return nullptr;
      }
   }

   return nullptr;
}

/*virtual*/ int ImplantTotalPS::CurrentStage(QString *name/* = nullptr*/) const /*override*/
{
    static const QString sDesignStageName[] = { "*Undefined*", "Femur Basis", "Femur Jigs" };
    if (name)
    {
        if (0 <= m_designStage && m_designStage < sizeof(sDesignStageName)/sizeof(*sDesignStageName))
        {
            *name = sDesignStageName[m_designStage];
        }
        else
        {
            *name = "Unknown";
        }
    }

    return m_designStage;
}

void ImplantTotalPS::OnDesignStageCommenced(Module* pMod)
{
    // look for the stage that is registered to this module
    auto res = m_designStageByModule.find(pMod);
    if (res != m_designStageByModule.end())
        SetDesignStage(res->second);
}

void ImplantTotalPS::OnDesignStageCommenced(Module* pMod, int ord)
{
    // look for the stage that is registered to this module
    auto start = m_designStageByModule.lower_bound(pMod);
    auto end = m_designStageByModule.upper_bound(pMod);

    // now find the stage number 'ord' in all those with 'pMod'
    int i = 0;
    for (auto it = start; it != end; ++it, ++i)
    {
        if (i == ord)
        {
            SetDesignStage(it->second);
            return;
        }
    }
}

void ImplantTotalPS::OnDesignStageCompleted(Module* pMod)
{
    // look for the stage that is registered to this module
    auto res = m_designStageByModule.find(pMod);

    if (res != m_designStageByModule.end())
    {
        // stage with ID res->second is now complete.  Was it active?
        assert(m_designStage == res->second);
        MoveToNextDesignStage();
    }
}

void ImplantTotalPS::OnDesignStageCompleted(Module* pMod, int ord)
{
    // look for the stage that is registered to this module
    auto start = m_designStageByModule.lower_bound(pMod);
    auto end = m_designStageByModule.upper_bound(pMod);

    // now find the stage number 'ord' in all those with 'pMod'
    int i = 0;
    for (auto it = start; it != end; ++it, ++i)
    {
        if (i == ord)
        {
            assert(m_designStage == it->second);
            MoveToNextDesignStage();
            return;
        }
    }
}

void ImplantTotalPS::SetDesignStage(eDesignStage s)
{
    switch (s)
    {
    case FEMUR_BASIS:
    case FEM_JIGS:
        m_designStage = s;
        break;
    default:
        throw "Incorrect Design Stage setting attempted";
    }
}

void ImplantTotalPS::MoveToNextDesignStage()
{
    switch (m_designStage)
    {
    case NOTYET:
    case FEMUR_BASIS:
        m_designStage = eDesignStage(m_designStage + 1);
    case FEM_JIGS:
    default:
        ; // do nothing
    }
}

void ImplantTotalPS::parseUserHistory() // parses the 'userHistory' QDomElement and fills 'uHistory' member
{
    uHistory.clear();
    if (userHistory.tagName() == "UserHistory")
    {
        // take 'userHistory' element and tease out individual 'user' structures from it
        for (int i = 0, n = userHistory.childNodes().size(); i < n; ++i)
        {
            QDomElement uU = userHistory.childNodes().item(i).toElement();
            auto attrs = uU.attributes();
            QString name = attrs.contains("Name") ? attrs.namedItem("Name").toAttr().nodeValue() : "";
            QString ws = attrs.contains("PC") ? attrs.namedItem("PC").toAttr().nodeValue() : "";
            QString datetime = attrs.contains("When") ? attrs.namedItem("When").toAttr().nodeValue() : "";
            QDateTime tt = QDateTime::fromString(datetime);
            int mode = attrs.contains("Mode") ? attrs.namedItem("Mode").toAttr().nodeValue().toInt() : 0;
            QString ver = attrs.contains("Version") ? attrs.namedItem("Version").toAttr().nodeValue() : "";
            // and then stuff each into the uHistory vector
            User u = { name, ws, tt, mode, ver };
            uHistory.push_back(User(u));
        }
    }
    else // reset to make it right
    {
        userHistory.clear();
        userHistory.setTagName("UserHistory");
        userHistory.setAttribute("Version", "1.0");
    }
    // now let's store the current name in the history
    char acComputerName[100];
    DWORD nComputerName = sizeof(acComputerName);
    GetComputerName(acComputerName, &nComputerName);
    QString computerName(acComputerName);
    int mode = isProduction() ? 3 : isBetaTesting() ? 2 : isDevelopment() ? 1 : 0;
    User u_current = { CurrentUserName(), computerName, QDateTime::currentDateTime(), mode, mainWindow->GetAppVersion() };
    uHistory.push_back(u_current);
}
