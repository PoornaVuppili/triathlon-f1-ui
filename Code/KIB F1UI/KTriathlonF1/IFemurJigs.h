#pragma once
#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\EntityEnums.h"
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include <QtGui>

class TEST_EXPORT_TW IFemurJigs
{
public:
	IFemurJigs();
	~IFemurJigs();

	static bool IsFemurJigsComplete();

	// Display/Hide an entity
	static void HideAllFemurJigsEntities();

	static void Review_StartReviewJigs();

private:

};