#include "ViewportControlDialog.h"
#include "ui_ViewportControlDialog.h"
#include "ViewWidget.h"
#include "Viewport.h"

ViewportControlDialog::ViewportControlDialog(QWidget *parent)
    : QDialog(parent)
{
    ui = new Ui::ViewportControlDialog();
    ui->setupUi(this);
}

ViewportControlDialog::~ViewportControlDialog()
{
    delete ui;
}

bool ViewportControlDialog::Init(ViewWidget* vw)
{
    viewWidget = vw;
    num2DView = 0;

    QTableWidget* w = ui->tableWidget;
    w->setFocusPolicy(Qt::NoFocus);

    w->setRowCount( num2DView + 1);   // # of 2D view + one 3D view
    w->setColumnCount(3);
    w->setHorizontalHeaderLabels(QStringList() << "Viewport" << "Show" << "Maximized");
    w->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    w->verticalHeader()->setVisible(false);

    QTableWidgetItem *item,*itemShow,*itemMax;

    Viewport* v = viewWidget->GetViewport();
    item = new QTableWidgetItem(v->GetID());
    item->setFlags( Qt::NoItemFlags | Qt::ItemIsEnabled );
    w->setItem(num2DView,0, item);

    itemShow = new QTableWidgetItem();
    itemMax = new QTableWidgetItem("");
    w->setItem(num2DView,1, itemShow);
    w->setItem(num2DView,2, itemMax);

    for(int j=0; j<num2DView+1; j++)
        UpdateRow(j);

    connect(w, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
    return true;
}

// update the checkboxes on the specified row
void ViewportControlDialog::UpdateRow(int row)
{
    bool isShow, isMax;

    QTableWidget* w = ui->tableWidget;
    QTableWidgetItem* itemShow = w->item(row,1);
    QTableWidgetItem* itemMax  = w->item(row,2);

    if( row >= num2DView )
    {
        Viewport* v = viewWidget->GetViewport();
        isShow = true;  // 3d viewport is always shown
        isMax  = v->IsMaximized();

        itemShow->setCheckState( isShow ? Qt::Checked : Qt::Unchecked );
        itemShow->setFlags( Qt::ItemIsUserCheckable );

        itemMax->setCheckState( isMax ? Qt::Checked : Qt::Unchecked );
        itemMax->setFlags( Qt::ItemIsUserCheckable |Qt::ItemIsEnabled );
    }
}

void ViewportControlDialog::on_pushButtonOK_clicked()
{
    accept();
}

void ViewportControlDialog::onCellChanged(int row,int col)
{
 
}
