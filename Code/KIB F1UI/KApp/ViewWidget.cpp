#include "ViewWidget.h"
#include "MainWindow.h"
#include "Manager.h"
#include "..\KUtility\KUtility.h"
#include "..\KUtility\KApplicationMode.h"
#include "..\KUtility\KXmlHelper.h"
#include <QSettings>
#include <QMessageBox>
#include <QPainter>
#include <QPainterPath>

// Global instance
ViewWidget* viewWidget = NULL;
#define DIVIDERHWID  2   // divider half-width

ViewWidget::ViewWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    setFocusPolicy( Qt::StrongFocus );
    setMouseTracking(true);
	setAutoBufferSwap( false );
    quad = NULL;
    activeView = NULL;
    view3d = NULL;
    overlayImage = NULL;

    QSettings settings;
    QStringList tColor = settings.value("TopColor", "50,140,240").toString().split(",");
    QStringList bColor = settings.value("BottomColor", "200,255,255").toString().split(",");
    if( tColor.size()==3 )
        topColor = QColor(tColor[0].toInt(), tColor[1].toInt(), tColor[2].toInt());
    else
        topColor = QColor(50, 140, 240);
    if( bColor.size()==3 )
        bottomColor = QColor(bColor[0].toInt(), bColor[1].toInt(), bColor[2].toInt());
    else
		bottomColor = QColor(200, 255, 255);

    ClearContent();
}

ViewWidget::~ViewWidget()
{
    //makeCurrent();
    //gluDeleteQuadric(quad);

    //ClearContent();
}

void ViewWidget::SetTopColor(QColor c)
{
    topColor = c;
    updateGL();

    QSettings settings;
    settings.setValue("TopColor", QString("%1,%2,%3").arg(c.red()).arg(c.green()).arg(c.blue()));
}

void ViewWidget::SetBottomColor(QColor c)
{
    bottomColor = c;
    updateGL();

    QSettings settings;
    settings.setValue("BottomColor", QString("%1,%2,%3").arg(c.red()).arg(c.green()).arg(c.blue()));
}

///////////////////////////////////////////////////////////////////////
// Function name:    ClearContent
// Function purpose: Clear everything.
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::ClearContent()
{
    int i;

    // delete all viewports
    DeletePtr( view3d );
    
    vDivide.clear();
    hDivide[0].clear();
    hDivide[1].clear();

    // reset the active viewport
    activeView = NULL;

    // delete the overlay image if exists
    DeletePtr(overlayImage);

    mainWindow->UpdateUndoStack(nullptr);
    // delete all managers if exist
    for(i=0; i<managerStack.size(); i++)
        delete managerStack[i];
    managerStack.clear();

    makeCurrent();
}

// Create the (3D) viewport
Viewport* ViewWidget::CreateViewport(const QString& id)
{
    if( view3d )
        return NULL;   // already exists

    view3d = new Viewport();
    view3d->SetGLWidget(this);
    view3d->SetQuadricObj(quad);
    if( !id.isEmpty() )
        view3d->SetID(id);

	QToolBar *b = view3d->SetupPredefinedViewsToolBar();
	mainWindow->addToolBar(b);
	b->show();

    connect(view3d, SIGNAL(maximizeChanged()), this, SLOT(MaximizeRestoreViewport()));

    return view3d;
}

///////////////////////////////////////////////////////////////////////
// Function name:    initializeGL
// Function purpose: OpenGL initialization
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::initializeGL()
{
    // depth related
    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

    // lighting related
    glEnable( GL_LIGHTING );
    glShadeModel( GL_SMOOTH );

    glEnable( GL_POINT_SMOOTH );

    float	fAmbientLight = 0.1f;
	float	fDiffuseLight = 0.5f;

	GLfloat ambient[] = { fAmbientLight, fAmbientLight, fAmbientLight, 1.0f };
    GLfloat diffuse[] = { fDiffuseLight, fDiffuseLight, fDiffuseLight, 1.0f };
    glLightfv( GL_LIGHT0, GL_AMBIENT,	ambient );
    glLightfv( GL_LIGHT0, GL_DIFFUSE,	diffuse );

    glEnable( GL_LIGHT0 );
	glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );

    float	fAmbientMat = 0.4f;
	float	fDiffuseMat = 0.4f;
	float	fSpecularMat = 0.8f;
	float	fShininessMat = 20.0f;

    GLfloat		ambientMat[4] = { fAmbientMat, fAmbientMat, fAmbientMat, 1.0 };
	GLfloat		diffuseMat[4] = { fDiffuseMat, fDiffuseMat, fDiffuseMat, 1.0 };
	GLfloat		specularMat[4] = { fSpecularMat, fSpecularMat, fSpecularMat, 0.0 };

	glMaterialfv( GL_FRONT, GL_AMBIENT, ambientMat );
	glMaterialfv( GL_FRONT, GL_DIFFUSE, diffuseMat );
	glMaterialfv( GL_FRONT, GL_SPECULAR, specularMat );
	glMaterialf( GL_FRONT, GL_SHININESS, fShininessMat );

    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    quad = gluNewQuadric();
    gluQuadricDrawStyle(quad, GLU_FILL);
    gluQuadricNormals(quad, GLU_SMOOTH);

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_INDEX_ARRAY);
}

void ViewWidget::UpdateViewports()
{
    if( vDivide.size()==0 )
        return;

    if( activeView && activeView->IsMaximized() )
    {
        activeView->SetViewport(0, width(), 0, height());
    }
    else if( rowDivider==1 )
    {
        const vector<int>& hDiv = hDivide[0];
        const int n = (int) hDiv.size()-1;

        int i = 0;
       
        view3d->SetViewport(i==0 ? hDiv[n-1] : hDiv[n-1]+DIVIDERHWID, hDiv[n], vDivide[0], vDivide[1]);
    }
    else
    {
        const vector<int>& hDiv0 = hDivide[0];
        const vector<int>& hDiv1 = hDivide[1];
        const int n0 = (int)hDiv0.size()-1;
        const int n1 = (int)hDiv1.size()-1;

        int row = 1;
        int col = 0;
        
        view3d->SetViewport(col==0 ? hDiv0[col] : hDiv0[col]+DIVIDERHWID, hDiv0[col+1], vDivide[0], vDivide[1]-DIVIDERHWID);
    }
}

///////////////////////////////////////////////////////////////////////
// Function name:    resizeGL
// Function purpose: Handle the resize of the widget
// Input: 
//   width, height: the new size of the widget
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::resizeGL(int width, int height)
{
    if( view3d == NULL || vDivide.size()==0 )
        return;

    vDivide[1] = vDivide[1] * height / vDivide[2];
    vDivide[2] = height;

    for(int i=0; i<2; i++)
    {
        vector<int>& hDiv = hDivide[i];
        const int n = (int)hDiv.size();
        for(int j=1; j<n-1; j++)
            hDiv[j] = hDiv[j] * width / hDiv[n-1];
        if(n>0)
        {
            hDiv[0] = 0;
            hDiv[n-1] = width;
        }
    }
    
    UpdateViewports();
    updateGL();
}

void ViewWidget::glDraw()
{
    static bool isDrawing = false;
    static bool needRedraw = false;

    if( isDrawing )
    {
        needRedraw = true;  // something is updated during drawing -> need redraw
        return;
    }

    isDrawing = true;
    do
    {
        needRedraw = false;
        QGLWidget::glDraw();
    }
    while( needRedraw );
    isDrawing = false;
}

///////////////////////////////////////////////////////////////////////
// Function name:    paintGL
// Function purpose: Paint the widget client area.
// Input: None
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::paintGL()
{
	glDrawBuffer( GL_BACK );

	glClearDepth(1.0);
    glClear(GL_DEPTH_BUFFER_BIT);

    // draw the background
    DrawBackground();

    // draw the viewports
    if( activeView && activeView->IsMaximized() )
    {
        activeView->Draw();
    }
    else
    {
        if( view3d )
            view3d->Draw();
    }

    // draw the viewport divider
    if( !(activeView && activeView->IsMaximized()) )
        DrawViewportDivider();

	swapBuffers();
    glFinish();

	if( view3d )
		view3d->DrawAfterBuffersSwap();

#ifndef NDEBUG
    GLenum err = glGetError();
    if( err != GL_NO_ERROR )
        QMessageBox::critical(this, "GL Error", (char*)gluErrorString(err));
#endif
}

///////////////////////////////////////////////////////////////////////
// Function name:    DrawBackground
// Function purpose: Draw the background
// Input: None
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::DrawBackground()
{
    glPushAttrib( GL_ENABLE_BIT | GL_VIEWPORT_BIT );

    // backup the projection matrix and set up
    glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0, 1, 0, 1, -1, 1 );
    glViewport(0, 0, width(), height());

    // backup the model view matrix, attrib.. and set up
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glDisable( GL_LIGHTING );
	glDisable( GL_DEPTH_TEST );

    glBegin( GL_TRIANGLE_STRIP );
    glColor3f( topColor.redF(), topColor.greenF(), topColor.blueF() ); glVertex2f( 0, 1 );
	glColor3f( bottomColor.redF(), bottomColor.greenF(), bottomColor.blueF() );	glVertex2f( 0, 0 );
	glColor3f( topColor.redF(), topColor.greenF(), topColor.blueF() ); glVertex2f( 1, 1 );
	glColor3f( bottomColor.redF(), bottomColor.greenF(), bottomColor.blueF() ); glVertex2f( 1, 0 );
	glEnd();

#ifdef TESTING_MODE
    if( !view3d && view2d.size()== 0 )
    {
        QFont serifFont("Times", 20, QFont::Bold);
        glColor3ub(128,0,0);
        renderText(width()/3, height()/2, QString("Test version ") + mainWindow->GetAppVersion() + ". Not for production use.",serifFont);
    }
#endif
    if (mainWindow->IsReviewMode())
    {
      glLineWidth(9);
      glColor3ub(163, 73, 164);
      glBegin( GL_LINE_LOOP );
         glVertex2d( 0, 0 );
         glVertex2d( 1, 0 );
         glVertex2d( 1, 1 );
         glVertex2d( 0, 1 );
      glEnd();
    }

    // recover the matrices etc.
    glMatrixMode( GL_PROJECTION );
	glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	glPopAttrib();
}

// Draw the viewport divider
void ViewWidget::DrawViewportDivider()
{
    if( view3d==NULL || vDivide.size()==0 )
        return;

    QPalette pal;
    //QColor	activeColor = pal.color(QPalette::Normal, QPalette::Highlight);
    QColor	activeColor(128, 0, 0);
    QColor inactiveColor = pal.color(QPalette::Normal, QPalette::Window);

    glPushAttrib( GL_ENABLE_BIT | GL_VIEWPORT_BIT );
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LINE_SMOOTH);
    glDisable(GL_BLEND);

    int wid = width();
    int hei = height();

    // backup the projection matrix and set up
    glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0, wid, 0, hei, -1, 1 );
    glViewport(0, 0, wid, hei);

    // backup the model view matrix, attrib.. and set up
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();

    // draw the dividers
    glColor3f( inactiveColor.redF(), inactiveColor.greenF(), inactiveColor.blueF() );
    glBegin(GL_QUADS);
    if( rowDivider==2 )
    {
        for(int i=0; i<2; i++)
        {
            vector<int>& hDiv = hDivide[i];
            const int n = (int)hDiv.size();
            const int ymin = vDivide[i] + i*DIVIDERHWID;
            const int ymax = vDivide[i+1] - (1-i)*DIVIDERHWID;
            for(int j=1; j<n-1; j++)
            {
                glVertex3i(hDiv[j]-DIVIDERHWID, ymin, 0);
                glVertex3i(hDiv[j]+DIVIDERHWID, ymin, 0);
                glVertex3i(hDiv[j]+DIVIDERHWID, ymax, 0);
                glVertex3i(hDiv[j]-DIVIDERHWID, ymax, 0);
            }
        }
        glVertex3i(0,   vDivide[1]-DIVIDERHWID, 0);
        glVertex3i(wid, vDivide[1]-DIVIDERHWID, 0);
        glVertex3i(wid, vDivide[1]+DIVIDERHWID, 0);
        glVertex3i(0,   vDivide[1]+DIVIDERHWID, 0);
    }
    else if( vDivide.size()>0 )
    {
        vector<int>& hDiv = hDivide[0];
        const int n = (int)hDiv.size();
        const int ymin = vDivide[0];
        const int ymax = vDivide[1];
        for(int j=1; j<n-1; j++)
        {
            glVertex3i(hDiv[j]-DIVIDERHWID, ymin, 0);
            glVertex3i(hDiv[j]+DIVIDERHWID, ymin, 0);
            glVertex3i(hDiv[j]+DIVIDERHWID, ymax, 0);
            glVertex3i(hDiv[j]-DIVIDERHWID, ymax, 0);
        }
    }
    glEnd();

    // highlight the active view
    if( activeView )
    {
        glLineWidth(1);
        glColor3f( activeColor.redF(), activeColor.greenF(), activeColor.blueF() );
        
        int row, col;
        if( view3d==activeView )
        {
            row = 0;
            col = (int)hDivide[0].size() - 2;
        }
        else
        {
            int i=0;
            int j=0;
            
            if( rowDivider==1 )
            {
                row = 0;
                col = i;
            }
            else if( i>hDivide[1].size()-2 )
            {
                row = 0;
                col = i - ((int)hDivide[1].size()-1);
            }
            else
            {
                row = 1;
                col = i;
            }
        }

        int y1,y2;
        if( rowDivider==1 )
        {
            y1 = vDivide[0];
            y2 = vDivide[1];
        }
        else if(row==0)
        {
            y1 = vDivide[0];
            y2 = vDivide[1]-DIVIDERHWID;
        }
        else
        {
            y1 = vDivide[1]+DIVIDERHWID;
            y2 = vDivide[2];
        }
        y1 += 1;
        y2 -= 1;

        vector<int>& hDiv = hDivide[row];
        int x1 = hDiv[col];
        if( col!=0 )
            x1 += DIVIDERHWID;
        int x2 = hDiv[col+1];
        if( col != hDiv.size()-2 )
            x2 -= DIVIDERHWID;
        x1 += 1;
        x2 -= 1;
        glBegin(GL_LINE_LOOP);
        glVertex3i(x1, y2, 0);
        glVertex3i(x1, y1, 0);
        glVertex3i(x2, y1, 0);
        glVertex3i(x2, y2, 0);
        glEnd();
    }

    // recover the matrices etc.
    glMatrixMode( GL_PROJECTION );
	glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

    glPopAttrib();
}

///////////////////////////////////////////////////////////////////////
// Function name:    mousePressEvent
// Function purpose: Called when a mouse button is pressed. It currently handles:
//   Left button: pick an object 
//   Middle button: initialize rotation 
// Input:
//   event : the mouse event
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::mousePressEvent ( QMouseEvent * event )
{
    if( activeView && activeView->IsMaximized() )
        return activeView->ProcessMousePressEvent(event);

    int x = event->x();
    int y = height() - event->y();

    if( IsOnDivider(x, y) )
    {
        modifyDivider = true;
        return;
    }
    else
        modifyDivider = false;

    if( view3d && view3d->InsideViewport(x, y) )
    {
        if( activeView != view3d )
        {
            activeView = view3d;
            updateGL();
        }
        activeView->ProcessMousePressEvent(event);
    }
   
}

// Test if (x,y) is on a divider
bool ViewWidget::IsOnDivider(int x, int y)
{
    if( view3d )
    {
        const int sensitivity = 3;
        bool onVdivide = ( rowDivider==2 && abs(y-vDivide[1]) <= sensitivity );
        bool onHdivide = false;
        for(int i=0; i<rowDivider; i++)
        {
            vector<int>& hDiv = hDivide[i];
            const int n = (int)hDiv.size();
            
            int ymin = vDivide[i];
            int ymax = vDivide[i+1];

            for(int j=1; j<n-1; j++)
            {
                if( y>=ymin && y<=ymax && abs(x-hDiv[j]) <= sensitivity )
                {
                    onHdivide = true;
                    onVDividerIndex = i;
                    onHDividerIndex = j;
                    break;
                }
            }
            if( onHdivide )
                break;
        }
        if( onHdivide && onVdivide )
        {
            onDividerType = dividerBoth;
            return true;
        }
        else if( onHdivide )
        {
            onDividerType = dividerHor;
            return true;
        }
        else if( onVdivide )
        {
            onDividerType = dividerVer;
            return true;
        }
    }
    onDividerType = dividerNone;
    return false;
}

///////////////////////////////////////////////////////////////////////
// Function name:    mouseMoveEvent
// Function purpose: Called when a mouse is dragging. It currently handles:
//   Middle button: rotate the object.
// Input:
//   event : the mouse event
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::mouseMoveEvent ( QMouseEvent * event )
{
    if( activeView && activeView->IsMaximized() )
        return activeView->ProcessMouseMoveEvent(event);

    int x = event->x();
    int y = height() - event->y();

    if( event->buttons() == Qt::NoButton )
    {
      if( view3d && view3d->InsideViewport(x, y) )
            view3d->ProcessMouseMoveEvent(event);
        
        return;
    }
    else if( event->buttons() == Qt::LeftButton && modifyDivider )
    {
        const int limit = 20;
        bool needUpdate = false;
        if( onDividerType==dividerBoth || onDividerType==dividerHor )
        {
            vector<int>& hDiv = hDivide[onVDividerIndex];

            if( x > hDiv[onHDividerIndex-1] + limit && 
                x < hDiv[onHDividerIndex+1] - limit )
            {
                hDiv[onHDividerIndex] = x;
                needUpdate = true;
            }
        }
        if( onDividerType==dividerBoth || onDividerType==dividerVer )
        {
            if( y > vDivide[0] + limit && 
                y < vDivide[2] - limit )
            {
                vDivide[1] = y;
                needUpdate = true;
            }
        }
        if( needUpdate )
        {
            UpdateViewports();
            updateGL();
        }
        return;
    }

    if( activeView )
        activeView->ProcessMouseMoveEvent(event);
}

// Called when a mouse button is released
void ViewWidget::mouseReleaseEvent ( QMouseEvent * event )
{
    if( modifyDivider )
        modifyDivider = false;

    if( activeView )
        activeView->ProcessMouseReleaseEvent(event);
}

// Called when a mouse button is double clicked
void ViewWidget::mouseDoubleClickEvent( QMouseEvent * event )
{
    if( activeView && activeView->IsMaximized() )
        return activeView->ProcessDoubleClickEvent(event);

    int x = event->x();
    int y = height() - event->y();
    if( IsOnDivider(x, y) )
        return;

    if( view3d && view3d->InsideViewport(x, y) )
    {
        activeView = view3d;
        activeView->ProcessDoubleClickEvent(event);
    }
}

///////////////////////////////////////////////////////////////////////
// Function name:    wheelEvent
// Function purpose: Called when a mouse wheel is scrolled, which is 
//   used to zoom in and out the 3d view.
// Input:
//   event : the wheel event
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::wheelEvent ( QWheelEvent * event )
{
    if( activeView && activeView->IsMaximized() )
        return activeView->ProcessWheelEvent(event);

    if( view3d && view3d->InsideViewport(event->x(), height()-event->y()) )
    {
        if( activeView != view3d )
        {
            activeView = view3d;
            updateGL();
        }
		setFocus();
        activeView->ProcessWheelEvent(event);
    }
}

///////////////////////////////////////////////////////////////////////
// Function name:    keyPressEvent
// Function purpose: Called when a key is pressed. 
// Input:
//   event : the key event
// Output: None
///////////////////////////////////////////////////////////////////////
void ViewWidget::keyPressEvent(QKeyEvent* event)
{
    if( activeView )
        activeView->ProcessKeyPressEvent(event);
}

void ViewWidget::keyReleaseEvent(QKeyEvent* event)
{
    if (activeView)
        activeView->ProcessKeyReleaseEvent(event);
}

void ViewWidget::focusInEvent(QFocusEvent *event)
{
    if (activeView)
        activeView->ProcessFocusInEvent(event);
}

void ViewWidget::focusOutEvent(QFocusEvent *event)
{
    if (activeView)
        activeView->ProcessFocusOutEvent(event);
}

void ViewWidget::ZoomIn()
{
    if( activeView )
        activeView->Zoom(true);
}

void ViewWidget::ZoomOut()
{
    if( activeView )
        activeView->Zoom(false);
}

void ViewWidget::ZoomFit()
{
    if( activeView && activeView->IsMaximized() )
    {
        activeView->BestFit();
    }
    else
    {
        if( view3d )
            view3d->BestFit();
    }
    updateGL();
}

// Set the viewing status based on the content of 'docElem'
void ViewWidget::SetStatus(QDomElement docElem)
{
    try
    {
        if( docElem.tagName()!="View" || docElem.attribute("Version") != "1.0" )
            throw exception("Incorrect header or version");

        const int numView = 0;

        QDomNodeList nodes = docElem.childNodes();
        int numNode = nodes.count();
        const int N = 6;
        if( numNode != N + numView + 1 )
            throw exception("Incorrect content");
        // 0 - width
        // 1 - height
        // 2 - number of rows
        // 3 - v divider positions
        // 4,5 - h divider positions
        // numView of 2d viewport settings
        // 1 3d viewport setting

        int i,j;
        QDomElement elem;

        // content from the status
        int wid,hei;
        vector<int> vd;
        vector<int> hd[2];

        elem = nodes.item(0).toElement();
        if( elem.tagName() != "Width" || !ParseNode(elem, wid) )
            throw exception("Incorrect content");

        elem = nodes.item(1).toElement();
        if( elem.tagName() != "Height" || !ParseNode(elem, hei) )
            throw exception("Incorrect content");

        elem = nodes.item(2).toElement();
        if( elem.tagName() != "NumRow" || !ParseNode(elem, rowDivider) )
            throw exception("Incorrect content");

        elem = nodes.item(3).toElement();
        if( elem.tagName() != "VDivide" || !ParseNode(elem, vd) )
            throw exception("Incorrect content");

        elem = nodes.item(4).toElement();
        if( elem.tagName() != "HDivide0" || !ParseNode(elem, hd[0]) )
            throw exception("Incorrect content");

        elem = nodes.item(5).toElement();
        if( elem.tagName() != "HDivide1" || !ParseNode(elem, hd[1]) )
            throw exception("Incorrect content");

		// adjust the viewport
        vDivide.resize( vd.size() );
        for(i=0; i<vd.size(); i++)
            vDivide[i] = vd[i] * height() / hei;
        for(j=0; j<2; j++)
        {
            hDivide[j].resize( hd[j].size() );
            for(i=0; i<hd[j].size(); i++)
                hDivide[j][i] = hd[j][i] * width() / wid;
        }
        UpdateViewports();

		// 3d viewport information
        elem = nodes.item(N+numView).toElement();
		view3d->SetStatus(elem);
		if( view3d->IsMaximized() )
			activeView = view3d;

        for(i=0; i<numView; i++)
        {
            elem = nodes.item(N+i).toElement();
        }
    }
    catch(exception&)
    {
        // create the default viewing
        DivideViewports();
        view3d->BestFit();
    }
}

// Get the viewing status and save under 'docElem'
bool ViewWidget::GetStatus(QDomElement& viewElem)
{
    viewElem.setTagName("View");
    viewElem.setAttribute("Version", "1.0");

    // remove all child nodes
    while( viewElem.hasChildNodes() )
        viewElem.removeChild( viewElem.lastChild() );

    AppendNode(viewElem, "Width", width());
    AppendNode(viewElem, "Height", height());
    AppendNode(viewElem, "NumRow", rowDivider);
    AppendNode(viewElem, "VDivide", vDivide);
    for(int j=0; j<2; j++)
        AppendNode(viewElem, QString("HDivide%1").arg(j), hDivide[j]);

    QDomDocument doc = viewElem.ownerDocument();

    QDomElement root3D = doc.createElement("dummy");
    viewElem.appendChild(root3D);
	view3d->GetStatus(root3D);

    return true;
}

void ViewWidget::OnShowViewportChanged()
{
    DivideViewports();
    ZoomFit();
}

void ViewWidget::DivideViewports()
{
    int i,j,n[2];

    int numView = 1;    // 3d viewport is always show
  

    rowDivider = numView>2 ? 2 : 1;

    // divide the viewports
    const int w = width();
    const int h = height();
    const int h2 = height()/2;

    vDivide.resize(3);
    vDivide[0] = 0;
    vDivide[1] = rowDivider==2 ? h2 : h;
    vDivide[2] = h;

    if( rowDivider==2 )
    {
        n[1] = (numView+1)>>1;
        n[0] = numView - n[1];
    }
    else
    {
        n[1] = 0;
        n[0] = numView;
    }

    for(i=0; i<rowDivider; i++)
    {
        vector<int>& hDiv = hDivide[i];
        hDiv.resize(n[i] + 1);
        hDiv[0] = 0;
        hDiv[n[i]] = w;
        for(j=1; j<n[i]; j++)
            hDiv[j] = w * j / n[i];
    }

    UpdateViewports();
}

void ViewWidget::MaximizeRestoreViewport()
{
    // set the activeView if not set
    int i = 0;
   
    if( view3d->IsMaximized() )
        activeView = view3d;

    UpdateViewports();
    updateGL();
}

// Set the measure mode
void ViewWidget::SetMeasureMode(MeasureMode m)
{
    if( view3d )
        view3d->SetMeasureMode(m);
   
    updateGL();
    kStatus.ShowMessage("Ready");
}

float ViewWidget::GetActiveViewMeasure()
{
    return (activeView==NULL) ? 0 : activeView->GetMeasure();
}

// Start the overlay
void ViewWidget::StartOverlay()
{
    updateGL();
    glColorMask(false,false,false,false);
    glDepthMask(false);
    overlayImage = new QImage();
    *overlayImage = grabFrameBuffer();
}

// Draw the overlay
void ViewWidget::DrawOverlay(QColor& color, ContourInt& contour)
{
    int len = contour.GetLength();
    if( len<=0 || overlayImage==NULL)
        return;

    QPainter painter(this);
    painter.drawImage(0,0,*overlayImage);

    // draw the selection contour
    QBrush cb = painter.brush();
    painter.setBrush(color);
    QPainterPath path;
    path.moveTo(contour.X(0), contour.Y(0));
    for(int i=0; i<len; i++)
        path.lineTo(contour.X(i), contour.Y(i));
    path.closeSubpath();
    path.setFillRule(Qt::WindingFill);
    painter.drawPath( path );
    painter.setBrush(cb);
    swapBuffers();
}

// End the overlay
void ViewWidget::EndOverlay()
{
    repaint();
    DeletePtr(overlayImage);
    glColorMask(true,true,true,true);
    glDepthMask(true);
}

Manager* ViewWidget::GetManager(QString id)
{
	for(int i=0; i<managerStack.size(); i++)
		if(managerStack[i]->GetId() == id)
			return managerStack[i];

	return NULL;
}

// Add the manager for 3D viewport or 2D viewport image. Default to viewport only.
//  inViewport          : true/false = set the manager to/not to the 3D viewport
void ViewWidget::AddManager(Manager* mgr, bool inViewport/*, int inViewportImageFirst, int inViewportImageCount*/)
{
    QWidget* w;
    QToolBar* b;

    if( !managerStack.empty() )
    {
        Manager* c_mgr = managerStack.top();
        if( c_mgr == mgr )
            return;

    }

    managerStack.push( mgr );

    // add manager to the 3D viewport
    if( inViewport )
    {
        assert( view3d != NULL );
        view3d->AddManager( mgr );
    }
   
    // display the widget/toolbar associated with the manager if exists
    if( (w=mgr->GetWidget()) != NULL ) // has a widget
        mainWindow->SetEditControl(w, true);
    else if( (b=mgr->GetToolBar()) != NULL )
        mainWindow->SetEditControl(b, true);

    // enable the undo stack associated with the manager if exists
    mainWindow->UpdateUndoStack( mgr->GetUndoStack() );

    QString helpfulHint;
    if (mgr->GetHint(helpfulHint))
        mainWindow->ContextHelp(helpfulHint);
}

// Remove the manager from all viewports if being used
void ViewWidget::RemoveManager(Manager* mgr)
{
    if( managerStack.empty() || managerStack.top() != mgr )
        return;

    managerStack.pop();

    // remove manager from all viewports if they are on the top of the stack
    if(view3d) view3d->RemoveManager( mgr );

    // remove the widget associated with the manager if exists
    QWidget* w;
    QToolBar* b;

    if( (w=mgr->GetWidget()) != NULL ) // has a widget
        mainWindow->SetEditControl(w, false);
    else if( (b=mgr->GetToolBar()) != NULL )
        mainWindow->SetEditControl(b, false);

    // update the undo stack
    // - if the top stack has undo, replace the undo with it; otherwise, null
    if( !managerStack.empty() )
    {
        Manager* c_mgr = managerStack.top();
        // display the widget/toolbar associated with the manager if exists
        if( (w=c_mgr->GetWidget()) != NULL ) // has a widget
            mainWindow->SetEditControl(w, true);
        else if( (b=c_mgr->GetToolBar()) != NULL )
            mainWindow->SetEditControl(b, true);

        mainWindow->UpdateUndoStack( c_mgr->GetUndoStack() );

        QString helpfulHint;
        if (c_mgr->GetHint(helpfulHint))
            mainWindow->ContextHelp(helpfulHint);
    }
    else
    {
        mainWindow->UpdateUndoStack( NULL );
        mainWindow->ContextHelp("");
    }
}

// renders text with default font and at pixel offset
void ViewWidget::RenderText(double x, double y, double z, const QString & str, int off_x /*= 0*/, int off_y /*= 0*/)
{
   // positive off_x means to the right
   // positive off_y means up
   // let's convert the offset into a vector using the current view orientation/scale
   Viewport *vp = view3d;
   Rotate3Float ir = vp->GetTransform().GetRotationMatrix().Inverse();
   const float unit = vp->GetResolution_MMPerPixel();

   Point3Float vx = ir * Point3Float(unit * off_x,0,0);
   Point3Float vy = ir * Point3Float(0,unit * off_y,0);

   double xx = x + vx.x() + vy.x(), yy = y + vx.y() + vy.y(), zz = z + vx.z() + vy.z();

   QGLWidget::renderText(xx, yy, zz, str);
}

// renders text with default font and at pixel offset
void ViewWidget::RenderTextBg(double x, double y, double z, const QString & str, COLORREF bgcolor, int off_x /*= 0*/, int off_y /*= 0*/)
{
   //// first, let's measure the text
   //QFontMetrics fm(font);
   //int width = fm.width(str) + 2;
   //int height = fm.height();

   //// positive off_x means to the right
   //// positive off_y means up
   //// let's convert the offset into a vector using the current view orientation/scale
   //Viewport *vp = view3d;
   //Rotate3Float ir = vp->GetTransform().GetRotationMatrix().Inverse();
   //const float unit = vp->GetResolution_MMPerPixel();

   //Point3Float vx = ir * Point3Float(unit * off_x,0,0);
   //Point3Float vy = ir * Point3Float(0,unit * off_y,0);

   //double xx = x + vx.x() + vy.x(), yy = y + vx.y() + vy.y(), zz = z + vx.z() + vy.z();

   //Point3Float vxbg = ir * Point3Float(unit * (off_x - 1),0,0);
   //Point3Float vybg = ir * Point3Float(0,unit * (off_y - 1),0);
   //Point3Float vxBG = ir * Point3Float(unit * (off_x - 1 + width),0,0);
   //Point3Float vyBG = ir * Point3Float(0,unit * (off_y - 1 + height),0);
   //double xxbg = x + vxbg.x() + vybg.x(), yybg = y + vxbg.y() + vybg.y(), zzbg = z + vxbg.z() + vybg.z();
   //double xxBg = x + vxBG.x() + vybg.x(), yyBg = y + vxBG.y() + vybg.y(), zzBg = z + vxBG.z() + vybg.z();
   //double xxBG = x + vxBG.x() + vyBG.x(), yyBG = y + vxBG.y() + vyBG.y(), zzBG = z + vxBG.z() + vyBG.z();
   //double xxbG = x + vxbg.x() + vyBG.x(), yybG = y + vxbg.y() + vyBG.y(), zzbG = z + vxbg.z() + vyBG.z();

   //// now draw the rectangle, then the text
   //glPushAttrib(GL_CURRENT_BIT);
   //glColor3ub(GetRValue(bgcolor), GetGValue(bgcolor), GetBValue(bgcolor));
   //glBegin(GL_QUADS);
   //   glVertex3f(xxbg, yybg, zzbg);
   //   glVertex3f(xxBg, yyBg, zzBg);
   //   glVertex3f(xxBG, yyBG, zzBG);
   //   glVertex3f(xxbG, yybG, zzbG);
   //glEnd();
   //glPopAttrib();

   //QGLWidget::renderText(xx, yy, zz, str);
   ViewWidget::RenderTextBg(x, y, z, str, bgcolor, QFont(), off_x, off_y);
}


// renders text with default font and at pixel offset
void ViewWidget::RenderTextBg(double x, double y, double z, const QString & str,
   COLORREF bgcolor, QFont const & otherfont, int off_x /*= 0*/, int off_y /*= 0*/)
{
   // first, let's measure the text
   QFontMetrics fm(otherfont);
   int width = fm.width(str) + 2;
   int height = fm.height();

   // positive off_x means to the right
   // positive off_y means up
   // let's convert the offset into a vector using the current view orientation/scale
   Viewport *vp = view3d;
   Rotate3Float ir = vp->GetTransform().GetRotationMatrix().Inverse();
   const float unit = vp->GetResolution_MMPerPixel();

   Point3Float vx = ir * Point3Float(unit * off_x,0,0);
   Point3Float vy = ir * Point3Float(0,unit * off_y,0);

   double xx = x + vx.x() + vy.x(), yy = y + vx.y() + vy.y(), zz = z + vx.z() + vy.z();

   Point3Float vxbg = ir * Point3Float(unit * (off_x - 1),0,0);
   Point3Float vybg = ir * Point3Float(0,unit * (off_y - 1),0);
   Point3Float vxBG = ir * Point3Float(unit * (off_x - 1 + width),0,0);
   Point3Float vyBG = ir * Point3Float(0,unit * (off_y - 1 + height),0);
   double xxbg = x + vxbg.x() + vybg.x(), yybg = y + vxbg.y() + vybg.y(), zzbg = z + vxbg.z() + vybg.z();
   double xxBg = x + vxBG.x() + vybg.x(), yyBg = y + vxBG.y() + vybg.y(), zzBg = z + vxBG.z() + vybg.z();
   double xxBG = x + vxBG.x() + vyBG.x(), yyBG = y + vxBG.y() + vyBG.y(), zzBG = z + vxBG.z() + vyBG.z();
   double xxbG = x + vxbg.x() + vyBG.x(), yybG = y + vxbg.y() + vyBG.y(), zzbG = z + vxbg.z() + vyBG.z();

   // now draw the rectangle, then the text
   glPushAttrib(GL_CURRENT_BIT);
   glColor3ub(GetRValue(bgcolor), GetGValue(bgcolor), GetBValue(bgcolor));
   glBegin(GL_QUADS);
      glVertex3f(xxbg, yybg, zzbg);
      glVertex3f(xxBg, yyBg, zzBg);
      glVertex3f(xxBG, yyBG, zzBG);
      glVertex3f(xxbG, yybG, zzbG);
   glEnd();
   glPopAttrib();

   QGLWidget::renderText(xx, yy, zz, str, otherfont);
}
