/****************************************************************************
** Meta object code from reading C++ file 'TotalView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../TotalView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TotalView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CTotalView_t {
    QByteArrayData data[89];
    char stringdata0[1801];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CTotalView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CTotalView_t qt_meta_stringdata_CTotalView = {
    {
QT_MOC_LITERAL(0, 0, 10), // "CTotalView"
QT_MOC_LITERAL(1, 11, 11), // "OnViewFront"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 10), // "OnViewBack"
QT_MOC_LITERAL(4, 35, 9), // "OnViewTop"
QT_MOC_LITERAL(5, 45, 12), // "OnViewBottom"
QT_MOC_LITERAL(6, 58, 10), // "OnViewLeft"
QT_MOC_LITERAL(7, 69, 11), // "OnViewRight"
QT_MOC_LITERAL(8, 81, 14), // "OnViewFrontIso"
QT_MOC_LITERAL(9, 96, 13), // "OnViewBackIso"
QT_MOC_LITERAL(10, 110, 20), // "OnImportFemurSurface"
QT_MOC_LITERAL(11, 131, 20), // "CManagerActivateType"
QT_MOC_LITERAL(12, 152, 10), // "manActType"
QT_MOC_LITERAL(13, 163, 17), // "OnMakeFemoralAxes"
QT_MOC_LITERAL(14, 181, 19), // "OnReviewFemoralAxes"
QT_MOC_LITERAL(15, 201, 19), // "OnHandleFemoralAxes"
QT_MOC_LITERAL(16, 221, 24), // "OnAutoInitializeAllSteps"
QT_MOC_LITERAL(17, 246, 24), // "OnAutoInitializeJCOSteps"
QT_MOC_LITERAL(18, 271, 28), // "calledFromInitializeAllSteps"
QT_MOC_LITERAL(19, 300, 24), // "OnAutoRegenerateJCOSteps"
QT_MOC_LITERAL(20, 325, 20), // "OnAutoRefineJCOSteps"
QT_MOC_LITERAL(21, 346, 31), // "OnAutoInitializeFromFemoralAxes"
QT_MOC_LITERAL(22, 378, 23), // "OnManualDefaultJCOSteps"
QT_MOC_LITERAL(23, 402, 22), // "OnDefineOutlineProfile"
QT_MOC_LITERAL(24, 425, 22), // "OnReviewOutlineProfile"
QT_MOC_LITERAL(25, 448, 10), // "OnCutFemur"
QT_MOC_LITERAL(26, 459, 19), // "OnReviewFemoralCuts"
QT_MOC_LITERAL(27, 479, 15), // "OnHandleFillets"
QT_MOC_LITERAL(28, 495, 24), // "OnAutoInitializeFPCSteps"
QT_MOC_LITERAL(29, 520, 17), // "OnAdvancedControl"
QT_MOC_LITERAL(30, 538, 7), // "OnTest1"
QT_MOC_LITERAL(31, 546, 25), // "OnImportOsteophyteSurface"
QT_MOC_LITERAL(32, 572, 17), // "OnHandleCartilage"
QT_MOC_LITERAL(33, 590, 28), // "OnAutoInitializeAllJigsSteps"
QT_MOC_LITERAL(34, 619, 31), // "OnAutoInitializeDrivenJigsSteps"
QT_MOC_LITERAL(35, 651, 17), // "OnDefineCartilage"
QT_MOC_LITERAL(36, 669, 17), // "OnReviewCartilage"
QT_MOC_LITERAL(37, 687, 26), // "OnDefineOutlineProfileJigs"
QT_MOC_LITERAL(38, 714, 26), // "OnReviewOutlineProfileJigs"
QT_MOC_LITERAL(39, 741, 24), // "OnHandleOuterSurfaceJigs"
QT_MOC_LITERAL(40, 766, 22), // "OnMakeInnerSurfaceJigs"
QT_MOC_LITERAL(41, 789, 24), // "OnReviewInnerSurfaceJigs"
QT_MOC_LITERAL(42, 814, 22), // "OnMakeOuterSurfaceJigs"
QT_MOC_LITERAL(43, 837, 24), // "OnReviewOuterSurfaceJigs"
QT_MOC_LITERAL(44, 862, 21), // "OnMakeSideSurfaceJigs"
QT_MOC_LITERAL(45, 884, 23), // "OnMakeSolidPositionJigs"
QT_MOC_LITERAL(46, 908, 25), // "OnReviewSolidPositionJigs"
QT_MOC_LITERAL(47, 934, 18), // "OnDefineStylusJigs"
QT_MOC_LITERAL(48, 953, 18), // "OnReviewStylusJigs"
QT_MOC_LITERAL(49, 972, 25), // "OnMakeAnteriorSurfaceJigs"
QT_MOC_LITERAL(50, 998, 27), // "OnReviewAnteriorSurfaceJigs"
QT_MOC_LITERAL(51, 1026, 12), // "OnOutputJigs"
QT_MOC_LITERAL(52, 1039, 21), // "OnAdvancedControlJigs"
QT_MOC_LITERAL(53, 1061, 23), // "OnMeasureScreenDistance"
QT_MOC_LITERAL(54, 1085, 20), // "OnMeasureScreenAngle"
QT_MOC_LITERAL(55, 1106, 21), // "OnMeasureScreenRadius"
QT_MOC_LITERAL(56, 1128, 23), // "OnMeasureEntityDistance"
QT_MOC_LITERAL(57, 1152, 20), // "OnMeasureEntityAngle"
QT_MOC_LITERAL(58, 1173, 20), // "OnMeasureCurveRadius"
QT_MOC_LITERAL(59, 1194, 16), // "OnAnalyzeImplant"
QT_MOC_LITERAL(60, 1211, 17), // "OnValidateImplant"
QT_MOC_LITERAL(61, 1229, 16), // "displayReportDlg"
QT_MOC_LITERAL(62, 1246, 24), // "OnAcceptValidationReport"
QT_MOC_LITERAL(63, 1271, 23), // "OnPrintValidationReport"
QT_MOC_LITERAL(64, 1295, 14), // "outputFileName"
QT_MOC_LITERAL(65, 1310, 27), // "GetValidationReportTextEdit"
QT_MOC_LITERAL(66, 1338, 10), // "QTextEdit*"
QT_MOC_LITERAL(67, 1349, 14), // "OnValidateJigs"
QT_MOC_LITERAL(68, 1364, 28), // "OnAcceptValidationReportJigs"
QT_MOC_LITERAL(69, 1393, 27), // "OnPrintValidationReportJigs"
QT_MOC_LITERAL(70, 1421, 31), // "GetValidationReportJigsTextEdit"
QT_MOC_LITERAL(71, 1453, 10), // "OnFileInfo"
QT_MOC_LITERAL(72, 1464, 13), // "OnPatientInfo"
QT_MOC_LITERAL(73, 1478, 30), // "OnDetermineViewingNormalRegion"
QT_MOC_LITERAL(74, 1509, 19), // "OnAcceptThisManager"
QT_MOC_LITERAL(75, 1529, 9), // "CManager*"
QT_MOC_LITERAL(76, 1539, 11), // "thisManager"
QT_MOC_LITERAL(77, 1551, 29), // "OnAcceptAnalyzeImplantManager"
QT_MOC_LITERAL(78, 1581, 27), // "OnActivateNextReviewManager"
QT_MOC_LITERAL(79, 1609, 19), // "pManagerToBeDeleted"
QT_MOC_LITERAL(80, 1629, 8), // "CEntRole"
QT_MOC_LITERAL(81, 1638, 5), // "eRole"
QT_MOC_LITERAL(82, 1644, 5), // "bNext"
QT_MOC_LITERAL(83, 1650, 31), // "OnActivateNextJigsReviewManager"
QT_MOC_LITERAL(84, 1682, 22), // "OnGoToPSFeaturesReview"
QT_MOC_LITERAL(85, 1705, 19), // "OnCancelThisManager"
QT_MOC_LITERAL(86, 1725, 30), // "OnCancelAllMeasurementManagers"
QT_MOC_LITERAL(87, 1756, 32), // "OnSetModifiedMarkerToFemoralJigs"
QT_MOC_LITERAL(88, 1789, 11) // "OnDoNothing"

    },
    "CTotalView\0OnViewFront\0\0OnViewBack\0"
    "OnViewTop\0OnViewBottom\0OnViewLeft\0"
    "OnViewRight\0OnViewFrontIso\0OnViewBackIso\0"
    "OnImportFemurSurface\0CManagerActivateType\0"
    "manActType\0OnMakeFemoralAxes\0"
    "OnReviewFemoralAxes\0OnHandleFemoralAxes\0"
    "OnAutoInitializeAllSteps\0"
    "OnAutoInitializeJCOSteps\0"
    "calledFromInitializeAllSteps\0"
    "OnAutoRegenerateJCOSteps\0OnAutoRefineJCOSteps\0"
    "OnAutoInitializeFromFemoralAxes\0"
    "OnManualDefaultJCOSteps\0OnDefineOutlineProfile\0"
    "OnReviewOutlineProfile\0OnCutFemur\0"
    "OnReviewFemoralCuts\0OnHandleFillets\0"
    "OnAutoInitializeFPCSteps\0OnAdvancedControl\0"
    "OnTest1\0OnImportOsteophyteSurface\0"
    "OnHandleCartilage\0OnAutoInitializeAllJigsSteps\0"
    "OnAutoInitializeDrivenJigsSteps\0"
    "OnDefineCartilage\0OnReviewCartilage\0"
    "OnDefineOutlineProfileJigs\0"
    "OnReviewOutlineProfileJigs\0"
    "OnHandleOuterSurfaceJigs\0"
    "OnMakeInnerSurfaceJigs\0OnReviewInnerSurfaceJigs\0"
    "OnMakeOuterSurfaceJigs\0OnReviewOuterSurfaceJigs\0"
    "OnMakeSideSurfaceJigs\0OnMakeSolidPositionJigs\0"
    "OnReviewSolidPositionJigs\0OnDefineStylusJigs\0"
    "OnReviewStylusJigs\0OnMakeAnteriorSurfaceJigs\0"
    "OnReviewAnteriorSurfaceJigs\0OnOutputJigs\0"
    "OnAdvancedControlJigs\0OnMeasureScreenDistance\0"
    "OnMeasureScreenAngle\0OnMeasureScreenRadius\0"
    "OnMeasureEntityDistance\0OnMeasureEntityAngle\0"
    "OnMeasureCurveRadius\0OnAnalyzeImplant\0"
    "OnValidateImplant\0displayReportDlg\0"
    "OnAcceptValidationReport\0"
    "OnPrintValidationReport\0outputFileName\0"
    "GetValidationReportTextEdit\0QTextEdit*\0"
    "OnValidateJigs\0OnAcceptValidationReportJigs\0"
    "OnPrintValidationReportJigs\0"
    "GetValidationReportJigsTextEdit\0"
    "OnFileInfo\0OnPatientInfo\0"
    "OnDetermineViewingNormalRegion\0"
    "OnAcceptThisManager\0CManager*\0thisManager\0"
    "OnAcceptAnalyzeImplantManager\0"
    "OnActivateNextReviewManager\0"
    "pManagerToBeDeleted\0CEntRole\0eRole\0"
    "bNext\0OnActivateNextJigsReviewManager\0"
    "OnGoToPSFeaturesReview\0OnCancelThisManager\0"
    "OnCancelAllMeasurementManagers\0"
    "OnSetModifiedMarkerToFemoralJigs\0"
    "OnDoNothing"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CTotalView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
     110,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  564,    2, 0x0a /* Public */,
       3,    0,  565,    2, 0x0a /* Public */,
       4,    0,  566,    2, 0x0a /* Public */,
       5,    0,  567,    2, 0x0a /* Public */,
       6,    0,  568,    2, 0x0a /* Public */,
       7,    0,  569,    2, 0x0a /* Public */,
       8,    0,  570,    2, 0x0a /* Public */,
       9,    0,  571,    2, 0x0a /* Public */,
      10,    1,  572,    2, 0x0a /* Public */,
      10,    0,  575,    2, 0x2a /* Public | MethodCloned */,
      13,    1,  576,    2, 0x0a /* Public */,
      13,    0,  579,    2, 0x2a /* Public | MethodCloned */,
      14,    0,  580,    2, 0x0a /* Public */,
      15,    1,  581,    2, 0x0a /* Public */,
      15,    0,  584,    2, 0x2a /* Public | MethodCloned */,
      16,    1,  585,    2, 0x0a /* Public */,
      16,    0,  588,    2, 0x2a /* Public | MethodCloned */,
      17,    1,  589,    2, 0x0a /* Public */,
      17,    0,  592,    2, 0x2a /* Public | MethodCloned */,
      19,    0,  593,    2, 0x0a /* Public */,
      20,    0,  594,    2, 0x0a /* Public */,
      21,    0,  595,    2, 0x0a /* Public */,
      22,    0,  596,    2, 0x0a /* Public */,
      23,    1,  597,    2, 0x0a /* Public */,
      23,    0,  600,    2, 0x2a /* Public | MethodCloned */,
      24,    0,  601,    2, 0x0a /* Public */,
      25,    1,  602,    2, 0x0a /* Public */,
      25,    0,  605,    2, 0x2a /* Public | MethodCloned */,
      26,    0,  606,    2, 0x0a /* Public */,
      27,    1,  607,    2, 0x0a /* Public */,
      27,    0,  610,    2, 0x2a /* Public | MethodCloned */,
      28,    1,  611,    2, 0x0a /* Public */,
      28,    0,  614,    2, 0x2a /* Public | MethodCloned */,
      29,    0,  615,    2, 0x0a /* Public */,
      30,    0,  616,    2, 0x0a /* Public */,
      31,    1,  617,    2, 0x0a /* Public */,
      31,    0,  620,    2, 0x2a /* Public | MethodCloned */,
      32,    1,  621,    2, 0x0a /* Public */,
      32,    0,  624,    2, 0x2a /* Public | MethodCloned */,
      33,    1,  625,    2, 0x0a /* Public */,
      33,    0,  628,    2, 0x2a /* Public | MethodCloned */,
      34,    1,  629,    2, 0x0a /* Public */,
      34,    0,  632,    2, 0x2a /* Public | MethodCloned */,
      35,    1,  633,    2, 0x0a /* Public */,
      35,    0,  636,    2, 0x2a /* Public | MethodCloned */,
      36,    0,  637,    2, 0x0a /* Public */,
      37,    1,  638,    2, 0x0a /* Public */,
      37,    0,  641,    2, 0x2a /* Public | MethodCloned */,
      38,    0,  642,    2, 0x0a /* Public */,
      39,    1,  643,    2, 0x0a /* Public */,
      39,    0,  646,    2, 0x2a /* Public | MethodCloned */,
      40,    1,  647,    2, 0x0a /* Public */,
      40,    0,  650,    2, 0x2a /* Public | MethodCloned */,
      41,    0,  651,    2, 0x0a /* Public */,
      42,    1,  652,    2, 0x0a /* Public */,
      42,    0,  655,    2, 0x2a /* Public | MethodCloned */,
      43,    0,  656,    2, 0x0a /* Public */,
      44,    1,  657,    2, 0x0a /* Public */,
      44,    0,  660,    2, 0x2a /* Public | MethodCloned */,
      45,    1,  661,    2, 0x0a /* Public */,
      45,    0,  664,    2, 0x2a /* Public | MethodCloned */,
      46,    0,  665,    2, 0x0a /* Public */,
      47,    1,  666,    2, 0x0a /* Public */,
      47,    0,  669,    2, 0x2a /* Public | MethodCloned */,
      48,    0,  670,    2, 0x0a /* Public */,
      49,    1,  671,    2, 0x0a /* Public */,
      49,    0,  674,    2, 0x2a /* Public | MethodCloned */,
      50,    0,  675,    2, 0x0a /* Public */,
      51,    0,  676,    2, 0x0a /* Public */,
      52,    0,  677,    2, 0x0a /* Public */,
      53,    0,  678,    2, 0x0a /* Public */,
      54,    0,  679,    2, 0x0a /* Public */,
      55,    0,  680,    2, 0x0a /* Public */,
      56,    0,  681,    2, 0x0a /* Public */,
      57,    0,  682,    2, 0x0a /* Public */,
      58,    0,  683,    2, 0x0a /* Public */,
      59,    1,  684,    2, 0x0a /* Public */,
      59,    0,  687,    2, 0x2a /* Public | MethodCloned */,
      60,    2,  688,    2, 0x0a /* Public */,
      60,    1,  693,    2, 0x2a /* Public | MethodCloned */,
      60,    0,  696,    2, 0x2a /* Public | MethodCloned */,
      62,    0,  697,    2, 0x0a /* Public */,
      63,    1,  698,    2, 0x0a /* Public */,
      63,    0,  701,    2, 0x2a /* Public | MethodCloned */,
      65,    0,  702,    2, 0x0a /* Public */,
      67,    2,  703,    2, 0x0a /* Public */,
      67,    1,  708,    2, 0x2a /* Public | MethodCloned */,
      67,    0,  711,    2, 0x2a /* Public | MethodCloned */,
      68,    0,  712,    2, 0x0a /* Public */,
      69,    1,  713,    2, 0x0a /* Public */,
      69,    0,  716,    2, 0x2a /* Public | MethodCloned */,
      70,    0,  717,    2, 0x0a /* Public */,
      71,    0,  718,    2, 0x0a /* Public */,
      72,    0,  719,    2, 0x0a /* Public */,
      73,    0,  720,    2, 0x0a /* Public */,
      74,    1,  721,    2, 0x0a /* Public */,
      77,    0,  724,    2, 0x0a /* Public */,
      78,    3,  725,    2, 0x0a /* Public */,
      78,    2,  732,    2, 0x2a /* Public | MethodCloned */,
      78,    1,  737,    2, 0x2a /* Public | MethodCloned */,
      78,    0,  740,    2, 0x2a /* Public | MethodCloned */,
      83,    3,  741,    2, 0x0a /* Public */,
      83,    2,  748,    2, 0x2a /* Public | MethodCloned */,
      83,    1,  753,    2, 0x2a /* Public | MethodCloned */,
      83,    0,  756,    2, 0x2a /* Public | MethodCloned */,
      84,    0,  757,    2, 0x0a /* Public */,
      85,    1,  758,    2, 0x0a /* Public */,
      86,    0,  761,    2, 0x0a /* Public */,
      87,    0,  762,    2, 0x0a /* Public */,
      88,    0,  763,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::Bool,   18,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Bool,   12,   61,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   64,
    QMetaType::Void,
    0x80000000 | 66,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Bool,   12,   61,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   64,
    QMetaType::Void,
    0x80000000 | 66,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 75,   76,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 75, 0x80000000 | 80, QMetaType::Bool,   79,   81,   82,
    QMetaType::Void, 0x80000000 | 75, 0x80000000 | 80,   79,   81,
    QMetaType::Void, 0x80000000 | 75,   79,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 75, 0x80000000 | 80, QMetaType::Bool,   79,   81,   82,
    QMetaType::Void, 0x80000000 | 75, 0x80000000 | 80,   79,   81,
    QMetaType::Void, 0x80000000 | 75,   79,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 75,   76,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CTotalView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CTotalView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnViewFront(); break;
        case 1: _t->OnViewBack(); break;
        case 2: _t->OnViewTop(); break;
        case 3: _t->OnViewBottom(); break;
        case 4: _t->OnViewLeft(); break;
        case 5: _t->OnViewRight(); break;
        case 6: _t->OnViewFrontIso(); break;
        case 7: _t->OnViewBackIso(); break;
        case 8: _t->OnImportFemurSurface((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 9: _t->OnImportFemurSurface(); break;
        case 10: _t->OnMakeFemoralAxes((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 11: _t->OnMakeFemoralAxes(); break;
        case 12: _t->OnReviewFemoralAxes(); break;
        case 13: _t->OnHandleFemoralAxes((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 14: _t->OnHandleFemoralAxes(); break;
        case 15: _t->OnAutoInitializeAllSteps((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 16: _t->OnAutoInitializeAllSteps(); break;
        case 17: { bool _r = _t->OnAutoInitializeJCOSteps((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 18: { bool _r = _t->OnAutoInitializeJCOSteps();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 19: _t->OnAutoRegenerateJCOSteps(); break;
        case 20: _t->OnAutoRefineJCOSteps(); break;
        case 21: _t->OnAutoInitializeFromFemoralAxes(); break;
        case 22: _t->OnManualDefaultJCOSteps(); break;
        case 23: _t->OnDefineOutlineProfile((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 24: _t->OnDefineOutlineProfile(); break;
        case 25: _t->OnReviewOutlineProfile(); break;
        case 26: _t->OnCutFemur((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 27: _t->OnCutFemur(); break;
        case 28: _t->OnReviewFemoralCuts(); break;
        case 29: _t->OnHandleFillets((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 30: _t->OnHandleFillets(); break;
        case 31: _t->OnAutoInitializeFPCSteps((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 32: _t->OnAutoInitializeFPCSteps(); break;
        case 33: _t->OnAdvancedControl(); break;
        case 34: _t->OnTest1(); break;
        case 35: _t->OnImportOsteophyteSurface((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 36: _t->OnImportOsteophyteSurface(); break;
        case 37: _t->OnHandleCartilage((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 38: _t->OnHandleCartilage(); break;
        case 39: _t->OnAutoInitializeAllJigsSteps((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 40: _t->OnAutoInitializeAllJigsSteps(); break;
        case 41: _t->OnAutoInitializeDrivenJigsSteps((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 42: _t->OnAutoInitializeDrivenJigsSteps(); break;
        case 43: _t->OnDefineCartilage((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 44: _t->OnDefineCartilage(); break;
        case 45: _t->OnReviewCartilage(); break;
        case 46: _t->OnDefineOutlineProfileJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 47: _t->OnDefineOutlineProfileJigs(); break;
        case 48: _t->OnReviewOutlineProfileJigs(); break;
        case 49: _t->OnHandleOuterSurfaceJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 50: _t->OnHandleOuterSurfaceJigs(); break;
        case 51: _t->OnMakeInnerSurfaceJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 52: _t->OnMakeInnerSurfaceJigs(); break;
        case 53: _t->OnReviewInnerSurfaceJigs(); break;
        case 54: _t->OnMakeOuterSurfaceJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 55: _t->OnMakeOuterSurfaceJigs(); break;
        case 56: _t->OnReviewOuterSurfaceJigs(); break;
        case 57: _t->OnMakeSideSurfaceJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 58: _t->OnMakeSideSurfaceJigs(); break;
        case 59: _t->OnMakeSolidPositionJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 60: _t->OnMakeSolidPositionJigs(); break;
        case 61: _t->OnReviewSolidPositionJigs(); break;
        case 62: _t->OnDefineStylusJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 63: _t->OnDefineStylusJigs(); break;
        case 64: _t->OnReviewStylusJigs(); break;
        case 65: _t->OnMakeAnteriorSurfaceJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 66: _t->OnMakeAnteriorSurfaceJigs(); break;
        case 67: _t->OnReviewAnteriorSurfaceJigs(); break;
        case 68: _t->OnOutputJigs(); break;
        case 69: _t->OnAdvancedControlJigs(); break;
        case 70: _t->OnMeasureScreenDistance(); break;
        case 71: _t->OnMeasureScreenAngle(); break;
        case 72: _t->OnMeasureScreenRadius(); break;
        case 73: _t->OnMeasureEntityDistance(); break;
        case 74: _t->OnMeasureEntityAngle(); break;
        case 75: _t->OnMeasureCurveRadius(); break;
        case 76: _t->OnAnalyzeImplant((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 77: _t->OnAnalyzeImplant(); break;
        case 78: _t->OnValidateImplant((*reinterpret_cast< CManagerActivateType(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 79: _t->OnValidateImplant((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 80: _t->OnValidateImplant(); break;
        case 81: _t->OnAcceptValidationReport(); break;
        case 82: _t->OnPrintValidationReport((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 83: _t->OnPrintValidationReport(); break;
        case 84: { QTextEdit* _r = _t->GetValidationReportTextEdit();
            if (_a[0]) *reinterpret_cast< QTextEdit**>(_a[0]) = std::move(_r); }  break;
        case 85: _t->OnValidateJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 86: _t->OnValidateJigs((*reinterpret_cast< CManagerActivateType(*)>(_a[1]))); break;
        case 87: _t->OnValidateJigs(); break;
        case 88: _t->OnAcceptValidationReportJigs(); break;
        case 89: _t->OnPrintValidationReportJigs((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 90: _t->OnPrintValidationReportJigs(); break;
        case 91: { QTextEdit* _r = _t->GetValidationReportJigsTextEdit();
            if (_a[0]) *reinterpret_cast< QTextEdit**>(_a[0]) = std::move(_r); }  break;
        case 92: _t->OnFileInfo(); break;
        case 93: _t->OnPatientInfo(); break;
        case 94: _t->OnDetermineViewingNormalRegion(); break;
        case 95: _t->OnAcceptThisManager((*reinterpret_cast< CManager*(*)>(_a[1]))); break;
        case 96: _t->OnAcceptAnalyzeImplantManager(); break;
        case 97: _t->OnActivateNextReviewManager((*reinterpret_cast< CManager*(*)>(_a[1])),(*reinterpret_cast< CEntRole(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 98: _t->OnActivateNextReviewManager((*reinterpret_cast< CManager*(*)>(_a[1])),(*reinterpret_cast< CEntRole(*)>(_a[2]))); break;
        case 99: _t->OnActivateNextReviewManager((*reinterpret_cast< CManager*(*)>(_a[1]))); break;
        case 100: _t->OnActivateNextReviewManager(); break;
        case 101: _t->OnActivateNextJigsReviewManager((*reinterpret_cast< CManager*(*)>(_a[1])),(*reinterpret_cast< CEntRole(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 102: _t->OnActivateNextJigsReviewManager((*reinterpret_cast< CManager*(*)>(_a[1])),(*reinterpret_cast< CEntRole(*)>(_a[2]))); break;
        case 103: _t->OnActivateNextJigsReviewManager((*reinterpret_cast< CManager*(*)>(_a[1]))); break;
        case 104: _t->OnActivateNextJigsReviewManager(); break;
        case 105: _t->OnGoToPSFeaturesReview(); break;
        case 106: _t->OnCancelThisManager((*reinterpret_cast< CManager*(*)>(_a[1]))); break;
        case 107: _t->OnCancelAllMeasurementManagers(); break;
        case 108: _t->OnSetModifiedMarkerToFemoralJigs(); break;
        case 109: _t->OnDoNothing(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CTotalView::staticMetaObject = { {
    QMetaObject::SuperData::link<COpenGLView::staticMetaObject>(),
    qt_meta_stringdata_CTotalView.data,
    qt_meta_data_CTotalView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CTotalView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CTotalView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CTotalView.stringdata0))
        return static_cast<void*>(this);
    return COpenGLView::qt_metacast(_clname);
}

int CTotalView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = COpenGLView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 110)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 110;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 110)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 110;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
