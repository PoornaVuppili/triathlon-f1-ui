#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )

#include "TotalDoc.h"
#include "FemoralCuts.h"
#include "OutlineProfile.h"
#include "FemoralAxes.h"
//#include "JCurves.h"
#include "..\KAppTotal\Utilities.h"
#include "ValidateOutlineProfile.h"
#include "OutlineProfileMgrUI3D.h"
#include "DefineOutlineProfile.h"

namespace Validation
{
	namespace OutlineProfile
	{
		///////////////////////////////////////////////////////////////////////
		// This function validates users settings.
		///////////////////////////////////////////////////////////////////////
		CEntValidateStatus Validate
		(
			CTotalDoc* pDoc,							// I:
			QString& message,							// O: error messages
			IwTArray<IwPoint3d>& unqualifiedLocations	// O: unqualified locations
		)
		{
			pDoc->AppendLog(QString("Validation::OutlineProfile::Validate()"));

			pDoc->AppendValidationReportEntry("\n");
			pDoc->AppendValidationReportEntry("**** Outline Profile ****");

			// initialize data
			message.clear();
			int vStatus = 0;
			unqualifiedLocations.RemoveAll();
			IwTArray<double> unqualifiedDistances;

			double allowableDistMax = pDoc->GetVarTableValue("OUTLINE PROFILE MAX DISTANCE TO CUT EDGE");
			double allowableDistMin = pDoc->GetVarTableValue("OUTLINE PROFILE MIN DISTANCE TO CUT EDGE");
			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE DISTANCE TO CUT PROFILE STATUS") != -1)
			{
				IwTArray<IwPoint3d> thisUnQualifiedLocations;
				IwTArray<double> thisUnQualifiedDistances;
				IwTArray<int> thisOverhangings;
				bool closeEnoughToEdge = ValidateDistanceToCutProfile(pDoc, allowableDistMax, allowableDistMin, thisUnQualifiedLocations, thisUnQualifiedDistances, thisOverhangings);
				unqualifiedLocations.Append(thisUnQualifiedLocations);
				unqualifiedDistances.Append(thisUnQualifiedDistances);
				if (!closeEnoughToEdge)
				{
					pDoc->AppendValidateMessage(QString("OUTLINE PROFILE DISTANCE TO CUT PROFILE"), vStatus, message);
					message += QString("\n     on Faces: ");
					int oSize = (int)thisOverhangings.GetSize();
					if (pDoc->IsPositiveSideLateral())
					{
						for (int i = oSize - 2; i > -1; i -= 2)// decrease 2
						{
							if (thisOverhangings.GetAt(i + 1) > 0) // underhanging
								message += QString("uM%1 ").arg((12 - i) / 2);
							else if (thisOverhangings.GetAt(i + 1) < 0) // overhanging
								message += QString("oM%1 ").arg((12 - i) / 2);
						}
						for (int i = oSize - 2; i > -1; i -= 2)// decrease 2
						{
							if (thisOverhangings.GetAt(i) > 0) // underhanging
								message += QString("uL%1 ").arg((12 - i) / 2);
							else if (thisOverhangings.GetAt(i) < 0) // overhanging
								message += QString("oL%1 ").arg((12 - i) / 2);
						}
					}
					else
					{
						for (int i = oSize - 2; i > -1; i -= 2)// decrease 2
						{
							if (thisOverhangings.GetAt(i) > 0) // underhanging.
								message += QString("uM%1 ").arg((12 - i) / 2);
							else if (thisOverhangings.GetAt(i) < 0) // overhanging.
								message += QString("oM%1 ").arg((12 - i) / 2);
						}
						for (int i = oSize - 2; i > -1; i -= 2)// decrease 2
						{
							if (thisOverhangings.GetAt(i + 1) > 0) // underhanging
								message += QString("uL%1 ").arg((12 - i) / 2);
							else if (thisOverhangings.GetAt(i + 1) < 0) // overhanging
								message += QString("oL%1 ").arg((12 - i) / 2);
						}
					}
				}
			}

			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE LATERAL ANTERIOR DISTANCE TO CUT PROFILE STATUS") != -1)
			{
				IwTArray<IwPoint3d> thisUnQualifiedLocations;
				IwTArray<double> thisUnQualifiedDistances;
				bool insideLateralAntCut = ValidateDistanceToLateralAntCutProfile(pDoc, thisUnQualifiedLocations, thisUnQualifiedDistances);
				unqualifiedLocations.Append(thisUnQualifiedLocations);
				unqualifiedDistances.Append(thisUnQualifiedDistances);
				if (!insideLateralAntCut)
				{
					pDoc->AppendValidateMessage(QString("OUTLINE PROFILE LATERAL ANTERIOR DISTANCE TO CUT PROFILE"), vStatus, message);
				}
			}
			if (unqualifiedLocations.GetSize() > 0)
			{
				QString msg = QString("Fail: The distance from outline profile to cut profile (%1 ~ %2mm) is not valid.").arg(allowableDistMin, 0, 'f', 2).arg(allowableDistMax, 0, 'f', 2);
				pDoc->AppendValidationReportEntry(msg, false);
			}
			else
			{
				QString msg = QString("Pass: The distance from outline profile to cut profile (%1 ~ %2mm) is valid.").arg(allowableDistMin, 0, 'f', 2).arg(allowableDistMax, 0, 'f', 2);
				pDoc->AppendValidationReportEntry(msg, true);
			}
			// Determine the overhanging
			SortIwTArray(unqualifiedDistances);
			if (unqualifiedDistances.GetSize() > 0 && unqualifiedDistances.GetAt(0) < 0)
			{
				QString msg = QString("Fail: The outline profile is overhanging on peripheral edges. A supervisor signature is needed."); // NOTE - do NOT change the wording. It needs to be synchronized with CTotalView::OnValidateImplant()
				pDoc->AppendValidationReportEntry(msg, false);
			}
			else
			{
				QString msg = QString("Pass: The outline profile is not overhanging on peripheral edges.");
				pDoc->AppendValidationReportEntry(msg, true);
			}
			//
			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE MEDIAL ANTERIOR PROFILE STATUS") != -1)
			{
				bool passRefCircle = ValidateMedialAnteriorProfile(pDoc);
				if (!passRefCircle)
				{
					pDoc->AppendValidateMessage(QString("OUTLINE PROFILE MEDIAL ANTERIOR PROFILE"), vStatus, message);
					pDoc->AppendValidationReportEntry(QString("Fail: Medial anterior profile does not pass reference circle."), false);
				}
				else
				{
					pDoc->AppendValidationReportEntry(QString("Pass: Medial anterior profile passes reference circle."));
				}
			}

			//
			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE PATELLA COVERAGE STATUS") != -1)
			{
				bool enoughPatellaCoverage = ValidatePatellaCoverage(pDoc);
				if (!enoughPatellaCoverage)
				{
					pDoc->AppendValidateMessage(QString("OUTLINE PROFILE PATELLA COVERAGE"), vStatus, message);
					pDoc->AppendValidationReportEntry(QString("Fail: Anterior outline profile does not provide at least 1/2 patella coverage."), false);
				}
				else
				{
					pDoc->AppendValidationReportEntry(QString("Pass: Anterior outline profile provides at least 1/2 patella coverage."));
				}
			}

			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE ML SIZE STATUS") != -1)
			{
				bool goodMLSize = ValidateMLSize(pDoc);
				if (!goodMLSize)
				{
					pDoc->AppendValidateMessage(QString("OUTLINE PROFILE ML SIZE"), vStatus, message);
				}
			}

			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE NOTCH TIP DISTANCE TO ORIGIN STATUS") != -1 ||
				pDoc->GetValidateTableStatus("OUTLINE PROFILE NOTCH EIGHTEEN WIDTH STATUS") != -1 ||
				pDoc->GetValidateTableStatus("OUTLINE PROFILE NOTCH LEANING MEDIALLY STATUS") != -1)
			{
				bool goodNotchTipDistance, goodNotchWidth, goodLeaningMedially;
				double tipDist, eighteenWidth;
				ValidateNotchProfile(pDoc, goodNotchTipDistance, goodNotchWidth, goodLeaningMedially, tipDist, eighteenWidth);

				if (tipDist > 0)
				{
					double floorValue = floor(100.0 * tipDist + 0.5) / 100.0;
					QString VRmessageNTD = QString("Pass: The distance (7mm ~ 12mm) from notch profile apex to femoral axis is valid: %1mm").arg(floorValue, 0, 'f', 2);
					if (!goodNotchTipDistance)
					{
						pDoc->AppendValidateMessage(QString("OUTLINE PROFILE NOTCH TIP DISTANCE TO ORIGIN"), vStatus, message);
						VRmessageNTD = QString("Fail: The distance (7mm ~ 12mm) from notch profile apex to femoral axis is not valid: %1mm").arg(floorValue, 0, 'f', 2);
					}
					pDoc->AppendValidationReportEntry(VRmessageNTD, goodNotchTipDistance);
				}
				else
				{
					if (!message.isEmpty())
						message += QString("\n");
					message += QString("Software validation is not reliable. Please proceed manual checking for notch distance to femoral origin.");
					vStatus = max(vStatus, 2);
					pDoc->AppendValidationReportEntry("Software validation is not reliable. Please proceed manual checking for notch distance to femoral axis.", false);
				}

				if (eighteenWidth > 0)
				{
					double floorValue = floor(100.0 * eighteenWidth + 0.5) / 100.0;
					QString VRmessageNW = QString("Pass: Max notch width of 18mm at 120 degree flexion is valid: %1mm").arg(floorValue, 0, 'f', 2);
					if (!goodNotchWidth)
					{
						pDoc->AppendValidateMessage(QString("OUTLINE PROFILE NOTCH EIGHTEEN WIDTH"), vStatus, message);
						VRmessageNW = QString("Fail: Max notch width of 18mm at 120 degree flexion is not valid: %1mm").arg(floorValue, 0, 'f', 2);
					}
					pDoc->AppendValidationReportEntry(VRmessageNW, goodNotchWidth);
				}
				else
				{
					if (!message.isEmpty())
						message += QString("\n");
					message += QString("Software validation is not reliable. Please proceed manual checking for notch 18mm width.");
					vStatus = max(vStatus, 2);
					pDoc->AppendValidationReportEntry("Software validation is not reliable. Please proceed manual checking for notch 18mm width.", false);
				}

				if (eighteenWidth > 0 && tipDist > 0)
				{
					if (!goodLeaningMedially)
					{
						pDoc->AppendValidateMessage(QString("OUTLINE PROFILE NOTCH LEANING MEDIALLY"), vStatus, message);
					}
				}
				else
				{
					if (!message.isEmpty())
						message += QString("\n");
					message += QString("Software validation is not reliable. Please proceed manual checking for leaning medially.");
					vStatus = max(vStatus, 2);
				}

			}

			if (pDoc->GetValidateTableStatus("OUTLINE PROFILE ANTERIOR EAR RADIUS STATUS") != -1)
			{
				double minEarRadius = ValidateAnteriorEarProfile(pDoc);
				if (minEarRadius > 0 && minEarRadius < 5.95)// hard coded ~"6.0". Needs to synchronize with OutlineProfileMgrModel::ConvertToArc()
				{
					pDoc->AppendValidateMessage(QString("OUTLINE PROFILE ANTERIOR EAR RADIUS"), vStatus, message);
					pDoc->AppendValidationReportEntry(QString("Fail: There is sharp transition in curvature."), false);
				}
				else
				{
					pDoc->AppendValidationReportEntry(QString("Pass: There is no sharp transition in curvature."), true);
				}

			}

			if (vStatus == 0)
			{
				message = QString("All pass.");
			}

			return (CEntValidateStatus)vStatus;
		}

		bool ValidateDistanceToCutProfile
		(
			CTotalDoc* pDoc,							// I:
			double allowableDistMax,					// I:
			double allowableDistMin,					// I:
			IwTArray<IwPoint3d>& unqualifiedLocations,	// O:
			IwTArray<double>& unqualifiedDistances,		// O:
			IwTArray<int>& overhangings					// O: for 0~7th faces, [8]:8th face positive side, [9]:8th face negative side (total 0~9=10), 
														//    0:good, >0:underhanging, <0:overhanging (same definition as distance)
		)
		{
			bool closeToCutProfile = true;
			// initialize 
			unqualifiedLocations.RemoveAll();
			unqualifiedDistances.RemoveAll();
			overhangings.SetSize(10);
			for (int i = 0; i < 10; i++)
				overhangings.SetAt(i, 0);// all good.

			if (pDoc->GetOutlineProfile() == NULL)
				return closeToCutProfile;
			if (pDoc->GetFemoralCuts() == NULL)
				return closeToCutProfile;

			// Determine the cut feature points
			IwTArray<IwPoint3d> cutFeaturePoints;
			pDoc->GetFemoralCuts()->GetFeaturePoints(cutFeaturePoints, NULL);
			if (cutFeaturePoints.GetSize() == 0)
				return closeToCutProfile;

			// Determine the sketch surface
			IwBSplineSurface* sketchSurface;
			pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);

			IwTArray<IwPoint3d> subUnQualifiedLocations;
			IwTArray<double> subUnQualifiedDistances;

			int faceIndex = 0;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 0;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 1;
			IwTArray<IwPoint3d> subUnQualifiedLocations1;
			IwTArray<double> subUnQualifiedDistances1;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 1;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 2;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 3;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 4;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 5;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 6;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 7;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 8;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(8, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(8, -1); // underhanging
			}

			faceIndex = 8;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(9, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(9, -1); // underhanging
			}

			if (unqualifiedLocations.GetSize() == 0)
				return true;
			else
				return false;
		}

		bool ValidateDistanceToBothPosteriorCutProfiles
		(
			CTotalDoc* pDoc,							// I:
			double allowableDistMax,					// I:
			double allowableDistMin,					// I:
			IwTArray<IwPoint3d>& unqualifiedLocations,	// O:
			IwTArray<double>& unqualifiedDistances,		// O:
			IwTArray<int>& overhangings					// O: for 0~1th faces, 0:good, >0:underhanging, <0:overhanging (same definition as distance)
		)
		{
			bool closeToCutProfile = true;
			// initialize 
			unqualifiedLocations.RemoveAll();
			unqualifiedDistances.RemoveAll();
			overhangings.SetSize(2);
			for (int i = 0; i < 2; i++)
				overhangings.SetAt(i, 0);// all good.

			if (pDoc->GetOutlineProfile() == NULL)
				return closeToCutProfile;
			if (pDoc->GetFemoralCuts() == NULL)
				return closeToCutProfile;

			// Determine the cut feature points
			IwTArray<IwPoint3d> cutFeaturePoints;
			pDoc->GetFemoralCuts()->GetFeaturePoints(cutFeaturePoints, NULL);
			if (cutFeaturePoints.GetSize() == 0)
				return closeToCutProfile;
			// Determine the sketch surface
			IwBSplineSurface* sketchSurface;
			pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);

			IwTArray<IwPoint3d> subUnQualifiedLocations;
			IwTArray<double> subUnQualifiedDistances = NULL;

			int faceIndex = 0;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 0;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 1;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			faceIndex = 1;
			ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, subUnQualifiedLocations, subUnQualifiedDistances);
			unqualifiedLocations.Append(subUnQualifiedLocations);
			unqualifiedDistances.Append(subUnQualifiedDistances);
			if (subUnQualifiedDistances.GetSize() > 0)
			{
				SortIwTArray(subUnQualifiedDistances);// ascending
				if (subUnQualifiedDistances.GetLast() > allowableDistMax)
					overhangings.SetAt(faceIndex, 1); // overhanging
				else if (subUnQualifiedDistances.GetAt(0) < allowableDistMin)
					overhangings.SetAt(faceIndex, -1); // underhanging
			}

			if (unqualifiedLocations.GetSize() == 0)
				return true;
			else
				return false;
		}

		bool ValidateDistanceToACutFace
		(
			CTotalDoc* pDoc,							// I:
			IwTArray<IwPoint3d>& cutFeaturePoints,		// I:
			IwBSplineSurface* sketchSurface,			// I:
			IwBSplineCurve* uvCurve,					// I:
			int faceIndex,								// I:
			bool positiveSide,							// I: if a face has outline profile crossing on its two side,
														//    then determine the distance for positive or negative side.
			double allowableDistMax,					// I:
			double allowableDistMin,					// I:
			IwTArray<IwPoint3d>& unqualifiedLocations,	// O:
			IwTArray<double>& unqualifiedDistances,		// O: 
			IwVector3d* optVector						// I: If given, the distance will be determined along this direction.
														//    It is useful to determine how much movement the posterior profile can move.
		)
		{
			unqualifiedLocations.RemoveAll();
			unqualifiedDistances.RemoveAll();

			// get the adjacent faces and edges info
			IwTArray<IwFace*> cutFaces;
			IwFace* thisFace = NULL, * thisFace1 = NULL,
				* adjacentFace0 = NULL, * adjacentFace01 = NULL,
				* adjacentFace1 = NULL, * adjacentFace11 = NULL;
			int prevEdgeIndex = -1, postEdgeIndex = -1;
			IwExtent1d halfDom = IwExtent1d(0, 1);// initialize to be full domain
			if (faceIndex == 0)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex + 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				// prev edge index and post edge index
				prevEdgeIndex = 2;
				postEdgeIndex = 3;
				if (positiveSide)
					halfDom = IwExtent1d(0.0, 0.5);
				else
					halfDom = IwExtent1d(0.5, 1.0);
			}
			else if (faceIndex == 1)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex + 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				// prev edge index and post edge index
				prevEdgeIndex = 4;
				postEdgeIndex = 5;
				if (positiveSide)
					halfDom = IwExtent1d(0.0, 0.5);
				else
					halfDom = IwExtent1d(0.5, 1.0);
			}
			else if (faceIndex == 2)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex - 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				adjFaceIndex = faceIndex + 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace1 = cutFaces.GetAt(0);
				// prev edge index and post edge index
				if (positiveSide)
				{
					prevEdgeIndex = 6;
					postEdgeIndex = 2;
				}
				else
				{
					prevEdgeIndex = 3;
					postEdgeIndex = 7;
				}
			}
			else if (faceIndex == 3)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex - 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				adjFaceIndex = faceIndex + 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace1 = cutFaces.GetAt(0);
				// prev edge index and post edge index
				if (positiveSide)
				{
					prevEdgeIndex = 8;
					postEdgeIndex = 4;
				}
				else
				{
					prevEdgeIndex = 5;
					postEdgeIndex = 9;
				}
			}
			else if (faceIndex == 4)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex - 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				adjFaceIndex = faceIndex + 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace1 = cutFaces.GetAt(0);
				// prev edge index and post edge index
				prevEdgeIndex = 10;
				postEdgeIndex = 6;
			}
			else if (faceIndex == 5)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex - 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				adjFaceIndex = faceIndex + 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace1 = cutFaces.GetAt(0);
				// prev edge index and post edge index
				prevEdgeIndex = 9;
				postEdgeIndex = 13;
			}
			else if (faceIndex == 6)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex - 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				adjFaceIndex = faceIndex + 2;// face 8
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace1 = cutFaces.GetAt(0);
				if (cutFaces.GetSize() == 2)
					adjacentFace11 = cutFaces.GetAt(1);
				// prev edge index and post edge index
				prevEdgeIndex = 14;
				postEdgeIndex = 10;
			}
			else if (faceIndex == 7)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				// adjacent face
				int adjFaceIndex = faceIndex - 2;
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace0 = cutFaces.GetAt(0);
				adjFaceIndex = faceIndex + 1;// 8
				pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
				adjacentFace1 = cutFaces.GetAt(0);
				if (cutFaces.GetSize() == 2)
					adjacentFace11 = cutFaces.GetAt(1);
				// prev edge index and post edge index
				prevEdgeIndex = 13;
				postEdgeIndex = 17;
			}
			else if (faceIndex == 8)
			{
				// this face
				pDoc->GetFemoralCuts()->SearchCutFaces(faceIndex, cutFaces);
				thisFace = cutFaces.GetAt(0);
				if (cutFaces.GetSize() > 1)
					thisFace1 = cutFaces.GetAt(1);
				// prev edge index and post edge index
				if (positiveSide)
				{
					// adjacent face
					int adjFaceIndex = faceIndex - 2;//6
					pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
					adjacentFace0 = cutFaces.GetAt(0);
					adjFaceIndex = faceIndex + 1;//9
					pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
					adjacentFace1 = cutFaces.GetAt(0);
					prevEdgeIndex = 18;
					postEdgeIndex = 14;
				}
				else
				{
					// adjacent face
					int adjFaceIndex = faceIndex - 1;//7
					pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
					adjacentFace0 = cutFaces.GetAt(0);
					adjFaceIndex = faceIndex + 1;//9
					pDoc->GetFemoralCuts()->SearchCutFaces(adjFaceIndex, cutFaces);
					adjacentFace1 = cutFaces.GetAt(0);
					prevEdgeIndex = 17;
					postEdgeIndex = 19;
				}
			}

			// Determine the prev and post cut feature points
			IwPoint3d prevCutFeaturePoint = cutFeaturePoints.GetAt(prevEdgeIndex);
			IwPoint3d postCutFeaturePoint = cutFeaturePoints.GetAt(postEdgeIndex);
			// Determine the corresponding parameter on sketch surface
			IwPoint3d cPnt;
			IwPoint2d param2d;
			DistFromPointToSurface(prevCutFeaturePoint, sketchSurface, cPnt, param2d);
			IwPoint3d prevCutFeaturePointUV = IwPoint3d(param2d.x, param2d.y, 0);
			// Determine the corresponding parameter on uvCurve
			double prevParam;
			DistFromPointToCurve(prevCutFeaturePointUV, uvCurve, cPnt, prevParam);
			if (0)
			{
				IwVector3d tempPnt;
				sketchSurface->EvaluatePoint(IwPoint2d(cPnt.x, cPnt.y), tempPnt);
				ShowPoint(pDoc, tempPnt, red);
			}

			DistFromPointToSurface(postCutFeaturePoint, sketchSurface, cPnt, param2d);
			IwPoint3d postCutFeaturePointUV = IwPoint3d(param2d.x, param2d.y, 0);
			double postParam;
			DistFromPointToCurve(postCutFeaturePointUV, uvCurve, cPnt, postParam);
			if (0)
			{
				IwVector3d tempPnt;
				sketchSurface->EvaluatePoint(IwPoint2d(cPnt.x, cPnt.y), tempPnt);
				ShowPoint(pDoc, tempPnt, blue);
			}
			// Check whether we only need half domain or the full domain
			if (halfDom.GetMax() != 1 || halfDom.GetMin() != 0)
			{
				IwExtent1d newDom = IwExtent1d(prevParam, postParam);
				prevParam = newDom.Evaluate(halfDom.GetMin());
				postParam = newDom.Evaluate(halfDom.GetMax());
			}

			// Sample the curve in between
			double length;
			uvCurve->Length(IwExtent1d(prevParam, postParam), 0.01, length);
			int sampleNo = (int)(length / 3.0) + 1;
			IwTArray<IwPoint3d> sPnts;
			uvCurve->EquallySpacedPoints(prevParam, postParam, sampleNo, 0.01, &sPnts, NULL);

			// Extrude a line from sampling point to intersect the face
			for (unsigned i = 0; i < sPnts.GetSize(); i++)
			{
				IwPoint2d uv = IwPoint2d(sPnts.GetAt(i).x, sPnts.GetAt(i).y);
				IwVector3d pnt, normal;
				sketchSurface->EvaluateNormal(uv, FALSE, FALSE, normal);
				sketchSurface->EvaluatePoint(uv, pnt);
				IwFace* intFace = thisFace;
				IwPoint3d intPnt;
				bool bInt = IntersectFaceByLine(thisFace, pnt, normal, intPnt);
				// If no intersection, try thisFace1
				if (!bInt && thisFace1)
				{
					bInt = IntersectFaceByLine(thisFace1, pnt, normal, intPnt);
					intFace = thisFace1;
				}
				// If no intersection, try adjacent faces
				if (!bInt && adjacentFace0)
				{
					bInt = IntersectFaceByLine(adjacentFace0, pnt, normal, intPnt);
					intFace = adjacentFace0;
				}
				if (!bInt && adjacentFace01)
				{
					bInt = IntersectFaceByLine(adjacentFace01, pnt, normal, intPnt);
					intFace = adjacentFace01;
				}
				if (!bInt && adjacentFace1)
				{
					bInt = IntersectFaceByLine(adjacentFace1, pnt, normal, intPnt);
					intFace = adjacentFace1;
				}
				if (!bInt && adjacentFace11)
				{
					bInt = IntersectFaceByLine(adjacentFace11, pnt, normal, intPnt);
					intFace = adjacentFace11;
				}
				if (0)
					ShowPoint(pDoc, pnt, red);
				// If intersect, determine the distance to edges
				if (bInt)
				{
					IwPoint3d closestPnt;
					double dist, minDist = 10000000;
					IwTArray<IwEdge*> edges;
					intFace->GetEdges(edges);
					for (unsigned j = 0; j < edges.GetSize(); j++)
					{
						if (!edges.GetAt(j)->GetCurve()->IsLinear(0.001))// only check non-linear edge
						{
							dist = DistFromPointToEdge(intPnt, edges.GetAt(j), closestPnt, optVector);
							if (dist == -1) // dist = -1 if no solution, try the end points
							{
								IwVertex* sV, * eV;
								edges.GetAt(j)->GetVertices(sV, eV);
								double sDist, eDist;
								sDist = sV->GetPoint().DistanceBetween(intPnt);
								eDist = eV->GetPoint().DistanceBetween(intPnt);
								dist = min(sDist, eDist);
							}
							if (dist > -0.1 && dist < minDist)
							{
								minDist = dist;
							}
						}
					}
					if (minDist < 10.0)// got an answer and a reasonable answer
					{
						if (minDist > allowableDistMax || minDist < allowableDistMin)
						{
							unqualifiedLocations.Add(pnt);
							unqualifiedDistances.Add(minDist);
						}
					}
				}
				else// If no intersection, it could be outside the face or solution not found. 
				{
					IwTArray<IwFace*> faces;
					if (thisFace)
						faces.Add(thisFace);
					if (thisFace1)
						faces.Add(thisFace1);
					if (adjacentFace0)
						faces.Add(adjacentFace0);
					if (adjacentFace01)
						faces.Add(adjacentFace01);
					if (adjacentFace1)
						faces.Add(adjacentFace1);
					if (adjacentFace11)
						faces.Add(adjacentFace11);
					IwBSplineCurve* aLine;
					IwBSplineCurve::CreateLineSegment(pDoc->GetIwContext(), 3, pnt - 30 * normal, pnt + 30 * normal, aLine);
					IwPoint3d facePnt, linePnt;
					double outsideDist = DistFromFacesToCurve(faces, aLine, facePnt, linePnt);
					if (outsideDist >= 0.001) // "0.001" as tolerance to prevent from intersection
					{
						unqualifiedLocations.Add(pnt);
						unqualifiedDistances.Add(-outsideDist);
					}
					// Thus, if outsideDist < 0.001 "Intersect with faces", but previously not found,
					// We do not add such pnt to the unqualifiedLocations.
					if (aLine)
						IwObjDelete(aLine);
				}
			}

			if (unqualifiedLocations.GetSize() == 0)
				return true;
			else
				return false;
		}

		//////////////////////////////////////////////////////////////////////////////////////
		// This function validates the distance from lateral ear peak to 
		// ant/antCham cut edge (if earOnly=false), or to 7.5% of overall outline length (if earOnly=true).
		bool ValidateDistanceToLateralAntCutProfile
		(
			CTotalDoc* pDoc,							// I:
			IwTArray<IwPoint3d>& unqualifiedLocations,	// O:
			IwTArray<double>& unqualifiedDistances,		// O:
			bool earOnly,								// I:
			IwPoint3d* arc2ndPoint,						// O: the arc 2nd point 
			double* arc2ndDist							// O: the arc 2nd point distance to edge
		)
		{
			bool insideCutProfile = true;

			if (pDoc->GetOutlineProfile() == NULL)
				return insideCutProfile;
			if (pDoc->GetFemoralCuts() == NULL)
				return insideCutProfile;

			// Get femoral axis
			IwAxis2Placement wslAxis;
			pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxis);

			// Get the feature point
			IwTArray<IwPoint3d> featurePoints, featurePointsRemap;
			IwTArray<int> featurePointsInfo;
			pDoc->GetOutlineProfile()->GetOutlineProfileFeaturePoints(featurePoints, featurePointsInfo, featurePointsRemap);
			// Determine the sketch surface
			IwBSplineSurface* sketchSurface;
			pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);

			// determine the face and adjacent face
			IwTArray<IwFace*> cutFaces;
			IwFace* antFace = NULL, * antChamFace = NULL, * antChamFace1 = NULL;
			int antFaceIndex = 9;
			pDoc->GetFemoralCuts()->SearchCutFaces(antFaceIndex, cutFaces);
			antFace = cutFaces.GetAt(0);
			int antChamFaceIndex = 8;
			pDoc->GetFemoralCuts()->SearchCutFaces(antChamFaceIndex, cutFaces);
			antChamFace = cutFaces.GetAt(0);
			if (cutFaces.GetSize() > 1)
				antChamFace1 = cutFaces.GetAt(1);
			// ant/ant chamfer edge index in lateral side
			int lateralAntCutEdgeIndex;
			// determine side info
			QString sideInfo;
			CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);
			bool positiveSideIsLateral;
			if (implantSide == IMPLANT_SIDE_RIGHT)
			{
				positiveSideIsLateral = true;
				lateralAntCutEdgeIndex = 18;
			}
			else
			{
				positiveSideIsLateral = false;
				lateralAntCutEdgeIndex = 19;
			}
			// convert the femoral cuts edge index to the outline profile index, then
			// get the corresponding uv point on the edge
			int lateralAntCutOutlineIndex = pDoc->GetOutlineProfile()->GetOutlineProfileFeatureIndexFromFemoralCutsFeatureIndex(lateralAntCutEdgeIndex);
			if (lateralAntCutOutlineIndex < 0)// old file or something wrong
				return true;
			IwPoint3d lateralAntCutFeaturePoint = featurePoints.GetAt(lateralAntCutOutlineIndex) - featurePointsRemap.GetAt(lateralAntCutOutlineIndex);
			lateralAntCutFeaturePoint = IwPoint3d(lateralAntCutFeaturePoint.x, lateralAntCutFeaturePoint.y, 0);
			// Determine the corresponding parameter on uvCurve
			IwPoint3d cPnt;
			double lateralAntCutParam;
			DistFromPointToCurve(lateralAntCutFeaturePoint, uvCurve, cPnt, lateralAntCutParam);
			// determine the anterior ear-peak point in lateral side
			// determine the seam point
			IwPoint3d seamPoint;
			uvCurve->EvaluatePoint(uvCurve->GetNaturalInterval().GetMin(), seamPoint);
			// determine the ear point in the lateral side
			double lateralAntEarParam;
			IwPoint3d farAntPoint;
			IwSolutionArray sols;
			IwSolution sol;
			IwExtent1d crvDom = uvCurve->GetNaturalInterval();
			if (positiveSideIsLateral)
			{
				IwExtent1d searchDom = IwExtent1d(crvDom.Evaluate(0), crvDom.Evaluate(0.2));
				farAntPoint = seamPoint + IwPoint3d(20, -100, 0);
				uvCurve->GlobalPointSolve(searchDom, IW_SO_MINIMIZE, farAntPoint, 0.01, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() == 0)
				{
					lateralAntEarParam = crvDom.Evaluate(0);
				}
				else
				{
					sol = sols.GetAt(0);
					lateralAntEarParam = sol.m_vStart.m_adParameters[0];
				}
			}
			else
			{
				IwExtent1d searchDom = IwExtent1d(crvDom.Evaluate(0.8), crvDom.Evaluate(1.0));
				farAntPoint = seamPoint + IwPoint3d(-20, -100, 0);
				uvCurve->GlobalPointSolve(searchDom, IW_SO_MINIMIZE, farAntPoint, 0.01, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() == 0)
				{
					lateralAntEarParam = crvDom.Evaluate(1);
				}
				else
				{
					sol = sols.GetAt(0);
					lateralAntEarParam = sol.m_vStart.m_adParameters[0];
				}
			}

			double prevParam, postParam;
			if (positiveSideIsLateral)
			{
				prevParam = lateralAntEarParam;
				postParam = lateralAntCutParam;
				if (earOnly)
					postParam = lateralAntEarParam + 0.075 * crvDom.GetLength();
			}
			else
			{
				prevParam = lateralAntCutParam;
				postParam = lateralAntEarParam;
				if (earOnly)
					prevParam = lateralAntEarParam - 0.075 * crvDom.GetLength();
			}


			// Sample the curve in between
			double length;
			uvCurve->Length(IwExtent1d(prevParam, postParam), 0.01, length);
			int sampleNo = (int)(length / 2.0) + 1;
			IwTArray<IwPoint3d> sPnts;
			uvCurve->EquallySpacedPoints(prevParam, postParam, sampleNo, 0.01, &sPnts, NULL);

			// Extrude a line from sampling point to intersect the face
			for (unsigned i = 0; i < sPnts.GetSize(); i++)
			{
				IwPoint2d uv = IwPoint2d(sPnts.GetAt(i).x, sPnts.GetAt(i).y);
				IwVector3d pnt, normal;
				sketchSurface->EvaluateNormal(uv, FALSE, FALSE, normal);
				sketchSurface->EvaluatePoint(uv, pnt);
				IwFace* intFace = antFace;
				IwPoint3d intPnt;
				bool bInt = IntersectFaceByLine(antFace, pnt, normal, intPnt);
				// If no intersection, try ant/antChamfer face
				if (!bInt && antChamFace)
				{
					bInt = IntersectFaceByLine(antChamFace, pnt, normal, intPnt);
					intFace = antChamFace;
				}
				// If no intersection, try antChamFace1 face
				if (!bInt && antChamFace1)
				{
					bInt = IntersectFaceByLine(antChamFace1, pnt, normal, intPnt);
					intFace = antChamFace1;
				}
				// If no intersection and outsideDist > 0 (really outside of lateral side), then it is outside of the face.
				if (!bInt)
				{
					IwTArray<IwEdge*> edges;
					antFace->GetEdges(edges);
					IwPoint3d cPnt;
					DistFromPointToEdges(pnt, edges, cPnt);
					IwVector3d tempVec = pnt - cPnt;
					double outsideDist;
					if (positiveSideIsLateral)
						outsideDist = tempVec.Dot(wslAxis.GetXAxis());// only care x-component
					else
						outsideDist = tempVec.Dot(-wslAxis.GetXAxis());// only care x-component
					if (outsideDist > 0)
					{
						unqualifiedLocations.Add(pnt);
						unqualifiedDistances.Add(outsideDist);
					}
				}
			}

			///////////////////////////////////////////////////////////////////////////////////
			// Additionally here also validate whether the arc 2nd point is too far away 
			// from the lateral bone cut.
			// Get the arc 2nd point
			IwPoint3d secondArcPointUV, secondArcPoint3d, normal, intPnt;
			if (positiveSideIsLateral)
			{
				if (featurePointsInfo.GetAt(2) != 0)// an arc in lateral ear
				{
					secondArcPointUV = featurePoints.GetAt(3);
					pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(secondArcPointUV, secondArcPoint3d, normal);
				}
			}
			else
			{
				unsigned nSize = featurePointsInfo.GetSize();
				if (featurePointsInfo.GetAt(nSize - 3) != 0)// an arc in lateral ear
				{
					secondArcPointUV = featurePoints.GetAt(nSize - 4);
					pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(secondArcPointUV, secondArcPoint3d, normal);
				}
			}
			// Determine the distance between the secondArcPoint3d to the lateral side boundary
			if (secondArcPoint3d.IsInitialized())
			{
				IwTArray<IwEdge*> edges;
				antFace->GetEdges(edges);
				IwPoint3d lateralMovedPnt;
				if (positiveSideIsLateral)
					lateralMovedPnt = secondArcPoint3d + 10.0 * wslAxis.GetXAxis();
				else
					lateralMovedPnt = secondArcPoint3d - 10.0 * wslAxis.GetXAxis();
				IwPoint3d lateralBndPnt;
				bool bInt = IntersectEdgesByPlane(edges, lateralMovedPnt, wslAxis.GetZAxis(), lateralBndPnt);
				if (bInt)
				{
					double insideDist = lateralMovedPnt.DistanceBetween(lateralBndPnt) - 10.0;// make it negative to be consistent with the others.
					if (arc2ndPoint && arc2ndDist)
					{
						*arc2ndPoint = secondArcPoint3d;
						*arc2ndDist = insideDist;
					}
				}
			}


			if (unqualifiedLocations.GetSize() == 0)
				return true;
			else
				return false;
		}

		bool ValidatePatellaCoverage(CTotalDoc* pDoc)
		{
			bool enoughPatellaCoverage = true;

			if (pDoc->GetFemoralAxes() == NULL)
				return enoughPatellaCoverage;
			if (pDoc->GetOutlineProfile() == NULL)
				return enoughPatellaCoverage;

			// determine ant plane
			IwPoint3d antCutPlane, antCutPlaneDir;
			pDoc->GetFemoralAxes()->GetAntCutPlaneInfo(antCutPlane, antCutPlaneDir);
			// determine the patellar 1/2 point
			IwPoint3d patellaMiddlePoint;
			double factor = 0.5;
			patellaMiddlePoint = patellaMiddlePoint.ProjectPointToPlane(antCutPlane, antCutPlaneDir);

			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);

			// determine the closest point
			IwPoint3d closestPoint;
			double param;
			double dist = DistFromPointToCurve(patellaMiddlePoint, crvOnSurf, closestPoint, param);

			// check patellaMiddlePoint is inside or outside of outline profile
			IwAxis2Placement femAxes;
			pDoc->GetFemoralAxes()->GetSweepAxes(femAxes);
			IwVector3d tempVec = closestPoint - patellaMiddlePoint;

			if (tempVec.Dot(femAxes.GetZAxis()) < 0)
				enoughPatellaCoverage = false;

			// if patellaMiddlePoint is outside the outline profile,
			// give a second test by using the two ear tips 
			if (!enoughPatellaCoverage)
			{
				IwPoint3d farAntPoint = closestPoint + 1000 * femAxes.GetZAxis();
				IwExtent1d crvDom = crvOnSurf->GetNaturalInterval();
				IwSolutionArray sols;
				IwPoint3d earTip0, earTip1;
				IwExtent1d searchDom = IwExtent1d(crvDom.Evaluate(0), crvDom.Evaluate(0.15));
				crvOnSurf->GlobalPointSolve(searchDom, IW_SO_MINIMIZE, farAntPoint, 0.001, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() > 0)
				{
					crvOnSurf->EvaluatePoint(sols.GetAt(0).m_vStart.m_adParameters[0], earTip0);
				}
				searchDom = IwExtent1d(crvDom.Evaluate(0.85), crvDom.Evaluate(1));
				crvOnSurf->GlobalPointSolve(searchDom, IW_SO_MINIMIZE, farAntPoint, 0.001, NULL, NULL, IW_SR_SINGLE, sols);
				if (sols.GetSize() > 0)
				{
					crvOnSurf->EvaluatePoint(sols.GetAt(0).m_vStart.m_adParameters[0], earTip1);
				}
				if (earTip0.IsInitialized() && earTip1.IsInitialized())
				{
					// check whether the patellaMiddlePoint and farAntPoint 
					// are on the opposite side of line defined by earTip0 & erTip1.
					IwVector3d tipTipVec = earTip1 - earTip0;
					IwVector3d farAntVec = farAntPoint - earTip0;
					IwVector3d middleVec = patellaMiddlePoint - earTip0;
					if (tipTipVec.Length() > 0.001)
					{
						IwVector3d vec0 = tipTipVec * farAntVec;
						IwVector3d vec1 = tipTipVec * middleVec;
						if (vec0.Dot(vec1) < 0)// opposite side
							enoughPatellaCoverage = true;
					}
				}
			}

			return enoughPatellaCoverage;
		}

		bool ValidateMLSize(CTotalDoc* pDoc)
		{
			bool goodMLSize = true;

			if (pDoc->GetFemoralAxes() == NULL)
				return goodMLSize;
			if (pDoc->GetOutlineProfile() == NULL)
				return goodMLSize;

			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);
			// check ML size
			IwAxis2Placement femAxes;
			pDoc->GetFemoralAxes()->GetSweepAxes(femAxes);
			IwPoint3d posFarPoint = femAxes.GetOrigin() + 1000 * femAxes.GetXAxis();
			IwPoint3d negFarPoint = femAxes.GetOrigin() - 1000 * femAxes.GetXAxis();
			IwPoint3d posClosestPoint;
			double param;
			double posDist = DistFromPointToCurve(posFarPoint, crvOnSurf, posClosestPoint, param);
			IwPoint3d negClosestPoint;
			double negDist = DistFromPointToCurve(negFarPoint, crvOnSurf, negClosestPoint, param);

			IwVector3d tempVec = posClosestPoint - negClosestPoint;
			double MLSize = fabs(tempVec.Dot(femAxes.GetXAxis()));

			double MLSizeHigh, MLSizeLow;
			pDoc->GetImplantMLSizeRange(MLSizeHigh, MLSizeLow);

			if (MLSize > MLSizeHigh || MLSize < MLSizeLow)
				goodMLSize = false;

			return goodMLSize;
		}

		void ValidateNotchProfile
		(
			CTotalDoc* pDoc,			// I:
			bool& goodTipDist,			// O:
			bool& goodEighteenWidth,	// O:
			bool& leaningMedially,		// O:
			double& tipDist,			// O:
			double& eighteenWidth		// O:
		)
		{
			//	goodTipDist = true;
			//	goodEighteenWidth = true;
			//	leaningMedially = true;

			//	if ( pDoc->GetFemoralAxes() == NULL )
			//		return;
			//	if ( pDoc->GetOutlineProfile() == NULL )
			//		return;
			//	if ( pDoc->GetJCurves() == NULL )
			//		return;

			//	// determine coordinate
			//	IwAxis2Placement mechanicalAxes, wslAxes, sweepAxes;
			//	pDoc->GetFemoralAxes()->GetMechanicalAxes(mechanicalAxes);
			//	pDoc->GetFemoralAxes()->GetSweepAxes(sweepAxes);
			//	pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
			//	// determine the outline curve
			//	IwBSplineCurve *crvOnSurf, *uvCurve;
			//	pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);

			//	// determine the notch tip distance to origin
			//	IwPoint3d farAntPoint = sweepAxes.GetOrigin() + 2500*sweepAxes.GetYAxis();
			//	IwExtent1d crvDom = crvOnSurf->GetNaturalInterval();
			//	IwExtent1d searchDom = IwExtent1d(crvDom.Evaluate(0.35), crvDom.Evaluate(0.65));
			//	IwSolutionArray sols;
			//	crvOnSurf->GlobalPointSolve(searchDom, IW_SO_MINIMIZE, farAntPoint, 0.001, NULL, NULL,  IW_SR_SINGLE, sols);
			//	double notchParam=-1;
			//	tipDist = -1.0;
			//	if ( sols.GetSize() > 0 )
			//	{
			//		IwPoint3d notchTip;
			//		notchParam = sols.GetAt(0).m_vStart.m_adParameters[0];
			//		crvOnSurf->EvaluatePoint(notchParam, notchTip);
			//		IwVector3d tempVec = wslAxes.GetOrigin() - notchTip;
			//		double distAlongY = tempVec.Dot(wslAxes.GetYAxis());
			//		double maxDist = pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MAX DISTANCE TO ORIGIN");
			//		double minDist = pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MIN DISTANCE TO ORIGIN");
			//		if ( distAlongY < minDist || distAlongY > maxDist)
			//			goodTipDist = false;
			//		tipDist = distAlongY;
			//	}

			//	// determine 18mm notch gap at 30 flexion degrees of mechanical axes
			//	static double ang = -30.0;
			//	double deg30Radian = ang/(180.0/IW_PI);
			//	IwAxis2Placement rotMat, rotatedMat;
			//	rotMat.RotateAboutAxis(deg30Radian, IwVector3d(1,0,0));
			//	rotMat.TransformAxis2Placement(mechanicalAxes, rotatedMat);
			//	IwVector3d flex30Vec = -rotatedMat.GetZAxis();
			//	IwPoint3d farDownPoint = sweepAxes.GetOrigin() + 2500*flex30Vec;
			//	// get sketch profile
			//	IwBSplineCurve* sketchProfile = CDefineOutlineProfile::DetermineSketchProfile(pDoc);
			//	IwPoint3d closestPoint;
			//	double param;
			//	DistFromPointToCurve(farDownPoint, sketchProfile, closestPoint, param);
			//if (0)
			//ShowPoint(pDoc, closestPoint, red);
			//	IwBSplineCurve *line;
			//	IwBSplineCurve::CreateLineSegment(pDoc->GetIwContext(), 3, closestPoint+15*sweepAxes.GetXAxis(), closestPoint-15*sweepAxes.GetXAxis(), line);
			//if (0)
			//ShowCurve(pDoc, line, red);
			//searchDom = IwExtent1d(notchParam+0.005*crvDom.GetLength(), notchParam+0.15*crvDom.GetLength());
			//IwExtent1d searchDomLine = IwExtent1d(line->GetNaturalInterval().Evaluate(0.5), line->GetNaturalInterval().Evaluate(1.0));
			//IwPoint3d posIntPoint, negIntPoint, cPnt;
			//double negDist = DistFromCurveToCurve(line, searchDomLine, crvOnSurf, searchDom, cPnt, param, negIntPoint, param);
			//searchDom = IwExtent1d(notchParam-0.005*crvDom.GetLength(), notchParam-0.15*crvDom.GetLength());
			//searchDomLine = IwExtent1d(line->GetNaturalInterval().Evaluate(0.0), line->GetNaturalInterval().Evaluate(0.5));
			//double posDist = DistFromCurveToCurve(line, searchDomLine, crvOnSurf, searchDom, cPnt, param, posIntPoint, param);
			//eighteenWidth = -1.0;
			//if ( fabs(posDist) < 0.1 && posIntPoint.IsInitialized() && 
			//	 fabs(negDist) < 0.1 && negIntPoint.IsInitialized() )
			//	{
			//		double dist18mm = posIntPoint.DistanceBetween(negIntPoint);
			//		if ( dist18mm > 18.0 )
			//			goodEighteenWidth = false;
			//		eighteenWidth = dist18mm;
			//if (0)
			//{
			//ShowPoint(pDoc, posIntPoint, red);
			//ShowPoint(pDoc, negIntPoint, blue);
			//}
			//	}

			//	// determine leaningMedially
			//	// Get implant side information
			//	QString sideInfo;
			//	CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);
			//	bool positiveSideIsLateral;
			//	if (implantSide == IMPLANT_SIDE_RIGHT)
			//	{
			//		positiveSideIsLateral = true;
			//	}
			//	else
			//	{
			//		positiveSideIsLateral = false;
			//	}
			//	IwBSplineCurve* tCurve = (IwBSplineCurve*)pDoc->GetJCurves()->GetJCurve(2);
			//	if ( tCurve == NULL )
			//		return;
			//	IwVector3d sPnt = closestPoint + 30*flex30Vec;
			//	IwVector3d ePnt = closestPoint - 20*flex30Vec;
			//	IwBSplineCurve *line30;
			//	IwBSplineCurve::CreateLineSegment(pDoc->GetIwContext(), 3, sPnt, ePnt, line30);
			//	IwPoint3d tcPnt;
			//	DistFromCurveToCurve(tCurve, line30, tcPnt, param, cPnt, param);
			//	if ( positiveSideIsLateral )
			//	{
			//		if ( tcPnt.DistanceBetween(posIntPoint) > tcPnt.DistanceBetween(negIntPoint) )
			//			leaningMedially = false;
			//	}
			//	else
			//	{
			//		if ( tcPnt.DistanceBetween(posIntPoint) < tcPnt.DistanceBetween(negIntPoint) )
			//			leaningMedially = false;
			//	}

			//	if ( sketchProfile )
			//		IwObjDelete(sketchProfile);
			//	if ( line )
			//		IwObjDelete(line);
			//	if ( line30 )
			//		IwObjDelete(line30);

		}

		double ValidateAnteriorEarProfile(CTotalDoc* pDoc)
		{
			double minEarRadius = 20.0;// a good number
			if (pDoc->GetOutlineProfile() == NULL)
				return minEarRadius;
			IwBSplineSurface* sketchSurface = NULL;
			pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			if (sketchSurface == NULL) return minEarRadius;

			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);
			IwTArray<IwPoint3d> UVFeatPnts, UVFeatPntsRemap;
			IwTArray<int> UVFeatPntsInfo;
			pDoc->GetOutlineProfile()->GetOutlineProfileFeaturePoints(UVFeatPnts, UVFeatPntsInfo, UVFeatPntsRemap);
			int nSize = UVFeatPnts.GetSize();

			// check the positive side ear
			// check the 1st point and nearby, regardless interpolation or arc
			{
				IwExtent1d crvDom = crvOnSurf->GetNaturalInterval();
				double delta = crvDom.GetLength() / 500.0;
				IwPoint2d UV = IwPoint2d(UVFeatPnts.GetAt(1).x, UVFeatPnts.GetAt(1).y);
				IwPoint3d pnt3d;
				sketchSurface->EvaluatePoint(UV, pnt3d);
				double param;
				DistFromPointToCurve(pnt3d, crvOnSurf, pnt3d, param);
				double radius = 20.0;
				for (double paramT = param - 5 * delta; paramT <= param + 5 * delta; paramT += delta)
				{
					double t = paramT;
					double curvature;
					if (t < crvDom.GetMin())
						t = crvDom.GetMin();
					if (t > crvDom.GetMax())
						t = crvDom.GetMax();
					crvOnSurf->EvaluateCurvature(t, curvature);
					radius = min(radius, 1.0 / curvature);
				}
				minEarRadius = min(minEarRadius, radius);
			}

			// check the negative side ear
			// check the nSize-2 point and nearby, regardless interpolation or arc
			{
				IwExtent1d crvDom = crvOnSurf->GetNaturalInterval();
				double delta = crvDom.GetLength() / 500.0;
				IwPoint2d UV = IwPoint2d(UVFeatPnts.GetAt(nSize - 2).x, UVFeatPnts.GetAt(nSize - 2).y);
				IwPoint3d pnt3d;
				sketchSurface->EvaluatePoint(UV, pnt3d);
				double param;
				DistFromPointToCurve(pnt3d, crvOnSurf, pnt3d, param);
				double radius = 20.0;
				for (double paramT = param - 5 * delta; paramT <= param + 5 * delta; paramT += delta)
				{
					double t = paramT;
					double curvature;
					if (t < crvDom.GetMin())
						t = crvDom.GetMin();
					if (t > crvDom.GetMax())
						t = crvDom.GetMax();
					crvOnSurf->EvaluateCurvature(t, curvature);
					radius = min(radius, 1.0 / curvature);
				}
				minEarRadius = min(minEarRadius, radius);
			}

			return minEarRadius;
		}

		bool ValidateMedialAnteriorProfile
		(
			CTotalDoc* pDoc,			// I:
			IwPoint3d* recommendedPoint	// O: recommended medial ant ear ctrl point when is not valid.
		)
		{
			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);
			// Determine reference point
			IwPoint3d refCtrPnt;
			double refRadius;
			OutlineProfileMgrUI3D::DetermineAntMedialProfileReferencePoint(pDoc, refCtrPnt, refRadius);
			// Determine distance
			IwPoint3d cPnt;
			double param;
			double dist = DistFromPointToCurve(refCtrPnt, crvOnSurf, cPnt, param);
			if (recommendedPoint)
			{
				IwVector3d tempVec = cPnt - refCtrPnt;
				tempVec.Unitize();
				*recommendedPoint = refCtrPnt + 0.75 * refRadius * tempVec;
			}
			if (dist > refRadius)
				return false;
			else
				return true;
		}

		double ValidatePosteriorDistanceToPosteriorCutProfile
		(
			CTotalDoc* pDoc,				// I:
			int faceIndex,					// I: 0: positive side posterior face, 1: negative side posterior face. (cannot be others)
			IwTArray<IwPoint3d>& locations, // O: sampling points 
			IwTArray<double>& distances		// O: posterior distance from sampling points to cut edge
		)
		{
			locations.RemoveAll();
			distances.RemoveAll();

			double minDistance = 1.0;

			// Determine posterior vector
			IwAxis2Placement sweepAxis;
			pDoc->GetFemoralAxes()->GetSweepAxes(sweepAxis);
			IwVector3d postVector = sweepAxis.GetZAxis();
			// Determine the cut feature points
			IwTArray<IwPoint3d> cutFeaturePoints;
			pDoc->GetFemoralCuts()->GetFeaturePoints(cutFeaturePoints, NULL);
			if (cutFeaturePoints.GetSize() == 0)
				return minDistance;

			// Determine the sketch surface
			IwBSplineSurface* sketchSurface;
			pDoc->GetOutlineProfile()->GetSketchSurface(sketchSurface);
			// determine the outline curve
			IwBSplineCurve* crvOnSurf, * uvCurve;
			pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);

			//
			double allowableDistMax = -10.0; // a negative number
			double allowableDistMin = 20.0;  // a positive number
			IwTArray<IwPoint3d> thisLocations;
			IwTArray<double> thisDistances, ascendingDistancesTrue, ascendingDistancesFalse;
			if (faceIndex == 0 || faceIndex == 1)
			{
				ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, true, allowableDistMax, allowableDistMin, thisLocations, thisDistances, &postVector);
				locations.Append(thisLocations);
				// sort distance
				ascendingDistancesTrue.Append(thisDistances);
				SortIwTArray(ascendingDistancesTrue);
				//
				ValidateDistanceToACutFace(pDoc, cutFeaturePoints, sketchSurface, uvCurve, faceIndex, false, allowableDistMax, allowableDistMin, thisLocations, thisDistances, &postVector);
				locations.Append(thisLocations);
				// sort distance
				ascendingDistancesFalse.Append(thisDistances);
				SortIwTArray(ascendingDistancesFalse);
				//
				minDistance = min(ascendingDistancesTrue.GetAt(0), ascendingDistancesFalse.GetAt(0));
			}

			return minDistance;
		}

	};
};

