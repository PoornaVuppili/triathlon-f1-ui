#pragma once

#include "CustomEventTypes.h"

class CloseImplantEvent : public QEvent
{
public:
	CloseImplantEvent(bool confirm=false)
		: QEvent(CLOSE_IMPLANT_EVENT), confirm(confirm) {}

	bool GetShouldConfirm()		{	return confirm;	}

private:
	bool confirm;
};