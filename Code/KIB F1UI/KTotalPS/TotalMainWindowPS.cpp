#include <QtGui>

#include "TotalMainWindowPS.h"
#include "TotalDocPS.h"
#include "TotalViewPS.h"
#include "TotalEntPanelPS.h"
#include "EntityForTwin.h"
//#include "AdvancedControlPS.h"
#include "..\KTriathlonF1\TotalEntPanelJigs.h"
#include "..\KTriathlonF1\AdvancedControl.h"
#include "..\KTriathlonF1\SquishWrapper.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\EntityWidget.h"
#include "..\KApp\ViewWidget.h"
#include "..\KApp\Viewport.h"
#include "..\KApp\Implant.h"
#include "..\KUtility\KUtility.h"
#include "..\KTriathlonF1\IFemurImplant.h"
	
KTotalMainWindowPS::KTotalMainWindowPS()
	: CTotalMainWindow()
	, m_pEntPanelPSFeatures(nullptr)

{
}

KTotalMainWindowPS::~KTotalMainWindowPS()
{
}

bool KTotalMainWindowPS::Initialize()
{
	// Set main widgets
	m_pDoc = new KTotalDocPS(this);
	m_pView = new KTotalViewPS( this );
    m_pEntPanel = new KTotalEntPanelPS(entityWidget);
	m_pEntPanelJigs = new CTotalEntPanelJigs(entityWidget);
	m_pEntPanelPSFeatures = new KTotalEntPanelPS(entityWidget);

	KTotalDocPS::SetTotalDocPS((KTotalDocPS*)m_pDoc);
	m_pDoc->SetView( m_pView );
	m_pDoc->SetEntPanel( m_pEntPanel );
	GetTotalDoc()->SetEntPanelJigs( m_pEntPanelJigs );
	GetTotalDocPS()->SetEntPanelPSFeatures(m_pEntPanelPSFeatures);

	// Also set to CTotalDoc
	CTotalDoc::SetTotalDoc((CTotalDoc*)m_pDoc);

	m_pView->SetDoc( m_pDoc );
    m_pView->SetMainWindow( this );

	m_pEntPanel->SetDoc( m_pDoc );
	m_pEntPanelJigs->SetDoc( m_pDoc );
	m_pEntPanelPSFeatures->SetDoc(m_pDoc);

	/*******************************************************/
    m_menuFemur = new QMenu("Fem Basis");
    menus.push_back(m_menuFemur);

    /******************* MAKING MENU **********************/
	m_actFileNew			= m_menuFemur->addAction( "New" );
	m_actMakeFemoralAxes	= m_menuFemur->addAction( "Make Femoral Axes" );
   
	m_actDefinePegs			= NULL;
	m_actConvertCastingImplant = NULL;
	
	m_actSeparator			= m_menuFemur->addSeparator();
	m_actAdvancedControl	= m_menuFemur->addAction( "Adv Ctrl Fem Basis");

	connect( m_menuFemur, SIGNAL( aboutToShow() ), this, SLOT( OnEnableMakingActions() ) );
	connect( m_actFileNew, SIGNAL( triggered() ), this, SLOT( OnFileNew() ) );
	connect( m_actMakeFemoralAxes, SIGNAL( triggered() ), m_pView, SLOT( OnHandleFemoralAxes() ) );
	
	connect( m_actAdvancedControl, SIGNAL( triggered() ), m_pView, SLOT( OnAdvancedControl() ) );

	// Create toolbar
	JCOSToolbar = new QToolBar("JCOS");
	
	JCOSToolbar->setVisible(false);
	toolbars.push_back( JCOSToolbar );

	/******************* FILE MENU **********************/
	m_actSeparator				= m_menuFemur->addSeparator();
	m_menuFile					= m_menuFemur->addMenu( "&File" );
	m_actFileImportCADEntity	= m_menuFile->addAction( "Import CAD Entity to Femur" );
	char *saveNewTWFile = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (saveNewTWFile != NULL && QString("1") == saveNewTWFile)// under-development mode to optionally save new tw file only
	{
		m_actFileSaveNewFile		= m_menuFile->addAction( "Save New tw File" );
		connect( m_actFileSaveNewFile, SIGNAL( triggered() ), this, SLOT( OnSaveNewFile() ) );
	}
	m_menuFile->addSeparator();

	connect( m_actFileImportCADEntity, SIGNAL( triggered() ), this, SLOT( OnImportCADEntity() ) );

	/******************* VIEW MENU **********************/
	m_menuView			= m_menuFemur->addMenu( "&View" );
	m_menuView->addSeparator();
	m_menuViewTessellation		= m_menuView->addMenu( "Tessellation" );
	m_actTessellationHigh	= m_menuViewTessellation->addAction( "ScreenShot" );
	m_actTessellationFine		= m_menuViewTessellation->addAction( "Fine" );
	m_actTessellationNormal		= m_menuViewTessellation->addAction( "Normal" );
	m_actTessellationCoarse		= m_menuViewTessellation->addAction( "Coarse" );
	m_actTessellationFine->setCheckable(true);
	m_actTessellationNormal->setCheckable(true);
	m_actTessellationCoarse->setCheckable(true);
	m_groupTessellation = new QActionGroup( this );
	m_groupTessellation->addAction(m_actTessellationHigh);
	m_groupTessellation->addAction(m_actTessellationFine);
	m_groupTessellation->addAction(m_actTessellationNormal);
	m_groupTessellation->addAction(m_actTessellationCoarse);
	m_actTessellationFine->setChecked(true);
	m_menuViewTransparency			= m_menuView->addMenu( "Transparency" );
	m_actTransparencyTransparentTotal= m_menuViewTransparency->addAction( "TransparentTotal" );
	m_actTransparencyTransparentPlus= m_menuViewTransparency->addAction( "Transparent++" );
	m_actTransparencyTransparent	= m_menuViewTransparency->addAction( "Transparent+" );
	m_actTransparencyNormal			= m_menuViewTransparency->addAction( "Transparent" );
	m_actTransparencyOpaque			= m_menuViewTransparency->addAction( "Transparent-" );
	m_actTransparencyOpaquePlus		= m_menuViewTransparency->addAction( "Transparent--" );
	m_actTransparencyTransparentTotal->setCheckable(true);
	m_actTransparencyTransparentPlus->setCheckable(true);
	m_actTransparencyTransparent->setCheckable(true);
	m_actTransparencyNormal->setCheckable(true);
	m_actTransparencyOpaque->setCheckable(true);
	m_actTransparencyOpaquePlus->setCheckable(true);
	m_groupTransparency = new QActionGroup( this );
	m_groupTransparency->addAction(m_actTransparencyTransparentTotal);
	m_groupTransparency->addAction(m_actTransparencyTransparentPlus);
	m_groupTransparency->addAction(m_actTransparencyTransparent);
	m_groupTransparency->addAction(m_actTransparencyNormal);
	m_groupTransparency->addAction(m_actTransparencyOpaque);
	m_groupTransparency->addAction(m_actTransparencyOpaquePlus);
	m_actTransparencyNormal->setChecked(true);

	{
		m_actCleanTempDisplay	= m_menuView->addAction( "CleanUp Temp Display" );
		connect( m_actCleanTempDisplay, SIGNAL( triggered() ), this, SLOT( OnCleanUpTempDisplay() ) );
	}

	connect( m_actTessellationHigh, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationHigh() ) );
	connect( m_actTessellationFine, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationFine() ) );
	connect( m_actTessellationNormal, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationNormal() ) );
	connect( m_actTessellationCoarse, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationCoarse() ) );
	connect( m_actTransparencyTransparentTotal, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentTotal() ) );
	connect( m_actTransparencyTransparentPlus, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentPlus() ) );
	connect( m_actTransparencyTransparent, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparent() ) );
	connect( m_actTransparencyNormal, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyNormal() ) );
	connect( m_actTransparencyOpaque, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaque() ) );
	connect( m_actTransparencyOpaquePlus, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaquePlus() ) );

	/********************* TOOL ************************************/
	m_menuTools					= m_menuFemur->addMenu( "&Tools" );
	m_actAnalyzeImplant			= m_menuTools->addAction( "Evaluate Implant" );
	m_actAnalyzeImplant->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_E) );
	m_actReviewImplant			= m_menuTools->addAction( "Review Fem Basis" );
	m_actReviewImplant->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_W) );

	connect( m_actAnalyzeImplant, SIGNAL( triggered() ), m_pView, SLOT( OnAnalyzeImplant() ) );
	connect( m_actReviewImplant, SIGNAL( triggered() ), this, SLOT( OnFemoralImplantReview() ) );

	/******************** INFO MENU **********************/
	m_menuInfo					= m_menuFemur->addMenu( "&Info" );
	m_actUserHistory			= m_menuInfo->addAction( "File Info" );
	m_actPatientInfo			= m_menuInfo->addAction( "Patient Info" );
	connect( m_actUserHistory, SIGNAL( triggered() ), m_pView, SLOT( OnFileInfo() ) );
	connect( m_actPatientInfo, SIGNAL( triggered() ), m_pView, SLOT( OnPatientInfo() ) );

    return true;
}

bool KTotalMainWindowPS::InitializeJigs()
{

	/*******************************************************/
    m_menuFemurJigs = new QMenu("Fem Jigs");
    menus.push_back(m_menuFemurJigs);

	/******************* MAKING JIGS MENU **********************/
	m_actImportOsteophytesJigs		= m_menuFemurJigs->addAction( "Import Osteophyte" );
	m_actDefineCartilageJigs		= m_menuFemurJigs->addAction( "Define Cartilage" );
	m_actDefineOutlineProfileJigs	= m_menuFemurJigs->addAction( "Define Outline" );
	m_actMakeInnerSurfaceJigs		= m_menuFemurJigs->addAction( "Inner Surface" );
	m_actMakeOuterSurfaceJigs		= m_menuFemurJigs->addAction( "Outer Surface" );
	m_actMakeSideSurfaceJigs		= m_menuFemurJigs->addAction( "Side Surface" );
	m_actMakeSolidPositionJigs		= m_menuFemurJigs->addAction( "Position Jigs" );
	m_actDefineStylusJigs			= m_menuFemurJigs->addAction( "Stylus Jigs" );
	m_actMakeAnteriorSurfaceJigs	= m_menuFemurJigs->addAction( "Anterior Surface" );

	m_actSeparator					= m_menuFemurJigs->addSeparator();
	m_actAdvancedControlJigs		= m_menuFemurJigs->addAction( "Adv Ctrl Jigs");
	m_actValidateJigs				= m_menuFemurJigs->addAction( "Validate Jigs" );
	m_actOutputJigs					= m_menuFemurJigs->addAction( "Output Jigs" );

	connect( m_menuFemurJigs, SIGNAL( aboutToShow() ), this, SLOT( OnEnableMakingActionsJigs() ) );
	connect( m_actImportOsteophytesJigs, SIGNAL( triggered() ), m_pView, SLOT( OnImportOsteophyteSurface() ) );
	connect( m_actDefineCartilageJigs, SIGNAL( triggered() ), m_pView, SLOT( OnHandleCartilage() ) );
	connect( m_actDefineOutlineProfileJigs, SIGNAL( triggered() ), m_pView, SLOT( OnDefineOutlineProfileJigs() ) );
	connect( m_actMakeInnerSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeInnerSurfaceJigs() ) );
	connect( m_actMakeOuterSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnHandleOuterSurfaceJigs() ) );
	connect( m_actMakeSideSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeSideSurfaceJigs() ) );
	connect( m_actMakeSolidPositionJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeSolidPositionJigs() ) );
	connect( m_actDefineStylusJigs, SIGNAL( triggered() ), m_pView, SLOT( OnDefineStylusJigs() ) );
	connect( m_actMakeAnteriorSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeAnteriorSurfaceJigs() ) );
	connect( m_actAdvancedControlJigs, SIGNAL( triggered() ), m_pView, SLOT( OnAdvancedControlJigs() ) );
	connect( m_actValidateJigs, SIGNAL( triggered() ), m_pView, SLOT( OnValidateJigs() ) );
	connect( m_actOutputJigs, SIGNAL( triggered() ), m_pView, SLOT( OnOutputJigs() ) );

	/******************* FILE MENU **********************/
	m_actSeparator				= m_menuFemurJigs->addSeparator();
	m_menuFileJigs				= m_menuFemurJigs->addMenu( "&File" );
	m_actFileJigsImportCADEntity	= m_menuFileJigs->addAction( "Import CAD Entity" );

	connect( m_actFileJigsImportCADEntity, SIGNAL( triggered() ), this, SLOT( OnImportCADEntityJigs() ) );

	/******************* VIEW MENU **********************/
	m_menuViewJigs				= m_menuFemurJigs->addMenu( "&View" );
	m_menuViewTessellationJigs		= m_menuViewJigs->addMenu( "Tessellation" );
	m_actTessellationHighJigs		= m_menuViewTessellationJigs->addAction( "ScreenShot" );
	m_actTessellationFineJigs		= m_menuViewTessellationJigs->addAction( "Fine" );
	m_actTessellationNormalJigs		= m_menuViewTessellationJigs->addAction( "Normal" );
	m_actTessellationCoarseJigs		= m_menuViewTessellationJigs->addAction( "Coarse" );
	m_actTessellationFineJigs->setCheckable(true);
	m_actTessellationNormalJigs->setCheckable(true);
	m_actTessellationCoarseJigs->setCheckable(true);
	m_groupTessellationJigs = new QActionGroup( this );
	m_groupTessellationJigs->addAction(m_actTessellationHighJigs);
	m_groupTessellationJigs->addAction(m_actTessellationFineJigs);
	m_groupTessellationJigs->addAction(m_actTessellationNormalJigs);
	m_groupTessellationJigs->addAction(m_actTessellationCoarseJigs);
	m_actTessellationFineJigs->setChecked(true);
	m_menuViewTransparencyJigs			= m_menuViewJigs->addMenu( "Transparency" );
	m_actTransparencyTransparentTotalJigs= m_menuViewTransparencyJigs->addAction( "TransparentTotal" );
	m_actTransparencyTransparentPlusJigs= m_menuViewTransparencyJigs->addAction( "Transparent++" );
	m_actTransparencyTransparentJigs	= m_menuViewTransparencyJigs->addAction( "Transparent+" );
	m_actTransparencyNormalJigs			= m_menuViewTransparencyJigs->addAction( "Transparent" );
	m_actTransparencyOpaqueJigs			= m_menuViewTransparencyJigs->addAction( "Transparent-" );
	m_actTransparencyOpaquePlusJigs		= m_menuViewTransparencyJigs->addAction( "Transparent--" );
	m_actTransparencyTransparentTotalJigs->setCheckable(true);
	m_actTransparencyTransparentPlusJigs->setCheckable(true);
	m_actTransparencyTransparentJigs->setCheckable(true);
	m_actTransparencyNormalJigs->setCheckable(true);
	m_actTransparencyOpaqueJigs->setCheckable(true);
	m_actTransparencyOpaquePlusJigs->setCheckable(true);
	m_groupTransparencyJigs = new QActionGroup( this );
	m_groupTransparencyJigs->addAction(m_actTransparencyTransparentTotalJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyTransparentPlusJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyTransparentJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyNormalJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyOpaqueJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyOpaquePlusJigs);
	m_actTransparencyNormalJigs->setChecked(true);

	connect( m_actTessellationHighJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationHigh() ) );
	connect( m_actTessellationFineJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationFine() ) );
	connect( m_actTessellationNormalJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationNormal() ) );
	connect( m_actTessellationCoarseJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationCoarse() ) );
	connect( m_actTransparencyTransparentTotalJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentTotal() ) );
	connect( m_actTransparencyTransparentPlusJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentPlus() ) );
	connect( m_actTransparencyTransparentJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparent() ) );
	connect( m_actTransparencyNormalJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyNormal() ) );
	connect( m_actTransparencyOpaqueJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaque() ) );
	connect( m_actTransparencyOpaquePlusJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaquePlus() ) );

	///******************* TOOLS MENU **********************/
	m_menuToolsJigs					= m_menuFemurJigs->addMenu( "&Tools" );
	m_actReviewJigs					= m_menuToolsJigs->addAction( "Review Jigs" );

	connect( m_actReviewJigs, SIGNAL( triggered() ), m_pView, SLOT( OnActivateNextJigsReviewManager() ) );

	/******************* INFO MENU **********************/

    return true;
}

void KTotalMainWindowPS::EnableActions( bool bEnable )
{
	bool hasEntity = !m_pDoc->IsEmpty();
	bool bHasEntityAndEnable = bEnable && hasEntity;

	// Menu
	m_menuFemur->setEnabled( bEnable );
	m_menuFemurJigs->setEnabled( bEnable );

	// Enable Tools 
	m_actAnalyzeImplant->setEnabled( bHasEntityAndEnable );
	//m_actValidateImplant->setEnabled( bHasEntityAndEnable );
	m_actReviewImplant->setEnabled( bHasEntityAndEnable );
	// Enable Info
	m_actPatientInfo->setEnabled( hasEntity );
	m_actUserHistory->setEnabled( hasEntity );
	// Enable view
	m_menuViewTessellation->setEnabled( hasEntity );
	m_menuViewTransparency->setEnabled( hasEntity );
	// Enable AdvancedControl 
	m_actAdvancedControl->setEnabled( bHasEntityAndEnable );
	m_actAdvancedControlJigs->setEnabled( bHasEntityAndEnable );

	// Reset to disabled, then based on the rules to enabled.
	EnableMakingActions( false );
	EnableMakingActions( bHasEntityAndEnable ); 
 
	EnableMakingActionsJigs( false );
	EnableMakingActionsJigs( bHasEntityAndEnable ); 
}

bool KTotalMainWindowPS::OutputResults() const // virtual -- overridden
{
    bool rv(false);

    if (KTotalDocPS* pDoc = dynamic_cast<KTotalDocPS*>(m_pDoc))
    {
	    // Output Femur PS features
        QString sFileName = pDoc->GetFileName();
	    int length = sFileName.length();
	    sFileName.remove(length-3, 3);

	    QString	sPatientId = pDoc->GetPatientId();
        QString outputs(mainWindow->GetImplant()->GetInfo("ExpLocFemur"));
        QDir of(outputs);
        if (!of.exists())
            of.mkdir(outputs);

	    QString outputFolder_PS(mainWindow->GetImplant()->GetInfo("ExpLocFemPS"));
        QDir dd(outputFolder_PS);
        if (!dd.exists())
            dd.mkdir(outputFolder_PS);

        if (!outputFolder_PS.endsWith('\\'))
            outputFolder_PS.append('\\');
        
        QString dummy;

    }

    return CTotalMainWindow::OutputResults() && rv;
}

bool KTotalMainWindowPS::AllStepsComplete(std::string const &phase, std::string const &subphase) const
{
   // this is where all the items are checked to be "ready"
   if (phase == "Implant" /*"Profile"*/ && subphase == "Design")
   {
      if (KTotalDocPS* pDoc = const_cast<KTotalMainWindowPS*>(this)->GetTotalDocPS())
         return pDoc->IsFemoralImplantDesignComplete()
               && pDoc->IsFemoralJigsDesignComplete();
   }

   return true;
}
