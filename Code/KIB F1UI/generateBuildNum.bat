rem @echo off
p4 info
if not %ERRORLEVEL% == 0 goto noperforce
set getlastCL=p4 -c %1 changes -m 1 -s submitted #have
if exist %2 (
  set getformerCL=type "%2"
) else (
  set getformerCL=echo blah blah blah blah
)
FOR /f "tokens=2" %%L IN ('%getlastCL%') DO FOR /f "tokens=3" %%O in ('%getformerCL%') DO IF NOT %%L==%%O echo #define LASTSUBMITTEDCHANGELIST %%L>%2
:noperforce