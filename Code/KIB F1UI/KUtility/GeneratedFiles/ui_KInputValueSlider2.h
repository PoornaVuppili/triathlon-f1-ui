/********************************************************************************
** Form generated from reading UI file 'KInputValueSlider2.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KINPUTVALUESLIDER2_H
#define UI_KINPUTVALUESLIDER2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_InputValueSlider2Class
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label1;
    QSlider *horizontalSlider1;
    QLabel *label2;
    QSlider *horizontalSlider2;
    QSpinBox *spinBox1;
    QSpinBox *spinBox2;
    QWidget *layoutWidget1;
    QHBoxLayout *hboxLayout;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;
    QCheckBox *checkBox;

    void setupUi(QDialog *InputValueSlider2Class)
    {
        if (InputValueSlider2Class->objectName().isEmpty())
            InputValueSlider2Class->setObjectName(QString::fromUtf8("InputValueSlider2Class"));
        InputValueSlider2Class->resize(400, 128);
        layoutWidget = new QWidget(InputValueSlider2Class);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 9, 371, 71));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label1 = new QLabel(layoutWidget);
        label1->setObjectName(QString::fromUtf8("label1"));

        gridLayout->addWidget(label1, 0, 0, 1, 1);

        horizontalSlider1 = new QSlider(layoutWidget);
        horizontalSlider1->setObjectName(QString::fromUtf8("horizontalSlider1"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(horizontalSlider1->sizePolicy().hasHeightForWidth());
        horizontalSlider1->setSizePolicy(sizePolicy);
        horizontalSlider1->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider1, 0, 2, 1, 1);

        label2 = new QLabel(layoutWidget);
        label2->setObjectName(QString::fromUtf8("label2"));

        gridLayout->addWidget(label2, 1, 0, 1, 1);

        horizontalSlider2 = new QSlider(layoutWidget);
        horizontalSlider2->setObjectName(QString::fromUtf8("horizontalSlider2"));
        sizePolicy.setHeightForWidth(horizontalSlider2->sizePolicy().hasHeightForWidth());
        horizontalSlider2->setSizePolicy(sizePolicy);
        horizontalSlider2->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider2, 1, 2, 1, 1);

        spinBox1 = new QSpinBox(layoutWidget);
        spinBox1->setObjectName(QString::fromUtf8("spinBox1"));

        gridLayout->addWidget(spinBox1, 0, 1, 1, 1);

        spinBox2 = new QSpinBox(layoutWidget);
        spinBox2->setObjectName(QString::fromUtf8("spinBox2"));

        gridLayout->addWidget(spinBox2, 1, 1, 1, 1);

        layoutWidget1 = new QWidget(InputValueSlider2Class);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(210, 90, 176, 27));
        hboxLayout = new QHBoxLayout(layoutWidget1);
        hboxLayout->setSpacing(20);
        hboxLayout->setContentsMargins(11, 11, 11, 11);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        pushButtonOK = new QPushButton(layoutWidget1);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        hboxLayout->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(layoutWidget1);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        hboxLayout->addWidget(pushButtonCancel);

        checkBox = new QCheckBox(InputValueSlider2Class);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(20, 90, 171, 18));
        QWidget::setTabOrder(spinBox1, horizontalSlider1);
        QWidget::setTabOrder(horizontalSlider1, spinBox2);
        QWidget::setTabOrder(spinBox2, horizontalSlider2);
        QWidget::setTabOrder(horizontalSlider2, pushButtonOK);
        QWidget::setTabOrder(pushButtonOK, pushButtonCancel);

        retranslateUi(InputValueSlider2Class);

        pushButtonOK->setDefault(true);


        QMetaObject::connectSlotsByName(InputValueSlider2Class);
    } // setupUi

    void retranslateUi(QDialog *InputValueSlider2Class)
    {
        InputValueSlider2Class->setWindowTitle(QCoreApplication::translate("InputValueSlider2Class", "InputValueSlider2", nullptr));
        label1->setText(QCoreApplication::translate("InputValueSlider2Class", "Text1", nullptr));
        label2->setText(QCoreApplication::translate("InputValueSlider2Class", "Text2", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("InputValueSlider2Class", "OK", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("InputValueSlider2Class", "Cancel", nullptr));
        checkBox->setText(QCoreApplication::translate("InputValueSlider2Class", "CheckBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InputValueSlider2Class: public Ui_InputValueSlider2Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KINPUTVALUESLIDER2_H
