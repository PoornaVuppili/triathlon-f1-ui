#include "KEditingBrief.h"
#include <QFocusEvent>

// KSpinBoxBrief
KSpinBoxBrief::KSpinBoxBrief(QWidget *parent)
    : QSpinBox(parent)
{
    setFocusPolicy(Qt::ClickFocus);
}

KSpinBoxBrief::~KSpinBoxBrief()
{

}

void KSpinBoxBrief::focusOutEvent(QFocusEvent * eVent)
{
    QSpinBox::focusOutEvent(eVent);

    deleteLater();
}

void KSpinBoxBrief::keyPressEvent(QKeyEvent * eVent)
{
    QSpinBox::keyPressEvent(eVent);

    // hit Esc -> lose focus
    if( eVent->key() == Qt::Key_Escape )
        clearFocus();
}

// KComboBoxBrief
KComboBoxBrief::KComboBoxBrief(QWidget *parent)
    : QComboBox(parent)
{
    setFocusPolicy(Qt::ClickFocus);
}

KComboBoxBrief::~KComboBoxBrief()
{

}

void KComboBoxBrief::focusOutEvent(QFocusEvent * eVent)
{
    QComboBox::focusOutEvent(eVent);

    if( eVent->reason() != Qt::PopupFocusReason && eVent->reason() != Qt::OtherFocusReason )
        deleteLater();
}

void KComboBoxBrief::keyPressEvent(QKeyEvent * eVent)
{
    QComboBox::keyPressEvent(eVent);

    // hit Esc -> lose focus
    if( eVent->key() == Qt::Key_Escape )
        clearFocus();
}
