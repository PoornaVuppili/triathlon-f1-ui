#pragma once

#ifndef TPICKRECT_H_INCLUDED
#define TPICKRECT_H_INCLUDED

// A ray for picking objects in the viewports
#include "TLine3.h" // this also includes TPoint3 and TVector3, so we don't have to
#include "TRigid3.h"

template <typename T>
class TPicRect
{
protected:
    TPoint3<T>  corner1, corner2; // corners of the rect (screen)
    // the transforms are such that, when applied to the model point
    // make it the screen point and allows LT GT comparison with
    // the corner points' coordinates
    TRigid3<T> xf1;
    TRigid3<T> xf2;

public:
    TPicRect(const TPoint3<T>& c1, const TPoint3<T>& c2, const TRigid3<T>& t1, const TRigid3<T>& t2)
       : corner1(c1), corner2(c2), xf1(t1), xf2(t2)
    {}

    template<typename U>
    bool Contains(const TPoint3<U>& pt) const;
};

// implementation
template<typename T>
template<typename U>
bool TPicRect<T>::Contains(const TPoint3<U>& pt) const
{
   TPoint3<T> lpt = xf2 * (xf1 * TPoint3<T>(pt.GetX(), pt.GetY(), pt.GetZ()));

   return lpt.GetX() >= std::min(corner1.GetX(), corner2.GetX()) &&
          lpt.GetX() <= std::max(corner1.GetX(), corner2.GetX()) &&
          lpt.GetY() >= std::min(corner1.GetY(), corner2.GetY()) &&
          lpt.GetY() <= std::max(corner1.GetY(), corner2.GetY()) ;
}

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TPicRect<int> PickRectInt;
typedef TPicRect<float> PickRectFloat;
typedef TPicRect<double> PickRectDouble;

#endif //TPICKRECT_H_DEFINED
