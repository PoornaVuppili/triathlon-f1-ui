///////////////////////////////////////////////////////////////////////
//	File:    TCircle3.h
//	Purpose: A template class for 3D circle.
///////////////////////////////////////////////////////////////////////

#ifndef TCIRCLE3_H
#define TCIRCLE3_H

#include "TVector3.h"

template <typename T>
class TCircle3
{
public:
    // Construct the circle from center, normal, and radius
    TCircle3(const TPoint3<T>& c, const TVector3<T>& n, T rad)
        : center(c), nor(n), radius(rad)
    {
    }

    // Construct the circle from three points
    TCircle3(const TPoint3<T>& p1, const TPoint3<T>& p2, const TPoint3<T>& p3)
    {
        TVector3<T> t = p2-p1;
        TVector3<T> u = p3-p1;
        TVector3<T> v = p3-p2;
        TVector3<T> w = t.cross(u);

        T t2 = t.squaredNorm();
        T u2 = u.squaredNorm();
        T v2 = v.squaredNorm();
        T w2 = w.squaredNorm();

        center = p1+(t2*u.dot(v)*u-u2*t.dot(v)*t)/(2*w2);
        radius = .5*sqrt(t2*u2*v2/w2);
        //vx = t / sqrt(t2);
        //vy = (w / sqrt(w2)).cross(vx);
        nor = w / sqrt(w2);
    }

    // Get values
    const TPoint3<T>& GetCenter() const { return center; }
    const TVector3<T>& GetNormal() const { return nor; }
    T GetRadius() const { return radius; }

    // Set values
    void SetCenter(const TPoint3<T>& v) { center=v; }
    void SetNormal(const TVector3<T>& v) { nor=v; }
    void SetRadius(T v) { radius=v; }

    // Get x,y axis based on normal (one solution among many)
    void GetXYAxis(TVector3<T>& vx, TVector3<T>& vy) const;

protected:
    TPoint3<T> center;      // center
    TVector3<T> nor;        // normal
    T radius;               // radius
};

template<typename T> void TCircle3<T>::GetXYAxis(TVector3<T>& vx, TVector3<T>& vy) const
{
    vx = ( fabs(nor.x())>fabs(nor.y()) ) ?
        nor.UnitCross( fabs(nor.x())>fabs(nor.z()) ? TVector3<T>(nor.y(), nor.x(), nor.z()) : TVector3<T>(nor.z(), nor.y(), nor.x()) ) :
        nor.UnitCross( fabs(nor.y())>fabs(nor.z()) ? TVector3<T>(nor.x(), nor.z(), nor.y()) : TVector3<T>(nor.z(), nor.y(), nor.x()) );
    vy = nor.UnitCross( vx );
}

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TCircle3<int>       Circle3Int;
typedef TCircle3<float>     Circle3Float;
typedef TCircle3<double>    Circle3Double;

#endif //TCIRCLE3_H
