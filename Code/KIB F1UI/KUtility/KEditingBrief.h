#ifndef KSPINBOXBRIEF_H
#define KSPINBOXBRIEF_H

///////////////////////////////////////////////////////////////////////////////////////
// Widgets that provides the in-line editing for an item in the QStandardItemModel. 
// The widget deletes itself once it loses the focus. The widget can get the focus by 
// clicking, and lose the focus by hitting the 'esc' key.
///////////////////////////////////////////////////////////////////////////////////////

#include <QSpinBox>
#include <QComboBox>

class KSpinBoxBrief : public QSpinBox
{
    Q_OBJECT

public:
    KSpinBoxBrief(QWidget *parent=NULL);
    ~KSpinBoxBrief();

protected:
    virtual void focusOutEvent(QFocusEvent * event);
    // hit Esc -> lose focus
    virtual void keyPressEvent(QKeyEvent * event);
};

class KComboBoxBrief : public QComboBox
{
    Q_OBJECT

public:
    KComboBoxBrief(QWidget *parent=NULL);
    ~KComboBoxBrief();

protected:
    virtual void focusOutEvent(QFocusEvent * event);
    // hit Esc -> lose focus
    virtual void keyPressEvent(QKeyEvent * event);
};

#endif // KSPINBOXBRIEF_H
