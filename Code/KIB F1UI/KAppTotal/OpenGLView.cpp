#include <QtGui>
#include <QtOpenGL>
#include "Basics.h"
#include "OpenGLView.h"
#include "DataDoc.h"
#include "RangeSlider.h"
#include "..\KApp\ViewWidget.h"
#include "..\KApp\Viewport.h"
#pragma warning( disable : 4996 4805 )
#include <IwVertex.h>
#pragma warning( default : 4996 4805 )

COpenGLView::COpenGLView( QObject *parent )
    : EntityView( parent )
{
	m_pDoc					= NULL;
	m_pManager				= NULL;
	m_bFrontBuffer			= false;

	m_bDebugFlag			= false;

    m_AdditionViewingRotations = IwPoint3d(0, 0, 0);

	m_clipPlaneEqn[0]		= 0;
	m_clipPlaneEqn[1]		= 0;
	m_clipPlaneEqn[2]		= 0;
	m_clipPlaneEqn[3]		= 0;

	m_preDefinedView		= NONE_VIEW;
	SetMotionStudioMatrix(IwAxis2Placement());// identity matrix
	SetShowIntersection(true);// To support image intersection
	m_intersectedEntities = 0;

}


COpenGLView::~COpenGLView()
{
}


void COpenGLView::Draw()
{
	// **** We only visually flex/rotate femur components listed in Doc entities. 
	// **** We do not flex/rotate any objects displayed in managers.
	glPushMatrix();
	glMultMatrixd(m_motionStudioMatrix);

    m_pDoc->Display();

	glPopMatrix();

    if( m_pManager )
	{
		if( m_pManager->DestroyMe() )
		{
			delete m_pManager;
		}
		else
		{
			m_pManager->Display();
						
			for ( int i=0; i<m_pManagerStack.size(); i++ )
			{
				CManager* prevStackManager = m_pManagerStack.at(i);
				prevStackManager->Display();
			}
		}
	}
}

void COpenGLView::DrawTransparent()
{
    glPushMatrix();
	glMultMatrixd(m_motionStudioMatrix);

    m_pDoc->DisplayTransparent();

	glPopMatrix();
}

void COpenGLView::DisplayIntersection(int startIndex, int endIndex, IwPoint3d planePnt, IwVector3d planeNormal)
{
	// Get display list and display it.
	if ( (int)m_intersectionDisplayLists.GetSize() > startIndex && (int)m_intersectionDisplayLists.GetSize() > endIndex )
	{
		GLuint displayList = m_intersectionDisplayLists.GetAt(startIndex);
		if ( glIsList(displayList) ) // If displayList at startIndex is valid, displayLists between [startIndex,endIndex] will be all valid.
		{
			CColor clr;
			for (int i=startIndex; i<=endIndex; i++)
			{
				displayList = m_intersectionDisplayLists.GetAt(i);
				clr = m_intersectionDisplayColors.GetAt(i);
				glColor3ub(clr[0], clr[1], clr[2]);
				glCallList(displayList);
			}
			return;
		}
	}

	// Not valid display lists, create them.
	CreateDisplayListsForEntitiesCrossSections(startIndex, planePnt, planeNormal);	
}

void COpenGLView::CreateDisplayListsForEntitiesCrossSections(int startIndex, IwPoint3d planePnt, IwVector3d planeNormal)
{
	// plane info
	IwPoint3d planeInfo = planePnt + planeNormal;

	// Determine cross section curves
	IwTArray<IwCurve*> crossSectionCurves, allCrossSectionCurves;

	int entNo = m_pDoc->GetNumOfEntities();
	CEntity* ent;
	CDisplayMode dispMode;
	CColor clr, brightColor;
	QColor QClr;
	IwTArray<IwBrep*> Breps;
	IwTArray<IwCurve*> curves;
	IwTArray<IwPoint3d> points;
	IwBrep *brep;
	IwCurve* crv;
	double length;
	double tol = 0.3, aTol = 15, tol3D = 0.5;
	double interTol(0.01);
	IwTArray<IwPoint3d> pnts;
	IwPoint3d pt;
	int visibleEntityCount = 0;
	GLuint displayList;

	for (int i=0; i<entNo; i++)
	{
		ent = m_pDoc->GetEntity(i);
		dispMode = ent->GetDisplayMode();
		if ( dispMode != DISP_MODE_HIDDEN && dispMode != DISP_MODE_NONE ) // visible entity
		{
			clr = ent->GetColor();
			QClr.setRgb(clr[0], clr[1], clr[2]);
			QClr.lighter();
			brightColor = CColor(QClr.red(),QClr.green(),QClr.blue());
			glColor3ub(brightColor[0], brightColor[1], brightColor[2]);
			Breps.RemoveAll();
			curves.RemoveAll();
			points.RemoveAll();
			ent->GetSelectableEntities(Breps, curves, points);
			//// Create one display list for one entity
			displayList = glGenLists(1);
			glNewList(displayList, GL_COMPILE_AND_EXECUTE);

			for (unsigned j=0; j<Breps.GetSize(); j++)
			{
				crossSectionCurves.RemoveAll();
				brep = Breps.GetAt(j);
				brep->CreatePlanarSectionCurves(m_pDoc->GetIwContext(), planePnt, planeNormal, &interTol, NULL, &crossSectionCurves, NULL, NULL);
				for (unsigned k=0; k<crossSectionCurves.GetSize(); k++)
				{
					crv = crossSectionCurves.GetAt(k);
					crv->Length(crv->GetNaturalInterval(), interTol, length);
					if ( length > 0.1 )
					{
						crv->TessellateByBisection(crv->GetNaturalInterval(), tol, aTol, tol3D, NULL, &pnts);
						glBegin(GL_LINE_STRIP);
						for(ULONG j=0; j<pnts.GetSize(); j++)
						{
							pt = pnts.GetAt(j);
							glVertex3d(pt.x, pt.y, pt.z);
						}
						glEnd();
					}
				}
				allCrossSectionCurves.Append(crossSectionCurves);
			}

			glEndList();
			//// End of display list creation
			// Each displayed entity has the following 3 properties - displayList, color, planeInfo.
			m_intersectionDisplayLists.SetAt(startIndex+visibleEntityCount, displayList);
			m_intersectionDisplayColors.SetAt(startIndex+visibleEntityCount, brightColor);
			m_intersectionPlanesInfo.SetAt(startIndex+visibleEntityCount, planeInfo);
			visibleEntityCount++;
		}
	}

	// Delete curves
	for (unsigned i=0; i<allCrossSectionCurves.GetSize(); i++)
	{
		crv = allCrossSectionCurves.GetAt(i);
		IwObjDelete(crv);
	}

}

void COpenGLView::ClearIntersectionDisplayListAndPlaneInfo(int index)
{
	if ( index < 0 ) // clear all
	{
		for (unsigned i=0; i<m_intersectionDisplayLists.GetSize(); i++)
		{
			// delete gl display list
			GLuint dList = m_intersectionDisplayLists.GetAt(i);
			glDeleteLists( dList, 1 );
		}
		m_intersectionDisplayLists.RemoveAll();
		m_intersectionDisplayColors.RemoveAll();
		m_intersectionPlanesInfo.RemoveAll();
	}
	else if ( index < (int)m_intersectionDisplayLists.GetSize() )
	{
		// delete gl display list
		GLuint dList = m_intersectionDisplayLists.GetAt(index);
		glDeleteLists( dList, 1 );
		// reset 
		m_intersectionDisplayLists.SetAt(index, 0);
		m_intersectionDisplayColors.SetAt(index, CColor());
		m_intersectionPlanesInfo.SetAt(index, IwPoint3d());// un-initialized point
	}
	return;
}

void COpenGLView::DisplayPrompt()
{
	QFont	fontPrompt( "Ariel", 12, 10, true );
	int		iy = 22;

	glDrawBuffer( GL_FRONT );

	glDisable( GL_LIGHTING );

	glColor3ub( 255, 255, 255 );

	GetViewWidget()->renderText( 0, iy, m_sPrompt, fontPrompt );

	glEnable( GL_LIGHTING );

	glFlush();

	glDrawBuffer( GL_BACK );
}


void COpenGLView::SetPrompt( const QString& sPrompt )
{
	m_sPrompt = sPrompt;
}


void COpenGLView::Redraw()
{
	GetViewWidget()->updateGL();
}

void COpenGLView::Enter2dMode( bool bFrontBuffer )
{
	m_bFrontBuffer = bFrontBuffer;

	if( m_bFrontBuffer )
		glDrawBuffer( GL_FRONT );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0, 1, 0, 1, -1, 1 );
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glPushAttrib( GL_ENABLE_BIT );
	glDisable( GL_LIGHTING );
	glDisable( GL_DEPTH_TEST );

	//for( int i = 0; i < 6; i++ )
	//	glDisable( GL_CLIP_PLANE0 + i );
}

void COpenGLView::Restore3dMode()
{
	glPopAttrib();
	glPopMatrix();
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	
	if( m_bFrontBuffer )
		glDrawBuffer( GL_BACK );

	//for( int i = 0; i < 6; i++ )
	//	{
	//	if( m_ClipPlanes[i].bActive && m_bClippingEnabled )
	//		glEnable( GL_CLIP_PLANE0 + i );
	//	}
}


void COpenGLView::SetXOR()
{
	glLogicOp( GL_XOR );
	glEnable( GL_COLOR_LOGIC_OP );
}


void COpenGLView::ClearXOR()
{
	glLogicOp( GL_COPY );
	glDisable( GL_LOGIC_OP );
}

CManager* COpenGLView::GetManager()
{
	return m_pManager;
}

void COpenGLView::SetManager(CManager* manager, bool stacking)
{
	if ( stacking )
	{
		CManager* prevManager = this->GetManager();

		if( prevManager )
			this->PushManagerInStack(prevManager);
	}

	m_pManager = manager;
}

void COpenGLView::PushManagerInStack(CManager*& manager)
{
	m_pManagerStack.push( manager );
}

CManager* COpenGLView::PopManagerFromStack()
{
	return m_pManagerStack.isEmpty() ? NULL : m_pManagerStack.pop();
}

CManager* COpenGLView::GetLastManager()
{
	CManager* lastManager;
	if( m_pManagerStack.isEmpty() )
		lastManager = NULL;
	else
		lastManager = m_pManagerStack.last();

	return lastManager;
}

void COpenGLView::ClearView()
{
	ClearManagers();
	SetAdditionViewingRotations(0,0,0);
	double eqn[4]={0,0,0,0};
	SetClipPlaneInfo(eqn);
	SetMotionStudioMatrix(IwAxis2Placement());
	m_intersectedEntities = 0;
	m_intersectionDisplayLists.RemoveAll();
	m_intersectionPlanesInfo.RemoveAll();
}

void COpenGLView::ClearManagers()
{
	if (m_pManager)
		delete m_pManager;

	while ( !m_pManagerStack.isEmpty() )
	{
		CManager* stackManager;
		stackManager = PopManagerFromStack();
		if (stackManager)
			delete stackManager;
	}

}

void COpenGLView::SetCursorAndPrompt( CCursorType eCursorType, QString sPrompt )
{
	SetCursor( eCursorType );

	m_sPrompt = sPrompt;
}


void COpenGLView::ClearCursorAndPrompt()
{
	SetCursor( CURSOR_STANDARD );

	m_sPrompt = "";	
}


CCursorType COpenGLView::GetCursorType()
{
	return m_eCursorType;
}


void COpenGLView::SetSelectionMode( const QString& sText )
{
	SetCursorAndPrompt( CURSOR_SELECTION, sText );
}


void COpenGLView::SetQuestionMode( const QString& sText )
{
	SetCursorAndPrompt( CURSOR_QUESTION, sText );
}


void COpenGLView::SetSketchMode( const QString& sText )
{
	SetCursorAndPrompt( CURSOR_SKETCH, sText );
}


void COpenGLView::SetCursor( CCursorType eCursorType )
{
	m_eCursorType = eCursorType;

	switch( m_eCursorType )
	{
		case CURSOR_NONE:
		{
		}

		case CURSOR_STANDARD:
		{
			GetViewWidget()->setCursor( Qt::ArrowCursor );
			break;
		}

		case CURSOR_SELECTION:
		{
			GetViewWidget()->setCursor( Qt::CrossCursor );
			break;
		}

		case CURSOR_QUESTION:
		{
			GetViewWidget()->setCursor( Qt::ArrowCursor );
			break;
		}

		case CURSOR_SKETCH:
		{
		}
	}
}


void COpenGLView::Zoom( QPoint point0, QPoint point1 )
{
	int w = abs( point1.x() - point0.x() );
	int h = abs( point1.y() - point0.y() );
}


void COpenGLView::XYZtoUV( IwPoint3d& pt, int uv[2] )
{
    QPoint res = GetViewWidget()->GetViewport()->XYZtoUV( Point3Float(pt[0], pt[1], pt[2]) );
    uv[0] = res.x();
    uv[1] = res.y();
}


void COpenGLView::XYZtoUVW( IwPoint3d& pt, int uvw[3] )
{
    GetViewWidget()->GetViewport()->XYZtoUVW( Point3Float(pt[0], pt[1], pt[2]), uvw);
}


void COpenGLView::UVtoXYZ( int uv[2], IwPoint3d& pt )
{
    UVtoXYZ( QPoint(uv[0], uv[1]), pt);
}


void COpenGLView::UVWtoXYZ( int uvw[3], IwPoint3d& pt )
{
    Point3Float pf;
    GetViewWidget()->GetViewport()->UVWtoXYZ(uvw, pf);
    pt[0] = pf.GetX();
    pt[1] = pf.GetY();
    pt[2] = pf.GetZ();
}


void COpenGLView::UVtoXYZ( QPoint point, IwPoint3d& pt )
{
    Point3Float pf = GetViewWidget()->GetViewport()->UVtoXYZ(point);
    pt[0] = pf.GetX();
    pt[1] = pf.GetY();
    pt[2] = pf.GetZ();
}


void COpenGLView::UVto2D( QPoint point, double& x, double& y )
{
    int minX, maxX, minY, maxY;
    GetViewWidget()->GetViewport()->GetViewport(minX, maxX, minY, maxY);
    x = ((double) point.x() - minX) / (maxX - minX);
    y = ((double) GetViewWidget()->height()-1 - point.y() - minY) / (maxY - minY);
}


void COpenGLView::UVto2D( int uv[2], double& x, double& y )
{
    return UVto2D( QPoint(uv[0],uv[1]), x, y );
}


void COpenGLView::SetPredefinedView( int eView, double scale, bool doPlusView )
{
	CTransform m_trfRot = CTransform();

	// to correct the predefined view to align with femoral axes
	m_trfRot.Rotate(-m_AdditionViewingRotations[2], 0.0, 0.0, 1.0);
	m_trfRot.Rotate(-m_AdditionViewingRotations[1], 0.0, 1.0, 0.0);
	m_trfRot.Rotate(-m_AdditionViewingRotations[0], 1.0, 0.0, 0.0);

	switch( eView )
	{
		case FRONT_VIEW:
			if ( m_preDefinedView == FRONT_VIEW && doPlusView )
			{
				m_trfRot.Rotate( 90.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = FRONT_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( 90.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = FRONT_VIEW;
			}
			break;

		case BACK_VIEW:
			if ( m_preDefinedView == BACK_VIEW && doPlusView )
			{
				m_trfRot.Rotate( -90.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = BACK_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( -90.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = BACK_VIEW;
			}
			break;

		case TOP_VIEW:
			if ( m_preDefinedView == TOP_VIEW && doPlusView )
			{
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = TOP_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = TOP_VIEW;
			}
			break;

		case BOTTOM_VIEW:
			if ( m_preDefinedView == BOTTOM_VIEW && doPlusView )
			{
				m_trfRot.Rotate( 180.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = BOTTOM_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( 180.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = BOTTOM_VIEW;
			}
			break;

		case LEFT_VIEW:
			if ( m_preDefinedView == LEFT_VIEW && doPlusView )
			{
				m_trfRot.Rotate(  90.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( -90.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = LEFT_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate(  90.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( -90.0, 1.0, 0.0, 0.0 );
				m_preDefinedView = LEFT_VIEW;
			}
			break;
		
		case RIGHT_VIEW:
			if ( m_preDefinedView == RIGHT_VIEW && doPlusView )
			{
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate(  90.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( -90.0, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_preDefinedView = RIGHT_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( 180.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate(  90.0, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( -90.0, 1.0, 0.0, 0.0 );
				m_preDefinedView = RIGHT_VIEW;
			}
			break;

		case ISO1_VIEW:
			if ( m_preDefinedView == ISO1_VIEW && doPlusView )
			{
				m_trfRot.Rotate( 180, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( -175, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( -50, 0.0, 1.0, 0.0 );
				m_trfRot.Rotate( -9.1, 0.0, 0.0, 1.0 );
				m_preDefinedView = ISO1_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( 180, 0.0, 0.0, 1.0 );
				m_trfRot.Rotate( -175, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate(  40, 0.0, 1.0, 0.0 );
				m_trfRot.Rotate(  16.1, 0.0, 0.0, 1.0 );
				m_preDefinedView = ISO1_VIEW;
			}
			break;
		
		case ISO2_VIEW:
			if ( m_preDefinedView == ISO2_VIEW && doPlusView )
			{
				m_trfRot.Rotate( 30, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate( -30, 0.0, 1.0, 0.0 );
				m_trfRot.Rotate( -16.1, 0.0, 0.0, 1.0 );
				m_preDefinedView = ISO2_VIEW_PLUS;
			}
			else
			{
				m_trfRot.Rotate( 30, 1.0, 0.0, 0.0 );
				m_trfRot.Rotate(  30, 0.0, 1.0, 0.0 );
				m_trfRot.Rotate(  16.1, 0.0, 0.0, 1.0 );
				m_preDefinedView = ISO2_VIEW;
			}
			break;
	}

	CBounds3d		Bounds = GetDocBounds();

    IwPoint3d cent = ( Bounds.pt0 + Bounds.pt1 ) / 2.0;
    IwPoint3d span = (Bounds.pt1 - Bounds.pt0) * (.5/scale);

    Viewport* viewport = GetViewWidget()->GetViewport();

    // update the bounding box in the viewport
    Box3Float bb;
    bb.SetCenter( cent.x, cent.y, cent.z );
    bb.SetSpan(   span.x, span.y, span.z );
    viewport->SetBoundingBox(bb);
	
    // update the transform in the viewport
    CMatrix mat;
	m_trfRot.GetRotationMatrix(mat);
	double r[3][3];
	mat.GetMatrix(r);

    Rigid3Float tr;
    tr.Translate( -cent.x, -cent.y, -cent.z );
    tr.Rotate( Rotate3Float(r[0][0], r[0][1], r[0][2], r[1][0], r[1][1], r[1][2], r[2][0], r[2][1], r[2][2]) );
    viewport->SetTransform( tr );

	Redraw();
}


void COpenGLView::SetDebugFlag( bool bFlag )
{
	m_bDebugFlag = bFlag;
}

void COpenGLView::DrawMarker( IwPoint3d& pt )
{
	HWND		hWnd = (HWND)GetViewWidget()->winId();
	HDC			hDC = GetDC( hWnd );
	int			i, uv[2];

	XYZtoUV( pt, uv );

	SetROP2( hDC, R2_NOTXORPEN );

	HGDIOBJ		original = NULL;

	original = SelectObject( hDC, GetStockObject( BLACK_PEN ) );
	
	int			dx[11] = { -5, -4, -3, -2, -1,  0,  1,  2,  3,  4,  5 };
	int			dy[11] = {  1,  3,  4,  4,  5,  5,  5,  4,  4,  3,  1 };

	for( i = 0; i < 12; i++ )
	{
		MoveToEx( hDC, uv[0] + dx[i], uv[1] - dy[i], 0 );
		LineTo(   hDC, uv[0] + dx[i], uv[1] + dy[i] );
	}

	SelectObject( hDC, original );
	
	SetROP2( hDC, R2_COPYPEN );

	ReleaseDC( hWnd, hDC );
} 


/////////////////////////////////////////////////////////////////////
// The viewing vector is defined as follows:
// screenDir=0: screen x direction, from left to right of screen 
// screenDir=1: screen y direction, from top to down of screen 
// screenDir=2: screen z direction, from viewer's eye toward the screen (right hand rule of x and y dirs)
/////////////////////////////////////////////////////////////////////
IwVector3d COpenGLView::GetViewingVector(int screenDir,  bool unitized)
{
    Viewport* vp = GetViewWidget()->GetViewport();
    Rotate3Float ir = vp->GetTransform().GetRotationMatrix().Inverse();
    const float unit = vp->GetResolution_MMPerPixel();

    Point3Float v;
    if ( screenDir == 0 )
        v = ir * Point3Float(unit,0,0);
	else if ( screenDir == 1 )
        v = ir * Point3Float(0,-unit,0);
	else if ( screenDir == 2 )
        v = ir * Point3Float(0,0,-unit);

    IwVector3d viewingVector(v.x(), v.y(), v.z());
    
	if ( unitized )
		viewingVector.Unitize();

	return viewingVector;
}

//////////////////////////////////////////////////////////
// This function returns the offset distance from the given
// pnt to the desired layer. 
// The z length of glOrtho() is divided into 1000 layers
// (0th, 1st, ... 999th). the 0th is the one closest to
// the viewers.
//////////////////////////////////////////////////////////
IwVector3d COpenGLView::GetLayerOffsetVector(int layerNo, IwPoint3d pnt)
{
	Viewport* vp = GetViewWidget()->GetViewport();
	Box3Float boundingBox;
	vp->GetBoundingBox(boundingBox);
	Point3Float ct = boundingBox.GetCenter();
	IwPoint3d centerPoint = IwPoint3d(ct.x(), ct.y(), ct.z());
	float le, ri, bo, to, ne, fa;
	vp->GetViewRange(le, ri, bo, to, ne, fa);

	double deltaZdepth = 0.98*(fa - ne)/1000.0;  // 0.98

	IwVector3d viewVector = GetViewingVector();// toward screen

	IwVector3d offsetVector = -(500 - layerNo)*deltaZdepth*viewVector;// offsetVector to offset from center to this layer

	IwPoint3d layerPoint = centerPoint + offsetVector; // layerPoint - the point in this layer

	double offsetDist;
	if (pnt.IsInitialized())
		offsetDist = -viewVector.Dot(layerPoint - pnt);
	else
		offsetDist = -viewVector.Dot(layerPoint - centerPoint);

	IwVector3d layerOffsetVector = -offsetDist*viewVector;

	return layerOffsetVector;
}

void COpenGLView::SetClipPlaneInfo( double* eqn )
{

	m_clipPlaneEqn[0] = eqn[0];
	m_clipPlaneEqn[1] = eqn[1];
	m_clipPlaneEqn[2] = eqn[2];
	m_clipPlaneEqn[3] = eqn[3];

}

void COpenGLView::SetClipPlane(bool enable)
{
	int			idClipPlane = GL_CLIP_PLANE0 + 1;
	if ( !enable )
	{
		glDisable( idClipPlane );
	}
	else if ( m_clipPlaneEqn[0]==0 && m_clipPlaneEqn[1]==0 && m_clipPlaneEqn[2]==0 && m_clipPlaneEqn[3]==0 )
	{
		glDisable( idClipPlane );
	}
	else
	{
		glClipPlane( idClipPlane, m_clipPlaneEqn );
		glEnable( idClipPlane );
	}

}
/*
void COpenGLView::OnBackGroundColorTop() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnBackGroundColorTop()") );

    QColor      c = GetViewWidget()->GetTopColor();
	QColor		qcolor = QColorDialog::getColor( c, viewWidget );

	if( qcolor.isValid() && qcolor != c )
        viewWidget->SetTopColor( qcolor );
}

void COpenGLView::OnBackGroundColorBottom() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnBackGroundColorBottom()") );

    QColor      c = viewWidget->GetBottomColor();
	QColor		qcolor = QColorDialog::getColor( c, viewWidget );

	if( qcolor.isValid() && qcolor != c )
        viewWidget->SetBottomColor( qcolor );
}

void COpenGLView::OnBackGroundColorDefault() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnBackGroundColorDefault()") );

    viewWidget->SetTopColor( QColor( 50, 140, 240 ) );
    viewWidget->SetBottomColor( QColor( 200, 255, 255 ) );
}
*/
void COpenGLView::OnTessellationHigh() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTessellationHigh()") );

	m_pDoc->SetTessellationFactor(0.1);
}

void COpenGLView::OnTessellationFine() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTessellationFine()") );

	m_pDoc->SetTessellationFactor(0.33);
}

void COpenGLView::OnTessellationNormal() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTessellationNormal()") );

	m_pDoc->SetTessellationFactor(1.0);
}

void COpenGLView::OnTessellationCoarse() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTessellationCoarse()") );

	m_pDoc->SetTessellationFactor(3.0);
}

void COpenGLView::OnTransparencyTransparentTotal() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTransparencyTransparentTotal()") );

	m_pDoc->SetTransparencyFactor(0);
}

void COpenGLView::OnTransparencyTransparentPlus() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTransparencyTransparentPlus()") );

	m_pDoc->SetTransparencyFactor(16);
}

void COpenGLView::OnTransparencyTransparent() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTransparencyTransparent()") );

	m_pDoc->SetTransparencyFactor(32);
}

void COpenGLView::OnTransparencyNormal() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTransparencyNormal()") );

	m_pDoc->SetTransparencyFactor(64);
}

void COpenGLView::OnTransparencyOpaque() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTransparencyOpaque()") );

	m_pDoc->SetTransparencyFactor(144);
}

void COpenGLView::OnTransparencyOpaquePlus() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnTransparencyOpaquePlus()") );

	m_pDoc->SetTransparencyFactor(224);
}

void COpenGLView::OnArrowKeyRotation() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnArrowKeyRotation()") );

}

void COpenGLView::OnArrowKeyRotationPP() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnArrowKeyRotationPP()") );

}

void COpenGLView::OnArrowKeyRotationP() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnArrowKeyRotationP()") );

}

void COpenGLView::OnArrowKeyRotationM() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnArrowKeyRotationM()") );

}

void COpenGLView::OnArrowKeyRotationMM() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnArrowKeyRotationMM()") );

}

void COpenGLView::OnArrowKeyRotationMMM() 
{
	m_pDoc->AppendLog( QString("COpenGLView::OnArrowKeyRotationMMM()") );

}

IwExtent3d COpenGLView::GetShowRegion()
{
	CTransform transF = CTransform();
	// Need to transform, if m_motionStudioMatrix != identity
	IwAxis2Placement tranMatrix;
	bool bIdentity = GetMotionStudioMatrix(tranMatrix);
	if ( !bIdentity )
	{
		transF = CTransform::IwAxis2Placement2CTransform(tranMatrix);
	}

    IwExtent3d ext;

    CBounds3d bnd = m_pDoc->ComputeBounds(transF);
    ext.AddPoint3d( bnd.pt0 );
    ext.AddPoint3d( bnd.pt1 );
    if( ext.GetMaxDimension()<0.001 )   // use the same threshold as CBound3d class
        ext.Init(); // use the init value if empty

	// Also update region, entity view data member
	region = ext;

    return ext;
}

void COpenGLView::UpdateShowRegion()
{
	GetShowRegion();

	emit showChanged(true); 
}

void COpenGLView::DisplayPoint( const IwPoint3d& pt, const CColor& color )
{
	IwVector3d			vx( 1.0, 0.0, 0.0 );
	IwVector3d			vy( 0.0, 1.0, 0.0 );
	IwVector3d			vz( 0.0, 0.0, 1.0 );
	double				d;
	IwPoint3d			x0, x1, y0, y1, z0, z1;

	d = 3.0 * viewWidget->GetViewport()->GetResolution_MMPerPixel();

	glDisable( GL_LIGHTING );

	glColor3ub( color[0], color[1], color[2] );

	x0 = pt - d * vx;
	x1 = pt + d * vx;

	y0 = pt - d * vy;
	y1 = pt + d * vy;

	z0 = pt - d * vz;
	z1 = pt + d * vz;

	glLineWidth( 2.0 );

	glBegin( GL_LINES );

	glVertex3d( x0.x, x0.y, x0.z );
	glVertex3d( x1.x, x1.y, x1.z );

	glVertex3d( y0.x, y0.y, y0.z );
	glVertex3d( y1.x, y1.y, y1.z );

	glVertex3d( z0.x, z0.y, z0.z );
	glVertex3d( z1.x, z1.y, z1.z );

	glEnd();

	glLineWidth( 1.0 );

	glEnable( GL_LIGHTING );
}

CBounds3d COpenGLView::GetDocBounds()
{
	return m_pDoc->GetBounds();
}

CBounds3d COpenGLView::ComputeDocBounds( const CTransform& Trf, CBounds3d* pClipBounds )
{
	return m_pDoc->ComputeBounds( Trf, pClipBounds );
}


bool COpenGLView::IsDocEmpty()
{
	return m_pDoc->IsEmpty();
}


bool COpenGLView::IsDocModified()
{
	return m_pDoc->IsModified();
}

void COpenGLView::SetMotionStudioMatrix(IwAxis2Placement const& matrix)
{
	matrix.Load4x4(m_motionStudioMatrix);
}

bool COpenGLView::GetMotionStudioMatrix(IwAxis2Placement& matrix)
{
	IwPoint3d origin = IwPoint3d(m_motionStudioMatrix[12], m_motionStudioMatrix[13], m_motionStudioMatrix[14]);
	IwVector3d xAxis = IwVector3d(m_motionStudioMatrix[0], m_motionStudioMatrix[1], m_motionStudioMatrix[2]);
	IwVector3d yAxis = IwVector3d(m_motionStudioMatrix[4], m_motionStudioMatrix[5], m_motionStudioMatrix[6]);
	matrix = IwAxis2Placement(origin, xAxis, yAxis);

	bool identity = ( matrix == IwAxis2Placement() );

	return identity;
}

// Pick a point for measurement
bool COpenGLView::Measure(const PickRayFloat& ray, float tolerance, Point3Float& minPt)
{
    IwTArray<IwBrep*>			selectableBreps;
    IwTArray<bool>				selectableBrepsAreOpaque;
	IwTArray<IwCurve*>			selectableCurves;
    IwTArray<bool>				selectableCurvesToBeDeleted;
	IwTArray<IwPoint3d>			selectablePoints;

    // get the selectable entities.
	m_pDoc->GetSelectableEntities(selectableBreps, selectableBrepsAreOpaque, selectableCurves, selectablePoints);

    unsigned i;
    bool find = false;
    Point3Float pt;
    float d, minDist;

    Point3Float cen = ray.GetRayCen();
    Line3Float line = ray.GetRayLine();

    // go through the selectable points
    for(i=0; i<selectablePoints.GetSize(); i++)
    {
        IwPoint3d& pnt = selectablePoints[i];
        Point3Float pt(pnt.x, pnt.y, pnt.z);

        if( line.Distance(pt) < tolerance )
        {
            d = cen.Distance(pt);
            if( !find || d < minDist )
            {
                find = true;
                minDist = d;
                minPt = pt;
            }
        }
    }

    // go through the selectable breps
    for(i=0; i<selectableBreps.GetSize(); i++)
    {
        IwTArray<IwVertex*> vertices;
        selectableBreps[i]->GetVertices(vertices);

        for (unsigned int j=0; j<vertices.GetSize(); j++)
		{
			IwPoint3d pnt = vertices[j]->GetPoint();
            Point3Float pt(pnt.x, pnt.y, pnt.z);

            if( line.Distance(pt) < tolerance )
            {
                d = cen.Distance(pt);
                if( !find || d < minDist )
                {
                    find = true;
                    minDist = d;
                    minPt = pt;
                }
            }
		}
    }

    return find;
}

bool COpenGLView::Measure
(
	const PickRayFloat& ray,	// I:
	float tolerance,			// I:
	MeasureEntity& ent,			// O:
	MeasureSelectType mask,		// I:
	MeasureEntity* EntToAvoid	// I: the entity to avoid picking
)
{
    IwTArray<IwBrep*>			selectableBreps;
	IwTArray<bool>				selectableBrepsAreOpaque;
	IwTArray<IwCurve*>			selectableCurves;
	IwTArray<IwPoint3d>			selectablePoints;

    m_pDoc->GetSelectableEntities(selectableBreps, selectableBrepsAreOpaque, selectableCurves, selectablePoints);

    IwPoint3d  rayCen = ray.GetRayCen();
	IwVector3d rayVector = ray.GetRayDir();
	PickRayFloat myRay = ray;
	IwAxis2Placement transMatrix;
	bool isIdentity = GetMotionStudioMatrix(transMatrix);
	if ( !isIdentity )
	{
		IwAxis2Placement invertMatrix;
		transMatrix.Invert(invertMatrix);
		invertMatrix.TransformPoint(rayCen, rayCen);
		invertMatrix.TransformVector(rayVector, rayVector);
		myRay.SetRayCen(Point3Float(rayCen.x,rayCen.y,rayCen.z));
		myRay.SetRayDir(Point3Float(rayVector.x,rayVector.y,rayVector.z));
	}

    ULONG i,nBrep,nCurve;
    bool found = false;
    double dist,mindist;
    MeasureEntity e;

    nBrep = selectableBreps.GetSize();
    for(i=0; i<nBrep; i++)
    {
		if ( (mask & SelectFace) && !(mask & SelectFaceTransparent) )// opaque faces only
		{
			if ( selectableBrepsAreOpaque.GetAt(i) == false )
				continue;
		}
		else if ( !(mask & SelectFace) && (mask & SelectFaceTransparent) )// transparent faces only
		{
			if ( selectableBrepsAreOpaque.GetAt(i) == true )
				continue;
		}
        IwBrep* brep = selectableBreps.GetAt(i);
        if( MeasureEntity::Select(brep, myRay, tolerance, e, mask, EntToAvoid) )
        {
            dist = rayCen.DistanceBetween(e.GetHitPoint());
            if( !found || dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = e;
            }
        }
    }

    nCurve = selectableCurves.GetSize();
    if( MeasureEntity::Select(selectableCurves,myRay, tolerance, e, mask, EntToAvoid) )
    {
        dist = rayCen.DistanceBetween(e.GetHitPoint());
        if( !found || dist<mindist )
        {
            found = true;
            mindist = dist;
            ent = e;
        }
    }

    if( MeasureEntity::Select(selectablePoints, myRay, tolerance, e, mask, EntToAvoid) )
    {
        dist = rayCen.DistanceBetween(e.GetHitPoint());
        if( !found || dist<mindist )
        {
            found = true;
            mindist = dist;
            ent = e;
        }
    }

	// Transform
	if (found && !isIdentity)
	{
		ent.Transform(transMatrix);
	}

    return found;
}
