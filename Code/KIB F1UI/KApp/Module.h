#ifndef MODULE_H
#define MODULE_H

#include <QObject>
class QMenu;
class QToolBar;
class QDomElement;
class OptionPage;
class QAction;

#include <vector>
using namespace std;

class TEST_EXPORT Module : public QObject
{
    Q_OBJECT

public:
    Module(QObject *parent=NULL);
    virtual ~Module();

    // Initialize... Pure virtual. Derived class can
    // - create menus and add actions specific to the module
    // - create toolbars ...................................
    virtual bool Initialize() = 0;

    // Enable/disable actions on the menus or toolbars
    virtual void EnableActions(bool enable);

    // Menu functions
    int  GetNumberOfMenus() const { return (int)menus.size(); }
    QMenu* GetMenu(int index);

    // Toolbar functions
    int  GetNumberOfToolBars() const { return (int)toolbars.size(); }
    QToolBar* GetToolBar(int index);

    // Get the version info: module name & version
    virtual QString GetVersionInfo() = 0;

    // Get the option pages of module settings
    virtual vector<OptionPage*> GetGlobalOptionPages()  { return vector<OptionPage*>(); }
    virtual vector<OptionPage*> GetImplantOptionPages() { return vector<OptionPage*>(); }

    virtual bool IsCapableOfAutoStep() const { return false; }
    virtual QAction *nextAction() { return nullptr; }
    virtual QAction *prevAction() { return nullptr; }
    virtual void DoRework() {}

    void ContinueToNextStep();
    void ContinueToPrevStep();

    virtual bool AllStepsComplete(std::string const &phase, std::string const &subphase) const = 0;
    virtual bool OutputResults() const /* = 0 TODO: must it be overridden ??? */ { return true; }

public slots:
    void onManagerDismissedByAccept();
    void onManagerDismissedByReviewNext();
    void onManagerDismissedByReviewPrev();
    void onManagerDismissedByRework();

protected:
    // Clear everything. The base class: delete all menus & toolbars
    virtual void ClearAll();

    vector<QMenu*> menus;       // menus
    vector<QToolBar*> toolbars; // toolbars
};

#endif // MODULE_H
