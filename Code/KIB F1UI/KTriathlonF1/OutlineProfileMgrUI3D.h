#pragma once

#include "OutlineProfileMgrUIBase.h"
///////////////////////////////////////////////////////////////
// This class implements the action of manager
// It contains all the action the OutlineProfile manager should take
// in case of the 3D Outline Profile editing
// See DefineOutlineProfile.h for more information
///////////////////////////////////////////////////////////////
class OutlineProfileMgrUI3D : public OutlineProfileMgrUIBase
{
	Q_OBJECT

public:
					OutlineProfileMgrUI3D(CTotalDoc* doc, CManagerActivateType manActType, CManager *owner, OutlineProfileMgrModel *model);
	virtual			~OutlineProfileMgrUI3D();

    virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* vp=NULL );

	virtual void	Display(Viewport* vp=NULL);
	void			DisplayViewingNormalRegion(bool bHighLight = false);
	void			DisplayCriticalTipThicknessPoints(IwVector3d offsetVector);

	virtual void	CalculateSketchSurfaceNormalInfo(IwBSplineSurface*& skchSurface);
	virtual void	DetermineViewingNormalRegion();

	virtual void	SetMouseMoveSearchingList();
	
	virtual void	SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData);
	static void		DetermineAntMedialProfileReferencePoint(CTotalDoc* pDoc, IwPoint3d& refPnt, double& refRadius, IwTArray<IwPoint3d>* refCirclePnts=NULL);

private:
	void			DetermineCriticalTipThicknessPoints();
	void			DetermineAnteriorPosteriorCutNormal();
	void			DetermineAnteriorAirBallDepthReferencePlaneAndMedialReferencePoint();
	void			DisplayAnteriorAirBallDepthReferencePlane();

public slots:
	virtual void	OnDispRuler();
	void			OnReviewPredefinedView();

signals:

protected:
	virtual void	UpdateSketchSurface();
	virtual void	Reset();

private:
	bool						m_bMouseMiddleDown;
	int							m_predefinedViewIndex;
	IwVector3d					m_viewingNormal;			// The view normal of the determined ViewingNormalRegion
	IwTArray<IwVector3d>		m_outlineSurfaceNormal;		// When determine the viewing normal region,
	IwTArray<IwPoint3d>			m_outlineSurfacePointsMinU;	// the outline surface normal and points are 
	IwTArray<IwPoint3d>			m_outlineSurfacePointsMaxU;	// repeatedly referred. Therefore, here we
															// prepare such info to save CPU time.
	IwTArray<IwPoint3d>			m_viewingNormalRegion;
	IwTArray<IwPoint3d>			m_manipulatingNormalRegion;
	IwVector3d					m_anteriorCutNormal;		// Runtime data for display purpose
	IwVector3d					m_posteriorCutNormal;		// Runtime data for display purpose
	IwTArray<IwPoint3d>			m_antAirBallReferencePlaneCorners; // Runtime data for display purpose
	IwTArray<IwPoint3d>			m_antMedialProfileReferencePoints; // Runtime data for display purpose
	IwTArray<IwPoint3d>			m_criticalTipThicknessPoints;
};
