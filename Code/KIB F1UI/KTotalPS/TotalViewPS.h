#pragma once

#include "..\KTriathlonF1\TotalView.h"

class KTotalDocPS;
class KTotalMainWindowPS;

class KTotalViewPS : public CTotalView
{
    Q_OBJECT

public:
    KTotalViewPS( QObject *parent = 0 );
    virtual ~KTotalViewPS();
	KTotalDocPS*		GetTotalDocPS() {return ((KTotalDocPS*)m_pDoc);};
	KTotalMainWindowPS*	GetMainWindow() {return ((KTotalMainWindowPS*)m_pMainWindow);};

public slots:

	bool				OnAutoInitializeCBSteps(int action=0);
	void				OnActivateNextReviewManager(CManager* pManagerToBeDeleted=NULL, CEntRole eRole=ENT_ROLE_IMPLANT_PS_NONE, bool bNext=true);
	void				OnGoToJigsReviewManager();

	void				OnAcceptValidationReportPS();
	void				OnPrintValidationReportPS(QString outputFileName=QString(""));
	QTextEdit*			GetValidationReportPSTextEdit(){return m_validationReportPSTextEdit;};

protected slots:
	void				OnSetModifiedMarkerToFemurPSFeatures();

protected:
	QDialog*			m_validationReportPSDlg;
	QTextEdit*			m_validationReportPSTextEdit;

};
