/********************************************************************************
** Form generated from reading UI file 'SelectImplant.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTIMPLANT_H
#define UI_SELECTIMPLANT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_SelectImplantClass
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QLabel *label_6;
    QLabel *label_7;
    QComboBox *comboBoxFemurImplant;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QPushButton *pushButtonBrowse;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *comboImplantList;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButtonOpenReformat;
    QPushButton *pushButtonCancel;
    QLabel *label;

    void setupUi(QDialog *SelectImplantClass)
    {
        if (SelectImplantClass->objectName().isEmpty())
            SelectImplantClass->setObjectName(QString::fromUtf8("SelectImplantClass"));
        SelectImplantClass->setWindowModality(Qt::ApplicationModal);
        SelectImplantClass->resize(645, 198);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SelectImplantClass->sizePolicy().hasHeightForWidth());
        SelectImplantClass->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(SelectImplantClass);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetFixedSize);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_6 = new QLabel(SelectImplantClass);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy1);
        label_6->setLayoutDirection(Qt::RightToLeft);
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_6, 0, 0, 1, 1);

        label_7 = new QLabel(SelectImplantClass);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setLayoutDirection(Qt::RightToLeft);
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_7, 3, 0, 1, 1);

        comboBoxFemurImplant = new QComboBox(SelectImplantClass);
        comboBoxFemurImplant->setObjectName(QString::fromUtf8("comboBoxFemurImplant"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(comboBoxFemurImplant->sizePolicy().hasHeightForWidth());
        comboBoxFemurImplant->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(comboBoxFemurImplant, 3, 1, 1, 2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit = new QLineEdit(SelectImplantClass);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);

        pushButtonBrowse = new QPushButton(SelectImplantClass);
        pushButtonBrowse->setObjectName(QString::fromUtf8("pushButtonBrowse"));

        horizontalLayout_2->addWidget(pushButtonBrowse);


        gridLayout_2->addLayout(horizontalLayout_2, 0, 2, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        comboImplantList = new QComboBox(SelectImplantClass);
        comboImplantList->setObjectName(QString::fromUtf8("comboImplantList"));
        sizePolicy2.setHeightForWidth(comboImplantList->sizePolicy().hasHeightForWidth());
        comboImplantList->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(comboImplantList);

        horizontalSpacer = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        gridLayout_2->addLayout(horizontalLayout_3, 2, 2, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(10);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);

        pushButtonOpenReformat = new QPushButton(SelectImplantClass);
        pushButtonOpenReformat->setObjectName(QString::fromUtf8("pushButtonOpenReformat"));
        pushButtonOpenReformat->setEnabled(false);

        horizontalLayout_5->addWidget(pushButtonOpenReformat);

        pushButtonCancel = new QPushButton(SelectImplantClass);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        horizontalLayout_5->addWidget(pushButtonCancel);


        gridLayout_2->addLayout(horizontalLayout_5, 4, 2, 1, 1);

        label = new QLabel(SelectImplantClass);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setLayoutDirection(Qt::RightToLeft);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label, 2, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);

        QWidget::setTabOrder(pushButtonOpenReformat, pushButtonCancel);

        retranslateUi(SelectImplantClass);

        pushButtonOpenReformat->setDefault(true);


        QMetaObject::connectSlotsByName(SelectImplantClass);
    } // setupUi

    void retranslateUi(QDialog *SelectImplantClass)
    {
        SelectImplantClass->setWindowTitle(QCoreApplication::translate("SelectImplantClass", "Select an implant", nullptr));
        label_6->setText(QCoreApplication::translate("SelectImplantClass", "Implant root:", nullptr));
        label_7->setText(QCoreApplication::translate("SelectImplantClass", "Femoral implant:", nullptr));
        pushButtonBrowse->setText(QCoreApplication::translate("SelectImplantClass", "Browse...", nullptr));
        pushButtonOpenReformat->setText(QCoreApplication::translate("SelectImplantClass", "Open", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("SelectImplantClass", "Cancel", nullptr));
        label->setText(QCoreApplication::translate("SelectImplantClass", "Serial:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SelectImplantClass: public Ui_SelectImplantClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTIMPLANT_H
