#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "FemurSurfaceMgr.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"
#include "..\KAppTotal\Part.h"
#include "FemoralPart.h"
#include "..\KAppTotal\Utilities.h"
#include "..\KTriathlonF1\TotalDoc.h"
#include "..\KTriathlonF1\TotalView.h"

#include "IFemurImplant.h"

CFemurSurfaceMgr::CFemurSurfaceMgr( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
	bool activateUI = (manActType==MAN_ACT_TYPE_EDIT);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CFemurSurfaceMgr";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;

	if ( activateUI )
	{
		m_toolbar = new QToolBar("Import Femur Surf");
		m_actImportFemurSurf = m_toolbar->addAction("Femur");
		SetActionAsTitle( m_actImportFemurSurf );
		// Reset
		m_actReset = m_toolbar->addAction("Reload");
		EnableAction( m_actReset, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );// No Cancel

	}

	CFemoralPart* femurSurf = m_pDoc->GetFemur();
	if ( femurSurf == NULL  ) // if no object
	{
		// Reset is the way to import the surface.
		Reset( activateUI );
	}
	else 
	{
		// Load the parameters
	}

	if ( !activateUI )
		OnAccept();

}

CFemurSurfaceMgr::~CFemurSurfaceMgr()
{
	CFemoralPart* femurSurf = m_pDoc->GetFemur();
	if ( femurSurf != NULL )
	{
		int previoisDesignTime = femurSurf->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		femurSurf->SetDesignTime(previoisDesignTime+thisDesignTime);
	}
}

void CFemurSurfaceMgr::CreateFemurSurfaceObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_FEMUR) == NULL )
	{
		CFemoralPart* pFemurSurf = new CFemoralPart(pDoc, ENT_ROLE_FEMUR);
		pDoc->AddEntity(pFemurSurf, true);
	}

	return;
}

void CFemurSurfaceMgr::CreateHipObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_HIP) == NULL )
	{
		CPart* pHip = new CPart(pDoc, ENT_ROLE_HIP);
		pDoc->AddEntity(pHip, true);
	}

	return;
}

void CFemurSurfaceMgr::OnReset()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////
// Reset is the way to import surface
//////////////////////////////////////////////////////////////////////
void CFemurSurfaceMgr::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CFemurSurfaceMgr::Reset()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	if ( m_pDoc->GetFemur() )
	{
		if (underDevelopment != NULL && QString("1") == underDevelopment)
		{
			QApplication::restoreOverrideCursor();
			int	button = QMessageBox::information(mainWindow, "Info", "Do you want to delete all design features?", QMessageBox::Yes, QMessageBox::No);
			if (button == QMessageBox::Yes)
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
				m_pDoc->DeleteEntitiesBelongToFemoralImplant();
				m_pDoc->DeleteEntitiesBelongToPSFeatures();
				m_pDoc->DeleteEntitiesBelongToJigs();
			}
			else
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
			}
		}
		else
		{
			QApplication::restoreOverrideCursor();
			int	button = QMessageBox::information(mainWindow, "Info", "Reload will delete all design features. Do you want to continue?", QMessageBox::Yes, QMessageBox::No);
			if (button == QMessageBox::Yes)
			{
				QApplication::setOverrideCursor( Qt::WaitCursor );
				m_pDoc->DeleteEntitiesBelongToFemoralImplant();
				m_pDoc->DeleteEntitiesBelongToPSFeatures();
				m_pDoc->DeleteEntitiesBelongToJigs();
			}
			else
			{
				return;
			}
		}
	}

    // Check if parts are available
    Implant* implant = mainWindow->GetImplant();

	IwBrep *femurBrep = NULL;

	QString surfDir = mainWindow->GetImplant()->GetInfo("SurfDir");
	QString ImplantName = mainWindow->GetImplant()->GetInfo("ImplantName");
	QString fileNameFemur = surfDir + QDir::separator() + ImplantName + QString("_Femur_Inner.IGS");
	femurBrep = ReadIGES(fileNameFemur);

	if (underDevelopment != NULL && QString("1") == underDevelopment)
	{
		QApplication::restoreOverrideCursor();

		// Read it from surface IGES
		surfDir = mainWindow->GetImplant()->GetInfo("SurfDir");
		ImplantName = mainWindow->GetImplant()->GetInfo("ImplantName");
		fileNameFemur = surfDir + QDir::separator() + ImplantName + QString("_Femur_Inner.IGS");
		femurBrep = ReadIGES(fileNameFemur);

		QApplication::setOverrideCursor(Qt::WaitCursor);
	}

    if( femurBrep==NULL )
    {
		QApplication::restoreOverrideCursor();
		QMessageBox::information(mainWindow, "Error", "Missing the femur surface");
        return;
    }

	Point3Float hipCenter;

	std::map<QString, IwPoint3d> pointMap = IFemurImplant::FemoralAxesInp_PointsDataFromStrykerLayout();

	IwPoint3d hipPoint = pointMap["HIP Point"];

	if(hipPoint.IsInitialized())
	{
		hipCenter = Point3Float(hipPoint);
	}

    if( !hipPoint.IsInitialized() )
    {
		QApplication::restoreOverrideCursor();
        QMessageBox::information(mainWindow, "Error", "Missing the hip center");
        return;
    }

	bool bOK = m_pDoc->ImportPart( femurBrep, ENT_ROLE_FEMUR );
	if ( !bOK ) // Need to check whether the femoral surface template is loaded correctly.
	{
		QApplication::restoreOverrideCursor();
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Femoral surface error!"), 
										tr("Femoral surface does not exist, or\nfemoral surface template is not correct, or\nfemoral surface quality is not good.\nCan not create the case."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return;
	}
	m_pDoc->GetPart(ENT_ROLE_FEMUR)->SetColor(CColor( 255, 202, 128 ));

	// For Hip
    bOK = m_pDoc->ImportPart( IwPoint3d(hipCenter.GetX(), hipCenter.GetY(), hipCenter.GetZ()), ENT_ROLE_HIP );
	if ( !bOK )
	{
		QApplication::restoreOverrideCursor();
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Hip error!"), 
										tr("Hip center does not exist."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return;
	}

	// Change the status sign of itself
	if ( activateUI )
		m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_FEMUR, false, false);

	m_pView->Redraw();

	QApplication::restoreOverrideCursor();
}

void CFemurSurfaceMgr::OnAccept()
{
	m_pDoc->AppendLog( QString("CFemurSurfaceMgr::OnAccept()") );

	CFemoralPart* pFemurSurface = m_pDoc->GetFemur();
	// It should be always up-to-date.
	if ( pFemurSurface && !pFemurSurface->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_FEMUR, true, false);

	CPart* pHip = m_pDoc->GetPart(ENT_ROLE_HIP);
	// It should be always up-to-date.
	if ( pHip && !pHip->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_HIP, true, false);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	m_bDestroyMe = true;
	m_pView->Redraw();
}


void CFemurSurfaceMgr::OnCancel()
{
	m_pDoc->AppendLog( QString("CFemurSurfaceMgr::OnCancel()") );

	m_bDestroyMe = true;
	m_pView->Redraw();
}
