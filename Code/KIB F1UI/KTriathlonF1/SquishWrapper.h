#pragma once

#include "TotalView.h"
#include "TotalDoc.h"

class CTotalView;
class CTotalDoc;

class TEST_EXPORT_TW CSquishWrapper
{
public:
    CSquishWrapper();
	~CSquishWrapper();

	static CSquishWrapper* GetEntryPoint();
	static void SetEntryPoint(CSquishWrapper* entryPoint);

	void SetDoc(CTotalDoc* doc) {m_pDoc = doc;};
	void SetView(CTotalView* view) {m_pView = view;};

	void SetFilePath(const QString& filePath);
	void SetFixedWidgetSize();

	bool FileLastModified(const QString& fileName, int secondsAgo);
	int FilesLastModified(const QString& fileDir, int secondsAgo);

	//bool IsEqual(double a, double b, double tol=0.000001);
	//double GetEntityDistance();
	//double GetEntityAngle();
	//double GetCurveRadius();
	//double GetScreenDistance();
	//double GetScreenAngle();
	//double GetScreenRadius();

private:

	CTotalDoc* GetDoc() {return m_pDoc;};
	CTotalView* GetView() {return m_pView;};

public:
	static CSquishWrapper *pEntryPoint;

protected:

private:
	CTotalDoc*		m_pDoc;
	CTotalView*		m_pView;
};