#pragma once

#include <utility>
#include "SMLibHelpers.h"

#pragma warning( disable : 4996 4805 )
#include <IwTArray.h>
#include <IwVector2d.h>
#include <IwVector3d.h>
#pragma warning( default : 4996 4805 )

template<typename PointType> //2d vs 3d, mainly
class MeshVertex
{
public:
	MeshVertex()
	{}

	MeshVertex(const PointType& p)
	{
		SetPoint(p);
	}

	MeshVertex(const PointType& p, const IwTArray<MeshVertex<PointType>*>& neighbors)
	{
		SetPoint(p);
		SetNeighbors(neighbors);
	}

	~MeshVertex()
	{}

	void SetPoint(const PointType &p)
	{
		_pt = p;
	}

	void SetNeighbors(const IwTArray<MeshVertex<PointType>*>& neighbors)
	{
		_neighbors.SetSize(neighbors.GetSize());
		for(ULONG i=0; i<neighbors.GetSize(); i++)
			_neighbors[i] = neighbors[i];
	}

	void Set(const PointType &p, const IwTArray<MeshVertex<PointType>*>& neighbors)
	{
		SetPoint(p);
		SetNeighbors(neighbors);
	}

	void AddNeighbor(MeshVertex<PointType> *n)
	{
		_neighbors.Push(n);
	}

	const PointType &GetPoint()
	{
		return _pt;
	}

	const IwTArray<MeshVertex<PointType>*>& GetNeighbors()
	{
		return _neighbors;
	}

	virtual bool IsInsideNeighborsArea(IwTArray<IwVector2d> &intPnts){	return true;}

protected:
	PointType				_pt;
	IwTArray<MeshVertex<PointType>*>	_neighbors;
};

class MeshVertex2D : public MeshVertex<IwPoint2d>
{
public:
	MeshVertex2D() : MeshVertex<IwPoint2d>(){}

	MeshVertex2D(const IwPoint2d& p) : MeshVertex<IwPoint2d>(p){}

	MeshVertex2D(const IwPoint2d& p, const IwTArray<MeshVertex<IwPoint2d>*>& neighbors) 
		: MeshVertex<IwPoint2d>(p, neighbors){}

	virtual bool IsInsideNeighborsArea()
	{
		IwTArray<std::pair<MeshVertex<IwPoint2d>*, MeshVertex<IwPoint2d>*>> segments;

		for(ULONG i=1; i<_neighbors.GetSize(); i++)
			segments.Push(std::pair<MeshVertex<IwPoint2d>*, MeshVertex<IwPoint2d>*>
										  (_neighbors[i-1], _neighbors[i]));

		segments.Push(std::pair<MeshVertex<IwPoint2d>*, MeshVertex<IwPoint2d>*>
										(_neighbors.GetLast(), _neighbors[0]));

		for(ULONG i=0; i<_neighbors.GetSize(); i++)
		{
			const IwPoint2d& P0 = _pt;
			const IwPoint2d& P1 = _neighbors[i]->GetPoint();

			for(ULONG j=0; j<segments.GetSize(); j++)
			{
				std::pair<MeshVertex<IwPoint2d>*, MeshVertex<IwPoint2d>*> ends = segments[j];

				const IwPoint2d& Q0 = ends.first->GetPoint();
				const IwPoint2d& Q1 = ends.second->GetPoint();

				if(&P1 == &Q0 || &P1 == &Q1) continue;

				IwPoint2d intPt = SMLibHelpers::GetSegmentIntersectionPoint(P0, P1, Q0, Q1);

				if(!intPt.IsInitialized()) continue;

				return false;
			}
		}

		return true;
	}

	bool IsPointOutOfPlace(const IwExtent2d &domain)
	{
		IwTArray<MeshVertex2D> adjustedNeighbors;

		adjustedNeighbors.SetSize(_neighbors.GetSize());

		for(ULONG i=0; i<_neighbors.GetSize(); i++)
			adjustedNeighbors[i].SetPoint((IwPoint2d)_neighbors[i]->GetPoint());

		double v = fabs(domain.GetMax().y - domain.GetMin().y) / 4;

		MeshVertex2D adjustedPt = (MeshVertex2D)_pt;

		//Check the U direction
		bool nearMin = false;
		bool nearMax = false;

		IwTArray<ULONG> nearMinIndexes;

		for(ULONG i=0; i<_neighbors.GetSize(); i++)
		{
			if( adjustedNeighbors[i].GetPoint().y > domain.GetMin().y &&
				adjustedNeighbors[i].GetPoint().y < domain.GetMin().y + v)
			{
				nearMinIndexes.Push(i);
				nearMin = true;
			}

			if( adjustedNeighbors[i].GetPoint().y < domain.GetMax().y &&
				adjustedNeighbors[i].GetPoint().y > domain.GetMax().y - v)
			{
				//nearMaxIndexes.Push(i);
				nearMax = true;
			}
		}

		if(nearMin && nearMax)
		{
			for(ULONG i=0; i<nearMinIndexes.GetSize(); i++)
			{
				IwPoint2d pt = adjustedNeighbors[nearMinIndexes[i]].GetPoint();
				pt.y = (domain.GetMax().y - domain.GetMin().y) + pt.y;
				adjustedNeighbors[nearMinIndexes[i]].SetPoint(pt);
			}

			if( adjustedPt.GetPoint().y > domain.GetMin().y &&
				adjustedPt.GetPoint().y < domain.GetMin().y + v)
			{
				IwPoint2d pt = adjustedPt.GetPoint();
				pt.y = (domain.GetMax().y - domain.GetMin().y) + pt.y;
				adjustedPt.SetPoint(pt);
			}
		}

		IwTArray<std::pair<MeshVertex2D*, MeshVertex2D*>> segments;

		for(ULONG i=1; i<adjustedNeighbors.GetSize(); i++)
			segments.Push(std::pair<MeshVertex2D*, MeshVertex2D*>
										  (&adjustedNeighbors[i-1], &adjustedNeighbors[i]));

		segments.Push(std::pair<MeshVertex2D*, MeshVertex2D*>
										(&adjustedNeighbors[adjustedNeighbors.GetSize()-1], &adjustedNeighbors[0]));

		for(ULONG i=0; i<adjustedNeighbors.GetSize(); i++)
		{
			const IwPoint2d& P0 = adjustedPt.GetPoint();
			const IwPoint2d& P1 = adjustedNeighbors[i].GetPoint();

			for(ULONG j=0; j<segments.GetSize(); j++)
			{
				std::pair<MeshVertex2D*, MeshVertex2D*> ends = segments[j];

				const IwPoint2d& Q0 = ends.first->GetPoint();
				const IwPoint2d& Q1 = ends.second->GetPoint();

				if(&P1 == &Q0 || &P1 == &Q1) continue;

				IwPoint2d intPt = SMLibHelpers::GetSegmentIntersectionPoint(P0, P1, Q0, Q1);

				if(!intPt.IsInitialized()) continue;

				return true;
			}
		}

		return false;
	}
};