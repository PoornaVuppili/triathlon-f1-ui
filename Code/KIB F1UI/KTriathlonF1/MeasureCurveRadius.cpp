#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "MeasureCurveRadius.h"
#include "MeasureEntityDistance.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"


///////////////////////////////////////////////////////////////////////////////////////////
// Methods for CMeasureCurveRadius
///////////////////////////////////////////////////////////////////////////////////////////
CMeasureCurveRadius::CMeasureCurveRadius( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMeasureCurveRadius";

	m_toolbar = new QToolBar("Measure Radius");

	QLabel* labelResultRadius = new QLabel(tr("Radius:"));
	labelResultRadius->setFont(m_fontBold);
	m_toolbar->addWidget(labelResultRadius);

	m_textMeasuredRadius = new QLineEdit(tr(" "));
	m_textMeasuredRadius->setReadOnly(true);
	m_textMeasuredRadius->setFixedWidth(62);
	m_textMeasuredRadius->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMeasuredRadius);

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	//m_tbarMeasureRadius->setMovable( false );	
	m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	m_mouseLeftDown = false;
	m_hitCount = 0;
	m_firstHitEntity.Empty();
	m_circle = NULL;

}

CMeasureCurveRadius::~CMeasureCurveRadius()
{
}

bool CMeasureCurveRadius::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseLeftDown = true;

	// determine 1 pixel size
	int UV0[2];
	UV0[0] = 0;
	UV0[1] = 0;
	int UV55[2];
	UV55[0] = 5;
	UV55[1] = 5;
	IwPoint3d cpt0, cpt55, cpt;
	m_pView->UVtoXYZ(UV0, cpt0);
	m_pView->UVtoXYZ(UV55, cpt55);
	cpt = cpt55 - cpt0;
	double fivePixelSize = cpt.Length();

	m_selectableBreps.RemoveAll();
	m_selectableBrepsAreOpaque.RemoveAll();
	m_selectableCurves.RemoveAll();
	m_selectablePoints.RemoveAll();

	// whenever left button down, always update the selectable entities.
	if (CTotalDoc::GetTotalDoc())
		CTotalDoc::GetTotalDoc()->GetSelectableEntities(m_selectableBreps, m_selectableBrepsAreOpaque, m_selectableCurves, m_selectablePoints);

	// select the entities
	m_firstHitEntity.Empty();
	m_circlePnts.RemoveAll();
	m_curvatureHairs.RemoveAll();
	m_minCurvatureIndex = -1;
	if (m_circle) IwObjDelete(m_circle);

	SelectEntities(cursor, m_firstHitEntity);

	if ( !m_firstHitEntity.IsEmpty() )
	{
		// determine the radius
		QString radiusText = QString("No solution");
		if ( !m_firstHitEntity.IsEmpty() )
		{
			IwBSplineCurve *crv = (IwBSplineCurve*)m_firstHitEntity.GetCurve();
			// Determine the curve length
			IwExtent1d dom = crv->GetNaturalInterval();
			double curveLength;
			crv->Length(dom, 0.01, curveLength);
			// determine the curvature at the picked point
			IwVector3d gVectors[3];
			crv->EvaluateGeometric(m_firstHitEntity.GetHitPointUV().x, 2, FALSE, gVectors);
			IwPoint3d pos = gVectors[0];

			IwVector3d tangVec = gVectors[1];
			IwVector3d curvVec = gVectors[2];
			double length, radius=0.0;
			length = curvVec.Length();
			IwPoint3d center;
			if ( !IS_EQ_TOL6(length, 0) )
			{
				radius = 1.0/length;
				tangVec.Unitize();
				IwVector3d xAxis = tangVec;
				curvVec.Unitize();
				IwVector3d yAxis = curvVec;
				center = pos + radius*curvVec;

				IwContext& iwContext = m_pDoc->GetIwContext();
				m_circle = new (iwContext) IwCircle(center, xAxis, yAxis, 360, radius, 3, &iwContext, NULL, FALSE);
				IwExtent1d dom = m_circle->GetNaturalInterval();
				int seg = 90;
				if (radius > 50) seg = 180;
				else if (radius > 120) seg = 360;
				else if (radius > 500) seg = 720;
				else if (radius > 1500) seg = 2*720;
				else if (radius > 2500) seg = 4*720;
				m_circle->EquallySpacedPoints(dom.GetMin(), dom.GetMax(), seg, 0.01, &m_circlePnts, NULL);
				radiusText.setNum(radius, 'f', 3);
			}
			// determine the curvature hairs
			double delta = fivePixelSize*dom.GetLength()/curveLength;
			double param=m_firstHitEntity.GetHitPointUV().x-6*delta;
			for (int i=0; i<13; i++)
			{
				if (param<dom.GetMin() || param>dom.GetMax()) 
				{
					if (crv->IsClosed(dom))
					{
						if (param<dom.GetMin())
							param = dom.GetMax() - (dom.GetMin()-param);
						else if (param>dom.GetMax())
							param = dom.GetMin() + (param-dom.GetMax());
					}
					else
					{
						param += delta;
						continue;
					}
				}
				crv->EvaluateGeometric(param, 2, FALSE, gVectors);
				pos = gVectors[0];
				curvVec = gVectors[2];
				length = curvVec.Length();
				if (length < 0.0001)
				{
					param += delta;
					continue;
				}
				radius = 1.0/length;
				curvVec.Unitize();
				center = pos + radius*curvVec;
				m_curvatureHairs.Add(pos);
				m_curvatureHairs.Add(center);

				param += delta;
			}
			// determine the minimum radius
			double minRadius = 10000000;
			int minIndex = -1;
			IwVector3d tempVec;
			for (unsigned i=0; i<m_curvatureHairs.GetSize(); i+=2)
			{
				tempVec = m_curvatureHairs.GetAt(i) - m_curvatureHairs.GetAt(i+1);
				length = tempVec.Length();
				if (length < minRadius)
				{
					minRadius = length;
					minIndex = i;
				}
			}
			if (minIndex>-1)
				m_minCurvatureIndex = minIndex;
		}
		m_textMeasuredRadius->clear();
		m_textMeasuredRadius->setText(radiusText);
	}

	m_pView->Redraw();

	return true;
}

bool CMeasureCurveRadius::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{	
	m_pView->Redraw();

	return true;
}
bool CMeasureCurveRadius::MouseUp( const QPoint& cursor, Viewport* vp )
{

	m_pView->Redraw();


	return true;
}


bool CMeasureCurveRadius::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_pView->Redraw();

	return true;
}


bool CMeasureCurveRadius::MouseRight( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_pView->Redraw();
	return true;
}


bool CMeasureCurveRadius::keyPressEvent( QKeyEvent* event, Viewport* viewport )
{
	if ( event->key() == Qt::Key_Escape )
		Reject();

	return false; // false: do not execute subsequent actions.
}

void CMeasureCurveRadius::Display(Viewport* vp)
{		
	glDrawBuffer( GL_BACK );
	glDisable( GL_LIGHTING );

	glPointSize(6);
	glLineWidth(1.0);

	// Display hit point
	
	if ( !m_firstHitEntity.IsEmpty() )
	{
		glColor3ub( 255, 255, 0 );
		m_firstHitEntity.Display();
		glColor3ub( 255, 0, 0 );
		m_firstHitEntity.DisplayHitPoint();
	}

	//// Display  circle 
	glColor3ub( 255, 255, 0 );
	if ( m_circlePnts.GetSize() > 1 )
	{
		glLineWidth( 1.0 );
		glBegin( GL_LINES );
			for (unsigned i=1; i < m_circlePnts.GetSize(); i++)
			{
				glVertex3d( m_circlePnts.GetAt(i-1).x, m_circlePnts.GetAt(i-1).y, m_circlePnts.GetAt(i-1).z );
				glVertex3d( m_circlePnts.GetAt(i).x, m_circlePnts.GetAt(i).y, m_circlePnts.GetAt(i).z );
			}
		glEnd();
	}

		//// Display min curvature point 
	if ( m_curvatureHairs.GetSize() > 1 )
	{
		glColor3ub( 255, 192, 0 );
		//glLineWidth( 0.5 );
		//for (unsigned i=1; i < m_curvatureHairs.GetSize(); i+=4)
		//{
		//	glBegin( GL_LINES );
		//		glVertex3d( m_curvatureHairs.GetAt(i-1).x, m_curvatureHairs.GetAt(i-1).y, m_curvatureHairs.GetAt(i-1).z );
		//		glVertex3d( m_curvatureHairs.GetAt(i).x, m_curvatureHairs.GetAt(i).y, m_curvatureHairs.GetAt(i).z );
		//	glEnd();
		//}
		if (m_minCurvatureIndex > -1)
		{
			glPointSize(4);
			glColor3ub( 255, 82, 0 );
			glBegin( GL_POINTS );
				glVertex3d( m_curvatureHairs.GetAt(m_minCurvatureIndex).x, m_curvatureHairs.GetAt(m_minCurvatureIndex).y, m_curvatureHairs.GetAt(m_minCurvatureIndex).z );
			glEnd();
		}
	}

	glPointSize(1);
	glEnable( GL_LIGHTING );
}

void CMeasureCurveRadius::OnAccept()
{
	m_bDestroyMe = true;
	m_pView->Redraw();
}

/////////////////////////////////////////////////////////////////////
// This function only selects points and vertexes.
/////////////////////////////////////////////////////////////////////
void CMeasureCurveRadius::SelectEntities
(
	const QPoint&		cursor,			// I: cursor position 
	CSelectedEntity&	hitEntity		// O:
)
{

	// Get viewing vector from eyes to screen
	IwVector3d viewVec = m_pView->GetViewingVector();
	// determine the selection tolerance - 5 pixels
	int UV0[2];
	UV0[0] = cursor.x();
	UV0[1] = cursor.y();
	int UV5[2];
	UV5[0] = cursor.x()+5;
	UV5[1] = cursor.y();
	IwPoint3d cpt0, cpt5, cpt;
	m_pView->UVtoXYZ(UV0, cpt0);
	m_pView->UVtoXYZ(UV5, cpt5);
	cpt = cpt5 - cpt0;
	double sTol = cpt.Length();
	IwPoint3d viewPoint = (IwPoint3d)cpt0;

	if (1)
	{
		// find the hit curves and edges
		IwCurve* curveHitCurve = NULL;
		IwPoint3d curveHitPoint;
		double curveHitParam;
		double curveHeight;
		bool bCrvHit = CMeasureEntityDistance::HitCurves(m_selectableCurves, viewPoint, viewVec, sTol, curveHitCurve, curveHitPoint, curveHitParam, curveHeight);
		IwEdge* edgeHitEdge = NULL;
		IwPoint3d edgeHitPoint;
		double edgeHitParam;
		double edgeHeight;
		bool bEdgeHit = CMeasureEntityDistance::HitEdges(m_selectableBreps, viewPoint, viewVec, sTol, edgeHitEdge, edgeHitPoint, edgeHitParam, edgeHeight);
		if (bCrvHit && bEdgeHit)
		{
			if (curveHeight<edgeHeight)
				hitEntity.AddCurve(curveHitCurve, curveHitPoint, curveHitParam);
			else
				hitEntity.AddEdge(edgeHitEdge, edgeHitPoint, edgeHitParam);
			
			// if found one, return.
			return;
		}
		else if (bCrvHit)
		{
			hitEntity.AddCurve(curveHitCurve, curveHitPoint, curveHitParam);
			return;
		}
		else if (bEdgeHit)
		{
			hitEntity.AddEdge(edgeHitEdge, edgeHitPoint, edgeHitParam);
			return;
		}
	}

}
