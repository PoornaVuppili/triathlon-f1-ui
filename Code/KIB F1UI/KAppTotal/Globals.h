#pragma once

#include <QtGui>
#pragma warning( disable : 4996 4805 )
#pragma warning( disable : 4996 4805 )
#include <IwTArray.h>
#pragma warning( default : 4996 4805 )
#include "valgebra.h"
#pragma warning( default : 4996 4805 )

class IwCurve;
class IwEdge;
class IwFace;
class IwBrep;
class COpenGLView;
class CEntity;

typedef IwTArray< int >				CIntArray;
typedef IwTArray< double >			CDoubleArray;
typedef IwTArray< CPoint3d >		CPointArray;
typedef IwTArray< CVector3d >		CVectorArray;
typedef IwTArray< IwCurve* >		CCurveArray;
typedef IwTArray< IwBSplineCurve* >	CSplineArray;
typedef IwTArray< IwSurface* >		CSurfArray;
typedef IwTArray< IwVertex* >		CVertArray;
typedef IwTArray< IwEdge* >			CEdgeArray;
typedef IwTArray< IwFace* >			CFaceArray;
typedef IwTArray< IwBrep* >			CBrepArray;
typedef IwTArray< CEntity* >		CEntArray;

extern CColor		white;
extern CColor		red;
extern CColor		darkRed;
extern CColor		dDarkRed;
extern CColor		green;
extern CColor		darkGreen;
extern CColor		blue;
extern CColor		cyan;
extern CColor		darkCyan;
extern CColor		magenta;
extern CColor		darkMagenta;
extern CColor		lightYellow;
extern CColor		yellow;
extern CColor		darkYellow;
extern CColor		orange;
extern CColor		black;
extern CColor		darkGray;
extern CColor		gray;
extern CColor		brown;
extern CColor		darkBrown;
extern CColor		cPink;
extern CColor		mPurple;

extern CVector3d	vec_x;
extern CVector3d	vec_y;
extern CVector3d	vec_z;
extern CVector3d	zero;


void		Msg( const QString& sMsg );
void		ErrMes( const QString& sMessage );
bool		QuestionMes( const QString& sMsg );

void		InitStringPref( const QString& sName, const QString& sValue );
void		SetStringPref( const QString& sName, const QString& sValue );
QString		GetStringPref( const QString& sName );

void		InitDoublePref( const QString& sName, double fValue );
void		SetDoublePref( const QString& sName, double fValue );
double		GetDoublePref( const QString& sName );

void		InitIntPref( const QString& sName, int iValue );
void		SetIntPref( const QString& sName, int iValue );
int			GetIntPref( const QString& sName );

void		InitBoolPref( const QString& sName, bool bValue );
void		SetBoolPref( const QString& sName, bool bValue );
bool		GetBoolPref( const QString& sName );

QString		GetFileDir( const QString& sPathName );
QString		GetFileNameWithExtension( const QString& sPathName ); // global function name should be unique (or longer) to avoid confliction
QString		GetFileExt( const QString& sPathName );

void		QStringToCharArray( const QString& sText, char* out );

void		InvertPointArray( CPointArray& pts );

double		PutInRange( double val, double min, double max );

void		DistributePointsOnSegment(
				double					d,
				double					d0,
				double					d1,
				int						n,
				CDoubleArray&			arr );


bool		Solve2by2LinearSystem(
				double					a00,
				double					a01,
				double					a10,
				double					a11,
				double					b0,
				double					b1, 
				double&					x,
				double&					y );

double		Det( 
				double					m00, 
				double					m01, 
				double					m02,
				double					m10, 
				double					m11, 
				double					m12,
				double					m20, 
				double					m21, 
				double					m22 );

bool		Solve3by3LinearSystem(
				double					m[3][3],
				double					f[3],
				double					xyz[3] );

bool		IsValidationMode();

void		DeleteDocDir( const QString& sPath );

bool		IsProductionFolder( const QString sDir );