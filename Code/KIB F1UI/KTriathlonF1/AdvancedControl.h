#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Entity.h"
#include "..\KAppTotal\Basics.h"

class CTotalDoc;

class TEST_EXPORT_TW CAdvancedControl : public CEntity
{
public:
	CAdvancedControl( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_ADVANCED_CONTROL, const QString& sFileName="", int nEntId=-1 );
	~CAdvancedControl();
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	bool			Save( const QString& sDir, bool incrementalSave=true );
	bool			Load( const QString& sDir );
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&) {};
	virtual CBounds3d		GetBounds() {return CBounds3d();};
	virtual CBounds3d		ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL ) {return CBounds3d();};

	// Auto JOC steps
	bool			GetAutoJOCSteps() {return m_autoJOCSteps;};
	void			SetAutoJOCSteps(bool flag) {m_autoJOCSteps=flag;};
	bool			GetAutoMajorSteps() {return m_autoMajorSteps;};
	void			SetAutoMajorSteps(bool flag) {m_autoMajorSteps=flag;};
	bool			GetAutoAllSteps() {return m_autoAllSteps;};
	void			SetAutoAllSteps(bool flag) {m_autoAllSteps=flag;};
	bool			GetAutoNone() {return !m_autoAllSteps && !m_autoMajorSteps && !m_autoJOCSteps;};
	int             GetAutoAdditionalIterations() {return m_autoAdditionalIterations;};
	void            SetAutoAdditionalIterations(int val) {m_autoAdditionalIterations=val;};
	// Femoral Axes
	bool			GetFemoralAxesEnableRefine() {return m_femoralAxesEnableRefine;};
	void			SetFemoralAxesEnableRefine(bool val) {m_femoralAxesEnableRefine=val;};
	double			GetFemoralImplantCoronalRadius(){return m_femoralImplantCoronalRadius;};
	void			SetFemoralImplantCoronalRadius(double val) {m_femoralImplantCoronalRadius=val;SetModifiedFlag(true);};
	double			GetTrochlearGrooveRadius() {return m_trochlearGrooveRadius;};
	void			SetTrochlearGrooveRadius(double val) {m_trochlearGrooveRadius=val;m_bModified=true;};
	bool			GetFemoralAxesDisplayMechAxes() {return m_femoralAxesDisplayMechAxes;};
	void			SetFemoralAxesDisplayMechAxes(bool val) {m_femoralAxesDisplayMechAxes=val;};

	// Femoral Cuts
	bool			GetFemoralCutsEnableReset() {return m_femoralCutsEnableReset;};
	void			SetFemoralCutsEnableReset(bool val) {m_femoralCutsEnableReset=val;};
	bool			GetFemoralCutsOnlyMoveNormal() {return m_femoralCutsOnlyMoveNormal;};
	void			SetFemoralCutsOnlyMoveNormal(bool val) {m_femoralCutsOnlyMoveNormal=val;};
	double			GetViewProfileOffsetDistance() {return m_viewProfileOffsetDistance;};
	void			SetViewProfileOffsetDistance(double val) {m_viewProfileOffsetDistance=val;SetModifiedFlag(true);};
	double			GetCutLineDisplayLength() {return m_cutLineDisplayLength;};
	void			SetCutLineDisplayLength(double val) {m_cutLineDisplayLength=val;SetModifiedFlag(true);};
	bool			GetAchieveDesiredPostCuts() { return m_achieveDesiredPostCuts;};
	void			SetAchieveDesiredPostCuts(bool val) { m_achieveDesiredPostCuts = val;};// do not set SetModifiedFlag

	double			GetCutsExtraAllowance(){return m_cutsExtraAllowance;};
	void			SetCutsExtraAllowance(double val) {m_cutsExtraAllowance=val;};

	// Outline Profile
	bool			GetOutlineProfileEnableReset() {return m_outlineProfileEnableReset;};
	void			SetOutlineProfileEnableReset(bool val) {m_outlineProfileEnableReset=val;};
	double			GetOutlineProfilePosteriorCutFilterRadiusRatio() {return m_outlineProfilePosteriorCutFilterRadiusRatio;};
	void			SetOutlineProfilePosteriorCutFilterRadiusRatio(double val) {m_outlineProfilePosteriorCutFilterRadiusRatio=val;SetModifiedFlag(true);};
	double			GetOutlineProfileSketchSurfaceAntExtension() {return m_outlineProfileSketchSurfaceAntExtension;};
	void			SetOutlineProfileSketchSurfaceAntExtension(double val) {m_outlineProfileSketchSurfaceAntExtension=val;SetModifiedFlag(true);};
	double			GetOutlineProfileSketchSurfacePostExtension() {return m_outlineProfileSketchSurfacePostExtension;};
	void			SetOutlineProfileSketchSurfacePostExtension(double val) {m_outlineProfileSketchSurfacePostExtension=val;SetModifiedFlag(true);};
	double			GetOutlineProfileAntAirBallDistance() {return m_outlineProfileAntAirBallDistance;};
	void			SetOutlineProfileAntAirBallDistance(double val) {m_outlineProfileAntAirBallDistance=val;/*SetModifiedFlag(true);*/};

	//// Output Results
	bool			GetOutputNormalized() { return m_outputNormalized; };
	void			SetOutputNormalized(bool flag) { m_outputNormalized = flag; };
	bool			GetImportFromNormalized() { return m_importFromNormalized; };
	void			SetImportFromNormalized(bool flag) { m_importFromNormalized = flag; };
	bool			GetOutputWithTransformation() { return m_outputWithTransformation; };
	void			SetOutputWithTransformation(bool flag) { m_outputWithTransformation = flag; };
	IwVector3d		GetImportRotationXYZ() { return m_importRotationXYZ; };
	void			SetImportRotationXYZ(IwVector3d val) { m_importRotationXYZ = val; };
	IwVector3d		GetImportTranslationXYZ() { return m_importTranslationXYZ; };
	void			SetImportTranslationXYZ(IwVector3d val) { m_importTranslationXYZ = val; };
	IwVector3d		GetImportTransformationX() { return m_importTransformationX; };
	void			SetImportTransformationX(IwVector3d val) { m_importTransformationX = val; };
	IwVector3d		GetImportTransformationY() { return m_importTransformationY; };
	void			SetImportTransformationY(IwVector3d val) { m_importTransformationY = val; };
	IwVector3d		GetImportTransformationT() { return m_importTransformationT; };
	void			SetImportTransformationT(IwVector3d val) { m_importTransformationT = val; };

protected:

private:
	void			SetModifiedFlag( bool bModified );

private:


	bool			m_bModified;
	bool			m_bDataModified;
	// auto JOC steps
	bool			m_autoJOCSteps; // JOC = Jcurve + Outline profile + femoral Cuts
	bool			m_autoMajorSteps; // from femoral axes to solid implant
	bool			m_autoAllSteps; // from femoral axes to cast implant, for demo purpose
	int				m_autoAdditionalIterations;

	// Femoral Axes
	bool			m_femoralAxesEnableRefine;
	double			m_femoralImplantCoronalRadius;
	double			m_trochlearGrooveRadius;
	bool			m_femoralAxesDisplayMechAxes;

	// Femoral Cuts
	bool			m_femoralCutsEnableReset;
	bool			m_femoralCutsOnlyMoveNormal;
	double			m_viewProfileOffsetDistance;
	double			m_cutLineDisplayLength;
	bool			m_achieveDesiredPostCuts;
	double			m_cutsExtraAllowance; // the minimal cut thickness = critical thickness + this allowance

	// Outline Profile
	bool			m_outlineProfileEnableReset;
	double			m_outlineProfilePosteriorCutFilterRadiusRatio;
	double			m_outlineProfileSketchSurfaceAntExtension;
	double			m_outlineProfileSketchSurfacePostExtension;
	double			m_outlineProfileAntAirBallDistance;

	//// Output Results
	bool			m_outputNormalized;			// implant mechanical axis will be aligned with world coordinate system and origin at (0,0,0), and all scaled to epi-condylar 76.0mm.
	bool			m_importFromNormalized;		// Import geometry from world coordinate system to implant mechanical axis
	bool			m_outputWithTransformation;	// output the Motion Studio transformed geometry to the transformed location
	IwVector3d		m_importRotationXYZ;		// if !=(0,0,0), imported geometry will be rotated along x,y,z with the angles (degrees),
	//											// in order to roughly align the geometry x-y-z the same direction as femoral implant.
	IwVector3d		m_importTranslationXYZ;		// if !=(0,0,0), imported geometry will be translated to this location and exactly align with femoral implant x-y-z.			
	IwVector3d		m_importTransformationX;
	IwVector3d		m_importTransformationY;
	IwVector3d		m_importTransformationT;
};