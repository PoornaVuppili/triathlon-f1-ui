#pragma once

#pragma warning( disable : 4996 4805 )
#include <IwVector3d.h>
#include <iwcore_types.h>
#include <IwTArray.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
class CTotalDoc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class encapsulates all the functionality needed to define/update/manipulate the outline profile.
//
// The goal is to keep this class UI free such that an instance of this class can be passed
// into various active UI objects (see DefineOutlineProfile.h) and automation does not have to
// create UI objects.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OutlineProfileMgrModel
{
public:
	OutlineProfileMgrModel(CTotalDoc *doc);
	~OutlineProfileMgrModel();

	void Refine(CManagerActivateType manActType);
	void Refine_Refine(CManagerActivateType manActType);
	void Refine_Converge(CManagerActivateType manActType);

	void InitializeFeaturePoints();
	void DetermineInitialOutlineProfile();
	void UpdateValidationPoints();
	void SetOutlineProfileFeaturePoints();
	void GetOutlineProfileFeaturePoints();

	void CancelChanges();

	void ModifyAnteriorShape(IwTArray<IwPoint3d>& UVFeatPnts, IwTArray<int>& UVFeatPntsInfo, IwTArray<IwPoint3d>& UVFeatPntsRemap);
	void ModifyNotchShape(IwTArray<IwPoint3d>& UVFeatPnts, IwTArray<int>& UVFeatPntsInfo, IwTArray<IwPoint3d>& UVFeatPntsRemap);

	void DetermineNotchEighteenWidthPoints();

	bool RelaxOutlineProfileAtPoint(IwTArray<IwPoint3d>& UVFeatPnts, IwTArray<int>& UVFeatPntsInfo, IwTArray<IwPoint3d>& UVFeatPntsRemap, 
									double pointIndex, double maxDistToMove, bool bothSideToMove, IwPoint3d& relaxPoint, int& positionIndex);

	void RefineAnteriorOuterPoints(IwTArray<IwPoint3d>& cutFeatPnts, IwTArray<IwPoint3d>& antOuterPnts);
	bool RefineByValidation_Overall();
	bool RefineByValidation_LateralAnteriorEar();
	bool RefineByValidation_MedialAnteriorEar();
	bool RefineByVertexThickness();
	void ResampleFeaturePoints();
	bool ConvertPoint(int caughtIndex);
	bool IsCriticalThicknessControlPoint(int caughtIndex);
	bool IsMemberDataChanged();

	void MovePoint(int caughtIndex, IwPoint2d surfParam);
	int AddPoint(IwPoint2d surfParam);
	bool DeletePoint(int caughtIndex);
	static int	InsertAFeaturePointByOrder(IwTArray<IwPoint3d>& featPoints, IwTArray<int>& featPointsInfo, IwTArray<IwPoint3d>& featPointsRemap, IwPoint3d& insertPoint);

	void SetModelData(const IwTArray<IwPoint3d>& featurePoints, const IwTArray<int>& pointsInfo, const IwTArray<IwPoint3d>& pointsRemap);
	
	void ClearValidationData()									{	m_validateUnQualifiedLocations.RemoveAll();	};

	const IwTArray<IwPoint3d>& FeaturePoints()					{	return m_currFeaturePoints;					};
	const IwTArray<int>&	   FeaturePointsInfo()				{	return m_currFeaturePointsInfo;				};
	const IwTArray<IwPoint3d>& FeaturePointsRemap()				{	return m_currFeaturePointsRemap;			};

	IwTArray<IwPoint3d>& NotchEighteenWidthPoints()				{	return m_notchEighteenWidthPoints;			};
	IwTArray<IwPoint3d>& ValidateUnQualifiedLocations()			{	return m_validateUnQualifiedLocations;		};
	double	GetControlPointReferenceEdgeInfo(int index);

protected:

private: //methods
	bool	ConvertCutFeatPntsToOutlineFeatPnts(IwTArray<IwPoint3d>& cutFeatPnts, IwTArray<IwPoint3d>& antOuterPnts, IwTArray<IwPoint3d>* edgeMiddlePoints,
												IwTArray<IwPoint3d>& UVFeatPnts, IwTArray<int>& UVFeatPntsInfo, IwTArray<IwPoint3d>& UVFeatPntsRemap);
	bool	ProjectXYZtoSketchSurfaceUV(IwTArray<IwPoint3d>& inputXYZPnts, IwTArray<IwPoint3d>& outputUVPnts);
	bool	DefinePostProfile(IwFace*& cutFace, IwPoint3d posEdgePnt, IwPoint3d negEdgePnt, double moveFurtherPosteriorly, double posteriorTiltAngle,
							  IwTArray<IwPoint3d>& profileUVPnts, IwTArray<int>& profilePntsInfo, IwTArray<IwPoint3d>& profilePntsRemap);
	bool	ProjectXYZtoSketchSurfaceUV(IwPoint3d& inputXYZPnt, IwPoint3d& outputUVPnt);
	bool	DefinePostArc(IwPoint3d& prevEdgeUVPnt, IwPoint3d& arcTipUVPnt, IwPoint3d& postEdgeUVPnt, 
						  IwTArray<IwPoint3d>& arcUVPnts, IwTArray<int>& arcPntsInfo, IwTArray<IwPoint3d>& arcPntsRemap);
	bool	DefineNotchArc(IwTArray<IwPoint3d>& prevEdgeUVPnts, IwTArray<IwPoint3d>& postEdgeUVPnts, 
						   IwTArray<IwPoint3d>& arcUVPnts, IwTArray<int>& arcPntsInfo, IwTArray<IwPoint3d>& arcPntsRemap);
	bool	ConvertToArc(int index, IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, IwTArray<IwPoint3d>& featurePointsRemap, double minRadius=6.0); // Need to synchronize with Validat()
	bool	ConvertToRemapablePoint(int index, bool bConvertToRemapable, IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, IwTArray<IwPoint3d>& featurePointsRemap);
	bool	ConvertToPoint(int index, IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, IwTArray<IwPoint3d>& featurePointsRemap);
	void	RefineNotchProfile();
	void	AdjustLateralEarArc(IwTArray<IwPoint3d>& featurePoints, int arcCenterIndex, IwVector3d moveVec);

private: //data
	CTotalDoc*					m_pDoc;

	IwTArray<IwPoint3d>			m_notchEighteenWidthPoints;	// 0: preferred notch point, 1/2: 18mm width points, 3/4: notch limitaion (7~12mm)

	IwTArray<IwPoint3d>			m_oldFeaturePoints;			// see outline profile header file for definition
	IwTArray<int>				m_oldFeaturePointsInfo;		// see outline profile header file for definition
	IwTArray<IwPoint3d>			m_oldFeaturePointsRemap;	// see outline profile header file for definition
	IwTArray<IwPoint3d>			m_currFeaturePoints;		// see outline profile header file for definition
	IwTArray<int>				m_currFeaturePointsInfo;	// see outline profile header file for definition
	IwTArray<IwPoint3d>			m_currFeaturePointsRemap;	// see outline profile header file for definition
	// Cut feature points to speed up computation
	IwTArray<IwPoint3d>			m_cutFeatPnts;
	IwTArray<IwPoint3d>			m_antOuterPnts;
	IwTArray<IwPoint3d>			m_edgeMiddlePoints;
	IwTArray<IwPoint3d>			m_idealUVFeatPnts;
	IwTArray<int>				m_idealUVFeatPntsInfo;
	IwTArray<IwPoint3d>			m_idealUVFeatPntsRemap;

	//Validation data
	IwTArray<IwPoint3d>			m_validateUnQualifiedLocations;
};
