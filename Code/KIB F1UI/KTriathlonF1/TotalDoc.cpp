#include "TotalDoc.h"
#include "..\KAppTotal\Part.h"
#include "FemoralPart.h"
#include "TotalView.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\DocWriter.h"
#include "..\KAppTotal\DocLogWriter.h"
#include "TotalDocReader.h"
#include "TotalEntPanel.h"
#include "TotalEntPanelJigs.h"
#include "..\KAppTotal\IgesReader.h"
#include "..\KAppTotal\Utilities.h"
#include "..\KAppTotal\ImplantHeaderDlg.h"
#include "FemoralAxes.h"
#include "FemoralPart.h"
#include "FemoralCuts.h"
#include "OutlineProfile.h"
#include "AdvancedControl.h"
#include "AdvancedControlJigs.h"
#include "CartilageSurface.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "SideSurfaceJigs.h"
#include "OutlineProfileJigs.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "SideSurfaceJigs.h"
#include "SolidPositionJigs.h"
#include "StylusJigs.h"
#include "AnteriorSurfaceJigs.h"
#include "..\KAppTotal\PartTessThread.h"
#include "TotalMainWindow.h"
#include "..\KApp\Implant.h"
#include "..\KApp\MainWindow.h"
#include "..\KUtility\KUtility.h"
#include "..\KApp\EntityWidget.h"
#include "IFemurImplant.h"
#include "ValidateOutlineProfile.h"


CTotalDoc* CTotalDoc::m_pTotalDoc = NULL;

CTotalDoc::CTotalDoc(QObject *parent)
	:CDataDoc(parent)
{
	m_variableTableFileName = QString("variableTable.txt");
	m_validationTableFileName = QString("validateTable.txt");
}


CTotalDoc::~CTotalDoc()
{
}

void CTotalDoc::Clear()
{
	CDataDoc::Clear();
	m_pEntPanelJigs->Clear();
}

void CTotalDoc::AddEntity( CEntity* pEntity, bool bAddToEntPanel )
{

	CEntRole		eRole = pEntity->GetEntRole();
	int				id = pEntity->GetEntId();
	QString			sName = pEntity->GetEntName();
	bool			bOriginalAddToEntPanel = bAddToEntPanel;
	bool			bBelongToJigs = false;

	if ( eRole >= ENT_ROLE_JIGS_NONE &&
		 eRole <= ENT_ROLE_JIGS_END )
	{
		bAddToEntPanel = false; // Jigs entities never been added to m_pEntPanel, but m_pEntPanelJigs
		bBelongToJigs = true;
	}

	CDataDoc::AddEntity(pEntity, bAddToEntPanel);

	if( bBelongToJigs && bOriginalAddToEntPanel )
	{
		m_pEntPanelJigs->Add( id, sName, eRole );
	}


}

/////////////////////////////////////////////////////////////////////////////////////
// This function only creates CFemoralPart object, and executes necessary checking.
/////////////////////////////////////////////////////////////////////////////////////
bool CTotalDoc::ImportFemoralPart( const QString& sFileName, CEntRole eEntRole)
{
	AppendLog( "CTotalDoc::ImportFemoralPart() ");

	CColor		color( 255, 202, 128 );
	CFemoralPart* pPart = NULL;
	bool		bOK = false;
	
	pPart = new CFemoralPart( this, eEntRole, sFileName );
	bOK = pPart->ImportPart( sFileName );

	// Need to check whether the surface uses the correct template.
	if ( bOK )
	{
		IwBrep* brep=pPart->GetIwBrep();
		if (brep)
		{
			brep->SewAndOrient();
			IwTArray<IwFace*> faces;
			brep->GetFaces(faces);
			unsigned nFaces = faces.GetSize();
			IwTArray<IwEdge*> edges;
			IwEdge* edge;
			brep->GetEdges(edges);
			int bndEdgeNo = 0;
			for (unsigned i=0; i<edges.GetSize(); i++)
			{
				edge = edges.GetAt(i);
				if (edge->IsLamina())
					bndEdgeNo++;
			}

			double rowLength = (bndEdgeNo-2.0)/2.0;
			double columnLength = (nFaces+1.0)/(rowLength+1.0);
			double sideLength = (columnLength - 1.0)/2.0;
			// The rowLength, columnLength, and sideLength must be integer numbers.
			if ( !IS_EQ_TOL6(rowLength, floor(rowLength+0.000001)) ||
				 !IS_EQ_TOL6(columnLength, floor(columnLength+0.000001)) ||
				 !IS_EQ_TOL6(sideLength, floor(sideLength+0.000001)) )
			{
				delete pPart;
				pPart = NULL;
				bOK = false;
			}
		}
	}

	if( !bOK )
		return false;

	// Solidify the femur
	bool bMakeSolid = pPart->MakeSolidifyIwBrep();

	pPart->MakeTess();

//	color = magenta;

	pPart->SetColor( color );

	AddPart( pPart );

	if ( !bMakeSolid )
		return false;
	else
		return true;

}
///////////////////////////////////////////////////////////////////////////
// This function always create a CPart and imports an iges file from 
// sFileName, regardless eEntRole exists or not. 
// (See the next ImportPart function.)
///////////////////////////////////////////////////////////////////////////
bool CTotalDoc::ImportPart( const QString& sFileName, CEntRole eEntRole )
{
	bool OK = false;
	if ( eEntRole == ENT_ROLE_FEMUR ||
		 eEntRole == ENT_ROLE_OSTEOPHYTE_SURFACE )
		OK =  ImportFemoralPart( sFileName, eEntRole );
	else 
		OK = ImportCPart( sFileName, eEntRole );

	return OK;
}
///////////////////////////////////////////////////////////////////////////
// This function create a CPart and set the brep to it, if eEntRole does not exist.
// Otherwise, this function only updates brep. 
// (See the previous ImportPart function.)
///////////////////////////////////////////////////////////////////////////
bool CTotalDoc::ImportPart(IwBrep* brep, CEntRole eEntRole)
{

	if ( eEntRole == ENT_ROLE_FEMUR ||
		 eEntRole == ENT_ROLE_OSTEOPHYTE_SURFACE )
    {
		// Analyze template
		brep->SewAndOrient();
		IwTArray<IwFace*> faces;
		brep->GetFaces(faces);
		unsigned nFaces = faces.GetSize();
		IwTArray<IwEdge*> edges;
		IwEdge* edge;
		brep->GetEdges(edges);
		int bndEdgeNo = 0;
		for (unsigned i=0; i<edges.GetSize(); i++)
		{
			edge = edges.GetAt(i);
			if (edge->IsLamina())
				bndEdgeNo++;
		}

		double rowLength = (bndEdgeNo-2.0)/2.0;
		double columnLength = (nFaces+1.0)/(rowLength+1.0);
		double sideLength = (columnLength - 1.0)/2.0;
		// The rowLength, columnLength, and sideLength must be integer numbers.
		if ( !IS_EQ_TOL6(rowLength, floor(rowLength+0.000001)) ||
				!IS_EQ_TOL6(columnLength, floor(columnLength+0.000001)) ||
				!IS_EQ_TOL6(sideLength, floor(sideLength+0.000001)) )
		{
			// This is not a Stryker femur template
			return false;
		}

        CFemoralPart* pPart = NULL;
		pPart = (CFemoralPart*)this->GetPart(eEntRole);

		if ( pPart == NULL ) // Create a new CFemoralPart
		{
			pPart = new CFemoralPart( this, eEntRole );
			pPart->SetIwBrep( brep );
			// Solidify the femur
			bool isSolid = pPart->MakeSolidifyIwBrep();
			if( !isSolid )
			{
				delete pPart;
				pPart = NULL;
				return false;
			}
			pPart->SetColor( gray );
			AddPart( pPart );
		}
		else // update CFemoralPart
		{
			IwBrep* oldBrep = pPart->GetIwBrep();
			if ( oldBrep )
				IwObjDelete(oldBrep);
			pPart->SetIwBrep(brep);
			pPart->EmptyViewProfiles();
			bool isSolid = pPart->MakeSolidifyIwBrep();
			pPart->MakeTess();
		}
    }
    else // For non femur parts
    {
	    CPart* pPart = NULL;
		pPart = (CPart*)this->GetPart(eEntRole);

		if ( pPart == NULL )// Create a new CPart
		{
			pPart = new CPart( this, eEntRole );
			pPart->SetIwBrep( brep );
			pPart->SetColor( gray );
			AddPart( pPart );
		}
		else// update brep
		{
			IwBrep* oldBrep = pPart->GetIwBrep();
			if ( oldBrep )
				IwObjDelete(oldBrep);
			pPart->SetIwBrep(brep);		
		}
    }
	
    return true;
}

///////////////////////////////////////////////////////////////////////////
// This function create a CPart and set the point to it, if eEntRole does not exist.
// Otherwise, this function only updates the point. 
// (See the previous ImportPart function.)
///////////////////////////////////////////////////////////////////////////
bool CTotalDoc::ImportPart(IwPoint3d pt, CEntRole eEntRole)
{
    CColor		color = gray;
    CPart*		pPart = NULL;
	
	pPart = this->GetPart(eEntRole);
	
	if ( pPart == NULL )
	{
		pPart = new CPart( this, eEntRole );

		IwTArray<IwPoint3d> pts;
		pts.Add(pt);

		pPart->SetIwPoints( pts );
		pPart->SetColor( color );
		AddPart( pPart );
	}
	else
	{
		IwTArray<IwPoint3d> pts;
		pts.Add(pt);
		pPart->SetIwPoints( pts );
	}

    return true;
}

void CTotalDoc::SetModified( bool bModified )
{
	m_bModified = bModified;
}

bool CTotalDoc::Save( const QString& sDocFileName, bool incrementalSave )
{
	if ( !this->IsModified() )// if never modified, skip saving. 
		return true;

    QApplication::setOverrideCursor( Qt::WaitCursor );

	CDocWriter*		pDocWriter = new CDocWriter( this, sDocFileName );

	bool			bOK = pDocWriter->SaveFile( incrementalSave );

	delete pDocWriter;

	m_fileName = sDocFileName;

	m_bModified = false;

	SavePostProcessing(sDocFileName);

	QApplication::restoreOverrideCursor();

	return bOK;
}

bool CTotalDoc::Load( const QString& sDocFileName )
{
	AppendLog( QString("CTotalDoc::Load(). File name: " + sDocFileName ) );

	int						button;

	if( m_bModified )
	{
		GetView()->GetMainWindow()->OnFileSave();
	}

	// Whenever open a file, the previous user's names are initialized.
	m_previousUserInfo.clear();

    QApplication::setOverrideCursor( Qt::WaitCursor );

	Clear();

	m_fileName = sDocFileName;

	bool previouslyIncomplete = LoadPreProcessing(sDocFileName);

	std::shared_ptr<CTotalDocReader> pTotalDocReader(new CTotalDocReader( this, sDocFileName ));

	// get the version number
	int rel, rev, ver;
	QString type;
	bool bGotVersion = pTotalDocReader->ReadDataFileVersionInfo(type, rel, rev, ver);
	if ( bGotVersion )
	{
		if ( type == QString("PS") )// TriathlonF1PS file
		{
			button = QMessageBox::information( NULL, 
											"File Info", 
											"TriathlonF1(CR) can not read TriathlonF1PS data file.", 
											QMessageBox::Abort );
			return false;
		}
	}

	// load the file info & entities
	bool bOK = pTotalDocReader->LoadHeaderFileInfo();
	bOK = bOK && pTotalDocReader->LoadEntities(ENT_ROLE_NONE, ENT_ROLE_IMPLANT_END);
	bOK = bOK && pTotalDocReader->LoadEntities(ENT_ROLE_JIGS_NONE, ENT_ROLE_JIGS_END);
	// Sepcial check for cartilage thickness
	SpecialCheckForCartilageThickness();

	QApplication::restoreOverrideCursor();

	if( previouslyIncomplete )
	{
		// if previouslyIncomplete, make all design steps out of date.
		UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false, false);

		button = QMessageBox::information( NULL, 
										"File Info", 
										"File failed to be saved correctly.\n"
										"The current opened file is a backup.\n"
										"Please check any data loss.", 
										QMessageBox::Ok );	
	}

	return bOK;
}


bool CTotalDoc::LoadWithOptions( const QString& sDocFileName, QString options)
{
	AppendLog( QString("CTotalDoc::Load(). File name: " ) + sDocFileName  + QString(". options:") + options);

	int						button;

	// Whenever open a file, the previous user's names are initialized.
	m_previousUserInfo.clear();

    QApplication::setOverrideCursor( Qt::WaitCursor );

	// if load all or load fem basis, then m_pDoc should be clear.
	if ( options == "" || options == "FemBasis" )
		Clear();

	m_fileName = sDocFileName;

	bool previouslyIncomplete = LoadPreProcessing(sDocFileName);

	std::shared_ptr<CTotalDocReader> pTotalDocReader(new CTotalDocReader( this, sDocFileName ));

	// get the version number
	int rel, rev, ver;
	QString type;
	bool bGotVersion = pTotalDocReader->ReadDataFileVersionInfo(type, rel, rev, ver);
	if ( bGotVersion )
	{
		if ( type == QString("PS") )// TriathlonF1PS file
		{
			char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
			if (underDevelopment != NULL && QString("1") == underDevelopment)
			{
				button = QMessageBox::information( NULL, 
												"File Info", 
												"TriathlonF1 (CR) should not read TriathlonF1 PS data file.\nDo you still want to continue?", 
												QMessageBox::Ok, QMessageBox::No );
				if (button == QMessageBox::No) 
					return false;
			}
			else
			{
				button = QMessageBox::information( NULL, 
												"File Info", 
												"TriathlonF1 (CR) can not read TriathlonF1 PS data file.", 
												QMessageBox::Abort );
				return false;
			}
		}
	}

	// load the file info & entities
	bool bOK = pTotalDocReader->LoadHeaderFileInfo();

	if ( options == "" || options == "FemBasis" )
	{
		// Load the femoral basis implant entities
		bOK = bOK && pTotalDocReader->LoadEntities(ENT_ROLE_NONE, ENT_ROLE_IMPLANT_END);
	}
	if ( options == "" || options == "FemJigs" )
	{
		// Load the CR femoral jigs entities
		bOK = bOK && pTotalDocReader->LoadEntities(ENT_ROLE_JIGS_NONE, ENT_ROLE_JIGS_END);
		// Sepcial check for cartilage thickness
		SpecialCheckForCartilageThickness();
	}

	QApplication::restoreOverrideCursor();

	if( previouslyIncomplete )
	{
		// if previouslyIncomplete, make all design steps out of date.
		UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false, false);

		button = QMessageBox::information( NULL, 
										"File Info", 
										"File failed to be saved correctly.\n"
										"The current opened file is a backup.\n"
										"Please check any data loss.", 
										QMessageBox::Ok );	
	}

	return bOK;
}

void CTotalDoc::TogglePartDisplayMode( int id, CDisplayMode displayMode )
{
	int		iEnt, nEnts;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		if( pEntity->GetEntId() == id )
		{
			switch( pEntity->GetDisplayMode() )
			{
				case DISP_MODE_HIDDEN:
					{
						pEntity->SetDisplayMode( displayMode );

						if( pEntity->GetEntRole() == ENT_ROLE_FEMORAL_AXES)
							pEntity->SetDisplayMode( pEntity->Is2ndItemOn() ? DISP_MODE_DISPLAY_CT : DISP_MODE_TRANSPARENCY );

						if(	pEntity->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE ||
							pEntity->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE_JIGS)
							pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
					}
					break;

				case DISP_MODE_WIREFRAME:
				case DISP_MODE_DISPLAY:
				case DISP_MODE_DISPLAY_CT:
				case DISP_MODE_TRANSPARENT_CT:
				case DISP_MODE_DISPLAY_NET:
				case DISP_MODE_TRANSPARENCY:
				case DISP_MODE_TRANSPARENCY_NET:
					pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
					break;
			}
		}
	}

	m_pView->Redraw();
}

void CTotalDoc::DeleteEntitiesBelongToFemoralImplant()
{
	int		iEnt, nEnts, idEnt;
	CEntRole	eRole;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		idEnt = pEntity->GetEntId();
		eRole = pEntity->GetEntRole();

		if( eRole > ENT_ROLE_NONE && // femoral implant design roles
			eRole < ENT_ROLE_IMPLANT_END )
		{
			m_EntList.removeAt( iEnt );
			
			delete pEntity;
			
			iEnt--;
			nEnts--;

			m_pEntPanel->DeleteById( idEnt );
		}
	}

	m_pEntPanel->Clear();
	if ( entityWidget->IsItemExist(m_pEntPanel) )
		entityWidget->RemoveItem( m_pEntPanel );

	m_bModified = true;
}

void CTotalDoc::DeleteEntitiesBelongToJigs()
{
	int		iEnt, nEnts, idEnt;
	CEntRole	eRole;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		idEnt = pEntity->GetEntId();
		eRole = pEntity->GetEntRole();

		if( eRole >= ENT_ROLE_JIGS_NONE && // Jigs design roles
			eRole <= ENT_ROLE_JIGS_END )
		{
			m_EntList.removeAt( iEnt );
			
			delete pEntity;
			
			iEnt--;
			nEnts--;

			m_pEntPanelJigs->DeleteById( idEnt );
		}
	}

	m_pEntPanel->DeleteById( -1 ); // id = -1 for "==== Jigs ====", in iTW 5

	m_pEntPanelJigs->Clear();
	if ( entityWidget->IsItemExist(m_pEntPanelJigs) )
		entityWidget->RemoveItem( m_pEntPanelJigs );

	m_bModified = true;
}

//////////////////////////////////////////////////////////////////////
// The sequence in this function determines the design step order.
void CTotalDoc::UpdateEntPanelStatusMarker( CEntRole eEntRole, bool bUpToDate, bool implicitlyUpdate )
{
	switch( eEntRole )
		{
		case ENT_ROLE_FEMUR:	
			SetEntPanelStatusMarker( ENT_ROLE_FEMUR, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_HIP, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_AXES, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_CUTS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE, false );
			break;
		
		case ENT_ROLE_HIP:	
			SetEntPanelStatusMarker( ENT_ROLE_HIP, bUpToDate );
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_AXES, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_CUTS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE, false );
			break;

		case ENT_ROLE_FEMORAL_AXES:	
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_AXES, bUpToDate );
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_CUTS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE, false );
			break;
		
		case ENT_ROLE_OUTLINE_PROFILE:	
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE, bUpToDate );
			break;
		
		case ENT_ROLE_FEMORAL_CUTS:	
			SetEntPanelStatusMarker( ENT_ROLE_FEMORAL_CUTS, bUpToDate );
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE, false );
			break;
		
		default:
			break;
		}
		
	GetView()->GetMainWindow()->EnableActions(true);

	// If the femoral implant design change, make all jigs steps out-of-date
	if ( eEntRole < ENT_ROLE_IMPLANT_END )
		UpdateEntPanelStatusMarkerJigs(ENT_ROLE_ADVANCED_CONTROL_JIGS, false, false);
}

void CTotalDoc::UpdateEntPanelStatusMarkerJigs( CEntRole eEntRole, bool bUpToDate, bool implicitlyUpdate )
{
	switch( eEntRole )
		{
		case ENT_ROLE_ADVANCED_CONTROL_JIGS: // this means (ENT_ROLE_OSTEOPHYTE_SURFACE-1), all jigs design steps should be all marked "*".
			SetEntPanelStatusMarker( ENT_ROLE_OSTEOPHYTE_SURFACE, false );
			SetEntPanelStatusMarker( ENT_ROLE_CARTILAGE_SURFACE, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_INNER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_OUTER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_OSTEOPHYTE_SURFACE:	
			SetEntPanelStatusMarker( ENT_ROLE_OSTEOPHYTE_SURFACE, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_CARTILAGE_SURFACE, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_INNER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_OUTER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_CARTILAGE_SURFACE:	
			SetEntPanelStatusMarker( ENT_ROLE_CARTILAGE_SURFACE, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_INNER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_OUTER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_OUTLINE_PROFILE_JIGS:
			SetEntPanelStatusMarker( ENT_ROLE_OUTLINE_PROFILE_JIGS, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_INNER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_OUTER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_INNER_SURFACE_JIGS:
			SetEntPanelStatusMarker( ENT_ROLE_INNER_SURFACE_JIGS, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_OUTER_SURFACE_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_OUTER_SURFACE_JIGS:	
			SetEntPanelStatusMarker( ENT_ROLE_OUTER_SURFACE_JIGS, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, false ); 
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_SIDE_SURFACE_JIGS:	
			SetEntPanelStatusMarker( ENT_ROLE_SIDE_SURFACE_JIGS, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_SOLID_POSITION_JIGS:	
			SetEntPanelStatusMarker( ENT_ROLE_SOLID_POSITION_JIGS, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, false );
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_STYLUS_JIGS:	
			SetEntPanelStatusMarker( ENT_ROLE_STYLUS_JIGS, bUpToDate );

			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, false );
			break;

		case ENT_ROLE_ANTERIOR_SURFACE_JIGS:	
			SetEntPanelStatusMarker( ENT_ROLE_ANTERIOR_SURFACE_JIGS, bUpToDate );

			break;

		default:
			break;
		}
		
	GetView()->GetMainWindow()->EnableActions(true);

}

void CTotalDoc::SetEntPanelStatusMarker( CEntRole eEntRole, bool status )
{
	CEntity*	pEntity;

	pEntity = GetEntity( eEntRole );

	if (pEntity == NULL ) return;

	// EntPanel "*" sign should synchronize with entity UpToDateStatus
	pEntity->SetUpToDateStatus(status);

	if ( eEntRole <= ENT_ROLE_IMPLANT_END )
	{
		m_pEntPanel->SetModifiedMark( pEntity->GetEntId(), status );
		if ( !status )// not up-to-date
		{
			CEntValidateStatus validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
			QString validateMessage = "";
			if ( eEntRole == ENT_ROLE_FEMORAL_AXES ||
				 eEntRole == ENT_ROLE_OUTLINE_PROFILE || eEntRole == ENT_ROLE_FEMORAL_CUTS)
				validateMessage = "Not validate.";// Major design steps
			pEntity->SetValidateStatus(validateStatus, validateMessage);
			SetEntPanelValidateStatus( eEntRole, validateStatus, validateMessage);
		}
	}
	else if (eEntRole >= ENT_ROLE_JIGS_NONE && eEntRole <= ENT_ROLE_JIGS_END )
	{
		m_pEntPanelJigs->SetModifiedMark( pEntity->GetEntId(), status );
		if ( !status )// not up-to-date
		{
			CEntValidateStatus validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
			QString validateMessage = "";
			if ( eEntRole == ENT_ROLE_OUTLINE_PROFILE_JIGS || eEntRole == ENT_ROLE_SOLID_POSITION_JIGS || 
				 eEntRole == ENT_ROLE_STYLUS_JIGS )
				validateMessage = "Not validate.";// Major design steps
			pEntity->SetValidateStatus(validateStatus, validateMessage);
			m_pEntPanelJigs->SetValidateStatus( pEntity->GetEntId(), validateStatus, validateMessage);
		}
	}

	m_bModified = true;

}

CFemoralPart* CTotalDoc::GetFemur()
{
	return (CFemoralPart*)GetEntity( ENT_ROLE_FEMUR );
}

CPart* CTotalDoc::GetHip()
{
	return GetPart( ENT_ROLE_HIP );
}

CFemoralAxes* CTotalDoc::GetFemoralAxes()
{
	return (CFemoralAxes*)GetEntity( ENT_ROLE_FEMORAL_AXES );
}

CFemoralCuts* CTotalDoc::GetFemoralCuts()
{
	return (CFemoralCuts*)GetEntity( ENT_ROLE_FEMORAL_CUTS );
}

COutlineProfile* CTotalDoc::GetOutlineProfile()
{
	return (COutlineProfile*)GetEntity( ENT_ROLE_OUTLINE_PROFILE );
}

//// Jigs ////
CFemoralPart* CTotalDoc::GetOsteophyteSurface()
{
	return (CFemoralPart*)GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
}

CCartilageSurface* CTotalDoc::GetCartilageSurface()
{
	return (CCartilageSurface*)GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
}

COutlineProfileJigs* CTotalDoc::GetOutlineProfileJigs()
{
	return (COutlineProfileJigs*)GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS );
}

CInnerSurfaceJigs* CTotalDoc::GetInnerSurfaceJigs()
{
	return (CInnerSurfaceJigs*)GetEntity( ENT_ROLE_INNER_SURFACE_JIGS );
}

COuterSurfaceJigs* CTotalDoc::GetOuterSurfaceJigs()
{
	return (COuterSurfaceJigs*)GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS );
}

CSideSurfaceJigs* CTotalDoc::GetSideSurfaceJigs()
{
	return (CSideSurfaceJigs*)GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS );
}

CSolidPositionJigs* CTotalDoc::GetSolidPositionJigs()
{
	return (CSolidPositionJigs*)GetEntity( ENT_ROLE_SOLID_POSITION_JIGS );
}

CStylusJigs* CTotalDoc::GetStylusJigs()
{
	return (CStylusJigs*)GetEntity( ENT_ROLE_STYLUS_JIGS );
}

CAnteriorSurfaceJigs* CTotalDoc::GetAnteriorSurfaceJigs()
{
	return (CAnteriorSurfaceJigs*)GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS );
}

//////////////////////////////////////////////////////////////////////////
// This function will always return a non-NULL pointer of CAdvancedControl
//////////////////////////////////////////////////////////////////////////
CAdvancedControl* CTotalDoc::GetAdvancedControl()
{
	CAdvancedControl* advCtrl = (CAdvancedControl*)GetEntity( ENT_ROLE_ADVANCED_CONTROL );

	if (advCtrl == NULL)
	{
		advCtrl = new CAdvancedControl(this);
		AddEntity(advCtrl, false); // do not display on entity panel
	}

	return advCtrl;
}

//////////////////////////////////////////////////////////////////////////
// This function will always return a non-NULL pointer of CAdvancedControl
//////////////////////////////////////////////////////////////////////////
CAdvancedControlJigs* CTotalDoc::GetAdvancedControlJigs()
{
	CAdvancedControlJigs* advCtrl = (CAdvancedControlJigs*)GetEntity( ENT_ROLE_ADVANCED_CONTROL_JIGS );

	if (advCtrl == NULL)
	{
		advCtrl = new CAdvancedControlJigs(this);
		AddEntity(advCtrl, false); // do not display on entity panel
	}

	return advCtrl;
}
//////////////////////////////////////////////////////////////////////////
// This function only returns the pointer of CAdvancedControlJigs
//////////////////////////////////////////////////////////////////////////
CAdvancedControlJigs* CTotalDoc::GetAdvancedControlJigsPointer()
{
	CAdvancedControlJigs* advCtrl = (CAdvancedControlJigs*)GetEntity( ENT_ROLE_ADVANCED_CONTROL_JIGS );

	return advCtrl;
}

bool CTotalDoc::IsAutoCreateJOCSteps()
{
	CAdvancedControl* advCtrl = NULL;

	// Why? GetAdvancedControl() will create advanced control object if it does not exist.
	// But it only makes sense when the basis (femur,...) has been imported to the system.
	if ( this->GetFemur() )
		advCtrl = this->GetAdvancedControl();

	if ( advCtrl )
		return advCtrl->GetAutoJOCSteps();
	else 
		return false;
}

bool CTotalDoc::IsAutoCreateMajorSteps()
{
	CAdvancedControl* advCtrl = NULL;

	// Why? GetAdvancedControl() will create advanced control object if it does not exist.
	// But it only makes sense when the basis (femur,...) has been imported to the system.
	if ( this->GetFemur() )
		advCtrl = this->GetAdvancedControl();

	if ( advCtrl )
		return advCtrl->GetAutoMajorSteps();
	else 
		return false;
}

bool CTotalDoc::IsAutoCreateAllSteps()
{
	CAdvancedControl* advCtrl = NULL;

	// Why? GetAdvancedControl() will create advanced control object if it does not exist.
	// But it only makes sense when the basis (femur,...) has been imported to the system.
	if ( this->GetFemur() )
		advCtrl = this->GetAdvancedControl();

	if ( advCtrl )
		return advCtrl->GetAutoAllSteps();
	else
		return false;
}

void CTotalDoc::InitializeValidationReport()
{
	AppendLog( QString("CTotalDoc::InitializeValidationReport()") );

	// clear all messages
	m_validationReport.clear();
	m_validationAllGoodResults = true;
	
	// Title
	QString title = QString("TriathlonF1 Femoral Implant Design E-DHR");
	AppendValidationReportEntry(title);

	// Version info
	QString CRPS;
	if (this->isPosteriorStabilized())
		CRPS = QString("PS");
	else
		CRPS = QString("CR");
	bool bBeta = !this->isProductionSoftware();
	QString appVersion = m_pView->GetMainWindow()->GetAppVersion();
	QString msg;
	if (bBeta)
		msg = QString("Software: TriathlonF1 %1 beta version %2").arg(CRPS).arg(appVersion);
	else
		msg = QString("Software: TriathlonF1 %1 production version %2").arg(CRPS).arg(appVersion);

	AppendValidationReportEntry(msg);

	// Date
	QString			sDate = QString("Time: ") + QDateTime::currentDateTime().toString();
	AppendValidationReportEntry(sDate);

	// User info
	QString			userInfo = QString( "User: %1").arg( GetCurrentUserName());
	AppendValidationReportEntry(userInfo);

	// Workstation
	QString			workstation = QString( "Workstation: %1").arg( GetCurrentComputerName());
	AppendValidationReportEntry(workstation);

	// Patient ID
	QString patientID = this->GetPatientId();
	AppendValidationReportEntry("Patient ID: " + patientID);

	// All pass or not
    if (auto pTE = GetView()->GetValidationReportTextEdit())
    {
        pTE->setTextColor(QColor(255, 0, 0));
	    AppendValidationReportEntry("**** NOT ALL PASS ****");
	    pTE->setTextColor(QColor(0, 0, 0));
    }
}

void CTotalDoc::AppendValidationReportEntry(QString const& entry, bool goodResult)
{
	QString str = entry;
	
	if ( entry == QString("\n") )
		str = QString("");

	if ( !goodResult )
	{
		m_validationAllGoodResults = false;
		m_validationReport.append(str+QString("\n"));
		if (auto pTE = GetView()->GetValidationReportTextEdit())
		{
			pTE->setTextColor(QColor(255, 0, 0));
			pTE->append(str);
			pTE->setTextColor(QColor(0, 0, 0));
		}
	}
	else
	{
		m_validationReport.append(str+QString("\n"));
		if ( auto pTE = GetView()->GetValidationReportTextEdit() )
			pTE->append(str);
	}
}

QString CTotalDoc::GetValidationReport(bool& allGoodResults)
{
	allGoodResults=m_validationAllGoodResults;
	return m_validationReport;
}

void CTotalDoc::InitializeValidationReportJigs()
{
	AppendLog( QString("CTotalDoc::InitializeValidationReportJigs()") );

	// clear all messages
	m_validationReportJigs.clear();
	m_validationJigsAllGoodResults = true;
	
	// Title
	QString title = QString("TriathlonF1 Femoral Jig Design E-DHR");
	AppendValidationReportJigsEntry(title);

	// Version info
	// Version info
	QString CRPS;
	if (this->isPosteriorStabilized())
		CRPS = QString("PS");
	else
		CRPS = QString("CR");
	bool bBeta = !isProductionSoftware();
	QString appVersion = m_pView->GetMainWindow()->GetAppVersion();
	QString msg;
	if (bBeta)
		msg = QString("Software: TriathlonF1 %1 beta version %2").arg(CRPS).arg(appVersion);
	else
		msg = QString("Software: TriathlonF1 %1 production version %2").arg(CRPS).arg(appVersion);

	AppendValidationReportJigsEntry(msg);

	// Date
	QString			sDate = QString("Time: ") + QDateTime::currentDateTime().toString();
	AppendValidationReportJigsEntry(sDate);

	// User info
	QString			userInfo = QString( "User: %1").arg( GetCurrentUserName());
	AppendValidationReportJigsEntry(userInfo);

	// Workstation
	QString			workstation = QString( "Workstation: %1").arg( GetCurrentComputerName());
	AppendValidationReportJigsEntry(workstation);

	// Patient ID
	QString patientID = this->GetPatientId();
	AppendValidationReportJigsEntry("Patient ID: " + patientID);

	// All pass or not
	GetView()->GetValidationReportJigsTextEdit()->setTextColor(QColor(255, 0, 0));
	AppendValidationReportJigsEntry("**** NOT ALL PASS ****");
	GetView()->GetValidationReportJigsTextEdit()->setTextColor(QColor(0, 0, 0));
}

void CTotalDoc::AppendValidationReportJigsEntry(QString const& entry, bool goodResult)
{
	QString str = entry;
	
	if ( entry == QString("\n") )
		str = QString("");

	if ( !goodResult )
	{
		m_validationJigsAllGoodResults = false;
		m_validationReportJigs.append(str+QString("\n"));
		if ( GetView()->GetValidationReportJigsTextEdit() )
		{
			GetView()->GetValidationReportJigsTextEdit()->setTextColor(QColor(255, 0, 0));
			GetView()->GetValidationReportJigsTextEdit()->append(str);
			GetView()->GetValidationReportJigsTextEdit()->setTextColor(QColor(0, 0, 0));
		}
	}
	else
	{
		m_validationReportJigs.append(str+QString("\n"));
		if ( GetView()->GetValidationReportJigsTextEdit() )
			GetView()->GetValidationReportJigsTextEdit()->append(str);
	}
}

QString CTotalDoc::GetValidationReportJigs(bool& allGoodResults)
{
	allGoodResults=m_validationJigsAllGoodResults;
	return m_validationReportJigs;
}

void CTotalDoc::GetImplantAPSizeRange(double& APHigh, double& APLow)
{
	double highValue_MainZone = GetVarTableValue("IMPLANT AP SIZE HIGH");
	double lowValue_MainZone = GetVarTableValue("IMPLANT AP SIZE LOW");

	APHigh = highValue_MainZone;
	APLow = lowValue_MainZone;

	return;
}

void CTotalDoc::GetImplantMLSizeRange(double& MLHigh, double& MLLow)
{
	double highValue_MainZone = GetVarTableValue("IMPLANT ML SIZE HIGH");
	double lowValue_MainZone = GetVarTableValue("IMPLANT ML SIZE LOW");

	MLHigh = highValue_MainZone;
	MLLow = lowValue_MainZone;

	return;
}

void CTotalDoc::GetImplantMedialDistalRadiusRange(double& RadiusHigh, double& RadiusLow)
{
	double highValue_MainZone = GetVarTableValue("MEDIAL DISTAL RADIUS HIGH");
	double lowValue_MainZone = GetVarTableValue("MEDIAL DISTAL RADIUS LOW");

	RadiusHigh = highValue_MainZone;
	RadiusLow = lowValue_MainZone;

	return;
}

void CTotalDoc::GetImplantLateralDistalRadiusRange(double& RadiusHigh, double& RadiusLow)
{
	double highValue_MainZone = GetVarTableValue("LATERAL DISTAL RADIUS HIGH");
	double lowValue_MainZone = GetVarTableValue("LATERAL DISTAL RADIUS LOW");

	RadiusHigh = highValue_MainZone;
	RadiusLow = lowValue_MainZone;

	return;
}


void CTotalDoc::GetImplantMedialPosteriorRadiusRange(double& RadiusHigh, double& RadiusLow)
{
	double highValue_MainZone = GetVarTableValue("MEDIAL POST RADIUS HIGH");
	double lowValue_MainZone = GetVarTableValue("MEDIAL POST RADIUS LOW");

	RadiusHigh = highValue_MainZone;
	RadiusLow = lowValue_MainZone;

	return;
}

void CTotalDoc::GetImplantLateralPosteriorRadiusRange(double& RadiusHigh, double& RadiusLow)
{
	double highValue_MainZone = GetVarTableValue("LATERAL POST RADIUS HIGH");
	double lowValue_MainZone = GetVarTableValue("LATERAL POST RADIUS LOW");

	RadiusHigh = highValue_MainZone;
	RadiusLow = lowValue_MainZone;

	return;
}

bool CTotalDoc::GetImplantActualOverhang(double& overMax)
{
	IwTArray<IwPoint3d> unqualifiedLocations;
	IwTArray<double> unqualifiedDistances;
	IwTArray<int> overhangings;

    double allowableDistMax = this->GetVarTableValue("OUTLINE PROFILE MAX DISTANCE TO CUT EDGE");
    double allowableDistMin = this->GetVarTableValue("OUTLINE PROFILE MIN DISTANCE TO CUT EDGE");

    bool vd2cp = Validation::OutlineProfile::ValidateDistanceToCutProfile(this, allowableDistMax, allowableDistMin,
	    unqualifiedLocations,	// O:
	    unqualifiedDistances,	// O:
	    overhangings			// O: for 0~7th faces, [8]:8th face positive side, [9]:8th face negative side (total 0~9=10), 
								//     0:good, >0:underhanging, <0:overhanging (same definition as distance)
    );

    double maxOver = -1e10;
    int maxInd = -1;
    for (int iud = 0; iud < overhangings.GetSize(); ++iud)
    {
        if (overhangings[iud] < 0) // overhanging
        {
            if (maxOver < unqualifiedDistances[iud])
            {
                maxOver = unqualifiedDistances[iud];
                maxInd = iud;
            }
        }
    }

    // if there is a negative (overhang), set 'overMax' to it and return true
    if (maxInd >= 0)
    {
        overMax = maxOver;
        return true; // yes, there is overhang!!!
    }

    return false; // no overhang detected
}

///////////////////////////////////////////////////////
bool CTotalDoc::IsFemoralImplantDesignComplete()
{
	// First check the basis
	CFemoralAxes* pFemoralAxes = GetFemoralAxes();
	if ( !(pFemoralAxes && pFemoralAxes->GetUpToDateStatus()) )
		return false;
	
	CFemoralCuts* pFemoralCuts = GetFemoralCuts();
	if ( !(pFemoralCuts && pFemoralCuts->GetUpToDateStatus()) )
		return false;

	COutlineProfile* pOutlineProfile = GetOutlineProfile();
	if ( !(pOutlineProfile && pOutlineProfile->GetUpToDateStatus()) )
		return false;

	return true;
}

bool CTotalDoc::IsFemoralImplantDesignWithValidAPMLSizes()
{
	return true;
}

///////////////////////////////////////////////////////
bool CTotalDoc::IsFemoralJigsDesignComplete()
{
	CCartilageSurface* pCartilageSurface = this->GetCartilageSurface();
	if ( !(pCartilageSurface && pCartilageSurface->GetUpToDateStatus()) )
		return false;
	COutlineProfileJigs* pOutlineProfileJigs = this->GetOutlineProfileJigs();
	if ( !(pOutlineProfileJigs && pOutlineProfileJigs->GetUpToDateStatus()) )
		return false;
	CInnerSurfaceJigs* pInnerSurfaceJigs = this->GetInnerSurfaceJigs();
	if ( !(pInnerSurfaceJigs && pInnerSurfaceJigs->GetUpToDateStatus()) )
		return false;
	COuterSurfaceJigs* pOuterSurfaceJigs = this->GetOuterSurfaceJigs();
	if ( !(pOuterSurfaceJigs && pOuterSurfaceJigs->GetUpToDateStatus()) )
		return false;
	CSideSurfaceJigs* pSideSurfaceJigs = this->GetSideSurfaceJigs();
	if ( !(pSideSurfaceJigs && pSideSurfaceJigs->GetUpToDateStatus()) )
		return false;
	// If output anterior surface regardless, do not need to check these 2 entities.
	if ( !this->GetAdvancedControlJigs()->GetOutputAnteriorSurfaceRegardless() )
	{
		CSolidPositionJigs* pSolidPositionJigs = this->GetSolidPositionJigs();
		if ( !(pSolidPositionJigs && pSolidPositionJigs->GetUpToDateStatus()) )
			return false;
		CStylusJigs* pStylusJigs = this->GetStylusJigs();
		if ( !(pStylusJigs && pStylusJigs->GetUpToDateStatus()) )
			return false;
	}
	CAnteriorSurfaceJigs* pAnteriorSurfaceJigs = this->GetAnteriorSurfaceJigs();
	if ( !(pAnteriorSurfaceJigs && pAnteriorSurfaceJigs->GetUpToDateStatus()) )
		return false;

	return true;;
}

bool CTotalDoc::GetFemurImplantPosteriorTips
(
	IwPoint3d& medialPnt,	// O:
	IwPoint3d& lateralPnt	// O:
)
{
	medialPnt = IwPoint3d();// uninitialize
	lateralPnt = IwPoint3d();// uninitialize

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
		positiveSideIsLateral = true;
	else
		positiveSideIsLateral = false;

	// Get solid implant feature points
	IwTArray<IwPoint3d> sFeaturePoints;

	if ( positiveSideIsLateral )
	{
		medialPnt = sFeaturePoints.GetAt(1);
		lateralPnt = sFeaturePoints.GetAt(0);
	}
	else
	{
		medialPnt = sFeaturePoints.GetAt(0);
		lateralPnt = sFeaturePoints.GetAt(1);
	}

	return ( medialPnt.IsInitialized() && lateralPnt.IsInitialized() );
}

void CTotalDoc::ToggleEntPanelItemByID(int id, bool On)
{
	CEntity* pEntity = GetEntityById(id);
	if ( pEntity )
	{
		if ( pEntity->GetEntRole() <= ENT_ROLE_IMPLANT_END )
			CDataDoc::ToggleEntPanelItemByID(id, On);
		else if ( pEntity->GetEntRole() >= ENT_ROLE_JIGS_NONE &&
			      pEntity->GetEntRole() <= ENT_ROLE_JIGS_END )
		{
			m_pEntPanelJigs->ToggleById(id, On);
		}
	}
}

void CTotalDoc::RemoveEntityFromEntPanel( CEntRole eEntRole )
{
	CEntity*	pEntity = GetEntity( eEntRole );
	if( pEntity )
	{
		if ( pEntity->GetEntRole() <= ENT_ROLE_IMPLANT_END )
			CDataDoc::RemoveEntityFromEntPanel(eEntRole);
		else if ( pEntity->GetEntRole() >= ENT_ROLE_JIGS_NONE &&
			      pEntity->GetEntRole() <= ENT_ROLE_JIGS_END )
		{
			m_pEntPanelJigs->DeleteById( pEntity->GetEntId() );
		}
	}
}

void CTotalDoc::SetEntPanelValidateStatus( CEntRole eEntRole, CEntValidateStatus status, QString message )
{
	CEntity*	pEntity = GetEntity( eEntRole );

	if (pEntity)
	{
		if ( eEntRole <= ENT_ROLE_IMPLANT_END )
			CDataDoc::SetEntPanelValidateStatus(eEntRole, status, message);
		else if ( eEntRole >= ENT_ROLE_JIGS_NONE &&
			      eEntRole <= ENT_ROLE_JIGS_END )
		{
			m_pEntPanelJigs->SetValidateStatus( pEntity->GetEntId(), status, message );
		}
	}

}

void CTotalDoc::SetEntPanelFontColor(CEntRole entRole, CColor color)
{
	CEntity*	pEntity = GetEntity( entRole );

	if (pEntity)
	{
		if ( entRole <= ENT_ROLE_IMPLANT_END )
			CDataDoc::SetEntPanelFontColor(entRole, color);
		else if ( entRole >= ENT_ROLE_JIGS_NONE &&
			      entRole <= ENT_ROLE_JIGS_END )
		{
			m_pEntPanelJigs->SetFontColor( pEntity->GetEntId(), color );
		}
	}

}

void CTotalDoc::SetEntPanelToolTip(CEntRole entRole, QString message)
{
	CEntity*	pEntity = GetEntity( entRole );

	if (pEntity)
	{
		if ( entRole <= ENT_ROLE_IMPLANT_END )
			CDataDoc::SetEntPanelToolTip(entRole, message);
		else if ( entRole >= ENT_ROLE_JIGS_NONE &&
			      entRole <= ENT_ROLE_JIGS_END )
		{
			m_pEntPanelJigs->SetToolTip( pEntity->GetEntId(), message );
		}
	}
}

// 
void CTotalDoc::HideAllFemurImplantEntities()
{
	int entNum = this->GetNumOfEntities();

	for (int i=0; i<entNum; i++)
	{
		CEntity*	pEntity = this->GetEntity( i );
		if ( pEntity != NULL )
		{
			CEntRole entRole = pEntity->GetEntRole();
			if ( entRole >= ENT_ROLE_NONE && entRole <= ENT_ROLE_IMPLANT_END )
			{
				pEntity->SetDisplayMode(DISP_MODE_HIDDEN);
				this->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
			}
		}
	}
		
}

// 
void CTotalDoc::HideAllFemurJigsEntities()
{
	int entNum = this->GetNumOfEntities();

	for (int i=0; i<entNum; i++)
	{
		CEntity*	pEntity = this->GetEntity( i );
		if ( pEntity != NULL )
		{
			CEntRole entRole = pEntity->GetEntRole();
			if ( entRole >= ENT_ROLE_JIGS_NONE && entRole <= ENT_ROLE_JIGS_END )
			{
				pEntity->SetDisplayMode(DISP_MODE_HIDDEN);
				this->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
			}
		}
	}
		
}

void CTotalDoc::SpecialCheckForCartilageThickness()
{
	if ( !this->IsOpeningFileRightVersion(6,0,26) ) // before versions 6.0.26
	{
		double cartilageThickness = this->GetAdvancedControlJigs()->GetCartilageThickness();
		if ( fabs(cartilageThickness - 3.1 ) > 0.000001 ) // cartilageThickness != 3.1, force it to be 3.1
		{
			this->GetAdvancedControlJigs()->SetCartilageThickness(3.1); // force it to be 3.1
        	QApplication::restoreOverrideCursor();
			QMessageBox::information( NULL, 
									"Cartilage", 
									"Cartilage thickness has been changed to 3.1mm to meet Stryker production specification.\nCartilage surface and all positioning jig design have to be manually updated.", 
									QMessageBox::Ok );
		}
	}
}

void CTotalDoc::DeleteEntitiesAfterFemoralAxes()
{
	//
	CEntity * pEnt = GetEntity(ENT_ROLE_FEMORAL_CUTS);
	if ( pEnt )
		DeleteEntityById(pEnt->GetEntId());
	
	//
	pEnt = GetEntity(ENT_ROLE_OUTLINE_PROFILE);
	if ( pEnt )
		DeleteEntityById(pEnt->GetEntId());
	
	// Delete jigs entities
	DeleteEntitiesBelongToJigs();
	// Here also need to delete Femur PS features.
	//IFemurImplant::DeleteEntitiesBelongToPSFeatures();
}
