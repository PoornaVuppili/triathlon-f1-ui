#pragma once

#include <QtGui>
#pragma warning( disable : 4996 4805 )
#include <IwVector3d.h>
#pragma warning( default : 4996 4805 )

class CPart;

class CFakeXmlReader
	{
public:
	CFakeXmlReader( const QString& sFileName );
	~CFakeXmlReader();

	QString				GetString(const QString& tag);
	bool				GetIwPoint3d(const QString& tag, IwPoint3d& point);

private:

private:

	QString				m_sFileName;

	};
