/****************************************************************************
** Meta object code from reading C++ file 'ImportOsteophyteSurface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../ImportOsteophyteSurface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ImportOsteophyteSurface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CImportOsteophyteSurface_t {
    QByteArrayData data[7];
    char stringdata0[77];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CImportOsteophyteSurface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CImportOsteophyteSurface_t qt_meta_stringdata_CImportOsteophyteSurface = {
    {
QT_MOC_LITERAL(0, 0, 24), // "CImportOsteophyteSurface"
QT_MOC_LITERAL(1, 25, 19), // "SwitchToCoordSystem"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 4), // "name"
QT_MOC_LITERAL(4, 51, 7), // "OnReset"
QT_MOC_LITERAL(5, 59, 8), // "OnAccept"
QT_MOC_LITERAL(6, 68, 8) // "OnCancel"

    },
    "CImportOsteophyteSurface\0SwitchToCoordSystem\0"
    "\0name\0OnReset\0OnAccept\0OnCancel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CImportOsteophyteSurface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   37,    2, 0x08 /* Private */,
       5,    0,   38,    2, 0x08 /* Private */,
       6,    0,   39,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CImportOsteophyteSurface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CImportOsteophyteSurface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SwitchToCoordSystem((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->OnReset(); break;
        case 2: _t->OnAccept(); break;
        case 3: _t->OnCancel(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CImportOsteophyteSurface::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CImportOsteophyteSurface::SwitchToCoordSystem)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CImportOsteophyteSurface::staticMetaObject = { {
    QMetaObject::SuperData::link<CManager::staticMetaObject>(),
    qt_meta_stringdata_CImportOsteophyteSurface.data,
    qt_meta_data_CImportOsteophyteSurface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CImportOsteophyteSurface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CImportOsteophyteSurface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CImportOsteophyteSurface.stringdata0))
        return static_cast<void*>(this);
    return CManager::qt_metacast(_clname);
}

int CImportOsteophyteSurface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CManager::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void CImportOsteophyteSurface::SwitchToCoordSystem(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
