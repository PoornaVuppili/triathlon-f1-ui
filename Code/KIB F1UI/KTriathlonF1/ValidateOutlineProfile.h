#pragma once

#include <qstring.h>
#include "..\KAppTotal\Entity.h"

class CTotalDoc;
class IwBSplineCurve;
class IwBSplineSurface;

namespace Validation
{
	namespace OutlineProfile
	{
		CEntValidateStatus Validate(CTotalDoc* pDoc, QString &message, IwTArray<IwPoint3d>& unqualifiedLocations);
		bool		ValidateDistanceToCutProfile(CTotalDoc* pDoc, double allowableDistMax, double allowableDistMin, IwTArray<IwPoint3d>& unqualifiedLocations, IwTArray<double>& unqualifiedDistances, IwTArray<int>& overhangings);
		bool		ValidateDistanceToACutFace(CTotalDoc* pDoc, IwTArray<IwPoint3d>& cutFeaturePoints, IwBSplineSurface* sketchSurface, IwBSplineCurve* uvCurve, int faceIndex, bool positiveSide, double allowableDistMax, double allowableDistMin, IwTArray<IwPoint3d>& unqualifiedLocations, IwTArray<double>& unqualifiedDistances, IwVector3d* optVector=NULL);
		bool		ValidateDistanceToBothPosteriorCutProfiles(CTotalDoc* pDoc, double allowableDistMax, double allowableDistMin, IwTArray<IwPoint3d>& unqualifiedLocations, IwTArray<double> &unqualifiedDistances, IwTArray<int>& overhangings);
		bool		ValidateDistanceToLateralAntCutProfile(CTotalDoc* pDoc, IwTArray<IwPoint3d>& unqualifiedLocations, IwTArray<double>& unqualifiedDistances, bool earOnly=false, IwPoint3d* arc2ndPoint=NULL, double* arc2ndDist=NULL);
		double		ValidatePosteriorDistanceToPosteriorCutProfile(CTotalDoc* pDoc, int faceIndex, IwTArray<IwPoint3d>& locations, IwTArray<double>& distances);
		bool		ValidatePatellaCoverage(CTotalDoc* pDoc);
		bool		ValidateMLSize(CTotalDoc* pDoc);
		double		ValidateAnteriorEarProfile(CTotalDoc* pDoc);
		void		ValidateNotchProfile(CTotalDoc* pDoc, bool& goodTipDist, bool& goodEighteenWidth, bool& leaningMedially, double& tipDist, double& eighteenWidth);
		bool		ValidateMedialAnteriorProfile(CTotalDoc* pDoc, IwPoint3d* recommendedPoint=NULL);
	};
};
