#include "SelectImplantSimple.h"
#include "ui_SelectImplantSimple.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"
#include "..\KUtility\KUtility.h"
#include "..\KUtility\KPropertyFile.h"
#include "..\KUtility\KApplicationMode.h"
#include "..\KUtility\KImplantFolder.h"
#include "..\KUtility\KPropertyFile.h"

#include <QSettings>
#include <QTextStream>
#include <qfiledialog.h>
#include <qdom.h>

SelectImplantSimple::SelectImplantSimple(QWidget *parent)
    : QDialog(parent)
{
    ui = new Ui::SelectImplantSimpleClass();
    ui->setupUi(this);

	QSettings settings;
	QString eksYoonee = mainWindow->GetImplant()->strProdFolder();
    basePath = settings.value("ImplantRootFolder", eksYoonee).toString();

    if (!mainWindow->GetImplant()->isProduction())
    {
	   // Test version: make sure the root isn't X:/UNI or its subfolders
	   if( QFileInfo(basePath).canonicalFilePath().startsWith(eksYoonee, Qt::CaseInsensitive) )
		   basePath = QString("X:/TestData/") + mainWindow->GetAppName();
    }
	ui->lineEdit->setText(basePath);

    PopulateComboBox();

    connect(ui->lineEdit, SIGNAL(editingFinished()), this, SLOT(on_LineEdit_editingFinished()));
    connect(ui->comboImplantList, SIGNAL(currentIndexChanged(int)), this, SLOT(on_comboImplantList_indexChanged(int)));
}

SelectImplantSimple::~SelectImplantSimple()
{
    delete ui;
}

void SelectImplantSimple::on_pushButtonBrowse_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Select the implant root folder", ui->lineEdit->text());
    if( path != "" )
    {
        ui->lineEdit->setText( path );
        on_LineEdit_editingFinished();
    }
}

void SelectImplantSimple::on_LineEdit_editingFinished()
{
    QString str = ui->lineEdit->text();
    if( str==basePath )
        return;

    if( !QDir(str).exists() )
    {
        ui->lineEdit->setText(basePath);
        QMessageBox::information(this, "Error", "Folder doesn't exist:" + str);
        return;
    }
    if (!mainWindow->GetImplant()->isProduction())
    {
	   // Test version: make sure the root isn't X:/UNI or its subfolders
		QString eksYoonee = mainWindow->GetImplant()->strProdFolder();
	   if( QFileInfo(str).canonicalFilePath().startsWith(eksYoonee, Qt::CaseInsensitive) )
	   {
		   ui->lineEdit->setText(basePath);
           QMessageBox::information(this, "Error", QString("Test version cannot set implant root to %1 or its subfolders").arg(eksYoonee));
           return;
	   }
    }

    QSettings settings;
    settings.setValue("ImplantRootFolder", str);

    basePath = str;
    PopulateComboBox();
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Fill the combo box with the directory names in the UNIDIR folder
void SelectImplantSimple::PopulateComboBox()
{
    ui->comboImplantList->clear();
	QDir uniDir(basePath);
	
	ui->comboImplantList->addItem("<Select an implant>");
	QStringList implants = uniDir.entryList(QDir::Dirs, QDir::Name);
	for (QStringList::Iterator it = implants.begin(); it != implants.end(); ++it)
	{	
		if (*it == "." || *it == "..")
			continue;
		ui->comboImplantList->addItem(*it);
	}
}

// Set the current implant name
void SelectImplantSimple::SetImplantName(QString& name)
{
    int i = ui->comboImplantList->findText(name);
    ui->comboImplantList->setCurrentIndex( i<0 ? 0 : i);
}

void SelectImplantSimple::on_comboImplantList_indexChanged(int index)
{
    if( index==-1 )
        return;

    ui->pushButtonOpen->setEnabled( false );
    ui->labelInfo->clear();

    int vv = ui->comboImplantList->currentIndex();
    if( ui->comboImplantList->currentIndex()==0 )
        return;

    // parse the property file
    implantName = ui->comboImplantList->currentText();
    caseFolderName = implantName;

    QString pfPath = basePath + QDir::separator() + caseFolderName + QDir::separator() 
        + implantName + "_info.txt";
    KPropertyFile pf(pfPath);
    if( !pf.loadFile() )
    {
        QMessageBox::information(this, "Error", "Failed to load the property file:" + pfPath);
        return;
    }

    implantType = pf.getValue("ImplantType");
    kneeType = pf.getValue("Knee");
    compartmentType = pf.getValue("Compartment");
    country = pf.getValue("Country");

    if( implantType.compare("TriathlonF1",Qt::CaseInsensitive)==0 )
        implantType = "TriathlonF1";
    else if( implantType.compare("TriathlonF1PS",Qt::CaseInsensitive)==0 )
        implantType = "TriathlonF1PS";
    else if( implantType.compare("iDuo",Qt::CaseInsensitive)==0 )
        implantType = "iDuo";
    else if( implantType.compare("iUni",Qt::CaseInsensitive)==0 )
        implantType = "iUni";
    else
    {
        QMessageBox::information(this, "Error", "Invalid implant type");
        return;
    }

    // display the implant info
    QString strInfo = implantType;
    if( !kneeType.isEmpty() )
        strInfo += ", " + kneeType;
    if( !compartmentType.isEmpty() )
        strInfo += ", " + compartmentType;
    if( country.isEmpty() )
    {
        // for older implant without this information
        zone = "";
    }
    else
    {
        zone = pf.getValue("Zone");
        strInfo += ", " + country + " (" + zone + ")";
    }

    catalogNumber = pf.getValue("CatalogNumber");
    if( !catalogNumber.isEmpty() )
    {
        strInfo += ", " + catalogNumber;

        catalogNumberRev = pf.getValue("CatalogNumberRev");
        if( !catalogNumberRev.isEmpty() )
            strInfo += "(" + catalogNumberRev + ")";
    }

    kitEDNumber = pf.getValue("KitEDNumber");
    if( !kitEDNumber.isEmpty() )
    {
        strInfo += ", " + kitEDNumber;

        kitEDNumberRev = pf.getValue("KitEDNumberRev");
        if( !kitEDNumberRev.isEmpty() )
            strInfo += "(" + kitEDNumberRev + ")";
    }

    ui->labelInfo->setText( strInfo );

    QDir baseDir(basePath);
    imgDir = baseDir.absoluteFilePath(caseFolderName + "/CT");
    segDir = baseDir.absoluteFilePath(caseFolderName + "/Segmentation");
    surfDir = baseDir.absoluteFilePath(caseFolderName + "/Surfaces");
    stlDir = baseDir.absoluteFilePath(caseFolderName + "/STL");
    sldworksDir = baseDir.absoluteFilePath(caseFolderName + "/SolidWorks");

    ui->pushButtonOpen->setEnabled( true );
}


void SelectImplantSimple::on_pushButtonOpen_clicked()
{
    accept();
}

void SelectImplantSimple::on_pushButtonCancel_clicked()
{
    reject();
}

