#include "KXmlHelper.h"

#define PRECISION 12

// Functions for status nodes in XML file
void AppendNode(QDomElement& elem, const QString& tagName, int v)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(QString::number(v)) );
}

void AppendNode(QDomElement& elem, const QString& tagName, float v)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(QString::number(v, 'g', PRECISION)) );
}

void AppendNode(QDomElement& elem, const QString& tagName, double v)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(QString::number(v, 'g', PRECISION)) );
}

void AppendNode(QDomElement& elem, const QString& tagName, const QString& s)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(s) );
}

QDomNode GetFirstChildNodeByTag(const QDomNode &node, const QString &tag)
{
	for(int i=0; i<node.childNodes().count(); i++)
		if(node.childNodes().at(i).nodeName().toLower() == tag.toLower())
			return node.childNodes().at(i);

	return QDomNode();
}

QDomNode GetFirstChildNodeByAttribute(const QDomNode &node, const QString &attName, const QString &attValue)
{
	for(int i=0; i<node.childNodes().count(); i++)
	{
		QDomNamedNodeMap attribs = node.childNodes().at(i).attributes();
		QDomNode namedAttr = attribs.namedItem(attName);
		if( !namedAttr.isNull() && namedAttr.nodeValue().toLower() == attValue.toLower())
			return node.childNodes().at(i);
	}
	return QDomNode();
}

void AppendNode(QDomElement& elem, const QString& tagName, const QColor& color)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(QString("%1,%2,%3").arg(color.red()).arg(color.green()).arg(color.blue())) );
}

void AppendNode(QDomElement& elem, const QString& tagName, const std::vector<int>& vs)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    QStringList sl;
    sl.clear();
    for(size_t i=0; i<vs.size(); i++)
        sl << QString::number(vs[i]);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(sl.join(",")) );
}

void AppendNode(QDomElement& elem, const QString& tagName, const std::vector<float>& vs)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    QStringList sl;
    sl.clear();
    for(size_t i=0; i<vs.size(); i++)
        sl << QString::number(vs[i], 'g', PRECISION);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(sl.join(",")) );
}

void AppendNode(QDomElement& elem, const QString& tagName, const Box3Float& box)
{
    Point3Float cen = box.GetCenter();
    Point3Float ax0 = box.GetAxis(0);
    Point3Float ax1 = box.GetAxis(1);
    Point3Float ax2 = box.GetAxis(2);

    std::vector<float> vs;
    vs.reserve(15);
    vs.push_back(cen.x()); vs.push_back(cen.y()); vs.push_back(cen.z());
    vs.push_back(ax0.x()); vs.push_back(ax0.y()); vs.push_back(ax0.z());
    vs.push_back(ax1.x()); vs.push_back(ax1.y()); vs.push_back(ax1.z());
    vs.push_back(ax2.x()); vs.push_back(ax2.y()); vs.push_back(ax2.z());
    vs.push_back(box.GetSpan(0));
    vs.push_back(box.GetSpan(1));
    vs.push_back(box.GetSpan(2));

    AppendNode(elem, tagName, vs);
}

// Functions for parsing a status node 'elem' in the XML file.
bool ParseNode(const QDomElement& elem, int& v)
{
    bool ok;
    v = elem.text().toInt(&ok);
    return ok;
}

bool ParseNode(const QDomElement& elem, float& v)
{
    bool ok;
    v = elem.text().toFloat(&ok);
    return ok;
}

bool ParseNode(const QDomElement& elem, double& v)
{
    bool ok;
    v = elem.text().toDouble(&ok);
    return ok;
}

bool ParseNode(const QDomElement& elem, QString& s)
{
    s = elem.text();
    return true;
}

bool ParseNode(const QDomElement& elem, QColor& color)
{
    QStringList sl = elem.text().split(",");
    if( sl.size()!=3 )
        return false;
    bool ok[3];
    color = QColor(sl[0].toInt(&ok[0]), sl[1].toInt(&ok[1]), sl[2].toInt(&ok[2]));
    return ok[0] && ok[1] && ok[2];
}

bool ParseNode(const QDomElement& elem, Rotate3Float& r)
{
    QStringList sl = elem.text().split(',');
    if( sl.size() != 9 )
        return false;
    bool ok;
    float f[9];
    for(int i=0; i<9; i++)
    {
        f[i] = sl[i].toFloat(&ok);
        if( !ok )
            return false;
    }
    r(0,0)=f[0];
    r(0,1)=f[1];
    r(0,2)=f[2];
    r(1,0)=f[3];
    r(1,1)=f[4];
    r(1,2)=f[5];
    r(2,0)=f[6];
    r(2,1)=f[7];
    r(2,2)=f[8];
    return true;
}

bool ParseNode(const QDomElement& elem, Point3Float& pt)
{
    QStringList sl = elem.text().split(',');
    if( sl.size() != 3 )
        return false;
    bool ok[3];
    pt.SetXYZ(sl[0].toFloat(&ok[0]), sl[1].toFloat(&ok[1]), sl[2].toFloat(&ok[2]) );
    return ok[0] && ok[1] && ok[2];
}

bool ParseNode(const QDomElement& elem, std::vector<int>& vs)
{
    QStringList sl = elem.text().split(',');
    const int sz = sl.size();
    vs.resize(sz);
    bool ok;
    for(int i=0; i<sz; i++)
    {
        vs[i] = sl[i].toInt(&ok);
        if( !ok )
            return false;
    }
    return true;
}

bool ParseNode(const QDomElement& elem, int size, std::vector<int>& vs)
{
    QStringList sl = elem.text().split(',');
    if( sl.size() != size )
        return false;
    vs.resize(size);
    bool ok;
    for(int i=0; i<size; i++)
    {
        vs[i] = sl[i].toInt(&ok);
        if( !ok )
            return false;
    }
    return true;
}

bool ParseNode(const QDomElement& elem, int size, std::vector<float>& vs)
{
    QStringList sl = elem.text().split(',');
    if( sl.size() != size )
        return false;
    vs.resize(size);
    bool ok;
    for(int i=0; i<size; i++)
    {
        vs[i] = sl[i].toFloat(&ok);
        if( !ok )
            return false;
    }
    return true;
}

bool ParseNode(const QDomElement& elem, Box3Float& box)
{
    QStringList sl = elem.text().split(',');
    if( sl.size() != 15 )
        return false;
    bool ok;
    float f[15];
    for(int i=0; i<15; i++)
    {
        f[i] = sl[i].toFloat(&ok);
        if( !ok )
            return false;
    }
    box.SetCenter(f[0], f[1], f[2]);
    box.SetAxis(Point3Float(f[3], f[4], f[5]),
                Point3Float(f[6], f[7], f[8]),
                Point3Float(f[9], f[10], f[11]));
    box.SetSpan(f[12], f[13], f[14]);
    return true;
}
