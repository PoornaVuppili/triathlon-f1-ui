#pragma once

#include "..\KTriathlonF1\TotalDoc.h"

class KTotalViewPS;
class KTotalEntPanelPS;

class KTotalDocPS : public CTotalDoc
{
public:

	KTotalDocPS(QObject* parent = 0);
	~KTotalDocPS();
	static KTotalDocPS* GetTotalDocPS() { return (KTotalDocPS*)m_pTotalDocPS; };
	static void SetTotalDocPS(KTotalDocPS* pDoc) { m_pTotalDocPS = pDoc; };
	KTotalViewPS* GetView() { return ((KTotalViewPS*)m_pView); };
	KTotalEntPanelPS* GetEntPanel() { return ((KTotalEntPanelPS*)m_pEntPanel); };
	void				SetEntPanelPSFeatures(KTotalEntPanelPS* pEntPanelPSFeatures) { m_pEntPanelPSFeatures = pEntPanelPSFeatures; };
	KTotalEntPanelPS* GetEntPanelPSFeatures() { return ((KTotalEntPanelPS*)m_pEntPanelPSFeatures); };

	virtual void		Clear();
	virtual void		AddEntity(CEntity* pEntity, bool bAddToEntPanel = true);
	virtual bool		Save(const QString& sDocFileName, bool incrementalSave = false);
	virtual bool		Load(const QString& sDocFileName);
	virtual bool		LoadWithOptions(const QString& sDocFileName, QString options = QString(""));

	bool                DeleteEntityById(int id, bool bDestroyEntity = true) override;

	virtual void		SetEntPanelStatusMarker(CEntRole eEntRole, bool status);
	virtual void		UpdateEntPanelStatusMarker(CEntRole eEntRole, bool bUpToDate = true, bool implicitlyUpdate = true);
	virtual void		SetEntPanelValidateStatus(CEntRole eEntRole, CEntValidateStatus status, QString message);
	virtual void		SetEntPanelFontColor(CEntRole entRole, CColor color);
	virtual void		SetEntPanelToolTip(CEntRole entRole, QString message);
	virtual bool		IsFemoralSidePSFeaturesDesignComplete();
	virtual void		InitializeValidationReportPS();
	virtual void		AppendValidationReportPSEntry(QString const& entry, bool goodResult = true);
	virtual QString		GetValidationReportPS(bool& allGoodResults);
	int					CountEntitiesBelongToPS();
	virtual void		ToggleEntPanelItemByID(int id, bool On); // over write in TotalDoc
	virtual void		RemoveEntityFromEntPanel(CEntRole eEntRole);// over write in DataDoc
	virtual void		DeleteEntitiesBelongToPSFeatures();
	void				DeletePSEntityById(int id, bool bDestroyEntity = true);
	// Special purpose
	virtual bool		GetMotionPatternsExtendToolbar();

public:
	static KTotalDocPS* m_pTotalDocPS;

protected:
	KTotalEntPanelPS* m_pEntPanelPSFeatures;

private:
	ValidationReport			m_validationReportPS;
	bool						m_validationPSAllGoodResults;
};
