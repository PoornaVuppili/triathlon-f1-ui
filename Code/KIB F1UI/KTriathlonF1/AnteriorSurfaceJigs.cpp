#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "AnteriorSurfaceJigs.h"
#include "IFemurImplant.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"

CAnteriorSurfaceJigs::CAnteriorSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_PART );

	m_eDisplayMode = DISP_MODE_DISPLAY;

	m_color = CColor(150, 150, 150);

	m_bASJigsModified = false;
	m_bASJigsDataModified = false;
	m_glAnteriorSurfaceList = 0;

}

CAnteriorSurfaceJigs::~CAnteriorSurfaceJigs()
{
}

void CAnteriorSurfaceJigs::Display()
{
	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	// Display anterior peak point
	glDisable( GL_LIGHTING );

	if( m_bASJigsModified && m_glAnteriorSurfaceList > 0 )
		DeleteDisplayList( m_glAnteriorSurfaceList );

	if( m_glAnteriorSurfaceList == 0 )
	{
		m_glAnteriorSurfaceList = NewDisplayList();
		glNewList( m_glAnteriorSurfaceList, GL_COMPILE_AND_EXECUTE );
		DisplayASJigsPoints();
		glEndList();
	}
	else
	{
		glCallList( m_glAnteriorSurfaceList );
	}

	glEnable( GL_LIGHTING );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	m_bASJigsModified = false;

	CPart::Display();

}

void CAnteriorSurfaceJigs::DisplayASJigsPoints()
{

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glPointSize(6);

	glColor3ub( 0, 0, 0 );

	glBegin( GL_POINTS );
		if ( m_lateralPeakPoint.IsInitialized() )
			glVertex3d( m_lateralPeakPoint.x, m_lateralPeakPoint.y, m_lateralPeakPoint.z );

	glEnd();

	glPointSize(1);

}

bool CAnteriorSurfaceJigs::Save( const QString& sDir, bool incrementalSave )
{
	CPart::Save(sDir, incrementalSave);

	QString				sFileName;
	bool				bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bASJigsDataModified ) 
		return bOK;

	//
	sFileName = sDir + "\\" + m_sFileName + "_as_jigs.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	File.write( (char*) &m_lateralPeakPoint, sizeof( IwPoint3d ) );

	// Parameters for iTW v6.0.24, but retired in iTW 6.0.26
	unsigned osteophyteTimeStamp = 0;
	unsigned cartilageTimeStamp = 0;
	File.write( (char*) &osteophyteTimeStamp, sizeof(unsigned) );
	File.write( (char*) &cartilageTimeStamp, sizeof(unsigned) );

	File.close();

	m_bASJigsDataModified = false;

	return true;
}


bool CAnteriorSurfaceJigs::Load( const QString& sDir )
{
	CPart::Load(sDir);

	QString				sFileName;
	bool				bOK;

	//
	sFileName = sDir + "\\" + m_sFileName + "_as_jigs.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;


	// Variables for Jigs version 5.0.0
	IwPoint3d lateralPeakPoint;
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		File.read( (char*) &lateralPeakPoint, sizeof(IwPoint3d) );
	}
	// variables for iTW 6.0.24, but retired in iTW 6.0.26
	if (m_pDoc->IsOpeningFileRightVersion(6,0,24))
	{
		unsigned osteophyteTimeStamp, cartilageTimeStamp;
		File.read( (char*) &osteophyteTimeStamp, sizeof( unsigned ) );
		File.read( (char*) &cartilageTimeStamp, sizeof( unsigned ) );
	}

	File.close();

	// Actions for Jigs version 5.0.0
	if (m_pDoc->IsOpeningFileRightVersion(5,0,0))
	{
		this->SetLateralPeakPoint(lateralPeakPoint);
	}

	m_bASJigsDataModified = false;

	return true;
}

void CAnteriorSurfaceJigs::SetASJigsModifiedFlag( bool bModified )
{
	m_bASJigsModified = bModified;
	if (m_bASJigsModified)
	{
		m_bASJigsDataModified = true;
		m_pDoc->SetModified(m_bASJigsModified);
	}
}

void CAnteriorSurfaceJigs::SetLateralPeakPoint(IwPoint3d& peakPoint)
{
	m_lateralPeakPoint = peakPoint;
	SetASJigsModifiedFlag(true);
}

IwPoint3d CAnteriorSurfaceJigs::GetLateralPeakPoint()
{
	return m_lateralPeakPoint;
}

/////////////////////////////////////////////////////////////////////////////////
// This is a special function for output to support F3 creation in SolidWorks.
/////////////////////////////////////////////////////////////////////////////////
// This is a special function for output to support F3 creation in SolidWorks.
IwBrep*	CAnteriorSurfaceJigs::SolidifyAnteriorSurfaceForOutput()
{
	IwBrep* resultBrep = NULL;

	resultBrep = SolidifyAnteriorSurfaceForOutput_Trial(3, false);
	if ( resultBrep == NULL )
		resultBrep = SolidifyAnteriorSurfaceForOutput_Trial(4, false);
	if ( resultBrep == NULL )
		resultBrep = SolidifyAnteriorSurfaceForOutput_Trial(2, false);
	if ( resultBrep == NULL )
		resultBrep = SolidifyAnteriorSurfaceForOutput_Trial(5, false);
	if ( resultBrep == NULL )
		resultBrep = SolidifyAnteriorSurfaceForOutput_Trial(1, false);
	if ( resultBrep == NULL )
		resultBrep = SolidifyAnteriorSurfaceForOutput_Trial(6, true);

	return resultBrep;
}

/////////////////////////////////////////////////////
// Use different tolerances
IwBrep*	CAnteriorSurfaceJigs::SolidifyAnteriorSurfaceForOutput_Trial(int tolLevel, bool finalTrial)
{
	IwBrep* resultBrep = NULL;

	IwBrep* antSurfBrep = GetIwBrep();
	if ( antSurfBrep == NULL )
		return NULL;

	IwTArray<IwFace*> faces;
	antSurfBrep->GetFaces(faces);
	if ( faces.GetSize()==0 )
		return NULL;
	// copy surface
	IwBSplineSurface* antSurf = (IwBSplineSurface*)faces.GetAt(0)->GetSurface();
	IwSurface* surfCopy;
	antSurf->Copy(m_pDoc->GetIwContext(), surfCopy);
	IwBSplineSurface* antSurfCopy = (IwBSplineSurface*)surfCopy;

	// create 2 iso curves near min/max U, which are close to anterior/posterior and almost planar
	IwExtent2d uvDom = antSurfCopy->GetNaturalUVDomain();
	double nearMinU = uvDom.Evaluate(0.015, 0.5).x;
	double nearMaxU = uvDom.Evaluate(0.985, 0.5).x;
	IwBSplineCurve *nearMinUCurve, *nearMaxUCurve;
	antSurfCopy->CreateIsoParametricCurve(m_pDoc->GetIwContext(), IW_SP_U, nearMinU, 0.01, nearMinUCurve);
	antSurfCopy->CreateIsoParametricCurve(m_pDoc->GetIwContext(), IW_SP_U, nearMaxU, 0.01, nearMaxUCurve);
	// Convert to points
	IwExtent1d crvDom = nearMinUCurve->GetNaturalInterval();
	IwTArray<IwPoint3d> points;
	nearMinUCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), 50, 0.01, &points, NULL);
	// Create nearMinUSurface
	IwBSplineSurface *nearMinUSurface, *nearMinUSurfaceExt;
	IwBSplineSurface::ApproximateRandomPoints(m_pDoc->GetIwContext(), points, 4, 4, 3, 3, NULL, 1, nearMinUSurface);
	nearMinUSurface->CreateExtendedSurface(m_pDoc->GetIwContext(),  5.0, IW_CT_G1, nearMinUSurfaceExt); 
	if (nearMinUSurface)
		IwObjDelete(nearMinUSurface);
	// Convert to points
	points.RemoveAll();
	crvDom = nearMaxUCurve->GetNaturalInterval();
	nearMaxUCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), 50, 0.01, &points, NULL);
	// Create nearMaxUSurface
	IwBSplineSurface *nearMaxUSurface, *nearMaxUSurfaceExt;
	IwBSplineSurface::ApproximateRandomPoints(m_pDoc->GetIwContext(), points, 4, 4, 3, 3, NULL, 1, nearMaxUSurface);
	nearMaxUSurface->CreateExtendedSurface(m_pDoc->GetIwContext(),  5.0, IW_CT_G1, nearMaxUSurfaceExt); 
	if (nearMaxUSurface)
		IwObjDelete(nearMaxUSurface);

	// create 2 iso curves near min/max V, which are the 2 side curves
	double nearMinV = uvDom.Evaluate(0.5, 0.015).y;
	double nearMaxV = uvDom.Evaluate(0.5, 0.985).y;
	IwBSplineCurve *nearMinVCurve, *nearMaxVCurve;
	antSurfCopy->CreateIsoParametricCurve(m_pDoc->GetIwContext(), IW_SP_V, nearMinV, 0.01, nearMinVCurve);
	antSurfCopy->CreateIsoParametricCurve(m_pDoc->GetIwContext(), IW_SP_V, nearMaxV, 0.01, nearMaxVCurve);

	// Convert to points
	points.RemoveAll();
	crvDom = nearMinVCurve->GetNaturalInterval();
	double length;
	nearMinVCurve->Length(crvDom, 0.01, length);
	int simplingNo = (int)length/2.0;
	nearMinVCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), simplingNo, 0.01, &points, NULL);
	IwPoint3d nearMinVStartPoint = points.GetAt(0);
	IwPoint3d nearMinVEndPoint = points.GetLast();
	// Approximate to nearMinVCurveSmooth
	double tol = 1.0;
	IwBSplineCurve *nearMinVCurveSmooth;
	IwBSplineCurve::ApproximatePoints(m_pDoc->GetIwContext(), points, 3, NULL, NULL, FALSE, &tol, nearMinVCurveSmooth);
	// Convert to points
	points.RemoveAll();
	crvDom = nearMaxVCurve->GetNaturalInterval();
	nearMaxVCurve->Length(crvDom, 0.01, length);
	simplingNo = (int)length/2.0;
	nearMaxVCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), simplingNo, 0.01, &points, NULL);
	IwPoint3d nearMaxVStartPoint = points.GetAt(0);
	IwPoint3d nearMaxVEndPoint = points.GetLast();
	// Approximate to nearMaxVCurveSmooth
	IwBSplineCurve *nearMaxVCurveSmooth;
	IwBSplineCurve::ApproximatePoints(m_pDoc->GetIwContext(), points, 3, NULL, NULL, FALSE, &tol, nearMaxVCurveSmooth);
	//
	if (nearMinVCurve)
		IwObjDelete(nearMinVCurve);
	if (nearMaxVCurve)
		IwObjDelete(nearMaxVCurve);

	// Now use nearMinVCurveSmooth and nearMaxVCurveSmooth and (nearMinVStartPoint,nearMaxVStartPoint) and (nearMinVEndPoint,nearMaxVEndPoint) to create a coon patch
	IwBSplineCurve *lineSegmentStart, *lineSegmentEnd;
	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, nearMinVStartPoint, nearMaxVStartPoint, lineSegmentStart);
	IwBSplineCurve::CreateLineSegment(m_pDoc->GetIwContext(), 3, nearMinVEndPoint, nearMaxVEndPoint, lineSegmentEnd);
	//
	IwTArray<IwBSplineCurve*> boundaryCurves;
	boundaryCurves.Add(nearMinVCurveSmooth);
	boundaryCurves.Add(nearMaxVCurveSmooth);
	boundaryCurves.Add(lineSegmentStart);
	boundaryCurves.Add(lineSegmentEnd);

	//
	IwBSplineSurface *coonsPatch, *bottomSurface;
	IwBSplineSurface::CreateCoonsPatch(m_pDoc->GetIwContext(), 0.01, TRUE, boundaryCurves, NULL, coonsPatch);
	//
	coonsPatch->CreateExtendedSurface(m_pDoc->GetIwContext(), 5.0, IW_CT_G1, bottomSurface); 
	// 
	if (coonsPatch)
		IwObjDelete(coonsPatch);

	// Create a solid body
	// Create Brep first
	IwBrep* Brep0 = new ( m_pDoc->GetIwContext() ) IwBrep;
	IwFace* aFace;
	Brep0->CreateFaceFromSurface(nearMinUSurfaceExt, nearMinUSurfaceExt->GetNaturalUVDomain(), aFace);
	Brep0->CreateFaceFromSurface(nearMaxUSurfaceExt, nearMaxUSurfaceExt->GetNaturalUVDomain(), aFace);

	IwBrep* Brep1 = new ( m_pDoc->GetIwContext() ) IwBrep;
	Brep1->CreateFaceFromSurface(antSurfCopy, antSurfCopy->GetNaturalUVDomain(), aFace);

	IwBrep* Brep2 = new ( m_pDoc->GetIwContext() ) IwBrep;
	Brep2->CreateFaceFromSurface(bottomSurface, bottomSurface->GetNaturalUVDomain(), aFace);

	// Set tolerance
	double mergeTol;
	double mergeAngTol;
	// Note, these tolerance levels are not linear;
	if (tolLevel == 1)
	{
		mergeTol = 0.25;
		mergeAngTol = 25.0*IW_PI/180.0;
	}
	else if (tolLevel == 2)
	{
		mergeTol = 0.015;
		mergeAngTol = 5.0*IW_PI/180.0;
	}
	else if (tolLevel == 3)
	{
		mergeTol = 0.001;
		mergeAngTol = 2.5*IW_PI/180.0;
	}
	else if (tolLevel == 4)
	{
		mergeTol = 0.0005;
		mergeAngTol = 1*IW_PI/180.0;
	}
	else if (tolLevel == 5)
	{
		mergeTol = 0.00002;
		mergeAngTol = 0.25*IW_PI/180.0;
	}
	else if (tolLevel == 6)
	{
		mergeTol = 0.00001;
		mergeAngTol = 0.125*IW_PI/180.0;
	}

	// Merge one by one
	IwBrep* Brep3=NULL;
	IwMerge merge3(m_pDoc->GetIwContext(), Brep0, Brep1, mergeTol, mergeAngTol);
	merge3.NonManifoldBoolean(IW_BO_MERGE, Brep3);
	if ( Brep3 == NULL )
		return NULL;

	IwBrep* Brep4=NULL;
	IwMerge merge4(m_pDoc->GetIwContext(), Brep2, Brep3, mergeTol, mergeAngTol);
	merge4.NonManifoldBoolean(IW_BO_MERGE, Brep4);
	if ( Brep4 == NULL )
		return NULL;

	IwStatus error = Brep4->MakeManifold();
	if ( error != IW_SUCCESS )
		return NULL;

	Brep4->SewAndOrient();

	// If the Brep contains more than one volume, then only take the largest one (the region with max faces). 
	IwBrep* rBrep=Brep4;
	IwTArray<IwRegion*> regions;
	rBrep->GetRegions(regions);
	if (regions.GetSize()>2) // If there are more than 2 regions, i.e., more than 1 volume of solid body,
	{
		unsigned maxFaceNo=0;
		IwTArray<IwShell*> shells;
		IwTArray<IwFaceuse*> faceUses;
		IwTArray<IwFace*> faces;
		for (unsigned i=0; i<regions.GetSize(); i++)
		{
			IwRegion* region = regions.GetAt(i);
			IwShell* shell;
			if ( !region->IsVoid() )// not infinite (outside) region
			{	
				region->GetShells(shells);
				for (unsigned j=0; j<shells.GetSize(); j++)
				{
					shell = shells.GetAt(j);
					shell->GetFaceuses(faceUses);
					if (faceUses.GetSize() > maxFaceNo)
					{
						maxFaceNo = faceUses.GetSize();
						faces.RemoveAll();
						for (unsigned k=0; k<faceUses.GetSize();k++)
						{
							faces.Add(faceUses.GetAt(k)->GetFace());
						}
					}
				}
			}
		}

		// If there are more than 2 regions, i.e., more than 1 volume of solid body,
		// take the region with max faces.
		if ( regions.GetSize()>2 )
		{
			IwBrep* singleVolumeBrep = new (m_pDoc->GetIwContext()) IwBrep();
			rBrep->CopyFaces(faces, singleVolumeBrep);
			singleVolumeBrep->SewAndOrient();
			if (rBrep) IwObjDelete(rBrep);
			rBrep = singleVolumeBrep;
		}
	}

	//
	resultBrep = rBrep;
	resultBrep->SewAndOrient();

if (0)
ShowBrep(m_pDoc, resultBrep, "SolidifyAnteriorSurface", brown);

	return resultBrep;
}
