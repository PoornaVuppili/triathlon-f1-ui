/********************************************************************************
** Form generated from reading UI file 'KInputValueSlider.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KINPUTVALUESLIDER_H
#define UI_KINPUTVALUESLIDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_InputValueSliderClass
{
public:
    QSlider *horizontalSlider;
    QWidget *layoutWidget;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QSpinBox *spinBox;
    QWidget *widget;
    QHBoxLayout *hboxLayout1;
    QPushButton *pushButtonOK;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *InputValueSliderClass)
    {
        if (InputValueSliderClass->objectName().isEmpty())
            InputValueSliderClass->setObjectName(QString::fromUtf8("InputValueSliderClass"));
        InputValueSliderClass->resize(333, 117);
        horizontalSlider = new QSlider(InputValueSliderClass);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(10, 50, 311, 21));
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider->setTickPosition(QSlider::NoTicks);
        layoutWidget = new QWidget(InputValueSliderClass);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(11, 21, 311, 22));
        hboxLayout = new QHBoxLayout(layoutWidget);
        hboxLayout->setSpacing(6);
        hboxLayout->setContentsMargins(11, 11, 11, 11);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        hboxLayout->addWidget(label);

        spinBox = new QSpinBox(layoutWidget);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(spinBox->sizePolicy().hasHeightForWidth());
        spinBox->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(spinBox);

        widget = new QWidget(InputValueSliderClass);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(140, 80, 176, 27));
        hboxLayout1 = new QHBoxLayout(widget);
        hboxLayout1->setSpacing(20);
        hboxLayout1->setContentsMargins(11, 11, 11, 11);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        pushButtonOK = new QPushButton(widget);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        hboxLayout1->addWidget(pushButtonOK);

        pushButtonCancel = new QPushButton(widget);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        hboxLayout1->addWidget(pushButtonCancel);


        retranslateUi(InputValueSliderClass);

        pushButtonOK->setDefault(true);


        QMetaObject::connectSlotsByName(InputValueSliderClass);
    } // setupUi

    void retranslateUi(QDialog *InputValueSliderClass)
    {
        InputValueSliderClass->setWindowTitle(QCoreApplication::translate("InputValueSliderClass", "InputValueSlider", nullptr));
        label->setText(QCoreApplication::translate("InputValueSliderClass", "TextLabel", nullptr));
        pushButtonOK->setText(QCoreApplication::translate("InputValueSliderClass", "OK", nullptr));
        pushButtonCancel->setText(QCoreApplication::translate("InputValueSliderClass", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InputValueSliderClass: public Ui_InputValueSliderClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KINPUTVALUESLIDER_H
