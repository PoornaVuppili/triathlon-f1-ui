#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "SolidPositionJigs.h"
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"
#include "..\KAppTotal\Entity.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

enum CMakeSolidPositionJigsErrorCodes
{
	MAKE_SOLID_POSITION_JIGS_ERROR_CODES_NONE,
	MAKE_SOLID_POSITION_JIGS_ERROR_CODES_INPUT_SURFACES_NOT_GOOD,
	MAKE_SOLID_POSITION_JIGS_ERROR_CODES_TOO_THIN
};


struct CMakeSolidPositionJigsErrorCodesEntry
{
	CMakeSolidPositionJigsErrorCodes	ErrorCodes;
	QString								ErrorMessages;
};

class TEST_EXPORT_TW CMakeSolidPositionJigs : public CManager
{
    Q_OBJECT

public:
					CMakeSolidPositionJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CMakeSolidPositionJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

	virtual bool	MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp  ( const QPoint& cursor, Viewport* vp=NULL );
	virtual void	Display(Viewport* vp=NULL);
	static CEntValidateStatus Validate(CTotalDoc* pDoc, QString &message);
	static bool		ValidateThickness(CTotalDoc* pDoc, double& minThickness, IwPoint3d& minPnt0, IwPoint3d& minPnt1, bool& isOnEdge);
	static bool		ValidateF2NotchThickness(CTotalDoc* pDoc, double& minNotchThickness);
	static void		DetermineDistalPlanePoints(CTotalDoc* pDoc, IwTArray<IwPoint3d>& planeCornerPoints);
	static bool		DetermineTabFilletEdges(CTotalDoc* pDoc, IwBrep*& filletBrep, IwTArray<IwEdge*>& filletEdges, IwTArray<IwPoint3d>& filletPoints);
	static IwFace*	GetMainFace(CTotalDoc* pDoc, bool bInnerFace);

private:
	bool			Reset(int tolLevel=3, bool finalTrial=true);
	void			MyReset();
	void			DetermineF2Profiles();
	static bool		ValidateTabContact(CTotalDoc* pDoc, bool &bMedialAntContact, bool &bLateralMidContact, bool &bMedialPegContact);
	void			SetValidateIcon(CEntValidateStatus vStatus, QString message);
	// for error codes
	void			AddErrorCodes(CMakeSolidPositionJigsErrorCodes errorCode){m_errorCodes.AddUnique(errorCode);};
	void			GetErrorCodeMessage(CMakeSolidPositionJigsErrorCodes& errorCode, QString& errorMsg);
	void			DisplayErrorMessages();

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnReset();
	void			OnDisplayDistalPlanes();
	void			OnAccept();
	void			OnCancel();
	void			OnValidate();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewBack();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actMakeSolidPositionJigs;
	QAction*					m_actReset;
	QAction*					m_actDispDistalPlanes;
	QAction*					m_actValidate;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewRework;

	int							m_predefinedViewIndex;
	bool						m_updateSuccessfully;
	bool						m_displayDistalPlanes;

	IwTArray<IwPoint3d>			m_distalCutPlanePoints;

	// For error messages
	IwTArray<CMakeSolidPositionJigsErrorCodes>	m_errorCodes;
	static CMakeSolidPositionJigsErrorCodesEntry	MakeSolidPositionJigsErrorCodes[];
	//// For Validation ///////////////////////////////////////////////////////////////
	CEntValidateStatus			m_validateStatus;
	QString						m_validateMessage;

};
