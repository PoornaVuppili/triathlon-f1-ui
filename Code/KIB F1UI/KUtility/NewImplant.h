#ifndef NEWIMPLANT_H
#define NEWIMPLANT_H

#include <QDialog>
#include <QDir>
#include "ui_NewImplant.h"
#include <KOracleDatabase.h>

class NewImplant : public QDialog
{
    Q_OBJECT

public:
    NewImplant(QString implantRoot, QWidget *parent = 0);
    ~NewImplant();

    // Query the databased on the implant name

    // Get the implant name
    QString GetImplantName();

    // Get the implant type: "iUni, "iDuo" "TriathlonF1" or "TriathlonF1PS"
    QString GetImplantType();

    // Get the knee type : "Left" or "Right"
    QString GetKneeType();

    // Get the compartment type: "Medial" or "Lateral" or iUni/iDuo, "" for TriathlonF1, TriathlonF1PS
    QString GetCompartmentType();

    // Get the country and zone information
    QString GetCountryCode();
    QString GetZone();

private:
    Ui::NewImplantClass ui;

    // database related
    bool hasDatabase;       // whether a database is available to get the implant information
    KOracleDatabase db;     // database

    QString basePath;
    QList< QPair<QString,QString> > pairCountryZone;	// list of country + zone pairs
    QList<QString> zones;								// list of unique zones

    void EnableDisableOK();
    // reset all entry
    void ResetEntries();

private slots:
    void on_RadioButton_toggled(bool);
    void on_pushButtonCancel_clicked();
    void on_pushButtonOK_clicked();
    void on_lineEditImplantNum_textChanged(const QString &);
    void on_comboBoxCountry_currentIndexChanged(int);
	void on_comboBoxZone_currentIndexChanged(int);
    void on_pushButtonQuery_clicked();
};

#endif // NEWIMPLANT_H
