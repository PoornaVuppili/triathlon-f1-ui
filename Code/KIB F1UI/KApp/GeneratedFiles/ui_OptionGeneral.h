/********************************************************************************
** Form generated from reading UI file 'OptionGeneral.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTIONGENERAL_H
#define UI_OPTIONGENERAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionGeneral
{
public:
    QTabWidget *tabWidget;
    QWidget *tabColor;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listWidgetColor;
    QVBoxLayout *verticalLayout_2;
    QLabel *labelColor;
    QPushButton *pushButtonColor;
    QWidget *tabOracle;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QGridLayout *gridLayout;
    QLineEdit *lineEditDatabaseName;
    QLabel *labelHostname;
    QLineEdit *lineEditHostname;
    QLabel *labelPort;
    QLineEdit *lineEditPort;
    QLabel *labelUsername;
    QLineEdit *lineEditUsername;
    QLabel *labelPassword;
    QLineEdit *lineEditPassword;
    QLabel *labelDatabaseName;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButtonTestOracle;

    void setupUi(QWidget *OptionGeneral)
    {
        if (OptionGeneral->objectName().isEmpty())
            OptionGeneral->setObjectName(QString::fromUtf8("OptionGeneral"));
        OptionGeneral->resize(376, 368);
        tabWidget = new QTabWidget(OptionGeneral);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 374, 330));
        tabColor = new QWidget();
        tabColor->setObjectName(QString::fromUtf8("tabColor"));
        horizontalLayoutWidget_2 = new QWidget(tabColor);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 10, 301, 80));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        listWidgetColor = new QListWidget(horizontalLayoutWidget_2);
        listWidgetColor->setObjectName(QString::fromUtf8("listWidgetColor"));

        horizontalLayout_2->addWidget(listWidgetColor);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        labelColor = new QLabel(horizontalLayoutWidget_2);
        labelColor->setObjectName(QString::fromUtf8("labelColor"));
        labelColor->setFrameShape(QFrame::Box);

        verticalLayout_2->addWidget(labelColor);

        pushButtonColor = new QPushButton(horizontalLayoutWidget_2);
        pushButtonColor->setObjectName(QString::fromUtf8("pushButtonColor"));

        verticalLayout_2->addWidget(pushButtonColor);


        horizontalLayout_2->addLayout(verticalLayout_2);

        tabWidget->addTab(tabColor, QString());
        tabOracle = new QWidget();
        tabOracle->setObjectName(QString::fromUtf8("tabOracle"));
        horizontalLayoutWidget_3 = new QWidget(tabOracle);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(10, 10, 351, 142));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lineEditDatabaseName = new QLineEdit(horizontalLayoutWidget_3);
        lineEditDatabaseName->setObjectName(QString::fromUtf8("lineEditDatabaseName"));

        gridLayout->addWidget(lineEditDatabaseName, 0, 1, 1, 1);

        labelHostname = new QLabel(horizontalLayoutWidget_3);
        labelHostname->setObjectName(QString::fromUtf8("labelHostname"));

        gridLayout->addWidget(labelHostname, 1, 0, 1, 1);

        lineEditHostname = new QLineEdit(horizontalLayoutWidget_3);
        lineEditHostname->setObjectName(QString::fromUtf8("lineEditHostname"));

        gridLayout->addWidget(lineEditHostname, 1, 1, 1, 1);

        labelPort = new QLabel(horizontalLayoutWidget_3);
        labelPort->setObjectName(QString::fromUtf8("labelPort"));

        gridLayout->addWidget(labelPort, 2, 0, 1, 1);

        lineEditPort = new QLineEdit(horizontalLayoutWidget_3);
        lineEditPort->setObjectName(QString::fromUtf8("lineEditPort"));
        lineEditPort->setInputMethodHints(Qt::ImhDigitsOnly);

        gridLayout->addWidget(lineEditPort, 2, 1, 1, 1);

        labelUsername = new QLabel(horizontalLayoutWidget_3);
        labelUsername->setObjectName(QString::fromUtf8("labelUsername"));

        gridLayout->addWidget(labelUsername, 3, 0, 1, 1);

        lineEditUsername = new QLineEdit(horizontalLayoutWidget_3);
        lineEditUsername->setObjectName(QString::fromUtf8("lineEditUsername"));

        gridLayout->addWidget(lineEditUsername, 3, 1, 1, 1);

        labelPassword = new QLabel(horizontalLayoutWidget_3);
        labelPassword->setObjectName(QString::fromUtf8("labelPassword"));

        gridLayout->addWidget(labelPassword, 4, 0, 1, 1);

        lineEditPassword = new QLineEdit(horizontalLayoutWidget_3);
        lineEditPassword->setObjectName(QString::fromUtf8("lineEditPassword"));
        lineEditPassword->setEchoMode(QLineEdit::PasswordEchoOnEdit);

        gridLayout->addWidget(lineEditPassword, 4, 1, 1, 1);

        labelDatabaseName = new QLabel(horizontalLayoutWidget_3);
        labelDatabaseName->setObjectName(QString::fromUtf8("labelDatabaseName"));

        gridLayout->addWidget(labelDatabaseName, 0, 0, 1, 1);


        horizontalLayout_3->addLayout(gridLayout);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        pushButtonTestOracle = new QPushButton(horizontalLayoutWidget_3);
        pushButtonTestOracle->setObjectName(QString::fromUtf8("pushButtonTestOracle"));

        verticalLayout_3->addWidget(pushButtonTestOracle);


        horizontalLayout_3->addLayout(verticalLayout_3);

        tabWidget->addTab(tabOracle, QString());
        QWidget::setTabOrder(listWidgetColor, pushButtonColor);
        QWidget::setTabOrder(pushButtonColor, lineEditDatabaseName);
        QWidget::setTabOrder(lineEditDatabaseName, lineEditHostname);
        QWidget::setTabOrder(lineEditHostname, lineEditPort);
        QWidget::setTabOrder(lineEditPort, lineEditUsername);
        QWidget::setTabOrder(lineEditUsername, lineEditPassword);

        retranslateUi(OptionGeneral);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(OptionGeneral);
    } // setupUi

    void retranslateUi(QWidget *OptionGeneral)
    {
        OptionGeneral->setWindowTitle(QCoreApplication::translate("OptionGeneral", "General", nullptr));
        pushButtonColor->setText(QCoreApplication::translate("OptionGeneral", "Change...", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabColor), QCoreApplication::translate("OptionGeneral", "Color", nullptr));
        labelHostname->setText(QCoreApplication::translate("OptionGeneral", "Hostname:", nullptr));
        labelPort->setText(QCoreApplication::translate("OptionGeneral", "Port:", nullptr));
        labelUsername->setText(QCoreApplication::translate("OptionGeneral", "Username:", nullptr));
        labelPassword->setText(QCoreApplication::translate("OptionGeneral", "Password:", nullptr));
        labelDatabaseName->setText(QCoreApplication::translate("OptionGeneral", "Database name:", nullptr));
        pushButtonTestOracle->setText(QCoreApplication::translate("OptionGeneral", "Test\n"
"connection", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabOracle), QCoreApplication::translate("OptionGeneral", "Oracle", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OptionGeneral: public Ui_OptionGeneral {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTIONGENERAL_H
