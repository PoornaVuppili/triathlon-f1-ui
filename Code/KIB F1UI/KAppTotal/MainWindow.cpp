#include <QtGui>
#include "MainWindow.h"
#include "OpenGLView.h"
#include "DataDoc.h"
#include "EntPanel.h"
#include "Globals.h"
#include "TotalDocDlg.h"
#include "FileNewDlg.h"
#include "InfoTextReader.h"
#include "Utilities.h"

#include "..\KApp\MainWindow.h"
#include "..\KApp\EntityWidget.h"
#include "..\KApp\ViewWidget.h"
#include "..\KApp\Viewport.h"
#include "..\KApp\Implant.h"
#include "..\KUtility\KUtility.h"

#include <qmessagebox.h>

CMainWindow::CMainWindow()
{
	m_bModified = false;
}

CMainWindow::~CMainWindow()
{
    DeletePtr(m_pView);
    DeletePtr(m_pDoc);
    DeletePtr(m_pEntPanel);
}

void CMainWindow::OnFileSaveAutoOn(bool checked)
{
	// change icon image
	//if ( checked )
	//	m_actFileSave->setIcon( QIcon( ":/Resources/FileSaveAuto.png" ) );
	//else
	//	m_actFileSave->setIcon( QIcon( ":/Resources/FileSave.png" ) );

	//// set the flag
	//m_pDoc->SetFileSaveAuto( checked );
}

void CMainWindow::ResetViewDefaultSettings()
{
//	m_pView->OnBackGroundColorDefault();
	m_pView->OnTessellationFine();
	m_actTessellationFine->setChecked(true);
	m_pView->OnTransparencyNormal();
	m_actTransparencyNormal->setChecked(true);
	m_pView->OnArrowKeyRotation();
	//m_actArrowKeyRotation->setChecked(true);
}

void CMainWindow::ImportCADEntity(CEntRole entRole, IwAxis2Placement* additionalTransformation)
{
	m_pDoc->AppendLog( QString("CMainWindow::ImportCADEntity()") );

	QString					sDocName;
	QString					sDir = GetStringPref( "Last used part folder" );
	bool					bOK;

	QFileDialog				dlg( mainWindow );
	dlg.setDirectory( sDir );
	//dlg.setNameFilters(QStringList() << "*.iges" << "*.igs" << "*.IGES" << "*.IGS");
	//dlg.setFilter( "IGES Files (*.iges *.igs *.IGES *.IGS)" ); 
	dlg.show();
	if( dlg.exec() )
	{
		sDocName = dlg.selectedFiles()[0];

		if( !(sDocName.endsWith( ".iges", Qt::CaseInsensitive ) || sDocName.endsWith( ".igs", Qt::CaseInsensitive )) )
		{
			int		button;
			button = QMessageBox::warning( mainWindow, 
											tr("Warning Message!"), 
											tr("Only IGES files can be imported.\n"), 
											QMessageBox::Ok);	

			if( button == QMessageBox::Ok ) return;
		}

		bOK = m_pDoc->ImportPart( sDocName, entRole );

		if( !bOK )
			return;

		m_pView->Redraw();

	}

	// Apply additional transformation
	if ( additionalTransformation )
	{
		IwAxis2Placement transM;
		if ( additionalTransformation )
			transM = *additionalTransformation;
		CPart* pPart = (CPart*)m_pDoc->GetLastEntity();
		// Brep
		IwBrep* pBrep = pPart->GetIwBrep();
		if ( pBrep )
		{
			pBrep->Transform(transM);
			pPart->MakeTess();
		}
		// Curves
		IwTArray<IwCurve*> curves = pPart->GetIwCurves();
		for (unsigned i=0; i<curves.GetSize(); i++)
			curves.GetAt(i)->Transform(transM);
		pPart->SetIwCurves(curves);
		// Points
		IwTArray<IwPoint3d> points = pPart->GetIwPoints();
		IwPoint3d pnt;
		for (unsigned i=0; i<points.GetSize(); i++)
		{
			transM.TransformPoint(points.GetAt(i), pnt);
			points.SetAt(i, pnt);
		}
		pPart->SetIwPoints(points);
	}

    // Add entity panel to the entity widget; and view to the viewport, if not yet
	if ( !entityWidget->IsItemExist(m_pEntPanel) )
	{
		entityWidget->AddItem( m_pEntPanel );
	}
	if ( !viewWidget->GetViewport()->IsViewExist(m_pView) )
	{
		viewWidget->GetViewport()->AddView( m_pView );
	}

	// Update bounds
	m_pView->UpdateShowRegion();

}

void CMainWindow::OnCleanUpTempDisplay()
{
	m_pDoc->CleanUpTempPoints();
}

bool CMainWindow::HasImplant()
{
    return !m_pDoc->IsEmpty();
}

bool CMainWindow::IsImplantModified()
{
    return m_pDoc->IsModified();
}
