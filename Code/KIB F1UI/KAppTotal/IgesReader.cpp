#include <windows.h>
#include "DataDoc.h"
#include "IgesReader.h"
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwTess.h>
#include <HwTSLibIges.h>
#pragma warning( default : 4996 4805 )
#include <qnamespace.h>
#include "Part.h"
#include "Tess.h"
#include "IgesTracker.h"
#include "ProgressWidget.h"
#include "Globals.h"
#include "Utilities.h"
#include <HwTranslatorGeneric.h>

CIgesReader::CIgesReader( CPart* pPart, const QString& sFileName )
	{
	QString		sPartName;

	m_pPart = pPart;
	m_sFileName = sFileName;

	pPart->SetPartType( PART_TYPE_BREP );
	}


CIgesReader::~CIgesReader()
	{
	}


bool CIgesReader::LoadFile()
	{
	QByteArray			byteArray = m_sFileName.toLatin1();
	char*				filename = byteArray.data();
	QWidget*			pParentWidget;
	QString				sProgressText;
	bool				bOK;

	pParentWidget = (QWidget*) m_pPart->GetDoc()->GetView();

	sProgressText = "Loading " + GetFileNameWithExtension( m_sFileName );
	
	bOK = ReadIgesFile( filename );

	if( !bOK )
		{
 		return false;
		}
	
	m_pPart->MakeTess();

	return true;
	}


bool CIgesReader::ReadIgesFile( const char* filename )
{
	IwContext&			iwContext = m_pPart->GetDoc()->GetIwContext();
    IwTArray<IwBrep*>	IwBreps;
	CIgesTracker*		pTracker = nullptr;
	CCurveArray			curves;
	IwTArray<IwPoint3d>	points3d;
   HwImportOptions Options;
   Options.make_all_surfaces_trimmed = true;
   Options.make_single_brep = TRUE;
   Options.group_trimmed_surfaces_into_brep = TRUE;
   Options.store_transforms_as_attributes = TRUE;
   //HwNullTracker sTracker;
   HwNullLogger logger;
   //HwUnitsAndScale2 sUnits;
   HwHeaderInfo sHeader;
   double dIgesTol = 0.0;
   IwStatus iwStatus;
#if HW_VERSION_NUMBER >= 1917
	// SMLib 8.8.0
   	IwTArray<HwWrapObjectWithIwAttributes<IwPoint3d>*> points;
    iwStatus = HwTSLibIgesRead( iwContext, filename, Options, logger, sTracker, sHeader, dIgesTol, sUnits, &points, nullptr, &curves, nullptr, &IwBreps, nullptr ); 
	if ( points.GetSize() > 0 )
	{
		for (unsigned int i=0; i<points.GetSize(); i++)
			points3d.Add(IwPoint3d(points[i]->value));
	}
#else if HW_VERSION_NUMBER > 1900
   // SMLib 8.6.17
   Options.path_to_parasolid_schemas = FALSE; 

   sHeader.SetGlobalTolerance(1e-4);
   sHeader.SetGlobalUnits(HW_U_MILLIMETERS, 1.0);
   iwStatus = HwTSLibIgesRead( iwContext, filename, logger, Options,/* sTracker,*/ sHeader/*, dIgesTol, sUnits*/, &points3d/*, nullptr*/, &curves, nullptr, &IwBreps, nullptr );
#endif

	delete pTracker;

	if( iwStatus != IW_SUCCESS )
		return false;

	if ( points3d.GetSize() > 0 )
	{
		m_pPart->SetIwPoints(points3d);
	}

	if ( curves.GetSize() > 0 )
	{
		m_pPart->SetIwCurves(curves);
	}

	if ( IwBreps.GetSize() > 0 )
	{
		if ( IwBreps.GetSize() == 1 )// single Brep
		{
			m_pBrep = IwBreps[0];
		}
		else if (IwBreps.GetSize() > 1 ) // multiple Breps, just merge Breps
		{
			m_pBrep = IwBreps[0];
			for (unsigned i=1; i<IwBreps.GetSize(); i++)
			{
				m_pBrep->MergeBrep(*IwBreps[i]);
			}
		}
		if (0)
		{
			m_pBrep->ShrinkGeometry();
			m_pBrep->SewAndOrient();
			IwBoolean isSolid = m_pBrep->IsManifoldSolid();
		}
		m_pPart->SetIwBrep( m_pBrep );
	}
	

	return true;
}




void CIgesReader::SewFaces()
	{
	IwContext&			iwContext = m_pPart->GetDoc()->GetIwContext();
    ULONG				lNumSewn, lNumLamina;
    double				dMaxVGap, dMaxEGap;
 
    m_pBrep->SetTolerance( 0.0001 );

    m_pBrep->m_bEditingEnabled = true;

    SE( m_pBrep->SewFaces( 0.001, lNumSewn, lNumLamina, dMaxVGap, dMaxEGap ) );
    
	m_pBrep->m_bEditingEnabled = false;

    // Note that 0.001 is not large enough to sew all of the faces and that 
	// there will still be lamina edges - so we sew again with a larger tolerance.
    if( lNumLamina > 0 ) 
		{
        m_pBrep->m_bEditingEnabled = true;

        SE( m_pBrep->SewFaces( 0.005, lNumSewn, lNumLamina, dMaxVGap, dMaxEGap ) );
        
		m_pBrep->m_bEditingEnabled = false;
		}
	}



bool CIgesReader::LoadFemurAndTabcyl( IwBrep*& pTabcylBrep )
	{
	QByteArray			byteArray = m_sFileName.toLatin1();
	char*				filename = byteArray.data();
	QWidget*			pParentWidget;
	QString				sProgressText;
	bool				bOK;

	pParentWidget = (QWidget*) m_pPart->GetDoc()->GetView();

	sProgressText = "Loading " + GetFileNameWithExtension( m_sFileName );
	
	bOK = ReadFemurAndTabcyl( filename, pTabcylBrep );

	if( !bOK )
		{
 		return false;
		}
	
	SewFaces();


	m_pPart->MakeTess();

	return true;
	}


bool CIgesReader::ReadFemurAndTabcyl( const char* filename, IwBrep*& pTabcylBrep )
	{
	IwContext&			iwContext = m_pPart->GetDoc()->GetIwContext();
    IwTArray<IwBrep*>	IwBreps;
	CIgesTracker*		pTracker = nullptr;
	CFaceArray			AnalyticFaces, TabcylFaces, ProfileFaces;
	CEdgeArray			Edges;
	int					i, j, nFaces, nEdges, nLaminaEdges;//, nTabcylFaces, nProfileFaces;

	// Read a Brep which consists of trimmed surfaces from the IGES file.
#if HW_VERSION_NUMBER > 1900
   HwImportOptions Options;
   HwNullTracker sTracker;
   HwNullLogger logger;
   HwUnitsAndScale2 sUnits;
   HwHeaderInfo sHeader;
   double dIgesTol = 0.0;
   IwStatus iwStatus = HwTSLibIgesRead( iwContext, filename, Options, logger, sTracker, sHeader, dIgesTol, sUnits, nullptr, nullptr, nullptr, nullptr, &IwBreps, nullptr );
#else
	IwStatus iwStatus = hwiges_ReadIges( iwContext, filename, NULL, false, NULL, false, &IwBreps, true, true, pTracker );
#endif

	delete pTracker;

	if( iwStatus != IW_SUCCESS )
		return false;

	m_pBrep = IwBreps[0];

	m_pBrep->m_bEditingEnabled = true;
	pTabcylBrep->m_bEditingEnabled = true;

	CFaceArray		Faces;

	m_pBrep->GetFaces( Faces );

	nFaces = Faces.GetSize();

	for( i = 0; i < nFaces; i++ )
		{
		IwFace*		pFace = Faces[i];

		IwSurface*	pSurf = pFace->GetSurface();

		bool		bAnalytic = pSurf->IsAnalytic();

		if( bAnalytic )
			{
			bool	bPlanar = pSurf->IsKindOf( IwPlane_TYPE );
			bool	bExtruded = pSurf->IsKindOf( IwSurfOfExtrusion_TYPE );
			int		a = 1;

			AnalyticFaces.Add( pFace );
			}
		}

	m_pBrep->CopyFaces( AnalyticFaces, pTabcylBrep );

	m_pBrep->RemoveFaces( AnalyticFaces );

	SewFaces();

	m_pBrep->m_bEditingEnabled = true;

	AnalyticFaces.ReSet();

	m_pBrep->GetFaces( Faces );

	nFaces = Faces.GetSize();

	for( i = 0; i < nFaces; i++ )
		{
		IwFace*		pFace = Faces[i];

		pFace->GetEdges( Edges );

		nEdges = Edges.GetSize();
		nLaminaEdges = 0;

		for( j = 0; j < nEdges; j++ )
			{
			if( Edges[j]->IsLamina() )
				nLaminaEdges++;
			}

		if( nLaminaEdges == nEdges )
			AnalyticFaces.Add( pFace );
		}

	m_pBrep->CopyFaces( AnalyticFaces, pTabcylBrep );

	m_pBrep->RemoveFaces( AnalyticFaces );

	m_pBrep->m_bEditingEnabled = false;
	pTabcylBrep->m_bEditingEnabled = false;

	pTabcylBrep->SewAndOrient();

	m_pPart->SetIwBrep( m_pBrep );

	return true;
	}



