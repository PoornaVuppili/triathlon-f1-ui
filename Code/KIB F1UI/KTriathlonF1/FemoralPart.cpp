#include <QtGui>
#include <QtOpenGL>
#include <gl\gl.h>
#include "FemoralPart.h"
#include "FemoralAxes.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Utilities.h"
#include <IwTolerance.h>

#pragma warning( disable : 4996 4805 )
#include <IwTess.h>
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )

////////////////////////////////////////////////////////////////////
// This class merges femur surface, determines view profiles,
// and solidify femur Brep. When solidifing, the original surfaces 
// is from Part.h m_pBrep, then merge the patches as possible, and 
// solidify them. Set the solidified Brep back to m_pBrep.
////////////////////////////////////////////////////////////////////


CFemoralPart::CFemoralPart( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
		CPart( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_FEMORAL_PART );
	
	m_pMainFemurSurface = NULL;
	m_pLeftSideSurface= NULL;
	m_pRightSideSurface = NULL;

	m_glFemoralPartCurveList = 0;

	m_viewProfileVector = IwVector3d(0,0,0);
	m_bViewProfileDisplay = false;
	m_bFPModified = false;
	m_bFPDataModified = false;
}


CFemoralPart::~CFemoralPart()
{
	
}

bool CFemoralPart::Save( const QString& sDir, bool incrementalSave )
{

	CPart::Save(sDir, incrementalSave);

	QString				sFileName, sBrepFileName;
	CSurfArray			surfaces;
	bool bOK=true;

	// if it's incremental save (general Save) && data is not modified -> just return.
	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bFPDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_fp.surf~";

	surfaces.Add(m_pMainFemurSurface);
	surfaces.Add(m_pLeftSideSurface);
	surfaces.Add(m_pRightSideSurface);

	bOK = WriteSurfaces( surfaces, sFileName );

	if( !bOK )
		return false;

	sFileName = sDir + "\\" + m_sFileName + "_fp.crv~";

	bOK = WriteCurves( m_viewProfileCurves, sFileName );

	if( !bOK )
		return false;

	sFileName = sDir + "\\" + m_sFileName + "_fp.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	File.write( (char*) &m_viewProfileVector, sizeof( IwVector3d ) );

	File.close();

	m_bFPDataModified = false;

	return true;
}


bool CFemoralPart::Load( const QString& sDir )
{
	CPart::Load(sDir);

	QString				sFileName, sBrepFileName;
	bool				bOK;
	CSurfArray			surfaces;
	CCurveArray			sCurves;

	// Variables for TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{
		sFileName = sDir + "\\" + m_sFileName + "_fp.surf";

		bOK = ReadSurfaces( sFileName, surfaces );

		if( !bOK )
			return false;

		m_pMainFemurSurface = (IwBSplineSurface*)surfaces.GetAt(0);
		m_pLeftSideSurface = (IwBSplineSurface*)surfaces.GetAt(1);
		m_pRightSideSurface = (IwBSplineSurface*)surfaces.GetAt(2);

		sFileName = sDir + "\\" + m_sFileName + "_fp.crv";

		bOK = ReadCurves( sFileName, sCurves );

		if( !bOK )
			return false;

		m_viewProfileCurves.Append(sCurves);
	}

	sFileName = sDir + "\\" + m_sFileName + "_fp.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	// Variables for TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{
		IwVector3d viewVector;
		File.read( (char*) &viewVector, sizeof( IwVector3d ) );
		m_viewProfileVector = viewVector;
	}
	File.close();

	// Actions for TriathlonF1 version 4.0.0
	// Files were earlier than 4.0.1
	if ( !m_pDoc->IsOpeningFileRightVersion(4,0,1) )
	{
		// the m_pMainFemurSurface has to be ReparametrizeWithArcLength(),
		// otherwise, the trochlear curve distance to the femur surface can not be determined right
		// due to the new SMLib v8.3.0. 
		m_pMainFemurSurface->ReparametrizeWithArcLength();
		m_pLeftSideSurface->ReparametrizeWithArcLength();
		m_pRightSideSurface->ReparametrizeWithArcLength();
		// set the flag true to re-save the m_pMainFemurSurface
		SetFPModifiedFlag( true );
	}
	else
	{
		m_bFPDataModified = false;
	}

	return true;
}

void CFemoralPart::Display()
{
	
	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	glDisable( GL_LIGHTING );

	// Display view profile curves
	if ( m_bViewProfileDisplay )
	{
		if( m_bFPModified && m_glFemoralPartCurveList > 0 )
			DeleteDisplayList( m_glFemoralPartCurveList );

		if( m_glFemoralPartCurveList == 0 )
		{
			m_glFemoralPartCurveList = NewDisplayList();
			glNewList( m_glFemoralPartCurveList, GL_COMPILE_AND_EXECUTE );
			DisplayFemoralPartViewProfiles();
			glEndList();
			SetFPModifiedFlag(false);
		}
		else
		{
			glCallList( m_glFemoralPartCurveList );
		}
	}

	glEnable( GL_LIGHTING );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	// Call CPart::Display();
	CPart::Display();

}


void CFemoralPart::DisplayFemoralPartViewProfiles()
{
	if ( !m_bViewProfileDisplay ) return;
			
	IwPoint3d	pt;
	CColor		color( 79, 36, 36 );

	IwPoint3d sPData[64];
	
	IwTArray<IwPoint3d> sPnts( 64, sPData );


	//// DRAW VIEW PROFILES ///////////////////////////////////////////////////////
	// We do not need the blend (Transparency) for view profiles
	IwVector3d viewVector;
	IwTArray<IwCurve*> viewProfileCurves;
	GetViewProfilesInfo(viewVector, viewProfileCurves);

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	glLineWidth(3.0);

	glColor4ub( color[0], color[1], color[2], m_pDoc->GetTransparencyFactor());

	for( ULONG i = 0; i < viewProfileCurves.GetSize(); i++ ) 
	{
		
		IwCurve *pCurve = viewProfileCurves.GetAt(i);

		IwExtent1d sIvl = pCurve->GetNaturalInterval();

		double	dChordHeightTol = 0.3;
		double	dAngleTolInDegrees = 5.0;

		IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees, 0, 0.001 );

		sCrvTess.TessellateCurve( *pCurve, sIvl, NULL, &sPnts );

		int nPoints = sPnts.GetSize();

		glBegin( GL_LINE_STRIP );

		for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
		{
			pt = sPnts.GetAt(k);

			glVertex3d( pt.x, pt.y, pt.z );
		}

		glEnd();
	}

	glLineWidth(1.0);

}

void CFemoralPart::SetDisplayViewProfiles(bool flag)
{
	m_bViewProfileDisplay = flag;
	// delete the display list to force update the display
	DeleteDisplayList( m_glFemoralPartCurveList );
}

////////////////////////////////////////////////////////////////////
// This function takes Brep with small patches (patches in template) 
// as input. Merge the small patches into 5 big patches 
// (3 in the main area, and 2 isdes), and cover the top to solidify 
// the femur surface.
////////////////////////////////////////////////////////////////////
bool CFemoralPart::MakeSolidifyIwBrep()
{
	IwBrep* mergedBrep = NULL, *solifyBrep = NULL;
	IwBrep* pBrep = GetIwBrep();
	
	if ( pBrep->IsManifoldSolid() ) // had been solidified.
		return true;
	MergeFemurSurfaces(pBrep, mergedBrep);
	bool bSolidified = SolidifyBrepByCutPlane(mergedBrep, solifyBrep);
	if ( !bSolidified )
		bSolidified = SolidifyBrepBySealLaminaEdges(mergedBrep, solifyBrep);

	if ( bSolidified )
	{
		m_pBrep = solifyBrep;
		return true;
	}
	else
	{
		return false;
	}
}

IwBrep*	CFemoralPart::GetSolidifyIwBrep()
{
	// this function will solidify the femur, if not yet.
	MakeSolidifyIwBrep();
	return m_pBrep;
}

///////////////////////////////////////////////////////////////
// This function uses cutter to cover the femur top. 
///////////////////////////////////////////////////////////////
bool CFemoralPart::SolidifyBrepByCutPlane
( 
	IwBrep* openBrep,		// I:
	IwBrep*& closedBrep		// O:
)
{
	if (openBrep == NULL) return false;

	IwContext& iwContext = m_pDoc->GetIwContext();
	IwBrep* copiedBrep = new (iwContext) IwBrep(*openBrep);
	copiedBrep->SewAndOrient();

	IwTArray<IwEdge*> edges;
	copiedBrep->GetEdges(edges);
	IwEdge *edge;
	IwTArray<IwEdge*> laminaEdges;
	IwBSplineCurve *curve;
	IwExtent1d dom;
	IwTArray<IwPoint3d> pnts, edgePoints;
	double length;
	// Get 4 open boundary curves from openBrep
	for (unsigned i=0; i< edges.GetSize(); i++)
	{
		edge = edges.GetAt(i);
		if ( edge->IsLamina() ) // the openBrep should only have 4 lamina edges.
		{
			laminaEdges.Add(edge);
			curve = (IwBSplineCurve*)edge->GetCurve();
			dom = curve->GetNaturalInterval();
			curve->Length(dom, 0.01, length);
			pnts.RemoveAll();
			curve->EquallySpacedPoints(dom.GetMin(), dom.GetMax(), (int)length, 0.1, &pnts, NULL);
			edgePoints.Append(pnts);
		}
	}
	//
	// create a plane approximate these 4 lamina edges.
	IwBSplineSurface* plane=NULL, *extPlane=NULL;
	IwBSplineSurface::ApproximateRandomPoints(m_pDoc->GetIwContext(), edgePoints, 2, 2, 1, 1, NULL, 1, plane);
	// Extend
	plane->CreateExtendedSurface(m_pDoc->GetIwContext(), 10.0, IW_CT_C1 , extPlane);
if (0)
	ShowSurface(m_pDoc, extPlane, "extPlane", blue);

	// move the extPlane a bit down
	IwExtent2d domSurf = plane->GetNaturalUVDomain();
	IwVector3d normalUp;
	plane->EvaluateNormal(domSurf.Evaluate(0.5, 0.5), FALSE, FALSE, normalUp);
	IwVector3d upVector(0,0,1);// global z up vector
	if (normalUp.Dot(upVector) < 0 )
		normalUp = -normalUp;
	IwPoint3d farDownPoint = edgePoints.GetAt(0) - 100000*normalUp;
	IwPoint3d downPnt, cPnt;
	DistFromPointToEdges(farDownPoint, laminaEdges, downPnt);
	IwPoint2d paramUV;
	double distToMove = DistFromPointToSurface(downPnt, plane, cPnt, paramUV) + 0.5; // cut 0.5 more
	IwAxis2Placement transM = IwAxis2Placement(-distToMove*normalUp, IwVector3d(1,0,0), IwVector3d(0,1,0));
	extPlane->Transform(transM);
	// 
	IwBrep*	planeBrep = new ( m_pDoc->GetIwContext() ) IwBrep;
	IwFace *face;
	planeBrep->CreateFaceFromSurface(extPlane, extPlane->GetNaturalUVDomain(), face);

	// Create solid
	IwMerge merge(m_pDoc->GetIwContext(), copiedBrep, planeBrep, 0.001, 0.01 );
	IwBrep* rBrep=NULL;
	merge.NonManifoldBoolean(IW_BO_MERGE, rBrep);
	IwStatus error = rBrep->MakeManifold();
	rBrep->SewAndOrient();

	closedBrep = rBrep;
if (0)
ShowBrep(m_pDoc, closedBrep, "closedBrep", blue);

	IwBoolean isSolid = closedBrep->IsManifoldSolid();

	if ( !isSolid )
		return false;
	else
		return true;
}

///////////////////////////////////////////////////////////////
// This function creates 4 Coons patch to cover the femur top. 
///////////////////////////////////////////////////////////////
bool CFemoralPart::SolidifyBrepBySealLaminaEdges
( 
	IwBrep* openBrep,		// I:
	IwBrep*& closedBrep		// O:
)
{
	if (openBrep == NULL) return false;

	IwContext& iwContext = m_pDoc->GetIwContext();
	closedBrep = new (iwContext) IwBrep(*openBrep);

	openBrep->SewAndOrient();

	IwTArray<IwEdge*> edges;
	openBrep->GetEdges(edges);
	IwEdge *edge;
	IwEdge *edgeShortPos=NULL, *edgeShortNeg=NULL, *edgeLongPos=NULL, *edgeLongNeg=NULL;
	IwCurve *curve, *curveShortPos=NULL, *curveShortNeg=NULL, *curveLongPos=NULL, *curveLongNeg=NULL;
	double length;
	// Get 4 open boundary curves from openBrep
	for (unsigned i=0; i< edges.GetSize(); i++)
	{
		edge = edges.GetAt(i);
		if ( edge->IsLamina() ) // the openBrep should only have 4 lamina edges.
		{
			curve = edge->GetCurve();
			curve->Length(curve->GetNaturalInterval(), 0.01, length);
			if ( length < 25.0 )
			{
				if ( edgeShortPos == NULL )
				{
					edgeShortPos = edge;
					curve->Copy(iwContext, curveShortPos);
				}
				else if ( edgeShortNeg == NULL )
				{
					edgeShortNeg = edge;
					curve->Copy(iwContext, curveShortNeg);
				}
			}
			else
			{
				if ( edgeLongPos == NULL )
				{
					edgeLongPos = edge;
					curve->Copy(iwContext, curveLongPos);
				}
				else if ( edgeLongNeg == NULL )
				{
					edgeLongNeg = edge;
					curve->Copy(iwContext, curveLongNeg);
				}
			}
		}
	}
	// ensure edgeShortPos & edgeShortNeg are on positive and negative sides
	IwPoint3d midPntPos, midPntNeg, midPnt;
	IwExtent1d crvDom;
	crvDom = edgeShortPos->GetInterval();
	edgeShortPos->GetCurve()->EvaluatePoint(crvDom.Evaluate(0.5), midPntPos);
	crvDom = edgeShortNeg->GetInterval();
	edgeShortNeg->GetCurve()->EvaluatePoint(crvDom.Evaluate(0.5), midPntNeg);
	IwVector3d tempVec;
	tempVec = midPntPos - midPntNeg;
	if ( tempVec.Dot(IwVector3d(1,0,0)) < 0 )
	{
		edge = edgeShortPos;
		edgeShortPos = edgeShortNeg;
		edgeShortNeg = edge;
		curve = curveShortPos;
		curveShortPos = curveShortNeg;
		curveShortNeg = curve;
		midPnt = midPntPos;
		midPntPos = midPntNeg;
		midPntNeg = midPnt;
	}
	// Ensure edgeLongPos & edgeLongNeg point toward the same direction
	IwPoint3d sPntShortPos, ePntShortPos;
	curveShortPos->GetEnds(sPntShortPos, ePntShortPos);
	IwPoint3d sPnt, ePnt;
	curveLongPos->GetEnds(sPnt, ePnt);
	// Ensure the start point of curveLongPos always connects to the curveShortPos
	if ( sPnt.DistanceBetween(sPntShortPos) > 5.0 &&
		 sPnt.DistanceBetween(ePntShortPos) > 5.0 )
	{
		// flip curve
		curveLongPos->ReverseParameterization(curveLongPos->GetNaturalInterval(), crvDom);
	}
	curveLongNeg->GetEnds(sPnt, ePnt);
	// Ensure the start point of curveLongNeg always connects to the curveShortPos
	if ( sPnt.DistanceBetween(sPntShortPos) > 5.0 &&
		 sPnt.DistanceBetween(ePntShortPos) > 5.0 )
	{
		// flip curve
		curveLongNeg->ReverseParameterization(curveLongNeg->GetNaturalInterval(), crvDom);
	}

	// Find the middle points of both curveLongPos & curveLongNeg
	IwPoint3d midPntLongPos, midPntLongNeg;
	curveLongPos->EvaluatePoint(curveLongPos->GetNaturalInterval().Evaluate(0.5), midPntLongPos);
	curveLongNeg->EvaluatePoint(curveLongNeg->GetNaturalInterval().Evaluate(0.5), midPntLongNeg);
	midPnt = 0.5*midPntLongPos + 0.5*midPntLongNeg;

	// Use midPntPos, midPnt, & midPntNeg to create a curve
	IwBSplineCurve* centralCurve;
	IwTArray<IwPoint3d> intPnts;
	intPnts.Add(midPntPos);
	intPnts.Add(midPnt);
	intPnts.Add(midPntNeg);
	IwBSplineCurve::InterpolatePoints(iwContext, intPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, centralCurve);
	// trim the curve a little bit
	centralCurve->ReparametrizeWithArcLength();
	crvDom = centralCurve->GetNaturalInterval();
	crvDom.ExpandAbsolute(-5.0);
	centralCurve->Trim(crvDom);
	// make a new curve
	// create a curve in 3rd dregee
	intPnts.RemoveAll();
	centralCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), 6, 0.001, &intPnts, NULL);
	IwBSplineCurve::InterpolatePoints(iwContext, intPnts, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, centralCurve);
	// make another copy of centralCurve
	IwCurve* crvCopy;
	centralCurve->Copy(iwContext, crvCopy);
	IwBSplineCurve* centralCurve2 = (IwBSplineCurve*)crvCopy;

	// Construct the cover patches
	// create pos side degenerate coons patch (3 sides)
	{
		IwPoint3d centralStartPnt;
		centralCurve->EvaluatePoint(centralCurve->GetNaturalInterval().Evaluate(0),centralStartPnt);
		IwPoint3d sPntPos, ePntPos;
		curveShortPos->GetEnds(sPntPos, ePntPos);
		// create 2 side lines
		IwBSplineCurve *leftCurve, *rightCurve;
		IwBSplineCurve::CreateLineSegment( iwContext, 3, sPntPos, centralStartPnt, leftCurve);
		IwBSplineCurve::CreateLineSegment( iwContext, 3, ePntPos, centralStartPnt, rightCurve);
		// Create point curve (degenerate curve)
		IwBSplineCurve* dCurve;
		IwBSplineCurve::CreateDegenerateCurve(iwContext, 3, centralStartPnt, dCurve);
		// Create coons patch
		IwTArray<IwBSplineCurve*> bndCurves, derCurves;
		bndCurves.Add(leftCurve);
		bndCurves.Add(rightCurve);
		bndCurves.Add((IwBSplineCurve*)curveShortPos);
		bndCurves.Add(dCurve);
		IwBSplineSurface* coverSurf = NULL;
		IwBSplineSurface::CreateCoonsPatch(iwContext, 0.0001, true, bndCurves, derCurves, coverSurf);
		if (coverSurf)
		{
			IwFace* pFace;
			closedBrep->CreateFaceFromSurface(coverSurf, coverSurf->GetNaturalUVDomain(), pFace);
		}
	}
	// create neg side degenerate coons patch (3 sides)
	{
		IwPoint3d centralEndPnt;
		centralCurve->EvaluatePoint(centralCurve->GetNaturalInterval().Evaluate(1.0),centralEndPnt);
		IwPoint3d sPntNeg, ePntNeg;
		curveShortNeg->GetEnds(sPntNeg, ePntNeg);
		// create 2 side lines
		IwBSplineCurve *leftCurve, *rightCurve;
		IwBSplineCurve::CreateLineSegment( iwContext, 3, sPntNeg, centralEndPnt, leftCurve);
		IwBSplineCurve::CreateLineSegment( iwContext, 3, ePntNeg, centralEndPnt, rightCurve);
		// Create point curve (degenerate curve)
		IwBSplineCurve* dCurve;
		IwBSplineCurve::CreateDegenerateCurve(iwContext, 3, centralEndPnt, dCurve);
		// Create coons patch
		IwTArray<IwBSplineCurve*> bndCurves, derCurves;
		bndCurves.Add(leftCurve);
		bndCurves.Add(rightCurve);
		bndCurves.Add((IwBSplineCurve*)curveShortNeg);
		bndCurves.Add(dCurve);
		IwBSplineSurface* coverSurf = NULL;
		IwBSplineSurface::CreateCoonsPatch(iwContext, 0.0001, true, bndCurves, derCurves, coverSurf);
		if (coverSurf)
		{
			IwFace* pFace;
			closedBrep->CreateFaceFromSurface(coverSurf, coverSurf->GetNaturalUVDomain(), pFace);
		}
	}
	// create top side coons patch (4 sides)
	{
		IwPoint3d centralStartPnt, centralEndPnt;
		centralCurve->GetEnds(centralStartPnt, centralEndPnt);
		IwPoint3d sPntPos, ePntPos;
		curveLongPos->GetEnds(sPntPos, ePntPos);
		// create 2 side lines
		IwBSplineCurve *leftCurve, *rightCurve;
		IwBSplineCurve::CreateLineSegment( iwContext, 3, centralStartPnt, sPntPos, leftCurve);
		IwBSplineCurve::CreateLineSegment( iwContext, 3, centralEndPnt, ePntPos, rightCurve);
		// Create coons patch
		IwTArray<IwBSplineCurve*> bndCurves, derCurves;
		bndCurves.Add(leftCurve);
		bndCurves.Add(rightCurve);
		bndCurves.Add(centralCurve);
		bndCurves.Add((IwBSplineCurve*)curveLongPos);
		IwBSplineSurface* coverSurf = NULL;
		IwBSplineSurface::CreateCoonsPatch(iwContext, 0.0001, true, bndCurves, derCurves, coverSurf);
		if (coverSurf)
		{
			IwFace* pFace;
			closedBrep->CreateFaceFromSurface(coverSurf, coverSurf->GetNaturalUVDomain(), pFace);
		}
	}
	// create bottom side coons patch (4 sides)
	{
		IwPoint3d centralStartPnt, centralEndPnt;
		centralCurve2->GetEnds(centralStartPnt, centralEndPnt);
		IwPoint3d sPntPos, ePntPos;
		curveLongNeg->GetEnds(sPntPos, ePntPos);
		// create 2 side lines
		IwBSplineCurve *leftCurve, *rightCurve;
		IwBSplineCurve::CreateLineSegment( iwContext, 3, centralStartPnt, sPntPos, leftCurve);
		IwBSplineCurve::CreateLineSegment( iwContext, 3, centralEndPnt, ePntPos, rightCurve);
		// Create coons patch
		IwTArray<IwBSplineCurve*> bndCurves, derCurves;
		bndCurves.Add(leftCurve);
		bndCurves.Add(rightCurve);
		bndCurves.Add(centralCurve2);
		bndCurves.Add((IwBSplineCurve*)curveLongNeg);
		IwBSplineSurface* coverSurf = NULL;
		IwBSplineSurface::CreateCoonsPatch(iwContext, 0.0001, true, bndCurves, derCurves, coverSurf);
		if (coverSurf)
		{
			IwFace* pFace;
			closedBrep->CreateFaceFromSurface(coverSurf, coverSurf->GetNaturalUVDomain(), pFace);
		}
	}
	
	closedBrep->SewAndOrient();
if (0)
ShowBrep(m_pDoc, closedBrep, "closedBrep", blue);

	closedBrep->MakeManifold();

	IwBoolean isSolid = closedBrep->IsManifoldSolid();

	if ( !isSolid )
		return false;
	else
		return true;
}

void CFemoralPart::GetSideSurfaces(IwBSplineSurface* &leftSideSurface, IwBSplineSurface* &rightSideSurface)
{
	leftSideSurface = m_pLeftSideSurface;
	rightSideSurface = m_pRightSideSurface;

	return;
}

///////////////////////////////////////////////////////////////////////////
// This function orders the fumur faces. 
///////////////////////////////////////////////////////////////////////////
bool CFemoralPart::OrderFaces
(
	IwBrep* pBrep,							// I:
	IwTArray<int> &dimension,				// O: 1st element-> main rowLength, 2nd-> main column Length, 3rd side Length
	IwTArray<IwFace*> &mainFaceOrder,		// O:
	IwTArray<IwFace*> &posSideFaceOrder,	// O:
	IwTArray<IwFace*> &negSideFaceOrder		// O:
)
{
	// Need to sew and orient first in order to get the correct topology info.
	pBrep->SewAndOrient();

	// Determine dimension
	IwTArray<IwFace*> allFaces;
	pBrep->GetFaces(allFaces);
	unsigned nFaces = allFaces.GetSize();

	IwTArray<IwEdge*> allEdges;
	IwEdge* edge;
	pBrep->GetEdges(allEdges);
	int bndEdgeNo = 0;
	for (unsigned i=0; i<allEdges.GetSize(); i++)
	{
		edge = allEdges.GetAt(i);
		if (edge->IsLamina())
			bndEdgeNo++;
	}

	int rowLength = (bndEdgeNo-2)/2;
	int columnLength = (nFaces+1)/(rowLength+1);
	int sideLength = (columnLength - 1)/2;

	dimension.Add(rowLength);
	dimension.Add(columnLength);
	dimension.Add(sideLength);

	// Initialize array
	mainFaceOrder.RemoveAll();
	posSideFaceOrder.RemoveAll();
	negSideFaceOrder.RemoveAll();

	// Calcuate the only two edges whose vertexes have exactly 3 connected edges.
	IwTArray<IwEdge*> edges, targetEdges, edges0, edges1;
	IwTArray<IwVertex*> Verts;
	pBrep->GetEdges( edges );

	for( unsigned i = 0; i < edges.GetSize(); i++ )
	{
		edge = edges.GetAt(i);

		if( edge->IsLamina() )
			continue;

		edge->GetVertices( Verts );

		Verts.GetAt(0)->GetEdges( edges0 );
		Verts.GetAt(1)->GetEdges( edges1 );

		if( edges0.GetSize() == 3 && edges1.GetSize() == 3 )
		{
			targetEdges.Add( edge );
		}
	}

	if( targetEdges.GetSize() != 2 )
		return false;

	// Determine the positive x side of edge
	IwEdge* posEdge;
	IwPoint3d pnt0, pnt1, pnt2, pnt3;
	GetEdgeEndPoints(targetEdges.GetAt(0), pnt0, pnt1);
	GetEdgeEndPoints(targetEdges.GetAt(1), pnt2, pnt3);
	if (pnt0[0] > pnt2[0])// compare x components
		posEdge = targetEdges.GetAt(0);
	else
		posEdge = targetEdges.GetAt(1);

	// Determine the positive z side (up) of face
	IwTArray<IwFace*> faces;
	IwFace* posFace;
	posEdge->GetFaces(faces);
	if (faces.GetSize() < 2)
		return false;
	IwTArray<IwVertex*> vertexes0, vertexes1;
	faces.GetAt(0)->GetVertices(vertexes0);
	faces.GetAt(1)->GetVertices(vertexes1);
	IwPoint3d totalPnt0, totalPnt1;
	// the faces all have 4 vertexes (do not need to average the vertex point).
	for (unsigned i=0; i<vertexes0.GetSize(); i++)
		totalPnt0 += vertexes0.GetAt(i)->GetPoint();
	for (unsigned i=0; i<vertexes1.GetSize(); i++)
		totalPnt1 += vertexes1.GetAt(i)->GetPoint();
	if (totalPnt0.z > totalPnt1.z)
		posFace = faces.GetAt(0);
	else
		posFace = faces.GetAt(1);

	// OK! Now we have the posEdge which have exactly 3 edges connecting to its both vertexes, and
	// the posFace which is on the up side of the posEdge.
	// The TraceAdjacentFaces() will trace the faces through the very bottom row (the middle row in main area) 
	// and the negSideFaceOrder
	IwTArray<IwFace*> facesThrough; 
	TraceAdjacentFaces(posFace, posEdge, rowLength + sideLength, facesThrough);
	// the first rowLength faces belong to the middle row and the remaining belong to negSideFaceOrder
	IwTArray<IwFace*> faces5462;// 54~62 faces 
	for (int i=0; i<(rowLength + sideLength); i++)
	{
		if (i<rowLength)
			mainFaceOrder.Add(facesThrough.GetAt(i));
		else
			negSideFaceOrder.Add(facesThrough.GetAt(i));
	}

	// Now we will trace the other faces of the femur
	for (int i=0; i<sideLength; i++)
	{
		// Add the posFace to posSideFaceOrder
		posSideFaceOrder.Add(posFace);
		// Now we trace the two rows of faces connected to the posFace 
		// (one in the front of femur, the other in the back)
		// Get two side edges of the posFace
		IwEdge *posSideEdge=NULL, *negSideEdge=NULL;
		GetFaceConnectedEdges(posFace, posEdge, posSideEdge, negSideEdge);
		if (posSideEdge==NULL || negSideEdge==NULL) return false;
		// Trace the faces on the front side (positive y) of the femur
		IwTArray<IwFace*> frontfaces;// 
		TraceAdjacentFaces(posFace, posSideEdge, rowLength, frontfaces);
		// insert into the front of array, to keep the order
		mainFaceOrder.InsertAt(0,&frontfaces);
		// Trace the faces on the back side (negative y) of the femur
		IwTArray<IwFace*> backfaces;// 
		TraceAdjacentFaces(posFace, negSideEdge, rowLength, backfaces);
		mainFaceOrder.Append(backfaces);
		// append to the end of array, to keep the order

		// Do not need to move up in the last face
		if ( i< (sideLength-1) )
		{
			// move the posEdge up (z) one face
			posEdge = GetFaceOtherEdge(posFace, posEdge);
			if (posEdge == NULL) return false;
			// move the posFace up (z) one face
			posFace = GetEdgeOtherFace(posEdge, posFace);
			if (posFace == NULL) return false;
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////
// This function merges the fumur surfaces in the main
// region into a single patch. It requires a very specific femur patch layout.
///////////////////////////////////////////////////////////////////////////
void CFemoralPart::MergeFemurSurfaces
(
	IwBrep* femurBrep,			// I:
	IwBrep*& mergedFemurBrep	// O:
)
{
	IwContext&			iwContext = m_pDoc->GetIwContext();
	bool succeed = true;

	if ( m_eEntType != ENT_TYPE_FEMORAL_PART ) return;
	if (femurBrep==NULL) return;

	IwTArray<int> dimension;
	IwTArray<IwFace*> mainOrder;
	IwTArray<IwFace*> posSideOrder;
	IwTArray<IwFace*> negSideOrder;
	IwTArray<IwBSplineSurface*> allSurfaces;

	succeed = OrderFaces(femurBrep, dimension, mainOrder, posSideOrder, negSideOrder);
	if ( !succeed )
		return;
	int rowLength = dimension.GetAt(0);
	int columnLength = dimension.GetAt(1);
	int sideLength = dimension.GetAt(2);

	for (int i=0; i<rowLength*columnLength; i++)
	{
		IwFace* face = mainOrder.GetAt(i);
		IwBSplineSurface* surf = (IwBSplineSurface*)face->GetSurface();
		allSurfaces.Add(surf);
	}

	IwTArray<IwBSplineSurface*> tempSurfaces;
	IwTArray<IwBSplineSurface*> allRowSurfaces;
	// merge surfaces on the same row
	for (int i=0; i<columnLength; i++)
	{
		IwBSplineSurface* resultSurf = allSurfaces.GetAt(i*rowLength); // get the first one in the row
		for (int j=1; j<rowLength; j++)
		{
			IwBSplineSurface* joinedSurface;
			joinedSurface = JoinSurfaces( resultSurf, allSurfaces.GetAt(i*rowLength + j) );
			if (joinedSurface == NULL)
			{
				succeed = false;
				break;
			}
			resultSurf = joinedSurface;
			tempSurfaces.Add(joinedSurface);
		}
		allRowSurfaces.Add(resultSurf);
	}

	if ( !succeed ) 
	{
		IwObjsDelete<IwBSplineSurface*> sCleanUpObjs(&tempSurfaces);
		return;
	}

	// merge surfaces representing each row
	IwBSplineSurface* resultSurf = allRowSurfaces.GetAt(0);
	// Merge surfaces for row 1~5 (front)
	for (int i=1; i<sideLength; i++)
	{
		IwBSplineSurface* joinedSurface;
		joinedSurface = JoinSurfaces( resultSurf, allRowSurfaces.GetAt(i) );
		if (joinedSurface == NULL)
		{
			succeed = false;
			break;
		}
		resultSurf = joinedSurface;
		tempSurfaces.Add(joinedSurface);
	}
	IwBSplineSurface* joinedSurfaceFront = new (iwContext) IwBSplineSurface(*resultSurf);

	// Surface for row bottom
	IwBSplineSurface* joinedSurfaceBottom = new (iwContext) IwBSplineSurface(*allRowSurfaces.GetAt(sideLength));

	// Merge surfaces for row 7~11 (Back)
	resultSurf = allRowSurfaces.GetAt(sideLength+1);
	for (int i=sideLength+2; i<columnLength; i++)
	{
		IwBSplineSurface* joinedSurface;
		joinedSurface = JoinSurfaces( resultSurf, allRowSurfaces.GetAt(i) );
		if (joinedSurface == NULL)
		{
			succeed = false;
			break;
		}
		resultSurf = joinedSurface;
		tempSurfaces.Add(joinedSurface);
	}
	IwBSplineSurface* joinedSurfaceBack = new (iwContext) IwBSplineSurface(*resultSurf);
		
	if ( !succeed ) 
	{
		IwObjsDelete<IwBSplineSurface*> sCleanUpObjs(&tempSurfaces);
		return;
	}

	// Then merge these 3 together
	IwBSplineSurface* joinedSurfaceFrontBottom;
	joinedSurfaceFrontBottom = JoinSurfaces( joinedSurfaceFront, joinedSurfaceBottom );
	IwBSplineSurface* joinedSurfaceSingle;
	joinedSurfaceSingle = JoinSurfaces( joinedSurfaceFrontBottom, joinedSurfaceBack );

	// we need to make a copy of this single patch surface
	IwBSplineSurface* singlePatch = new ( iwContext ) IwBSplineSurface( *joinedSurfaceSingle );
	singlePatch->ReparametrizeWithArcLength();
	m_pMainFemurSurface = singlePatch;


	// Merge the left side patches
	IwFace* leftFace = negSideOrder.GetAt(0);
	IwBSplineSurface* leftSurf = (IwBSplineSurface*)leftFace->GetSurface();
	resultSurf = leftSurf; 
	for (int j=1; j<sideLength; j++)
	{
		leftFace = negSideOrder.GetAt(j);
		leftSurf = (IwBSplineSurface*)leftFace->GetSurface();
		IwBSplineSurface* joinedSurface;
		joinedSurface = JoinSurfaces( resultSurf, leftSurf );
		if (joinedSurface == NULL)
		{
			succeed = false;
			break;
		}
		resultSurf = joinedSurface;
		tempSurfaces.Add(joinedSurface);
	}
	IwBSplineSurface* joinedLeftSideSurface = new ( iwContext ) IwBSplineSurface( *resultSurf );
	m_pLeftSideSurface = new ( iwContext ) IwBSplineSurface( *resultSurf );
	m_pLeftSideSurface->ReparametrizeWithArcLength();

	// Merge the right side patches
	IwFace* rightFace = posSideOrder.GetAt(0);
	IwBSplineSurface* rightSurf = (IwBSplineSurface*)rightFace->GetSurface();
	resultSurf = rightSurf; 
	for (int j=1; j<sideLength; j++)
	{
		rightFace = posSideOrder.GetAt(j);
		rightSurf = (IwBSplineSurface*)rightFace->GetSurface();
		IwBSplineSurface* joinedSurface;
		joinedSurface = JoinSurfaces( resultSurf, rightSurf );
		if (joinedSurface == NULL)
		{
			succeed = false;
			break;
		}
		resultSurf = joinedSurface;
		tempSurfaces.Add(joinedSurface);
	}
	IwBSplineSurface* joinedRightSideSurface = new ( iwContext ) IwBSplineSurface( *resultSurf );
	m_pRightSideSurface = new ( iwContext ) IwBSplineSurface( *resultSurf );
	m_pRightSideSurface->ReparametrizeWithArcLength();

	// m_pBrep is all the joined surfaces

	IwFace* pFace;
	IwBrep* brepAll = new (iwContext) IwBrep();
	brepAll->CreateFaceFromSurface(joinedSurfaceFront, joinedSurfaceFront->GetNaturalUVDomain(), pFace);
	brepAll->CreateFaceFromSurface(joinedSurfaceBottom, joinedSurfaceBottom->GetNaturalUVDomain(), pFace);
	brepAll->CreateFaceFromSurface(joinedSurfaceBack, joinedSurfaceBack->GetNaturalUVDomain(), pFace);
	brepAll->CreateFaceFromSurface(joinedLeftSideSurface, joinedLeftSideSurface->GetNaturalUVDomain(), pFace);
	brepAll->CreateFaceFromSurface(joinedRightSideSurface, joinedRightSideSurface->GetNaturalUVDomain(), pFace);

	mergedFemurBrep = brepAll;

	this->SetFPModifiedFlag(true);

	// free tempSurfaces
	IwObjsDelete<IwBSplineSurface*> sCleanUpObjs(&tempSurfaces);

	return;
}

//////////////////////////////////////////////////////////////
// Determine the input surface silhouette
/////////////////////////////////////////////////////////////
bool CFemoralPart::DetermineSilhouetteCurves
(
	IwContext&			iwContext,		// I:
	IwSurface*			surface,		// I:
	IwVector3d&			viewVector,		// I:
	IwTArray<IwCurve*>& silhouetteCurves,// O:
	double				tol				// I:
)
{
	silhouetteCurves.RemoveAll(); // it should be empty, but in case.

	bool succeed = true;

	if ( surface == NULL ) return false;

	IwSurface* femurSurf = surface;

	IwTArray<IwCurve*>  silhouette, silhouetteUV;
	double				approxTol(tol);
	double				angleTol = tol/2.0;
	femurSurf->CreateSilhouetteCurves(iwContext, femurSurf->GetNaturalUVDomain(), viewVector, false,
										&approxTol, &angleTol, &silhouette, &silhouetteUV); 

	if (silhouette.GetSize() == 0) return false;
	
	silhouetteCurves.RemoveAll(); // it should be empty, but in case.
	for (unsigned i=0; i<silhouette.GetSize(); i++)
	{
		IwCurve* curve = silhouetteUV.GetAt(i);
		IwPoint3d pnt;
		IwVector3d tangent;
		double deviation;
		if ( !curve->IsLinear( 0.01, &pnt, &tangent, &deviation) )
		{
			silhouetteCurves.Add(silhouette.GetAt(i));
		}
	}

	return succeed;
}

void CFemoralPart::EmptyViewProfiles()
{
	// Delete the previous profile curves
	for (unsigned i=0; i<m_viewProfileCurves.GetSize(); i++)
	{
		IwObjDelete(m_viewProfileCurves.GetAt(i));
	}

	// update m_viewProfileVector
	m_viewProfileVector = IwVector3d(0,0,0);

	// update m_viewProfileCurves
	m_viewProfileCurves.RemoveAll();
}

bool CFemoralPart::CurvesPlaneOuterIntersection
(
	IwTArray<IwCurve*>& silCurves,	// I:
	IwPlane*& plane,				// I:
	IwAxis2Placement& axes,			// I: to determine positive/negative side of femur
	IwPoint3d& posPnt,				// O:
	IwPoint2d& posUVPnt,			// O:
	IwPoint3d& negPnt,				// O:
	IwPoint2d& negUVPnt				// O:
)
{
	posPnt = IwPoint3d(-10000, -10000, -10000);
	negPnt = IwPoint3d(-10000, -10000, -10000);

	IwPoint3d origin = axes.GetOrigin();
	IwVector3d xAxis = axes.GetXAxis();

	IwAxis2Placement planeOrient;
	plane->GetCanonical(planeOrient);
	IwVector3d planeYAxis = planeOrient.GetYAxis();// plane's up vector

	int nSize = silCurves.GetSize();
	IwTArray<IwPoint3d> posIntPnts, negIntPnts;
	IwTArray<IwPoint2d> posIntUVPnts, negIntUVPnts;
	double intCrvParam;
	IwPoint2d planeParam;
	IwPoint3d intPnt;
	IwVector3d vec;
	double dot;
	for (int i=0; i<nSize; i++)
	{
		IwCurve* curve = silCurves.GetAt(i);
		IwSolutionArray solutions;
		plane->GlobalCurveIntersect(plane->GetNaturalUVDomain(), *curve, curve->GetNaturalInterval(), 0.01, solutions);
		int nSolSize = solutions.GetSize();
		for (int j=0; j<nSolSize; j++)
		{
			IwSolution& sol = solutions[j];
			intCrvParam = sol.m_vStart[0];
			curve->EvaluatePoint(intCrvParam, intPnt);
			vec = intPnt - origin;
			dot = xAxis.Dot(vec);
			if (dot > 0)
			{
				posIntPnts.Add(intPnt);
				posIntUVPnts.Add(IwPoint2d(sol.m_vStart[1], sol.m_vStart[2]));
			}
			else
			{
				negIntPnts.Add(intPnt);
				negIntUVPnts.Add(IwPoint2d(sol.m_vStart[1], sol.m_vStart[2]));
			}
		}
	}

	// if no intersection points with plane, try to get the closest points instead.
	if ( posIntPnts.GetSize() == 0 || negIntPnts.GetSize() == 0 )
	{
		posIntPnts.RemoveAll();
		negIntPnts.RemoveAll();
		posIntUVPnts.RemoveAll();
		negIntUVPnts.RemoveAll();
		for (int i=0; i<nSize; i++)
		{
			IwCurve* curve = silCurves.GetAt(i);
			IwSolutionArray solutions;
			plane->GlobalCurveSolve(plane->GetNaturalUVDomain(), *curve, curve->GetNaturalInterval(), 
									IW_SO_MINIMIZE, 0.001, NULL, NULL, IW_SR_SINGLE, solutions);
			int nSolSize = solutions.GetSize();
			for (int j=0; j<nSolSize; j++)
			{
				IwSolution& sol = solutions[j];
				planeParam = IwPoint2d(sol.m_vStart[1], sol.m_vStart[2]);
				plane->EvaluatePoint(planeParam, intPnt);
				vec = intPnt - origin;
				dot = xAxis.Dot(vec);
				if (dot > 0)
				{
					posIntPnts.Add(intPnt);
					posIntUVPnts.Add(IwPoint2d(sol.m_vStart[1], sol.m_vStart[2]));
				}
				else
				{
					negIntPnts.Add(intPnt);
					negIntUVPnts.Add(IwPoint2d(sol.m_vStart[1], sol.m_vStart[2]));
				}
			}
		}
	}

	double xProj;
	double maxXProjection = -10000;
	IwPoint3d maxXProjPnt;
	IwPoint2d maxXProjUVPnt;
	for (unsigned i=0; i<posIntPnts.GetSize(); i++)
	{
		vec = posIntPnts.GetAt(i) - origin;
		xProj = vec.Dot(planeYAxis);
		if (xProj > maxXProjection)
		{
			maxXProjection = xProj;
			maxXProjPnt = posIntPnts.GetAt(i);
			maxXProjUVPnt = posIntUVPnts.GetAt(i);
		}
	}
	if (maxXProjection > -10000) 
	{
		posPnt = maxXProjPnt;
		posUVPnt = maxXProjUVPnt;
	}

	maxXProjection = -10000;
	for (unsigned i=0; i<negIntPnts.GetSize(); i++)
	{
		vec = negIntPnts.GetAt(i) - origin;
		xProj = vec.Dot(planeYAxis);
		if (xProj > maxXProjection)
		{
			maxXProjection = xProj;
			maxXProjPnt = negIntPnts.GetAt(i);
			maxXProjUVPnt = negIntUVPnts.GetAt(i);
		}
	}
	if (maxXProjection > -10000) 
	{
		negPnt = maxXProjPnt;
		negUVPnt = maxXProjUVPnt;
	}

	return true;
}


bool CFemoralPart::CurvesPlaneInnerIntersection
(
	IwTArray<IwCurve*>& silCurves,	// I:
	IwPlane*& plane,				// I:
	IwAxis2Placement& axes,			// I: 
	double& refSize,				// I: 
	IwPoint3d& pnt,					// O:
	IwPoint2d& UVPnt				// O:
)
{
	pnt = IwPoint3d(-10000, -10000, -10000);

	IwPoint3d origin = axes.GetOrigin();
	IwVector3d xAxis = axes.GetXAxis();

	IwAxis2Placement planeOrient;
	plane->GetCanonical(planeOrient);
	IwVector3d planeYAxis = planeOrient.GetYAxis();// plane's up vector
	IwPoint3d planeOrigin = planeOrient.GetOrigin();

	int nSize = silCurves.GetSize();
	IwTArray<IwPoint3d> IntPnts;
	IwTArray<IwPoint2d> IntUVPnts;
	double intCrvParam;
	IwPoint3d intPnt;
	IwVector3d vec;
	double dot;
	for (int i=0; i<nSize; i++)
	{
		IwCurve* curve = silCurves.GetAt(i);
		IwSolutionArray solutions;
		plane->GlobalCurveIntersect(plane->GetNaturalUVDomain(), *curve, curve->GetNaturalInterval(), 0.01, solutions);
		int nSolSize = solutions.GetSize();
		for (int j=0; j<nSolSize; j++)
		{
			IwSolution& sol = solutions[j];
			intCrvParam = sol.m_vStart[0];
			curve->EvaluatePoint(intCrvParam, intPnt);
			vec = intPnt - origin;
			dot = fabs(xAxis.Dot(vec));
			if (dot < 0.125*refSize)// how wide we consider the intersection points are from anterior profiles
			{
				IntPnts.Add(intPnt);
				IntUVPnts.Add(IwPoint2d(sol.m_vStart[1], sol.m_vStart[2]));
			}
		}
	}

	double yProj;
	double minYProjection = 10000;
	IwPoint3d minYProjPnt;
	IwPoint2d minYProjUVPnt;
	for (unsigned i=0; i<IntPnts.GetSize(); i++)
	{
		vec = IntPnts.GetAt(i) - planeOrigin;
		yProj = vec.Dot(planeYAxis);
		if (yProj < minYProjection)
		{
			minYProjection = yProj;
			minYProjPnt = IntPnts.GetAt(i);
			minYProjUVPnt = IntUVPnts.GetAt(i);
		}
	}
	if (minYProjection < 10000) 
	{
		pnt = minYProjPnt;
		UVPnt = minYProjUVPnt;
	}

	return true;
}

bool CFemoralPart::GetOuterLoop()
{


	return true;
}

void CFemoralPart::GetSelectableEntities
(
	IwTArray<IwBrep*>& breps,		// O:
	IwTArray<IwCurve*>& curves,		// O:
	IwTArray<IwPoint3d>& points		// O:
)
{
	// Nothing is selectable in CFemoralPart level.

	CPart::GetSelectableEntities(breps, curves, points);
}

void CFemoralPart::SetFPModifiedFlag( bool bModified )
{
	m_bFPModified = bModified;
	if (m_bFPModified)
	{
		m_bFPDataModified = true;
		m_pDoc->SetModified(m_bFPModified);
	}
}
//////////////////////////////////////////////////////////////////////
// These are simplified planar silhouette curves. 
// 0th for positive, 1st for negative side, 2nd inner.
//////////////////////////////////////////////////////////////////////
void CFemoralPart::GetViewProfilesInfo
(
	IwVector3d& viewVector,					// I: if initialized, O: if not initialized
	IwTArray<IwCurve*>& viewProfiles		// O: 0th for positive, 1st for negative side, 2nd inner. All on Y-Z plane.
)
{
	viewProfiles.RemoveAll();

	// if not initialize, return the existing silhouettes.
	if ( !viewVector.IsInitialized() )
	{
		//if ( m_viewProfileCurves.GetSize() == 0 )
		//	DetermineViewProfiles();
		viewVector = m_viewProfileVector;
		viewProfiles.Append(m_viewProfileCurves);
	}
	// if initialized
	else
	{
		double angle;
		if ( m_viewProfileVector == IwVector3d(0,0,0) )
			angle = 1.0;// a big number
		else
			m_viewProfileVector.AngleBetween(viewVector, angle);
		if ( angle > 0.001/180*IW_PI )
		{
			// update view profile, if the viewing angle is different from existing one.
			//DetermineViewProfiles();
		}
		viewProfiles.Append(m_viewProfileCurves);
	}
}

//////////////////////////////////////////////////////////////////////
// If you call this function, you are absolutely wrong.
void CFemoralPart::SetViewProfiles_ThisIsWrongFunctionToCall(IwTArray<IwCurve*>& viewProfiles)
{
	// Delete the previous profile curves
	for (unsigned i=0; i<m_viewProfileCurves.GetSize(); i++)
	{
		IwObjDelete(m_viewProfileCurves.GetAt(i));
	}

	m_viewProfileCurves.Append(viewProfiles);

	return;
}

//////////////////////////////////////////////////////////////////////
void CFemoralPart::SmoothMainSurfaceAtCrisps(IwBSplineSurface*& mainSurface)
{
	// Get knot info
	ULONG ukCountMain, vkCountMain;
	double *uKnotsMain, *vKnotsMain;
	mainSurface->GetKnotsPointers(ukCountMain, vkCountMain, uKnotsMain, vKnotsMain);
	// Get control points
	ULONG uctrlCountMain, vctrlCountMain;
	double *ctrlPointersMain;
	mainSurface->GetControlPointsPointer(uctrlCountMain, vctrlCountMain, ctrlPointersMain);

	// prepare modifying control points
#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
   mainSurface->Notify(IW_NO_PRE_EDIT, mainSurface, nullptr, nullptr);
#else
   mainSurface->Notify(IW_NO_PRE_EDIT);
#endif

	// basic info
	int cpIndex, cpIndexPrevU, cpIndexPostU;
	IwPoint3d ctrlPoint, ctrlPointPrevU, ctrlPointPostU, avgCtrlPoint;

	///////////////////////////////////////////////////////////
	// Looking for the internal u knot multiplicity, 
	// where indicates the possible discontinuity.
	for (ULONG ui=4; ui<(uctrlCountMain-4); ui++)// every one except first 4 ones and last 4 ones
	{
		// detect the multiplicity
		if (IS_EQ_TOL6(uKnotsMain[ui+1], uKnotsMain[ui+2]) &&
			IS_EQ_TOL6(uKnotsMain[ui+2], uKnotsMain[ui+3]))
		{
			for (ULONG vi=1; vi<(vctrlCountMain-1); vi++) // every one except first one and last one
			{
				cpIndex = 4*(vi+ui*vctrlCountMain);
				ctrlPoint = IwPoint3d(ctrlPointersMain[cpIndex], ctrlPointersMain[cpIndex+1], ctrlPointersMain[cpIndex+2]);
				cpIndexPrevU = 4*(vi+(ui-1)*vctrlCountMain);
				ctrlPointPrevU = IwPoint3d(ctrlPointersMain[cpIndexPrevU], ctrlPointersMain[cpIndexPrevU+1], ctrlPointersMain[cpIndexPrevU+2]);
				cpIndexPostU = 4*(vi+(ui+1)*vctrlCountMain);
				ctrlPointPostU = IwPoint3d(ctrlPointersMain[cpIndexPostU], ctrlPointersMain[cpIndexPostU+1], ctrlPointersMain[cpIndexPostU+2]);
				// average prev and post control points.
				// Therefore, these 3 control points will be aligned, and
				// meet the C1 continuity.
				avgCtrlPoint = 0.5*ctrlPointPrevU + 0.5*ctrlPointPostU;
				// set back
				ctrlPointersMain[cpIndex] = avgCtrlPoint.x;
				ctrlPointersMain[cpIndex+1] = avgCtrlPoint.y;
				ctrlPointersMain[cpIndex+2] = avgCtrlPoint.z;
			}
		}
	}

	// Update mainSurface based on the new control points
#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	mainSurface->Notify(IW_NO_POST_EDIT, mainSurface, nullptr, nullptr);
#else
	mainSurface->Notify(IW_NO_POST_EDIT);
#endif
}

///////////////////////////////////////////////////////////////////////
// This function handles a very specific situation in FemoralPart.
// The (u,v) orientation should be specifically defined by 
// CFemoralPart::NormalizeThreeSurfaces().
// This function will smooth the boundaries along the mainSurface, 
// leftSurface, and rightSurface to ensure they are C1 continueous. 
///////////////////////////////////////////////////////////////////////
void CFemoralPart::SmoothThreeSurfaceBoundaries
(
	IwBSplineSurface*& mainSurface,		// I/O:
	IwBSplineSurface*& leftSurface,		// I/O:
	IwBSplineSurface*& rightSurface		// I/O:
)
{
	// Get knot info
	ULONG ukCountMain, vkCountMain, ukCountLeft, vkCountLeft, ukCountRight, vkCountRight;
	double *uKnotsMain, *vKnotsMain, *uKnotsLeft, *vKnotsLeft, *uKnotsRight, *vKnotsRight;
	mainSurface->GetKnotsPointers(ukCountMain, vkCountMain, uKnotsMain, vKnotsMain);
	leftSurface->GetKnotsPointers(ukCountLeft, vkCountLeft, uKnotsLeft, vKnotsLeft);
	rightSurface->GetKnotsPointers(ukCountRight, vkCountRight, uKnotsRight, vKnotsRight);
	// Get control points
	ULONG uctrlCountMain, vctrlCountMain, uctrlCountLeft, vctrlCountLeft, uctrlCountRight, vctrlCountRight;
	double *ctrlPointersMain, *ctrlPointersLeft, *ctrlPointersRight;
	mainSurface->GetControlPointsPointer(uctrlCountMain, vctrlCountMain, ctrlPointersMain);
	leftSurface->GetControlPointsPointer(uctrlCountLeft, vctrlCountLeft, ctrlPointersLeft);
	rightSurface->GetControlPointsPointer(uctrlCountRight, vctrlCountRight, ctrlPointersRight);

	// prepare modifying control points
#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	mainSurface->Notify(IW_NO_PRE_EDIT, mainSurface, nullptr, nullptr);
	leftSurface->Notify(IW_NO_PRE_EDIT, leftSurface, nullptr, nullptr);
	rightSurface->Notify(IW_NO_PRE_EDIT, rightSurface, nullptr, nullptr);
#else
	mainSurface->Notify(IW_NO_PRE_EDIT);
	leftSurface->Notify(IW_NO_PRE_EDIT);
	rightSurface->Notify(IW_NO_PRE_EDIT);
#endif
	// basic info
	int cpIndex, cpIndexPrevU, cpIndexPostU;
	IwPoint3d ctrlPoint, ctrlPointPrevU, ctrlPointPostU, avgCtrlPoint;

	///////////////////////////////////////////////////////////
	// Looking for the internal u knot multiplicity, 
	// where indicates the possible discontinuity.
	for (ULONG ui=4; ui<(uctrlCountMain-4); ui++)// every one except first 4 ones and last 4 ones
	{
		// detect the multiplicity
		if (IS_EQ_TOL6(uKnotsMain[ui+1], uKnotsMain[ui+2]) &&
			IS_EQ_TOL6(uKnotsMain[ui+2], uKnotsMain[ui+3]))
		{
			for (ULONG vi=1; vi<(vctrlCountMain-1); vi++) // every one except first one and last one
			{
				cpIndex = 4*(vi+ui*vctrlCountMain);
				ctrlPoint = IwPoint3d(ctrlPointersMain[cpIndex], ctrlPointersMain[cpIndex+1], ctrlPointersMain[cpIndex+2]);
				cpIndexPrevU = 4*(vi+(ui-1)*vctrlCountMain);
				ctrlPointPrevU = IwPoint3d(ctrlPointersMain[cpIndexPrevU], ctrlPointersMain[cpIndexPrevU+1], ctrlPointersMain[cpIndexPrevU+2]);
				cpIndexPostU = 4*(vi+(ui+1)*vctrlCountMain);
				ctrlPointPostU = IwPoint3d(ctrlPointersMain[cpIndexPostU], ctrlPointersMain[cpIndexPostU+1], ctrlPointersMain[cpIndexPostU+2]);
				// average prev and post control points.
				// Therefore, these 3 control points will be aligned, and
				// meet the C1 continuity.
				avgCtrlPoint = 0.5*ctrlPointPrevU + 0.5*ctrlPointPostU;
				// set back
				ctrlPointersMain[cpIndex] = avgCtrlPoint.x;
				ctrlPointersMain[cpIndex+1] = avgCtrlPoint.y;
				ctrlPointersMain[cpIndex+2] = avgCtrlPoint.z;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Smooth along the 2 iso-v boundaries, but not corner points
	int bLeftRight;
	ULONG uIndexOut, vIndexOut, uIndexOutNext, vIndexOutNext;
	bool bCorner;
	IwVector3d zeroVector = IwVector3d(0,0,0);
	for (ULONG ui=1; ui<(uctrlCountMain-1); ui++)// every one except first one and last one
	{
		for (ULONG vi=0; vi<vctrlCountMain; vi+=(vctrlCountMain-1)) // first one and last one
		{
			CFemoralPart::FindCorrespondentIndexOnSideSurface(ui, vi, uctrlCountMain, vctrlCountMain, 
										uctrlCountLeft, vctrlCountLeft, uctrlCountRight, vctrlCountRight, 
										bLeftRight, uIndexOut, vIndexOut, uIndexOutNext, vIndexOutNext, bCorner);
			if ( !bCorner ) // non lower corner points
			{
				if ( bLeftRight == 0 ) // Left surface
				{
					cpIndex = 4*(vi+ui*vctrlCountMain);
					ctrlPoint = IwPoint3d(ctrlPointersMain[cpIndex], ctrlPointersMain[cpIndex+1], ctrlPointersMain[cpIndex+2]);
					cpIndexPrevU = 4*((vi-1)+ui*vctrlCountMain);// left side should be minus one
					ctrlPointPrevU = IwPoint3d(ctrlPointersMain[cpIndexPrevU], ctrlPointersMain[cpIndexPrevU+1], ctrlPointersMain[cpIndexPrevU+2]);
					cpIndexPostU = 4*(vIndexOutNext+uIndexOutNext*vctrlCountLeft);
					ctrlPointPostU = IwPoint3d(ctrlPointersLeft[cpIndexPostU], ctrlPointersLeft[cpIndexPostU+1], ctrlPointersLeft[cpIndexPostU+2]);
					// average prev and post control points.
					// Therefore, these 3 control points will be aligned, and
					// meet the C1 continuity.
					avgCtrlPoint = 0.5*ctrlPointPrevU + 0.5*ctrlPointPostU;
					// set back
					ctrlPointersMain[cpIndex] = avgCtrlPoint.x;
					ctrlPointersMain[cpIndex+1] = avgCtrlPoint.y;
					ctrlPointersMain[cpIndex+2] = avgCtrlPoint.z;
					// move the left surface boundary control point
					CFemoralPart::MoveBoundaryControlPoint(avgCtrlPoint, zeroVector, uIndexOut, vIndexOut, uctrlCountLeft, vctrlCountLeft, ctrlPointersLeft);
				}
				else if ( bLeftRight == 1 ) // Right surface
				{
					cpIndex = 4*(vi+ui*vctrlCountMain);
					ctrlPoint = IwPoint3d(ctrlPointersMain[cpIndex], ctrlPointersMain[cpIndex+1], ctrlPointersMain[cpIndex+2]);
					cpIndexPrevU = 4*((vi+1)+ui*vctrlCountMain);// right side should be plus one
					ctrlPointPrevU = IwPoint3d(ctrlPointersMain[cpIndexPrevU], ctrlPointersMain[cpIndexPrevU+1], ctrlPointersMain[cpIndexPrevU+2]);
					cpIndexPostU = 4*(vIndexOutNext+uIndexOutNext*vctrlCountRight);
					ctrlPointPostU = IwPoint3d(ctrlPointersRight[cpIndexPostU], ctrlPointersRight[cpIndexPostU+1], ctrlPointersRight[cpIndexPostU+2]);
					// average prev and post control points.
					// Therefore, these 3 control points will be aligned, and
					// meet the C1 continuity.
					avgCtrlPoint = 0.5*ctrlPointPrevU + 0.5*ctrlPointPostU;
					// set back
					ctrlPointersMain[cpIndex] = avgCtrlPoint.x;
					ctrlPointersMain[cpIndex+1] = avgCtrlPoint.y;
					ctrlPointersMain[cpIndex+2] = avgCtrlPoint.z;
					// move the right surface boundary control point
					CFemoralPart::MoveBoundaryControlPoint(avgCtrlPoint, zeroVector, uIndexOut, vIndexOut, uctrlCountRight, vctrlCountRight, ctrlPointersRight);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Smooth the 2 lower corner points
	int cpIndexInnerSide;
	IwPoint3d ctrlPointInnerSide;
	for (ULONG ui=1; ui<(uctrlCountMain-1); ui++)// every one except first one and last one
	{
		for (ULONG vi=0; vi<vctrlCountMain; vi+=(vctrlCountMain-1)) // first one and last one
		{
			CFemoralPart::FindCorrespondentIndexOnSideSurface(ui, vi, uctrlCountMain, vctrlCountMain, 
										uctrlCountLeft, vctrlCountLeft, uctrlCountRight, vctrlCountRight, 
										bLeftRight, uIndexOut, vIndexOut, uIndexOutNext, vIndexOutNext, bCorner);
			if ( bCorner ) // if it is a lower corner point
			{
				if ( bLeftRight == 0 ) // Left surface
				{
					cpIndex = 4*(vi+ui*vctrlCountMain);
					ctrlPoint = IwPoint3d(ctrlPointersMain[cpIndex], ctrlPointersMain[cpIndex+1], ctrlPointersMain[cpIndex+2]);
					cpIndexInnerSide = 4*((vi-1)+ui*vctrlCountMain);
					ctrlPointInnerSide = IwPoint3d(ctrlPointersMain[cpIndexInnerSide], ctrlPointersMain[cpIndexInnerSide+1], ctrlPointersMain[cpIndexInnerSide+2]);
					cpIndexPrevU = 4*(vi+(ui-1)*vctrlCountMain);
					ctrlPointPrevU = IwPoint3d(ctrlPointersMain[cpIndexPrevU], ctrlPointersMain[cpIndexPrevU+1], ctrlPointersMain[cpIndexPrevU+2]);
					cpIndexPostU = 4*(vi+(ui+1)*vctrlCountMain);
					ctrlPointPostU = IwPoint3d(ctrlPointersMain[cpIndexPostU], ctrlPointersMain[cpIndexPostU+1], ctrlPointersMain[cpIndexPostU+2]);
					// average prev and post control points.
					// Therefore, these 3 control points will be aligned, and
					// meet the C1 continuity.
					avgCtrlPoint = 0.333333*ctrlPointPrevU + 0.333333*ctrlPointPostU + 0.333333*ctrlPointInnerSide;
					// set back
					ctrlPointersMain[cpIndex] = avgCtrlPoint.x;
					ctrlPointersMain[cpIndex+1] = avgCtrlPoint.y;
					ctrlPointersMain[cpIndex+2] = avgCtrlPoint.z;
					// move the left surface boundary control point
					CFemoralPart::MoveBoundaryControlPoint(avgCtrlPoint, zeroVector, uIndexOut, vIndexOut, uctrlCountLeft, vctrlCountLeft, ctrlPointersLeft);
				}
				else if ( bLeftRight == 1 ) // Right surface
				{
					cpIndex = 4*(vi+ui*vctrlCountMain);
					ctrlPoint = IwPoint3d(ctrlPointersMain[cpIndex], ctrlPointersMain[cpIndex+1], ctrlPointersMain[cpIndex+2]);
					cpIndexInnerSide = 4*((vi+1)+ui*vctrlCountMain);
					ctrlPointInnerSide = IwPoint3d(ctrlPointersMain[cpIndexInnerSide], ctrlPointersMain[cpIndexInnerSide+1], ctrlPointersMain[cpIndexInnerSide+2]);
					cpIndexPrevU = 4*(vi+(ui-1)*vctrlCountMain);
					ctrlPointPrevU = IwPoint3d(ctrlPointersMain[cpIndexPrevU], ctrlPointersMain[cpIndexPrevU+1], ctrlPointersMain[cpIndexPrevU+2]);
					cpIndexPostU = 4*(vi+(ui+1)*vctrlCountMain);
					ctrlPointPostU = IwPoint3d(ctrlPointersMain[cpIndexPostU], ctrlPointersMain[cpIndexPostU+1], ctrlPointersMain[cpIndexPostU+2]);
					// Project the corner control point onto a plane defined by ctrlPointPrevU, ctrlPointPostU, and ctrlPointInnerSide.
					IwVector3d v1, v2, cornerPlaneNormal;
					v1 = ctrlPointPrevU - ctrlPointInnerSide;
					v2 = ctrlPointPostU - ctrlPointInnerSide;
					cornerPlaneNormal = v1*v2;
					cornerPlaneNormal.Unitize();
					avgCtrlPoint = ctrlPoint.ProjectPointToPlane(ctrlPointInnerSide, cornerPlaneNormal);
					//avgCtrlPoint = 0.333333*ctrlPointPrevU + 0.333333*ctrlPointPostU + 0.333333*ctrlPointInnerSide;
					// set back
					ctrlPointersMain[cpIndex] = avgCtrlPoint.x;
					ctrlPointersMain[cpIndex+1] = avgCtrlPoint.y;
					ctrlPointersMain[cpIndex+2] = avgCtrlPoint.z;
					// move the right surface boundary control point
					CFemoralPart::MoveBoundaryControlPoint(avgCtrlPoint, zeroVector, uIndexOut, vIndexOut, uctrlCountRight, vctrlCountRight, ctrlPointersRight);
				}
			}
		}
	}

	// Update mainSurface, leftSurface, rightSurface based on the new control points
#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	mainSurface->Notify(IW_NO_POST_EDIT, mainSurface, nullptr, nullptr);
	leftSurface->Notify(IW_NO_POST_EDIT, leftSurface, nullptr, nullptr);
	rightSurface->Notify(IW_NO_POST_EDIT, rightSurface, nullptr, nullptr);
#else
	mainSurface->Notify(IW_NO_POST_EDIT);
	leftSurface->Notify(IW_NO_POST_EDIT);
	rightSurface->Notify(IW_NO_POST_EDIT);
#endif
	return;
}

//////////////////////////////////////////////////////////////////
// This function moves the control point along the boundary.
// Note that caller should notify IW_NO_PRE_EDIT & IW_NO_POST_EDIT
// In order to speed up the processing time, ucCount, vcCount, and 
// cPointers should be input.
//////////////////////////////////////////////////////////////////
void CFemoralPart::MoveBoundaryControlPoint
(
	IwPoint3d& ctrlPnt,			// I: the desired control point's (x,y,z)
	IwVector3d& movingVector,	// I: the vector (delta) to move
	ULONG uIndex,				// I: the control point position index along u
	ULONG vIndex,				// I: the control point position index along v
	ULONG& ucCount,				// I: the control point count along u
	ULONG& vcCount,				// I: the control point count along v
	double*& cPointers			// I/O: control point array [x,y,z,w,...]
)
{
	IwVector3d newLocation;
	int cpIndex;
	double* cp;

	cpIndex = 4*(vIndex+uIndex*vcCount);
	newLocation = ctrlPnt + movingVector;
	cp = &cPointers[cpIndex];
	*cp = newLocation.x;
	cp = &cPointers[cpIndex+1];
	*cp = newLocation.y;
	cp = &cPointers[cpIndex+2];
	*cp = newLocation.z;

	return;
}

////////////////////////////////////////////////////////////////////////
// This function will orient the 3 surfaces such that the surface normal
// are always outward, and the the mainSurface has (u,v)=(0,0) at the 
// positive y-axis (front) and positive x-axis, the u-direction along  
// the long boundary from the front to the back, the v-direction along 
// the short boundary from the positive x-axis to negative x-axis.
// 
// The leftSurface (the side surface at the negative x-axis) has
// (u,v)=(0,0) at positive z-axis (top) and positive y-axis (front), 
// the u-direction along the long boundary from top to bottom, 
// the v-direction along the short boundary along the positive y-axis (front) 
// to negative y-axis (back).
//
// The rightSurface (the side surface at the positive x-axis) has
// (u,v)=(0,0) at positive z-axis (top) and negative y-axis(back), 
// the u-direction along the long boundary from top to bottom, 
// the v-direction along the short boundary along the negative y-axis (back)
// to positive y-axis (front).
bool CFemoralPart::NormalizeThreeSurfaces
(
	IwAxis2Placement& refAxis		// I: as orientation reference
)
{
	bool bRet = CFemoralPart::NormalizeThreeSurfaces(refAxis, m_pMainFemurSurface, m_pLeftSideSurface, m_pRightSideSurface);

	return bRet;
}

//////////////////////////////////////////////////////////////
bool CFemoralPart::NormalizeThreeSurfaces
(
	IwAxis2Placement& refAxis,		// I: as orientation reference
	IwBSplineSurface*& mainSurface, // I/O:
	IwBSplineSurface*& leftSurface, // I/O:
	IwBSplineSurface*& rightSurface // I/O:
)
{
	bool changed = false;

	if ( mainSurface == NULL ||
		 leftSurface == NULL ||
		 rightSurface == NULL )
		 return false;

	IwPoint3d pntU0V0, pntU0V1, pntU1V0, pntU1V1;
	IwExtent2d dom;
	IwVector3d vecU, vecV, normal;
	double lengthU, lengthV;

	/////////////////////////////////////////////////////////////
	// Check left side surface
	dom = leftSurface->GetNaturalUVDomain();
	leftSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
	leftSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
	leftSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
	leftSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
	leftSurface->EvaluateNormal(dom.Evaluate(0.5,0.5), FALSE, FALSE, normal);
	vecU = pntU1V0 - pntU0V0;
	vecV = pntU0V1 - pntU0V0;
	lengthU = vecU.Length();
	lengthV = vecV.Length();
	
	// u should be the longer boundary
	if ( lengthV > lengthU )
	{
		leftSurface->SwapUV();
		// update data
		dom = leftSurface->GetNaturalUVDomain();
		leftSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
		leftSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
		leftSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
		leftSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
		vecU = pntU1V0 - pntU0V0;
		vecV = pntU0V1 - pntU0V0;
		lengthU = vecU.Length();
		lengthV = vecV.Length();
	}

	// u should be from top to bottom.
	if ( vecU.Dot(refAxis.GetZAxis()) > 0 )
	{
		leftSurface->Reverse(IW_SP_U);
		// update data
		dom = leftSurface->GetNaturalUVDomain();
		leftSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
		leftSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
		leftSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
		leftSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
		vecU = pntU1V0 - pntU0V0;
		vecV = pntU0V1 - pntU0V0;
		lengthU = vecU.Length();
		lengthV = vecV.Length();
	}

	// v should be from front to back.
	if ( vecV.Dot(refAxis.GetYAxis()) > 0 )
	{
		leftSurface->Reverse(IW_SP_V);
	}

	leftSurface->EvaluateNormal(dom.Evaluate(0.5,0.5), FALSE, FALSE, normal);

	/////////////////////////////////////////////////////////////
	// Check right side surface
	dom = rightSurface->GetNaturalUVDomain();
	rightSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
	rightSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
	rightSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
	rightSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
	rightSurface->EvaluateNormal(dom.Evaluate(0.5,0.5), FALSE, FALSE, normal);
	vecU = pntU1V0 - pntU0V0;
	vecV = pntU0V1 - pntU0V0;
	lengthU = vecU.Length();
	lengthV = vecV.Length();

	// u should be the longer boundary
	if ( lengthV > lengthU )
	{
		rightSurface->SwapUV();
		// update data
		dom = rightSurface->GetNaturalUVDomain();
		rightSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
		rightSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
		rightSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
		rightSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
		vecU = pntU1V0 - pntU0V0;
		vecV = pntU0V1 - pntU0V0;
		lengthU = vecU.Length();
		lengthV = vecV.Length();
	}

	// u should be from top to bottom.
	if ( vecU.Dot(refAxis.GetZAxis()) > 0 )
	{
		rightSurface->Reverse(IW_SP_U);
		// update data
		dom = rightSurface->GetNaturalUVDomain();
		rightSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
		rightSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
		rightSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
		rightSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
		vecU = pntU1V0 - pntU0V0;
		vecV = pntU0V1 - pntU0V0;
		lengthU = vecU.Length();
		lengthV = vecV.Length();
	}

	// v should be from back to front.
	if ( vecV.Dot(refAxis.GetYAxis()) < 0 )
	{
		rightSurface->Reverse(IW_SP_V);
	}

	rightSurface->EvaluateNormal(dom.Evaluate(0.5,0.5), FALSE, FALSE, normal);

	/////////////////////////////////////////////////////////////
	// Check mainSurface
	IwBSplineCurve *uBndCurve0, *uBndCurve1, *vBndCurve0, *vBndCurve1;
	IwContext&	iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	// create v = const boundary curves, that are the u boundary curves.
	mainSurface->CreateIsoBoundaries(iwContext, IW_SP_V, 0.01, uBndCurve0, uBndCurve1);
	// create u = const boundary curves, that are the v boundary curves.
	mainSurface->CreateIsoBoundaries(iwContext, IW_SP_U, 0.01, vBndCurve0, vBndCurve1);
	uBndCurve0->Length(uBndCurve0->GetNaturalInterval(), 0.01, lengthU);
	vBndCurve0->Length(vBndCurve0->GetNaturalInterval(), 0.01, lengthV);

	// u should be the longer boundary
	if ( lengthV > lengthU )
	{
		mainSurface->SwapUV();
	}
	// update data since lengthV & lengthU are determined in different way
	dom = mainSurface->GetNaturalUVDomain();
	mainSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
	mainSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
	mainSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
	mainSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
	vecU = pntU1V0 - pntU0V0;
	vecV = pntU0V1 - pntU0V0;
	lengthU = vecU.Length();
	lengthV = vecV.Length();

	// u should be from front (to bottom and then) to back.
	if ( vecU.Dot(refAxis.GetYAxis()) > 0 )
	{
		mainSurface->Reverse(IW_SP_U);
		// update data
		dom = mainSurface->GetNaturalUVDomain();
		mainSurface->EvaluatePoint(dom.Evaluate(0,0), pntU0V0);
		mainSurface->EvaluatePoint(dom.Evaluate(0,1), pntU0V1);
		mainSurface->EvaluatePoint(dom.Evaluate(1,0), pntU1V0);
		mainSurface->EvaluatePoint(dom.Evaluate(1,1), pntU1V1);
		vecU = pntU1V0 - pntU0V0;
		vecV = pntU0V1 - pntU0V0;
		lengthU = vecU.Length();
		lengthV = vecV.Length();
	}

	// v should be from positive x-axis to negative x-axis.
	if ( vecV.Dot(refAxis.GetXAxis()) > 0 )
	{
		mainSurface->Reverse(IW_SP_V);
	}

	mainSurface->EvaluateNormal(dom.Evaluate(0.5,0.5), FALSE, FALSE, normal);

	if ( uBndCurve0 )
		IwObjDelete(uBndCurve0);
	if ( uBndCurve1 )
		IwObjDelete(uBndCurve1);
	if ( vBndCurve0 )
		IwObjDelete(vBndCurve0);
	if ( vBndCurve1 )
		IwObjDelete(vBndCurve1);
	
	return true;
}

/////////////////////////////////////////////////////////////////////
// Note, the surfaces have to be normalized before calling this function.
/////////////////////////////////////////////////////////////////////
int CFemoralPart::FindCorrespondentIndexOnSideSurface
(
	ULONG& uIndexMain,		// I: the u index of a control point in main surface
	ULONG& vIndexMain,		// I: the v index of a control point in main surface
	ULONG& uCtrlCountMain,  // I: the total number of control point along u direction in main surface
	ULONG& vCtrlCountMain,	// I: the total number of control point along v direction in main surface
	ULONG& uCtrlCountLeft,  // I: the total number of control point along u direction in left surface
	ULONG& vCtrlCountLeft,	// I: the total number of control point along v direction in left surface
	ULONG& uCtrlCountRight, // I: the total number of control point along u direction in right surface
	ULONG& vCtrlCountRight, // I: the total number of control point along v direction in right surface
	int& leftRight,			// O: 0=left surface, 1=right surface, -1=no found
	ULONG& uIndexOut,		// O: the u index on the corresponding leftRight surface
	ULONG& vIndexOut,		// O: the v index on the corresponding leftRight surface
	ULONG& uIndexOutNext,	// O: the u index next to uIndexOut (the inner side of side surface)
	ULONG& vIndexOutNext,	// O: the v index next to uIndexOut (the inner side of side surface)
	bool& onLowerCorner		// O: whether is on the lower corner of the leftRight surface
)
{
	// When the femur patches was merged into 3 surfaces, actualy we did not control 
	// the directions of parametric u and v. Therefore, we need to do it differently
	// when find the corresponding index on the side surface.
	if ( uCtrlCountMain > vCtrlCountMain )
		return CFemoralPart::FindCorrespondentIndexOnSideSurface_LongerU(uIndexMain,vIndexMain,uCtrlCountMain,vCtrlCountMain,uCtrlCountLeft,vCtrlCountLeft,uCtrlCountRight,vCtrlCountRight,leftRight,uIndexOut,vIndexOut,uIndexOutNext,vIndexOutNext,onLowerCorner);
	else
		return CFemoralPart::FindCorrespondentIndexOnSideSurface_LongerV(uIndexMain,vIndexMain,uCtrlCountMain,vCtrlCountMain,uCtrlCountLeft,vCtrlCountLeft,uCtrlCountRight,vCtrlCountRight,leftRight,uIndexOut,vIndexOut,uIndexOutNext,vIndexOutNext,onLowerCorner);
}

int CFemoralPart::FindCorrespondentIndexOnSideSurface_LongerU
(
	ULONG& uIndexMain,		// I: the u index of a control point in main surface
	ULONG& vIndexMain,		// I: the v index of a control point in main surface
	ULONG& uCtrlCountMain,  // I: the total number of control point along u direction in main surface
	ULONG& vCtrlCountMain,	// I: the total number of control point along v direction in main surface
	ULONG& uCtrlCountLeft,  // I: the total number of control point along u direction in left surface
	ULONG& vCtrlCountLeft,	// I: the total number of control point along v direction in left surface
	ULONG& uCtrlCountRight, // I: the total number of control point along u direction in right surface
	ULONG& vCtrlCountRight, // I: the total number of control point along v direction in right surface
	int& leftRight,			// O: 0=left surface, 1=right surface, -1=no found
	ULONG& uIndexOut,		// O: the u index on the corresponding leftRight surface
	ULONG& vIndexOut,		// O: the v index on the corresponding leftRight surface
	ULONG& uIndexOutNext,	// O: the u index next to uIndexOut (the inner side of side surface)
	ULONG& vIndexOutNext,	// O: the v index next to uIndexOut (the inner side of side surface)
	bool& onLowerCorner		// O: whether is on the lower corner of the leftRight surface
)
{
	// initialize data
	leftRight = -1;
	onLowerCorner = false;
	// Some basic checkings
	// Whether the control points on main surface and left/right surfaces are consistent
	ULONG uLength = 2*uCtrlCountLeft+vCtrlCountLeft-2;
	if ( uLength != uCtrlCountMain )
		return leftRight;
	uLength = 2*uCtrlCountRight+vCtrlCountRight-2;
	if ( uLength != uCtrlCountMain )
		return leftRight;
	// Do the inputs make sense
	if ( uIndexMain < 0 || uIndexMain > (uCtrlCountMain-1) )
		return leftRight;
	if ( vIndexMain < 0 || vIndexMain > (vCtrlCountMain-1) )
		return leftRight;
	if ( vIndexMain != 0 && vIndexMain != (vCtrlCountMain-1) )// has to be on boundary
		return leftRight;

	// Whether on left or right surface
	if ( vIndexMain == 0 ) 
	{
		leftRight = 1; // right surface
		if ( uIndexMain < (uCtrlCountRight-1) )// front side boundary
		{
			uIndexOut = uIndexMain;
			vIndexOut = vCtrlCountLeft-1;
			uIndexOutNext = uIndexMain;
			vIndexOutNext = vCtrlCountLeft-1-1;//
		}
		else if ( uIndexMain == (uCtrlCountLeft-1) )// corner
		{
			uIndexOut = uCtrlCountLeft-1;
			vIndexOut = vCtrlCountLeft-1;
			uIndexOutNext = uCtrlCountLeft-1-1;//note
			vIndexOutNext = vCtrlCountLeft-1-1;//note
			onLowerCorner = true;
		}
		else if ( uIndexMain > (uCtrlCountLeft-1) &&
			      uIndexMain < (uCtrlCountLeft-1+vCtrlCountLeft-1) )// lower boundary
		{
			uIndexOut = uCtrlCountLeft-1;
			vIndexOut = (vCtrlCountLeft-1) - (uIndexMain - (uCtrlCountLeft-1));
			uIndexOutNext = uCtrlCountLeft-1-1;//
			vIndexOutNext = (vCtrlCountLeft-1) - (uIndexMain - (uCtrlCountLeft-1));
		}
		else if ( uIndexMain == (uCtrlCountLeft-1+vCtrlCountLeft-1) )// corner
		{
			uIndexOut = uCtrlCountLeft-1;
			vIndexOut = 0;
			uIndexOutNext = uCtrlCountLeft-1-1;//
			vIndexOutNext = 0 + 1;//
			onLowerCorner = true;
		}
		else if ( uIndexMain > (uCtrlCountLeft-1+vCtrlCountLeft-1) )// back side boundary
		{
			uIndexOut = (uCtrlCountLeft-1) - (uIndexMain - (uCtrlCountLeft-1+vCtrlCountLeft-1));
			vIndexOut = 0;
			uIndexOutNext = (uCtrlCountLeft-1) - (uIndexMain - (uCtrlCountLeft-1+vCtrlCountLeft-1));
			vIndexOutNext = 0 + 1;//
		}
		else
		{
			leftRight = -1;// Could this be possible?
		}
	}
	else if ( vIndexMain == (vCtrlCountMain-1) ) 
	{
		leftRight = 0; // left surface
		if ( uIndexMain < (uCtrlCountLeft-1) )// front side boundary
		{
			uIndexOut = uIndexMain;
			vIndexOut = 0;
			uIndexOutNext = uIndexMain;
			vIndexOutNext = 0 + 1;//
		}
		else if ( uIndexMain == (uCtrlCountLeft-1) )// corner
		{
			uIndexOut = uCtrlCountLeft-1;
			vIndexOut = 0;
			uIndexOutNext = uCtrlCountLeft-1-1;//
			vIndexOutNext = 0+1;//
			onLowerCorner = true;
		}
		else if ( uIndexMain > (uCtrlCountLeft-1) &&
			      uIndexMain < (uCtrlCountLeft-1+vCtrlCountLeft-1) )// lower boundary
		{
			uIndexOut = uCtrlCountLeft-1;
			vIndexOut = uIndexMain - (uCtrlCountLeft-1);
			uIndexOutNext = uCtrlCountLeft-1-1;//
			vIndexOutNext = uIndexMain - (uCtrlCountLeft-1);
		}
		else if ( uIndexMain == (uCtrlCountLeft-1+vCtrlCountLeft-1) )// corner
		{
			uIndexOut = uCtrlCountLeft-1;
			vIndexOut = vCtrlCountLeft-1;
			uIndexOutNext = uCtrlCountLeft-1-1;//
			vIndexOutNext = vCtrlCountLeft-1-1;//
			onLowerCorner = true;
		}
		else if ( uIndexMain > (uCtrlCountLeft-1+vCtrlCountLeft-1) )// back boundary
		{
			uIndexOut = (uCtrlCountLeft-1) - (uIndexMain - (uCtrlCountLeft-1+vCtrlCountLeft-1));
			vIndexOut = vCtrlCountLeft-1;
			uIndexOutNext = (uCtrlCountLeft-1) - (uIndexMain - (uCtrlCountLeft-1+vCtrlCountLeft-1));
			vIndexOutNext = vCtrlCountLeft-1-1;//
		}
		else
		{
			leftRight = -1;// Could this be possible?
		}
	}

	return leftRight;
}

int CFemoralPart::FindCorrespondentIndexOnSideSurface_LongerV
(
	ULONG& uIndexMain,		// I: the u index of a control point in main surface
	ULONG& vIndexMain,		// I: the v index of a control point in main surface
	ULONG& uCtrlCountMain,  // I: the total number of control point along u direction in main surface
	ULONG& vCtrlCountMain,	// I: the total number of control point along v direction in main surface
	ULONG& uCtrlCountLeft,  // I: the total number of control point along u direction in left surface
	ULONG& vCtrlCountLeft,	// I: the total number of control point along v direction in left surface
	ULONG& uCtrlCountRight, // I: the total number of control point along u direction in right surface
	ULONG& vCtrlCountRight, // I: the total number of control point along v direction in right surface
	int& leftRight,			// O: 0=left surface, 1=right surface, -1=no found
	ULONG& uIndexOut,		// O: the u index on the corresponding leftRight surface
	ULONG& vIndexOut,		// O: the v index on the corresponding leftRight surface
	ULONG& uIndexOutNext,	// O: the u index next to uIndexOut (the inner side of side surface)
	ULONG& vIndexOutNext,	// O: the v index next to uIndexOut (the inner side of side surface)
	bool& onLowerCorner		// O: whether is on the lower corner of the leftRight surface
)
{
	// initialize data
	leftRight = -1;
	onLowerCorner = false;
	// Some basic checkings
	// Whether the control points on main surface and left/right surfaces are consistent
	ULONG bndLength = 2*vCtrlCountLeft+uCtrlCountLeft-2;
	if ( bndLength != vCtrlCountMain )
		return -1;
	bndLength = 2*vCtrlCountRight+uCtrlCountRight-2;
	if ( bndLength != vCtrlCountMain )
		return -1;
	// Do the inputs make sense
	if ( uIndexMain < 0 || uIndexMain > (uCtrlCountMain-1) )
		return -1;
	if ( vIndexMain < 0 || vIndexMain > (vCtrlCountMain-1) )
		return -1;
	if ( uIndexMain != 0 && uIndexMain != (uCtrlCountMain-1) )// has to be on boundary
		return -1;

	// Whether on left or right surface
	if ( uIndexMain == 0 ) 
	{
		leftRight = 0; // left surface
		if ( vIndexMain < (vCtrlCountRight-1) )// anterior side boundary
		{
			vIndexOut = vIndexMain;
			uIndexOut = uCtrlCountLeft-1;
			vIndexOutNext = vIndexMain;
			uIndexOutNext = uCtrlCountLeft-1-1;//
		}
		else if ( vIndexMain == (vCtrlCountLeft-1) )// corner
		{
			vIndexOut = vCtrlCountLeft-1;
			uIndexOut = uCtrlCountLeft-1;
			vIndexOutNext = vCtrlCountLeft-1-1;//note
			uIndexOutNext = uCtrlCountLeft-1-1;//note
			onLowerCorner = true;
		}
		else if ( vIndexMain > (vCtrlCountLeft-1) &&
			      vIndexMain < (vCtrlCountLeft-1+uCtrlCountLeft-1) )// lower boundary
		{
			vIndexOut = vCtrlCountLeft-1;
			uIndexOut = (uCtrlCountLeft-1) - (vIndexMain - (vCtrlCountLeft-1));
			vIndexOutNext = vCtrlCountLeft-1-1;//
			uIndexOutNext = (uCtrlCountLeft-1) - (vIndexMain - (vCtrlCountLeft-1));
		}
		else if ( vIndexMain == (vCtrlCountLeft-1+uCtrlCountLeft-1) )// corner
		{
			vIndexOut = vCtrlCountLeft-1;
			uIndexOut = 0;
			vIndexOutNext = vCtrlCountLeft-1-1;//
			uIndexOutNext = 0 + 1;//
			onLowerCorner = true;
		}
		else if ( vIndexMain > (vCtrlCountLeft-1+uCtrlCountLeft-1) )// posterior side boundary
		{
			vIndexOut = (vCtrlCountLeft-1) - (vIndexMain - (vCtrlCountLeft-1+uCtrlCountLeft-1));
			uIndexOut = 0;
			vIndexOutNext = (vCtrlCountLeft-1) - (vIndexMain - (vCtrlCountLeft-1+uCtrlCountLeft-1));
			uIndexOutNext = 0 + 1;//
		}
		else
		{
			leftRight = -1;// Could this be possible?
		}
	}
	else if ( uIndexMain == (uCtrlCountMain-1) ) 
	{
		leftRight = 1; // right surface
		if ( vIndexMain < (vCtrlCountLeft-1) )// front side boundary
		{
			vIndexOut = vIndexMain;
			uIndexOut = 0;
			vIndexOutNext = vIndexMain;
			uIndexOutNext = 0 + 1;//
		}
		else if ( vIndexMain == (vCtrlCountLeft-1) )// corner
		{
			vIndexOut = vCtrlCountLeft-1;
			uIndexOut = 0;
			vIndexOutNext = vCtrlCountLeft-1-1;//
			uIndexOutNext = 0+1;//
			onLowerCorner = true;
		}
		else if ( vIndexMain > (vCtrlCountLeft-1) &&
			      vIndexMain < (vCtrlCountLeft-1+uCtrlCountLeft-1) )// lower boundary
		{
			vIndexOut = vCtrlCountLeft-1;
			uIndexOut = vIndexMain - (vCtrlCountLeft-1);
			vIndexOutNext = vCtrlCountLeft-1-1;//
			uIndexOutNext = vIndexMain - (vCtrlCountLeft-1);
		}
		else if ( vIndexMain == (vCtrlCountLeft-1+uCtrlCountLeft-1) )// corner
		{
			vIndexOut = vCtrlCountLeft-1;
			uIndexOut = uCtrlCountLeft-1;
			vIndexOutNext = vCtrlCountLeft-1-1;//
			uIndexOutNext = uCtrlCountLeft-1-1;//
			onLowerCorner = true;
		}
		else if ( vIndexMain > (vCtrlCountLeft-1+uCtrlCountLeft-1) )// back boundary
		{
			vIndexOut = (vCtrlCountLeft-1) - (vIndexMain - (vCtrlCountLeft-1+uCtrlCountLeft-1));
			uIndexOut = uCtrlCountLeft-1;
			vIndexOutNext = (vCtrlCountLeft-1) - (vIndexMain - (vCtrlCountLeft-1+uCtrlCountLeft-1));
			uIndexOutNext = uCtrlCountLeft-1-1;//
		}
		else
		{
			leftRight = -1;// Could this be possible?
		}
	}

	return leftRight;
}

IwPoint3d CFemoralPart::GetMostAnteriorPoint()
{
	IwPoint3d mostAntPoint;

	IwBrep* femurBrep = this->GetIwBrep();
	if ( !femurBrep )
		return mostAntPoint; // uninitialized point

	CFemoralAxes* pFemoralAxes = GetTotalDoc()->GetFemoralAxes();
	if ( !pFemoralAxes )
		return mostAntPoint; // uninitialized point

	IwAxis2Placement wslAxes;
	pFemoralAxes->GetWhiteSideLineAxes(wslAxes);
	IwPoint3d farAntPoint = wslAxes.GetOrigin() + 1000*wslAxes.GetZAxis();
	DistFromPointToBrep(farAntPoint, femurBrep, mostAntPoint);

	return mostAntPoint;
}
