#ifndef KFILEMONITOR_H
#define KFILEMONITOR_H

#include <windows.h>
#include <QThread>
#include <QDateTime>

class KFileMonitor : public QThread
{
    Q_OBJECT

public:
    KFileMonitor(QObject *parent=NULL);
    ~KFileMonitor();

    bool Monitor(QString& file);

protected:
    void run();

signals:
    void fileModified();

private:
    QString filePath;
    QDateTime lastModified;
    HANDLE handle; // file change notification
};

#endif // KFILEMONITOR_H
