#pragma once

#include <QString>
#include <QList.h>

class CInfoTextReader
{
public:
	CInfoTextReader( const QString& sFileName );
	~CInfoTextReader();

	bool		IsFileLoaded() {return m_bFileIsLoaded;};
	QString		GetString(const QString& tag);

private:

	QList<QString>	m_tags;
	QList<QString>	m_tagContents;

	bool			m_bFileIsLoaded;
};
