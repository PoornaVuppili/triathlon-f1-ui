#include "Manager.h"
#include "OpenGLView.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"
#include "..\KApp\ViewWidget.h"
#include "..\KTriathlonF1\TotalMainWindow.h"

CManager::CManager( COpenGLView* pView, CManagerActivateType manActType ) 
{
	m_pView			= pView;
	//m_pSketcher		= NULL;

    m_bDestroyMe	= false;

	m_bAltKeyDown	= false;
	m_bCtrlKeyDown	= false;
	m_bShftKeyDown	= false;

	m_bDialogOpen	= false;
	m_bMouseLast	= false;

	m_pView->SetManager(this);

	m_mouseIdleTime = 0;
	m_lastMouseMoveTime = QDateTime::currentDateTime();
	m_beginningTime = m_lastMouseMoveTime;

	//m_toolbar = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;

	m_fontNormal = QFont( "Tahoma", 8, QFont::Normal );
	m_fontBold = QFont( "Tahoma", 8, QFont::Bold );
	m_fontTitle = QFont( "Tahoma", 8, QFont::Black);

	m_sClassName = "";

	m_manActType = manActType;

	//
	m_searchPixelDistance = 5;
	m_mouseMoveCaughtPointIndex = -1;

	// Make JCOSToolbar invisible
	CTotalMainWindow* totalFemur = (CTotalMainWindow*) mainWindow->GetImplant()->GetModule("TotalFemur");
	if ( totalFemur == NULL )
		totalFemur = (CTotalMainWindow*) mainWindow->GetImplant()->GetModule("TotalPS"); // It could be PS 
	if ( totalFemur )
		totalFemur->SetJCOSToolbarVisible(false);
}


CManager::~CManager()
{
	CManager*	pManager = m_pView->PopManagerFromStack();
	m_pView->SetManager(pManager, false);

	if( pManager )
		{
		CCursorType		eCursorType;
		QString			sPrompt;

		pManager->UpdateData();

		pManager->GetCursorAndPrompt( eCursorType, sPrompt );

		m_pView->SetCursorAndPrompt( eCursorType, sPrompt );
		}
	else
		{
		ClearCursorAndPrompt();
		}

    if (m_manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW)
    {
    }

    viewWidget->RemoveManager(this);

	m_pView->Redraw();
}

void CManager::GetCursorAndPrompt( CCursorType& eCursorType, QString& sPrompt )
	{
	eCursorType	= m_eCursorType;
	sPrompt		= m_sPrompt;
	}


void CManager::SetCursorAndPrompt( CCursorType eCursorType, QString sPrompt )
	{
	m_eCursorType	= eCursorType;
	m_sPrompt		= sPrompt;

	m_pView->SetCursorAndPrompt( eCursorType, sPrompt );
	}


void CManager::ClearCursorAndPrompt()
	{
	m_eCursorType	= CURSOR_STANDARD;
	m_sPrompt		= "";

	m_pView->ClearCursorAndPrompt();
	}


void CManager::SetSelectionMode( QString sPrompt, CCursorType eCursorType )
	{
	SetCursorAndPrompt( eCursorType, sPrompt );

	m_pView->DisplayPrompt();
	}


void CManager::SetPrompt( QString sPrompt )
	{
	m_sPrompt = sPrompt;

	m_pView->SetPrompt( m_sPrompt );

	m_pView->DisplayPrompt();
	}


void CManager::SetQuestionMode( QString sPrompt )
	{
	SetCursorAndPrompt( CURSOR_QUESTION, sPrompt );

	m_pView->DisplayPrompt();
	}


void CManager::SetSketchMode( QString sPrompt )
	{
	SetCursorAndPrompt( CURSOR_SKETCH, sPrompt );

	m_pView->DisplayPrompt();
	}


void CManager::EnableAction( QAction* action, bool bEnable )
	{
		if ( action == NULL )
			return;

		action->setEnabled( bEnable );

		if( bEnable )
			action->setFont( m_fontBold );
		else
			action->setFont( m_fontNormal );
	}


void CManager::SetActionAsTitle( QAction* action )
	{
	action->setEnabled( false );

	action->setFont( m_fontTitle );
	}

bool CManager::GetCancelActionExistence()
{
	bool bCancelActionAvailable = false;
	if ( m_toolbar != NULL && m_actCancel != NULL )
	{
		QList<QAction*> actions = m_toolbar->actions();
		QAction* act;
		for ( int i=0; i<actions.size(); i++ )
		{
			act = actions.at(i);
			if ( act == m_actCancel )
			{
				bCancelActionAvailable = true;
				break;
			}
		}
	}

	return bCancelActionAvailable;
}

QString CManager::GetClassName()
	{
	return m_sClassName;
	}

void CManager::ReDraw()
{
	m_pView->Redraw();
}

void CManager::SetMouseMoveSearchingList
(
	IwTArray<IwPoint3d>& searchList,	// I:
	int pixelDist						// I: the pixel distance to define "caught"
)
{
	m_mouseMoveSearchingList.RemoveAll();
	m_mouseMoveSearchingList.Append(searchList);
	m_searchPixelDistance = pixelDist;
}

bool CManager::GetMouseMoveCaughtPoint
(
	IwPoint3d& caughtPoint,		// O:
	int& caughtPointIndex		// O:
)
{
	caughtPointIndex = m_mouseMoveCaughtPointIndex;

	if (m_mouseMoveCaughtPointIndex > -1)
	{
		caughtPoint = m_mouseMoveSearchingList.GetAt(m_mouseMoveCaughtPointIndex);
		return true;
	}

	return false;
}

bool CManager::MouseMove(const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport*)
{

	if (m_mouseMoveSearchingList.GetSize() > 0)
	{
		int maxW = -1000000;
		IwPoint3d pnt;
		int UVW[3];
		int dist;

		m_mouseMoveCaughtPointIndex = -1;
		for (unsigned i=0; i<m_mouseMoveSearchingList.GetSize(); i++)
		{
			pnt = m_mouseMoveSearchingList.GetAt(i);
			m_pView->XYZtoUVW(pnt, UVW);
			dist = (UVW[0] - cursor.x())*(UVW[0] - cursor.x()) + (UVW[1] - cursor.y())*(UVW[1] - cursor.y());
			// Find the points which are within the given pixel distance
			if (dist < m_searchPixelDistance*m_searchPixelDistance)
			{
				if (UVW[2] > maxW) // Find the point which is closer to the viewer.
				{
					maxW = UVW[2];
					m_mouseMoveCaughtPointIndex = i;
				}
			}
		}
	}

    m_posLast = cursor;

	return true;
}

bool CManager::keyPressEvent( QKeyEvent* event, Viewport* vp )
{
	bool continueResponse = true;

	switch (event->key())
	{
		case Qt::Key_N:
			{
				if (m_manActType == MAN_ACT_TYPE_REVIEW)
				{
					continueResponse = !OnReviewNext(); // if OnReviewNext() not do anything, continue response.
				}
			}
			break;

		case Qt::Key_B:
			{
				if (m_manActType == MAN_ACT_TYPE_REVIEW)
				{
					continueResponse = !OnReviewBack();
				}
			}
			break;

		case Qt::Key_R:
			{
				if (m_manActType == MAN_ACT_TYPE_REVIEW)
				{
					continueResponse = !OnReviewRework();
				}
			}
			break;

		default:
			break;

	}

	if ( !continueResponse )
		ReDraw();

	return continueResponse;
}

////////////////////////////////////////////////////////////
// Caller needs to set point size, color, lighting, ...
///////////////////////////////////////////////////////////
bool CManager::DisplayCaughtPoint(IwVector3d offset)
{
	if (m_mouseMoveCaughtPointIndex > -1)
	{
		IwPoint3d pnt;

		pnt = m_mouseMoveSearchingList.GetAt(m_mouseMoveCaughtPointIndex);

		// if offset is not given, display the caught point on the first layer (the closest layer) of view port.
		if ( offset == IwVector3d(0,0,0) )
			offset = m_pView->GetLayerOffsetVector(1, pnt);

		pnt = pnt + offset; // move closer to the viewer, therefore, to block the point at the same location, if any

		glBegin( GL_POINTS );
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();

		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////
// If movingVec is given, the caught point can only move along the 
// movingVec.
// If step != 0, the caught point can only move with n*step from the 
// base point.
/////////////////////////////////////////////////////////////////////
void CManager::MoveCaughtPoint
(
	const QPoint& cursor, 
	IwVector3d& movingVec
)
{
	IwPoint3d caughtPoint;
	int caughtPointIndex;
	if ( !GetMouseMoveCaughtPoint(caughtPoint, caughtPointIndex) ) return;

	IwPoint3d curPnt, lastPnt;
	m_pView->UVtoXYZ(cursor, curPnt);
	m_pView->UVtoXYZ(m_posLast, lastPnt);

	if ( movingVec == IwVector3d(0,0,0) )
	{
		// Move along the pixel plane
		caughtPoint += (curPnt - lastPnt);
	}
	else
	{
		// Move along the given vector
		movingVec.Unitize();
		IwVector3d vec = (curPnt - lastPnt);
		double dot = movingVec.Dot(vec);
		caughtPoint += dot*movingVec;
	}

	// set back to the search list
	m_mouseMoveSearchingList.SetAt(caughtPointIndex, caughtPoint);
}

////////////////////////////////////////////////
// Mouse moves without any button down.
////////////////////////////////////////////////
bool CManager::MouseNoneMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp ) 
{ 
	// calculate idle time and update current time
	int idleSeconds = m_lastMouseMoveTime.secsTo(QDateTime::currentDateTime());
	if ( idleSeconds > 60 )
		m_mouseIdleTime += idleSeconds;
	m_lastMouseMoveTime = QDateTime::currentDateTime();

	return MouseMove(cursor, keyModifier, vp); 
}

//////////////////////////////////////////////////////////////
// The time (seconds) from activating this manager to the 
// current, and substract the idle time.
//////////////////////////////////////////////////////////////
int CManager::GetElapseTime()
{
	int totalSeconds = m_beginningTime.secsTo(QDateTime::currentDateTime());
	int elapseSeconds = totalSeconds - m_mouseIdleTime;
	if ( elapseSeconds < 0 )
		elapseSeconds = 0;

	return elapseSeconds;
}