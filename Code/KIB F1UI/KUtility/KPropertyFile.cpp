///////////////////////////////////////////////////////////////////////////////////////////
//  Filename:	KPropertyFile.cpp
//	Purpose:	Class used to read and parse a property
//				file.  It also stores the values for retrieval in a hashtable.
//				The file format is key=value each on a seperate line
//	Author:		Travis Odegaard
//				Stryker
//	Date:		12/01/04
//	Version:	$Id: Propertyfile.cpp,v 1.2 2007/09/14 00:57:54 costast Exp $
///////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <QTextStream>
#include <QString>
#include <windows.h>

#include "KPropertyfile.h"

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		PropertyFile
//	Function purpose:	constructor - takes the file path as a parameter
///////////////////////////////////////////////////////////////////////////////////////////
KPropertyFile::KPropertyFile(QString absFilePath)
{
	filePath = absFilePath;
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		loadFile
//	Function purpose:	This method opens the file and stores the key value pairs in a 
//						hashtable.
///////////////////////////////////////////////////////////////////////////////////////////
bool KPropertyFile::loadFile()
{
	//property file
	QFile file(filePath);
    QString line;

	//reads the file 
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) 
	{
        QTextStream stream(&file);
        int i = 1;
        while (!stream.atEnd()) 
		{
            line = stream.readLine(); // line of text excluding '\n'

			//parse the line and store it in the dictionary
			int pos;
			QString key, value;
			line = line.trimmed();
			pos = line.indexOf("=", 0);
			if (pos == -1)
				continue;
			key = line.section("=", 0, 0);
			value = line.section("=", 1);

            dict.remove(key);
            dict.insert(key, value);
		}
        file.close();
    }
	else
	{
		return false;
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		loadFile
//	Function purpose:	This method writes out the hashtable to a file.
///////////////////////////////////////////////////////////////////////////////////////////
void KPropertyFile::saveFile()
{
	QFile file(filePath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) 
	{
        QTextStream stream(&file);
        QHashIterator<QString,QString> it(dict);
        while( it.hasNext() )
        {
            it.next();
            stream << it.key() << "=" << it.value() << endl;
        }
    }
	file.close();
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		loadFile
//	Function purpose:	This method returns the value associated with the given key.
///////////////////////////////////////////////////////////////////////////////////////////
QString KPropertyFile::getValue(const QString& key)
{
    QHash<QString,QString>::iterator it = dict.find(key);
    if( it != dict.end() )
        return it.value();
    else
        return "";
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		insertValue
//	Function purpose:	This method inserts the key value pair into the hashtable.
///////////////////////////////////////////////////////////////////////////////////////////
void KPropertyFile::insertValue(const QString& key, const QString& value)
{
	if (! value.isEmpty())
    {
        dict.remove(key);
        dict.insert(key, value);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		removeValue
//	Function purpose:	This method removes the key value from the hashtable.
///////////////////////////////////////////////////////////////////////////////////////////
void KPropertyFile::removeValue(const QString& key)
{
    QHash<QString,QString>::iterator it = dict.find(key);
    if( it != dict.end() )
        dict.remove(key);
}

///////////////////////////////////////////////////////////////////////////////////////////
//  Function name:		size
//	Function purpose:	This method returns the number of itmes in the hashtable.
///////////////////////////////////////////////////////////////////////////////////////////
int KPropertyFile::size()
{
	return dict.count();
}


//////////////////////////////////////////
// Convenience functions
//////////////////////////////////////////

bool KUpdateImplantInfoTxt(QString path, const QList<QPair<QString, QString> >& info)
{
    KPropertyFile pf(path);

    for(int i=0; i<info.size(); i++)
    {
        const QPair<QString, QString>& pair = info.at(i);
        pf.insertValue( pair.first, pair.second );
    }

	pf.saveFile();

    return true;
}
