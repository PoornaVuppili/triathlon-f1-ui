#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

class TEST_EXPORT_TW COutputResultsJigs : public CManager
{
    Q_OBJECT

public:
					COutputResultsJigs( CTotalView* pView );
	virtual			~COutputResultsJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

	virtual void	Display(Viewport* vp=NULL);


private:

private slots:
	void			OnOutputResults();
	void			OnOutputAuxiliaryEntities();
	void			OnAccept();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actOutput;
	QAction*					m_actOutputResults;
	QAction*					m_actOutputAuxiliaryEntities;

    friend class OutputterOfResultsJigs; // grant access to mgr's actions
};


class OutputterOfResultsJigs
{
public:
    OutputterOfResultsJigs(CTotalDoc* pDoc);

    bool DoValidate(QString const& folder, QString &msg);
    bool DoOutput(QString const& folder);

    void OutputSolidPositionJigs(const QString& outputFolder);
    void OutputStylusSurfaceJigs(const QString& outputFolder);
    void OutputAnteriorSurfaceJigs(const QString& outputFolder);
    void OutputOsteophyteSurfaceJigs(const QString& outputFolder);
    void OutputCartilageSurfaceJigs(const QString& outputFolder);
    void OutputOuterSurfaceJigs(const QString& outputFolder);
    void OutputOuterInsetSurfaceJigs(const QString& outputFolder);
    void OutputInnerSurfaceJigs(const QString& outputFolder);
    void OutputSideSurfaceJigsUnTrimmed(const QString& outputFolder);
    void OutputXMLInfo(const QString& outputFolder);

private:
    CTotalDoc* m_pDoc;
};

