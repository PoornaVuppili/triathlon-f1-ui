#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW CAnteriorSurfaceJigs : public CPart
{
public:
	CAnteriorSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_ANTERIOR_SURFACE_JIGS, const QString& sFileName="", int nEntId=-1 );
	~CAnteriorSurfaceJigs();

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );
	void			SetLateralPeakPoint(IwPoint3d& peakPoint);
	IwPoint3d		GetLateralPeakPoint();
	void			SetOffsetOsteophyteBrep(IwBrep*& osteoBrep);
	void			GetOffsetOsteophyteBrep(IwBrep*& osteoBrep);
	void			SetOffsetCartilageBrep(IwBrep*& cartiBrep);
	void			GetOffsetCartilageBrep(IwBrep*& cartiBrep);
	IwBrep*			SolidifyAnteriorSurfaceForOutput_Trial(int tolLevel, bool finalTrial);
	IwBrep*			SolidifyAnteriorSurfaceForOutput();

protected:
	void			DisplayASJigsPoints();

private:
	void			SetASJigsModifiedFlag( bool bModified );
	long			m_glAnteriorSurfaceList;

private:
	bool			m_bASJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bASJigsDataModified;// Geometric entities have not been saved yet to the file. 

	IwPoint3d		m_lateralPeakPoint;

};