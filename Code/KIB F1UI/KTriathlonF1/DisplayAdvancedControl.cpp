#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "DisplayAdvancedControl.h"
#include "AdvancedControl.h"
#include "AdvancedControlDlg.h"
#include "FemoralCuts.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Utilities.h"

CDisplayAdvancedControl::CDisplayAdvancedControl( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CDisplayAdvancedControl";

	m_toolbar = new QToolBar("Advanced Control");

	CAdvancedControl* advCtrl = m_pDoc->GetAdvancedControl();
	if (advCtrl == NULL) 
	{
		advCtrl = new CAdvancedControl( m_pDoc );
		m_pDoc->AddEntity( advCtrl, false ); // Do not display in Entity Panel
	}

	m_actAdvancedControl = m_toolbar->addAction("AdvCtrl");
	SetActionAsTitle( m_actAdvancedControl );
	m_actDispDlg = m_toolbar->addAction("Disp Dialog");
	EnableAction( m_actDispDlg, true );
	m_actAccept = m_toolbar->addAction("OK");
	EnableAction( m_actAccept, true );

	m_pMainWindow->connect( m_actDispDlg, SIGNAL( triggered() ), this, SLOT( OnDispDlg() ) );
	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	OnDispDlg(advCtrl);

}

CDisplayAdvancedControl::~CDisplayAdvancedControl()
{
}

void CDisplayAdvancedControl::OnDispDlg(CAdvancedControl* advCtrl)
{
	bool underDev = false;
	char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (underDevelopment != NULL && QString("1") == underDevelopment)
		underDev = true;

	// Create Advanced Control object and dialog here
	int forCRPS=0;
	if (m_pDoc->isPosteriorStabilized())//PS
		forCRPS = 2;
	else // CR
		forCRPS = 1;

	CAdvancedControlDlg* pDlg = new CAdvancedControlDlg( m_pDoc->GetView()->GetViewWidget(), 0, forCRPS );
	pDlg->setWindowTitle(QString("Advanced Control"));
	if ( underDev )
		pDlg->resize(500, 500);
	else
		pDlg->resize(500, 400);
	pDlg->setModal(true);
	
	// Map the Advanced Control object data to Dialog
	// Auto JOC Steps
	bool oldAutoJOCSteps = advCtrl->GetAutoJOCSteps();
	pDlg->SetAutoJOCSteps(oldAutoJOCSteps);
	bool oldAutoMajorSteps = advCtrl->GetAutoMajorSteps();
	pDlg->SetAutoMajorSteps(oldAutoMajorSteps);
	bool oldAutoAllSteps = advCtrl->GetAutoAllSteps();
	pDlg->SetAutoAllSteps(oldAutoAllSteps);
	int oldAutoAdditionalIterations = advCtrl->GetAutoAdditionalIterations();
	pDlg->SetAutoAdditionalIterations(oldAutoAdditionalIterations);
	// Femoral Axes
	bool oldFemoralAxesEnableRefine = advCtrl->GetFemoralAxesEnableRefine();
	double oldFemoralImplantCoronalRadius = advCtrl->GetFemoralImplantCoronalRadius();
	double oldTrochlearGrooveRadius = advCtrl->GetTrochlearGrooveRadius();
	bool oldFemoralAxesDisplayMechAxes = advCtrl->GetFemoralAxesDisplayMechAxes();
	pDlg->SetFemoralAxesEnableRefine(oldFemoralAxesEnableRefine);
	pDlg->SetFemoralImplantCoronalRadius(oldFemoralImplantCoronalRadius);
	pDlg->SetTrochlearGrooveRadius(oldTrochlearGrooveRadius);
	pDlg->SetFemoralAxesDisplayMechAxes(oldFemoralAxesDisplayMechAxes);

	// Femoral Cuts
	bool oldFemoralCutsEnableReset = advCtrl->GetFemoralCutsEnableReset();
	pDlg->SetFemoralCutsEnableReset(oldFemoralCutsEnableReset);
	bool oldFemoralCutsOnlyMoveNormal = advCtrl->GetFemoralCutsOnlyMoveNormal();
	pDlg->SetFemoralCutsOnlyMoveNormal(oldFemoralCutsOnlyMoveNormal);
	double oldViewProfileOffsetDistance = advCtrl->GetViewProfileOffsetDistance();
	pDlg->SetViewProfileOffsetDistance(oldViewProfileOffsetDistance);
	double oldCutLineDisplayLength = advCtrl->GetCutLineDisplayLength();
	pDlg->SetCutLineDisplayLength(oldCutLineDisplayLength);
	bool oldAchieveDesiredPostCuts = advCtrl->GetAchieveDesiredPostCuts();
	pDlg->SetAchieveDesiredPostCuts(oldAchieveDesiredPostCuts);

	double oldCutsExtraAllowance = advCtrl->GetCutsExtraAllowance();
	pDlg->SetCutsExtraAllowance(oldCutsExtraAllowance);


	// Outline Profile
	bool oldOutlineProfileEnableReset = advCtrl->GetOutlineProfileEnableReset();
	pDlg->SetOutlineProfileEnableReset(oldOutlineProfileEnableReset);
	double oldOutlineProfilePosteriorCutFilterRadiusRatio = advCtrl->GetOutlineProfilePosteriorCutFilterRadiusRatio();
	pDlg->SetOutlineProfilePosteriorCutFilterRadiusRatio(oldOutlineProfilePosteriorCutFilterRadiusRatio);
	double oldOutlineProfileSketchSurfaceAntExtension = advCtrl->GetOutlineProfileSketchSurfaceAntExtension();
	pDlg->SetOutlineProfileSketchSurfaceAntExtension(oldOutlineProfileSketchSurfaceAntExtension);
	double oldOutlineProfileSketchSurfacePostExtension = advCtrl->GetOutlineProfileSketchSurfacePostExtension();
	pDlg->SetOutlineProfileSketchSurfacePostExtension(oldOutlineProfileSketchSurfacePostExtension);
	double oldOutlineProfileAntAirBallDistance = advCtrl->GetOutlineProfileAntAirBallDistance();
	pDlg->SetOutlineProfileAntAirBallDistance(oldOutlineProfileAntAirBallDistance);

	//// OutputResults
	bool oldOutputNormalized = advCtrl->GetOutputNormalized();
	pDlg->SetOutputNormalized(oldOutputNormalized);
	bool oldImportFromNormalized = advCtrl->GetImportFromNormalized();
	pDlg->SetImportFromNormalized(oldImportFromNormalized);
	bool oldOutputWithTransformation = advCtrl->GetOutputWithTransformation();
	pDlg->SetOutputWithTransformation(oldOutputWithTransformation);
	double oldImportRotationX = advCtrl->GetImportRotationXYZ().x;
	pDlg->SetImportRotationX(oldImportRotationX);
	double oldImportRotationY = advCtrl->GetImportRotationXYZ().y;
	pDlg->SetImportRotationY(oldImportRotationY);
	double oldImportRotationZ = advCtrl->GetImportRotationXYZ().z;
	pDlg->SetImportRotationZ(oldImportRotationZ);
	double oldImportTranslationX = advCtrl->GetImportTranslationXYZ().x;
	pDlg->SetImportTranslationX(oldImportTranslationX);
	double oldImportTranslationY = advCtrl->GetImportTranslationXYZ().y;
	pDlg->SetImportTranslationY(oldImportTranslationY);
	double oldImportTranslationZ = advCtrl->GetImportTranslationXYZ().z;
	pDlg->SetImportTranslationZ(oldImportTranslationZ);

	double oldImportTransformationXx = advCtrl->GetImportTransformationX().x;
	pDlg->SetImportTransformationXx(oldImportTransformationXx);
	double oldImportTransformationXy = advCtrl->GetImportTransformationX().y;
	pDlg->SetImportTransformationXy(oldImportTransformationXy);
	double oldImportTransformationXz = advCtrl->GetImportTransformationX().z;
	pDlg->SetImportTransformationXz(oldImportTransformationXz);

	double oldImportTransformationYx = advCtrl->GetImportTransformationY().x;
	pDlg->SetImportTransformationYx(oldImportTransformationYx);
	double oldImportTransformationYy = advCtrl->GetImportTransformationY().y;
	pDlg->SetImportTransformationYy(oldImportTransformationYy);
	double oldImportTransformationYz = advCtrl->GetImportTransformationY().z;
	pDlg->SetImportTransformationYz(oldImportTransformationYz);

	double oldImportTransformationTx = advCtrl->GetImportTransformationT().x;
	pDlg->SetImportTransformationTx(oldImportTransformationTx);
	double oldImportTransformationTy = advCtrl->GetImportTransformationT().y;
	pDlg->SetImportTransformationTy(oldImportTransformationTy);
	double oldImportTransformationTz = advCtrl->GetImportTransformationT().z;
	pDlg->SetImportTransformationTz(oldImportTransformationTz);


	if ( pDlg->exec() == QDialog::Accepted )
	{
		if ( oldOutputNormalized != pDlg->GetOutputNormalized() ) // compare boolean
		{
			advCtrl->SetOutputNormalized(pDlg->GetOutputNormalized());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutputNormalized: %1, newOutputNormalized: %2 ").arg(oldOutputNormalized).arg(pDlg->GetOutputNormalized()) );
		}
		if ( oldImportFromNormalized != pDlg->GetImportFromNormalized() ) // compare boolean
		{
			advCtrl->SetImportFromNormalized(pDlg->GetImportFromNormalized());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldImportFromNormalized: %1, newImportFromNormalized: %2 ").arg(oldImportFromNormalized).arg(pDlg->GetImportFromNormalized()) );
		}
		if ( oldOutputWithTransformation != pDlg->GetOutputWithTransformation() ) // compare boolean
		{
			advCtrl->SetOutputWithTransformation(pDlg->GetOutputWithTransformation());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutputWithTransformation: %1, newOutputWithTransformation: %2 ").arg(oldOutputWithTransformation).arg(pDlg->GetOutputWithTransformation()) );
		}
		
		if ( !IS_EQ_TOL6(oldImportRotationX,pDlg->GetImportRotationX()) ||
			 !IS_EQ_TOL6(oldImportRotationY,pDlg->GetImportRotationY()) ||
			 !IS_EQ_TOL6(oldImportRotationZ,pDlg->GetImportRotationZ()) ) // compare double
		{
			IwVector3d vec = IwVector3d(pDlg->GetImportRotationX(),pDlg->GetImportRotationY(),pDlg->GetImportRotationZ());
			advCtrl->SetImportRotationXYZ(vec);
			//m_pDoc->AppendLog( );
		}
		if ( !IS_EQ_TOL6(oldImportTranslationX,pDlg->GetImportTranslationX()) ||
			 !IS_EQ_TOL6(oldImportTranslationY,pDlg->GetImportTranslationY()) ||
			 !IS_EQ_TOL6(oldImportTranslationZ,pDlg->GetImportTranslationZ()) ) // compare double
		{
			IwVector3d vec = IwVector3d(pDlg->GetImportTranslationX(),pDlg->GetImportTranslationY(),pDlg->GetImportTranslationZ());
			advCtrl->SetImportTranslationXYZ(vec);
			//m_pDoc->AppendLog( );
		}

		if ( !IS_EQ_TOL6(oldImportTransformationXx,pDlg->GetImportTransformationXx()) ||
			 !IS_EQ_TOL6(oldImportTransformationXy,pDlg->GetImportTransformationXy()) ||
			 !IS_EQ_TOL6(oldImportTransformationXz,pDlg->GetImportTransformationXz()) ) // compare double
		{
			IwVector3d vec = IwVector3d(pDlg->GetImportTransformationXx(),pDlg->GetImportTransformationXy(),pDlg->GetImportTransformationXz());
			advCtrl->SetImportTransformationX(vec);
			//m_pDoc->AppendLog( );
		}

		if ( !IS_EQ_TOL6(oldImportTransformationYx,pDlg->GetImportTransformationYx()) ||
			 !IS_EQ_TOL6(oldImportTransformationYy,pDlg->GetImportTransformationYy()) ||
			 !IS_EQ_TOL6(oldImportTransformationYz,pDlg->GetImportTransformationYz()) ) // compare double
		{
			IwVector3d vec = IwVector3d(pDlg->GetImportTransformationYx(),pDlg->GetImportTransformationYy(),pDlg->GetImportTransformationYz());
			advCtrl->SetImportTransformationY(vec);
			//m_pDoc->AppendLog( );
		}

		if ( !IS_EQ_TOL6(oldImportTransformationTx,pDlg->GetImportTransformationTx()) ||
			 !IS_EQ_TOL6(oldImportTransformationTy,pDlg->GetImportTransformationTy()) ||
			 !IS_EQ_TOL6(oldImportTransformationTz,pDlg->GetImportTransformationTz()) ) // compare double
		{
			IwVector3d vec = IwVector3d(pDlg->GetImportTransformationTx(),pDlg->GetImportTransformationTy(),pDlg->GetImportTransformationTz());
			advCtrl->SetImportTransformationT(vec);
			//m_pDoc->AppendLog( );
		}
	
		// Femoral Cuts
		bool FemoralCutsModified = false;
		if ( oldFemoralCutsEnableReset != pDlg->GetFemoralCutsEnableReset() )
		{
			advCtrl->SetFemoralCutsEnableReset(pDlg->GetFemoralCutsEnableReset());
			// Notice , do not need to update femoral cuts
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldFemoralCutsEnableReset: %1, newFemoralCutsEnableReset: %2 ").arg(oldFemoralCutsEnableReset).arg(pDlg->GetFemoralCutsEnableReset()) );
		}
		if ( oldFemoralCutsOnlyMoveNormal != pDlg->GetFemoralCutsOnlyMoveNormal() )
		{
			advCtrl->SetFemoralCutsOnlyMoveNormal(pDlg->GetFemoralCutsOnlyMoveNormal());
			// Notice , do not need to update femoral cuts
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldFemoralCutsOnlyMoveNormal: %1, newFemoralCutsOnlyMoveNormal: %2 ").arg(oldFemoralCutsOnlyMoveNormal).arg(pDlg->GetFemoralCutsOnlyMoveNormal()) );
		}
		if (oldViewProfileOffsetDistance != pDlg->GetViewProfileOffsetDistance())
		{
			advCtrl->SetViewProfileOffsetDistance(pDlg->GetViewProfileOffsetDistance());
		}
		if ( !IS_EQ_TOL6(oldCutLineDisplayLength, pDlg->GetCutLineDisplayLength()) )
		{
			advCtrl->SetCutLineDisplayLength(pDlg->GetCutLineDisplayLength());
			// ****** NOTICE ******
			// ****** CutLineDisplayLength is just for display purpose, do not need to update the status sign "*".
			// FemoralCutsModified = true;
		}
		
		if ( oldAchieveDesiredPostCuts != pDlg->GetAchieveDesiredPostCuts() )
		{
			advCtrl->SetAchieveDesiredPostCuts(pDlg->GetAchieveDesiredPostCuts());
			// No need to update femoral cuts
			//FemoralCutsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAchieveDesiredPostCuts: %1, newAchieveDesiredPostCuts: %2 ").arg(oldAchieveDesiredPostCuts).arg(pDlg->GetAchieveDesiredPostCuts()) );
		}
		
		if ( !IS_EQ_TOL6(oldCutsExtraAllowance, pDlg->GetCutsExtraAllowance()) )
		{
			advCtrl->SetCutsExtraAllowance(pDlg->GetCutsExtraAllowance());
			FemoralCutsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldCutsExtraAllowance: %1, newCutsExtraAllowance: %2 ").arg(oldCutsExtraAllowance).arg(pDlg->GetCutsExtraAllowance()) );
		}
		if ( FemoralCutsModified )
		{
			if ( m_pDoc->GetFemoralCuts() )
				m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_CUTS, false, false);// Therefore, femoral cuts will also show modified "*" mark.
		}
				
		// Outline Profile
		bool OutlineProfileModified = false;
		if ( oldOutlineProfileEnableReset != pDlg->GetOutlineProfileEnableReset() )
		{
			advCtrl->SetOutlineProfileEnableReset(pDlg->GetOutlineProfileEnableReset());
			// Notice , do not need to update outline profile
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutlineProfileEnableReset: %1, newOutlineProfileEnableReset: %2 ").arg(oldOutlineProfileEnableReset).arg(pDlg->GetOutlineProfileEnableReset()) );
		}
		if ( !IS_EQ_TOL6(oldOutlineProfilePosteriorCutFilterRadiusRatio, pDlg->GetOutlineProfilePosteriorCutFilterRadiusRatio()) )
		{
			advCtrl->SetOutlineProfilePosteriorCutFilterRadiusRatio(pDlg->GetOutlineProfilePosteriorCutFilterRadiusRatio());
			// Notice , do not need to update outline profile
			// OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutlineProfilePosteriorCutFilterRadiusRatio: %1, newOutlineProfilePosteriorCutFilterRadiusRatio: %2 ").arg(oldOutlineProfilePosteriorCutFilterRadiusRatio).arg(pDlg->GetOutlineProfilePosteriorCutFilterRadiusRatio()) );
		}
		if ( !IS_EQ_TOL6(oldOutlineProfileSketchSurfaceAntExtension, pDlg->GetOutlineProfileSketchSurfaceAntExtension()) )
		{
			advCtrl->SetOutlineProfileSketchSurfaceAntExtension(pDlg->GetOutlineProfileSketchSurfaceAntExtension());
			OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutlineProfileSketchSurfaceAntExtension: %1, newOutlineProfileSketchSurfaceAntExtension: %2 ").arg(oldOutlineProfileSketchSurfaceAntExtension).arg(pDlg->GetOutlineProfileSketchSurfaceAntExtension()) );
		}
		if ( !IS_EQ_TOL6(oldOutlineProfileSketchSurfacePostExtension, pDlg->GetOutlineProfileSketchSurfacePostExtension()) )
		{
			advCtrl->SetOutlineProfileSketchSurfacePostExtension(pDlg->GetOutlineProfileSketchSurfacePostExtension());
			OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutlineProfileSketchSurfacePostExtension: %1, newOutlineProfileSketchSurfacePostExtension: %2 ").arg(oldOutlineProfileSketchSurfacePostExtension).arg(pDlg->GetOutlineProfileSketchSurfacePostExtension()) );
		}
		if ( !IS_EQ_TOL6(oldOutlineProfileAntAirBallDistance, pDlg->GetOutlineProfileAntAirBallDistance()) )
		{
			advCtrl->SetOutlineProfileAntAirBallDistance(pDlg->GetOutlineProfileAntAirBallDistance());
			//OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldOutlineProfileAntAirBallDistance: %1, newOutlineProfileAntAirBallDistance: %2 ").arg(oldOutlineProfileAntAirBallDistance).arg(pDlg->GetOutlineProfileAntAirBallDistance()) );
		}
		if ( OutlineProfileModified )
		{
			if ( m_pDoc->GetOutlineProfile() )
				m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_OUTLINE_PROFILE, false, false);// Therefore, outline profile will also show modified "*" mark.
		}

		// Femoral Axes
		bool FemoralAxesModified = false;
		if ( oldFemoralAxesEnableRefine != pDlg->GetFemoralAxesEnableRefine() )
		{
			advCtrl->SetFemoralAxesEnableRefine(pDlg->GetFemoralAxesEnableRefine());
			// Notice , do not need to update femoral axes
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldFemoralAxesEnableRefine: %1, newFemoralAxesEnableRefine: %2 ").arg(oldFemoralAxesEnableRefine).arg(pDlg->GetFemoralAxesEnableRefine()) );
		}
		if ( !IS_EQ_TOL6(oldFemoralImplantCoronalRadius, pDlg->GetFemoralImplantCoronalRadius()) )
		{
			advCtrl->SetFemoralImplantCoronalRadius(pDlg->GetFemoralImplantCoronalRadius());
			FemoralAxesModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldFemoralImplantCoronalRadius: %1, newFemoralImplantCoronalRadius: %2 ").arg(oldFemoralImplantCoronalRadius).arg(pDlg->GetFemoralImplantCoronalRadius()) );
		}
		if ( !IS_EQ_TOL6(oldTrochlearGrooveRadius, pDlg->GetTrochlearGrooveRadius()) )
		{
			advCtrl->SetTrochlearGrooveRadius(pDlg->GetTrochlearGrooveRadius());
			FemoralAxesModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldTrochlearGrooveRadius: %1, newTrochlearGrooveRadius: %2 ").arg(oldTrochlearGrooveRadius).arg(pDlg->GetTrochlearGrooveRadius()) );
		}
		if ( oldFemoralAxesDisplayMechAxes != pDlg->GetFemoralAxesDisplayMechAxes() )
		{
			advCtrl->SetFemoralAxesDisplayMechAxes(pDlg->GetFemoralAxesDisplayMechAxes());
			// Notice , do not need to update femoral axes
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldFemoralAxesDisplayMechAxes: %1, newFemoralAxesDisplayMechAxes: %2 ").arg(oldFemoralAxesDisplayMechAxes).arg(pDlg->GetFemoralAxesDisplayMechAxes()) );
		}
		if (FemoralAxesModified)
		{
			if ( m_pDoc->GetFemoralAxes() )
				m_pDoc->UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false, false);// Therefore, femoral axes will also show modified "*" mark.
		}
		// Auto JOC Steps
		bool autoStepsModified = false;
		if ( oldAutoJOCSteps != pDlg->GetAutoJOCSteps() ) // compare boolean
		{
			advCtrl->SetAutoJOCSteps(pDlg->GetAutoJOCSteps());
			autoStepsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAutoJOCSteps: %1, newAutoJOCSteps: %2 ").arg(oldAutoJOCSteps).arg(pDlg->GetAutoJOCSteps()) );
		}
		if ( oldAutoMajorSteps != pDlg->GetAutoMajorSteps() ) // compare boolean
		{
			advCtrl->SetAutoMajorSteps(pDlg->GetAutoMajorSteps());
			autoStepsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAutoMajorSteps: %1, newAutoMajorSteps: %2 ").arg(oldAutoMajorSteps).arg(pDlg->GetAutoMajorSteps()) );
		}
		if ( oldAutoAllSteps != pDlg->GetAutoAllSteps() ) // compare boolean
		{
			advCtrl->SetAutoAllSteps(pDlg->GetAutoAllSteps());
			autoStepsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAutoAllSteps: %1, newAutoAllSteps: %2 ").arg(oldAutoAllSteps).arg(pDlg->GetAutoAllSteps()) );
		}
		if ( oldAutoAdditionalIterations != pDlg->GetAutoAdditionalIterations() ) // compare integer
		{
			advCtrl->SetAutoAdditionalIterations(pDlg->GetAutoAdditionalIterations());
			autoStepsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAutoAdditionalIterations: %1, newAutoAdditionalIterations: %2 ").arg(oldAutoAdditionalIterations).arg(pDlg->GetAutoAdditionalIterations()) );
		}
		if ( autoStepsModified )
		{
			GetView()->GetMainWindow()->EnableActions(true);
		}

	}

	OnAccept();

}

void CDisplayAdvancedControl::OnAccept()
{
	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	m_bDestroyMe = true;
	m_pView->Redraw();
}
