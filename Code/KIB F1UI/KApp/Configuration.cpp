#include "Configuration.h"

#include "MainWindow.h"

#include <qstring.h>
#include <qfile.h>
#include <qfileinfo.h>
#include <qtextstream.h>
#include <qmessagebox.h>

namespace {

   bool ParseInputLine(const QString& sLine, QString& sTag, QString& sValue,
                       const QString &sep, bool ignoreLeadingWhitespace)
   {
      int idx = sLine.indexOf(sep);
      if (idx > 0)
      {
         sTag = sLine.left(idx);
         sValue = sLine.mid(idx + 1);
         if (ignoreLeadingWhitespace)
         {
            QRegExp rxNonSpaceOrTab("[^ \t]");
            idx = sValue.indexOf(rxNonSpaceOrTab);
            sValue = sValue.mid(idx);
         }
         return true;
      }

      return false;
   }

   bool GetAllPairs(QTextStream& in, const QString &comment, const QString &sep, bool ignoreLeadingWhitespace,
                    Configuration::map_type &theMap)
   {
      // for all lines, read line, split into name and value and add to theMap
      bool atLeastOneLineRead = false;

      // Read through the file.
      while (!in.atEnd())
      {
         QString sLine = in.readLine();
         if (sLine == "" || sLine.startsWith(comment))
            continue;
         QString sTag, sValue;
         if (ParseInputLine(sLine, sTag, sValue, sep, ignoreLeadingWhitespace))
         {
            theMap[sTag] = sValue;
            atLeastOneLineRead = true;
         }
      }

      return atLeastOneLineRead;
   }
}

Configuration::Configuration(QString const &filename, QString const &seprtr, QString const &comment)
   : m_filename(filename)
   , m_separator(seprtr) // the string to be used to split the line into the name and the value
   , m_comment(comment) // the string such that if a line starts with it, ignore that line
{
   if (mainWindow == nullptr)
      throw "Configuration initialized too early - create a MainWindow first";
}

bool Configuration::Load(QString const &versionToCheck, bool clearFirst /*= true*/)
{
   if (clearFirst)
      m_values.clear();

   // try opening the file for reading - return false if an error
   if (!QFileInfo(m_filename).exists())
   {
      QMessageBox::critical(mainWindow, "Error", "The config file does not exist: " + m_filename);
      return false;
   }
   QFile file(m_filename);
   if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
   {
      QMessageBox::critical(mainWindow, "Error", "Failed to open the config file: " + m_filename);
      return false;
   }
   if (!file.isReadable())
   {
      file.close();
      QMessageBox::critical(mainWindow, "Error", "Config file is not readable: " + m_filename);
      return false;
   }

   QByteArray fileContent = file.readAll();
   QTextStream in(&fileContent);

   bool bOK = GetAllPairs(in, m_comment, m_separator, true, m_values);

   if (bOK)
   {
      QString ver;
      bool hasVer = GetValue("CONFIG_VERSION", ver);
      if (hasVer && ver != versionToCheck)
      {
         QMessageBox::critical(mainWindow, "Error", "The config file has wrong version (" + ver + "): " + m_filename);
         bOK = false;
      }
      else if (!hasVer && !versionToCheck.isEmpty())
      {
         QMessageBox::critical(mainWindow, "Error", "\"CONFIG_VERSION\" is missing from config file: " + m_filename);
         bOK = false;
      }
   }

   file.close();

   return bOK;
}

int Configuration::GetInteger(QString const &varName, int defVal /*= 0*/) const
{
   int i = 0;
   if (GetValue(varName, i))
      return i;
   return defVal;
}

bool Configuration::GetBoolean(QString const &varName, bool defVal /*= false*/) const
{
   bool b = 0;
   if (GetValue(varName, b))
      return b;
   return defVal;
}

double Configuration::GetDouble(QString const &varName, double defVal /*= 0*/) const
{
   double d = 0;
   if (GetValue(varName, d))
      return d;
   return defVal;
}

QString Configuration::GetString(QString const &varName, QString const &defVal/* = ""*/) const
{
   QString s;
   if (GetValue(varName, s))
      return s;
   return defVal;
}

// these are simply overloaded -- return 'true' if 'val' has new value, 'false' - no such named value
bool Configuration::GetValue(QString const &varName, int &val) const
{
   QString str;
   if (GetValue(varName, str))
   {
      // got the value, try converting to int
      bool ok = false;
      int i = str.toInt(&ok);
      if (ok)
      {
         val = i;
         return true;
      }
   }
   return false;
}

bool Configuration::GetValue(QString const &varName, bool &val) const
{
   QString str;
   if (GetValue(varName, str))
   {
      // got the value, try converting to int (0 or not 0)
      bool ok = false;
      int i = str.toInt(&ok);
      if (ok)
      {
         val = (i != 0);
         return true;
      }
   }
   return false;
}

bool Configuration::GetValue(QString const &varName, double &val) const
{
   QString str;
   if (GetValue(varName, str))
   {
      // got the value, try converting to double
      bool ok = false;
      double d = str.toDouble(&ok);
      if (ok)
      {
         val = d;
         return true;
      }
   }
   return false;
}

bool Configuration::GetValue(QString const &varName, QString &val) const
{
   auto it = m_values.find(varName);
   if (it != m_values.end())
   {
      val = it->second;
      return true;
   }
   return false;
}
