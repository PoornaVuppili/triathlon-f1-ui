#pragma once

#include "..\KApp\Module.h"
#include "..\KTriathlonF1\TotalMainWindow.h"

class KTotalViewPS;
class KTotalEntPanelPS;
class KTotalDocPS;

class KTotalMainWindowPS : public CTotalMainWindow
{
    Q_OBJECT

public:
    KTotalMainWindowPS();
	~KTotalMainWindowPS();
	KTotalDocPS*	GetTotalDocPS() {return ((KTotalDocPS*)m_pDoc);};
	KTotalViewPS*	GetView() {return ((KTotalViewPS*)m_pView);};
	KTotalEntPanelPS*	GetEntPanel() {return ((KTotalEntPanelPS*)m_pEntPanel);};
    KTotalEntPanelPS* GetEntPanelPSFeatures() { return ((KTotalEntPanelPS*)m_pEntPanelPSFeatures); };

    virtual bool    Initialize();
    bool			InitializeJigs();
    virtual void	EnableActions( bool bEnable );
    bool			AllStepsComplete(std::string const &phase, std::string const &subphase) const override;
    bool OutputResults() const override;

    KTotalEntPanelPS* m_pEntPanelPSFeatures;
};
