#pragma once

#include <QtGui>
#include "Entity.h"
#include "DataDoc.h"

class CDocReader
{
public:
	CDocReader( CDataDoc* pDoc, const QString& sDocFileName );
	~CDocReader();

	bool			LoadHeaderFileInfo();
	bool			ReadDataFileVersionInfo(QString& type, int& release, int& revision, int& dataFileVersion);
	static QString  GetKeyValue(const QString& sDocFileName, QString key);
	static bool		ParseInputLine( const QString& sLine, QString& sTag, QString& sValue );

protected:
	void			InitEntity();
	void			OnDoNothing() {};

protected:
	CDataDoc*		m_pDoc;
	QString			m_sDocFolder;
	bool			m_bEntity;
	CEntRole		m_eEntRole;
	CEntType		m_eEntityType;
	CPartType		m_ePartType;
	QString			m_sFileName;
	CDisplayMode	m_eDisplayMode;
	bool			m_bUpToDateStatus;
	CEntValidateStatus	m_validateStatus;
	QString			m_validateMessage;
	int				m_designTime;//seconds

private:
};