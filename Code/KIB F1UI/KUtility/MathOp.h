///////////////////////////////////////////////////////////////////////
//	File:    MathOp.h
//	Purpose: Math operations.
//
//	Author:	 Tong Zhang
//  Modified: Alexander Miropolsky
//  Copyright 2008 - 2012 Stryker, Inc.
///////////////////////////////////////////////////////////////////////

#ifndef MATHOP_H
#define MATHOP_H

#include "math.h"
#include <cmath>

#ifndef PI
#define PI 3.14159265358979
#endif

// intersection flags
class IntersectFlag
{
public:
    static const int NonIntersect = 0;
    static const int Intersect = 1;
    static const int Parallel = 2;
    static const int Coincident = 3;
};

// basic math functions
template <typename T> inline T Sign(T v) { if(v==0) return 0; else if(v>0) return 1; else return -1; }
template <typename T> inline T round(T v) { return v < 0.0 ? ceil(v - 0.5) : floor(v + 0.5); }

extern const char* errorStringOutofMemory;
extern const char* errorStringInvertSingularMatrix;
extern const char* errorStringDivideByZero;
extern const char* errorInvalidParameter;

// LAPACK routines
#ifdef __cplusplus
extern "C" {
#endif
    int dgesv_(long int *n, long int *nrhs, double *a, long int *lda,
               long int*ipiv, double *b, long int *ldb, long int *info);
#ifdef __cplusplus
}
#endif

#endif //MATHOP_H

