#pragma once

enum CEntType
	{
	ENT_TYPE_NONE,
	ENT_TYPE_PART,
	ENT_TYPE_FEMORAL_AXES,
	ENT_TYPE_FEMORAL_PART,
	ENT_TYPE_FEMORAL_CUTS,
	ENT_TYPE_ADVANCED_CONTROL,
	};


enum CEntRole
	{
	//// Femoral implant CR ////
	ENT_ROLE_NONE,
	ENT_ROLE_FEMUR,

	ENT_ROLE_HIP,
	ENT_ROLE_FEMORAL_AXES,
	
	ENT_ROLE_FEMORAL_CUTS,
	ENT_ROLE_OUTLINE_PROFILE,
	
	ENT_ROLE_ADVANCED_CONTROL,
	ENT_ROLE_IMPLANT_END = 99, // last one for CR femoral implant design
	ENT_ROLE_JIGS_NONE = 100,
	ENT_ROLE_ADVANCED_CONTROL_JIGS,
	ENT_ROLE_OSTEOPHYTE_SURFACE,
	ENT_ROLE_CARTILAGE_SURFACE,
	ENT_ROLE_OUTLINE_PROFILE_JIGS,
	ENT_ROLE_INNER_SURFACE_JIGS,
	ENT_ROLE_OUTER_SURFACE_JIGS,
	ENT_ROLE_SIDE_SURFACE_JIGS,
	ENT_ROLE_SOLID_POSITION_JIGS,
	ENT_ROLE_STYLUS_JIGS,
	ENT_ROLE_ANTERIOR_SURFACE_JIGS,
	ENT_ROLE_JIGS_END = 199, // last one for CR femoral jig design

	////////////////////////////////////////////////////////////////////////
	// Femoral Implant PS /////////////////
	ENT_ROLE_IMPLANT_PS_NONE = 1000,
	ENT_ROLE_IMPLANT_PS_END = 1099, // last one for PS femoral implant design

	//// Femoral Jigs PS ////
	ENT_ROLE_JIGS_PS_NONE = 1100,
	ENT_ROLE_JIGS_PS_END = 1199, // last one for PS femoral jig design

	///////////////////////////////////////////////////////////////////////
	//// Implant Post Process
	ENT_ROLE_IMPLANT_POST_PROCESS_NONE = 2400,
	ENT_ROLE_IMPLANT_APPLICABLE_NOTES,
	ENT_ROLE_IMPLANT_NC_FEMUR_HARDWARE,
    ENT_ROLE_IMPLANT_NC_FEMUR_LARGE_UNDERNANG,
	ENT_ROLE_IMPLANT_POST_PROCESS_END = 2499,
	///////////////////////////////////////////////////////////////////////
	ENT_ROLE_DISPLAY_ONLY = 3000
	};


enum CDisplayMode
	{
	DISP_MODE_NONE,				
	DISP_MODE_HIDDEN,			// 
	DISP_MODE_WIREFRAME,		// 
	//DISP_MODE_SHADE,			// 
	DISP_MODE_DISPLAY,			// 
	DISP_MODE_TRANSPARENCY,
	DISP_MODE_DISPLAY_NET,		// Display + Iso curves
	DISP_MODE_TRANSPARENCY_NET,	// Transparency + Iso curves
	DISP_MODE_DISPLAY_CT,
	DISP_MODE_TRANSPARENT_CT
	};

enum CEntValidateStatus
	{
		VALIDATE_STATUS_NOT_VALIDATE = -1,
		VALIDATE_STATUS_GREEN,
		VALIDATE_STATUS_YELLOW,
		VALIDATE_STATUS_RED
};

