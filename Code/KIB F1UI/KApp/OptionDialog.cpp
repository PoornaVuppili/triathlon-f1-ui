#include "OptionDialog.h"
#include "ui_OptionDialog.h"
#include "OptionPage.h"
#include <QColorDialog>
#include "..\KUtility\KOracleDatabase.h"

#include <algorithm>

OptionDialog::OptionDialog(QWidget *parent)
    : QDialog(parent)
{
    ui = new Ui::OptionDialog();
    ui->setupUi(this);

    ui->treeWidget->setColumnCount(1);
    
    // clear widgets
    int n;
    while( (n=ui->stackedWidget->count()) > 0 )
        ui->stackedWidget->removeWidget( ui->stackedWidget->widget(0) );

    rootGlobal = new QTreeWidgetItem(ui->treeWidget);
    rootGlobal->setText(0, "Global");
    rootGlobal->setExpanded(true);

    rootImplant = new QTreeWidgetItem(ui->treeWidget);
    rootImplant->setText(0, "Implant");
    rootImplant->setExpanded(true);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(On_OK_Clicked()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(On_Cancel_Clicked()));

    connect(ui->treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), 
            this, SLOT(OnTreeItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)));
}

OptionDialog::~OptionDialog()
{
    delete ui;
}

// Add an option page
//  pg     : the option page to add, whose 'windowTitle' will be added as an item in the tree widget
//  global : whether the setting is for 'global' (default) or per 'implant'.
void OptionDialog::AddPage(OptionPage* pg, bool global)
{
    // test if already existed
    for(int i=0; i<pairGlobal.size(); i++)
        if( pairGlobal[i].second == pg )
            return;

    for(int i=0; i<pairImplant.size(); i++)
        if( pairImplant[i].second == pg )
            return;

    ItemPagePair pair;
    pair.second = pg;

    QTreeWidgetItem* item;
    if( global )
    {
        ui->stackedWidget->insertWidget((int)pairGlobal.size(), pg);
        
        item = new QTreeWidgetItem( rootGlobal );
        pair.first = item;
        pairGlobal.push_back( pair );
    }
    else
    {
        ui->stackedWidget->insertWidget((int)pairGlobal.size()+(int)pairImplant.size(), pg);

        item = new QTreeWidgetItem( rootImplant );
        pair.first = item;
        pairImplant.push_back( pair );
    }
    item->setText(0, pg->windowTitle());

    if( ui->stackedWidget->currentWidget() == pg )
        ui->treeWidget->setCurrentItem( item );
}

void OptionDialog::OnTreeItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous)
{
    for(auto it = pairGlobal.begin(); it != pairGlobal.end(); it++)
    {
        if( (*it).first == current )
        {
            ui->stackedWidget->setCurrentWidget( (*it).second );
            return;
        }
    }
    for(auto it = pairImplant.begin(); it != pairImplant.end(); it++)
    {
        if( (*it).first == current )
        {
            ui->stackedWidget->setCurrentWidget( (*it).second );
            return;
        }
    }
}

void OptionDialog::On_OK_Clicked()
{
    // accept the settings on each page
    for(auto it = pairGlobal.begin(); it != pairGlobal.end(); it++)
        (*it).second->OnOK();

    for(auto it = pairImplant.begin(); it != pairImplant.end(); it++)
        (*it).second->OnOK();

    accept();
}

void OptionDialog::On_Cancel_Clicked()
{
    // cancel the settings on each page
    for(auto it = pairGlobal.begin(); it != pairGlobal.end(); it++)
        (*it).second->OnCancel();

    for(auto it = pairImplant.begin(); it != pairImplant.end(); it++)
        (*it).second->OnCancel();

    reject();
}

