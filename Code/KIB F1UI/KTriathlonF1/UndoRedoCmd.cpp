#include "UndoRedoCmd.h"

///////////////////////////////////////////////////////////////////////////
// Make Femoral Axes
///////////////////////////////////////////////////////////////////////////
MakeFemoralAxesCmd::MakeFemoralAxesCmd
(
	CManager* manager, 
	CMakeFemoralAxesUndoData &prevUndoData, 
	CMakeFemoralAxesUndoData &postUndoData, 
	QUndoCommand *parent)
{
	m_pManager			= manager;
	m_prevUndoData = prevUndoData;
	m_postUndoData = postUndoData;
}
void MakeFemoralAxesCmd::undo()
{
	if (m_pManager->GetClassName() != "CMakeFemoralAxes") return;

	CMakeFemoralAxes* makeFemoralAxes = (CMakeFemoralAxes*) m_pManager;
	makeFemoralAxes->SetMakeFemoralAxesUndoData(m_prevUndoData);
}

void MakeFemoralAxesCmd::redo()
{
	if (m_pManager->GetClassName() != "CMakeFemoralAxes") return;

	CMakeFemoralAxes* makeFemoralAxes = (CMakeFemoralAxes*) m_pManager;
	makeFemoralAxes->SetMakeFemoralAxesUndoData(m_postUndoData);
}

///////////////////////////////////////////////////////////////////////////
// Cut Femur
///////////////////////////////////////////////////////////////////////////
//CutFemurCmd::CutFemurCmd
//(
//	CManager* manager, 
//	CCutFemurUndoData &prevUndoData, 
//	CCutFemurUndoData &postUndoData, 
//	QUndoCommand *parent)
//{
//	m_pManager			= manager;
//	
//	m_prevUndoData = prevUndoData;
//	m_postUndoData = postUndoData;
//}
//void CutFemurCmd::undo()
//{
//	if (m_pManager->GetClassName() != "CCutFemur") return;
//
//	CCutFemur* cutFemur = (CCutFemur*) m_pManager;
//	cutFemur->SetCutFemurUndoData(m_prevUndoData);
//	cutFemur->ReDraw();
//}
//
//void CutFemurCmd::redo()
//{
//	if (m_pManager->GetClassName() != "CCutFemur") return;
//
//	CCutFemur* cutFemur = (CCutFemur*) m_pManager;
//	cutFemur->SetCutFemurUndoData(m_postUndoData);
//	cutFemur->ReDraw();
//}

///////////////////////////////////////////////////////////////////////////
// Define Outline Profile
///////////////////////////////////////////////////////////////////////////
DefineOutlineProfileCmd::DefineOutlineProfileCmd
(
	CManager* manager, 
	CDefineOutlineProfileUndoData &prevUndoData, 
	CDefineOutlineProfileUndoData &postUndoData, 
	QUndoCommand *parent)
{
	m_pManager			= manager;
	
	m_prevUndoData = prevUndoData;
	m_postUndoData = postUndoData;
}
void DefineOutlineProfileCmd::undo()
{
	if (m_pManager->GetClassName() != "CDefineOutlineProfile") return;

	CDefineOutlineProfile* defineOutlineProfile = (CDefineOutlineProfile*) m_pManager;
	defineOutlineProfile->SetDefineOutlineProfileUndoData(m_prevUndoData);
	defineOutlineProfile->ReDraw();
}

void DefineOutlineProfileCmd::redo()
{
	if (m_pManager->GetClassName() != "CDefineOutlineProfile") return;

	CDefineOutlineProfile* defineOutlineProfile = (CDefineOutlineProfile*) m_pManager;
	defineOutlineProfile->SetDefineOutlineProfileUndoData(m_postUndoData);
	defineOutlineProfile->ReDraw();
}
