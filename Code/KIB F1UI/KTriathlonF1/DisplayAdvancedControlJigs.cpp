#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "DisplayAdvancedControlJigs.h"
#include "AdvancedControlJigs.h"
#include "AdvancedControlDlgJigs.h"
#include "OuterSurfaceJigs.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Utilities.h"

CDisplayAdvancedControlJigs::CDisplayAdvancedControlJigs( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CDisplayAdvancedControlJigs";

	m_toolbar = new QToolBar("Advanced Control");

	CAdvancedControlJigs* advCtrl = m_pDoc->GetAdvancedControlJigs();
	if (advCtrl == NULL) 
	{
		advCtrl = new CAdvancedControlJigs( m_pDoc );
		m_pDoc->AddEntity( advCtrl, false ); // Do not display in Entity Panel
	}

	m_actAdvancedControl = m_toolbar->addAction("AdvCtrl");
	SetActionAsTitle( m_actAdvancedControl );
	m_actDispDlg = m_toolbar->addAction("Disp Dialog");
	EnableAction( m_actDispDlg, true );
	m_actAccept = m_toolbar->addAction("OK");
	EnableAction( m_actAccept, true );

	m_pMainWindow->connect( m_actDispDlg, SIGNAL( triggered() ), this, SLOT( OnDispDlg() ) );
	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	OnDispDlg(advCtrl);
}

CDisplayAdvancedControlJigs::~CDisplayAdvancedControlJigs()
{
}

void CDisplayAdvancedControlJigs::OnDispDlg(CAdvancedControlJigs* advCtrl)
{

	// Create Advanced Control object and dialog here
	CAdvancedControlDlgJigs* pDlg = new CAdvancedControlDlgJigs( NULL, 0 );
	pDlg->setWindowTitle(QString("Advanced Control"));
	pDlg->resize(600, 300);
	pDlg->setModal(true);
	
	// Map the Advanced Control object data to Dialog

	// Auto Steps
	bool oldAutoAllSteps = advCtrl->GetAutoAllSteps();
	pDlg->SetAutoAllSteps(oldAutoAllSteps);
	bool oldAutoDrivenSteps = advCtrl->GetAutoDrivenSteps();
	pDlg->SetAutoDrivenSteps(oldAutoDrivenSteps);

	// Osteophyte Surface

	// Cartilage Surface
	double oldCartilageThickness = advCtrl->GetCartilageThickness();
	pDlg->SetCartilageThickness(oldCartilageThickness);
	double oldTroGroAddCartilageThickness = advCtrl->GetTrochlearGrooveAdditionalCartilageThickness();
	pDlg->SetTrochlearGrooveAdditionalCartilageThickness(oldTroGroAddCartilageThickness);
	double oldTroGroCartilageTransitionWidth = advCtrl->GetTrochlearGrooveCartilageTransitionWidth();
	pDlg->SetTrochlearGrooveCartilageTransitionWidth(oldTroGroCartilageTransitionWidth);
	bool oldCartilageInitializeEdgeOnly = advCtrl->GetCartilageInitializeEdgeOnly();
	pDlg->SetCartilageInitializeEdgeOnly(oldCartilageInitializeEdgeOnly);
	bool oldCartilageUseWaterTightSurface = advCtrl->GetCartilageUseWaterTightSurface();
	pDlg->SetCartilageUseWaterTightSurface(oldCartilageUseWaterTightSurface);
	bool oldCartilageDisplaySmooth = advCtrl->GetCartilageDisplaySmooth();
	pDlg->SetCartilageDisplaySmooth(oldCartilageDisplaySmooth);

	// Outline Profile
	bool oldOutlineSquareOffPosteriorTip = advCtrl->GetOutlineSquareOffPosteriorTip();
	pDlg->SetOutlineSquareOffPosteriorTip(oldOutlineSquareOffPosteriorTip);
	double oldOutlineSquareOffCornerRadiusRatio = advCtrl->GetOutlineSquareOffCornerRadiusRatio();
	pDlg->SetOutlineSquareOffCornerRadiusRatio(oldOutlineSquareOffCornerRadiusRatio);
	double oldOutlineSquareOffTransitionFactor = advCtrl->GetOutlineSquareOffTransitionFactor();
	pDlg->SetOutlineSquareOffTransitionFactor(oldOutlineSquareOffTransitionFactor);

	// Inner Surface
	double oldInnerSurfaceApproachDegree = advCtrl->GetInnerSurfaceApproachDegree();
	pDlg->SetInnerSurfaceApproachDegree(oldInnerSurfaceApproachDegree);
	double oldInnerSurfaceDropOffsetDistance = advCtrl->GetInnerSurfaceDropOffsetDistance();
	pDlg->SetInnerSurfaceDropOffsetDistance(oldInnerSurfaceDropOffsetDistance);
	double oldInnerSurfaceAdditionalDropLength = advCtrl->GetInnerSurfaceAdditionalDropLength();
	pDlg->SetInnerSurfaceAdditionalDropLength(oldInnerSurfaceAdditionalDropLength);
	double oldInnerSurfaceAdditionalPosteriorCoverage = advCtrl->GetInnerSurfaceAdditionalPosteriorCoverage();
	pDlg->SetInnerSurfaceAdditionalPosteriorCoverage(oldInnerSurfaceAdditionalPosteriorCoverage);
	double oldInnerSurfaceAnteriorDropDegree = advCtrl->GetInnerSurfaceAnteriorDropDegree();
	pDlg->SetInnerSurfaceAnteriorDropDegree(oldInnerSurfaceAnteriorDropDegree);
	bool oldInnerSurfaceDisplayAnteriorDropSurface = advCtrl->GetInnerSurfaceDisplayAnteriorDropSurface();
	pDlg->SetInnerSurfaceDisplayAnteriorDropSurface(oldInnerSurfaceDisplayAnteriorDropSurface);

	// Outer Surface
	double oldOuterSurfaceOffsetDistance = advCtrl->GetOuterSurfaceOffsetDistance();
	pDlg->SetOuterSurfaceOffsetDistance(oldOuterSurfaceOffsetDistance);
	double oldOuterSurfaceTroGroAddThickness = advCtrl->GetOuterSurfaceTrochlearGrooveAdditionalThickness();
	pDlg->SetOuterSurfaceTrochlearGrooveAdditionalThickness(oldOuterSurfaceTroGroAddThickness);
	double oldOuterSurfaceTroGroAddThicknessTransitionWidth = advCtrl->GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth();
	pDlg->SetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth(oldOuterSurfaceTroGroAddThicknessTransitionWidth);
	double oldOuterSurfaceDropDegree = advCtrl->GetOuterSurfaceDropDegree();
	pDlg->SetOuterSurfaceDropDegree(oldOuterSurfaceDropDegree);
	bool oldOuterSurfaceDisplayAnteriorDropSurface = advCtrl->GetOuterSurfaceDisplayAnteriorDropSurface();
	pDlg->SetOuterSurfaceDisplayAnteriorDropSurface(oldOuterSurfaceDisplayAnteriorDropSurface);
	bool oldOuterSurfaceEnforceSmooth = advCtrl->GetOuterSurfaceEnforceSmooth();
	pDlg->SetOuterSurfaceEnforceSmooth(oldOuterSurfaceEnforceSmooth);

	// Side Surface
	double oldSideSurfaceExtendedWidth = advCtrl->GetSideSurfaceExtendedWidth();
	pDlg->SetSideSurfaceExtendedWidth(oldSideSurfaceExtendedWidth);

	// Solid Position Jigs
	int oldSolidPositionJigsToleranceLevel = advCtrl->GetSolidPositionJigsToleranceLevel();
	pDlg->SetSolidPositionJigsToleranceLevel(oldSolidPositionJigsToleranceLevel);

	// Stylus Jigs
	bool oldStylusJigsDisplayVirtualFeaturesOnly = advCtrl->GetStylusJigsDisplayVirtualFeaturesOnly();
	pDlg->SetStylusJigsDisplayVirtualFeaturesOnly(oldStylusJigsDisplayVirtualFeaturesOnly);
	bool oldStylusJigsFullControl = advCtrl->GetStylusJigsFullControl();
	pDlg->SetStylusJigsFullControl(oldStylusJigsFullControl);
	double oldStylusJigsClearance = advCtrl->GetStylusJigsClearance();
	pDlg->SetStylusJigsClearance(oldStylusJigsClearance);

	// Anterior Surface
	double oldAnteriorSurfaceApproachDegree = advCtrl->GetAnteriorSurfaceApproachDegree();
	pDlg->SetAnteriorSurfaceApproachDegree(oldAnteriorSurfaceApproachDegree);
	double oldAnteriorSurfaceAntExtension = advCtrl->GetAnteriorSurfaceAntExtension();
	pDlg->SetAnteriorSurfaceAntExtension(oldAnteriorSurfaceAntExtension);
	double oldAnteriorSurfaceDropExtension = advCtrl->GetAnteriorSurfaceDropExtension();
	pDlg->SetAnteriorSurfaceDropExtension(oldAnteriorSurfaceDropExtension);

	// Outputs
	bool oldOutputOsteophyteSurface = advCtrl->GetOutputOsteophyteSurface();
	pDlg->SetOutputOsteophyteSurface(oldOutputOsteophyteSurface);
	bool oldOutputCartilageSurface = advCtrl->GetOutputCartilageSurface();
	pDlg->SetOutputCartilageSurface(oldOutputCartilageSurface);
	bool oldOutputThreeSurfaces = advCtrl->GetOutputThreeSurfaces();
	pDlg->SetOutputThreeSurfaces(oldOutputThreeSurfaces);
	bool oldOutputAnteriorSurfaceRegardless = advCtrl->GetOutputAnteriorSurfaceRegardless();
	pDlg->SetOutputAnteriorSurfaceRegardless(oldOutputAnteriorSurfaceRegardless);

	if ( pDlg->exec() == QDialog::Accepted )
	{
		// Map the dialog data to Advanced Control object
		// **** Keep the reverse order in case more than one tab is changed. ****

		// Outputs
		if ( oldOutputOsteophyteSurface != pDlg->GetOutputOsteophyteSurface() ) // compare boolean
		{
			advCtrl->SetOutputOsteophyteSurface(pDlg->GetOutputOsteophyteSurface());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutputOsteophyteSurface: %1, newOutputOsteophyteSurface: %2 ").arg(oldOutputOsteophyteSurface).arg(pDlg->GetOutputOsteophyteSurface()) );
		}
		if ( oldOutputCartilageSurface != pDlg->GetOutputCartilageSurface() ) // compare boolean
		{
			advCtrl->SetOutputCartilageSurface(pDlg->GetOutputCartilageSurface());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutputCartilageSurface: %1, newOutputCartilageSurface: %2 ").arg(oldOutputCartilageSurface).arg(pDlg->GetOutputCartilageSurface()) );
		}
		if ( oldOutputThreeSurfaces != pDlg->GetOutputThreeSurfaces() ) // compare boolean
		{
			advCtrl->SetOutputThreeSurfaces(pDlg->GetOutputThreeSurfaces());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutputThreeSurfaces: %1, newOutputThreeSurfaces: %2 ").arg(oldOutputThreeSurfaces).arg(pDlg->GetOutputThreeSurfaces()) );
		}
		if ( oldOutputAnteriorSurfaceRegardless != pDlg->GetOutputAnteriorSurfaceRegardless() ) // compare boolean
		{
			advCtrl->SetOutputAnteriorSurfaceRegardless(pDlg->GetOutputAnteriorSurfaceRegardless());
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutputAnteriorSurfaceRegardless: %1, newOutputAnteriorSurfaceRegardless: %2 ").arg(oldOutputAnteriorSurfaceRegardless).arg(pDlg->GetOutputAnteriorSurfaceRegardless()) );
		}

		// Anterior Surface
		bool AnteriorSurfaceModified = false;
		if ( !IS_EQ_TOL6( oldAnteriorSurfaceApproachDegree, pDlg->GetAnteriorSurfaceApproachDegree() ) )
		{
			advCtrl->SetAnteriorSurfaceApproachDegree(pDlg->GetAnteriorSurfaceApproachDegree());
			AnteriorSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldAnteriorSurfaceApproachDegree: %1, newAnteriorSurfaceApproachDegree: %2 ").arg(oldAnteriorSurfaceApproachDegree).arg(pDlg->GetAnteriorSurfaceApproachDegree()) );
		}
		if ( !IS_EQ_TOL6( oldAnteriorSurfaceAntExtension, pDlg->GetAnteriorSurfaceAntExtension() ) )
		{
			advCtrl->SetAnteriorSurfaceAntExtension(pDlg->GetAnteriorSurfaceAntExtension());
			AnteriorSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldAnteriorSurfaceAntExtension: %1, newAnteriorSurfaceAntExtension: %2 ").arg(oldAnteriorSurfaceAntExtension).arg(pDlg->GetAnteriorSurfaceAntExtension()) );
		}
		if ( !IS_EQ_TOL6( oldAnteriorSurfaceDropExtension, pDlg->GetAnteriorSurfaceDropExtension() ) )
		{
			advCtrl->SetAnteriorSurfaceDropExtension(pDlg->GetAnteriorSurfaceDropExtension());
			AnteriorSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldAnteriorSurfaceDropExtension: %1, newAnteriorSurfaceDropExtension: %2 ").arg(oldAnteriorSurfaceDropExtension).arg(pDlg->GetAnteriorSurfaceDropExtension()) );
		}
		if (AnteriorSurfaceModified)
		{
			if ( m_pDoc->GetAnteriorSurfaceJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_ANTERIOR_SURFACE_JIGS, false, false); // Therefore, anterior surface jigs will show modified "*" mark.
		}

		// Stylus Jigs
		bool StylusJigsModified = false;
		if ( oldStylusJigsDisplayVirtualFeaturesOnly != pDlg->GetStylusJigsDisplayVirtualFeaturesOnly() )
		{
			advCtrl->SetStylusJigsDisplayVirtualFeaturesOnly(pDlg->GetStylusJigsDisplayVirtualFeaturesOnly());
			StylusJigsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldStylusJigsDisplayVirtualFeaturesOnly: %1, newStylusJigsDisplayVirtualFeaturesOnly: %2 ").arg(oldStylusJigsDisplayVirtualFeaturesOnly).arg(pDlg->GetStylusJigsDisplayVirtualFeaturesOnly()) );
		}
		if ( oldStylusJigsFullControl != pDlg->GetStylusJigsFullControl() )
		{
			advCtrl->SetStylusJigsFullControl(pDlg->GetStylusJigsFullControl());
			//StylusJigsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldStylusJigsFullControl: %1, newStylusJigsFullControl: %2 ").arg(oldStylusJigsFullControl).arg(pDlg->GetStylusJigsFullControl()) );
		}
		if ( !IS_EQ_TOL6( oldStylusJigsClearance, pDlg->GetStylusJigsClearance() ) )
		{
			advCtrl->SetStylusJigsClearance(pDlg->GetStylusJigsClearance());
			//AnteriorSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldStylusJigsClearance: %1, newStylusJigsClearance: %2 ").arg(oldStylusJigsClearance).arg(pDlg->GetStylusJigsClearance()) );
		}
		if (StylusJigsModified)
		{
			if ( m_pDoc->GetStylusJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_STYLUS_JIGS, false, false); // Therefore, stylus jigs will show modified "*" mark.
		}

		// Solid Position Jigs
		bool SolidPositionJigsModified = false;
		if ( oldSolidPositionJigsToleranceLevel != pDlg->GetSolidPositionJigsToleranceLevel() )
		{
			advCtrl->SetSolidPositionJigsToleranceLevel(pDlg->GetSolidPositionJigsToleranceLevel());
			//SolidPositionJigsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldSolidPositionJigsToleranceLevel: %1, newSolidPositionJigsToleranceLevel: %2 ").arg(oldSolidPositionJigsToleranceLevel).arg(pDlg->GetSolidPositionJigsToleranceLevel()) );
		}
		if (SolidPositionJigsModified)
		{
			if ( m_pDoc->GetSolidPositionJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SOLID_POSITION_JIGS, false, false); // Therefore, solid position jigs will show modified "*" mark.
		}

		// Side Surface
		bool SideSurfaceModified = false;
		if ( !IS_EQ_TOL6( oldSideSurfaceExtendedWidth, pDlg->GetSideSurfaceExtendedWidth() ) )
		{
			advCtrl->SetSideSurfaceExtendedWidth(pDlg->GetSideSurfaceExtendedWidth());
			SideSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldSideSurfaceExtendedWidth: %1, newSideSurfaceExtendedWidth: %2 ").arg(oldSideSurfaceExtendedWidth).arg(pDlg->GetSideSurfaceExtendedWidth()) );
		}
		if (SideSurfaceModified)
		{
			if ( m_pDoc->GetSideSurfaceJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_SIDE_SURFACE_JIGS, false, false); // Therefore, side surface jigs will show modified "*" mark.
		}

		// Outer Surface
		bool OuterSurfaceModified = false;
		if ( !IS_EQ_TOL6( oldOuterSurfaceOffsetDistance, pDlg->GetOuterSurfaceOffsetDistance() ) )
		{
			// initialize time stamp to OuterSurfaceJigs, in order to trigger the offset surface update.
			QDateTime initTime = QDateTime::currentDateTime();
			uint initTimeStamp = initTime.toTime_t();
			m_pDoc->GetOuterSurfaceJigs()->SetOsteophyteTimeStamp(initTimeStamp);
			m_pDoc->GetOuterSurfaceJigs()->SetCartilageTimeStamp(initTimeStamp);
			//
			advCtrl->SetOuterSurfaceOffsetDistance(pDlg->GetOuterSurfaceOffsetDistance());
			OuterSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOuterSurfaceOffsetDistance: %1, newOuterSurfaceOffsetDistance: %2 ").arg(oldOuterSurfaceOffsetDistance).arg(pDlg->GetOuterSurfaceOffsetDistance()) );
		}
		if ( !IS_EQ_TOL6( oldOuterSurfaceDropDegree, pDlg->GetOuterSurfaceDropDegree() ) )
		{
			advCtrl->SetOuterSurfaceDropDegree(pDlg->GetOuterSurfaceDropDegree());
			OuterSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOuterSurfaceDropDegree: %1, newOuterSurfaceDropDegree: %2 ").arg(oldOuterSurfaceDropDegree).arg(pDlg->GetOuterSurfaceDropDegree()) );
		}
		if ( oldOuterSurfaceDisplayAnteriorDropSurface != pDlg->GetOuterSurfaceDisplayAnteriorDropSurface() )
		{
			advCtrl->SetOuterSurfaceDisplayAnteriorDropSurface(pDlg->GetOuterSurfaceDisplayAnteriorDropSurface());
			//OuterSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOuterSurfaceDisplayAnteriorDropSurface: %1, newOuterSurfaceDisplayAnteriorDropSurface: %2 ").arg(oldOuterSurfaceDisplayAnteriorDropSurface).arg(pDlg->GetOuterSurfaceDisplayAnteriorDropSurface()) );
		}
		if ( !IS_EQ_TOL6(oldOuterSurfaceTroGroAddThickness, pDlg->GetOuterSurfaceTrochlearGrooveAdditionalThickness()) )
		{
			advCtrl->SetOuterSurfaceTrochlearGrooveAdditionalThickness(pDlg->GetOuterSurfaceTrochlearGrooveAdditionalThickness());
			OuterSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOuterSurfaceTroGroAddThickness: %1, newOuterSurfaceTroGroAddThickness: %2 ").arg(oldOuterSurfaceTroGroAddThickness).arg(pDlg->GetOuterSurfaceTrochlearGrooveAdditionalThickness()) );
		}
		if ( !IS_EQ_TOL6(oldOuterSurfaceTroGroAddThicknessTransitionWidth, pDlg->GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth()) )
		{
			advCtrl->SetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth(pDlg->GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth());
			OuterSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOuterSurfaceTroGroAddThicknessTransitionWidth: %1, newOuterSurfaceTroGroAddThicknessTransitionWidth: %2 ").arg(oldOuterSurfaceTroGroAddThicknessTransitionWidth).arg(pDlg->GetOuterSurfaceTrochlearGrooveAdditionalThicknessTransitionWidth()) );
		}
		if ( oldOuterSurfaceEnforceSmooth != pDlg->GetOuterSurfaceEnforceSmooth() )
		{
			advCtrl->SetOuterSurfaceEnforceSmooth(pDlg->GetOuterSurfaceEnforceSmooth());
			//OuterSurfaceSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOuterSurfaceEnforceSmooth: %1, newOuterSurfaceEnforceSmooth: %2 ").arg(oldOuterSurfaceEnforceSmooth).arg(pDlg->GetOuterSurfaceEnforceSmooth()) );
		}
		if (OuterSurfaceModified)
		{
			if ( m_pDoc->GetOuterSurfaceJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTER_SURFACE_JIGS, false, false); // Therefore, outer surface jigs will show modified "*" mark.
		}

		// Inner Surface
		bool InnerSurfaceModified = false;
		if ( !IS_EQ_TOL6( oldInnerSurfaceApproachDegree, pDlg->GetInnerSurfaceApproachDegree() ) )
		{
			advCtrl->SetInnerSurfaceApproachDegree(pDlg->GetInnerSurfaceApproachDegree());
			InnerSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldInnerSurfaceApproachDegree: %1, newInnerSurfaceApproachDegree: %2 ").arg(oldInnerSurfaceApproachDegree).arg(pDlg->GetInnerSurfaceApproachDegree()) );
		}
		if ( !IS_EQ_TOL6( oldInnerSurfaceDropOffsetDistance, pDlg->GetInnerSurfaceDropOffsetDistance() ) )
		{
			advCtrl->SetInnerSurfaceDropOffsetDistance(pDlg->GetInnerSurfaceDropOffsetDistance());
			InnerSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldInnerSurfaceDropOffsetDistance: %1, newInnerSurfaceDropOffsetDistance: %2 ").arg(oldInnerSurfaceDropOffsetDistance).arg(pDlg->GetInnerSurfaceDropOffsetDistance()) );
		}
		if ( !IS_EQ_TOL6( oldInnerSurfaceAdditionalDropLength, pDlg->GetInnerSurfaceAdditionalDropLength() ) )
		{
			advCtrl->SetInnerSurfaceAdditionalDropLength(pDlg->GetInnerSurfaceAdditionalDropLength());
			InnerSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldInnerSurfaceAdditionalDropLength: %1, newInnerSurfaceAdditionalDropLength: %2 ").arg(oldInnerSurfaceAdditionalDropLength).arg(pDlg->GetInnerSurfaceAdditionalDropLength()) );
		}
		if ( !IS_EQ_TOL6( oldInnerSurfaceAdditionalPosteriorCoverage, pDlg->GetInnerSurfaceAdditionalPosteriorCoverage() ) )
		{
			advCtrl->SetInnerSurfaceAdditionalPosteriorCoverage(pDlg->GetInnerSurfaceAdditionalPosteriorCoverage());
			InnerSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldInnerSurfaceAdditionalPosteriorCoverage: %1, newInnerSurfaceAdditionalPosteriorCoverage: %2 ").arg(oldInnerSurfaceAdditionalPosteriorCoverage).arg(pDlg->GetInnerSurfaceAdditionalPosteriorCoverage()) );
		}
		if ( !IS_EQ_TOL6( oldInnerSurfaceAnteriorDropDegree, pDlg->GetInnerSurfaceAnteriorDropDegree() ) )
		{
			advCtrl->SetInnerSurfaceAnteriorDropDegree(pDlg->GetInnerSurfaceAnteriorDropDegree());
			InnerSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldInnerSurfaceAnteriorDropDegree: %1, newInnerSurfaceAnteriorDropDegree: %2 ").arg(oldInnerSurfaceAnteriorDropDegree).arg(pDlg->GetInnerSurfaceAnteriorDropDegree()) );
		}
		if ( oldInnerSurfaceDisplayAnteriorDropSurface != pDlg->GetInnerSurfaceDisplayAnteriorDropSurface() )
		{
			advCtrl->SetInnerSurfaceDisplayAnteriorDropSurface(pDlg->GetInnerSurfaceDisplayAnteriorDropSurface());
			//InnerSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldInnerSurfaceDisplayAnteriorDropSurface: %1, newInnerSurfaceDisplayAnteriorDropSurface: %2 ").arg(oldInnerSurfaceDisplayAnteriorDropSurface).arg(pDlg->GetInnerSurfaceDisplayAnteriorDropSurface()) );
		}
		if (InnerSurfaceModified)
		{
			if ( m_pDoc->GetInnerSurfaceJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_INNER_SURFACE_JIGS, false, false); // Therefore, inner surface jigs will show modified "*" mark.
		}

		// Outline Profile
		bool OutlineProfileModified = false;
		if ( oldOutlineSquareOffPosteriorTip != pDlg->GetOutlineSquareOffPosteriorTip() )
		{
			advCtrl->SetOutlineSquareOffPosteriorTip(pDlg->GetOutlineSquareOffPosteriorTip());
			OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutlineSquareOffPosteriorTip: %1, newOutlineSquareOffPosteriorTip: %2 ").arg(oldOutlineSquareOffPosteriorTip).arg(pDlg->GetOutlineSquareOffPosteriorTip()) );
		}
		if ( !IS_EQ_TOL6(oldOutlineSquareOffCornerRadiusRatio,pDlg->GetOutlineSquareOffCornerRadiusRatio()) )
		{
			advCtrl->SetOutlineSquareOffCornerRadiusRatio(pDlg->GetOutlineSquareOffCornerRadiusRatio());
			OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutlineSquareOffCornerRadiusRatio: %1, newOutlineSquareOffCornerRadiusRatio: %2 ").arg(oldOutlineSquareOffCornerRadiusRatio).arg(pDlg->GetOutlineSquareOffCornerRadiusRatio()) );
		}
		if ( !IS_EQ_TOL6(oldOutlineSquareOffTransitionFactor,pDlg->GetOutlineSquareOffTransitionFactor()) )
		{
			advCtrl->SetOutlineSquareOffTransitionFactor(pDlg->GetOutlineSquareOffTransitionFactor());
			OutlineProfileModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldOutlineSquareOffTransitionFactor: %1, newOutlineSquareOffTransitionFactor: %2 ").arg(oldOutlineSquareOffTransitionFactor).arg(pDlg->GetOutlineSquareOffTransitionFactor()) );
		}
		if (OutlineProfileModified)
		{
			if ( m_pDoc->GetOutlineProfileJigs() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OUTLINE_PROFILE_JIGS, false, false); // Therefore, outline profile jigs will show modified "*" mark.
		}

		// Cartilage Surface
		bool CartilageSurfaceModified = false;
		if ( !IS_EQ_TOL6(oldCartilageThickness, pDlg->GetCartilageThickness()) )
		{
			advCtrl->SetCartilageThickness(pDlg->GetCartilageThickness());
			CartilageSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldCartilageThickness: %1, newCartilageThickness: %2 ").arg(oldCartilageThickness).arg(pDlg->GetCartilageThickness()) );
		}
		if ( !IS_EQ_TOL6(oldTroGroAddCartilageThickness, pDlg->GetTrochlearGrooveAdditionalCartilageThickness()) )
		{
			advCtrl->SetTrochlearGrooveAdditionalCartilageThickness(pDlg->GetTrochlearGrooveAdditionalCartilageThickness());
			CartilageSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldTroGroAddCartilageThickness: %1, newTroGroAddCartilageThickness: %2 ").arg(oldTroGroAddCartilageThickness).arg(pDlg->GetTrochlearGrooveAdditionalCartilageThickness()) );
		}
		if ( !IS_EQ_TOL6(oldTroGroCartilageTransitionWidth, pDlg->GetTrochlearGrooveCartilageTransitionWidth()) )
		{
			advCtrl->SetTrochlearGrooveCartilageTransitionWidth(pDlg->GetTrochlearGrooveCartilageTransitionWidth());
			CartilageSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldTroGroCartilageTransitionWidth: %1, newTroGroCartilageTransitionWidth: %2 ").arg(oldTroGroCartilageTransitionWidth).arg(pDlg->GetTrochlearGrooveCartilageTransitionWidth()) );
		}
		if ( oldCartilageInitializeEdgeOnly != pDlg->GetCartilageInitializeEdgeOnly() )
		{
			advCtrl->SetCartilageInitializeEdgeOnly(pDlg->GetCartilageInitializeEdgeOnly());
			//CartilageSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldCartilageInitializeEdgeOnly: %1, newCartilageInitializeEdgeOnly: %2 ").arg(oldCartilageInitializeEdgeOnly).arg(pDlg->GetCartilageInitializeEdgeOnly()) );
		}
		if ( oldCartilageUseWaterTightSurface != pDlg->GetCartilageUseWaterTightSurface() )
		{
			advCtrl->SetCartilageUseWaterTightSurface(pDlg->GetCartilageUseWaterTightSurface());
			//CartilageSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldCartilageUseWaterTightSurface: %1, newCartilageUseWaterTightSurface: %2 ").arg(oldCartilageUseWaterTightSurface).arg(pDlg->GetCartilageUseWaterTightSurface()) );
		}
		if ( oldCartilageDisplaySmooth != pDlg->GetCartilageDisplaySmooth() )
		{
			advCtrl->SetCartilageDisplaySmooth(pDlg->GetCartilageDisplaySmooth());
			//CartilageSurfaceModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControlJigs::OnDispDlg(), oldCartilageDisplaySmooth: %1, newCartilageDisplaySmooth: %2 ").arg(oldCartilageDisplaySmooth).arg(pDlg->GetCartilageDisplaySmooth()) );
		}
		if (CartilageSurfaceModified)
		{
			if ( m_pDoc->GetCartilageSurface() )
				m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_CARTILAGE_SURFACE, false, false); // Therefore, cartilage surface will also show modified "*" mark.
		}

		// Osteophyte Surface

		// Auto Steps
		bool autoStepsModified = false;
		if ( oldAutoAllSteps != pDlg->GetAutoAllSteps() ) // compare boolean
		{
			advCtrl->SetAutoAllSteps(pDlg->GetAutoAllSteps());
			autoStepsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAutoAllSteps: %1, newAutoAllSteps: %2 ").arg(oldAutoAllSteps).arg(pDlg->GetAutoAllSteps()) );
		}
		if ( oldAutoDrivenSteps != pDlg->GetAutoDrivenSteps() ) // compare boolean
		{
			advCtrl->SetAutoDrivenSteps(pDlg->GetAutoDrivenSteps());
			autoStepsModified = true;
			m_pDoc->AppendLog( QString("CDisplayAdvancedControl::OnDispDlg(), oldAutoDrivenSteps: %1, newAutoDrivenSteps: %2 ").arg(oldAutoDrivenSteps).arg(pDlg->GetAutoDrivenSteps()) );
		}
	}

	OnAccept();

}

void CDisplayAdvancedControlJigs::OnAccept()
{
	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	m_bDestroyMe = true;
	m_pView->Redraw();
}
