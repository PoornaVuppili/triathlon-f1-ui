#include "OpenGLView.h"
#include "ZoomView.h"


CZoomView::CZoomView( COpenGLView* pView ) : CManager( pView )
	{
	m_eStatus = INDICATE_LOWER_LEFT_POINT;

	SetSelectionMode( "Indicate lower left point" );
	}


bool CZoomView::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
	{
	if( m_eStatus == INDICATE_LOWER_LEFT_POINT )
		{
		m_eStatus = INDICATE_UPPER_RIGHT_POINT;

		SetSelectionMode( "Indicate upper right point" );

		m_mouseStart = cursor;

		m_pView->Redraw();
		}

	return true;
	}


bool CZoomView::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
	{
	if( m_eStatus == INDICATE_UPPER_RIGHT_POINT )
		{
		m_pView->Enter2dMode( true );
		m_pView->SetXOR();

		if( m_bMouseLast )
			DrawCursor();

		m_mouseLast = cursor;
		m_bMouseLast = true;

		DrawCursor();

		m_pView->ClearXOR();
		m_pView->Restore3dMode();

		glFlush();
		}

	return true;
	}


bool CZoomView::MouseUp( const QPoint& cursor, Viewport* vp )
	{
	if( m_eStatus == INDICATE_UPPER_RIGHT_POINT )
		{
		m_mouseLast = cursor;

		m_pView->Zoom( m_mouseStart, m_mouseLast );

		ClearCursorAndPrompt();
		m_pView->Redraw();

		m_bDestroyMe = true;
		}

	return true;
	}


//bool CZoomView::MouseRight( QPoint& cursor )
//	{
//	if( m_eStatus == INDICATE_LOWER_LEFT_POINT )
//		{
//		ClearCursorAndPrompt();
//		m_pView->Redraw();
//
//		m_bDestroyMe = true;
//		}
//
//	return true;
//	}


bool CZoomView::Reject()
	{
	switch( m_eStatus )
		{
		case INDICATE_LOWER_LEFT_POINT:
			ClearCursorAndPrompt();
			m_pView->Redraw();
			m_bDestroyMe = true;
			break;

		case INDICATE_UPPER_RIGHT_POINT:
			ClearCursorAndPrompt();
			m_pView->Redraw();
			m_eStatus = INDICATE_LOWER_LEFT_POINT;
			SetSelectionMode( "Indicate left lower point" );
			break;
		}

	return true;
	}


void CZoomView::DrawCursor()
	{
	double		xStart, yStart, xLast, yLast;

	m_pView->UVto2D( m_mouseStart, xStart, yStart );
	m_pView->UVto2D( m_mouseLast, xLast, yLast );

	glColor3ub( 255, 255, 255 );

	glBegin( GL_LINE_LOOP );

	glVertex2d(  xStart, yStart );
	glVertex2d(  xLast,	 yStart );
	glVertex2d(  xLast,	 yLast	);
	glVertex2d(  xStart, yLast	);

	glEnd();
	}

