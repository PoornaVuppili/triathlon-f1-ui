#include "GroodSuntayTransform.h"
#pragma warning( disable : 4996 4805 )
#include <IwAxis2Placement.h>
#pragma warning( default : 4996 4805 )
#include <cassert>

GroodSuntayTransform::GroodSuntayTransform(double flexRadian, double abductionRadian, double rotationRadian)
{
	double alpha(flexRadian), beta(abductionRadian), gamma(rotationRadian);

	double a = cos(alpha), A = sin(alpha);
	double b = cos(beta),  B = sin(beta);
	double g = cos(gamma), G = sin(gamma);

	m_xVec[0] = b*g - A*B*G;
	m_xVec[1] = b*G + A*B*g;
	m_xVec[2] = a*B;

	m_yVec[0] = -a*G;
	m_yVec[1] = a*g;
	m_yVec[2] = -A;

	m_zVec[0] = -B*g - A*b*G;
	m_zVec[1] = -B*G + A*b*g;
	m_zVec[2] = a*b;
}

void GroodSuntayTransform::GetGroodSuntayParameters(IwAxis2Placement const& mechanicalAxes, IwAxis2Placement const& _xform, double& flexRadian, double& abductionRadian, double& rotationRadian, double translate[3])
{
	IwAxis2Placement xform;
	IwAxis2Placement mechanicalAxesInv;
	mechanicalAxes.Invert(mechanicalAxesInv);
	mechanicalAxesInv.TransformAxis2Placement(_xform, xform);

	IwPoint3d  O(xform.GetOrigin());
    IwVector3d X(xform.GetXAxis());
    IwVector3d Y(xform.GetYAxis());
    IwVector3d Z(xform.GetZAxis());

	double A = -Y[2];
	double a = sqrt(1 - A*A);
	double ab = Z[2];
	if(ab < 0)
		a = -a;

	flexRadian = asin(A);
	if(A > 0 && a < 0)
		flexRadian = IW_PI - flexRadian;

	double b = ab/a;
	double aB = X[2];
	double B = aB/a;
	abductionRadian = (B >= 0 ? acos(b) : -acos(b));

	double ag = Y[1];
	double aG = -Y[0];
	double g = ag/a;
	double G = aG/a;
	rotationRadian = (G >= 0 ? acos(g) : -acos(g));

	if(bool debug = true)
	{
		GroodSuntayTransform xform2(flexRadian, abductionRadian, rotationRadian);
		for(int i = 0; i < 3; ++i)
		{
			assert(abs(xform2.m_xVec[i] - X[i]) < 1e-3);
			assert(abs(xform2.m_yVec[i] - Y[i]) < 1e-3);
			assert(abs(xform2.m_zVec[i] - Z[i]) < 1e-3);
		}
	}
}
