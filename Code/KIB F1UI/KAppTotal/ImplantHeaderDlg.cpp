#include <qpushbutton.h>
#include <qbuttongroup.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qradiobutton.h>
#include <qlayout.h>
#include <qstring.h>
#include "ImplantHeaderDlg.h"


CImplantHeaderDlg::CImplantHeaderDlg( QWidget* parent, int nPaientId, int nSide ) : 
						QDialog( parent, 0 )
	{
	setWindowTitle( "Implant Info" );

	m_nSide = nSide;

	QVBoxLayout*		layoutMain			= new QVBoxLayout;
	QHBoxLayout*		layoutPatientId		= new QHBoxLayout;
	QVBoxLayout*		layoutImplantSide	= new QVBoxLayout;
	QHBoxLayout*		layoutButtons		= new QHBoxLayout;

	m_labelPatientId	= new QLabel( "Patient Id:", this );
	m_editPatientId		= new QLineEdit( this );

	m_editPatientId->setAlignment( Qt::AlignCenter );
	m_editPatientId->setFixedWidth( 60 );

	layoutPatientId->addWidget( m_labelPatientId );
	layoutPatientId->addWidget( m_editPatientId );

	m_radioL			= new QRadioButton( "Left  Knee" );
	m_radioR			= new QRadioButton( "Right Knee" );

	layoutImplantSide->addWidget( m_radioL );
	layoutImplantSide->addWidget( m_radioR );

	layoutImplantSide->setAlignment( Qt::AlignCenter );
	
	m_buttonOK			= new QPushButton( "OK" );
	m_buttonCancel		= new QPushButton( "Cancel" );

	m_buttonOK->setFixedWidth( 80 );
	m_buttonCancel->setFixedWidth( 80 );

	layoutButtons->addWidget( m_buttonOK );
	layoutButtons->addWidget( m_buttonCancel );

	layoutMain->addLayout( layoutPatientId );
	layoutMain->addLayout( layoutImplantSide );
	layoutMain->addLayout( layoutButtons );

	setLayout( layoutMain );

	if( nPaientId != 0 )
	{
		QString sPatientId;
		sPatientId.sprintf( "%07d", nPaientId );
		m_editPatientId->setText( sPatientId );
	}

	switch( m_nSide )
	{
		case 0: m_radioL->setChecked( true );	break;
		case 1: m_radioR->setChecked( true );	break;
	}

	connect( m_buttonOK, SIGNAL(clicked()), this, SLOT(OnOK()));
	connect( m_buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
	}


void CImplantHeaderDlg::OnOK()
{
	if( m_radioL->isChecked() )
		m_nSide = 0;

	if( m_radioR->isChecked() )
		m_nSide = 1;

	accept();
}
