#ifndef MAINWINDOWPS_H
#define MAINWINDOWPS_H

#include "..\KApp\MainWindow.h"
#include "Version.h"

class MainWindowPS : public MainWindow
{
    Q_OBJECT

public:
    MainWindowPS(bool reviewmode, bool skipCT, QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~MainWindowPS();

    // Get the application name & version
    virtual QString GetAppName()    const { return PRODUCT_STR; }
    virtual QString GetAppVersion() const { return VERSION_STR; }

	void ShowStatusPrompt(QString const&);
	void ShowStatusResult(bool result_status, QString const&, int timeout = 2000);

protected slots:
    // Open an existing implant
    virtual void OpenImplant();

    // Set options
    virtual void SetOption();

private:
	bool skipCT;
};

#endif // MAINWINDOWPS_H
