#pragma once

#include <QList.h>
#pragma warning( disable : 4996 4805 )
#include <IwContext.h>
#pragma warning( default : 4996 4805 )
#include "TriathlonF1Definitions.h"
#include "Basics.h"
#include "Part.h"
#include "OpenGLView.h"
#include "DocLogWriter.h"

#include <vector>
#include <tuple>
#include <utility>

using namespace std;

class COpenGLView;
class CEntPanel;

enum CImplantSide
	{
	IMPLANT_SIDE_UNKNOWN = -1,
	IMPLANT_SIDE_LEFT,
	IMPLANT_SIDE_RIGHT
	};

enum CImplantRegulatoryZone
	{
	IMPLANT_ZONE_UNKNOWN = -1,
	IMPLANT_ZONE_US_FDA,
	IMPLANT_ZONE_CE_MARK,
	IMPLANT_ZONE_NEW_ZONE // always keep this in the last entry
	};

class CTempPoint
	{
public:
	IwPoint3d		pt;
	CColor			color;

	CTempPoint(){}
	~CTempPoint(){}
	};

class ValidationReport : public QString
{
public:
    void clear() { QString::clear(); m_eDHR.clear(); }
    QString& append(QString const& str) { return QString::append(str); }

    // how do we add the data for eDHR?
    QString& append(QString const& str, QString const& task, double param);

    typedef std::tuple<QString,double,FILETIME> vrrecord_t;

private:
    //std::vector<std::pair<QString,double>> m_eDHR;
    static bool StartCounting();

    static FILETIME         s_start; // this will be set at program start
    static LARGE_INTEGER    s_startCPUcounter; // this too will be set
    static bool             s_timeInit;
    std::vector<vrrecord_t> m_eDHR;
};

class CDataDoc
	{
public:

	CDataDoc(QObject *parent=0);
	~CDataDoc();
	void				SetView( COpenGLView* pView ) {m_pView=pView;};
	COpenGLView*		GetView() {return m_pView;};

	virtual void		AddEntity( CEntity* pEntity, bool bAddToEntPanel = true );
	void				AddPart( CPart* pPart, bool bAddToEntPanel = true );
	bool				ImportCPart( const QString& sFileName, CEntRole eEntRole = ENT_ROLE_NONE );
	virtual bool		ImportPart( const QString& sFileName, CEntRole eEntRole = ENT_ROLE_NONE )=0;
    virtual bool		ImportPart(IwBrep* brep, CEntRole eEntRole = ENT_ROLE_NONE)=0;
    virtual bool		ImportPart(IwPoint3d pt, CEntRole eEntRole = ENT_ROLE_NONE)=0;
	bool				ImportBrep( const QString& sFileName, const CColor& color );
	void				SetEntityDisplayMode( int id, CDisplayMode eDispMode );

	int					GetNumOfEntities(bool displayedOnly=false);
	CEntity*			GetEntity( int iElement );
	CEntity*			GetEntity( CEntRole eEntRole );
	CEntity*			GetEntityById( int id );
	CPart*				GetPart( CEntRole eEntRole );
	CPart*				GetPartById( int id );
	CEntity*			GetLastEntity();
	virtual bool		DeleteEntityById( int id, bool bDestroyEntity = true );
	void				DeleteEntitiesStartingId( int id );
	void				DeleteAllEntities();
	virtual void		RemoveEntityFromEntPanel( CEntRole eEntRole );
	void				Display();              // Display the opaque entities.
    void                DisplayTransparent();   // Display the transparent entities.
	virtual void		SetEntPanelStatusMarker( CEntRole eEntRole, bool status )=0;
	virtual void		UpdateEntPanelStatusMarker( CEntRole eEntRole, bool bUpToDate=true, bool implicitlyUpdate=true )=0;
	virtual void		SetEntPanelValidateStatus( CEntRole eEntRole, CEntValidateStatus status, QString message );
	virtual void		SetEntPanelFontColor(CEntRole entRole, CColor color);
	virtual void		SetEntPanelToolTip(CEntRole entRole, QString message);
	bool				EntityExists( CEntity* pEntity );

	CBounds3d			GetBounds();
	CBounds3d			ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL )	;
	void				UpdateBounds() {m_pView->UpdateShowRegion();};

	long				GetAvlDisplayList();

	bool				IsEmpty();
	bool				IsModified();
	virtual void		SetModified( bool bModified )=0;
	bool				IsReadOnlyFile() {return m_bReadOnlyFile;};
	void				SetReadOnlyFile(bool flag) {m_bReadOnlyFile=flag;};
	bool				IsFileSaveAuto() {return m_bFileSaveAuto;};
	void				SetFileSaveAuto(bool falg) {m_bFileSaveAuto=falg;};
	void				FileSaveAuto();

	IwContext&			GetIwContext();

	virtual void		Clear();

	void				SetEntPanel( CEntPanel* pEntPanel ) {m_pEntPanel=pEntPanel;};
	virtual void		ToggleEntPanelItemByID(int id, bool On);

	virtual bool		Save( const QString& sDocFileName, bool incrementalSave=false )=0;
	void				SavePostProcessing( const QString& sDocFileName );
	virtual bool		Load( const QString& sDocFileName )=0;
	virtual bool		LoadWithOptions( const QString& sDocFileName, QString options=QString("") ) {return false;};
	bool				LoadPreProcessing( const QString& sDocFileName );
	void				InitializeLogFile(const QString logFileDir=QString());
	void				AppendLog(const QString& logString, bool newLine=true);
	void				TerminateLogFile();
	void				SetLogFileDir(const QString& logFileDir);
	void				StartTimeLog(string strStepname);
	void				EndTimeLog(string strStepname);
	bool				GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<bool>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);
	QString				SaveTemp();
	void				SaveAnotherCopy(QString sourceDir, QString targetDir);

	void				DisplayOnlyPart( int id );
	virtual void		TogglePartDisplayMode( int id, CDisplayMode displayMode )=0;

	int					AddPreviousUserInfo(QString& prevUserInfo);
	int					GetPreviousUserInfo(QList<QString>& prevUserInfo);
	void				SetCurrentUserInfo(QString& currUserInfo) {m_currentUserInfo = currUserInfo;};
	QString				GetCurrentUserInfo() {return m_currentUserInfo;};
	QString				GetCurrentUserName() {return m_currentUserName;};
	QString				GetCurrentComputerName() {return m_currentComputerName;};
	void				SetOpeningFileReleaseNumber( int rel) { m_openingFileReleaseNumber = rel; };
	void				GetOpeningFileReleaseNumber(int& rel) {rel=m_openingFileReleaseNumber; };
	void				SetOpeningFileRevisionNumber( int rev) { m_openingFileRevisionNumber = rev; };
	void				GetOpeningFileRevisionNumber(int& rev) {rev=m_openingFileRevisionNumber;};
	void				SetOpeningFileDataFileVersionNumber( int v) { m_openingFileDataFileVersionNumber = v; };
	void				GetOpeningFileDataFileVersionNumber(int&v) {v=m_openingFileDataFileVersionNumber; };
	bool				IsOpeningFileRightVersion(int Rel, int Rev, int dVer);
	QString				GetFileName() {return m_fileName;};
	void				SetFileName(QString fileName) {m_fileName=fileName;};
	QString				GetFileFolder();
	QString				GetPatientId();
	CImplantSide		GetImplantSide( QString& sImplantSideName );
	bool				IsPositiveSideLateral();
	CImplantRegulatoryZone	GetImplantZone();

	void				SetTessellationFactor(double val) {m_tessellationFactor=val;};
	double				GetTessellationFactor() {return m_tessellationFactor;};
	void				SetTransparencyFactor(double val) {m_transparencyFactor=val;};
	double				GetTransparencyFactor() {return m_transparencyFactor;};

	int					GetAvlEntId();
	void				UpdateAvlEntId( int nEntId );
	void				SetAvlEntId( int id );
	int					ParsePatientId( const QString& sFileName );

	void				DisplayTempPoints();
	void				AddTempPoint( const IwPoint3d& pt, const CColor& color );
	void				CleanUpTempPoints();

	bool				GetImplantInfo( QString& sPatientId, CImplantSide& eImplantSide, CImplantRegulatoryZone& eImplantZone );
	bool				FolderExists(QString docFolderName);
	void				TessInvisibleParts();
	QString				GetVarTableString(QString const& key);
	double				GetVarTableValue(QString const& key);
	QString				GetValidateTableString(QString const& key);
	int					GetValidateTableStatus(QString const& key);
	void				AppendValidateMessage(QString const& key, int& status, QString& message);
	bool				DisplayValidationData();
	virtual void		InitializeValidationReport()=0;
	//virtual void		AppendValidationReportEntry(QString const& entry, bool goodResult=true)=0;
	virtual void		AppendValidationReportEntry(QString const& entry, bool goodResult=true)=0;
    void                AppendEDHREntry(QString const& name, double value);
	virtual QString		GetValidationReport(bool& allGoodResults)=0;
	int					GetTotalDesignTime();
	int					GetFemurImplantDesignTime();
	int					GetFemurJigsDesignTime();

	static bool			isCruciateRetaining();
	static bool			isPosteriorStabilized();
	static bool			isProductionSoftware();

	static QString		GetFemurMechCoordSysName() { return "Fem Mech Axis"; }
	static QString		GetFemurImplantCoordSysName() { return "Fem Implant Axis"; }

public:

protected:
	QList< CEntity* >			m_EntList;

	CBounds3d					m_Bounds;
	bool						m_bModified;
	bool						m_bReadOnlyFile;
	bool						m_bFileSaveAuto;
	
	COpenGLView*				m_pView;
	CEntPanel*					m_pEntPanel;

	int							m_openingFileReleaseNumber;
	int							m_openingFileRevisionNumber;
	int							m_openingFileDataFileVersionNumber;
	QString						m_openingFileBuildNumber;

	QList<QString>				m_previousUserInfo;
	QString						m_currentUserName;
	QString						m_currentUserInfo;
	QString						m_currentComputerName;
	QString						m_fileName;
	QString						m_sPatientId;
	CImplantSide				m_eImplantSide;
	CImplantRegulatoryZone		m_eImplantZone;

	int							m_nAvlEntId;

	QList< CTempPoint >			m_tempPoints;

	double						m_tessellationFactor;
	int							m_transparencyFactor;
	QString						m_variableTableFileName;
	QString						m_validationTableFileName;

    ValidationReport            m_validationReport;
	bool						m_validationAllGoodResults;

	CDocLogWriter				m_docLogWritter;

	vector<QString>             vectOfFieldsUsed;
	};



