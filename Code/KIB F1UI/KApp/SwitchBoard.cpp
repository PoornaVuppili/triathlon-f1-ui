#include <assert.h>
#include "SwitchBoard.h"

SwitchBoard& SwitchBoard::getInstance()
{
	static SwitchBoard instance;
	return instance;
}

SwitchBoard::SwitchBoard()
{
}

SwitchBoard::~SwitchBoard()
{
	QMutableListIterator<Sender *> i(senders);
	while(i.hasNext())
	{
		i.next();
		delete i.value();
		i.remove();
	}

	QMutableListIterator<Receiver *> j(receivers);
	while(i.hasNext())
	{
		j.next();
		delete j.value();
		j.remove();
	}
}

void SwitchBoard::addSignalSender(const char *signal, QObject *sender)
{
	SwitchBoard::getInstance().senders.append(new Sender(signal, sender));
	SwitchBoard::getInstance().makeConnections();
}

void SwitchBoard::removeSignalSender(QObject *sender)
{
	QMutableListIterator<Sender *> i(SwitchBoard::getInstance().senders);
	while(i.hasNext())
	{
		i.next();
		if(i.value()->obj == sender)
		{
			QMutableListIterator<Receiver *> j(SwitchBoard::getInstance().receivers);

			while(j.hasNext())
			{
				j.next();

				if(!strcmp(i.value()->signal, j.value()->signal))
				{
					disconnect(i.value()->obj, i.value()->signal, j.value()->obj, j.value()->slot);
				}
			}

			delete i.value();
			i.remove();
		}
	}
}

void SwitchBoard::addSignalReceiver(const char *signal, QObject *receiver, const char *slot)
{
	SwitchBoard::getInstance().receivers.append(new Receiver(signal, receiver, slot));
	SwitchBoard::getInstance().makeConnections();
}

void SwitchBoard::removeSignalReceiver(QObject *rc)
{
	QMutableListIterator<Receiver *> i(SwitchBoard::getInstance().receivers);
	while(i.hasNext())
	{
		i.next();
		if(i.value()->obj == rc)
		{
			QMutableListIterator<Sender *> j(SwitchBoard::getInstance().senders);

			while(j.hasNext())
			{
				j.next();

				if(!strcmp(j.value()->signal, i.value()->signal))
				{
					disconnect(j.value()->obj, j.value()->signal, i.value()->obj, i.value()->slot);
				}
			}

			delete i.value();
			i.remove();
		}
	}
}

void SwitchBoard::makeConnections()
{
	foreach(Sender *sender, senders)
	{
		foreach(Receiver *receiver, receivers)
		{
			if(!strcmp(sender->signal, receiver->signal))
			{
				connect(sender->obj, sender->signal, receiver->obj, receiver->slot, Qt::UniqueConnection);
			}
		}
	}
}
