#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW CFemoralCuts : public CPart
{
public:
	CFemoralCuts( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_FEMORAL_CUTS, const QString& sFileName="", int nEntId=-1 );
	virtual ~CFemoralCuts();
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );

	bool LoadInputs(const QString& sDir);

	virtual void	Display();
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);
	void			DisplayFemoralCutsViewProfiles();
	void			DisplayFemoralCutsOuterSideSurfacesIntersection();
	void			DisplayCutEdgeLines();
	bool			IsMemberDataChanged(IwTArray<IwPoint3d>&, IwTArray<IwVector3d>&);
	bool			IsMemberDataChanged(IwTArray<IwPoint3d>&, IwTArray<IwVector3d>&, IwTArray<IwPoint3d>&, IwTArray<IwVector3d>&);
	void			GetCutsInfo(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations);
	void			SetCutsInfo(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations, IwBrep* Brep=NULL);
	void			SetCutsInfoOnly(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations);
	void			GetFeaturePoints(IwTArray<IwPoint3d>& featurePoints, IwTArray<IwPoint3d>* antOuterPoints);
	void			GetFeaturePointsAtVertexes(IwTArray<IwPoint3d>& featurePoints);
	bool			DetermineFeaturePoints(bool withDefaultDistToEdge=true);
	bool			DetermineEdgeFeaturePoints(IwTArray<IwPoint3d>* cutFeaturePoints, IwTArray<IwPoint3d>& edgeFeaturePoints);
	bool			GetAntPostInfo(IwTArray<IwPoint3d> &antPostInfo);
	bool			GetCondylarCutCenters(IwPoint3d& posCenter, IwPoint3d& negCenter);
	bool			GetCondylarCutWidths(double& posWidth, double& negWidth);
	void			SetDisplayOffsetViewProfile(bool flag);
	bool			GetDisplayOffsetViewProfileFlag() {return m_bOffsetViewProfileDisplay;};
	void			SetOuterSideIntersectionCurves(IwTArray<IwCurve*>&);
	void			GetOuterSideIntersectionCurves(IwTArray<IwCurve*>&);
	void			SetDisplayOuterSideIntersectionCurves(bool flag);
	bool			GetDisplayOuterSideIntersectionCurves() {return m_bOuterSideSurfaceIntersectionDisplay;};
	void			SetThicknessCurves(IwTArray<IwCurve*>&);
	void			GetThicknessCurves(IwTArray<IwCurve*>&);
	void			SetThicknessCurvesNotch(IwTArray<IwCurve*>&);
	void			GetThicknessCurvesNotch(IwTArray<IwCurve*>&);
	void			UpdateCutEdges();
	void			DetermineCutEdge(int prevCutIndex, int nextCutIndex, double extraNotchGapDist, IwBSplineCurve*& cutEdgeCurve);
	void			DetermineCutPlaneEdges(IwTArray<IwCurve*>&);
	bool			SearchCutFaces(int &posIndex, IwTArray<IwFace*>& cutFaces);
	static bool		SearchCutFaces(IwBrep*& pBrep, IwPoint3d& cutPoint, IwVector3d& cutVector, IwTArray<IwFace*>& cutFaces);
	static bool		IsAllCutsExist(IwBrep*& pBrep, IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutVectors);
	void			GetCutEdgeCurves(IwTArray<IwCurve*>& cutEdgeCurves);
	void			SetNotchCutInfo(IwPoint3d& notchCutPoint, IwVector3d& notchCutOrientation) {m_cutPoints.SetAt(10,notchCutPoint);m_cutOrientations.SetAt(10,notchCutOrientation);};
	void			GetNotchCutInfo(IwPoint3d& notchCutPoint, IwVector3d& notchCutOrientation) {notchCutPoint=m_cutPoints.GetAt(10);notchCutOrientation=m_cutOrientations.GetAt(10);};
	void			SetInitialCutEdgePoints(IwTArray<IwPoint3d>& cutEdgePoints) {m_cutEdgePoints.RemoveAll();m_cutEdgePoints.Append(cutEdgePoints);};
	void			GetInitialCutEdgePoints(IwTArray<IwPoint3d>& cutEdgePoints) {cutEdgePoints.RemoveAll();cutEdgePoints.Append(m_cutEdgePoints);};
	void			DetermineThicknessCurves(double thickness, IwBSplineCurve*& posPosCurve, IwBSplineCurve*& posNegCurve, IwBSplineCurve*& negPosCurve, IwBSplineCurve*& negNegCurve);
	void			UpdateAntPostTipThicknessInfo();
	bool			GetCutEdgeAngles(double& A0, double& A1, double& posA2, double& posA3, double& posA4, double& negA2, double& negA3, double& negA4);
	double			GetAnteriorFlangeRatio(IwPoint3d &antAntChamEdgePoint);
	bool			MoveAnteriorCut(IwPoint3d refPoint);
	void			SetAdditionalDistalCutsDistance(double val) {m_additionalDistalCutsDistance=val;};
	double			GetAdditionalDistalCutsDistance() {return m_additionalDistalCutsDistance;};
	void			SetForceToOptiomizeCuts(bool bForceToOpt){m_forceToOptimizeCuts=bForceToOpt;};
	bool			GetForceToOptiomizeCuts(){return m_forceToOptimizeCuts;};

	void			GetCutsDelta(double& distalDelta, double& postChamferCutDelta1, double& postChamferCutDelta2, double& posteriorDelta);

	////// For Validation //////////////////
	void			SetCutsInfoValidation(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations);
	void			GetCutsInfoValidation(IwTArray<IwPoint3d>& cutPoints, IwTArray<IwVector3d>& cutOrientations);

private:
	bool			SearchCommonCutEdges(int &posIndex1, int &posIndex2, IwTArray<IwEdge*> &commonCutEdges, int howToSortEdges=0);
	bool			CalculateMostPosteriorPointOnPosteriorFaceEdges(IwFace* face, IwPoint3d& extremePnt, double *distanceToCutEdge=NULL);
	bool			SamplingPointsOnFaceOuterEdge(IwFace* face, IwVector3d faceNormal, 
							IwTArray<IwPoint3d>& connectedPnts, double &samplingDist, IwTArray<IwEdge*>& edgesToAvoid, IwTArray<IwPoint3d>& samplingPnts);
	void			SetOffsetViewProfileModifiedFlag(bool flag);
	void			SetOuterSideSurfaceIntersectionModifiedFlag(bool flag);
	void			SetCutEdgesModifiedFlag(bool flag);

	//psv---
	bool DetermineAntFeaturePoints();
	bool            ApproxCommonEdgeCurveForTwoNonInterFaces(int &posIndex1, int &posIndex2, IwCurve *& commonEdgeCurve);

private:
	long					m_glOffsetViewProfileCurveList;
	long					m_glOuterSideSurfaceIntersectionCurveList;
	long					m_glCutEdgesCurveList;
	IwBrep*					m_pInitialBrep;		// the initial (uncut) femur Brep
	IwTArray<IwPoint3d>		m_cutPoints;		// It should contain (by order) positive post cut point, negative post cut
												// positive 2nd post chamfer cut, negative 2nd post chamfer cut,
												// positive 1st post chamfer cut, negative 1st post chamfer cut,
												// positive distal chamfer cut, negative distal cut,
												// anterior chamfer cut, anterior cut.
												// **** NOTICE - we have NOT handle the 11th cut yet. ****
	IwTArray<IwVector3d>	m_cutOrientations;  // The the order as the m_cutPoints. The direction is toward femur.
	IwTArray<IwBSplineCurve*>	m_offsetViewProfileCurves; // These are the offset view profiles.
	bool					m_bOffsetViewProfileDisplay;	// Display offsetViewProfile or not
	bool					m_bOffsetViewProfileModified;	// femoral cuts view profile is modified or not
	IwTArray<IwCurve*>		m_outerSideIntersectionCurves;	// the intersection curves between side surface and outer surface
	bool					m_bOuterSideSurfaceIntersectionDisplay;	// Display m_outerSideIntersectionCurves or not
	bool					m_bOuterSideSurfaceIntersectionModified;	// femoral cuts side surface intersection is modified or not
	IwTArray<IwCurve*>		m_thicknessCurves;	// the thickness offset from m_outerSideIntersectionCurves
	IwTArray<IwCurve*>		m_thicknessCurvesNotch;	// the thickness offset from m_outerSideIntersectionCurves near notch
	IwTArray<IwCurve*>		m_cutEdgeCurves;	// the curves to indicate the latest cut edges
	bool					m_bCutEdgesModified;
	bool					m_bFCDataModified;

	bool					m_isFeatPntsUpToDate;
	IwTArray<IwPoint3d>		m_FeaturePoints;	// Feature points to locate outline profile
	IwTArray<IwPoint3d>		m_AntOuterPoints;	// anterior cut outer points to help define outline profile in ant cut, total 9 points
	double					m_additionalDistalCutsDistance; // The distal cuts have to be additionally cut by this distance. 
															// Usually only when the solid implant is created and analyzed.  
															// If the notch critical thickness around the step cut fillet is too small.
	bool					m_forceToOptimizeCuts;

	/////////////////////////////////////////////////////////////////////////////////
	// The following data members are available only when run OnInitializeJOCSteps()
	/////////////////////////////////////////////////////////////////////////////////
	IwPoint3d				m_antCutPoint;		// initial ant cut point
	IwPoint3d				m_antCutOrientation;// initial ant cut orientation
	IwTArray<IwPoint3d>		m_cutEdgePoints;	// estimated cut edge points
												// 0: ant/ant chamfer cut edge point,
												// 1: ant chamfer/dist cut edge point in medial side,
												// 2: ant chamfer/dist cut edge point in lateral side,
												// 3: dist/1st post cut edge point in medial side, 
												// 4: 1st/2nd post cut edge point in medial side, 
												// 5: 2nd/post cut edge point in medial side, 
												// 6: post tip point in medial side, 
												// 7: dist/1st post cut edge point in lateral side, 
												// 8: 1st/2nd post cut edge point in lateral side, 
												// 9: 2nd/post cut edge point in lateral side, 
												//10: post tip point in lateral side, 
	//// For Validation /////////////////
	IwTArray<IwPoint3d>		m_cutPointsValidation;			// See comments above
	IwTArray<IwVector3d>	m_cutOrientationsValidation;	// See comment above


	IwTArray<IwPoint3d>		m_edgeFeaturePoints;
};