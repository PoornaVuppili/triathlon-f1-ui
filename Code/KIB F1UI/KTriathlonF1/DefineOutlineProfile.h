#pragma once

#include <QtGui>
#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Manager.h"
#include "OutlineProfileMgrUIBase.h"

class CTotalDoc;
class CTotalView;
class OutlineProfileMgrModel;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class acts as a referee on which UI manager is active 2D or 3D
//
// In order to do that the class keeps track of a Manager Model object (OutlineProfileMgrModel.h)
// and it passes the instance to the active UI manager upon creation.
// When an UI manager (OutlineProfileMgrUI2D/3D) is created this class breaks the Qt connections from previous
// manager and establishes the connections with the new manager. 
//
// All user events are redirected to the current UI manager.
//
// The combinations of DefineOutlineProfile/OutlineProfileMgrModel/OutlineProfileMgrUIBase
// mimics the design of Model/View/Controller
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class TEST_EXPORT_TW CDefineOutlineProfile : public CManager
{
	Q_OBJECT

public:
    CDefineOutlineProfile( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	~CDefineOutlineProfile();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

#pragma region Static_Methods
	//////////////////////////////////////////////////////////////
	//////////STATIC methods should probably be on their own somewhere
	//////////////////////////////////////////////////////////////
	static void		CreateOutlineProfileObject(CTotalDoc* pDoc);
	//static void		InitializeNotchFeatures(CTotalDoc* pDoc);
	static bool		DetermineNotchFeatureProfiles(CTotalDoc* pDoc, IwBSplineSurface*& thicknessSurface, IwBSplineCurve*& notchProfile, IwBSplineCurve*& medialEnghteenCurve, IwBSplineCurve*& lateralEnghteenCurve);
	static bool		EstimateNotchOutlineProfileFeaturePoints(CTotalDoc* pDoc, IwBSplineSurface*& thicknessSurface, IwBSplineCurve*& notchProfile, IwBSplineCurve*& medialEnghteenCurve, IwBSplineCurve*& lateralEnghteenCurve, IwTArray<IwBSplineCurve*>& notchOutlineProfileCurves, IwTArray<IwPoint3d>& notchOutlineProfileFeaturePoints);
	static IwBSplineCurve*		DetermineSketchProfile(CTotalDoc* pDoc);
	static bool		DetermineSketchSurface(CTotalDoc* pDoc, IwBSplineSurface*& sketchSurface);
	//////////////////////////////////////////////////////////////
	//////////END OF STATIC methods should probably be on their own somewhere
	//////////////////////////////////////////////////////////////
#pragma endregion

	virtual void Display(Viewport* vp=NULL);

	//////////////////////////////////////////////////////////
	virtual void CalculateSketchSurfaceNormalInfo(IwBSplineSurface*& skchSurface);
	virtual void DetermineViewingNormalRegion();
	void RefineAnteriorOuterPoints(IwTArray<IwPoint3d>& cutFeatPnts, IwTArray<IwPoint3d>& antOuterPnts);
	//////////////////////////////////////////////////////////

    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
	virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool    MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp=NULL );
	virtual bool    MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* vp=NULL );

	bool BaseMouseMove(const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp);

	void DetermineOutlineProfile();

	void GetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData);
	void SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData);
	void SetValidateIcon(CEntValidateStatus vStatus, QString message);
	void EnableAddAction(bool enable)		{		EnableAction( m_actAdd, enable); }
	void EnableDelAction(bool enable)		{		EnableAction( m_actDel, enable); }

private:
	void ConnectMgrUI();
	void DisconnectMgrUI();

private slots:
	void OnToggleMode();
	void OnAccept();
	void OnCancel();
	virtual bool OnReviewBack();
	virtual bool OnReviewNext();
	virtual bool OnReviewRework();

private:
	bool			m_bUIType; // true -> 2D, false -> 3D
	QAction*		m_actDefineOutlineProfile;
	QAction*		m_actReset;
	QAction*		m_actRefine;
	QAction*		m_actDispRuler;
	QAction*		m_actAdd;
	QAction*		m_actDel;
	QAction*		m_actValidate;
	QAction*		m_actToggleMode;
	QAction*		m_actReviewBack;
	QAction*		m_actReviewNext;
	QAction*		m_actReviewRework;

	OutlineProfileMgrUIBase*	m_mgrUICurrent;
	OutlineProfileMgrUIBase*	m_mgrUI3D;
	OutlineProfileMgrUIBase*	m_mgrUI2D;
	OutlineProfileMgrModel*		m_mgrModel;

	CTotalDoc*					m_pDoc;
};