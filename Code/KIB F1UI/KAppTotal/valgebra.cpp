#include "Valgebra.h"

CColor SevenColors[7] = {
	CColor( 255, 255, 255 ),
	CColor( 255,   0,   0 ),
	CColor(   0, 255,   0 ),
	CColor(   0,   0, 255 ),
	CColor(   0, 255, 255 ),
	CColor( 255,   0, 255 ),
	CColor( 255, 255,   0 )
	};


bool IsPointInsideTriangle (
    const CPoint3d&		pt,
    const CPoint3d&		pt1,
    const CPoint3d&		pt2,
    const CPoint3d&		pt3 )
	{
	CPoint2d	p( pt[0], pt[1] );
	CPoint2d	p1( pt1[0], pt1[1] );
	CPoint2d	p2( pt2[0], pt2[1] );
	CPoint2d	p3( pt3[0], pt3[1] );
	CPoint2d	a, b;
	double		f1, f2;

	// Min-Max tests
	if( p[0] < p1[0] && p[0] < p2[0] && p[0] < p3[0] )
		return false;

	if( p[0] > p1[0] && p[0] > p2[0] && p[0] > p3[0] )
		return false;

	if( p[1] < p1[1] && p[1] < p2[1] && p[1] < p3[1] )
		return false;

	if( p[1] > p1[1] && p[1] > p2[1] && p[1] > p3[1] )
		return false;

	// Check if the point's projection is on the same side
	// of all three sides of triangle
	a = p - p1;
	b = p2 - p1;
	f1 = Cross2d( a, b );

	a = p - p2;
	b = p3 - p2;
	f2 = Cross2d( a, b );

	if( ( f1 >= 0.0 ) != ( f2 >= 0.0 ) )
		return false;

	f1 = f2;

	a = p - p3;
	b = p1 - p3;
	f2 = Cross2d( a, b );

	if( ( f1 >= 0.0 ) != ( f2 >= 0.0 ) )
		return false;

	return true;
	}


bool IsPointInsideQuad (
    const CPoint3d&		pt,
    const CPoint3d&		pt1,
    const CPoint3d&		pt2,
    const CPoint3d&		pt3,
    const CPoint3d&		pt4 )
	{
	CPoint2d	p( pt[0], pt[1] );
	CPoint2d	p1( pt1[0], pt1[1] );
	CPoint2d	p2( pt2[0], pt2[1] );
	CPoint2d	p3( pt3[0], pt3[1] );
	CPoint2d	p4( pt4[0], pt4[1] );
	CPoint2d	a, b;
	double		f1, f2;

	// Min-Max tests
	if( p[0] < p1[0] && p[0] < p2[0] && p[0] < p3[0] && p[0] < p4[0] )
		return false;

	if( p[0] > p1[0] && p[0] > p2[0] && p[0] > p3[0] && p[0] > p4[0] )
		return false;

	if( p[1] < p1[1] && p[1] < p2[1] && p[1] < p3[1] && p[1] < p4[1] )
		return false;

	if( p[1] > p1[1] && p[1] > p2[1] && p[1] > p3[1] && p[1] > p4[1] )
		return false;

	// Check if the point's projection is on the same side
	// of all three sides of triangle

	a = p - p1;
	b = p2 - p1;
	f1 = Cross2d( a, b );

	a = p - p2;
	b = p3 - p2;
	f2 = Cross2d( a, b );

	if( ( f1 >= 0.0 ) != ( f2 >= 0.0 ) )
		return false;

	f1 = f2;

	a = p - p3;
	b = p4 - p3;
	f2 = Cross2d( a, b );

	if( ( f1 >= 0.0 ) != ( f2 >= 0.0 ) )
		return false;

	f1 = f2;

	a = p - p4;
	b = p1 - p4;
	f2 = Cross2d( a, b );

	if( ( f1 >= 0.0 ) != ( f2 >= 0.0 ) )
		return false;

	return true;
	}


bool CPoint3d::False()
	{ 
	return false;
	}



bool FindCircleCenter( CPoint3d pts[3], CPoint3d& center )
	{
	CPoint3d	p1, p2, pt1, pt2;
	CVector3d	a, b, nm, v1, v2;
	double		c0, c1, c2, det, s, t;
	bool		bOK;

	a = pts[1] - pts[0];
	b = pts[2] - pts[0];

	bOK = a.Normalize();
	if( !bOK )
		return false;

	bOK = b.Normalize();
	if( !bOK )
		return false;

	nm = Cross3d( a, b );
	
	bOK = nm.Normalize();
	if( !bOK )
		return false;

	p1 = ( pts[0] + pts[1] ) / 2.0;
	p2 = ( pts[0] + pts[2] ) / 2.0;

	v1 = Cross3d( nm, a );
	v2 = Cross3d( nm, b );

	c0 = v1 * v2;
	c1 = ( p1 - p2 ) * v1;
	c2 = ( p1 - p2 ) * v2;

	det = 1.0 - c0 * c0;

	if( fabs( det ) < 1e-10 )
		return false;

	s = ( c2 - c1 * c0 ) / det;
	t = ( c0 * c2 - c1 ) / det;

	pt1 = p1 + v1 * t;
	pt2 = p2 + v2 * s;

	center = ( pt1 + pt2 ) / 2.0;

	return true;
	}


bool SolveLinearSystem2x2( 
	const CPoint2d&		a, 
	const CPoint2d&		b, 
	const CPoint2d&		c, 
	double&				x, 
	double&				y )
	{
	double		det, detX, detY;

	det = a[0] * b[1] - a[1] * b[0];

	if( fabs( det ) < 1e-10 )
		return false;

	detX = c[0] * b[1] - c[1] * b[0];
	detY = a[0] * c[1] - a[1] * c[0];

	x = detX / det;
	y = detY / det;

	return true;
	}


bool IntersectLines( 
	CPoint2d&		p0, 
	CPoint2d&		v0,
	CPoint2d&		p1, 
	CPoint2d&		v1,
	CPoint2d&		pnt )
	{
	double			det, s, t;
	double			eps = 1e-4;
	CPoint2d		r, pt1, pt2;

	det = v0[1] * v1[0] - v0[0] * v1[1];

	if( fabs( det ) < eps )
		return false;

	r = p1 - p0;

	t = ( r[1] * v1[0] - r[0] * v1[1] ) / det;
	s = ( v0[0] * r[1] - v0[1] * r[0] ) / det;

	pt1 = p0 + t * v0;
	pt2 = p1 + s * v1;

	pnt = pt1;

	return true;
	}


bool IntersectLines( 
	CPoint3d&		p0, 
	CPoint3d&		p1,
	CPoint3d&		q0, 
	CPoint3d&		q1,
	CVector3d		norm,
	CPoint3d&		pnt,
	bool			bInfiniteLines )
	{
	double			w0, w1, d;
	double			eps = 1e-4;
	CVector3d		vec, v0, v1;
	CPoint3d		pt;

	norm.Normalize();

	vec = p1 - p0;
	vec.Normalize();

	v0 = q0 - p0;
	v1 = q1 - p0;

	w0 = Cross3d( vec, q0 - p0 ) * norm;
	w1 = Cross3d( vec, q1 - p0 ) * norm;

	if( fabs( w0 ) < eps )
		w0 = 0.0;

	if( fabs( w1 ) < eps )
		w1 = 0.0;

	if( fabs( w1 - w0 ) < eps )
		return false;

	if( w0 * w1 > 0.0 )
		return false;

	pt = ( w1 * q0 - w0 * q1 ) / ( w1 - w0 );

	if( !bInfiniteLines )
		{
	d = ( pt - p0 ) * vec;

	if( d < -0.01 )
		return false;
		}

	pnt = pt;
	return true;
	}


bool IntersectLines( 
	CPoint3d&		p0, 
	CVector3d&		v0,
	CPoint3d&		p1, 
	CVector3d&		v1,
	CVector3d		norm,
	CPoint3d&		pnt,
	bool			bInfiniteLines )
	{
	CPoint3d		q0, q1;

	q0 = p0 + 100.0 * v0;
	q1 = p1 + 100.0 * v1;

	return IntersectLines( p0, q0, p1, q1, norm, pnt, bInfiniteLines );
	}


CPoint3d RotatePoint( CPoint3d& p0, CVector3d& axis, double angle, CPoint3d& p )
	{
	CVector3d	v0, v1, v2;
	CPoint3d	pt, p1;
	double		r, proj;

	v0 = axis;
	v0.Normalize();

	proj = ( p - p0 ) * v0;
	p1 = p0 + proj * v0;

	v1 = p - p1;
	r = v1.Length();
	v1.Normalize();

	v2 = NormalizedCross( v0, v1 );

	pt = p1 + r * cos(angle) * v1 + r * sin(angle) * v2;

	return pt;
	}


bool PierceTriangle( CPoint3d pt, CPoint3d pts[3], double& z )
	{
	CVector3d	a, b, norm;
	double		f1, f2;

	if( pt[0] < pts[0][0] && pt[0] < pts[1][0] && pt[0] < pts[2][0] )
		return false;

	if( pt[0] > pts[0][0] && pt[0] > pts[1][0] && pt[0] > pts[2][0] )
		return false;

	if( pt[1] < pts[0][1] && pt[1] < pts[1][1] && pt[1] < pts[2][1] )
		return false;

	if( pt[1] > pts[0][1] && pt[1] > pts[1][1] && pt[1] > pts[2][1] )
		return false;

	// Check if the point's projection is on the same side
	// of all three sides of triangle
	a = pt - pts[0];
	b = pts[1] - pts[0];
	a.Normalize2d();
	b.Normalize2d();
	f1 = a[0] * b[1] - a[1] * b[0];

	a = pt - pts[1];
	b = pts[2] - pts[1];
	a.Normalize2d();
	b.Normalize2d();
	f2 = a[0] * b[1] - a[1] * b[0];

	if( ( f1 > 0.0 ) != ( f2 > 0.0 ) )
		return false;

	f1 = f2;

	a = pt - pts[2];
	b = pts[0] - pts[2];
	a.Normalize2d();
	b.Normalize2d();
	f2 = a[0] * b[1] - a[1] * b[0];

	if( ( f1 > 0.0 ) != ( f2 > 0.0 ) )
		return false;

	norm = NormalizedCross( pts[1] - pts[0], pts[2] - pts[1] );

	z = pt[2] - ( pt - pts[0] ) * norm / norm[2]; 

	return true;
	}


CVector3d RotateVec( CVector3d& axis, double angle, CVector3d& vec )
	{
	CPoint3d	origin( 0.0, 0.0, 0.0 );
	CPoint3d	p( vec[0], vec[1], vec[2] );

	p = RotatePoint( origin, axis, angle, p );

	return CVector3d( p[0], p[1], p[2] );
	}


double Angle( CVector3d vec0, CVector3d vec1 )
	{
	vec0.Normalize();
	vec1.Normalize();

	return acos( vec0 * vec1 );
	}


double TriangleArea( CPoint3d& pt0, CPoint3d& pt1, CPoint3d& pt2 )
	{
	CVector3d	vec = Cross3d( pt1 - pt0, pt2 - pt0 );

	return vec.Length();
	}


bool IsPointInsideTriangle( const CPoint3d& p, CPoint3d pts[3] )
	{
	CVector3d	norm, vec, a, b;
	CPoint3d	pt;
	double		l, cs;
	double		eps = 1e-3;
	int			i, j;

	norm = NormalizedCross( pts[1] - pts[0], pts[2] - pts[1] );

	if( fabs( norm[0] ) + fabs( norm[1] ) + fabs( norm[2] ) < 0.00001 )
		return false;

	vec = p - pts[0];

	l = vec.Length();

	vec.Normalize();

	cs = vec * norm;

	if( fabs(cs) > eps )
		return false;

	pt = p - l * cs * norm;

	for( i = 0; i < 3; i++ )
		{
		j = ( i + 1 ) % 3;

		a = pts[j] - pts[i];
		b = pt - pts[i];

		double ff = MixedProduct( a, b, norm );

		if( ff < 0.0 )
			return false;
		}

	return true;
	}


double dist( CPoint3d& pt, CPoint3d& p0, CPoint3d& p1 )
	{
	CVector3d	vec1, vec2;
	double		d1, d2, h;

	vec1 = p1 - p0;
	vec2 = pt - p0;

	d1 = vec1.Length();
	d2 = vec2.Length();

	h = vec1 * vec2 / d1;

	if( fabs( d2 - h ) < 1e-12 )
		return 0.0;

	if( fabs(d2) < fabs(h) )
		return 0.0;

	return sqrt( d2 * d2 - h * h );
	}


double ProjectPointOnLine( CPoint3d& pt, CPoint3d& p0, CPoint3d& p1, CPoint3d* pnt )
	{
	CVector3d	vec1, vec2;
	double		d, t;

	vec1 = p1 - p0;
	vec2 = pt - p0;

	d = vec1.Length();

	if( d == 0.0 )
		t = 0.0;
	else
		t = ( vec1 * vec2 ) / ( d * d );

	if( pnt )
		*pnt = p0 + t * vec1;

	return t;
	}


double DistPointToSeg( CPoint3d& pt, CPoint3d& p0, CPoint3d& p1 )
	{
	double	t, d, d0, d1;

	t = ProjectPointOnLine( pt, p0, p1 );

	if( 0.0 < t && t < 1.0 )
		{
		d = dist( pt, p0, p1 );
		}
	else
		{
		d0 = dist( pt, p0 );
		d1 = dist( pt, p1 );

		d = std::min( d0, d1 );
		}

	return d;
	}


int FindNearestEdge( CPoint3d& pt, CPoint3d pts[3] )
	{
	int			i, j, idx;
	double		d, d_min;

	pt[2] = pts[0][2] = pts[1][2] = pts[2][2] = 0.0;

	d_min = HUGE_DOUBLE;

	for( i = 0; i < 3; i++ )
		{
		j = ( i + 1 ) % 3;

		d = dist( pt, pts[i], pts[j] );

		if( d < d_min )
			{
			d_min = d;
			idx = i;
			}
		}

	return idx;
	}


void ComputeBarycentricCoords( CPoint3d pts[3], CPoint3d& pt, double w[3] )
	{
	int				i, j, k;
	double			s;

	for( i = 0; i < 3; i++ )
		{
		j = ( i + 1 ) % 3;
		k = ( j + 1 ) % 3;

		w[i] = TriangleArea( pts[j], pts[k], pt );	
		}

	s = w[0] + w[1] + w[2];

	w[0] /= s;
	w[1] /= s;
	w[2] /= s;
	}


CColor ComputeBarycentricColor( 
	CPoint3d		pts[3], 
	CPoint3d&		pt, 
	CColor			colors[3] )
	{
	double			w[3];
	CColor			color;

	if( colors[0] == colors[1] && colors[1] == colors[2] )
		return colors[0];

	ComputeBarycentricCoords( pts, pt, w );

	color = lincom( w[0], colors[0], w[1], colors[1], w[2], colors[2] );

	return color;
	}


CPoint2d ConstructPolygonInnerPoint( int n, CPoint2d p[] )
	{
	int			i, j, i_max;
	double		d, d_max, x0, y0, x1, y1;
	double		t, y, y_min;
	CPoint2d	pt, p0, vec_x, vec_y;

	d_max = 0.0;

	for( i = 0; i < n; i++ )
		{
		j = ( i + 1 ) % n;

		d = dist( p[i], p[j] );

		if( d > d_max )
			{
			d_max = d;
			i_max = i;
			}
		}

	i = i_max;
	j = ( i + 1 ) % n;

	p0 = ( p[i] + p[j] ) / 2.0;

	vec_x = p[j] - p[i];
	vec_x /= d_max;

	vec_y[0] = -vec_x[1];
	vec_y[1] =  vec_x[0];

	y_min = HUGE_DOUBLE;

	for( i = 0; i < n; i++ )
		{
		if( i == i_max )
			continue;

		j = ( i + 1 ) % n;

		x0 = Dot2d( p[i] - p0, vec_x );
		y0 = Dot2d( p[i] - p0, vec_y );

		x1 = Dot2d( p[j] - p0, vec_x );
		y1 = Dot2d( p[j] - p0, vec_y );

		if( x1 == x0 )
			continue;

		if( x0 * x1 > 0.0 )
			continue;

		t = ( 0.0 - x0 ) / ( x1 - x0 );
		y = ( 1.0 - t ) * y0 + t * y1;

		if( y <= 0.0 )
			continue;

		y_min = std::min( y_min, y );
		}

	d = y_min / 2.0;

	pt = p0 + d * vec_y;

	return pt;
	}


bool SameVectors( CVector3d& a, CVector3d& b )
	{
	double	eps = 1e-3;

	if( fabs( a[0] - b[0] ) < eps &&
		fabs( a[1] - b[1] ) < eps &&
		fabs( a[2] - b[2] ) < eps ||

		fabs( a[0] + b[0] ) < eps &&
		fabs( a[1] + b[1] ) < eps &&
		fabs( a[2] + b[2] ) < eps )

		return true;
	else
		return false;
	}


bool IsPointInsideFrame( 
	const CPoint3d&		pt, 
	const CPoint3d&		p0, 
	const CPoint3d&		p1 )
	{
	if( pt[0] < p0[0] || pt[0] > p1[0] )
		return false;

	if( pt[1] < p0[1] || pt[1] > p1[1] )
		return false;

	return true;
	}


bool Intersect2dSegments( 
	CPoint3d&		p00, 
	CPoint3d&		p01,
	CPoint3d&		p10, 
	CPoint3d&		p11,
	CPoint3d&		pnt )
	{
	double			d, t, f0, f1, s;
	double			eps = 1e-4;
	CPoint2d		vec, v, v0, v1, pt;

	CPoint2d		p0( p00[0], p00[1] );
	CPoint2d		p1( p01[0], p01[1] );
	CPoint2d		q0( p10[0], p10[1] );
	CPoint2d		q1( p11[0], p11[1] );

	d = dist( p0, p1 );

	if( d < eps )
		return false;

	vec = Normalize2d( p1 - p0 );

	v0 = q0 - p0;
	v1 = q1 - p0;

	f0 = Cross2d( vec, v0 );
	f1 = Cross2d( vec, v1 );

	if( f0 > 0.0 && f1 > 0.0 || f0 < 0.0 && f1 < 0.0 )
		return false;

	f0 = fabs( f0 );
	f1 = fabs( f1 );
	
	s = f0 + f1;

	if( s < eps )
		return false;

	f0 /= s;
	f1 /= s;

	pt = f1 * q0 + f0 * q1;

	v = pt - p0;

	t = Dot2d( vec, v ) / d;

	if( t < 0.0 || t > 1.0 )
		return false;

	pnt = CPoint3d( pt[0], pt[1], 0.0 );

	return true;
	}


int IntersectTriangleAndQuad( 
	CPoint3d		tp[3], 
	CPoint3d		qp[4], 
	CPoint3d		pts[] )
	{
	double			xq_min, yq_min, xq_max, yq_max;
	double			xt_min, yt_min, xt_max, yt_max;
	int				i, j, i1, j1, np = 0;
	CPoint3d		pt;

	xq_min = yq_min = xt_min = yt_min =  HUGE_DOUBLE;
	xq_max = yq_max = xt_max = yt_max = -HUGE_DOUBLE;

	for( i = 0; i < 4; i++ )
		{
		xq_min = std::min( xq_min, qp[i][0] );
		yq_min = std::min( yq_min, qp[i][1] );
		xq_max = std::max( xq_max, qp[i][0] );
		yq_max = std::max( yq_max, qp[i][1] );
		}

	for( j = 0; j < 3; j++ )
		{
		xt_min = std::min( xt_min, tp[j][0] );
		yt_min = std::min( yt_min, tp[j][1] );
		xt_max = std::max( xt_max, tp[j][0] );
		yt_max = std::max( yt_max, tp[j][1] );
		}

	if( xq_max < xt_min || xt_max < xq_min )
		return false;

	if( yq_max < yt_min || yt_max < yq_min )
		return false;

	for( i = 0; i < 4; i++ )
		{
		if( IsPointInsideTriangle( qp[i], tp[0], tp[1], tp[2] ) )
			pts[np++] = qp[i];
		}

	for( j = 0; j < 3; j++ )
		{
		if( IsPointInsideQuad( tp[j], qp[0], qp[1], qp[2], qp[3] ) )
			pts[np++] = tp[j];
		}

	for( i = 0; i < 4; i++ )
		{
		i1 = ( i + 1 ) % 4;

		for( j = 0; j < 3; j++ )
			{
			j1 = ( j + 1 ) % 3;

			if( Intersect2dSegments( qp[i], qp[i1], tp[j], tp[j1], pt ) )
				pts[np++] = pt;
			}
		}

	return np;
	}


bool TriangleIntersectPlane( 
	CPoint3d		pts[3], 
	CPoint3d		pt, 
	CVector3d		nm,
	int				idx[2],
	CPoint3d		p[2] )
	{
	int				i, j, k, n;
	double			d[3];
	double			eps = 1e-6;

	n = 0;

	for( i = 0; i < 3; i++ )
		{
		d[i] = ( pts[i] - pt ) * nm;
		
		if( fabs( d[i] ) < eps )
			d[i] = 0.0;
		}

	for( i = 0; i < 3; i++ )
		{
		if( d[i] == 0.0 )
			{
			j = ( i + 1 ) % 3;
			k = ( i + 2 ) % 3;

			if( d[j] > 0.0 && d[k] < 0.0 ||
				d[j] < 0.0 && d[k] > 0.0 )
				{
				idx[0] = i;
				p[0] = pts[i];

				idx[1] = j;
				p[1] = ( d[k] * pts[j] - d[j] * pts[k] ) / ( d[k] - d[j] );

				return true;
				}
			}
		}

	for( i = 0; i < 3; i++ )
		{
		j = ( i + 1 ) % 3;

		if( d[i] > 0.0 && d[j] < 0.0 ||
			d[i] < 0.0 && d[j] > 0.0 )
			{
			idx[n] = i;
			p[n] = ( d[j] * pts[i] - d[i] * pts[j] ) / ( d[j] - d[i] );
			n++;
			}
		}

	return n == 2;
	}


bool TrianglesIntersectEachOther( 
	CPoint3d		p[3], 
	CPoint3d		q[3],
	int				idxFace[2],
	int				idxEdge[2],
	CPoint3d		pts[2] )
	{
	int				i, p_idx[2], q_idx[2];
	double			p_min, p_max, q_min, q_max, t0, t1, dp, dq;
	CPoint3d		p_pts[2], q_pts[2], p_pt, q_pt;
	CVector3d		p_nm, q_nm, p_vec, q_vec;
	bool			bIntersect;
	double			eps = 1e-8;

	for( i = 0; i < 3; i++ )
		{
		p_min = min3( p[0][i], p[1][i], p[2][i] );
		p_max = max3( p[0][i], p[1][i], p[2][i] );
		
		q_min = min3( q[0][i], q[1][i], q[2][i] );
		q_max = max3( q[0][i], q[1][i], q[2][i] );
		
		if( p_max < q_min || p_min > q_max )
			return false;
		}

	p_nm = NormalizedCross( p[1] - p[0], p[2] - p[1] );
	q_nm = NormalizedCross( q[1] - q[0], q[2] - q[1] );

	p_pt = p[0];
	q_pt = q[0];

	if( !TriangleIntersectPlane( p, q_pt, q_nm, p_idx, p_pts ) )
		return false;

	if( !TriangleIntersectPlane( q, p_pt, p_nm, q_idx, q_pts ) )
		return false;

	bIntersect = false;

	p_vec = p_pts[1] - p_pts[0];
	q_vec = q_pts[1] - q_pts[0];

	dp = p_vec * p_vec;
	dq = q_vec * q_vec;

	if( dp > dq )
		{
		t0 = ( q_pts[0] - p_pts[0] ) * p_vec / dp;
		t1 = ( q_pts[1] - p_pts[0] ) * p_vec / dp;

		if( t1 < t0 )
			{
			Swap( t0, t1 );
			Swap( q_pts[0], q_pts[1] );
			Swap( q_idx[0], q_idx[1] );
			}

		if( t0 < 1.0 - eps && t1 > eps )
			{
			bIntersect = true;

			if( t0 < eps )
				{
				idxFace[0]	= 0;
				idxEdge[0]	= p_idx[0];
				pts[0]		= p_pts[0];
				}
			else
				{
				idxFace[0]	= 1;
				idxEdge[0]	= q_idx[0];
				pts[0]		= q_pts[0];
				}

			if( t1 < 1.0 - eps )
				{
				idxFace[1]	= 1;
				idxEdge[1]	= q_idx[1];
				pts[1]		= q_pts[1];
				}
			else
				{
				idxFace[1]	= 0;
				idxEdge[1]	= p_idx[1];
				pts[1]		= p_pts[1];
				}
			}
		}
	else
		{
		t0 = ( p_pts[0] - q_pts[0] ) * q_vec / dq;
		t1 = ( p_pts[1] - q_pts[0] ) * q_vec / dq;

		if( t1 < t0 )
			{
			Swap( t0, t1 );
			Swap( p_pts[0], p_pts[1] );
			Swap( p_idx[0], p_idx[1] );
			}

		if( t0 < 1.0 - eps && t1 > eps )
			{
			bIntersect = true;

			if( t0 < eps )
				{
				idxFace[0]	= 1;
				idxEdge[0]	= q_idx[0];
				pts[0]		= q_pts[0];
				}
			else
				{
				idxFace[0]	= 0;
				idxEdge[0]	= p_idx[0];
				pts[0]		= p_pts[0];
				}

			if( t1 < 1.0 - eps )
				{
				idxFace[1]	= 0;
				idxEdge[1]	= p_idx[1];
				pts[1]		= p_pts[1];
				}
			else
				{
				idxFace[1]	= 1;
				idxEdge[1]	= q_idx[1];
				pts[1]		= q_pts[1];
				}
			}
		}

	return bIntersect;
	}


bool SegmentsIntersectEachOther( 
	CPoint3d&		p00, 
	CPoint3d&		p01,
	CPoint3d&		p10, 
	CPoint3d&		p11,
	CPoint3d&		pnt )
	{
	double			d, t, f0, f1, s;
	CPoint2d		vec, v, v0, v1, pt;
	double			eps1 = 1e-4;
	double			eps2 = 1e-7;

	CPoint2d		p0( p00[0], p00[1] );
	CPoint2d		p1( p01[0], p01[1] );
	CPoint2d		q0( p10[0], p10[1] );
	CPoint2d		q1( p11[0], p11[1] );

	d = dist( p0, p1 );

	if( d < eps1 )
		return false;

	vec = Normalize2d( p1 - p0 );

	v0 = q0 - p0;
	v1 = q1 - p0;

	f0 = Cross2d( vec, v0 );
	f1 = Cross2d( vec, v1 );

	if( f0 > 0.0 && f1 > 0.0 || f0 < 0.0 && f1 < 0.0 )
		return false;

	f0 = fabs( f0 );
	f1 = fabs( f1 );
	
	s = f0 + f1;

	if( s < eps1 )
		return false;

	f0 /= s;
	f1 /= s;

	pt = f1 * q0 + f0 * q1;

	v = pt - p0;

	t = Dot2d( vec, v ) / d;

	if( t < -eps2 || t > 1.0 + eps2 )
		return false;

	pnt = CPoint3d( pt[0], pt[1], 0.0 );

	return true;
	}


bool SegmentsIntersectEachOther( 
	CPoint2d&		p0, 
	CPoint2d&		p1,
	CPoint2d&		q0, 
	CPoint2d&		q1,
	CPoint2d&		pt )
	{
	double			d, t, f0, f1, s;
	CPoint2d		vec, v, v0, v1;
	double			eps1 = 1e-4;
	double			eps2 = 1e-7;

	d = dist( p0, p1 );

	if( d < eps1 )
		return false;

	vec = Normalize2d( p1 - p0 );

	v0 = q0 - p0;
	v1 = q1 - p0;

	f0 = Cross2d( vec, v0 );
	f1 = Cross2d( vec, v1 );

	if( f0 > 0.0 && f1 > 0.0 || f0 < 0.0 && f1 < 0.0 )
		return false;

	f0 = fabs( f0 );
	f1 = fabs( f1 );
	
	s = f0 + f1;

	if( s < eps1 )
		return false;

	f0 /= s;
	f1 /= s;

	pt = f1 * q0 + f0 * q1;

	v = pt - p0;

	t = Dot2d( vec, v ) / d;

	if( t < -eps2 || t > 1.0 + eps2 )
		return false;

	return true;
	}


bool CheckForGraph( int np, CPoint2d* pts, int nsegs, CIndSeg* segs )
	{
	int			i, j, i0, i1, j0, j1;
	CPoint2d	pt;
	double		eps = 1e-8;

	for( i = 0; i < np; i++ )
		{
		for( j = i + 1; j < np; j++ )
			{
			if( dist( pts[i], pts[j] ) < eps )
				return false;
			}
		}

	for( i = 0; i < nsegs - 1; i++ )
		{
		i0 = segs[i].i0;
		i1 = segs[i].i1;

		for( j = i + 1; j < nsegs; j++ )
			{
			j0 = segs[j].i0;
			j1 = segs[j].i1;

			if( SegmentsIntersectEachOther( pts[i0], pts[i1], pts[j0], pts[j1], pt ) )
				{
				if( dist( pt, pts[i0] ) > eps &&
					dist( pt, pts[i1] ) > eps &&
					dist( pt, pts[j0] ) > eps &&
					dist( pt, pts[j1] ) > eps )
					return false;
				}
			}
		}

	return true;	
	}


CVector3d MakeNormalToVector( const CVector3d& vec )
	{
	CVector3d	v, nm;
	CVector3d	v0( 1.0, 0.0, 0.0 );
	CVector3d	v1( 0.0, 1.0, 0.0 );

	v = Normalize( vec );

	if( fabs( v * v0 ) < fabs( v * v1 ) )
		{
		nm = NormalizedCross( v, v0 );
		}
	else
		{
		nm = NormalizedCross( v, v1 );
		}

	return nm;
	}



double MakeFillet( 
	const CPoint3d&			vert, 
	const CPoint3d&			pt0, 
	const CPoint3d&			pt1,
	CPoint3d&				center,
	CPoint3d&				p0, 
	CPoint3d&				p1 ) 
	{
	double					angle, rad, d, d0, d1;
	CVector3d				vec, vec0, vec1;

	vec0 = Normalize( pt0 - vert );
	vec1 = Normalize( pt1 - vert );
	d0 = dist( pt0, vert );
	d1 = dist( pt1, vert );
	d = std::min( d0, d1 ) / 3.0;
	p0 = vert + d * vec0;
	p1 = vert + d * vec1;
	vec = Normalize( ( vec0 + vec1 ) / 2.0 );
	angle = acos( vec0 * vec1 ) / 2.0;
	center = vert + d / cos( angle ) * vec;
	rad = d * tan( angle );

	return rad;
	}


bool MakeFillet( 
	const CPoint3d&			vert, 
	const CPoint3d&			pt0, 
	const CPoint3d&			pt1,
	double					rad,
	CPoint3d&				center,
	CPoint3d&				p0, 
	CPoint3d&				p1,
	bool					bStrict ) 
	{
	double					d1, d2, angle, dist0, dist1;
	CVector3d				v0, v1, vec;

	v0 = Normalize( pt0 - vert );
	v1 = Normalize( pt1 - vert );
	vec = Normalize( ( v0 + v1 ) / 2.0 );
	angle = acos( v0 * v1 ) / 2.0;
	d1 = rad / sin( angle );
	d2 = rad / tan( angle );

	dist0 = dist( vert, pt0 );
	dist1 = dist( vert, pt1 );

	if( d2 > dist0 || d2 > dist1 )
		{
		if( bStrict )
			return false;

		d2 = std::min( dist0, dist1 );
		rad = d2 * tan( angle );
		}

	center = vert + d1 * vec;
	p0 = vert + d2 * v0;
	p1 = vert + d2 * v1;

	return true;
	}


CVector3d Rot90( CVector3d vec )
	{
	CVector3d		nm;

	nm[0] = -vec[1];
	nm[1] =  vec[0];
	nm[2] =  vec[2];

	return nm;
	}


bool IntersectLines( CPoint3d& pt00, CPoint3d& pt01, CPoint3d& pt10, CPoint3d& pt11, CPoint3d& pnt )
	{
	CPoint2d	p0( pt00[0], pt00[1] );
	CPoint2d	p1( pt01[0], pt01[1] );
	CPoint2d	q0( pt10[0], pt10[1] );
	CPoint2d	q1( pt11[0], pt11[1] );
	CPoint2d	v0 = p1 - p0;
	CPoint2d	v1 = q1 - q0;
	CPoint2d	pt;

	bool		bOK = IntersectLines( p0, v0, q0, v1, pt );

	pnt = CPoint3d( pt[0], pt[1], pt00[2] );

	return bOK;
	}


CVector3d BisectAngle( CPoint3d& vertex, CPoint3d& pt0, CPoint3d pt1 )
	{
	CVector3d		vec, vec0, vec1;

	vec0 = Normalize( pt0 - vertex );
	vec1 = Normalize( pt1 - vertex );
	vec = Normalize( ( vec0 + vec1 ) / 2.0 );

	return vec;
	}


CPoint3d ArcMiddle( CPoint3d& center, CPoint3d& pt0, CPoint3d& pt1 )
	{
	CVector3d		vec;
	CPoint3d		pt;
	double			rad;

	if( dist( pt0, pt1 ) < 0.01 )
		{
		pt = ( pt0 + pt1 ) / 2.0;
		}
	else
		{
		vec = BisectAngle( center, pt0, pt1 );
		rad = dist( center, pt0 );
		pt = center + rad * vec;
		}

	return pt;
	}


CVector3d ProjectVectorOnPlane( const CVector3d& vec, const CVector3d& nm, bool bNormalize )
	{
	CVector3d		v = vec - ( vec * nm ) * nm;

	if( bNormalize )
		v.Normalize();

	return v;
	}


CPoint3d ProjectPointOnPlane( const CPoint3d& pt, const CPoint3d& p0, const CVector3d& nm )
	{
	CVector3d		norm = Normalize( nm );
	double			cs = ( pt - p0 ) * nm;
	CPoint3d		pnt = pt - cs * nm;

	return pnt;
	}


CVector3d OrientVector( const CVector3d& vec1, const CVector3d& vec2 )
	{
	CVector3d		vec = ( vec1 * vec2 > 0.0 )?	vec1 : -vec1;

	return vec;
	}


CPoint3d LineMirror( 
	const CPoint3d&		pt0,
	const CPoint3d&		pt1,
	const CPoint3d&		pt )
	{
	CVector3d			vec;
	CPoint3d			pnt;
	double				cs;

	vec = Normalize( pt1 - pt0 );
	cs = ( pt - pt0 ) * vec;
	pnt = pt0 + cs * vec;
	pnt = 2.0 * pnt - pt;

	return pnt;
	}


bool SphereLineIntersection( 
	const CPoint3d&			center,
	double					rad,
	const CPoint3d&			pt0,	
	const CPoint3d&			pt1,
	CPoint3d				pts[2] )
	{
	CPoint3d				pnt, pt;
	CVector3d				vec;
	double					cs, d;

	pnt = ( pt0 + pt1 ) / 2.0;
	vec = Normalize( pt1 - pt0 );

	cs = ( center - pnt ) * vec; 
	pt = pnt + cs * vec;

	d = dist( center, pt );

	if( d > rad )
		return false;

	d = sqrt( rad * rad - d * d );

	pts[0] = pt - d * vec;
	pts[1] = pt + d * vec;

	return true;
	}


bool SphereLineIntersection( 
	const CPoint3d&			center,
	double					rad,
	const CPoint3d&			pnt,	
	const CVector3d&		vec,
	const CPoint3d&			guess,
	CPoint3d&				pt )
	{
	CPoint3d				p0, p1, pts[2];
	bool					bOK;

	p0 = pnt;
	p1 = pnt + vec;

	bOK = SphereLineIntersection( center, rad, p0, p1, pts );

	if( !bOK )
		return false;

	double d0 = dist( pts[0], guess );
	double d1 = dist( pts[1], guess );

	if( dist( pts[0], guess ) < dist( pts[1], guess ) )
		pt = pts[0];
	else
		pt = pts[1];

	return true;
	}


double ComputeArc( 
	const CPoint3d&			pt0, 
	const CVector3d&		axis, 
	const CPoint3d&			pt1, 
	CPoint3d&				cnt )
	{
	double					d, cs, rad;
	CVector3d				vec;

	d = dist( pt0, pt1 ) / 2.0;

	vec = Normalize( pt1 - pt0 );

	cs = axis * vec;

	rad = d / cs;

	cnt = pt0 + rad * axis;

	return rad;
	}


bool MakeArcLineFillet( 
	const CPoint3d&			cnt,				// in
	const CPoint3d&			pt0,				// in
	const CPoint3d&			pt1,				// in
	const CPoint3d&			pt2,				// in
	double					rad,				// in
	CPoint3d&				center,				// out
	CPoint3d&				pnt0,				// out
	CPoint3d&				pnt1 )				// out
	{
	double					r1, r2, d, d0, d1;
	CVector3d				v0, v1, v2, v3, v, vec, nm;
	CPoint3d				p1, p2, pts[2];

	r1 = dist( cnt, pt1 );
	r2 = r1 - rad;

	v0 = Normalize( pt0 - cnt );		// vector to pt0
	v1 = Normalize( pt1 - cnt );		// vector to pt1
	v2 = NormalizedCross( v1, v0 );		// normal to plane
	v = NormalizedCross( v2, v1 );		// tangent to the arc at pt1 with the same direction as the arc
	vec = Normalize( pt2 - pt1 );		// line vector
	v3 = NormalizedCross( v, vec );		// normal to plane with direction reflecting side of the line
	nm = NormalizedCross( vec, v3 );	// normal to line with correct direction

	p1 = pt1 + rad * nm;
	p2 = pt2 + rad * nm;

	SphereLineIntersection( cnt, r2, p1, p2, pts );

	d0 = dist( pts[0], pt1 );
	d1 = dist( pts[1], pt1 );

	center = ( d0 < d1 )?	pts[0] : pts[1];

	pnt0 = center + rad * Normalize( center - cnt );

	d = ( center - pt1 ) * vec;
	pnt1 = pt1 + d * vec;

	return true;
	}


CPoint3d ArcCenterByTwoPointsAndRadius(
	const CPoint3d&			pt0,
	const CPoint3d&			pt1,
	double					rad,
	const CVector3d&		vec )	// points from mid point toward the center
	{
	CPoint3d				pt, cnt;
	CVector3d				vec1, vec2, nm;
	double					h;

	vec1 = Normalize( pt1 - pt0 );
	
	nm = NormalizedCross( vec1, vec );
	
	vec2 = NormalizedCross( vec1, nm );
	if( vec2 * vec < 0.0 ) 
		vec2 *= -1.0;

	h = dist( pt0, pt1 ) / 2.0;
	h = sqrt( rad * rad - h * h );

	pt = ( pt0 + pt1 ) / 2.0;
		
	cnt = pt + h * vec2;

	return cnt;
	}


bool PlaneLineIntersection( 
	const CPoint3d&			ptPlane,
	const CVector3d&		nmPlane,
	const CPoint3d&			pt,
	const CVector3d&		vec,
	CPoint3d&				pnt )
	{
	double					t;
	double					eps = 1e-5;

	if( fabs( vec * nmPlane ) < eps )
		return false;

	t = ( ( ptPlane - pt ) * nmPlane ) / ( vec * nmPlane );

	pnt = pt + t * vec;

	return true;
	}


double DistPointToPlane( 
	const CPoint3d&			pt,
	const CPoint3d&			ptPlane,
	const CVector3d&		nmPlane )
	{
	return fabs( ( pt - ptPlane ) * nmPlane );
	}


CVector3d BisectChords(
	const CPoint3d&			pt0,
	const CPoint3d&			pt1,
	const CPoint3d&			pt2 )
	{
	CVector3d				v0, v1, vec;
	double					h0, h1;

	v0 = pt1 - pt0;
	v1 = pt2 - pt1;

	h0 = v0.Length();
	h1 = v1.Length();

	vec = ( v0 * h1 + v1 * h0 ) / ( h0 + h1 );

	return vec;
	}
