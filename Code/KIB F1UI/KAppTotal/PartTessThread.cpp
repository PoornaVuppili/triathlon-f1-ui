#include "PartTessThread.h"
#include "DataDoc.h"
#include "OpenGLView.h"
#include "MainWindow.h"

CPartTessThread::CPartTessThread( CEntity* pPart, int visibility )
{
	m_visibility = visibility;
	m_part = pPart;
	m_partList.empty();

	if ( m_part )
		connect(this, SIGNAL(finished()), m_part->GetDoc()->GetView(), SLOT(RedrawForced()));
}
CPartTessThread::CPartTessThread( QList< CEntity* >& partList, int visibility )
{
	m_visibility = visibility;
	m_part = NULL;
	m_partList.append(partList);

	if ( partList.size() > 0 )
	{
		CEntity* ent = partList.at(0);
		if ( ent )
		{
			COpenGLView* pView = (COpenGLView*)ent->GetDoc()->GetView();
			connect(this, SIGNAL(finished()), pView->GetMainWindow(), SLOT(LoadingFileFinished()));
		}
	}

}
CPartTessThread::~CPartTessThread()
{
}

////////////////////////////////////////////////////////////
// This is the 2nd thread. We set MakeTess(false) to not 
// update screen during tessellation.
////////////////////////////////////////////////////////////
void CPartTessThread::run()
{
	if (m_part)
	{
		CEntType entType = m_part->GetEntType();
		if ( entType == ENT_TYPE_PART ||
			 entType == ENT_TYPE_FEMORAL_PART )
		{
			CPart* part = (CPart*)m_part;
			CDisplayMode dispMode = part->GetDisplayMode();
			double tessFactor = part->GetDoc()->GetTessellationFactor();
			if ( part->GetEntRole() == ENT_ROLE_FEMUR ) // set fine tessellation if femur. 
				part->GetDoc()->GetView()->OnTessellationFine();

			if ( m_visibility == 0 ) // tess all
			{
				part->MakeTess(false);
			}
			else if ( m_visibility == 1 ) // tess visible only
			{
				if (dispMode == DISP_MODE_DISPLAY ||
					dispMode == DISP_MODE_TRANSPARENCY)
				{
					part->MakeTess(false);
				}
			}
			else if ( m_visibility == 2 ) // tess invisible only
			{
				if (dispMode != DISP_MODE_DISPLAY &&
					dispMode != DISP_MODE_TRANSPARENCY)
				{
					part->MakeTess(false);
				}
			}
			part->GetDoc()->SetTessellationFactor(tessFactor);// restore tessellation factor 
		}
	}
	else if (m_partList.size() > 0)
	{
		for (int i=0; i < m_partList.size(); i++)
		{
			CEntity *ent = m_partList.at(i);
			CEntType entType = ent->GetEntType();

			if ( entType == ENT_TYPE_PART ||
				 entType == ENT_TYPE_FEMORAL_PART )
			{
				CPart* part = (CPart*)ent;
				CDisplayMode dispMode = part->GetDisplayMode();
				double tessFactor = part->GetDoc()->GetTessellationFactor();
				if ( part->GetEntRole() == ENT_ROLE_FEMUR ) // set fine tessellation if femur. 
					part->GetDoc()->GetView()->OnTessellationFine();

				if ( m_visibility == 0 ) // tess all
				{
					if ( part->GetFacets().size() == 0 )
						part->MakeTess(false);
				}
				else if ( m_visibility == 1 ) // tess visible only
				{
					if (dispMode == DISP_MODE_DISPLAY ||
						dispMode == DISP_MODE_TRANSPARENCY)
					{
						if ( part->GetFacets().size() == 0 )
							part->MakeTess(false);
					}
				}
				else if ( m_visibility == 2 ) // tess invisible only
				{
					if (dispMode != DISP_MODE_DISPLAY &&
						dispMode != DISP_MODE_TRANSPARENCY)
					{
						if ( part->GetFacets().size() == 0 )
							part->MakeTess(false);
					}
				}
				part->GetDoc()->SetTessellationFactor(tessFactor);// restore tessellation factor 
			}
		}
	}

	exit();
}
