#pragma once

#include <QtGui>
#include "TriathlonF1Definitions.h"
#include "..\KApp\EntityItem.h"
#include "Entity.h"
#include "..\KUtility\TPoint3.h"
#include "..\KUtility\TVector3.h"

class CDataDoc;

class CEntPanel : public EntityItem
{
	Q_OBJECT

public:
	CEntPanel(QTreeView* view);
	~CEntPanel();
	CDataDoc*			GetDoc() {return m_pDoc;};
	void				SetDoc( CDataDoc* pDoc ) {m_pDoc=pDoc;};

    // Get the item id
    virtual QString GetID() const { return "unknownItem"; }
    // Get/set the status
    void GetStatus(QDomElement& elem) {}
    void SetStatus(QDomElement elem)  {}

	virtual void		Add( int id, const QString& sName, CEntRole eRole )=0;
	virtual void		DeleteById( int id );
	virtual void		ToggleById( int id, bool On );
	void				Clear();
	void				SetCheckState( int id, CDisplayMode eDispMode );
	virtual void		SetModifiedMark( int id, bool removeMark);
	virtual void		SetValidateStatus( int id, CEntValidateStatus status, QString message );
	virtual void		SetFontColor( int id, CColor color );
	virtual void		SetToolTip( int id, QString message );

    virtual void        OnItemClicked(const QModelIndex&)=0;
    virtual void        OnItemRightClicked(const QModelIndex&)=0;
    virtual void        OnItemDoubleClicked(const QModelIndex&)=0;


protected:
	virtual QStandardItem*	    FindItemById( int id );
	void				ItemClicked(const QModelIndex&);

public slots:
	void				OnDisplay();
	void				OnDisplayNet();
	void				OnTransparency();
	void				OnTransparencyNet();
	void				OnWireframe();
	void				OnHide();
	void				OnColor();
	void				OnExportStl();
	void				OnExportIges();
	void				OnDelete();
	void				OnRetessellate();

signals:
   void					AlignedFeaturesNeedToChange();
   void					FemoralJigsNeedToChange();

protected:
	struct CPanelElem
		{
		int				id;
		QString			sEntName;
		CEntRole		eEntRole;
		};

	CDataDoc*				m_pDoc;
	int						m_id;
	QList< CPanelElem >		m_list;

	QAction*			m_actDisplay;
	QAction*			m_actDisplayNet;
	QAction*			m_actTransparency;
	QAction*			m_actTransparencyNet;
	QAction*			m_actWireframe;
	QAction*			m_actHide;
	QAction*			m_actColor;
	QAction*			m_actExportStl;
	QAction*			m_actDelete;
	QAction*			m_actRetessellate;
};
