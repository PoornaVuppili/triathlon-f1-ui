#include "Measure.h"
#include "..\KUtility\SMLibInterface.h"
#include "..\KUtility\KStatusIndicator.h"
#include "..\KUtility\SMLibHelpers.h"
#include <IwFace.h>
#include <IwCircle.h>
#include <IwTopologySolver.h>

MeasureResult MeasureResult::ComputeEntityDistance(MeasureEntity& e0, MeasureEntity& e1)
{
    switch(e0.selectType)
    {
        // point 
        case SelectPoint:
            return ComputeEntityDistance(Point3Double(e0.selectValue.point.x, e0.selectValue.point.y, e0.selectValue.point.z), e1);

        // line
        case SelectLine:
        {
            switch(e1.selectType)
            {
            case SelectPoint:
                return ComputeEntityDistance(e1, e0);

            default:
                return ComputeEntityDistance(
                            LineSegment3Double(e0.selectValue.line.xs, e0.selectValue.line.ys, e0.selectValue.line.zs,
                                               e0.selectValue.line.xe, e0.selectValue.line.ye, e0.selectValue.line.ze), 
                            e0.GetHitPoint(), e1);
            }
        }

        // circle
        case SelectCircle:
        {
            switch(e1.selectType)
            {
            case SelectPoint:
            case SelectLine:
                return ComputeEntityDistance(e1, e0);

            default:
                return ComputeEntityDistance(
                        Circle3Double(Point3Double(e0.selectValue.circle.x, e0.selectValue.circle.y, e0.selectValue.circle.z),
                                      Vector3Double(e0.selectValue.circle.nx, e0.selectValue.circle.ny, e0.selectValue.circle.nz),
                                      e0.selectValue.circle.rad),
                        e0.GetHitPoint(), e1);
            }
        }

        case SelectPlane:   // plane
        {
            switch(e1.selectType)
            {
            case SelectPoint:
            case SelectLine:
            case SelectCircle:
                return ComputeEntityDistance(e1, e0);

            default:
                return ComputeEntityDistance(
                            Point3Double(e0.selectValue.plane.cx, e0.selectValue.plane.cy, e0.selectValue.plane.cz), 
                            Point3Double(e0.selectValue.plane.ux, e0.selectValue.plane.uy, e0.selectValue.plane.uz),
                            Point3Double(e0.selectValue.plane.vx, e0.selectValue.plane.vy, e0.selectValue.plane.vz),
                            e0.GetHitPoint(), e1);
            }
        }
        
        case SelectCurve:   // curve
        {
            switch(e1.selectType)
            {
            case SelectPoint:
            case SelectLine:
            case SelectCircle:
            case SelectPlane:
                return ComputeEntityDistance(e1, e0);

            default:
				{
					if ( e0.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e0.hitPoint.GetX(), e0.hitPoint.GetY(), e0.hitPoint.GetZ()), e1);
					else
						return ComputeEntityDistance(e0.selectValue.curve.curve, 
							e0.selectValue.curve.curve->GetNaturalInterval(), 
							e0.selectValue.curve.hitU, e0.GetHitPoint(), e1);
				}
            }
        }

        case SelectEdge:    // edge
        {
            switch(e1.selectType)
            {
            case SelectPoint:
            case SelectLine:
            case SelectCircle:
            case SelectPlane:
            case SelectCurve:
                return ComputeEntityDistance(e1, e0);

            default:
				{
					if ( e0.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e0.hitPoint.GetX(), e0.hitPoint.GetY(), e0.hitPoint.GetZ()), e1);
					else
						return ComputeEntityDistance(e0.selectValue.edge.edge->GetCurve(), 
							e0.selectValue.edge.edge->GetInterval(), 
							e0.selectValue.edge.hitU, e0.GetHitPoint(), e1);
				}
            }
        }

        case SelectSurface: // surface
        {
            switch(e1.selectType)
            {
            case SelectPoint:
            case SelectLine:
            case SelectCircle:
            case SelectPlane:
            case SelectCurve:
            case SelectEdge:
                return ComputeEntityDistance(e1, e0);

            case SelectSurface: // surface
				{
					if ( e0.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e0.hitPoint.GetX(), e0.hitPoint.GetY(), e0.hitPoint.GetZ()), e1);
					else if ( e1.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()), e0);
					else
						return Distance(
							e0.selectValue.surface.surface, e0.selectValue.surface.hitU, e0.selectValue.surface.hitV,
							e1.selectValue.surface.surface, e1.selectValue.surface.hitU, e1.selectValue.surface.hitV);
				}
            case SelectFace:    // face
				{
					if ( e0.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e0.hitPoint.GetX(), e0.hitPoint.GetY(), e0.hitPoint.GetZ()), e1);
					else if ( e1.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()), e0);
					else
						return Distance(
							e0.selectValue.surface.surface, e0.selectValue.surface.hitU, e0.selectValue.surface.hitV,
							e1.selectValue.face.face->GetSurface(), e1.selectValue.surface.hitU, e1.selectValue.surface.hitV);
				}
            }
        }
        break;

        case SelectFace:        // face
        {
            switch(e1.selectType)
            {
            case SelectPoint:
            case SelectLine:
            case SelectCircle:
            case SelectPlane:
            case SelectCurve:
            case SelectEdge:
            case SelectSurface:
                return ComputeEntityDistance(e1, e0);

            case SelectFace:    // face
				{
					if ( e0.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e0.hitPoint.GetX(), e0.hitPoint.GetY(), e0.hitPoint.GetZ()), e1);
					else if ( e1.IsUseHitPointToMeasure() )
						return ComputeEntityDistance(Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()), e0);
					else
						return Distance(
							e0.selectValue.face.face, e0.selectValue.face.hitU, e0.selectValue.face.hitV,
							e1.selectValue.face.face, e1.selectValue.face.hitU, e1.selectValue.face.hitV);
				}
            }
        }
        break;
    }
    kStatus.ShowMessage("Error: wrong entity type for distance measure");
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityDistance(const Point3Double& p0, MeasureEntity& e1)
{
    switch(e1.selectType)
    {
        case SelectPoint:    // point
            return Distance(p0, Point3Double(e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z));

        case SelectLine:    // line
            return Distance(p0, LineSegment3Double(
                        e1.selectValue.line.xs, e1.selectValue.line.ys, e1.selectValue.line.zs,
                        e1.selectValue.line.xe, e1.selectValue.line.ye, e1.selectValue.line.ze));

        case SelectCircle:  // circle
        {
            Point3Double ct(e1.selectValue.circle.x, e1.selectValue.circle.y, e1.selectValue.circle.z);
            Vector3Double nor(e1.selectValue.circle.nx, e1.selectValue.circle.ny, e1.selectValue.circle.nz);
            return Distance(p0, Plane3Double(ct,nor));
        }

        case SelectPlane:   // plane
        {
            Point3Double pc(e1.selectValue.plane.cx, e1.selectValue.plane.cy, e1.selectValue.plane.cz); 
            Point3Double pu = Point3Double(e1.selectValue.plane.ux, e1.selectValue.plane.uy, e1.selectValue.plane.uz) - pc;
            Point3Double pv = Point3Double(e1.selectValue.plane.vx, e1.selectValue.plane.vy, e1.selectValue.plane.vz) - pc;
            return Distance(p0, Plane3Double(pc, pu.UnitCross(pv)));
        }

        case SelectCurve:   // curve
				if ( e1.IsUseHitPointToMeasure() )
					return Distance(p0, Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()));
				else
					return Distance(p0, e1.selectValue.curve.curve);

        case SelectEdge:    // edge
				if ( e1.IsUseHitPointToMeasure() )
					return Distance(p0, Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()));
				else
					return Distance(p0, e1.selectValue.edge.edge);

        case SelectSurface: // surface
			{
				if ( e1.IsUseHitPointToMeasure() )
					return Distance(p0, Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()));
				else
					return Distance(p0, e1.selectValue.surface.surface);
			}

        case SelectFace:    // face
			{
				if ( e1.IsUseHitPointToMeasure() )
					return Distance(p0, Point3Double(e1.hitPoint.GetX(), e1.hitPoint.GetY(), e1.hitPoint.GetZ()));
				else
					return Distance(p0, e1.selectValue.face.face);
			}

        default:
            kStatus.ShowMessage("Error: wrong entity type for distance measure");
            break;
    }
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityDistance(const LineSegment3Double& ln, const Point3Float& hitPt, MeasureEntity& e1)
{
    switch(e1.selectType)
    {
    case SelectLine:    // line
        return Distance(ln, LineSegment3Double(
                        e1.selectValue.line.xs, e1.selectValue.line.ys, e1.selectValue.line.zs,
                        e1.selectValue.line.xe, e1.selectValue.line.ye, e1.selectValue.line.ze));

    case SelectCircle:  // circle
        {
            Point3Double ct(e1.selectValue.circle.x, e1.selectValue.circle.y, e1.selectValue.circle.z);
            Vector3Double nor(e1.selectValue.circle.nx, e1.selectValue.circle.ny, e1.selectValue.circle.nz);
            return Distance(ln, Plane3Double(ct,nor));
        }

    case SelectPlane:   // plane
        {
            Point3Double pc(e1.selectValue.plane.cx, e1.selectValue.plane.cy, e1.selectValue.plane.cz); 
            Point3Double pu = Point3Double(e1.selectValue.plane.ux, e1.selectValue.plane.uy, e1.selectValue.plane.uz) - pc;
            Point3Double pv = Point3Double(e1.selectValue.plane.vx, e1.selectValue.plane.vy, e1.selectValue.plane.vz) - pc;
            return Distance(ln, Plane3Double(pc, pu.UnitCross(pv)));
        }

    case SelectCurve:   // curve
        return Distance(ln, hitPt, e1.selectValue.curve.curve, e1.selectValue.curve.hitU);

    case SelectEdge:    // edge
        return Distance(ln, hitPt, e1.selectValue.edge.edge, e1.selectValue.edge.hitU);

    case SelectSurface: // surface
        return Distance(ln, hitPt, e1.selectValue.surface.surface, e1.selectValue.surface.hitU, e1.selectValue.surface.hitV);

    case SelectFace:    // face
        return Distance(ln, hitPt, e1.selectValue.face.face, e1.selectValue.face.hitU, e1.selectValue.face.hitV);

    default:
        kStatus.ShowMessage("Error: wrong entity type for distance measure");
        break;
    }
    
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityDistance(const Circle3Double& circle, const Point3Float& hitPt, MeasureEntity& e1)
{
    switch(e1.selectType)
    {
    case SelectCircle:  // circle
        {
            return Distance(circle, Plane3Double(
                        Point3Double(e1.selectValue.circle.x, e1.selectValue.circle.y, e1.selectValue.circle.z),
                        Vector3Double(e1.selectValue.circle.nx, e1.selectValue.circle.ny, e1.selectValue.circle.nz)));
        }

    case SelectPlane:   // plane
        {
            Point3Double pc(e1.selectValue.plane.cx, e1.selectValue.plane.cy, e1.selectValue.plane.cz); 
            Point3Double pu = Point3Double(e1.selectValue.plane.ux, e1.selectValue.plane.uy, e1.selectValue.plane.uz) - pc;
            Point3Double pv = Point3Double(e1.selectValue.plane.vx, e1.selectValue.plane.vy, e1.selectValue.plane.vz) - pc;
            return Distance(circle, Plane3Double(pc, pu.UnitCross(pv)));
        }

    case SelectCurve:   // curve
        return Distance(circle, hitPt, e1.selectValue.curve.curve, e1.selectValue.curve.hitU);

    case SelectEdge:    // edge
        return Distance(circle, hitPt, e1.selectValue.edge.edge, e1.selectValue.edge.hitU);

    case SelectSurface: // surface
        return Distance(circle, hitPt, e1.selectValue.surface.surface, e1.selectValue.surface.hitU, e1.selectValue.surface.hitV);

    case SelectFace:    // face
        return Distance(circle, hitPt, e1.selectValue.face.face, e1.selectValue.face.hitU, e1.selectValue.face.hitV);

    default:
        kStatus.ShowMessage("Error: wrong entity type for distance measure");
        break;
    }
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityDistance(const Point3Double& pc, const Point3Double& pu, const Point3Double& pv, const Point3Float& hitPt, MeasureEntity& e1)
{
    Plane3Double plane(pc, (pu-pc).cross(pv-pc).normalized());

    switch(e1.selectType)
    {
    case SelectPlane:   // plane
        {
            Point3Double c1(e1.selectValue.plane.cx, e1.selectValue.plane.cy, e1.selectValue.plane.cz); 
            Point3Double u1 = Point3Double(e1.selectValue.plane.ux, e1.selectValue.plane.uy, e1.selectValue.plane.uz);
            Point3Double v1 = Point3Double(e1.selectValue.plane.vx, e1.selectValue.plane.vy, e1.selectValue.plane.vz);
            if( plane.IsParallel(Plane3Double(c1, ((u1-c1).cross(v1-c1)).normalized())) )
                return Distance(LineSegment3Double(u1,v1), plane);
            else
                kStatus.ShowMessage("Error: two planes are not parallel");
        }
        break;

    case SelectCurve:   // curve
        {
            IwCurve* crv = e1.selectValue.curve.curve;
            if (crv->IsLinear())
	        {
		        IwPoint3d st, ed;
		        crv->GetEnds(st, ed);
		        return Distance(LineSegment3Double(st,ed), plane);
	        }
            double hitU,hitV;
            IwBSplineSurface* bs = CreateSurface(pc, pu, pv, hitPt, hitU, hitV);
            MeasureResult r = Distance(bs, hitU, hitV, e1.selectValue.curve.curve, e1.selectValue.curve.hitU);
            delete bs;
            return r;
        }

    case SelectEdge:    // edge
        {
            IwEdge* edge = e1.selectValue.edge.edge;
            IwCurve* crv = edge->GetCurve();
            IwExtent1d intv = edge->GetInterval();
            if( crv->IsLinear() )
            {
                IwPoint3d ps,pe;
                crv->EvaluatePoint(intv.GetMin(), ps);
                crv->EvaluatePoint(intv.GetMax(), pe);
                return Distance(LineSegment3Double(ps,pe), plane);
            }
            double hitU,hitV;
            IwBSplineSurface* bs = CreateSurface(pc, pu, pv, hitPt, hitU, hitV);
            MeasureResult r = Distance(bs, hitU, hitV, e1.selectValue.edge.edge, e1.selectValue.edge.hitU);
            delete bs;
            return r;
        }

    case SelectSurface: // surface
        {
            IwSurface* surf = e1.selectValue.surface.surface;
            IwPoint3d facePnt, faceNormal;
	        if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
            {
                if( plane.IsParallel(Plane3Double(facePnt, faceNormal)) )
                    return Distance(LineSegment3Double(pc, (pu+pv)*.5), plane);
                else
                    kStatus.ShowMessage("Error: two planar surfaces are not parallel");
            }
	        else
	        {
                double hitU,hitV;
                IwBSplineSurface* bs = CreateSurface(pc, pu, pv, hitPt, hitU, hitV);
                MeasureResult r = Distance(bs, hitU, hitV, surf, e1.selectValue.surface.hitU, e1.selectValue.surface.hitV);
                delete bs;
                return r;
            }
        }
        break;

    case SelectFace:    // face
        {
            IwFace* face = e1.selectValue.face.face;
            IwPoint3d facePnt, faceNormal;
            IwSurface* surf = face->GetSurface();
	        if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
            {
                if( plane.IsParallel(Plane3Double(facePnt, faceNormal)) )
                    return Distance(LineSegment3Double(pc, (pu+pv)*.5), plane);
                else
                    kStatus.ShowMessage("Error: two planar surfaces are not parallel");
            }
	        else
	        {
                double hitU,hitV;
                IwBSplineSurface* bs = CreateSurface(pc, pu, pv, hitPt, hitU, hitV);
                MeasureResult r = Distance(bs, hitU, hitV, surf, e1.selectValue.face.hitU, e1.selectValue.face.hitV);
                delete bs;
                return r;
            }
        }
        break;

    default:
        kStatus.ShowMessage("Error: wrong entity type for distance measure");
        break;
    }
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityDistance(IwCurve* crv, const IwExtent1d& crvIntv, double crvHitU, const Point3Float& hitPt, MeasureEntity& e1)
{
    if (crv->IsLinear())
	{
		IwPoint3d st, ed;
		crv->GetEnds(st, ed);
        return ComputeEntityDistance(LineSegment3Double(st.x, st.y, st.z, ed.x, ed.y, ed.z), hitPt, e1);
	}

    switch(e1.selectType)
    {
    case SelectCurve:   // curve
        {
            IwCurve* crv1 = e1.selectValue.curve.curve;
            if (crv1->IsLinear())
	        {
		        IwPoint3d st, ed;
		        crv->GetEnds(st, ed);
		        return Distance(LineSegment3Double(st,ed), e1.hitPoint, crv, crvHitU);
	        }
            return Distance(crv, crvIntv, crvHitU,
                            crv1, crv1->GetNaturalInterval(), e1.selectValue.curve.hitU);
        }

    case SelectEdge:    // edge
        {
            IwEdge* edge1 = e1.selectValue.edge.edge;
            IwCurve* crv1 = edge1->GetCurve();
            IwExtent1d intv1 = edge1->GetInterval();
            if( crv1->IsLinear() )
            {
                IwPoint3d ps,pe;
                crv1->EvaluatePoint(intv1.GetMin(), ps);
                crv1->EvaluatePoint(intv1.GetMax(), pe);
                return Distance(LineSegment3Double(ps,pe), e1.hitPoint, crv, crvHitU);
            }
            return Distance(crv, crvIntv, crvHitU,
                            crv1, intv1,  e1.selectValue.curve.hitU);
        }

    case SelectSurface: // surface
        {
            MeasureResult r;
            IwSurface* surf = e1.selectValue.surface.surface;
            if( !LocalDistance(crv, crvIntv, crvHitU, surf, surf->GetNaturalUVDomain(), 
                e1.selectValue.surface.hitU, e1.selectValue.surface.hitV, r) )
            {
                GlobalDistance(crv, crvIntv, surf, r);
            }
            return r;
        }

    case SelectFace:    // face
        {
            MeasureResult r;
            IwFace* face = e1.selectValue.face.face;
            if( !LocalDistance(crv, crvIntv, crvHitU, face, 
                e1.selectValue.face.hitU, e1.selectValue.face.hitV, r) )
            {
                GlobalDistance(crv, crvIntv, face, r);
            }
            return r;
        }

    default:
        kStatus.ShowMessage("Error: wrong entity type for distance measure");
        break;
    }
    return MeasureResult();
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, const Point3Double& qt)
{
    MeasureResult r;
    r.mode = EntityDistance;
    r.value.distance.distance = pt.Distance(qt);
    r.value.distance.x1 = pt.x();
    r.value.distance.y1 = pt.y();
    r.value.distance.z1 = pt.z();
    r.value.distance.x2 = qt.x();
    r.value.distance.y2 = qt.y();
    r.value.distance.z2 = qt.z();
    r.value.distance.isMax = false;
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, const LineSegment3Double& ln)
{
    MeasureResult r;
    r.mode = EntityDistance;
    Point3Double qt = Line3Double(ln.GetP1(), ln.GetP2()).Project(pt);
    r.value.distance.distance = pt.Distance(qt);
    r.value.distance.x1 = pt.x();
    r.value.distance.y1 = pt.y();
    r.value.distance.z1 = pt.z();
    r.value.distance.x2 = qt.x();
    r.value.distance.y2 = qt.y();
    r.value.distance.z2 = qt.z();
    r.value.distance.isMax = false;
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, const Plane3Double& pl)
{
    MeasureResult r;
    r.mode = EntityDistance;
    Point3Double qt = pl.Project(pt);
    r.value.distance.distance = pt.Distance(qt);
    r.value.distance.x1 = pt.x();
    r.value.distance.y1 = pt.y();
    r.value.distance.z1 = pt.z();
    r.value.distance.x2 = qt.x();
    r.value.distance.y2 = qt.y();
    r.value.distance.z2 = qt.z();
    r.value.distance.isMax = false;
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, IwCurve* crv)
{
    if (crv->IsLinear())
	{
		IwPoint3d st, ed;
		crv->GetEnds(st, ed);
		return Distance(pt, LineSegment3Double(st,ed));
	}
	else
	    return Distance(pt, crv, crv->GetNaturalInterval());
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, IwEdge* edge)
{
    IwCurve* crv = edge->GetCurve();
    IwExtent1d intv = edge->GetInterval();
    if( crv->IsLinear() )
    {
        IwPoint3d ps,pe;
        crv->EvaluatePoint(intv.GetMin(), ps);
        crv->EvaluatePoint(intv.GetMax(), pe);
        return Distance(pt, LineSegment3Double(ps,pe));
    }
    else
        return Distance(pt, crv, intv);
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, IwCurve* crv, const IwExtent1d& intv)
{
    MeasureResult r;
    IwSolutionArray sols;
	crv->GlobalPointSolve(intv, IW_SO_MINIMIZE, pt, 0.0001, NULL, NULL, IW_SR_SINGLE, sols);
	if (sols.GetSize() > 0)
	{
		IwSolution sol = sols[0];
        IwPoint3d pnt;
		crv->EvaluatePoint(sol.m_vStart[0], pnt);

        r.mode = EntityDistance;
        r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
        r.value.distance.x1 = pt.x();
        r.value.distance.y1 = pt.y();
        r.value.distance.z1 = pt.z();
        r.value.distance.x2 = pnt.x;
        r.value.distance.y2 = pnt.y;
        r.value.distance.z2 = pnt.z;
        r.value.distance.isMax = false;
        r.valid = true;
	}
    else
        kStatus.ShowMessage("Error: distance measure failed");
    return r;
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, IwSurface* surf)
{
    IwPoint3d facePnt, faceNormal;
	if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
        return Distance(pt, Plane3Double(facePnt, faceNormal));
	else
	{
		MeasureResult r;
		IwSolutionArray sols;
		IwSolution sol;
		surf->GlobalPointSolve(surf->GetNaturalUVDomain(), IW_SO_MINIMIZE, pt, 0.00001, NULL, IW_SR_SINGLE, sols);
		if ( sols.GetSize() > 0 )
		{
			sol = sols.GetAt(0);
			r.mode = EntityDistance;
			r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
			r.value.distance.x1 = pt.x();
			r.value.distance.y1 = pt.y();
			r.value.distance.z1 = pt.z();

			IwPoint2d param = IwPoint2d(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
			IwPoint3d closestPoint;
			surf->EvaluatePoint(param, closestPoint);
			r.value.distance.x2 = closestPoint.x;
			r.value.distance.y2 = closestPoint.y;
			r.value.distance.z2 = closestPoint.z;
			r.value.distance.isMax = false;
			r.valid = true;
		}
        return r;
	}
}

MeasureResult MeasureResult::Distance(const Point3Double& pt, IwFace* face)
{
    IwPoint3d facePnt, faceNormal;
    IwSurface* surf = face->GetSurface();
	if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
        return Distance(pt, Plane3Double(facePnt, faceNormal));
	else
	{
		IwBrep* tempBrep = new (*iwContext) IwBrep();
		IwBrep* originalBrep = face->GetBrep();
		IwTArray<IwFace*> faces;
		faces.Add(face);
		originalBrep->CopyFaces(faces, tempBrep);
		MeasureResult r;
		IwSolutionArray sols;
		IwSolution sol;
		IwTopologySolver::BrepPointSolve(tempBrep, pt, IW_SO_MINIMIZE, IW_SR_SINGLE, 0.00001, IW_BIG_DOUBLE, NULL, sols);
		if (sols.GetSize() > 0)
		{
			sol = sols[0];
			IwObject* pObject = (IwObject*)sol.m_apObjects[0];
			if (pObject->IsKindOf(IwEdge_TYPE))
			{
				double edgeParam = sol.m_vStart.m_adParameters[0];
				IwEdge* myEdge = (IwEdge*)pObject;
				IwCurve* myCrv = myEdge->GetCurve();
				myCrv->EvaluatePoint(edgeParam, facePnt);
			}
			else if (pObject->IsKindOf(IwFace_TYPE))
			{
				IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
				surf->EvaluatePoint(faceUV, facePnt);
			}
			else if (pObject->IsKindOf(IwVertex_TYPE))
			{
				IwVertex* vertex = (IwVertex*)pObject;
				facePnt = vertex->GetPoint();
			}

			r.mode = EntityDistance;
			r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
			r.value.distance.x1 = pt.x();
			r.value.distance.y1 = pt.y();
			r.value.distance.z1 = pt.z();

			r.value.distance.x2 = facePnt.x;
			r.value.distance.y2 = facePnt.y;
			r.value.distance.z2 = facePnt.z;
			r.value.distance.isMax = false;
			r.valid = true;

		}
		IwObjDelete delBrep(tempBrep);
		return r;
	}
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln1, const LineSegment3Double& ln2)
{
    MeasureResult r;
    Point3Double p1,p2;
    if( ln1.IsParallel(ln2) )
    {
        p1 = (ln1.GetP1() + ln1.GetP2())*.5;
        p2 = Line3Double(ln2.GetP1(), ln2.GetP2()).Project(p1);
    }
    else
    {
        Point3Double n1p1 = ln1.GetP1();
        Point3Double n1p2 = ln1.GetP2();
        Point3Double n2p1 = ln2.GetP1();
        Point3Double n2p2 = ln2.GetP2();
        Plane3Double plane(n1p1, ((n1p2 - n1p1).cross(n2p2 - n2p1)).normalized());
        Point3Double pn2p1 = plane.Project(n2p1);
        if( Line3Double(pn2p1, plane.Project(n2p2)).Intersect(Line3Double(n1p1,n1p2), p1)!=IntersectFlag::Intersect )
            return r;
        p2 = p1 + n2p1 - pn2p1;
    }
    r.mode = EntityDistance;
    r.value.distance.distance = p1.Distance(p2);
    r.value.distance.x1 = p1.x();
    r.value.distance.y1 = p1.y();
    r.value.distance.z1 = p1.z();
    r.value.distance.x2 = p2.x();
    r.value.distance.y2 = p2.y();
    r.value.distance.z2 = p2.z();
    r.value.distance.isMax = false;
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln, const Plane3Double& pl)
{
    MeasureResult r;
    if( pl.IsParallel(Line3Double(ln.GetP1(), ln.GetP2())) )
    {
        Point3Double p1 = (ln.GetP1()+ln.GetP2())*.5;
        Point3Double p2 = pl.Project(p1);
        r.mode = EntityDistance;
        r.value.distance.distance = p1.Distance(p2);
        r.value.distance.x1 = p1.x();
        r.value.distance.y1 = p1.y();
        r.value.distance.z1 = p1.z();
        r.value.distance.x2 = p2.x();
        r.value.distance.y2 = p2.y();
        r.value.distance.z2 = p2.z();
        r.value.distance.isMax = false;
        r.valid = true;
    }
    else
        kStatus.ShowMessage("Warning: line and plane aren't parallel");
    return r;
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwCurve* crv, double hitU)
{
    if (crv->IsLinear())
	{
		IwPoint3d st, ed;
		crv->GetEnds(st, ed);
		return Distance(ln, LineSegment3Double(st,ed));
	}
	else
        return Distance(ln, hitPt, crv, crv->GetNaturalInterval(), hitU);
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwEdge* edge, double hitU)
{
    IwCurve* crv = edge->GetCurve();
    IwExtent1d intv = edge->GetInterval();
    if( crv->IsLinear() )
    {
        IwPoint3d ps,pe;
        crv->EvaluatePoint(intv.GetMin(), ps);
        crv->EvaluatePoint(intv.GetMax(), pe);
        return Distance(ln, LineSegment3Double(ps,pe));
    }
    else
        return Distance(ln, hitPt, crv, intv, hitU);
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwCurve* crv, const IwExtent1d& intv, double hitU)
{
    MeasureResult r;
    IwBSplineCurve* lncrv;
    if( IwBSplineCurve::CreateLineSegment(*iwContext, 3, ln.GetP1(), ln.GetP2(), lncrv)==IW_SUCCESS )
    {
        IwExtent1d lnintv = lncrv->GetNaturalInterval();
        double hitv = lnintv.Evaluate( ln.GetP1().Distance(hitPt.cast<double>()) / ln.GetP1().Distance(ln.GetP2()) );
        r = Distance(lncrv, lnintv, hitv, crv, intv, hitU);
        delete lncrv;
    }
    else
        kStatus.ShowMessage("Error: distance measure failed");
    return r;
}

MeasureResult MeasureResult::Distance(IwCurve* crv1, const IwExtent1d& intv1, double hitU1, IwCurve* crv2, const IwExtent1d& intv2, double hitU2)
{
    MeasureResult r;
    // local solution first
    IwBoolean foundAnswer;
    IwSolution sol;
	crv1->LocalCurveSolve(intv1, *crv2, intv2, IW_SO_MINIMIZE, 0.0001, NULL, NULL, hitU1, hitU2, foundAnswer, sol);
	bool solAtBoundary = fabs(intv1.Evaluate(0)-sol.m_vStart.m_adParameters[0]) < 0.0001 ||
					     fabs(intv1.Evaluate(1)-sol.m_vStart.m_adParameters[0]) < 0.0001 ||
					     fabs(intv2.Evaluate(0)-sol.m_vStart.m_adParameters[1]) < 0.0001 ||
					     fabs(intv2.Evaluate(1)-sol.m_vStart.m_adParameters[1]) < 0.0001 ;
	if (foundAnswer && !solAtBoundary )
	{
        IwPoint3d pnt, pnt2;
		crv1->EvaluatePoint(sol.m_vStart[0], pnt);
		crv2->EvaluatePoint(sol.m_vStart[1], pnt2);

        r.mode = EntityDistance;
        r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
        r.value.distance.x1 = pnt.x;
        r.value.distance.y1 = pnt.y;
        r.value.distance.z1 = pnt.z;
        r.value.distance.x2 = pnt2.x;
        r.value.distance.y2 = pnt2.y;
        r.value.distance.z2 = pnt2.z;
        r.value.distance.isMax = false;
        r.valid = true;

		// the local solver only return a stable solution. It is not necessary min or max.
		// We need to test whether it is a min/max distance.
		IwPoint3d pntTest;
		double tolDist = 0.0001;
		crv1->EvaluatePoint(sol.m_vStart[0]+0.001, pntTest);
		crv2->LocalPointSolve(intv2, IW_SO_MINIMIZE, pntTest, &tolDist, NULL, NULL, sol.m_vStart[1], foundAnswer, sol);
		if (foundAnswer)
			r.value.distance.isMax = ( r.value.distance.distance > sol.m_vStart.m_dSolutionValue );
	}
    // global if local has no solution
	else
	{
        IwSolutionArray sols;
		crv1->GlobalCurveSolve(intv1, *crv2, intv2, IW_SO_MINIMIZE, 0.0001, NULL, NULL, IW_SR_SINGLE, sols);
		if (sols.GetSize() > 0)
		{
			sol = sols.GetAt(0);
            IwPoint3d pnt, pnt2;
			crv1->EvaluatePoint(sol.m_vStart[0], pnt);
			crv2->EvaluatePoint(sol.m_vStart[1], pnt2);

            r.mode = EntityDistance;
            r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
            r.value.distance.x1 = pnt.x;
            r.value.distance.y1 = pnt.y;
            r.value.distance.z1 = pnt.z;
            r.value.distance.x2 = pnt2.x;
            r.value.distance.y2 = pnt2.y;
            r.value.distance.z2 = pnt2.z;
            r.value.distance.isMax = false;
            r.valid = true;
		}
        else
            kStatus.ShowMessage("Error: distance measure failed");
	}
    return r;
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwSurface* surf, double hitU, double hitV)
{
    IwPoint3d facePnt, faceNormal;
	if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
        return Distance(ln, Plane3Double(facePnt, faceNormal));
	else
	{
        MeasureResult r;
        double hitv;
        IwBSplineCurve* lncrv = CreateCurve(ln, hitPt, hitv);
        if( lncrv )
        {
            IwExtent1d lnintv = lncrv->GetNaturalInterval();
            if( !LocalDistance(lncrv, lnintv, hitv, surf, surf->GetNaturalUVDomain(), hitU, hitV, r) )
            {
                GlobalDistance(lncrv, lnintv, surf, r);
            }
            delete lncrv;
        }
        else
            kStatus.ShowMessage("Error: distance measure failed");
        return r;
	}
}

MeasureResult MeasureResult::Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwFace* face, double hitU, double hitV)
{
    IwPoint3d facePnt, faceNormal;
    IwSurface* surf = face->GetSurface();
	if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
        return Distance(ln, Plane3Double(facePnt, faceNormal));
	else
	{
        MeasureResult r;
        double hitv;
        IwBSplineCurve* lncrv = CreateCurve(ln, hitPt, hitv);
        if( lncrv )
        {
            IwExtent1d lnintv = lncrv->GetNaturalInterval();
            if( !LocalDistance(lncrv, lnintv, hitv, surf, surf->GetNaturalUVDomain(), hitU, hitV, r) )
            {
                GlobalDistance(lncrv, lnintv, face, r);
            }
            delete lncrv;
        }
        return r;
	}
}

bool MeasureResult::LocalDistance(IwCurve* crv, const IwExtent1d& intv, double hit, IwSurface* surf, const IwExtent2d& ints, double hitU, double hitV, MeasureResult& r)
{
    IwSolution sol;
    ///////// Local Solution //////////////////////////////////////////////////////
	IwBoolean haveAnswer = false, foundAnswer;
	surf->LocalCurveSolve(ints, *crv, intv,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, IwPoint2d(hitU,hitV), hit, foundAnswer, sol);
	if (foundAnswer)
	{
        IwPoint3d pnt, pnt2;
		crv->EvaluatePoint(sol.m_vStart[0], pnt);
		IwVector2d paramUV(sol.m_vStart[1], sol.m_vStart[2]);
		surf->EvaluatePoint(paramUV, pnt2);

        r.mode = EntityDistance;
        r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
        r.value.distance.x1 = pnt.x;
        r.value.distance.y1 = pnt.y;
        r.value.distance.z1 = pnt.z;
        r.value.distance.x2 = pnt2.x;
        r.value.distance.y2 = pnt2.y;
        r.value.distance.z2 = pnt2.z;
        r.value.distance.isMax = false;
        r.valid = true;
		haveAnswer = true;
        // The local solver returns a stable solution. It is not necessary min or max.
		// We need to test it.
		IwPoint3d pntTest;
		crv->EvaluatePoint(sol.m_vStart[0]+0.001, pntTest);
		surf->LocalPointSolve(ints, IW_SO_MINIMIZE, pntTest, IwPoint2d(hitU,hitV), foundAnswer, sol);
		if (foundAnswer)
			r.value.distance.isMax = ( r.value.distance.distance > sol.m_vStart.m_dSolutionValue );
	}
    return haveAnswer;
}

bool MeasureResult::LocalDistance(IwCurve* crv, const IwExtent1d& intv, double hit, IwFace* face, double hitU, double hitV, MeasureResult& r)
{
    IwSolution sol;
    ///////// Local Solution //////////////////////////////////////////////////////
	IwSurface* surf = face->GetSurface();
	IwExtent2d ints = face->GetUVDomain();
	IwBoolean haveAnswer=false, foundAnswer;
	surf->LocalCurveSolve(ints, *crv, intv,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, IwPoint2d(hitU,hitV), hit, foundAnswer, sol);
	if (foundAnswer)
	{
        IwPoint3d pnt1, pnt2;
		crv->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt1);
		IwVector2d paramUV(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
		surf->EvaluatePoint(paramUV, pnt2);

		bool crvSolAtBoundary = intv.IsValueOnBoundary(sol.m_vStart.m_adParameters[0],0.001); 
		IwPointClassification pClass;
		face->Point3DClassify(pnt2, 0.001, TRUE, pClass);
		bool surfSolInsideBoundary = (pClass.GetPointClass() == IW_PC_FACE);
			
		if ( !crvSolAtBoundary && surfSolInsideBoundary )
		{
			r.mode = EntityDistance;
			r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
			r.value.distance.x1 = pnt1.x;
			r.value.distance.y1 = pnt1.y;
			r.value.distance.z1 = pnt1.z;
			r.value.distance.x2 = pnt2.x;
			r.value.distance.y2 = pnt2.y;
			r.value.distance.z2 = pnt2.z;
			r.value.distance.isMax = false;
			r.valid = true;
			haveAnswer=true;
			// The local solver returns a stable solution. It is not necessary min or max.
			// We need to test it.
			IwPoint3d pntTest;
			crv->EvaluatePoint(sol.m_vStart[0]+0.001, pntTest);
			surf->LocalPointSolve(ints, IW_SO_MINIMIZE, pntTest, IwPoint2d(hitU,hitV), foundAnswer, sol);
			if (foundAnswer)
				r.value.distance.isMax = ( r.value.distance.distance > sol.m_vStart.m_dSolutionValue );
		}
	}
    return haveAnswer;
}

bool MeasureResult::GlobalDistance(IwCurve* crv, const IwExtent1d& intv, IwSurface* surf, MeasureResult& r)
{
    ////////Global Solution////////////////////////////////////////////////////////
	IwPoint3d pntCrv, pntSurf;
	double minDist = SMLibHelpers::DistFromCurveToSurface((IwBSplineCurve*)crv, intv, surf, NULL, &pntCrv, &pntSurf);

    r.mode = EntityDistance;
    r.value.distance.distance = minDist;
    r.value.distance.x1 = pntCrv.x;
    r.value.distance.y1 = pntCrv.y;
    r.value.distance.z1 = pntCrv.z;
    r.value.distance.x2 = pntSurf.x;
    r.value.distance.y2 = pntSurf.y;
    r.value.distance.z2 = pntSurf.z;
    r.value.distance.isMax = false;
    r.valid = true;
	
    return true;
}


bool MeasureResult::GlobalDistance(IwCurve* crv, const IwExtent1d& intv, IwFace* face, MeasureResult& r)
{
    ////////Global Solution////////////////////////////////////////////////////////
	IwPoint3d pntCrv, pntShape;
	double minDist = SMLibHelpers::DistFromShapeToCurve(NULL, face, NULL, crv, intv, pntShape, pntCrv);

    r.mode = EntityDistance;
    r.value.distance.distance = minDist;
    r.value.distance.x1 = pntShape.x;
    r.value.distance.y1 = pntShape.y;
    r.value.distance.z1 = pntShape.z;
    r.value.distance.x2 = pntCrv.x;
    r.value.distance.y2 = pntCrv.y;
    r.value.distance.z2 = pntCrv.z;
    r.value.distance.isMax = false;
    r.valid = true;
	
    return true;
}

MeasureResult MeasureResult::Distance(const Circle3Double& cr, const Plane3Double& pl)
{
    MeasureResult r;
    const Point3Double& ct = cr.GetCenter();
    if( Plane3Double(ct, cr.GetNormal()).IsParallel(pl) )
    {
        Point3Double pj = pl.Project(ct);
        r.mode = EntityDistance;
        r.value.distance.distance = pj.Distance(ct);
        r.value.distance.x1 = ct.x();
        r.value.distance.y1 = ct.y();
        r.value.distance.z1 = ct.z();
        r.value.distance.x2 = pj.x();
        r.value.distance.y2 = pj.y();
        r.value.distance.z2 = pj.z();
        r.value.distance.isMax = false;
        r.valid = true;
    }
    else
        kStatus.ShowMessage("Warning: circle and plane are not parallel");
    return r;
}

MeasureResult MeasureResult::Distance(const Circle3Double& cr, const Point3Float& hitPt, IwCurve* crv, double hitU)
{
    if (crv->IsLinear())
	{
		IwPoint3d st, ed;
		crv->GetEnds(st, ed);
		return Distance(LineSegment3Double(st,ed), Plane3Double(cr.GetCenter(),cr.GetNormal()));
	}
	else
    {
        double hitv;
        IwCircle *iwCr = CreateCircle(cr, hitPt, hitv);
        MeasureResult r = Distance(iwCr, iwCr->GetNaturalInterval(), hitv, crv, crv->GetNaturalInterval(), hitU);
        delete iwCr;
        return r;
    }
}

MeasureResult MeasureResult::Distance(const Circle3Double& cr, const Point3Float& hitPt, IwEdge* edge, double hitU)
{
    IwCurve* crv = edge->GetCurve();
    IwExtent1d intv = edge->GetInterval();
    if( crv->IsLinear() )
    {
        IwPoint3d ps,pe;
        crv->EvaluatePoint(intv.GetMin(), ps);
        crv->EvaluatePoint(intv.GetMax(), pe);
        return Distance(LineSegment3Double(ps,pe), Plane3Double(cr.GetCenter(),cr.GetNormal()));
    }
    else
    {
        double hitv;
        IwCircle *iwCr = CreateCircle(cr, hitPt, hitv);
        MeasureResult r = Distance(iwCr, iwCr->GetNaturalInterval(), hitv, crv, intv, hitU);
        delete iwCr;
        return r;
    }
}

MeasureResult MeasureResult::Distance(const Circle3Double& cr, const Point3Float& hitPt, IwSurface* surf, double hitU, double hitV)
{
    IwPoint3d facePnt, faceNormal;
	if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
        return Distance(cr, Plane3Double(facePnt, faceNormal));
	else
	{
        MeasureResult r;
        double hitv;
        IwCircle* iwCr = CreateCircle(cr, hitPt, hitv);
        if( iwCr )
        {
            IwExtent1d crintv = iwCr->GetNaturalInterval();
            if( !LocalDistance(iwCr, crintv, hitv, surf, surf->GetNaturalUVDomain(), hitU, hitV, r) )
            {
                GlobalDistance(iwCr, crintv, surf, r);
            }
            delete iwCr;
        }
        else
            kStatus.ShowMessage("Error: distance measure failed");
        return r;
	}
}

MeasureResult MeasureResult::Distance(const Circle3Double& cr, const Point3Float& hitPt, IwFace* face, double hitU, double hitV)
{
    IwPoint3d facePnt, faceNormal;
    IwSurface* surf = face->GetSurface();
	if (surf->IsPlanar(0.01, &facePnt, &faceNormal))// if the face is planar
        return Distance(cr, Plane3Double(facePnt, faceNormal));
	else
	{
        MeasureResult r;
        double hitv;
        IwCircle* iwCr = CreateCircle(cr, hitPt, hitv);
        if( iwCr )
        {
            IwExtent1d crintv = iwCr->GetNaturalInterval();
            if( !LocalDistance(iwCr, crintv, hitv, surf, surf->GetNaturalUVDomain(), hitU, hitV, r) )
            {
                GlobalDistance(iwCr, crintv, face, r);
            }
            delete iwCr;
        }
        else
            kStatus.ShowMessage("Error: distance measure failed");
        return r;
	}
}

MeasureResult MeasureResult::Distance(IwSurface* surf, double hitU, double hitV, IwCurve* crv, double hit)
{
    MeasureResult r;
    IwExtent1d intv = crv->GetNaturalInterval();
    if( !LocalDistance(crv, intv, hit, surf, surf->GetNaturalUVDomain(), hitU, hitV, r) )
    {
        GlobalDistance(crv, intv, surf, r);
    }
    return r;
}

MeasureResult MeasureResult::Distance(IwSurface* surf, double hitU, double hitV, IwEdge* edge, double hit)
{
    MeasureResult r;
    IwCurve* crv = edge->GetCurve();
    IwExtent1d intv = edge->GetInterval();
    if( !LocalDistance(crv, intv, hit, surf, surf->GetNaturalUVDomain(), hitU, hitV, r) )
    {
        GlobalDistance(crv, intv, surf, r);
    }
    return r;
}

MeasureResult MeasureResult::Distance(IwSurface* surf1, double hitU1, double hitV1, IwSurface* surf2, double hitU2, double hitV2)
{
    MeasureResult res;
    if( !LocalDistance(surf1, surf1->GetNaturalUVDomain(), IwPoint2d(hitU1,hitV1),
                       surf2, surf2->GetNaturalUVDomain(), IwPoint2d(hitU2,hitV2), res) )
    {
        GlobalDistance(surf1, surf2, res);
    }
    return res;
}

MeasureResult MeasureResult::Distance(IwFace* face1, double hitU1, double hitV1, IwFace* face2, double hitU2, double hitV2)
{
    MeasureResult res;
    IwSurface* surf1 = face1->GetSurface();
    IwSurface* surf2 = face2->GetSurface();

	// test planar face to planar face
	IwPoint3d pnt1, pnt2, normal1, normal2;
 	bool isPlanar1 = surf1->IsPlanar(0.001, &pnt1, &normal1);
 	bool isPlanar2 = surf2->IsPlanar(0.001, &pnt2, &normal2);
	bool isParallel = normal1.IsParallelTo(normal2, 0.001);
	if ( isPlanar1 && isPlanar2 && isParallel )
	{
		IwVector3d pnt11;
		surf1->EvaluatePoint(IwPoint2d(hitU1,hitV1), pnt11);
		normal1.Unitize();
		IwVector3d vec = pnt2 - pnt11;
		double dotValue = vec.Dot(normal1);
		IwVector3d pnt22 = pnt11 + dotValue*normal1;
		return Distance(Point3Double(pnt11), Point3Double(pnt22));	
	}

	// run local solver
	bool gotLocalAnswer = LocalDistance(face1, face1->GetUVDomain(), IwPoint2d(hitU1,hitV1),
								face2, face2->GetUVDomain(), IwPoint2d(hitU2,hitV2), res);
	if ( gotLocalAnswer )
		return res;

	// Run global solver
    GlobalDistance(face1, face2, res);
    return res;
}

bool MeasureResult::LocalDistance(IwSurface* thisSurface,  const IwExtent2d& thisUVDomain,  const IwPoint2d& hitThisUV, 
                                  IwSurface* otherSurface, const IwExtent2d& otherUVDomain, const IwPoint2d& hitOtherUV, 
                                  MeasureResult& r)
{
    /////////// Local Solution /////////////////////////////////////////////
    IwSolution sol;
	IwBoolean haveAnswer = false, foundAnswer;
	thisSurface->LocalSurfaceSolve(thisUVDomain, *otherSurface, otherUVDomain,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, hitThisUV, hitOtherUV, foundAnswer, sol);
	if( foundAnswer)
	{
        IwPoint3d pnt, pnt2;
		IwVector2d paramUV(sol.m_vStart[0], sol.m_vStart[1]);
		IwVector2d otherParamUV(sol.m_vStart[2], sol.m_vStart[3]);
		if ( !thisUVDomain.IsPoint2dOnBoundary(paramUV, 0.001) && !otherUVDomain.IsPoint2dOnBoundary(otherParamUV, 0.001) )
		{
			thisSurface->EvaluatePoint(paramUV, pnt);
			otherSurface->EvaluatePoint(otherParamUV, pnt2);

			r.mode = EntityDistance;
			r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
			r.value.distance.x1 = pnt.x;
			r.value.distance.y1 = pnt.y;
			r.value.distance.z1 = pnt.z;
			r.value.distance.x2 = pnt2.x;
			r.value.distance.y2 = pnt2.y;
			r.value.distance.z2 = pnt2.z;
			r.value.distance.isMax = false;
			r.valid = true;
			haveAnswer = true;

			// The local solver returns a stable solution. It is not necessary min or max.
			// We need to test it.
			IwPoint3d pntTest;
			thisSurface->EvaluatePoint(paramUV+IwVector2d(0.001, 0.001), pntTest);
			otherSurface->LocalPointSolve(otherUVDomain, IW_SO_MINIMIZE, pntTest, hitOtherUV, foundAnswer, sol);
			if (foundAnswer)
				r.value.distance.isMax = ( r.value.distance.distance > sol.m_vStart.m_dSolutionValue );
		}
	}
    return haveAnswer;
}

bool MeasureResult::LocalDistance(IwFace* face1,  const IwExtent2d& thisUVDomain,  const IwPoint2d& hitThisUV, 
                                  IwFace* face2, const IwExtent2d& otherUVDomain, const IwPoint2d& hitOtherUV, 
                                  MeasureResult& r)
{
    /////////// Local Solution /////////////////////////////////////////////
    IwSolution sol;
	IwBoolean haveAnswer = false, foundAnswer;
	IwSurface* thisSurface = face1->GetSurface();
	IwSurface* otherSurface = face2->GetSurface();
	thisSurface->LocalSurfaceSolve(thisUVDomain, *otherSurface, otherUVDomain,  IW_SO_MINIMIZE, 0.0001, NULL, NULL, hitThisUV, hitOtherUV, foundAnswer, sol);
	if( foundAnswer)
	{
        IwPoint3d pnt1, pnt2;
		IwVector2d paramUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
		thisSurface->EvaluatePoint(paramUV, pnt1);
		IwVector2d otherParamUV(sol.m_vStart.m_adParameters[2], sol.m_vStart.m_adParameters[3]);
		otherSurface->EvaluatePoint(otherParamUV, pnt2);
		IwPointClassification pClass1, pClass2;
		face1->Point3DClassify(pnt1, 0.001, TRUE, pClass1);
		bool surfSolInsideBoundary1 = (pClass1.GetPointClass() == IW_PC_FACE);
		face2->Point3DClassify(pnt2, 0.001, TRUE, pClass2);
		bool surfSolInsideBoundary2 = (pClass2.GetPointClass() == IW_PC_FACE);

		if ( surfSolInsideBoundary1 && surfSolInsideBoundary2 )
		{
			r.mode = EntityDistance;
			r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
			r.value.distance.x1 = pnt1.x;
			r.value.distance.y1 = pnt1.y;
			r.value.distance.z1 = pnt1.z;
			r.value.distance.x2 = pnt2.x;
			r.value.distance.y2 = pnt2.y;
			r.value.distance.z2 = pnt2.z;
			r.value.distance.isMax = false;
			r.valid = true;
			haveAnswer = true;

			// The local solver returns a stable solution. It is not necessary min or max.
			// We need to test it.
			IwPoint3d pntTest;
			thisSurface->EvaluatePoint(paramUV+IwVector2d(0.001, 0.001), pntTest);
			otherSurface->LocalPointSolve(otherUVDomain, IW_SO_MINIMIZE, pntTest, hitOtherUV, foundAnswer, sol);
			if (foundAnswer)
				r.value.distance.isMax = ( r.value.distance.distance > sol.m_vStart.m_dSolutionValue );
		}
		// Now check whether intersect if the distance is small
		double refDistance = 0.5;
		if ( r.value.distance.distance < refDistance )
		{
			IwVector3d refVector = pnt2 - pnt1;
			refVector.Unitize();
			IwBoolean foundAnswer2;
			IwSolution sol2;
			thisSurface->LocalSurfaceIntersect(thisUVDomain, *otherSurface, otherUVDomain, 0.0001, hitThisUV, hitOtherUV, NULL, NULL, FALSE, foundAnswer2, sol2);
			if( foundAnswer2 )
			{
				IwPoint3d pnt11;
				IwVector2d paramUV2(sol2.m_vStart.m_adParameters[0], sol2.m_vStart.m_adParameters[1]);
				thisSurface->EvaluatePoint(paramUV2, pnt11);
				double dist = pnt11.DistanceBetween(pnt1);
				if ( dist < 20*refDistance ) // if intersect locally, this distance becomes negative (interference)
				{
					r.value.distance.distance =-r.value.distance.distance;
					r.value.distance.isMax = false;
				}
			}
		}
	}
    return haveAnswer;
}

bool MeasureResult::GlobalDistance(IwFace* face1, IwFace* face2, MeasureResult& r)
{
	/////////// Global Solution /////////////////////////////////////////////
	IwPoint3d pnt1, pnt2;
	double distance = SMLibHelpers::DistFromFaceToFace(face1, face2, pnt1, pnt2);
	if ( distance != -1 )
	{
        r.value.distance.x1 = pnt1.x;
        r.value.distance.y1 = pnt1.y;
        r.value.distance.z1 = pnt1.z;
		r.value.distance.x2 = pnt2.x;
        r.value.distance.y2 = pnt2.y;
        r.value.distance.z2 = pnt2.z;
        r.mode = EntityDistance;
        r.value.distance.distance = distance;
        r.value.distance.isMax = false;
        r.valid = true;
		return true;
	}
    else
    {
        kStatus.ShowMessage("Error: face face distance measure");
        return false;
    }
}

bool MeasureResult::GlobalDistance(IwSurface* surface1, IwSurface* surface2, MeasureResult& r)
{
	/////////// Global Solution /////////////////////////////////////////////
	IwSolutionArray sols;
	surface1->GlobalSurfaceSolve(surface1->GetNaturalUVDomain(), *surface2, surface2->GetNaturalUVDomain(), IW_SO_MINIMIZE, 0.001, NULL, NULL, IW_SR_SINGLE, sols);
	if ( sols.GetSize() > 0 )
	{
		IwSolution sol = sols.GetAt(0);
		IwPoint3d pnt1, pnt2;
		IwVector2d paramUV1(sol.m_vStart[0], sol.m_vStart[1]);
		IwVector2d paramUV2(sol.m_vStart[2], sol.m_vStart[3]);
		surface1->EvaluatePoint(paramUV1, pnt1);
		surface2->EvaluatePoint(paramUV2, pnt2);

        r.value.distance.x1 = pnt1.x;
        r.value.distance.y1 = pnt1.y;
        r.value.distance.z1 = pnt1.z;
		r.value.distance.x2 = pnt2.x;
        r.value.distance.y2 = pnt2.y;
        r.value.distance.z2 = pnt2.z;
        r.mode = EntityDistance;
		r.value.distance.distance = sol.m_vStart.m_dSolutionValue;
        r.value.distance.isMax = false;
        r.valid = true;
		return true;
	}
    else
    {
        kStatus.ShowMessage("Error: surface surface distance measure");
        return false;
    }
}

IwBSplineCurve* MeasureResult::CreateCurve(const LineSegment3Double& ln, const Point3Float& hitPt, double& hitV)
{
    IwBSplineCurve* lncrv;
    if( IwBSplineCurve::CreateLineSegment(*iwContext, 3, ln.GetP1(), ln.GetP2(), lncrv)!=IW_SUCCESS )
        return nullptr;
    
    IwExtent1d lnintv = lncrv->GetNaturalInterval();
    hitV = lnintv.Evaluate( ln.GetP1().Distance(hitPt.cast<double>()) / ln.GetP1().Distance(ln.GetP2()) );
    return lncrv;
}

IwCircle* MeasureResult::CreateCircle(const Circle3Double& cr, const Point3Float& hitPt, double& hitV)
{
    const Point3Double& ct = cr.GetCenter();
    const Point3Double& nor = cr.GetNormal();
    Vector3Double ax,ay;
    cr.GetXYAxis(ax,ay);
    IwCircle *iwCr = new (*iwContext) IwCircle(ct, ax, ay, IwExtent1d(-180,180), cr.GetRadius(), 3);
    Point3Double pv = hitPt.cast<double>() - ct;
    hitV = atan2(pv.dot(ay), pv.dot(ax))*180/PI;
    return iwCr;
}

IwBSplineSurface* MeasureResult::CreateSurface(const Point3Double& pc,const Point3Double& p1,const Point3Double& p2, const Point3Float& hitPt, double& hitU, double& hitV)
{
    Vector3Double v1 = p1 - pc;
    Vector3Double v2 = p2 - pc;
    Point3Double p3 = p2 + v1;
    IwBSplineSurface* bs;
    IwBSplineSurface::CreateBilinearSurface(*iwContext, pc, p1, p2, p3, bs);

    v1.normalize();
    v2.normalize();
    Vector3Double v = hitPt.cast<double>() - pc;
    double v1v2 = v1.dot(v2);
    double vv1 = v.dot(v1);
    double vv2 = v.dot(v2);
    double iv1v2 = 1.0/(v1v2*v1v2-1);
    hitU = (v1v2*vv2-vv1)*iv1v2;
    hitV = (v1v2*vv1-vv2)*iv1v2;
    return bs;
}
