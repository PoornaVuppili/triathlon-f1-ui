#include "KTreeView.h"


KTreeView::KTreeView(QWidget *parent):QTreeView(parent)
{
}


KTreeView::~KTreeView(void)
{
}

void KTreeView::selectionChanged( const QItemSelection & selected, const QItemSelection & deselected )
{
	emit selectionChanged(selected.indexes().first());
}