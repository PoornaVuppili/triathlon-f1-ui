#pragma once
#ifndef CONFIGURATION_H_INCLUDED
#define CONFIGURATION_H_INCLUDED

#include <qstring>
#include <map>

// this implements a common for all "Implants" mechanism for loading
// a configuration (name-value pairs) from a file of a given name.
// The name-value pairs

class Configuration
{
public:
   typedef std::map<QString,QString> map_type;

   Configuration(QString const &filename, QString const &seprtr, QString const &comment);
   bool Load(QString const &versionToCheck, bool clearFirst = true);

   // we can't overload by return value type, so these have different names
   int GetInteger(QString const &varName, int defVal = 0) const;
   bool GetBoolean(QString const &varName, bool defVal = false) const;
   double GetDouble(QString const &varName, double defVal = 0) const;
   QString GetString(QString const &varName, QString const &defVal = "") const;

   // these are simply overloaded -- return 'true' if 'val' has new value, 'false' - no such named value
   bool GetValue(QString const &varName, int &val) const;
   bool GetValue(QString const &varName, bool &val) const;
   bool GetValue(QString const &varName, double &val) const;
   bool GetValue(QString const &varName, QString &val) const;

private:

   QString   m_filename; // the location of the configuration on the exernal storage
   QString   m_separator; // the string to be used to split the line into the name and the value
   QString   m_comment; // the string such that if a line starts with it, ignore that line
   map_type  m_values;
};

#endif // def CONFIGURATION_H_INCLUDED
