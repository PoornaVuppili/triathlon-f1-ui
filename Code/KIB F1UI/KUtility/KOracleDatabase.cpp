#include "KOracleDatabase.h"
#include <qsqldatabase.h>
#include <qsqlquery.h>
#include <qvariant.h>

#include <qsqlerror.h>
#include <qdebug.h>
                         
KOracleDatabase::KOracleDatabase(void)
{
    QSqlDatabase::addDatabase("QOCI", "oracle");
}

KOracleDatabase::~KOracleDatabase(void)
{
    QSqlDatabase::removeDatabase("oracle");
}

// Set connection parameters
//  name : database name
//  host : ........ host name
//  port : ........ port number
//  user : ........ user name
//  password : .... password
bool KOracleDatabase::SetParameters(QString& name, QString& host, int port, QString& user, QString& password)
{
    QSqlDatabase db = QSqlDatabase::database("oracle", false);
    if( !db.isValid() ) // if a valid driver exists
        return false;

    // close the database if it is still open
    if( db.isOpen() )
        db.close();

    db.setDatabaseName( name );
    db.setHostName( host );
    db.setPort( port );
    db.setUserName( user );
    db.setPassword( password );

    return true;
}

// Test connection
// Return: true - successful, false - failed
bool KOracleDatabase::TestConnection()
{
    QSqlDatabase db = QSqlDatabase::database("oracle", false);
    if( !db.isValid() ) // if a valid driver exists
        return false;

    if( db.open() )
    {
        db.close();
        return true;
    }
    return false;
}

// Query based on the implant number. The function returns an implant description string, country code.
// From the description string, the implant type, left/right knee, medial/lateral information can be extracted.
//   
// Parameters:
//  implantNum          (I) : serial number
//  desc                (O) : implant description 
//  country             (O) : country code
//  lastName,firstName  (O) : name (optional)
// Return:
//  true - successful; false: failed.
bool KOracleDatabase::Query(const QString& implantNum, QString& desc, QString& country, QString* lastName, QString* firstName)
{
    QSqlDatabase db = QSqlDatabase::database("oracle", false);
    if( !db.isValid() ) // if a valid driver exists
        return false;

    if( !db.open() )
		return false;

	bool hasDesc = false;
    bool hasCnty = false;

	// query
    QSqlQuery query(QString("SELECT DISTINCT ") + 
		" psn.project_number PROJECT_NUMBER, " + 
		" msi.segment1 ITEM, " +
		" wj.bom_revision REVISION, " +
		" msi.description DESCRIPTION, " +
		" hl.country, " +
		" ooha.attribute1 PATIENT_LAST_NAME, " +
		" ooha.attribute2 PATIENT_FIRST_NAME " +
		" FROM " +
		" apps.oe_order_lines_all oola, " +
		" apps.oe_order_headers_all ooha, " +
		" apps.pjm_seiban_numbers psn, " +
		" apps.wip_discrete_jobs_v wj, " +
		" apps.mtl_system_items msi, " +
		" apps.hz_locations hl, " +
		" apps.hz_cust_site_uses_all hcsua, " +
		" apps.hz_cust_acct_sites_all hcasa, " +
		" apps.hz_party_sites hps " +
		" WHERE " +
		" oola.header_id = ooha.header_id " +
		" AND wj.project_id = psn.project_id " +
		" AND wj.project_id = oola.project_id " +
		" AND msi.inventory_item_id = wj.primary_item_id " +
		" AND msi.organization_id = wj.organization_id " +
		" AND oola.ship_to_org_id = hcsua.site_use_id " +
		" AND hcsua.cust_acct_site_id = hcasa.cust_acct_site_id " +
		" AND hcasa.party_site_id = hps.party_site_id " +
		" AND hps.location_id = hl.location_id " +
		" and psn.project_number = '" + implantNum + "'", db);

    int n = 0;
    while( query.next() )
    {
		QString s1 = query.value(1).toString();
		QString s3 = query.value(3).toString();

		if( s1.startsWith("M", Qt::CaseInsensitive) ||
			(s1.startsWith("ED", Qt::CaseInsensitive) && s3.startsWith("KIT", Qt::CaseInsensitive) &&
			!s3.contains("JIG", Qt::CaseInsensitive) && !s3.contains("TRAY", Qt::CaseInsensitive)) )
		{
			if( !hasDesc )
			{
				desc = s3;
				hasDesc = !desc.isEmpty();
			}
			if( !hasCnty )
			{
				country = query.value(4).toString();
				hasCnty = !country.isEmpty();
			}
			if( lastName )
				*lastName = query.value(5).toString();
			if( firstName )
				*firstName = query.value(6).toString();
		}
    }

    db.close();
    return hasDesc || hasCnty;
}
