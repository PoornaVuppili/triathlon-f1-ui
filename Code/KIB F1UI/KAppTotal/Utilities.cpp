#include "DataDoc.h"
#include "Utilities.h"
#pragma warning( disable : 4996 4805 )
#include <iwtopologyTraverser.h>
//#include <IwFilletSolver.h>
//#include <IwFilletStandardSolver.h>
//#include <IwFilletExecutive.h>
#include <IwTrimmingTools.h>
#include <IwInterface.h>
#include <IwCutter.h>
#include <IwBrepCutting.h>
#include <HwTSLibIges.h>
#include "IgesTracker.h"
#pragma warning( default : 4996 4805 )
#include "..\KUtility\SMLibInterface.h"

#include <IwTolerance.h>

#include <HwTSLibIges.h>

IwBrep* NewBrep( double tol )
	{
	IwBrep*					pBrep = new ( *iwContext ) IwBrep;

	pBrep->SetTolerance( tol );

	return pBrep;
	}


IwBrep* CopyBrep( IwBrep const* pBrep )
	{
		if(!pBrep) return nullptr;
		return new ( *iwContext ) IwBrep( *pBrep );
	}


IwVertex const* FindVertex(IwBrep const* pBrep, IwPoint3d const& P, double tol)
{
	IwTArray<IwVertex*> vertices;
	pBrep->GetVertices(vertices);
	for(int i = 0; i < vertices.GetSize(); ++i)
	{
		if(IwVertex const* v = vertices[i])
		{
			if( (v->GetPoint() - P).LengthSquared() < tol*tol )
				return v;
		}
	}
	return nullptr;
}

IwCurve* CopyCurve( IwCurve const* pCurve, IwAxis2Placement const* xform )
	{
		IwCurve* crvCpy = nullptr;
		if(pCurve)
		{
			pCurve->Copy(*iwContext, crvCpy);
			if(xform)
				crvCpy->Transform(*xform);
		}
		return crvCpy ;
	}

IwBSplineCurve* CopyCurve( IwBSplineCurve* pCurve, bool bInvertCurve )
	{
	IwBSplineCurve*			pCurveCopy = new ( *iwContext ) IwBSplineCurve( *pCurve );

	if( bInvertCurve )
		InvertCurve( pCurveCopy );

	return pCurveCopy;
	}

IwEdge const* FindAdjacentEdgeAtDirection(IwEdge const* e, IwVector3d const& dir, double deg_tol)
{
	IwVertex *v = nullptr, *v0 = nullptr, *v1 = nullptr;
	e->GetVertices(v0, v1);
	v = (v1->GetPoint() - v0->GetPoint()).Dot(dir) > 0 ? v1 : v0;
	IwTArray<IwEdge*> edges;
	v->GetEdges(edges);
	IwPoint3d p = v->GetPoint();
	for(int i = 0; i < edges.GetSize(); ++i)
	{
		if(IwEdge* const ae = edges.GetAt(i))
		{
			if(ae != e)
			{
				double rad = 0;
				(ae->GetOtherVertex(v)->GetPoint() - p).AngleBetween(dir, rad);
				if(rad * 180 / M_PI < deg_tol)
					return ae;
			}
		}
	}
	return nullptr;
}

IwTArray<IwCurve *> CopyCurves(IwTArray<IwCurve *>const& crvs, IwAxis2Placement const* xform)
{
	IwTArray<IwCurve *> cpys;

	for(int i = 0; i < crvs.GetSize(); ++i)
	{
		if(IwCurve* crv = crvs.GetAt(i))
		{
			IwCurve* crvCpy = nullptr;
			crv->Copy(*iwContext, crvCpy);
			if(crvCpy) 
			{
				if(xform)
					crvCpy->Transform(*xform);
				cpys.Add(crvCpy);
			}
		}
	}
	assert(cpys.GetSize() == crvs.GetSize());
	return cpys;
}

bool IsPointOnTorus(
	IwPoint3d const& c,
	IwVector3d const& a,
	double const _R,
	double const r,

	IwPoint3d const& p,
	double tol
	)
{	
	IwVector3d cp = p - c;
	double z = (p-c).Dot(a);
	IwVector3d co = cp - z*a;
	co.Unitize();
	double R = _R;
	for(int i = 0; i < 2; ++i)
	{
		IwPoint3d o = c + R * co;
		double actual_r = (p-o).Length();
		if(fabs(actual_r - r) < tol)
			return true;
		if(R < r) // spindle torus: p and o could be on opposite sides of c.
			R = -R;
		else
			break;
	}
	return false;

	/*
	torus implicit equation at z = 0 plane,
	(x^2 + y^2 + z^2 + R^2 - r^2)^2 - 4 * R^2 * (x^2 + y^2) = 0
	double d2 = (p-c).LengthSquared();
	double z = (p-c).Dot(a);
	double x2_plus_y2 = d2 - z*z;
	double diff = fabs((d2 + R*R - r*r)*(d2 + R*R - r*r) - 4 * R*R * x2_plus_y2);
	return diff < tol;
	*/
}

IwEdge* GetIncidentEdge(IwVertex const* V0, IwVertex const* V1)
{
	if(V0 && V1)
	{
		IwTArray<IwEdge*> edges;
		V0->GetEdges(edges);
		for(int i = 0; i < edges.GetSize(); ++i)
		{
			if(IwEdge* e = edges.GetAt(i))
			{
				IwVertex *v0 = nullptr, *v1 = nullptr;
				e->GetVertices(v0, v1);
				if(V1 == v0 || V1 == v1)
					return e;
			}
		}
	}
	return nullptr;
}

IwPoint3d VertexCenter(IwTArray<IwVertex*> const& vs)
{
	IwPoint3d P(0,0,0);
	int cnt = 0;
	for(int i = 0; i < vs.GetSize(); ++i)
	{
		if(IwVertex const* v = vs.GetAt(i))
		{
			P += v->GetPoint();
			cnt++;
		}
	}
	return P / cnt;
}

IwPoint3d EdgeVertexCenter(IwEdge const* e)
{
	IwVertex *v0 = nullptr, *v1 = nullptr;
	if(e) e->GetVertices(v0, v1);
	IwTArray<IwVertex*> vs;
	if(v0) vs.Add(v0);
	if(v1) vs.Add(v1);
	return VertexCenter(vs);
}

IwPoint3d FaceVertexCenter(IwFace const* f)
{
	IwTArray<IwVertex*> vs;
	if(f) f->GetVertices(vs);
	return VertexCenter(vs);
}


IwBSplineSurface* CopySurface( IwBSplineSurface* pSurf )
	{
	return new ( *iwContext ) IwBSplineSurface( *pSurf );
	}


bool PierceBrep( IwBrep* pBrep, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, IwFace** pFace )
	{
	IwSolution			Sols[10];
	IwSolutionArray		Solutions( 10, Sols );
	double				dRayTolerance = 0.00001;
	double				dRayStepoffValue = 0.00001;
	int					nSols;
	IwVector3d			V = vec;
	IwFace*				pPiercedFace;

	pBrep->RayIntersection( pt, &V, dRayTolerance, dRayStepoffValue, Solutions ); 

	nSols = Solutions.GetSize();

	if( nSols == 0 )
		return false;

	IwSolution&		rSol = Solutions[0];

	IwObject*		pObject = (IwObject*) rSol.m_apObjects[0]; 

	pPiercedFace = IW_CAST_PTR( IwFace, pObject );

	if( pPiercedFace == NULL )
		return false;

	IwPoint2d		uv( rSol.m_vStart[0], rSol.m_vStart[1] );

	IwSurface*		pSur = pPiercedFace->GetSurface();
	
	pSur->EvaluatePoint( uv, pnt );

	if( pFace )
		*pFace = pPiercedFace;

	return true;
	}


bool PierceSurface( IwSurface* pSurface, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, double* uv )
	{
	IwExtent2d				domain = pSurface->GetNaturalUVDomain();
	IwSolutionArray			sols;
	double					t;

	pSurface->GlobalLineIntersect( domain, pt, vec, NULL, true, 0.001, sols );

	if( sols.GetSize() == 0 )
		return false;

	t = sols[0].m_vStart.m_adParameters[0];

	pnt = pt + t * vec;

	if( uv )
		{
		uv[0] = sols[0].m_vStart.m_adParameters[1];
		uv[1] = sols[0].m_vStart.m_adParameters[2];
		}

	return true;
	}

//////////////////////////////////////////////////////////////////////////////
// If mutiple intersections occur, this function returns
// the point that is closest to the input pt.
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The pBrep has better to be ShrinkGeometry()
/////////////////////////////////////////////////////////////////////////////
bool IntersectBrepByLine
( 
		IwBrep*						pBrep,	// I:
		const IwPoint3d&			pt,		// I:
		const IwVector3d&			vec,	// I:
		IwPoint3d&					pnt,	// O:
		IwFace** face						// O:
)
{
	IwLine* line = NULL;
	IwLine::CreateCanonical(*iwContext, pt, vec, line);

	IwPoint3d refPt = pt;
	bool gotIT = IntersectBrepByCurve(pBrep, line, pnt, &refPt, face);

	if ( line )
		IwObjDelete(line);

	return gotIT;
}

bool IntersectBrepByCurve
( 
		IwBrep*						pBrep,	// I:
		IwCurve*					curve,	// I:
		IwPoint3d&					pnt,	// O:
		IwPoint3d*					refPt,  // I: optional, the returned pnt will be the closest one if there are multiple intersection points. 
		IwFace**					face	// O:
)
{
	IwSolutionArray sols;
	IwTopologySolver::BrepCurveSolve(pBrep, *curve, curve->GetNaturalInterval(), IW_SO_INTERSECT, IW_SR_ALL, 0.001, IW_BIG_DOUBLE, NULL, sols);

	if ( sols.GetSize() == 0 ) return false;

	IwPoint3d tempPt;
	IwVector3d tempVec;
	double t, minDist = 1000000;
	IwFace* minFace = NULL;
	for (unsigned i=0; i<sols.GetSize(); i++)
	{
		t = sols[i].m_vStart.m_adParameters[0];
		curve->EvaluatePoint(t, tempPt);
		if ( refPt )
		{
			tempVec = *refPt - tempPt;
			if ( tempVec.Length() < minDist )
			{
				minDist = tempVec.Length();
				pnt = tempPt;
				if ( sols[i].m_apObjects[1]->IsKindOf(IwFace_TYPE) )
					minFace = (IwFace*)sols[i].m_apObjects[1];// face
			}
		}
		else
		{
			pnt = tempPt;
			if ( sols[i].m_apObjects[1]->IsKindOf(IwFace_TYPE) )
				minFace = (IwFace*)sols[i].m_apObjects[1];// face
		}
	}	

	if ( face )
		*face = minFace;

	return true;
}

//////////////////////////////////////////////////////////////////////////////
// If mutiple intersections occur, this function returns
// the point that is closest to the input pt.
/////////////////////////////////////////////////////////////////////////////
bool IntersectFaceByLine( 
		IwFace*						pFace, 
		const IwPoint3d&			pt, 
		const IwVector3d&			vec, 
		IwPoint3d&					pnt)
{

	IwSurface* surface = pFace->GetSurface();

	IwSolutionArray sols;
	surface->GlobalLineIntersect(pFace->GetUVDomain(), pt, vec, NULL, FALSE, 0.001, sols);

	if ( sols.GetSize() == 0 ) return false;

	IwPoint3d tempPt;
	IwVector3d tempVec;
	double t, minDist = 1000000;
	IwPoint2d uv;
	for (unsigned i=0; i<sols.GetSize(); i++)
	{
		t = sols[i].m_vStart.m_adParameters[0];

		tempPt = pt + t * vec;

		tempVec = pt - tempPt;

		if ( tempVec.Length() < minDist )
		{
			minDist = tempVec.Length();
			pnt = tempPt;
			uv = IwPoint2d(sols[i].m_vStart.m_adParameters[1], sols[i].m_vStart.m_adParameters[2]);
		}
	}

	// At least intersect with the surface.
	// Now let us check thether the intersection is inside the face domain.
	if ( minDist < 1000000 )
	{
		IwPointClassification pClassify;
		pFace->PointClassify(uv, TRUE, TRUE, pClassify);
		if ( pClassify.GetPointClass() == IW_PC_FACE ||
			 pClassify.GetPointClass() == IW_PC_EDGE ||
			 pClassify.GetPointClass() == IW_PC_VERTEX )
			 return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////
// If multiple intersections occur, this function returns
// the point that is closest to the input pt.
/////////////////////////////////////////////////////////////////////////////
bool IntersectSurfaceByLine( IwSurface* pSurface, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, double* uv )
{
	IwExtent2d				domain = pSurface->GetNaturalUVDomain();
	IwSolutionArray			sols;
	double					t;
	IwPoint3d				tempPt;
	IwVector3d				tempVec;

	pSurface->GlobalLineIntersect( domain, pt, vec, NULL, false, 0.0001, sols );

	if( sols.GetSize() == 0 )
		return false;

	double minDist = 1000000;
	for (unsigned i=0; i<sols.GetSize(); i++)
	{
		t = sols[i].m_vStart.m_adParameters[0];

		tempPt = pt + t * vec;

		tempVec = pt - tempPt;

		if ( tempVec.Length() < minDist )
		{
			minDist = tempVec.Length();
			pnt = tempPt;
			if( uv )
			{
				uv[0] = sols[0].m_vStart.m_adParameters[1];
				uv[1] = sols[0].m_vStart.m_adParameters[2];
			}
		}
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////
bool IntersectSurfaceByFireRay( IwSurface* pSurface, const IwPoint3d& pt, const IwVector3d& vec, IwPoint3d& pnt, double* uv )
{
	IwExtent2d				domain = pSurface->GetNaturalUVDomain();
	IwSolutionArray			sols;
	double					t;
	IwPoint3d				tempPt;
	IwVector3d				tempVec;

	pSurface->GlobalLineIntersect( domain, pt, vec, NULL, true, 0.001, sols );

	if( sols.GetSize() == 0 )
		return false;

	double minDist = 1000000;
	for (unsigned i=0; i<sols.GetSize(); i++)
	{
		t = sols[i].m_vStart.m_adParameters[0];

		tempPt = pt + t * vec;

		tempVec = pt - tempPt;

		if ( tempVec.Length() < minDist )
		{
			minDist = tempVec.Length();
			pnt = tempPt;
			if( uv )
			{
				uv[0] = sols[0].m_vStart.m_adParameters[1];
				uv[1] = sols[0].m_vStart.m_adParameters[2];
			}
		}
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////
// This function determines local intersection point.
/////////////////////////////////////////////////////////////////////////////
bool IntersectSurfaceByLine
( 
	IwSurface* pSurface,	// I:
	IwPoint3d& pt,			// I:
	IwVector3d& vec,		// I:
	IwPoint2d& guessUV,		// I:
	IwPoint3d& pnt,			// O:
	IwVector3d&	normal,		// O:
	IwPoint2d& intUV		// O:
)
{
	IwExtent2d				domain = pSurface->GetNaturalUVDomain();
	IwExtent1d				interval = IwExtent1d(-1000, 1000);
	IwBoolean				foundInt = FALSE;
	double					foundT, dev;

	pSurface->LocalLineIntersect( domain, pt, vec, interval, 0.1, 0.05, guessUV, foundInt, intUV, foundT, dev, pnt, normal);

	if( !foundInt )
		return false;

	return true;
}

/////////////////////////////////////////////////////////////////////////
// If there exist multiple intersection points, the closest one 
// will be returned.
bool IntersectSurfacesByLine
(
	IwTArray<IwSurface*>		surfaces,	// I:
	IwPoint3d&					pt,			// I:
	IwVector3d&					vec,		// I:
	IwPoint3d&					pnt,		// O:
	int*						surfIndex,	// O:
	double*						uv			// O:
)
{
	double minDist = HUGE_DOUBLE;
	double dist;
	bool bInt, bRet = false;
	IwPoint3d cPnt;
	IwPoint3d intPnt;
	double param[2];

	for (unsigned i=0; i<surfaces.GetSize(); i++)
	{
		bInt = IntersectSurfaceByLine(surfaces.GetAt(i), pt, vec, cPnt, param);
		if ( bInt )
		{
			bRet = true;
			intPnt = cPnt;
			dist = intPnt.DistanceBetween(pt);
			if ( dist < minDist )
			{
				minDist = dist;
				pnt = intPnt;
				if ( surfIndex )
					*surfIndex = i;
				if ( uv )
				{
					uv[0] = param[0];
					uv[1] = param[1];
				}
			}
		}
	}
	return bRet;

}
/////////////////////////////////////////////////////////////////////////
bool IntersectSurfacesByFireRay
(
	IwTArray<IwSurface*>		surfaces,	// I:
	IwPoint3d&					pt,			// I:
	IwVector3d&					vec,		// I:
	double						maxDistToHit,// I: 
	IwPoint3d&					pnt,		// O:
	int*						surfIndex,	// O:
	IwPoint2d*					uv,			// O: hit uv 
	IwTArray<int>*				dotValues,	// I: When the input surfaces are super complex (like surfaces with a big offset),
											//    you may need to further check/ensure whether the hit surface normals are consistent.
											//    If >0, hitNormal*vec must be >0. If <0, -> hitNormal*vec<0. If=0, not care.
	IwTArray<bool>*				hitEachSurf	// O: When the input surfaces are super complex (like surfaces with a big offset),
											//    you may also want to know how the fire ray hit each surface.
)
{
	if (hitEachSurf)
		hitEachSurf->RemoveAll();

	double minDist = HUGE_DOUBLE;
	double dist, dotValue;
	bool bInt, bRet = false;
	IwPoint3d cPnt;
	double param[2];
	IwPoint2d uvParam;
	IwVector3d normal;

	for (unsigned i=0; i<surfaces.GetSize(); i++)
	{
		bInt = IntersectSurfaceByFireRay(surfaces.GetAt(i), pt, vec, cPnt, param);
		if ( bInt && cPnt.DistanceBetween(pt) < maxDistToHit )
		{
			uvParam = IwPoint2d(param[0], param[1]);
			if ( dotValues )
			{
				surfaces.GetAt(i)->EvaluateNormal(uvParam, FALSE, FALSE, normal);
				dotValue = normal.Dot(vec);
				if ( dotValue*dotValues->GetAt(i) < 0 )// normal and vec are in opposite directions
				{
					if (hitEachSurf)
						hitEachSurf->Add(false);
					continue; // This is not an expected answer.
				}
			}

			bRet = true;
			if (hitEachSurf)
				hitEachSurf->Add(true);
			dist = cPnt.DistanceBetween(pt);
			if ( dist < minDist )
			{
				minDist = dist;
				pnt = cPnt;
				if ( surfIndex )
					*surfIndex = i;
				if (uv)
					*uv = uvParam;
			}
		}
		else
		{
			if (hitEachSurf)
				hitEachSurf->Add(false);
		}
	}
	return bRet;

}
////////////////////////////////////////////////////////////////
bool IntersectSurfaceByCurve( 
		const IwSurface*			pSurface,	// I:
		const IwCurve*				pCurve,		// I:
		IwTArray<IwPoint3d>&		intPnts,	// O: intersection points
		IwTArray<IwPoint2d>&		intUVParams,// O: surface intersection parameters
		IwTArray<double>&			intParams	// O: curve intersection parameters
)
{
	intPnts.RemoveAll();
	intUVParams.RemoveAll();
	intParams.RemoveAll();

	double tol=0.001;
	IwSolutionArray sols;
	IwSolution sol;
	pSurface->GlobalCurveIntersect(pSurface->GetNaturalUVDomain(), *pCurve, pCurve->GetNaturalInterval(), tol, sols);

	if ( sols.GetSize() == 0 )
		return false;

	for (unsigned i=0; i<sols.GetSize(); i++)
	{
		sol = sols.GetAt(i);
		double param = sol.m_vStart.m_adParameters[0];
		IwPoint2d UVParam(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
		IwPoint3d intPnt;
		pCurve->EvaluatePoint(param, intPnt);
		intPnts.Add(intPnt);
		intUVParams.Add(UVParam);
		intParams.Add(param);
	}

	return true;
}
////////////////////////////////////////////////////////////////
bool IntersectSurfaceByCurves( 
		const IwSurface*			pSurface,	// I:
		const IwTArray<IwCurve*>	pCurves,	// I:
		IwTArray<IwPoint3d>&		intPnts,	// O: intersection points
		IwTArray<IwPoint2d>&		intUVParams,// O: surface intersection parameters
		IwTArray<double>&			intParams	// O: curve intersection parameters
)
{
	intPnts.RemoveAll();
	intUVParams.RemoveAll();
	intParams.RemoveAll();

	IwTArray<IwPoint3d> pnts;
	IwTArray<IwPoint2d> UVParams;
	IwTArray<double> params;
	for (unsigned i=0; i<pCurves.GetSize(); i++)
	{
		IntersectSurfaceByCurve(pSurface, pCurves.GetAt(i), pnts, UVParams, params);
		intPnts.Append(pnts);
		intUVParams.Append(UVParams);
		intParams.Append(params);
	}

	if ( intPnts.GetSize() > 0 )
		return true;
	else
		return false;
}
bool GetBrepNormalAtPoint( IwBrep* pBrep, const IwPoint3d& pt, IwVector3d& nm, double tol, double* uv )
	{
	IwSolution			Sols[10];
	IwSolutionArray		Solutions( 10, Sols );
	double				dRayTolerance = 0.1;
	double				dRayStepoffValue = 0.1;
	int					nSols;
	IwFace*				pFace;

	dRayTolerance = tol;

	pBrep->RayIntersection( pt, NULL, dRayTolerance, dRayStepoffValue, Solutions ); 

	nSols = Solutions.GetSize();

	if( nSols == 0 )
		return false;

	IwSolution&		rSol = Solutions[0];

	IwObject*		pObject = (IwObject*) rSol.m_apObjects[0]; 

	pFace = IW_CAST_PTR( IwFace, pObject );

	if( pFace == NULL )
		return false;

	IwPoint2d		UV( rSol.m_vStart[0], rSol.m_vStart[1] );

	IwSurface*		pSur = pFace->GetSurface();

	IwPoint3d		pnt;
	double			d;

	pSur->EvaluatePoint( UV, pnt );

	d = dist( pt, pnt );
	
	pSur->EvaluateNormal( UV, true, true, nm );

	if( uv )
		{
		uv[0] = UV[0];
		uv[1] = UV[1];
		}

	return true;
	}


IwVector3d GetSurfNormalAtPoint( IwSurface* pSurf, const IwPoint3d& pt )
	{
	IwVector3d				nm;
	double					gap;
	IwBoolean				bOK;
	IwVector2d				uv;

	pSurf->DropPoint( pt, pSurf->GetNaturalUVDomain(), NULL, bOK, uv, gap );

	pSurf->EvaluateNormal( uv, true, true, nm );

	return nm;
	}


IwPoint3d DropPointOnSurface( IwSurface* pSurf, const IwPoint3d& pt, double* uv )
	{
	double					gap;
	IwBoolean				bOK;
	IwPoint2d				uvp;
	IwPoint3d				pnt;

	pSurf->DropPoint( pt, pSurf->GetNaturalUVDomain(), NULL, bOK, uvp, gap );

	pnt = EvalSurfacePoint( pSurf, uvp[0], uvp[1] );

	if( uv )
		{
		uv[0] = uvp[0];
		uv[1] = uvp[1];
		}

	return pnt;
	}

IwPoint3d DropPointOnPlane( 
		const IwPoint3d&				planeOrigin, 
		const IwVector3d&			planeNormal,	
		const IwPoint3d&				pointToBeDropped
)
{
	IwVector3d nm = planeNormal;
	nm.Unitize();

	IwPoint3d tempVec = planeOrigin - pointToBeDropped;
	double dotValue = nm.Dot(tempVec);
	IwVector3d nmc = nm;

	IwPoint3d droppedPoint = pointToBeDropped + dotValue*nmc;

	return droppedPoint;
}

void SetFacesId( IwBrep* pBrep, const char* filename )
	{
	IwTArray< IwFace* >		Faces;
	int					iFace, nFaces;

	pBrep->GetFaces( Faces );

	nFaces = Faces.GetSize();

	for( iFace = 0; iFace < nFaces; iFace++ )
		{
		IwFace*		pFace = Faces[iFace];

		pFace->SetUserIndex1( iFace + 1 );
		}

	if( filename )
		pBrep->WriteToFile( filename );
	}



void SetEdgesId( IwBrep* pBrep )
	{
	IwTArray< IwEdge* >		Edges;
	int					iEdge, nEdges;

	pBrep->GetEdges( Edges );

	nEdges = Edges.GetSize();

	for( iEdge = 0; iEdge < nEdges; iEdge++ )
		{
		IwEdge*		pEdge = Edges[iEdge];

		pEdge->SetUserIndex1( iEdge + 1 );
		}
	}



void CheckFaces( IwBrep* pBrep )
	{
	IwTArray< IwFace* >		Faces;
	int					iFace, nFaces;

	pBrep->GetFaces( Faces );

	nFaces = Faces.GetSize();

	for( iFace = 0; iFace < nFaces; iFace++ )
		{
		IwFace*		pFace = Faces[iFace];

		int id = pFace->GetUserIndex1();

		int a = 1;
		}
	}


void CollectLaminaEdgeCurves( IwBrep* pBrep, IwTArray< IwCurve* >& rCurves )
	{
	IwTArray< IwEdge* >		Edges;
	IwEdge*					pEdge;
	int						iEdge, nEdges;
	int						id, i, nFaces;

	pBrep->GetEdges( Edges );

	nEdges = Edges.GetSize();

	for( iEdge = 0; iEdge < nEdges; iEdge++ )
		{
		pEdge = Edges[iEdge];

		if( pEdge->IsLamina() )
			{
			IwTArray< IwFace* >		Faces;

			pEdge->GetFaces( Faces );

			nFaces = Faces.GetSize();

			for( i = 0; i < nFaces; i++ )
				{
				IwFace*		pFace = Faces[i];

				id = pFace->GetUserIndex1();

				int a = 1;
				}

			IwCurve*	pCurve;

			pEdge->GetCurve()->Copy( *iwContext, pCurve );

			rCurves.Add( pCurve );
			}
		}
	}


IwBrep* MakeBox( 
	const IwPoint3d&			center,
	const IwVector3d&		vx,
	const IwVector3d&		vy,
	double					l,
	double					w,
	double					h )
	{
	IwBrep*					pBrep = NewBrep( 0.0001 );
	IwPrimitiveCreation		pc( pBrep->GetInfiniteRegion() );
	IwPoint3d				orig = center - l/2 * vx - w/2 * vy;
	IwAxis2Placement		sFrame( orig, vx, vy );

	pc.CreateBox( l, w, h, sFrame );

	pBrep->SewAndOrient( 0.01 );

	return pBrep;
	}


IwBrep* MakeCylinder( 
	const IwPoint3d&			orig,
	const IwVector3d&		vx,
	const IwVector3d&		vy,
	double					r,
	double					h )
	{
	IwBrep*					pBrep = NewBrep( 0.1 );
	IwPrimitiveCreation		pc( pBrep->GetInfiniteRegion() );
	IwAxis2Placement		sFrame( orig, vx, vy );

	pc.CreateCone( h, r, r, 0.0, 360.0, sFrame );

	pBrep->SewAndOrient( 0.01 );

	return pBrep;
	}


IwBrep* MakeCylinder( 
	const IwPoint3d&			orig,
	const IwVector3d&		axis,
	double					r,
	double					h )
	{
	IwVector3d				vec, vx, vy;
	IwVector3d				v0( 0.6, 0.8, 0.0 );
	IwVector3d				v1( -0.8, 0.6, 0.0 );

	vec = ( fabs( axis[0] ) < fabs( axis[1] ) )?	v0 : v1;

	vx = NormalizedCross( vec, axis );
	vy = NormalizedCross( axis, vx );

	return MakeCylinder( orig, vx, vy, r, h );
	}


IwBrep* Boolean_ByIteration( 
	IwBooleanOperationType	eOperation,
	IwBrep*					pBrep1, 
	IwBrep*					pBrep2,
	double					tol,
	double					angTol,
	bool					bCheckSolidResult) 
	{
		IwBrep*					pResultBrep = NULL;
		double					original_tol = tol;
		double					original_angTol = angTol;
		IwTArray<IwFace*>		faces;
		unsigned faceNo;

		pResultBrep = Boolean(eOperation, pBrep1, pBrep2, original_tol, original_angTol, true);
		faceNo = 0;
		if ( pResultBrep != NULL )
		{
			pResultBrep->GetFaces(faces);
			faceNo = faces.GetSize();
			if (bCheckSolidResult)
			{
				bool isSolid = pResultBrep->IsManifoldSolid();
				if ( !isSolid )
					faceNo = 0;
			}
		}

		if (faceNo == 0)// try again with tighter tolerance
		{
			pResultBrep = Boolean(eOperation, pBrep1, pBrep2, original_tol/5, original_angTol/5, true);
			faceNo = 0;
			if (pResultBrep != NULL)
			{
				pResultBrep->GetFaces(faces);
				faceNo = faces.GetSize();
				if (bCheckSolidResult)
				{
					bool isSolid = pResultBrep->IsManifoldSolid();
					if ( !isSolid )
						faceNo = 0;
				}
			}

			if (faceNo == 0)// try again with tighter tolerance
			{
				pResultBrep = Boolean(eOperation, pBrep1, pBrep2, original_tol/25, original_angTol/25, true);
				faceNo = 0;
				if (pResultBrep != NULL)
				{
					pResultBrep->GetFaces(faces);
					faceNo = faces.GetSize();
					if (bCheckSolidResult)
					{
						bool isSolid = pResultBrep->IsManifoldSolid();
						if ( !isSolid )
							faceNo = 0;
					}
				}
				if (faceNo == 0)// try again with loosen tolerance
				{
					pResultBrep = Boolean(eOperation, pBrep1, pBrep2, 5*original_tol, 5*original_angTol, true);
				}
				// if it still fails, let it fail.
			}
		}

		return pResultBrep;
	}

IwBrep* Boolean( 
	IwBooleanOperationType	eOperation,
	IwBrep*					pBrep1, 
	IwBrep*					pBrep2,
	double					tol,
	double					angTol,
	bool					bCopyInputBreps) 
	{
	long					rc;
	IwBrep*					pCopy1 = NULL;
	IwBrep*					pCopy2 = NULL;
	IwBrep*					pResultBrep = NULL;
	double					dTol, dAngleTol;

	if ( bCopyInputBreps )
	{
		pCopy1 = new ( *iwContext ) IwBrep( *pBrep1 );
		pCopy2 = new ( *iwContext ) IwBrep( *pBrep2 );
	}
	else
	{
		pCopy1 = pBrep1;
		pCopy2 = pBrep2;
	}
	
	dTol = ( tol == 0.0 )?		0.01 : tol;
	dAngleTol = ( angTol == 0.0 )?	0.01 : angTol;

	pCopy1->SewAndOrient();
//	pCopy1->ShrinkGeometry();

	pCopy2->SewAndOrient();
//	pCopy2->ShrinkGeometry();

	IwMerge					iwMerge( *iwContext, pCopy1, pCopy2, dTol, dAngleTol );

	rc = iwMerge.ManifoldBoolean( eOperation, pResultBrep );

	if( rc == IW_SUCCESS )
	{
		pResultBrep->SewAndOrient();
		pResultBrep->SewAndOrient();
	}
	else
	{
		if ( bCopyInputBreps )
		{
			IwObjDelete(pCopy1);
			IwObjDelete(pCopy2);
		}
		pResultBrep = NULL;
	}

	return pResultBrep;
	}


CPart* AddNewPart( CDataDoc* pDoc, IwBrep* pBrep, CEntRole eEntRole, const CColor& color )
	{
	CPart*	pPart = new CPart( pDoc, eEntRole );

	pPart->SetIwBrep( pBrep );
	
	pPart->SetColor( color );

	pPart->MakeTess();

	pDoc->AddPart( pPart );

	return pPart;
	}

struct CCurve
	{
	IwCurve*		pCurve;
	IwPoint3d		pt0;
	IwPoint3d		pt1;
	bool			bTaken;

	CCurve()
		{
		pCurve = NULL;
		bTaken = false;
		}
	};


void SortSectionCurves( CCurveArray& Curves, int iStart )
	{
	int						iCurve, nCurves, nc;
	CCurve*					inArray;
	IwExtent1d				extent;
	double					d0, d1;
	IwCurve*				pCurve;
	IwPoint3d				pt0, pt1, ptCur;
	double					eps = 0.001;

	nCurves = Curves.GetSize();

	inArray = new CCurve[ nCurves ];

	for( iCurve = 0; iCurve < nCurves; iCurve++ )
		{
		pCurve = Curves[iCurve];

		GetCurveEndPoints( pCurve, pt0, pt1 );

		inArray[iCurve].pCurve	= pCurve;
		inArray[iCurve].pt0		= pt0;
		inArray[iCurve].pt1		= pt1;
		}

	pCurve = Curves[iStart];
		
	nc = 0;

	Curves[ nc++ ] = pCurve;
	inArray[iStart].bTaken = true;

	ptCur = inArray[iStart].pt1;

	while( nc < nCurves )
		{
		for( iCurve = 0; iCurve < nCurves; iCurve++ )
			{
			if( inArray[iCurve].bTaken )
				continue;

			d0 = dist( inArray[iCurve].pt0, ptCur );
			d1 = dist( inArray[iCurve].pt1, ptCur );

			if( d0 < eps || d1 < eps )
				{
				pCurve = inArray[iCurve].pCurve;

				Curves[ nc++ ] = pCurve;
				inArray[iCurve].bTaken = true;

				if( d1 < eps )
					{
					InvertCurve( pCurve );
					Swap( inArray[iCurve].pt0, inArray[iCurve].pt1 );
					}
					
				ptCur = inArray[iCurve].pt1;
				}
			}
		}

	delete [] inArray;
	}


void SortCurvesChain( CCurveArray& Curves, int iStart )
	{
	int						iCurve, nCurves, i, n;
	CCurve*					inArray;
	IwExtent1d				extent;
	double					d0, d1;
	IwCurve*				pCurve;
	IwPoint3d				pt0, pt1, ptCur;
	double					eps = 0.01;
	CCurveArray				Curves0, Curves1;
	bool					bDone;

	nCurves = Curves.GetSize();

	if( nCurves < 2 )
		return;

	inArray = new CCurve[ nCurves ];

	for( iCurve = 0; iCurve < nCurves; iCurve++ )
		{
		pCurve = Curves[iCurve];

		inArray[iCurve].pCurve	= pCurve;
		inArray[iCurve].pt0		= GetCurveStartPoint( pCurve );
		inArray[iCurve].pt1		= GetCurveEndPoint( pCurve );
		}

	pCurve = Curves[iStart];
		
	Curves0.Add( pCurve );
	inArray[iStart].bTaken = true;
	ptCur = inArray[iStart].pt1;
	bDone = false;

	while( !bDone )
		{
		for( iCurve = 0; iCurve < nCurves; iCurve++ )
			{
			if( inArray[iCurve].bTaken )
				continue;

			d0 = dist( inArray[iCurve].pt0, ptCur );
			d1 = dist( inArray[iCurve].pt1, ptCur );

			if( d0 < eps || d1 < eps )
				{
				pCurve = inArray[iCurve].pCurve;

				Curves0.Add( pCurve );
				inArray[iCurve].bTaken = true;

				if( d1 < eps )
					{
					extent = pCurve->GetNaturalInterval();
					pCurve->ReverseParameterization( extent, extent );
					Swap( inArray[iCurve].pt0, inArray[iCurve].pt1 );
					}
					
				ptCur = inArray[iCurve].pt1;

				break;
				}
			}

		if( iCurve == nCurves )
			bDone = true;
		}

	ptCur = inArray[iStart].pt0;
	bDone = false;

	while( !bDone )
		{
		for( iCurve = 0; iCurve < nCurves; iCurve++ )
			{
			if( inArray[iCurve].bTaken )
				continue;

			d0 = dist( inArray[iCurve].pt0, ptCur );
			d1 = dist( inArray[iCurve].pt1, ptCur );

			if( d0 < eps || d1 < eps )
				{
				pCurve = inArray[iCurve].pCurve;

				Curves1.Add( pCurve );
				inArray[iCurve].bTaken = true;

				if( d1 < eps )
					{
					extent = pCurve->GetNaturalInterval();
					pCurve->ReverseParameterization( extent, extent );
					Swap( inArray[iCurve].pt0, inArray[iCurve].pt1 );
					}
					
				ptCur = inArray[iCurve].pt1;

				break;
				}
			}

		if( iCurve == nCurves )
			bDone = true;
		}

	delete [] inArray;

	Curves.SetSize(0);

	n = Curves1.GetSize();

	for( i = n - 1; i >= 0; i-- )
		{
		pCurve = Curves1[i];

		extent = pCurve->GetNaturalInterval();
		pCurve->ReverseParameterization( extent, extent );

		Curves.Add( pCurve );
		}

	n = Curves0.GetSize();

	for( i = 0; i < n; i++ )
		{
		pCurve = Curves0[i];

		Curves.Add( pCurve );
		}
	}


int SortCurves( CCurveArray& Curves, CIntArray& nCurves )
	{
	int						nc, i, n1, n;
	CCurveArray				crvs1( Curves );
	CCurveArray				crvs2( Curves );

	nc = Curves.GetSize();

	Curves.SetSize(0);

	while( crvs2.GetSize() > 0 )
		{
		SortCurvesChain( crvs2 );
		n = crvs2.GetSize();

		Curves.Append( crvs2 );
		nCurves.Add( n );

		if( Curves.GetSize() == nc )
			break;

		crvs2.SetSize(0);

		n1 = crvs1.GetSize();

		for( i = 0; i < n1; i++ )
			{
			if( IsInArray( crvs1[i], Curves ) )
				{
				crvs1.RemoveAt( i, 1 );
				i--;
				n1--;
				}
			else
				{
				crvs2.Add( crvs1[i] );
				}
			}
		}

	return nCurves.GetSize();
	}


IwPoint3d GetCurveStartPoint( IwCurve* pCurve )
	{
	IwExtent1d				extent;
	IwPoint3d				pt;

	extent = pCurve->GetNaturalInterval();

	EvalCurvePoint( pCurve, extent.GetMin(), pt );

	return pt;
	}


IwPoint3d GetCurveEndPoint( IwCurve* pCurve )
	{
	IwExtent1d				extent;
	IwPoint3d				pt;

	extent = pCurve->GetNaturalInterval();

	EvalCurvePoint( pCurve, extent.GetMax(), pt );

	return pt;
	}


IwVector3d GetCurveStartTangent( IwCurve* pCurve, bool bNomalize )
	{
	IwExtent1d				extent;
	IwVector3d				tn;

	extent = pCurve->GetNaturalInterval();

	EvalCurveTangent( pCurve, extent.GetMin(), tn, bNomalize );

	return tn;
	}


IwVector3d GetCurveEndTangent( IwCurve* pCurve, bool bNomalize )
	{
	IwExtent1d				extent;
	IwVector3d				tn;

	extent = pCurve->GetNaturalInterval();

	EvalCurveTangent( pCurve, extent.GetMax(), tn, bNomalize );

	return tn;
	}


IwBSplineCurve* MakeLine( 
	const IwPoint3d&			p0, 
	const IwPoint3d&			p1 )
	{
	IwBSplineCurve*			pCurve;

	IwBSplineCurve::CreateLineSegment( *iwContext, 3, p0, p1, pCurve );

	return pCurve;
	}


IwBSplineCurve* MakeArc( 
	const IwPoint3d&			cnt, 
	const IwPoint3d&			p0, 
	const IwPoint3d&			p1 )
	{
	IwBSplineCurve*			pCurve;

	IwBSplineCurve::CreateArcFromPoints( *iwContext, 3, cnt, p0, p1, IW_CO_QUADRATIC, pCurve );
	
	return pCurve;
	}


IwBSplineCurve* AddLine( 
	const IwPoint3d&			p0, 
	const IwPoint3d&			p1, 
	CCurveArray&			Curves )
	{
	IwBSplineCurve*			pCurve = MakeLine( p0, p1 );

	Curves.Add( pCurve );

	return pCurve;
	}


IwBSplineCurve* AddArc( 
	const IwPoint3d&			cnt, 
	const IwPoint3d&			p0, 
	const IwPoint3d&			p1, 
	CCurveArray&			Curves )
	{
	IwBSplineCurve*			pCurve = MakeArc( cnt, p0, p1 );

	Curves.Add( pCurve );

	return pCurve;
	}


IwBSplineCurve* AddLine( 
	const IwPoint3d&			p0, 
	const IwPoint3d&			p1, 
	CSplineArray&			Curves )
	{
	IwBSplineCurve*			pCurve = MakeLine( p0, p1 );

	Curves.Add( pCurve );

	return pCurve;
	}


IwBSplineCurve* AddArc( 
	const IwPoint3d&			cnt, 
	const IwPoint3d&			p0, 
	const IwPoint3d&			p1, 
	CSplineArray&			Curves )
	{
	IwBSplineCurve*			pCurve = MakeArc( cnt, p0, p1 );

	Curves.Add( pCurve );

	return pCurve;
	}


IwBrep* MakeLinearSweep( 
	CCurveArray&			Curves,
	const IwVector3d&		vec,
	double					height,
	bool					bMakeCaps )
	{
	IwBrep*					pBrep = NewBrep( 0.0001 );
	IwPrimitiveCreation		pc( pBrep->GetInfiniteRegion() );
	IwVector3d				V = vec;

	pc.CreateLinearSweep( Curves, V, height, 1, bMakeCaps, false );

	pBrep->SewAndOrient( 0.01 );

	return pBrep;
	}


IwBrep* MakeRotationalSweep( 
	CCurveArray&			Curves,
	const IwPoint3d&			pt,			
	const IwVector3d&		vec,
	double					angle )
	{
	IwBrep*					pBrep = NewBrep( 0.0001 );
	IwPrimitiveCreation		pc( pBrep->GetInfiniteRegion() );
	IwVector3d				V = vec;
	IwPoint3d				P = pt;

	pc.CreateRotationalSweep( Curves, P, V, angle, 1, true, false );

	pBrep->SewAndOrient( 0.01 );

	return pBrep;
	}


long MakePlanarFace( IwBrep* pBrep, CCurveArray& curves, IwFace** pNewFace )
	{
	IwFace*		pFace;
	long		rc;

	rc = pBrep->CreatePlanarFaceWith3DCurves( pBrep->GetInfiniteRegion(), curves, 0.00001, pFace );

	if( rc == IW_SUCCESS && pNewFace )
		*pNewFace = pFace;

	return rc;
	}


bool IsCurvePlanar( IwCurve* pCurve, IwPoint3d& pt, IwVector3d& nm )
	{
	IwPoint3d				PT;
	IwVector3d				NM;
	double					tol = 0.001;
	bool					bPlanar;

	bPlanar = pCurve->IsPlanar( tol, &PT, &NM );

	if( bPlanar )
		{
		pt = PT;
		nm = NM;
		}

	return bPlanar;
	}

void FindCommonEdges( const CFaceArray& Faces1, const CFaceArray& Faces2, CEdgeArray& CommonEdges )
	{
	ULONG					i, j, n1, n2;
	IwEdge*					pEdge1;
	IwEdge*					pEdge2;
	CEdgeArray				Edges, Edges1, Edges2;

	n1 = Faces1.GetSize();

	for( i = 0; i < n1; i++ )
		{
		Faces1[i]->GetEdges( Edges );

		for( j = 0; j < Edges.GetSize(); j++ )
			Edges1.AddUnique( Edges[j] );
		}	

	n2 = Faces2.GetSize();

	for( i = 0; i < n2; i++ )
		{
		Faces2[i]->GetEdges( Edges );

		for( j = 0; j < Edges.GetSize(); j++ )
			Edges2.AddUnique( Edges[j] );
		}	

	n1 = Edges1.GetSize();
	n2 = Edges2.GetSize();

	for( i = 0; i < n1; i++ )
		{
		pEdge1 = Edges1[i];

		for( j = 0; j < n2; j++ )
			{
			pEdge2 = Edges2[j];

			if( pEdge1 == pEdge2 )
				{
				CommonEdges.AddUnique( pEdge1 );
				break;
				}
			}
		}
	}


int GetFaceIndex( IwBrep* pBrep, IwFace* pFace )
	{
	CFaceArray				Faces;
	int						i, nFaces;

	pBrep->GetFaces( Faces );

	nFaces = Faces.GetSize();

	for( i = 0; i < nFaces; i++ )
		{
		if( Faces[i] == pFace )
			return i;
		}

	return -1;
	}


int GetEdgeIndex( IwBrep* pBrep, IwEdge* pEdge )
	{
	CEdgeArray				Edges;
	int						i, nEdges;

	pBrep->GetEdges( Edges );

	nEdges = Edges.GetSize();

	for( i = 0; i < nEdges; i++ )
		{
		if( Edges[i] == pEdge )
			return i;
		}

	return -1;
	}


IwBSplineCurve* GetEdgeCurve( IwEdge* pEdge )
	{
	return (IwBSplineCurve*) pEdge->GetCurve();
	}


IwEdge* GetEdge( IwBrep* pBrep, int iEdge )
	{
	CEdgeArray				Edges;

	pBrep->GetEdges( Edges );

	return Edges[iEdge];
	}

///////////////////////////////////////////////////////////////
// tolFactor = how to sample the input curves
IwBSplineCurve*	ApproximateWithBSpline( CCurveArray& Curves, double	tolFactor )
	{
	IwBSplineCurve*			pSpline;
	IwTArray< IwPoint3d >		points;
	int						i, np, iCurve, nCurves, n;
	IwCurve*				pCurve;
	double					tol = 0.01;
	double					tolDeg = 0.1;
	double					eps = 0.01;
	double					d0, d1, d2;
	double					len;
	IwTArray<IwPoint3d>		pts;

	nCurves = Curves.GetSize();

	for( iCurve = 0; iCurve < nCurves; iCurve++ )
		{
		pCurve = Curves[iCurve];

		pCurve->Length(pCurve->GetNaturalInterval(), 0.001, len);

		if( len < eps )
			continue;

		n = points.GetSize();

		if( n > 0 )
			points.RemoveAt( n - 1 );

		np = (int)(len/tolFactor) + 1;
		if ( np < 10 )
			np = 10; // at least 10 points tp approximate a curve

		ComputePointsOnCurve( pCurve, np, pts, true );

		if( points.GetSize() == 0 )
			points.Add( pts[0] );

		for( i = 1; i < np; i++ )
			points.Add( pts[i] );
		}

	if( points.GetSize() == 0 )
		return NULL;

	long rc = IwBSplineCurve::ApproximatePoints( *iwContext, points, 3, NULL, NULL, false, &tol, pSpline );

	pSpline->RemoveKnots(tol); // Simplify curve

	return pSpline;
	}

IwBSplineCurve*	ApproximateClosedCurvesWithBSpline( 
		CCurveArray&				Curves,
		IwPoint3d					newSealPoint,
		double						tolFactor)
{
	IwBSplineCurve*			pSpline;
	IwTArray< IwPoint3d >		points;
	int						i, np, iCurve, nCurves, n;
	IwCurve*				pCurve;
	double					tol = 0.01;
	double					tolDeg = 0.1;
	double					eps = 0.01;
	double					d0, d1, d2;
	double					len;
	IwTArray<IwPoint3d>		pts;

	nCurves = Curves.GetSize();

	if ( nCurves == 1 )
	{
		pCurve = Curves[0];
		if ( !pCurve->IsClosed(pCurve->GetNaturalInterval()) )
			return NULL;
	}

	for( iCurve = 0; iCurve < nCurves; iCurve++ )
		{
		pCurve = Curves[iCurve];

		pCurve->Length(pCurve->GetNaturalInterval(), 0.001, len);

		if( len < eps )
			continue;

		n = points.GetSize();

		if( n > 0 )
			points.RemoveAt( n - 1 );

		np = (int)(len/tolFactor) + 1;
		if ( np < 10 )
			np = 10; // at least 10 points tp approximate a curve

		ComputePointsOnCurve( pCurve, np, pts, true );

		if( points.GetSize() == 0 )
			points.Add( pts[0] );

		for( i = 1; i < np; i++ )
			points.Add( pts[i] );
		}

	if( points.GetSize() == 0 )
		return NULL;

	if ( points.GetAt(0).DistanceBetween(points.GetLast()) > eps ) // gap exist, not closed curve
		return NULL;

	// Remove the last one
	points.RemoveLast();
	// Rotate the array
	IwPoint3d cPnt;
	int index;
	double dist = DistFromPointToIwTArray(newSealPoint, points, NULL, cPnt, &index);
	points.RotateArray(index);
	// Add the first one to last one
	points.Add(points.GetAt(0));

	long rc = IwBSplineCurve::ApproximatePoints( *iwContext, points, 3, NULL, NULL, true, &tol, pSpline );

	pSpline->RemoveKnots(tol); // Simplify curve

	return pSpline;
}

void ApproximateChainsWithSplines(
	const CIntArray&		nc,
	const CCurveArray&		curves,
	CSplineArray&			splines )
	{
	CCurveArray				crvs;
	int						i, i0, i1, ic, nChains;

	nChains = nc.GetSize();

	splines.SetSize( nChains );

	i1 = 0;

	for( ic = 0; ic < nChains; ic++ )
		{
		crvs.ReSet();

		i0 = i1;
		i1 += nc[ic];

		for( i = i0; i < i1; i++ )
			crvs.Add( curves[i] );

		splines[ic] = ApproximateWithBSpline( crvs );
		}
	}

IwBSplineCurve* CrossSectBrep( 
	IwBrep*					pBrep,
	const IwPoint3d&			pt,
	const IwVector3d&		nm )
	{
	CCurveArray				curves;
	IwBSplineCurve*			pCurve;
	CIntArray				nc;
	double					tol(1.0e-5);

	pBrep->CreatePlanarSectionCurves( *iwContext, pt, nm, &tol, NULL, &curves, NULL, NULL );

	if( curves.GetSize() == 0 )
		return NULL;

	pCurve = MakeSplineFromCurves( curves, pt );

	return pCurve;
	}


IwBSplineCurve* MakeSplineFromCurves( 
	CCurveArray				curves,
	const IwPoint3d&			pt )
	{
	IwBSplineCurve*			pCurve;
	CIntArray				nc;
	int						nChains;
	bool					bDebug = false;

	nChains = SortCurves( curves, nc );

	if( nChains == 1 )
		{
		pCurve = ApproximateWithBSpline( curves );
		}
	else
		{
		CSplineArray	SplineArray;
		CCurveArray		crvs;
		int				i0, i1, i, iChain, iChainBest;
		double			d, dmin = HUGE_DOUBLE;
		IwPoint3d		pnt;

		SplineArray.SetSize( nChains );
		
		i1 = 0;

		for( iChain = 0; iChain < nChains; iChain++ )
			{
			crvs.ReSet();

			i0 = i1;
			i1 += nc[iChain];

			for( i = i0; i < i1; i++ )
				crvs.Add( curves[i] );

			SplineArray[iChain] = ApproximateWithBSpline( crvs );

			if( SplineArray[iChain] == NULL )
				continue;

			DropPointOnCurve( SplineArray[iChain], pt, &pnt );

			d = dist( pt, pnt );

			if( d < dmin )
				{
				dmin = d;
				iChainBest = iChain;
				}
			}

		pCurve = SplineArray[iChainBest];
		}

	return pCurve;
	}


IwBSplineCurve* MakeBrepSection( 
	IwBrep*					pBrep,
	const IwPoint3d&			pt,
	const IwVector3d&		nm )
	{
	CCurveArray				curves;
	IwBSplineCurve*			pCurve;
	double					tol(1.0e-5);

	pBrep->CreatePlanarSectionCurves( *iwContext, pt, nm, &tol, NULL, &curves, NULL, NULL );

	if( curves.GetSize() == 0 )
		return NULL;

	pCurve = (IwBSplineCurve*) curves[0];

	return pCurve;
	}


int CrossSectSurface(
	IwBSplineSurface*		pSurface,
	const IwPoint3d&			pt,
	const IwVector3d&		nm,
	CCurveArray&			curves )
	{
	double					tol(1.0e-4);
	double					angtol = 1e-3;

	pSurface->CreatePlanarSectionCurves( *iwContext, pSurface->GetNaturalUVDomain(),
						pt, nm, &tol, &angtol, NULL, &curves );

	return curves.GetSize();
	}


double DropPointOnCurve( IwCurve* pCurve, const IwPoint3d& pt, IwPoint3d* pnt )
	{
	IwExtent1d				extent = pCurve->GetNaturalInterval();
	IwBoolean				bOK;
	double					t, d;

	pCurve->DropPoint( extent, pt, NULL, IW_BIG_DOUBLE, NULL, bOK, t, d );

	if( pnt )
		EvalCurvePoint( pCurve, t, *pnt );

	return t;
	}

void GetCurveEndPoints( IwCurve* pCurve, IwPoint3d& pt0, IwPoint3d& pt1 )
	{
	pt0 = GetCurveStartPoint( pCurve );
	pt1 = GetCurveEndPoint( pCurve );
	}


void GetCurveEndParams(  IwCurve* pCurve, double& t0, double& t1 )
	{
	IwExtent1d				extent;

	extent = pCurve->GetNaturalInterval();

	t0 = extent.GetMin();
	t1 = extent.GetMax();
	}


void GetEdgeEndParams( IwEdge* pEdge, double& t0, double& t1 )
	{
	IwExtent1d				extent;

	extent = pEdge->GetInterval();

	t0 = extent.GetMin();
	t1 = extent.GetMax();
	}


void GetEdgeEndPoints( IwEdge* pEdge, IwPoint3d& p0, IwPoint3d& p1 )
	{
	IwExtent1d				extent;
	IwCurve*				pCurve;
	double					t0, t1;

	pCurve = pEdge->GetCurve();
	extent = pEdge->GetInterval();

	t0 = extent.GetMin();
	t1 = extent.GetMax();

	EvalCurvePoint( pCurve, t0, p0 );
	EvalCurvePoint( pCurve, t1, p1 );
	}

int IntersectCurveByPlane( 
	IwCurve*				pCurve, 
	const IwPoint3d&		pt, 
	const IwVector3d&		nm,
	CPointArray&			pts,
	IwTArray<double>*		params)
{
	IwExtent1d				extent = pCurve->GetNaturalInterval();
	IwSolutionArray			Sols;
	IwVector3d				NM = nm;
	int						i, np;
	double					D, t;
	double					tol = 0.001;
	
	D = - pt.Dot( nm );

	pCurve->GlobalPropertyAnalysis( extent, IW_CP_PLANE_INTERSECTION, &D, &NM, tol, Sols );

	np = Sols.GetSize();

	pts.SetSize( np );

	for( i = 0; i < np; i++ )
	{
		t = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve, t, pts[i] );

		if ( params )
			(*params).Add(t);
	}

	return np;
}


bool IntersectCurveByPlane( 
	IwCurve*				pCurve, 
	const IwPoint3d&			pnt, 
	const IwVector3d&		nrm,
	IwPoint3d&				pt,
	double*					t )
	{
	int						i, n, ibest;
	double					d, dmin;
	CPointArray				pts;

	n = IntersectCurveByPlane( pCurve, pnt, nrm, pts );

	if( n == 0 )
		return false;

	if( n == 1 )
		{
		pt = pts[0];
		}
	else
		{
		dmin = HUGE_DOUBLE;

		for( i = 0; i < n; i++ )
			{
			d = dist( pts[i], pnt );

			if( d < dmin )
				{
				dmin = d;
				ibest = i;
				}
			}

		pt = pts[ibest];
		}

	if( t != NULL )
		*t = DropPointOnCurve( pCurve, pt );

	return true;
	}

bool IntersectCurvesByPlane
( 
	IwTArray<IwCurve*>		pCurve, // I:
	const IwPoint3d&		pnt,	// I:
	const IwVector3d&		nrm,	// I:
	IwPoint3d&				pt,		// O:
	double*					t,		// O:
	int*					crvIndex// O: the intersection point occurs in this curve.
)
{
	double dist , minDist = HUGE_DOUBLE;
	IwPoint3d cPnt;
	double param;

	for (unsigned i=0; i<pCurve.GetSize(); i++)
	{
		if ( IntersectCurveByPlane(pCurve.GetAt(i), pnt, nrm, cPnt, &param) )
		{
			dist = pnt.DistanceBetween(cPnt);
			if ( dist < minDist )
			{
				minDist = dist;
				pt = cPnt;
				if ( t )
					*t = param;
				if ( crvIndex )
					*crvIndex = i;
			}
		}
	}

	if ( minDist < HUGE_DOUBLE )
		return true;
	else
		return false;
}

int IntersectCurveByPlane2( 
	IwCurve*				pCurve, 
	const IwPoint3d&			pt, 
	const IwVector3d&		nm,
	CPointArray&			pts )
	{
	int						i, np;
	double					u, t, t0, t1;
	double					par, par0, par1, f0, f1, D;
	IwPoint3d				p, p0, p1;
	IwVector3d				NM = nm;
	double					tol = 0.001;
	IwBoolean				bFound;
	IwSolution				Sol;
	CPointArray				points;
	
	D = - pt.Dot(nm);

	np = 100;

	IwExtent1d				extent = pCurve->GetNaturalInterval();
	
	t0 = extent.GetMin();
	t1 = extent.GetMax();

	for( i = 0; i < np; i++ )
		{
		u = 1.0 * i / ( np - 1 );

		t = ( 1.0 - u ) * t0 + u * t1;

		EvalCurvePoint( pCurve, t, p );

		points.Add( p );
		}

	for( i = 0; i < np - 1; i++ )
		{
		p0 = points[i];
		p1 = points[i+1];

		f0 = ( p0 - pt ).Dot(nm);
		f1 = ( p1 - pt ).Dot(nm);

		if( f0 > 0.0 != f1 > 0.0 )
			{
			//p = ( f1 * p0 - f0 * p1 ) / ( f1 - f0 );

			par0 = double( i ) / ( np - 1 );
			par1 = double( i + 1 ) / ( np - 1 );
			par = ( f1 * par0 - f0 * par1 ) / ( f1 - f0 );

			pCurve->LocalPropertyAnalysis( extent, IW_CP_PLANE_INTERSECTION, t, &D, &NM, bFound, Sol );

			if( bFound )
				{
				u = Sol.m_vStart.m_adParameters[0];

				EvalCurvePoint( pCurve, u, p );

				pts.Add( p );
				}
			}
		}

	return pts.GetSize();
	}

int IntersectEdgeByPlane( 
	IwEdge*					pEdge, 
	const IwPoint3d&		pt, 
	const IwVector3d&		nm,
	CPointArray&			pts)
{
	IwCurve*				pCurve = pEdge->GetCurve();
	IwExtent1d				extent = pEdge->GetInterval();
	IwSolutionArray			Sols;
	IwVector3d				NM = nm;
	int						i, np;
	double					D, t;
	double					tol = 0.001;
	
	D = - pt.Dot( nm );

	pCurve->GlobalPropertyAnalysis( extent, IW_CP_PLANE_INTERSECTION, &D, &NM, tol, Sols );

	np = Sols.GetSize();

	pts.SetSize( np );

	for( i = 0; i < np; i++ )
	{
		t = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve, t, pts[i] );
	}

	return np;
}


bool IntersectEdgeByPlane( 
	IwEdge*					pEdge, 
	const IwPoint3d&		pnt, 
	const IwVector3d&		nrm,
	IwPoint3d&				pt )
{
	int						i, n, ibest;
	double					d, dmin;
	CPointArray				pts;

	n = IntersectEdgeByPlane( pEdge, pnt, nrm, pts );

	if( n == 0 )
		return false;

	if( n == 1 )
		{
		pt = pts[0];
		}
	else
		{
		dmin = HUGE_DOUBLE;

		for( i = 0; i < n; i++ )
			{
			d = dist( pts[i], pnt );

			if( d < dmin )
				{
				dmin = d;
				ibest = i;
				}
			}

		pt = pts[ibest];
		}

	return true;
}

bool IntersectEdgesByPlane
( 
	IwTArray<IwEdge*>			pEdges, 
	const IwPoint3d&			pnt, 
	const IwVector3d&			nrm,
	IwPoint3d&					pt,
	int*						edgeIndex
)
{
	double dist, minDist = HUGE_DOUBLE;
	bool bInt;
	IwPoint3d cPnt;

	for (unsigned i=0; i<pEdges.GetSize(); i++)
	{
		bInt = IntersectEdgeByPlane(pEdges.GetAt(i), pnt, nrm, cPnt);
		if ( bInt )
		{
			dist = pnt.DistanceBetween(cPnt);
			if ( dist < minDist )
			{
				minDist = dist;
				pt = cPnt;
				if ( edgeIndex )
					*edgeIndex = i;
			}
		}
	}

	if ( minDist < HUGE_DOUBLE )
		return true;
	else
		return false;
}

bool IntersectCurveByVector(
	    IwCurve*					pCurve,
		IwExtent1d					crvIntval,
		IwPoint3d					point,
		IwVector3d					vector,
		double*						guessParam,
		double						maxDist,
		IwPoint3d&					intersectPnt,
		double&						intersectParam,
		IwVector3d&					intersectTangent)
{
	bool intersected = false;

	if ( pCurve == NULL ) return intersected;

	IwLine* line = NULL;
	IwLine::CreateCanonical(*iwContext, point, vector, line);

	if ( guessParam == NULL )
	{
		IwSolutionArray sols;
		pCurve->GlobalCurveSolve(crvIntval, *line, line->GetNaturalInterval(), IW_SO_MINIMIZE, 0.01, NULL, NULL, IW_SR_ALL, sols);

		if ( sols.GetSize() == 0 )
		{
			if (line) 
				IwObjDelete(line);
			return intersected;
		}
		else if ( sols.GetSize() == 1 )
		{
			if ( sols.GetAt(0).m_vStart.m_dSolutionValue > maxDist )
				return intersected;
			intersectParam = sols.GetAt(0).m_vStart.m_adParameters[0];
		}
		else // multiple
		{
			IwPoint3d pnt;
			double dist, minDist = HUGE_DOUBLE;
			double minParam;
			for (unsigned i=0;i<sols.GetSize();i++) 
			{
				pCurve->EvaluatePoint(sols.GetAt(i).m_vStart.m_adParameters[0], pnt);
				dist = point.DistanceBetween(pnt);
				if ( dist < minDist )
				{
					minDist = dist;
					minParam = sols.GetAt(i).m_vStart.m_adParameters[0];
				}
			}

			if ( minDist > maxDist )
				return intersected;
			intersectParam = minParam;
		}

		IwVector3d derives[2];
		pCurve->Evaluate(intersectParam, 1, FALSE, derives);
		intersectPnt = derives[0];
		intersectTangent = derives[1];
		intersected = true;
	}
	else
	{
		IwBoolean foundAnswer;
		IwSolution sol;
		pCurve->LocalCurveSolve(crvIntval, *line, line->GetNaturalInterval(), IW_SO_MINIMIZE, 0.01, NULL, NULL, *guessParam, 0.0, foundAnswer, sol);
		if ( !foundAnswer )
			return intersected;
		if (sol.m_vStart.m_dSolutionValue > maxDist )
			return intersected;

		intersectParam = sol.m_vStart.m_adParameters[0];
		IwVector3d derives[2];
		pCurve->Evaluate(intersectParam, 1, FALSE, derives);
		intersectPnt = derives[0];
		intersectTangent = derives[1];
		intersected = true;
	}

	return intersected;
}

bool IntersectCurvesByVector
(
	    IwTArray<IwCurve*>			curves,			// I:
		IwPoint3d					point,			// I:
		IwVector3d					vector,			// I:
		IwPoint3d&					intersectPnt,	// O:
		double&						intersectParam,	// O:
		int&						whichOne,		// O:
		double*						tol				// I:
)
{
	bool intersected = false;

	bool bInt, found = false;

	whichOne = -1;
	
	double distTol = 0.01;
	if ( tol != NULL )
		distTol = *tol;

	IwCurve* crv;

	IwPoint3d intPnt, tang;
	double intParam, dist, minDist = HUGE_DOUBLE;

	for (unsigned i=0; i<curves.GetSize(); i++)
	{
		crv = curves.GetAt(i);
		bInt = IntersectCurveByVector(crv, crv->GetNaturalInterval(), point, vector, NULL, distTol, intPnt, intParam, tang); 
		if ( bInt )
		{
			dist = point.DistanceBetween(intPnt);
			if ( dist < minDist )
			{
				minDist = dist;
				intersectPnt = intPnt;
				intersectParam = intParam;
				whichOne = i;
				found = true;
			}
		}
	}

	return found;
}

bool IntersectVectorByVector(
		IwPoint3d pnt0,
		IwVector3d vector0,
		IwPoint3d pnt1,
		IwVector3d vector1,
		IwPoint3d& intersectPnt)
{
	bool intersected = false;

	IwLine* line0 = NULL;
	IwLine::CreateCanonical(*iwContext, pnt0, vector0, line0);
	IwLine* line1 = NULL;
	IwLine::CreateCanonical(*iwContext, pnt1, vector1, line1);

	IwSolutionArray sols;
	IwSolution sol;
	IwBoolean needMoreAnswer = FALSE;
	line0->IntersectWithLine(line0->GetNaturalInterval(), *line1, line1->GetNaturalInterval(), 0.025, needMoreAnswer, sols);
	if ( sols.GetSize() > 0 )
	{
		sol = sols.GetAt(0);
		intersectPnt = pnt0 + sol.m_vStart.m_adParameters[0]*vector0;
		intersected = true;
	}

	if ( line0 )
		IwObjDelete(line0);
	if ( line1 )
		IwObjDelete(line1);

	return intersected;
}

bool IntersectPlaneByVector(
		IwPoint3d PlanePnt,
		IwVector3d PlaneNormal,
		IwPoint3d vectorPnt,
		IwVector3d vector,
		IwPoint3d& intersectPnt)
{
	bool intersected = false;

	if ( PlaneNormal.IsPerpendicularTo(vectorPnt, 0.01) )
		return false;

	IwVector3d vectorUnit = vector;
	vectorUnit.Unitize();

	IwPoint3d projectedPnt = vectorPnt.ProjectPointToPlane( PlanePnt, PlaneNormal );
	IwVector3d towardPlaneVec = projectedPnt - vectorPnt;
	towardPlaneVec.Unitize();
	double distToPlane = vectorPnt.DistanceBetween(projectedPnt);
	double thetaRadian;
	towardPlaneVec.AngleBetween(vectorUnit, thetaRadian);
	double distToIntersection = distToPlane/cos(thetaRadian);

	intersectPnt = vectorPnt + distToIntersection*vectorUnit;
	intersected = true;

	return intersected;
}

bool IntersectPolylinesByLine
(
	IwTArray<IwPoint3d> polylines,	// I: define polylines
	IwTArray<IwPoint3d> line,		// I: two points define line
	IwVector3d* normalVector		// I: polylines and line plane normal
)
{
	IwTArray<IwPoint3d> coplanarPolylines;
	IwTArray<IwPoint3d> coplanarLine;
	if ( normalVector )
	{
		IwPoint3d lineHead = line.GetAt(0);
		IwPoint3d pnt;
		for (unsigned i=0; i<polylines.GetSize(); i++)
		{
			pnt = polylines.GetAt(i).ProjectPointToPlane(lineHead, *normalVector);
			coplanarPolylines.Add(pnt);
		}
		for (unsigned i=0; i<line.GetSize(); i++)
		{
			pnt = line.GetAt(i).ProjectPointToPlane(lineHead, *normalVector);
			coplanarLine.Add(pnt);
		}
	}
	else
	{
		coplanarPolylines.Append(polylines);
		coplanarLine.Append(line);
	}

	// determine intersection
	for (unsigned i=1; i<coplanarPolylines.GetSize(); i++)
	{
		// When 2 line intersect, the 2 end points of any line are on the opposite sides of the other line 
		IwVector3d tempVec = coplanarPolylines.GetAt(i) - coplanarPolylines.GetAt(i-1);
		IwVector3d tempVec0 = coplanarLine.GetAt(0) - coplanarPolylines.GetAt(i-1);
		IwVector3d tempVec1 = coplanarLine.GetAt(1) - coplanarPolylines.GetAt(i-1);
		IwVector3d crossVec0 = tempVec*tempVec0;
		IwVector3d crossVec1 = tempVec*tempVec1;

		IwVector3d tempVec_Other = coplanarLine.GetAt(1) - coplanarLine.GetAt(0);
		IwVector3d tempVec0_Other = coplanarPolylines.GetAt(i-1) - coplanarLine.GetAt(0);
		IwVector3d tempVec1_Other = coplanarPolylines.GetAt(i) - coplanarLine.GetAt(0);
		IwVector3d crossVec0_Other = tempVec_Other*tempVec0_Other;
		IwVector3d crossVec1_Other = tempVec_Other*tempVec1_Other;

		if ( crossVec0.Dot(crossVec1) < 0 && crossVec0_Other.Dot(crossVec1_Other) < 0)
		{
			return true;
		}
	}

	return false;
}

void ComputePointsOnCurve( IwCurve* pCurve, double t0, double t1, int np, IwTArray<IwPoint3d>& pts, bool bReset )
	{
	int						i;
	double					u, t;
	IwPoint3d				pt;
	IwExtent1d				extent;
	double					tmin, tmax, range;
	double					eps = 1e-5;

	if( bReset )
		pts.ReSet();

	extent = pCurve->GetNaturalInterval();
	tmin = extent.GetMin();
	tmax = extent.GetMax();
	range = tmax - tmin;

	for( i = 0; i < np; i++ )
		{
		u = 1.0 * i / ( np - 1 );

		t = ( 1.0 - u ) * t0 + u * t1;

		if( t < tmin - eps )
			t += range;

		if( t > tmax + eps )
			t -= range;

		EvalCurvePoint( pCurve, t, pt );

		pts.Add( pt );
		}
	}


void ComputePointsAndTangentsOnCurve( IwCurve* pCurve, double t0, double t1, int np, CPointArray& pts, CVectorArray& tns, bool bReset )
	{
	int						i;
	double					u, t;
	IwPoint3d				pt;
	IwVector3d				tn;
	IwExtent1d				extent;
	double					tmin, tmax, range;
	double					eps = 1e-5;

	if( bReset )
		{
		pts.ReSet();
		tns.ReSet();
		}

	extent = pCurve->GetNaturalInterval();
	tmin = extent.GetMin();
	tmax = extent.GetMax();
	range = tmax - tmin;

	for( i = 0; i < np; i++ )
		{
		u = 1.0 * i / ( np - 1 );

		t = ( 1.0 - u ) * t0 + u * t1;

		if( t < tmin - eps )
			t += range;

		if( t > tmax + eps )
			t -= range;

		EvalCurve( pCurve, t, pt, tn );

		pts.Add( pt );
		tns.Add( tn );
		}
	}


void ComputePointsOnCurve( IwCurve* pCurve, int np, IwTArray<IwPoint3d>& pts, bool bReset )
	{
	IwExtent1d				extent;
	double					t0, t1;

	extent = pCurve->GetNaturalInterval();
	t0 = extent.GetMin();
	t1 = extent.GetMax();

	ComputePointsOnCurve( pCurve, t0, t1, np, pts, bReset );
	}


void ComputePointsAndTangentsOnCurve( IwCurve* pCurve, int np, CPointArray& pts, CVectorArray& tns, bool bReset )
	{
	IwExtent1d				extent;
	double					t0, t1;

	extent = pCurve->GetNaturalInterval();
	t0 = extent.GetMin();
	t1 = extent.GetMax();

	ComputePointsAndTangentsOnCurve( pCurve, t0, t1, np, pts, tns, bReset );
	}


void ComputePointsOnCurve( IwEdge* pEdge, int np, IwTArray<IwPoint3d>& pts )
	{
	double					t0, t1;
	IwCurve*				pCurve;

	pCurve = pEdge->GetCurve();

	GetEdgeEndParams( pEdge, t0, t1 );	

	ComputePointsOnCurve( pCurve, t0, t1, np, pts, true );
	}


void ComputePointsAndTangentsOnCurve( IwEdge* pEdge, int np, CPointArray& pts, CVectorArray& tns )
	{
	double					t0, t1;
	IwCurve*				pCurve;

	pCurve = pEdge->GetCurve();

	GetEdgeEndParams( pEdge, t0, t1 );	

	ComputePointsAndTangentsOnCurve( pCurve, t0, t1, np, pts, tns, true );
	}




void EvalCurvePoint( IwCurve* pCurve, double t, IwPoint3d& pt )
	{
	pCurve->EvaluatePoint( t, pt );
	}


void EvalCurveTangent( IwCurve* pCurve, double t, IwVector3d& tn, bool bNormalize )
	{
	IwVector3d				vecs[2];

	pCurve->Evaluate( t, 1, true, vecs );

	tn = vecs[1];
		
	if( bNormalize )
		tn.Unitize();
	}


void EvalCurve( IwCurve* pCurve, double t, IwPoint3d& pt, IwVector3d& tn )
	{
	IwVector3d				vecs[2];

	pCurve->Evaluate( t, 1, true, vecs );

	pt = vecs[0];
	tn = vecs[1];
		
	tn.Unitize();
	}


IwVector3d EvalSurfacePoint( IwSurface* pSurf, double u, double v )
	{
	IwPoint3d				pt;
	double					uv[2];

	uv[0] = u;
	uv[1] = v;

	pSurf->EvaluatePoint( uv, pt );

	return pt;
	}


IwVector3d EvalSurfacePoint( IwSurface* pSurf, double uv[2] )
	{
	IwPoint3d				pt;

	pSurf->EvaluatePoint( uv, pt );

	return pt;
	}


IwVector3d EvalSurfaceNormal( IwSurface* pSurf, double uv[2] )
	{
	IwVector3d				nm;

	pSurf->EvaluateNormal( uv, true, true, nm );

	return nm;
	}


IwVector3d EvalSurfaceNormal( IwSurface* pSurf, double u, double v )
	{
	IwVector3d				nm;
	double					uv[2];

	uv[0] = u;
	uv[1] = v;

	pSurf->EvaluateNormal( uv, true, true, nm );

	return nm;
	}


void EvalSurface( IwSurface* pSurf, double uv[2], IwPoint3d& pt, IwVector3d& nm )
	{
	pSurf->EvaluatePoint( uv, pt );
	pSurf->EvaluateNormal( uv, true, true, nm );
	}


void EvalSurface( IwSurface* pSurf, double u, double v, IwPoint3d& pt, IwVector3d& nm )
	{
	double					uv[2];

	uv[0] = u;
	uv[1] = v;

	pSurf->EvaluatePoint( uv, pt );
	pSurf->EvaluateNormal( uv, true, true, nm );
	}


void EvalSurfacePrincipalCurvature( IwSurface* pSurf, double uv[2], double& crv1, double& crv2 )
	{
	double					rdGaussianCurvature;
	double					rdNormalCurvature;
	double					rdPrincipleCurvature1;
	double					rdPrincipleCurvature2;
	IwVector3d				rEFGOfFirstFundForm;
	IwVector3d				rLMNOfSecondFundForm;
	IwVector3d				rPrincipleCurvatureVector1;
	IwVector3d				rPrincipleCurvatureVector2;

	pSurf->EvaluateGeometric(	uv, true, true, 
								rdGaussianCurvature, 
								rdNormalCurvature,
								rdPrincipleCurvature1, 
								rdPrincipleCurvature2,
								rEFGOfFirstFundForm,
								rLMNOfSecondFundForm,
								rPrincipleCurvatureVector1,
								rPrincipleCurvatureVector2 );

	crv1 = rdPrincipleCurvature1;
	crv2 = rdPrincipleCurvature2;
	}


void EvalSurfacePrincipalCurvature( IwSurface* pSurf, double u, double v, double& crv1, double& crv2 )
	{
	double					uv[2];

	uv[0] = u;
	uv[1] = v;

	EvalSurfacePrincipalCurvature( pSurf, uv, crv1, crv2 );
	}


bool FilletBrep( IwBrep* pBrep, CEdgeArray& Edges, double rad )
	{

	return false;
	////IwContext&				iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	//IwTArray< int >				id1, id2, idE;
	//int						i, nEdges;
	//long					rc;

	//int ne = Edges.GetSize();

	//for( i = 0; i < ne; i++ )
	//	idE.Add( GetEdgeIndex( pBrep, Edges[i] ) );

	//double dApproximationTol = 1.0e-3;

	//pBrep->m_bEditingEnabled = true;

 //   IwCircularCrossSectionFSG		sFSG( true, 0.1 );  // Creates circular cross section fillet
 //   double							dStepBackFactor = 1.0;
 //   IwMakeSurfaceBlendSIH			sSIH( dStepBackFactor );
 //  
	//IwFilletExecutive*				pFilExec = new( *iwContext ) IwFilletExecutive( *iwContext, pBrep );
 //       
	//pFilExec->SetSelfIntersectionHandler( &sSIH );

	//IwObjDelete						sClean( pFilExec );

	//nEdges = Edges.GetSize();
	//
 //   for( i = 0; i < nEdges; i++ ) 
	//	{
	//	IwEdge*						pE = Edges[i];

	//	IwEdgeuse*					pEU = pE->GetPrimaryEdgeuse();

	//	if( pEU == NULL ) 
	//		continue;

	//	IwConstantRadiusFS*			pFS = new( *iwContext ) IwConstantRadiusFS(
	//									*iwContext,
	//									0.0001, // dApproximationTol,
	//									0.174,  // 30.0 * IW_PI / 180.0, 
	//									0.087,	// 2.0 * IW_PI / 180.0, 
	//									rad, 
	//									pEU );

	//	pFS->SetFilletSurfaceGenerator( &sFSG );

	//	pFilExec->LoadFilletSolver( pFS );
 //       }

	//// Make Fillets
 //   rc = pFilExec->CreateFilletCorners();

 //   rc = pFilExec->DoFilleting();

	//return rc == IW_SUCCESS;
	}


void HighlightEdges( CEdgeArray& Edges, bool bHighlight )
	{
	int						i, nEdges;
	IwEdge*					pEdge;

	nEdges = Edges.GetSize();

	for( i = 0; i < nEdges; i++ )
		{
		pEdge = Edges[i];

		pEdge->SetFlag( SEL_BIT, bHighlight );
		}
	}



IwBSplineCurve*	ApproximatePoints( const CPointArray& pts, double tol )
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		points.Add( pts[i] );

	IwBSplineCurve::ApproximatePoints( *iwContext, points, 3, NULL, NULL, false, &tol, pSpline );

	return pSpline;
	}


IwBSplineCurve*	InterpolatePoints( 
	const CPointArray&		pts, 
	const IwVector3d&		tn0, 
	const IwVector3d&		tn1 )
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		vectors;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		points.Add( pts[i] );

	vectors.Add( tn0 );
	vectors.Add( tn1 );

	IwBSplineCurve::CreateInterpolatingCurve( *iwContext, IW_CP_CHORDLENGTH, 3, 3, 
												points, vectors, NULL, true, pSpline );

	return pSpline;
	}


IwBSplineCurve*	InterpolatePoints( 
	const CPointArray&		pts, 
	const CVectorArray&		tns )
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		vectors;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		{
		points.Add( pts[i] );
		vectors.Add( tns[i] );
		}

	IwBSplineCurve::CreateInterpolatingCurve( *iwContext, IW_CP_CHORDLENGTH, 3, 3, 
												points, vectors, NULL, true, pSpline );

	return pSpline;
	}


IwBSplineCurve*	InterpolatePoints( 
	const CPointArray&		pts, 
	const CDoubleArray&		params )
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		points.Add( pts[i] );

	IwBSplineCurve::InterpolatePoints( *iwContext, points, &params, 3, NULL, NULL, false, IW_IT_CHORDLENGTH, pSpline );

	return pSpline;
	}


IwBSplineCurve*	InterpolatePoints( 
	const CPointArray&		pts, 
	const CDoubleArray&		params,
	IwVector3d&				tn0,
	IwVector3d&				tn1 )
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwBSplineCurve*			pSpline;
	IwVector3d				tan0, tan1;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		points.Add( pts[i] );

	tan0 = tn0;
	tan1 = tn1;

	IwBSplineCurve::InterpolatePoints( *iwContext, points, &params, 3, &tan0, &tan1, false, IW_IT_CHORDLENGTH, pSpline );

	return pSpline;
	}


IwBSplineCurve*	InterpolatePoints( const CPointArray& pts, const CVectorArray& tns, const CDoubleArray& knots )
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		vectors;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		{
		points.Add( pts[i] );
		vectors.Add( tns[i] );
		}

	IwBSplineCurve::CreateByInterpolateWithKnots ( *iwContext, 3, points, vectors, knots, pSpline );

	return pSpline;
	}


IwBSplineCurve*	MakeUniformSpline( 
	const CPointArray&		pts, 
	IwVector3d				tn0,				
	IwVector3d				tn1 )				
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		vectors;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		points.Add( pts[i] );

	vectors.Add( tn0 );
	vectors.Add( tn1 );

	IwBSplineCurve::CreateInterpolatingCurve( *iwContext, IW_CP_UNIFORM, 3, 3, 
												points, vectors, NULL, true, pSpline );

	return pSpline;
	}


IwBSplineCurve*	MakeUniformSpline( const CPointArray& pts ) 
	{
	int						i, np;
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		vectors;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		points.Add( pts[i] );

	IwBSplineCurve::CreateInterpolatingCurve( *iwContext, IW_CP_UNIFORM, 3, 3, 
												points, vectors, NULL, true, pSpline );

	return pSpline;
	}


IwBSplineCurve*	MakeHermiteUniformSpline( const CPointArray& pts ) 
	{
	int						i, j, k, np;
	double					h0, h1;
	IwVector3d				v0, v1, vec;
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		vectors;
	IwBSplineCurve*			pSpline;

	np = pts.GetSize();

	for( i = 0; i < np; i++ )
		{
		points.Add( pts[i] );

		j = i - 1;
		k = i + 1;

		if( j < 0 )
			j = np - 2;

		if( k == np )
			k = 1;

		if( i == 164 )
			{
			int a = 1;
			}

		v0 = ( pts[i] - pts[j] );
		v1 = ( pts[k] - pts[i] );

		h0 = v0.Length();
		h1 = v1.Length();

		vec = ( v0 * h1 + v1 * h0 ) / ( h0 + h1 );

		vectors.Add( vec );
		}

	IwBSplineCurve::CreateInterpolatingCurve( *iwContext, IW_CP_UNIFORM, 3, 3, 
												points, vectors, NULL, false, pSpline );

	return pSpline;
	}


IwBSplineSurface* MakeLoftSurface( 
	CSplineArray			Curves, 
	double					tol,
	IwBSplineCurve*			pRail1, 
	IwBSplineCurve*			pRail2, 
	CDoubleArray*			params,
	bool					bRemoveKnots )
	{
	IwBSplineSurface*		pLoftSurface = NULL;
	IwBSplineSurface*		pNewSurface = NULL;
	IwTArray< double >			ParametersOfCrossSections;
	IwBSplineSurface*		pDerivSurfs[2] = { NULL, NULL };
	long					rc;

	rc = IwBSplineSurface::CreateSkinnedSurface( *iwContext, Curves, false, IW_SP_V, tol,
												pRail1, pRail2, false, params,
												pDerivSurfs, pLoftSurface );
											
#if 0
	if( params )
		{
		IwBSplineSurface*		pNewSurf;
		IwTArray< IwPoint3d >		ptArray;
		IwPoint3d				pt;
		IwPoint2d				uv;
		int						nu, nv;
		IwExtent2d				domain;
		double					u, v;
		long					rc;

		domain = pLoftSurface->GetNaturalUVDomain();

		nu = 5;
		nv = 300;

		ptArray.ReSet();

		for( int i = 0; i < nu; i++ )
			{
			for( int j = 0; j < nv; j++ )
				{
				u = 1.0 * i / ( nu - 1 );
				v = 1.0 * j / ( nv - 1 );

				uv[0] = ( 1.0 - u ) * domain.GetUMin() + u * domain.GetUMax();
				uv[1] = ( 1.0 - v ) * domain.GetVMin() + v * domain.GetVMax();

				pLoftSurface->EvaluatePoint( uv, pt );

				ptArray.Add( pt );
				}
			}

		tol = 0.0001;
		rc = IwBSplineSurface::ApproximatePoints( iwContext, ptArray, nu, nv, 3, 3, &tol, pNewSurf );

		return pNewSurf;
		}
#endif

	if( bRemoveKnots )
		pLoftSurface->RemoveKnots( 0.001, true, true );

	return pLoftSurface;
	}


IwBrep* MakeLoftBrep( 
	CSplineArray			Curves, 
	double					tol,
	IwBSplineCurve*			pRail1, 
	IwBSplineCurve*			pRail2, 
	CDoubleArray*			params,
	bool					bRemoveKnots )
	{
	IwBrep*					pBrep;
	IwFace*					pNewFace;
	IwExtent2d				domain;
	IwBSplineSurface*		pLoftSurface = NULL;

	pLoftSurface = MakeLoftSurface( Curves, tol, pRail1, pRail2, params, bRemoveKnots );

	if( pLoftSurface == NULL )
		return NULL;

	pBrep = NewBrep( 0.0001 );

	domain = pLoftSurface->GetNaturalUVDomain();

	pBrep->CreateFaceFromSurface( pLoftSurface, domain, pNewFace );

	return pBrep;
	}


IwBSplineSurface* MakeGordonSurface( CSplineArray& uCurves, CSplineArray& vCurves )
	{
	long					rc;
	IwBSplineSurface*		pSurf;

	rc = IwBSplineSurface::CreateGordonSurface( *iwContext, uCurves, vCurves, pSurf );

	if( rc != IW_SUCCESS )
		return NULL;

	return pSurf;
	}



IwBrep* MakeGordonBrep( CSplineArray& uCurves, CSplineArray& vCurves )
	{
	IwBrep*					pBrep;
	IwFace*					pNewFace;
	IwExtent2d				domain;
	IwBSplineSurface*		pGordonSurface;

	pGordonSurface = MakeGordonSurface( uCurves, vCurves );

	if( pGordonSurface == NULL )
		return NULL;

	pBrep = NewBrep( 0.0001 );

	domain = pGordonSurface->GetNaturalUVDomain();

	pBrep->CreateFaceFromSurface( pGordonSurface, domain, pNewFace );

	return pBrep;
	}


IwBSplineSurface* MakeRuledSurface(IwBSplineCurve* pCurve0, IwBSplineCurve* pCurve1 )
	{
	//IwContext&				iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	long					rc;
	IwBSplineSurface*		pSurf;

	rc = IwBSplineSurface::CreateRuledSurfaceSafe( *iwContext, *pCurve0, *pCurve1, IW_SP_U, pSurf );

	if( rc != IW_SUCCESS )
		return NULL;

#if 1
	pSurf = ApproximateSurfaceByBSplineSurface( pSurf, 5, 1000 );
#endif

	return pSurf;
	}


IwBrep* MakeRuledBrep( IwBSplineCurve* pCurve0, IwBSplineCurve* pCurve1 )
	{
	IwBSplineSurface*		pSurf = NULL;
	IwFace*					pFace;
	IwBrep*					pBrep;

	pSurf = MakeRuledSurface( pCurve0, pCurve1 );

	if( pSurf == NULL )
		return NULL;

	pBrep = NewBrep( 0.0001 );

	pBrep->CreateFaceFromSurface( pSurf, pSurf->GetNaturalUVDomain(), pFace );

	return pBrep;
	}


IwBSplineSurface* ApproximateSurfaceByBSplineSurface( 
	IwBSplineSurface*		pSurf,
	int						nu,
	int						nv,
	bool					bDelete )
	{
	IwBSplineSurface*		pNewSurface;
	IwTArray< IwPoint3d >		ptArray;
	double					tol;
	IwPoint3d				pt;
	IwPoint2d				uv;

	ptArray.ReSet();

	for( int i = 0; i < nu; i++ )
		{
		for( int j = 0; j < nv; j++ )
			{
			uv[0] = 1.0 * i / ( nu - 1 );
			uv[1] = 1.0 * j / ( nv - 1 );

			pSurf->EvaluatePoint( uv, pt );

			ptArray.Add( pt );
			}
		}

	tol = 0.0001;
	IwBSplineSurface::ApproximatePoints( *iwContext, ptArray, nu, nv, 3, 3, &tol, pNewSurface );

	if( bDelete )
		delete pSurf;

	return pNewSurface;
	}

////////////////////////////////////////////////////////////////////
// This function does the boolean-cut the pBrep
////////////////////////////////////////////////////////////////////
IwBrep* CutBrepByPlane
( 
	IwBrep*						pBrep,		// I:
	const IwPoint3d&			pt,			// I:
	const IwVector3d&			nm,			// I: direction to be trimmed
	double						*refSize	// I:
)
{
	IwVector3d xVec(1,0,0);
	IwVector3d yVec(0,1,0);
	IwVector3d normal = nm;
	IwVector3d perpVector;

	if (normal.Length() < 0.0001) return NULL;
	normal.Unitize();

	double dot = normal.Dot(xVec);
	if ( fabs(dot) > 0.9 ) // very parallel to the x-axis
	{
		perpVector = yVec;
	}
	else
	{
		perpVector = xVec;
	}
	IwVector3d xAxis, yAxis;

	xAxis = normal*perpVector;
	yAxis = normal*xAxis;


	double size;;
	if (refSize == NULL)
	{
		IwExtent3d bBox;
		pBrep->CalculateBoundingBox(bBox);
		size = bBox.GetSize().Length();
	}
	else
	{
		size = fabs(*refSize);
	}

	IwExtent2d domain(-size, -size, size, size); 
	IwPlane* plane;
	IwAxis2Placement ori(pt, xAxis, yAxis);
	IwPlane::CreateCanonical(*iwContext, ori, plane);
	plane->TrimWithDomain(domain);
	IwBrep* cutBrep=NULL;
	ThickenPlane((IwBSplineSurface*)plane, size, cutBrep);
	if (cutBrep == NULL) return NULL;

	IwBrep* resultBrep = Boolean(IW_BO_DIFFERENCE, pBrep, cutBrep, 0.001, 2.5/180.0*IW_PI);

	return resultBrep;
}

//////////////////////////////////////////////////////////////////////
// The sequence in planarPolygonPoints matters.
// planarPolygonPoints should not define a self-intersection polygon.
IwBrep* CutBrepByPlanarPolygon
( 
	IwBrep*						pBrep,				// I:
	IwTArray<IwPoint3d>			planarPolygonPoints,// I: points define a closed polygon
	const IwVector3d&			nm,					// I: direction to extrude the polygon
	double						*refSize			// I: 
)
{
	if (planarPolygonPoints.GetSize()<3)
		return NULL;
	if (planarPolygonPoints.GetSize()==3 && planarPolygonPoints.GetAt(0)==planarPolygonPoints.GetLast())
		return NULL;

	double size;
	if (refSize == NULL)
	{
		IwExtent3d bBox;
		pBrep->CalculateBoundingBox(bBox);
		size = bBox.GetSize().Length();
	}
	else
	{
		size = fabs(*refSize);
	}

	// Create polygon
	IwTArray<IwCurve*> lines;
	IwLine* line;
	for (unsigned i=1; i<planarPolygonPoints.GetSize(); i++)
	{
		IwLine::CreateLineSegment(*iwContext, 3, planarPolygonPoints.GetAt(i-1), planarPolygonPoints.GetAt(i), line);
		lines.Add(line);
	}
	// If it is not a closed polygon, close it by the last and first points.
	if ( !(planarPolygonPoints.GetLast()==planarPolygonPoints.GetAt(0)) )
	{
		IwLine::CreateLineSegment(*iwContext, 3, planarPolygonPoints.GetLast(), planarPolygonPoints.GetAt(0), line);
		lines.Add(line);
	}

	// extrude the polygon
	IwBrep *polygonBrep = new (*iwContext) IwBrep();
	IwPrimitiveCreation pPC(polygonBrep->GetInfiniteRegion());
	pPC.CreateLinearSweep(lines, nm, size, 1, TRUE);// Notice "1.26"
	if (polygonBrep==NULL)
		return NULL;

	// Trim 
	IwBrep* resultBrep = Boolean_ByIteration(IW_BO_DIFFERENCE, pBrep, polygonBrep, 0.001, 2.5/180.0*IW_PI);

	return resultBrep;

}

IwPlane* MakePlaneSurf(
	const IwPoint3d&			pt,
	const IwVector3d&		vec0,
	const IwVector3d&		vec1,
	double					w,
	double					h )
	{
	//IwContext&				iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	IwAxis2Placement		csys;
	IwPlane*				pNewPlane = NULL;
	IwPoint2d				uvMin( -w/2, -h/2 );
	IwPoint2d				uvMax(  w/2,  h/2 );
	IwExtent2d				domain( uvMin, uvMax );
	IwVector3d				vx = vec0;
	IwVector3d				vy = vec1;

	vx.Unitize();
	vy.Unitize();

	csys.SetCanonical( pt, vx, vy );

	IwPlane::CreateCanonical( *iwContext, csys, pNewPlane );

	pNewPlane->AdjustSTEPUVDomain( domain );

	return pNewPlane;
	}


IwBrep* MakePlane( 
	const IwPoint3d&			pt,
	const IwVector3d&		vx,
	const IwVector3d&		vy,
	double					w,
	double					h )
	{
	IwBrep*					pPlaneBrep;
	IwPlane*				pPlane;
	IwFace*					pFace;

	pPlane = MakePlaneSurf( pt, vx, vy, w, h );

	pPlaneBrep = NewBrep( 0.0001 );
	
	pPlaneBrep->CreateFaceFromSurface( pPlane, pPlane->GetNaturalUVDomain(), pFace );

	return pPlaneBrep;
	}


bool WriteBrep( IwBrep* pBrep, const char* filename )
	{
	IwTArray< IwCurve*>		sCurves;
	IwTArray< IwSurface*>	sSurfaces;
	IwTArray<long>			sTree;
	IwTArray< IwBrep*>		sBreps;
	long					rc;

	sBreps.Add( pBrep );

	// IwFileType eType = IW_BINARY; // Change to save as ASCII in iTW v6 & PS v2
	IwFileType eType = IW_ASCII;

	rc = IwBrepData::WritePartToFile( filename, sCurves, sSurfaces, sTree, sBreps, eType);

	return rc == IW_SUCCESS;
	}


bool WriteBrep( IwBrep* pBrep, const QString& sFileName )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return WriteBrep( pBrep, filename );
	}


IwBrep* ReadBrep( const char* filename )
	{
	//IwContext&				iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	IwTArray< IwCurve*>		sCurves;
	IwTArray< IwSurface*>	sSurfaces;
	IwTArray<long>			sTree;
	IwTArray< IwBrep*>		sBreps;
	IwBrep*					pBrep = NULL;

	sBreps.Add( pBrep );

	if( !QFile::exists( filename ) )
		RenameIwbToIwp( filename );	

	IwFileType eType = IW_ASCII;

	IwStatus status = IwBrepData::ReadPartFromFile( *iwContext, filename, sCurves, sSurfaces, sTree, sBreps, eType );

	if (status==IW_SUCCESS && sBreps.GetSize()>0)
		pBrep = sBreps.GetAt(0);

	// All Brep files should be in ascii format, but old versions (before iTW v6 & PS v2) are still in binary.
	if ( pBrep == NULL )
	{
		IwFileType eType = IW_BINARY;

		IwBrepData::ReadPartFromFile( *iwContext, filename, sCurves, sSurfaces, sTree, sBreps, eType );

		pBrep = sBreps[0];

	}

	return pBrep;
	}


IwBrep* ReadBrep( const QString& sFileName )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return ReadBrep( filename );
	}


bool WriteSurfaces( CSurfArray& surfaces, const char* filename )
	{
	IwTArray< IwCurve*>		sCurves;
	IwTArray< IwSurface*>	sSurfaces;
	IwTArray<long>			sTree;
	IwTArray< IwBrep*>		sBreps;
	long					rc;

	sSurfaces.Append( surfaces );

	IwFileType eType = IW_BINARY;
	rc = IwBrepData::WritePartToFile( filename, sCurves, sSurfaces, sTree, sBreps, eType );

	return rc == IW_SUCCESS;
	}


bool WriteSurfaces( CSurfArray& surfaces, const QString& sFileName )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return WriteSurfaces( surfaces, filename );
	}


bool ReadSurfaces( const char* filename, CSurfArray& surfaces )
	{
	IwTArray< IwCurve*>		sCurves;
	IwTArray<long>			sTree;
	IwTArray< IwBrep*>		sBreps;
	long					rc;

	if( !QFile::exists( filename ) )
		RenameIwbToIwp( filename );	

	IwFileType eType = IW_BINARY;;
	rc = IwBrepData::ReadPartFromFile( *iwContext, filename, sCurves, surfaces, sTree, sBreps, eType );

	return rc == IW_SUCCESS;;
	}


bool ReadSurfaces( const QString& sFileName, CSurfArray& surfaces )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return ReadSurfaces( filename, surfaces );
	}

IwBrep* ReadIGES( const QString& sFileName )
{
	IwBrep* Brep=NULL;

	char					filename[2048];
	QStringToCharArray( sFileName, filename );

    IwTArray<IwBrep*>	IwBreps;
	CIgesTracker*		pTracker = nullptr;

   HwImportOptions Options;
   Options.make_all_surfaces_trimmed = true;
   Options.make_single_brep = TRUE;
   Options.group_trimmed_surfaces_into_brep = TRUE;
   Options.store_transforms_as_attributes = TRUE;
   //HwNullTracker sTracker;
   HwNullLogger logger;
   //HwUnitsAndScale2 sUnits;
   HwHeaderInfo sHeader;
   double dIgesTol = 0.0;

#if HW_VERSION_NUMBER >= 1917

#else if HW_VERSION_NUMBER > 1900
   Options.path_to_parasolid_schemas = FALSE; 
#endif
   sHeader.SetGlobalTolerance(1e-4);
   sHeader.SetGlobalUnits(HW_U_MILLIMETERS, 1.0);
   //IwStatus iwStatus = HwTSLibIgesRead( *iwContext, filename, Options, logger, sTracker, sHeader, dIgesTol, sUnits, nullptr, nullptr, nullptr, nullptr, &IwBreps, nullptr );
   IwStatus iwStatus = HwTSLibIgesRead(*iwContext, filename, logger, Options,/* sTracker,*/ sHeader/*, dIgesTol, sUnits*/, nullptr/*, nullptr*/, nullptr, nullptr, &IwBreps, nullptr);

	delete pTracker;

	if( iwStatus != IW_SUCCESS )
		return false;

	if ( IwBreps.GetSize() > 0 )
	{
		Brep = IwBreps[0];

		Brep->SewAndOrient();

		IwBoolean isSolid = Brep->IsManifoldSolid();
	}

	return Brep;

}


IwTArray<IwCurve *> ReadCurvesFromIges( const QString& sFileName )
{
	IwBrep* Brep=NULL;

	char					filename[2048];
	QStringToCharArray( sFileName, filename );

    IwTArray<IwBrep*>	IwBreps;
	IwTArray<IwCurve *> arrOfCurves;
	CIgesTracker*		pTracker = nullptr;

   HwImportOptions Options;
   Options.make_all_surfaces_trimmed = true;
   Options.make_single_brep = TRUE;
   Options.group_trimmed_surfaces_into_brep = TRUE;
   Options.store_transforms_as_attributes = TRUE;
   //HwNullTracker sTracker;
   HwNullLogger logger;
   //HwUnitsAndScale2 sUnits(HW_U_MILLIMETERS, 1.0);//HwUnitsAndScale2(HW_U_MILLIMETERS, 1.0)
   HwHeaderInfo sHeader;
   double dIgesTol = 0.0;

#if HW_VERSION_NUMBER >= 1917

#else if HW_VERSION_NUMBER > 1900
   Options.path_to_parasolid_schemas = FALSE; 
#endif

   sHeader.SetGlobalTolerance(1e-4);
   sHeader.SetGlobalUnits(HW_U_MILLIMETERS, 1.0);
   IwStatus iwStatus = HwTSLibIgesRead( *iwContext, filename, logger, Options, sHeader/*, dIgesTol, sUnits*/, nullptr/*,  nullptr*/, &arrOfCurves, nullptr, &IwBreps, nullptr );

	delete pTracker;

	if( iwStatus != IW_SUCCESS )
		return false;

	if ( IwBreps.GetSize() > 0 )
	{
		Brep = IwBreps[0];

		Brep->SewAndOrient();

		IwBoolean isSolid = Brep->IsManifoldSolid();
	}

	return arrOfCurves;

}


bool WriteCurves( CCurveArray& curves, const char* filename )
	{
	IwTArray< IwSurface*>	sSurfaces;
	IwTArray<long>			sTree;
	IwTArray< IwBrep*>		sBreps;
	long					rc;

	IwFileType eType = IW_BINARY;
	rc = IwBrepData::WritePartToFile( filename, curves, sSurfaces, sTree, sBreps, eType );

	return rc == IW_SUCCESS;
	}


bool WriteCurves( CCurveArray& curves, const QString& sFileName )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return WriteCurves( curves, filename );
	}


bool ReadCurves( const char* filename, CCurveArray& curves )
	{
	IwTArray< IwSurface*>	sSurfaces;
	IwTArray<long>			sTree;
	IwTArray< IwBrep*>		sBreps;
	long					rc;

	if( !QFile::exists( filename ) )
		RenameIwbToIwp( filename );

	IwFileType eType = IW_BINARY;
	rc = IwBrepData::ReadPartFromFile( *iwContext, filename, curves, sSurfaces, sTree, sBreps, eType );

	return rc == IW_SUCCESS;
	}


bool ReadCurves( const QString& sFileName, CCurveArray& curves )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return ReadCurves( filename, curves );
	}


bool WriteIwp( IwBrep* pBrep, CCurveArray& curves, CSurfArray& surfaces, const char* filename )
	{
	IwTArray< IwBrep*>		breps;
	long					rc;

	breps.Add( pBrep );

	IwFileType eType = IW_BINARY;
	if (0)
		eType = IW_ASCII;

	rc = IwBrepData::WritePartToFile( filename, curves, surfaces, NULL, breps, eType );

	return rc == IW_SUCCESS;
	}


bool WriteIwp( IwBrep* pBrep, CCurveArray& curves, CSurfArray& surfaces, const QString& sFileName )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return WriteIwp( pBrep, curves, surfaces, filename );
	}


bool ReadIwp( IwBrep** ppBrep, CCurveArray& curves, CSurfArray& surfaces, const char* filename )
	{
	IwTArray< IwBrep*>		breps;
	IwTArray< long >		trees;
	long					rc;

	if( !QFile::exists( filename ) )
		RenameIwbToIwp( filename );

	IwFileType eType = IW_BINARY;
	if(0)
		eType = IW_ASCII;

	rc = IwBrepData::ReadPartFromFile( *iwContext, filename, curves, surfaces, trees, breps, eType );

	if( rc != IW_SUCCESS )
		return false;

	if( ppBrep )
		*ppBrep = ( breps.GetSize() > 0 )?	breps[0] : NULL;

	return true;
	}


bool ReadIwp( IwBrep** ppBrep, CCurveArray& curves, CSurfArray& surfaces, const QString& sFileName )
	{
	char					filename[2048];

	QStringToCharArray( sFileName, filename );

	return ReadIwp( ppBrep, curves, surfaces, filename );
	}


void RenameIwbToIwp( const char* filename )
	{
	char*					ext;
	char					iwb_name[2048];

	strncpy( iwb_name, filename, 2048 );

	ext = strrchr( iwb_name, '.' );

	strcpy( ext, ".iwb" );

	if( QFile::exists( iwb_name ) )
		rename( iwb_name, filename );
	}


IwPoint3d ComputeArcCenter( 
	const IwPoint3d&			pt0,
	const IwPoint3d&			pt1,
	const IwPoint3d&			pt2 )
	{
	IwPoint3d				center;
	IwVector3d				x_vec, y_vec;       
	IwExtent1d				domain;  
	double					rad;
  
	iwgu_CircleFrom3Points( pt0, pt1, pt2, center, x_vec, y_vec, domain, rad, 3, false );

	return center;
	}

///////////////////////////////////////////////////////////////////////////////////////
//void WriteIges( QString filename, IwBrep* pBrepTrimmedSurf, IwBrep* pBrepSolid )
//{
//	char	filenameChar[2048];
//	QStringToCharArray( filename, filenameChar );
//
//	WriteIges(filenameChar,  pBrepTrimmedSurf, pBrepSolid );
//
//}
//
//void WriteIges( const char* filename, IwBrep* pBrepTrimmedSurf, IwBrep* pBrepSolid )
//	{
//	CBrepArray				BrepsTrimmedSurf, BrepsSolid;
//	HwNullLogger			logger;
//
//	BrepsTrimmedSurf.Add( pBrepTrimmedSurf );
//	BrepsSolid.Add( pBrepSolid );
//
//#if HW_VERSION_NUMBER > 1900
//	HwExportOptions			options;
//   HwNullTracker tracker;
//	HwHeaderInfo			header;
//   HwEntityCounts error_counter;
//	HwTSLibIgesWrite (filename, options, logger, tracker, header,
//                           1e-4,
//                           HwUnitsAndScale2(HW_U_MILLIMETERS, 1.0),
//                           nullptr,
//                           nullptr,
//                           nullptr,
//                           nullptr,
//                           &BrepsTrimmedSurf, &BrepsSolid,
//                           nullptr,
//                           nullptr,
//                           error_counter);
//#else
//	hwiges_WriteIges( filename, logger, NULL, NULL, NULL, &BrepsTrimmedSurf, &BrepsSolid );
//#endif
//	}

/////////////////////////////////////////////////////////////////////////////////////
void WriteIges(	
	QString const&          filename, 
	CPointArray*			pPointArray, 
	CCurveArray*			pCurveArray, 
	CSurfArray*				pSurfArray, 
	CBrepArray*				pBrepTrimmedSurfArray,
	CBrepArray*				pBrepSolidArray)
{
	char	filenameChar[2048];
	QStringToCharArray( filename, filenameChar );

	WriteIges(filenameChar, pPointArray, pCurveArray, pSurfArray, pBrepTrimmedSurfArray, pBrepSolidArray);
}

void WriteIges(	
	const char*				filename, 
	CPointArray*			pPointArray, 
	CCurveArray*			pCurveArray, 
	CSurfArray*				pSurfArray, 
	CBrepArray*				pBrepTrimmedSurfArray,
	CBrepArray*				pBrepSolidArray)
{
	HwNullLogger			logger;
	HwExportOptions			options;
	HwHeaderInfo			header;
	IwTArray< IwPoint3d >		points3d;
	IwPoint3d				point3d;
	int						i, np;

	options.output_analytic_curves = true;
#ifdef SM_VERSION_STRING
#if SM_VERSION_NUMBER >= 80617
	options.flatten_assemblies = true;
#endif
#endif

    //HwEntityCounts error_counter;
    //HwNullTracker tracker;

#if HW_VERSION_NUMBER >= 1917
	IwTArray<HwWrapObjectWithIwAttributes<IwPoint3d>*> points;
	if( pPointArray )
	{
		np = pPointArray->GetSize();
		for( i = 0; i < np; i++ )
		{
			point3d = pPointArray->GetAt(i);
			HwWrapObjectWithIwAttributes<IwPoint3d> *point = new (*iwContext) HwWrapObjectWithIwAttributes<IwPoint3d> (point3d);
			points.Add(point);
		}
	}

	HwTSLibIgesWrite (filename, options, logger, tracker, header,
                           1e-4,
                            HwUnitsAndScale2(HW_U_MILLIMETERS, 1.0),
                           &points,
                           nullptr, 
                           pCurveArray,
                           pSurfArray,
                           pBrepTrimmedSurfArray,
                           pBrepSolidArray,
                           nullptr,
                           nullptr,
                           error_counter); 

#else if HW_VERSION_NUMBER > 1900
	if( pPointArray )
	{
		np = pPointArray->GetSize();
		points3d.SetSize( np );
		for( i = 0; i < np; i++ )
			points3d[i] = (*pPointArray)[i];
	}

	header.SetGlobalTolerance(1e-4);
	header.SetGlobalUnits(HW_U_MILLIMETERS, 1.0);
	HwTSLibIgesWrite (filename, logger, options/*, tracker*/, header,
                           /*1e-4,
                            HwUnitsAndScale2(HW_U_MILLIMETERS, 1.0),*/
                           &points3d,
                          // nullptr, 
                           pCurveArray,
                           pSurfArray,
                           pBrepTrimmedSurfArray,
                           pBrepSolidArray/*,
                           nullptr,
                           nullptr,
                           error_counter*/); 
#endif

}


IwFace* GetBrepFace( IwBrep* pBrep, int iUserIndex2 )
	{
	int						i, nFaces;
	CFaceArray				Faces;

	pBrep->GetFaces( Faces );

	nFaces = Faces.GetSize();

	for( i = 0; i < nFaces; i++ )
		{
		if( Faces[i]->GetUserIndex2() == iUserIndex2 )
			return Faces[i];
		}

	return NULL;
	}


void CloseBrepHoles( IwBrep* pBrep )
	{
	CCurveArray				curves;

	CollectLaminaEdgeCurves( pBrep, curves );

	MakePlanarFace( pBrep, curves );

	pBrep->SewAndOrient( 5.0 * pBrep->GetTolerance() );
	}



bool IsConvexEdge( IwEdge* pEdge )
	{
	return GetEdgeConvexity( pEdge ) == EDGE_CONVEX;
	}


bool IsConcaveEdge( IwEdge* pEdge )
	{
	return GetEdgeConvexity( pEdge ) == EDGE_CONCAVE;
	}


CEdgeConvexity GetEdgeConvexity( IwEdge* pEdge )
	{
	IwTArray< double >		params;
	IwEdgeuse*				pEU0;
	IwEdgeuse*				pEU1;
	IwExtent1d				domain;
	IwPoint3d				pt;
	IwVector3d				bin, bin0, bin1, nm, nm0, nm1;
	double					t;
	int						i;
	IwVector3d				NM0, NM1;
	const int				np = 5;
	double					par[np];
	CEdgeConvexity			conv[np];

	par[0] = 0.34;
	par[1] = 0.78;
	par[2] = 0.55;
	par[3] = 0.12;
	par[4] = 0.93;

	pEU0 = pEdge->GetPrimaryEdgeuse();
	pEU1 = pEU0->GetRadial();

	for( i = 0; i < np; i++ ) 
		{
		domain = pEdge->GetInterval();

		t = domain.Evaluate( par[i] );

		pEU0->EvaluateBinormal( t, false, pt, bin0, NULL, &NM0 );
		pEU1->EvaluateBinormal( t, false, pt, bin1, NULL, &NM1 );

		nm0 = NM0;
		nm1 = NM1;

		bin = ( bin0 + bin1 ) / 2.0;

		if( bin.Length() < 0.01 )
			{
			conv[i] = EDGE_TANGENT;
			}
		else
			{
				bin.Unitize();

			nm = Normalize( ( nm0 + nm1 ) / 2.0 );

			conv[i] = ( nm.Dot(bin) > 0.0 )?	EDGE_CONCAVE : EDGE_CONVEX;
			}

		if( i > 0 && conv[i] != conv[0] )
			return EDGE_UNDEFINED;
		}

	return conv[0];
	}


bool SewBrep( IwBrep* pBrep, double tol )
	{
	ULONG					n1, n2;
	double					d1, d2;
	long					rc;

	rc = pBrep->SewFaces( 0.001, n1, n2, d1, d2 );

	return rc == IW_SUCCESS;
	}


IwBSplineCurve* JoinCurves( IwTArray<IwCurve*>& Curves, bool bDelete )
	{
	IwBSplineCurve*			pCurve;
	IwBSplineCurve*			pNextCurve;
	int						i, nCurves;

	nCurves = Curves.GetSize();

	if( nCurves == 0 )
		return NULL;

	pCurve = CopyCurve( (IwBSplineCurve*) Curves.GetAt(0) );

	if( bDelete )
		delete Curves.GetAt(0);

	for( i = 1; i < nCurves; i++ )
		{
			pNextCurve = CopyCurve( (IwBSplineCurve*) Curves.GetAt(i) );

		if( bDelete )
			delete Curves.GetAt(i);

		pCurve->JoinWith( 1, pNextCurve, 0 );
		}

	return pCurve;
	}

IwBSplineSurface* JoinSurfaces( 
		IwBSplineSurface*				Surface1,
		IwBSplineSurface*				Surface2)
{
	IwBSplineSurface*		resultSurface = NULL;
	//IwContext&				iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();

	// determine the parametric sequence of both surfaces
	// assume surface1 parametric	surface2 parametric (can not handle the other situations)
	// min ->U						 min ->U	or max    V
	//  |							  |					  |
	//  V   max						  V   max	   U <-	min
	//
	//			s2(c)
	//	s2(a)	s1		s2(b)
	//			s2(d)
	IwPoint3d minUminVPntSurf1, minUmaxVPntSurf1, maxUminVPntSurf1, maxUmaxVPntSurf1;
	IwPoint3d minUmidVPntSurf1, maxUmidVPntSurf1, midUminVPntSurf1, midUmaxVPntSurf1;
	IwVector2d minUVSurf1, maxUVSurf1;
	minUVSurf1 = Surface1->GetNaturalUVDomain().GetMin();
	maxUVSurf1 = Surface1->GetNaturalUVDomain().GetMax();
	Surface1->EvaluatePoint(minUVSurf1, minUminVPntSurf1);
	Surface1->EvaluatePoint(IwVector2d(minUVSurf1.x, maxUVSurf1.y), minUmaxVPntSurf1);
	Surface1->EvaluatePoint(IwVector2d(maxUVSurf1.x, minUVSurf1.y), maxUminVPntSurf1);
	Surface1->EvaluatePoint(maxUVSurf1, maxUmaxVPntSurf1);
	Surface1->EvaluatePoint(IwVector2d(minUVSurf1.x, 0.5*(minUVSurf1.y+maxUVSurf1.y)), minUmidVPntSurf1);
	Surface1->EvaluatePoint(IwVector2d(maxUVSurf1.x, 0.5*(minUVSurf1.y+maxUVSurf1.y)), maxUmidVPntSurf1);
	Surface1->EvaluatePoint(IwVector2d(0.5*(minUVSurf1.x+maxUVSurf1.x), minUVSurf1.y), midUminVPntSurf1);
	Surface1->EvaluatePoint(IwVector2d(0.5*(minUVSurf1.x+maxUVSurf1.x), maxUVSurf1.y), midUmaxVPntSurf1);

	IwPoint3d minUminVPntSurf2, minUmaxVPntSurf2, maxUminVPntSurf2, maxUmaxVPntSurf2;
	IwPoint3d minUmidVPntSurf2, maxUmidVPntSurf2, midUminVPntSurf2, midUmaxVPntSurf2;
	IwVector2d minUVSurf2, maxUVSurf2;
	minUVSurf2 = Surface2->GetNaturalUVDomain().GetMin();
	maxUVSurf2 = Surface2->GetNaturalUVDomain().GetMax();
	Surface2->EvaluatePoint(minUVSurf2, minUminVPntSurf2);
	Surface2->EvaluatePoint(IwVector2d(minUVSurf2.x, maxUVSurf2.y), minUmaxVPntSurf2);
	Surface2->EvaluatePoint(IwVector2d(maxUVSurf2.x, minUVSurf2.y), maxUminVPntSurf2);
	Surface2->EvaluatePoint(maxUVSurf2, maxUmaxVPntSurf2);
	Surface2->EvaluatePoint(IwVector2d(minUVSurf2.x, 0.5*(minUVSurf2.y+maxUVSurf2.y)), minUmidVPntSurf2);
	Surface2->EvaluatePoint(IwVector2d(maxUVSurf2.x, 0.5*(minUVSurf2.y+maxUVSurf2.y)), maxUmidVPntSurf2);
	Surface2->EvaluatePoint(IwVector2d(0.5*(minUVSurf2.x+maxUVSurf2.x), minUVSurf2.y), midUminVPntSurf2);
	Surface2->EvaluatePoint(IwVector2d(0.5*(minUVSurf2.x+maxUVSurf2.x), maxUVSurf2.y), midUmaxVPntSurf2);

	// The same parametric direction
	if ( maxUminVPntSurf1.DistanceBetween(minUminVPntSurf2) < 0.001 && 
		 maxUmaxVPntSurf1.DistanceBetween(minUmaxVPntSurf2) < 0.001 &&
		 maxUmidVPntSurf1.DistanceBetween(minUmidVPntSurf2) < 0.001 )
		Surface1->JoinSurface(*iwContext, Surface2, IW_SP_U, 0.001, resultSurface);
	else if ( minUmaxVPntSurf1.DistanceBetween(minUminVPntSurf2) < 0.001 && 
			  maxUmaxVPntSurf1.DistanceBetween(maxUminVPntSurf2) < 0.001 &&
			  midUmaxVPntSurf1.DistanceBetween(midUminVPntSurf2) < 0.001 )
		Surface1->JoinSurface(*iwContext, Surface2, IW_SP_V, 0.001, resultSurface);
	else if ( minUminVPntSurf1.DistanceBetween(maxUminVPntSurf2) < 0.001 && 
			  minUmaxVPntSurf1.DistanceBetween(maxUmaxVPntSurf2) < 0.001 &&
			  minUmidVPntSurf1.DistanceBetween(maxUmidVPntSurf2) < 0.001 )
		Surface2->JoinSurface(*iwContext, Surface1, IW_SP_U, 0.001, resultSurface);
	else if ( minUminVPntSurf1.DistanceBetween(minUmaxVPntSurf2) < 0.001 && 
			  maxUminVPntSurf1.DistanceBetween(maxUmaxVPntSurf2) < 0.001 &&
			  midUminVPntSurf1.DistanceBetween(midUmaxVPntSurf2) < 0.001 )
		Surface2->JoinSurface(*iwContext, Surface1, IW_SP_V, 0.001, resultSurface);
	else
	{
		Surface2->Reverse(IW_SP_U);
		Surface2->Reverse(IW_SP_V);
		minUVSurf2 = Surface2->GetNaturalUVDomain().GetMin();
		maxUVSurf2 = Surface2->GetNaturalUVDomain().GetMax();
		Surface2->EvaluatePoint(minUVSurf2, minUminVPntSurf2);
		Surface2->EvaluatePoint(IwVector2d(minUVSurf2.x, maxUVSurf2.y), minUmaxVPntSurf2);
		Surface2->EvaluatePoint(IwVector2d(maxUVSurf2.x, minUVSurf2.y), maxUminVPntSurf2);
		Surface2->EvaluatePoint(maxUVSurf2, maxUmaxVPntSurf2);
		Surface2->EvaluatePoint(IwVector2d(minUVSurf2.x, 0.5*(minUVSurf2.y+maxUVSurf2.y)), minUmidVPntSurf2);
		Surface2->EvaluatePoint(IwVector2d(maxUVSurf2.x, 0.5*(minUVSurf2.y+maxUVSurf2.y)), maxUmidVPntSurf2);
		Surface2->EvaluatePoint(IwVector2d(0.5*(minUVSurf2.x+maxUVSurf2.x), minUVSurf2.y), midUminVPntSurf2);
		Surface2->EvaluatePoint(IwVector2d(0.5*(minUVSurf2.x+maxUVSurf2.x), maxUVSurf2.y), midUmaxVPntSurf2);

		if ( maxUminVPntSurf1.DistanceBetween(minUminVPntSurf2) < 0.001 && 
			 maxUmaxVPntSurf1.DistanceBetween(minUmaxVPntSurf2) < 0.001 &&
			 maxUmidVPntSurf1.DistanceBetween(minUmidVPntSurf2) < 0.001 )
			Surface1->JoinSurface(*iwContext, Surface2, IW_SP_U, 0.001, resultSurface);
		else if ( minUmaxVPntSurf1.DistanceBetween(minUminVPntSurf2) < 0.001 && 
				  maxUmaxVPntSurf1.DistanceBetween(maxUminVPntSurf2) < 0.001 &&
				  midUmaxVPntSurf1.DistanceBetween(midUminVPntSurf2) < 0.001 )
			Surface1->JoinSurface(*iwContext, Surface2, IW_SP_V, 0.001, resultSurface);
		else if ( minUminVPntSurf1.DistanceBetween(maxUminVPntSurf2) < 0.001 && 
				  minUmaxVPntSurf1.DistanceBetween(maxUmaxVPntSurf2) < 0.001 &&
				  minUmidVPntSurf1.DistanceBetween(maxUmidVPntSurf2) < 0.001 )
			Surface2->JoinSurface(*iwContext, Surface1, IW_SP_U, 0.001, resultSurface);
		else if ( minUminVPntSurf1.DistanceBetween(minUmaxVPntSurf2) < 0.001 && 
				  maxUminVPntSurf1.DistanceBetween(maxUmaxVPntSurf2) < 0.001 &&
				  midUminVPntSurf1.DistanceBetween(midUmaxVPntSurf2) < 0.001 )
			Surface2->JoinSurface(*iwContext, Surface1, IW_SP_V, 0.001, resultSurface);
	}

	return  resultSurface;
}


IwFace*	MakeSimpleFaceFromCurves( IwBrep* pBrep, IwSurface* pSurface, const CCurveArray& Curves )
	{
	IwTArray< ULONG >					sCurveLoops;
    IwRegion*						pNewRegion;
    IwShell*						pNewShell;
    IwFace*							pNewFace;
	int								i, nCurves;
	IwTArray<IwOrientationType>		eOrientArray;
	IwTArray<IwPoint3d>				sLoopPoints;
	long							rc;

	nCurves = Curves.GetSize();

	// Orientations
	for( i = 0; i < nCurves; i++ )
		eOrientArray.Add( IW_OT_SAME );	

	// One contour consists of all curves
	sCurveLoops.Add( nCurves );

	pSurface->SetOwner( NULL );

	rc = pBrep->MakeFaceWithCurves(	pBrep->GetInfiniteRegion(),
									sCurveLoops, &Curves, NULL, eOrientArray, 
									sLoopPoints, pSurface,
									pSurface->GetNaturalUVDomain(), IW_OT_SAME, 
									pNewRegion, pNewShell, pNewFace ); 

	if( rc != IW_SUCCESS )
		return NULL;

	pNewFace->SetTolerance( 0.0001 );
	
	return pNewFace;
	}


double ApproxEdgeLength( IwEdge* pEdge )
	{
	IwTArray<IwPoint3d>		pts;
	double					t0, t1, d;
	IwExtent1d				extent;
	int						i, np;
	
	extent = pEdge->GetInterval();

	t0 = extent.GetMin();
	t1 = extent.GetMax();

	np = 10;
	d = 0.0;

	ComputePointsOnCurve( pEdge->GetCurve(), t0, t1, np, pts );

	for( i = 0; i < np - 1; i++ )
		{
		d += dist( pts[i], pts[i+1] );
		}

	return d;
	}


void OrientCurve( IwCurve* pCurve, CCurveEnd eStartEnd, const IwPoint3d& pt )
	{
	IwPoint3d				p0, p1;
	IwExtent1d				extent;

	GetCurveEndPoints( pCurve, p0, p1 );

	if( eStartEnd == END_POINT )
		Swap( p0, p1 );

	if( dist( p0, pt ) > dist( p1, pt ) )
		{
		extent = pCurve->GetNaturalInterval();
		pCurve->ReverseParameterization( extent, extent );
		}	
	}


void TrimCurve( IwCurve* pCurve, double t0, double t1 )
	{
	double					tmin, tmax;
	
	tmin = min( t0, t1 );
	tmax = max( t0, t1 );

	pCurve->Trim( IwExtent1d( tmin, tmax ) );
	}


void TrimCurve( IwCurve* pCurve, const IwPoint3d& pt0, const IwPoint3d& pt1 )
	{
	double					t0, t1;

	t0 = DropPointOnCurve( pCurve, pt0 );
	t1 = DropPointOnCurve( pCurve, pt1 );

	TrimCurve( pCurve, t0, t1 );
	}

double MarchAlongCurve( IwCurve* pCurve, double t0, double dt, double d_needed )
	{
	double					par, par0, par1, range, d, t;
	IwPoint3d				p0, p1;

	GetCurveEndParams( pCurve, par0, par1 );

	range = par1 - par0;

	t = t0;
	d = 0.0;

	EvalCurvePoint( pCurve, t, p0 );

	while( d < d_needed )
		{
		t += dt;

		par = t;

		if( par < par0 )
			par += range;

		if( par > par1 )
			par -= range;

		EvalCurvePoint( pCurve, par, p1 );

		d += dist( p0, p1 );

		p0 = p1;
		}

	return t;
	}


double ComputeCircleByThreePoints(
	const IwPoint3d&			p0,
	const IwPoint3d&			p1,
	const IwPoint3d&			p2,
	IwPoint3d&				cnt )
	{
	IwVector3d				axisX, axisY;
	IwExtent1d				extent;
	double					rad;

	iwgu_CircleFrom3Points( p0, p1, p2, cnt, axisX, axisY, extent, rad, 3, false );

	return rad;
	}


void InvertCurve( IwCurve* pCurve )
	{
	IwExtent1d		extent;
		
	extent = pCurve->GetNaturalInterval();
	pCurve->ReverseParameterization( extent, extent );
	}


void AddLogPanelText( const QString& sText )
	{
/*
	CTotalDoc::GetTotalDoc()->AddLogPanelText( sText );
*/
	}


void GetFaceBounds( IwFace* pFace, IwPoint3d& pt0, IwPoint3d& pt1 )
	{
	IwExtent3d				bbox;

	pFace->CalculateBoundingBox( bbox );

	pt0 = bbox.GetMin();
	pt1 = bbox.GetMax();
	}


int FindCurvePointsParallelToVector( 
	IwCurve*				pCurve, 
	double					t0,
	double					t1,
	const IwVector3d&		vec,
	CPointArray&			pts )
	{
	double					t_min = min( t0, t1 );
	double					t_max = max( t0, t1 );
	IwExtent1d				extent( t_min, t_max );
	IwSolutionArray			Sols;
	IwVector3d				V = vec;
	int						i, np;
	double					t, tol = 0.001;
	
	pCurve->GlobalPropertyAnalysis( extent, IW_CP_PARALLEL_TO_VECTOR, NULL, &V, tol, Sols );

	np = Sols.GetSize();

	pts.SetSize( np );

	for( i = 0; i < np; i++ )
		{
		t = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve, t, pts[i] );
		}

	return np;
	}


int FindCurvePointsPerpendicularToVector( 
	IwCurve*				pCurve, 
	double					t0,
	double					t1,
	const IwVector3d&		vec,
	CPointArray&			pts )
	{
	double					t_min = min( t0, t1 );
	double					t_max = max( t0, t1 );
	IwExtent1d				extent( t_min, t_max );
	IwSolutionArray			Sols;
	IwVector3d				V = vec;
	int						i, np;
	double					t, tol = 0.001;
	
	pCurve->GlobalPropertyAnalysis( extent, IW_CP_PERPENDICULAR_TO_VECTOR, NULL, &V, tol, Sols );

	np = Sols.GetSize();

	pts.SetSize( np );

	for( i = 0; i < np; i++ )
		{
		t = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve, t, pts[i] );
		}

	return np;
	}


bool IntersectCurves( 
	IwCurve*				pCurve0, 
	IwCurve*				pCurve1, 
	const IwPoint3d&			pt, 
	IwPoint3d&				pnt,
	double&					t0,
	double&					t1 )
	{
	IwStatus				rc;
	IwSolutionArray			Sols;
	double					tol = 1e-3;
	double					t, d, dmin;
	int						i, n;
	IwPoint3d				p;

	rc = pCurve0->GlobalCurveIntersect(	pCurve0->GetNaturalInterval(),
									*pCurve1, pCurve1->GetNaturalInterval(), 
									tol, Sols );

	if( rc != IW_SUCCESS )
		return false;
	
	n = Sols.GetSize();

	if( n == 0 )
		return false;

	dmin = HUGE_DOUBLE;

	for( i = 0; i < n; i++ )
		{
		t = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve0, t, p );

		d = dist( p, pt );

		if( dist( p, pt ) < dmin )
			{
			dmin = d;
			t0 = t;
			}
		}

	EvalCurvePoint( pCurve0, t0, pnt );

	t1 = DropPointOnCurve( pCurve1, pnt );

	return true;
	}


bool IntersectCurves( 
	IwCurve*				pCurve0, 
	IwCurve*				pCurve1, 
	IwExtent1d*				interval0,
	IwExtent1d*				interval1,
	const IwPoint3d&			pt, 
	IwPoint3d&				pnt,
	double&					t0,
	double&					t1 )
	{
	IwStatus				rc;
	IwSolutionArray			Sols;
	double					tol = 1e-3;
	double					t, d, dmin;
	int						i, n;
	IwPoint3d				p;

	IwExtent1d itval0 = pCurve0->GetNaturalInterval();
	if ( interval0 != NULL )
		itval0 = *interval0;
	IwExtent1d itval1 = pCurve1->GetNaturalInterval();
	if ( interval1 != NULL )
		itval1 = *interval1;

	rc = pCurve0->GlobalCurveIntersect(	itval0,
									*pCurve1, itval1, 
									tol, Sols );

	if( rc != IW_SUCCESS )
		return false;
	
	n = Sols.GetSize();

	if( n == 0 )
		return false;

	dmin = HUGE_DOUBLE;

	for( i = 0; i < n; i++ )
		{
		t = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve0, t, p );

		d = dist( p, pt );

		if( dist( p, pt ) < dmin )
			{
			dmin = d;
			t0 = t;
			}
		}

	EvalCurvePoint( pCurve0, t0, pnt );

	t1 = DropPointOnCurve( pCurve1, pnt );

	return true;
	}

IwBSplineSurface* GetFaceSurface( IwFace* pFace )
	{
	return (IwBSplineSurface*) pFace->GetSurface();
	}


IwBrep* MakeBrepFromSurface( double tol, IwSurface* pSurface )
	{
	IwBrep*					pBrep;
	IwFace*					pFace;

	pBrep = NewBrep( tol );

	pBrep->CreateFaceFromSurface( pSurface, pSurface->GetNaturalUVDomain(), pFace );

	return pBrep;
	}



double DistFromEdgeToSurface( IwEdge* pEdge, IwSurface* pSurf )
	{
	IwBSplineCurve*			pCurve;

	IwExtent1d interval = pEdge->GetInterval();
	pCurve = GetEdgeCurve( pEdge );

	return DistFromCurveToSurface( pCurve, &interval, pSurf );
	}

double DistFromCurveToEdge
( 
		IwCurve*					pCurve,		
		IwEdge*						pEdge,
		IwPoint3d*					curvePnt,	// O: optional output
		IwPoint3d*					edgePnt		// O: optional output
 )
{
	IwCurve*			pEdgeCurve = pEdge->GetCurve();
	IwExtent1d interval = pEdge->GetInterval();
	IwSolutionArray sols;
	IwSolution sol;
	pCurve->GlobalCurveSolve(pCurve->GetNaturalInterval(), *pEdgeCurve, pEdge->GetInterval(), IW_SO_MINIMIZE, 0.001, NULL, NULL, IW_SR_SINGLE, sols);
	if (sols.GetSize() == 0 )
		return -1;

	sol = sols.GetAt(0);

	double dist = sol.m_vStart.m_dSolutionValue;

	if ( curvePnt )
	{
		IwPoint3d pnt;
		pCurve->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt);
		*curvePnt = pnt;
	}
	if ( edgePnt )
	{
		IwPoint3d pnt;
		pEdgeCurve->EvaluatePoint(sol.m_vStart.m_adParameters[1], pnt);
		*edgePnt = pnt;
	}

	return dist;
}

double DistFromCurveToEdges
( 
		IwCurve*					pCurve,		
		IwTArray<IwEdge*>			pEdges,
		IwPoint3d*					curvePnt,
		IwPoint3d*					edgePnt
 )
{
	IwSolutionArray sols;
	IwSolution sol;
	double dist, minDistance = HUGE_DOUBLE;
	IwPoint3d minCurvePnt, minEdgePnt;
	for ( unsigned i=0; i<pEdges.GetSize(); i++ )
	{
		dist = DistFromCurveToEdge(pCurve, pEdges.GetAt(i), &minCurvePnt, &minEdgePnt);
		if ( dist < minDistance )
		{
			minDistance = dist;
			if ( curvePnt )
				*curvePnt = minCurvePnt;
			if ( edgePnt )
				*edgePnt = minEdgePnt;
		}
	}

	return minDistance;
}

double DistFromCurveToSurface
( 
	IwBSplineCurve* pCurve, 
	IwExtent1d* paramInterval, 
	IwSurface* pSurf 
)
{
	IwExtent1d interval;
	if ( paramInterval == NULL )
		interval = pCurve->GetNaturalInterval();
	else
		interval = *paramInterval;

	IwSolutionArray sols;
	IwSolution sol;
	pSurf->GlobalCurveSolve(pSurf->GetNaturalUVDomain(), *pCurve, interval, IW_SO_MINIMIZE, 0.01, NULL, NULL, IW_SR_SINGLE, sols);
	if (sols.GetSize() == 0)
		return -1;

	sol = sols.GetAt(0);
	
	return sol.m_vStart.m_dSolutionValue;

}

double DistFromCurveToSurface( IwBSplineCurve* pCurve, IwSurface* pSurf )
	{
	IwExtent1d interval = pCurve->GetNaturalInterval();

	return DistFromCurveToSurface( pCurve, &interval, pSurf );
	}

// global min distance with given interval
double DistFromCurveToSurface
(
	IwBSplineCurve*				pCurve, 
	IwExtent1d					searchParamInterval, 
	IwSurface*					pSurf,
	IwVector2d*					guessSParam,
	IwPoint3d*					closestPointOnCurve,
	IwPoint3d*					closestPointOnSurface
)
{
	double guessParam = searchParamInterval.GetMid();

	IwExtent1d crvInterval = pCurve->GetNaturalInterval();
	double lowValue = searchParamInterval.GetMin();
	if (lowValue < crvInterval.GetMin() )
		lowValue = crvInterval.GetMin();
	double highValue = searchParamInterval.GetMax();
	if (highValue > crvInterval.GetMax() )
		highValue = crvInterval.GetMax();

	IwExtent1d crvLocalInterval = IwExtent1d(lowValue, highValue);

	IwPoint3d pntOnCurve, cPnt;
	pCurve->EvaluatePoint(guessParam, pntOnCurve);
	IwVector2d surfParam;
	if ( guessSParam )
	{
		surfParam = *guessSParam;
	}
	else
	{
		DistFromPointToSurface(pntOnCurve, (IwBSplineSurface*)pSurf, cPnt, surfParam);
	}

	IwSolutionArray sols;
	IwSolution sol;
	pSurf->GlobalCurveSolve(pSurf->GetNaturalUVDomain(), *pCurve, crvLocalInterval, IW_SO_MINIMIZE, 0.001, 
		NULL, NULL, IW_SR_SINGLE, sols);
	if ( sols.GetSize() == 0 )
		return -1;
	
	sol = sols.GetAt(0);
	if ( closestPointOnCurve )
	{
		IwPoint3d pnt;
		pCurve->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt);
		*closestPointOnCurve = pnt;
	}

	if ( closestPointOnSurface )
	{
		IwPoint3d pnt;
		IwVector2d paramSurf = IwVector2d(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
		pSurf->EvaluatePoint(paramSurf, pnt);
		*closestPointOnSurface = pnt;
	}

	return sol.m_vStart.m_dSolutionValue;
}
/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The brep has better to be ShrinkGeometry()
double DistFromPointToBrep
(
	    IwPoint3d					point,			// I:
		IwBrep*						brep,			// I:
		IwPoint3d					&closestPoint	// O:
)
{
	IwSolutionArray sols;
	IwSolution sol;
	double minDist = 10000000;
	IwTopologySolver::BrepPointSolve(brep, point, IW_SO_MINIMIZE, IW_SR_ALL, 0.001, IW_BIG_DOUBLE, NULL, sols);
	if (sols.GetSize() > 0)
	{
		for (unsigned i=0; i<sols.GetSize(); i++)
		{
			sol = sols[i];
			if (sol.m_vStart.m_dSolutionValue < minDist)
			{
				IwObject* pObject = (IwObject*)sol.m_apObjects[0];
				if (pObject->IsKindOf(IwEdge_TYPE))
				{
					double edgeParam = sol.m_vStart.m_adParameters[0];
					IwEdge* myEdge = (IwEdge*)pObject;
					IwCurve* myCrv = myEdge->GetCurve();
					myCrv->EvaluatePoint(edgeParam, closestPoint);
				}
				else if (pObject->IsKindOf(IwFace_TYPE))
				{
					IwFace* myFace = (IwFace*)pObject;
					IwSurface* mySurf = myFace->GetSurface();
					IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
					mySurf->EvaluatePoint(faceUV, closestPoint);
				}
				else if (pObject->IsKindOf(IwVertex_TYPE))
				{
					IwVertex* vertex = (IwVertex*)pObject;
					closestPoint = vertex->GetPoint();
				}
				minDist = sol.m_vStart.m_dSolutionValue;
			}
		}
	}

	return minDist;
}


/////////////////////////////////////////////////////////
// This function determines the local minimum distance.
double DistFromPointToBrepLocal
(
	    IwPoint3d					point,			// I:
		IwBrep*						brep,			// I:
		IwPoint3d					initGuess,		// I: guessing point on brep
		IwPoint3d					&closestPoint	// O:
)
{
	closestPoint = initGuess;

	IwTArray<IwFace*> faces;
	brep->GetFaces(faces);
	IwSurface* initSurface = NULL;
	IwPoint3d initPoint;
	IwPoint2d initUV;
	double uv[2];

	// determine the start surface where the input init guess point is on.
	double minDist = 10000000;
	for (unsigned i=0; i<faces.GetSize(); i++)
	{
		IwSurface* surf = faces.GetAt(i)->GetSurface();
		IwPoint3d pointOnSurf = (IwPoint3d)DropPointOnSurface(surf, initGuess, uv);
		if ( pointOnSurf.DistanceBetween(initGuess) < minDist )
		{
			initSurface = surf;
			initPoint = pointOnSurf;
			initUV = IwPoint2d(uv[0], uv[1]);
			minDist = pointOnSurf.DistanceBetween(initGuess);
		}
	}

	// Determine the local minimum distance
	bool secondSearch = false;
	double bestDistSoFar = point.DistanceBetween(initPoint);
	IwBoolean foundAnswer = false;
	IwExtent2d surfDomain = initSurface->GetNaturalUVDomain();
	IwSolution sol;
	initSurface->LocalPointSolve( surfDomain, IW_SO_MINIMIZE, point, initUV, foundAnswer, sol);
	if ( foundAnswer )
	{
		if ( bestDistSoFar > sol.m_vStart.m_dSolutionValue )
		{
			bestDistSoFar = sol.m_vStart.m_dSolutionValue;
			IwPoint2d paramUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
			initSurface->EvaluatePoint(paramUV, closestPoint);
			// if the closestPoint is on the boundary, we need a second search
			if ( surfDomain.IsPoint2dOnBoundary(paramUV, 0.01) )
			{
				secondSearch = true;
			}
		}
	}

	// Second search
	if ( secondSearch )
	{
		IwSurface* secondSurface = NULL;
		IwPoint3d secondPoint;
		IwPoint2d secondUV;

		for (unsigned i=0; i<faces.GetSize(); i++)
		{
			IwSurface* surf = faces.GetAt(i)->GetSurface();
			if ( surf == initSurface ) 
				continue;
			IwPoint3d pointOnSurf = (IwPoint3d)DropPointOnSurface(surf, closestPoint, uv);
			if ( pointOnSurf.DistanceBetween(closestPoint) < 0.05 )
			{
				secondSurface = surf;
				secondPoint = pointOnSurf;
				secondUV = IwPoint2d(uv[0], uv[1]);
				break;
			}
		}

		if ( secondSurface )
		{
			secondSurface->LocalPointSolve( secondSurface->GetNaturalUVDomain(), IW_SO_MINIMIZE, point, secondUV, foundAnswer, sol);
			if ( foundAnswer )
			{
				if ( bestDistSoFar > sol.m_vStart.m_dSolutionValue )
				{
					bestDistSoFar = sol.m_vStart.m_dSolutionValue;
					IwPoint2d paramUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
					secondSurface->EvaluatePoint(paramUV, closestPoint);
				}
			}
		}

	}

	return bestDistSoFar;
}

/////////////////////////////////////////////////////////
// This function determines the global minimum distance.
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The brep has better to be ShrinkGeometry()
double DistFromPlaneToBrep
(
	    IwPoint3d					point,
		IwVector3d					normal,
		IwVector3d					*xAxis,
		IwExtent2d					*domain,
		IwBrep*						brep,
		IwPoint3d					&closestPoint
)
{
	//IwContext& iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	IwVector3d xRefAxis, yRefAxis, norm;
	if ( xAxis )
	{
		xRefAxis = *xAxis;
		yRefAxis = normal*xRefAxis;
	}
	else
	{
		normal.MakeUnitOrthoVectors(NULL, norm, xRefAxis, yRefAxis); 
	}
	IwPlane* plane;
	IwAxis2Placement planeAxes(point, xRefAxis, yRefAxis);
	IwPlane::CreateCanonical(*iwContext, planeAxes, plane);
	if ( domain )
	{
		plane->TrimWithDomain(*domain);
	}
	IwBrep* planeBrep = new (*iwContext) IwBrep();
	IwFace* face;
	planeBrep->CreateFaceFromSurface(plane, *domain, face);

	IwSolutionArray sols;
	IwSolution sol;
	double minDist = 10000000;
	IwTopologySolver::BrepBrepSolve(brep, planeBrep, IW_SO_MINIMIZE, IW_SR_ALL, 0.001, IW_BIG_DOUBLE, NULL, sols);
	if (sols.GetSize() > 0)
	{
		for (unsigned i=0; i<sols.GetSize(); i++)
		{
			sol = sols[i];
			if (sol.m_vStart.m_dSolutionValue < minDist)
			{
				IwObject* pObject = (IwObject*)sol.m_apObjects[0];
				if (pObject->IsKindOf(IwEdge_TYPE))
				{
					double edgeParam = sol.m_vStart.m_adParameters[0];
					IwEdge* myEdge = (IwEdge*)pObject;
					IwCurve* myCrv = myEdge->GetCurve();
					myCrv->EvaluatePoint(edgeParam, closestPoint);
				}
				else if (pObject->IsKindOf(IwFace_TYPE))
				{
					IwFace* myFace = (IwFace*)pObject;
					IwSurface* mySurf = myFace->GetSurface();
					IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
					mySurf->EvaluatePoint(faceUV, closestPoint);
				}
				else if (pObject->IsKindOf(IwVertex_TYPE))
				{
					IwVertex* vertex = (IwVertex*)pObject;
					closestPoint = vertex->GetPoint();
				}
				minDist = sol.m_vStart.m_dSolutionValue;
			}
		}
	}

	if ( planeBrep )
		IwObjDelete(planeBrep);

	return minDist;
}

double DistFromPointToSurface(
	    IwPoint3d					point,
		IwBSplineSurface*			surface,
		IwPoint3d					&closestPoint,
		IwPoint2d					&param)
{
	double distance = -1;

	IwSolutionArray sols;
	IwSolution sol;
	surface->GlobalPointSolve(surface->GetNaturalUVDomain(), IW_SO_MINIMIZE, point, 0.01, NULL, IW_SR_SINGLE, sols);
	if ( sols.GetSize() == 0 ) return distance;

	sol = sols.GetAt(0);

	param = IwPoint2d(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
	surface->EvaluatePoint(param, closestPoint);
	distance = sol.m_vStart.m_dSolutionValue;

	return distance;
}

double DistFromPointToSurfaces
(
	    IwPoint3d					point,
		IwTArray<IwSurface*>		surfaces,
		IwPoint3d					&closestPoint,
		IwPoint2d					&param,
		int							&surfaceIndex
)
{
	double dist, minDist = HUGE_DOUBLE;
	IwPoint3d cPnt;
	IwPoint2d paramUV;
	int index;

	for (unsigned i=0; i<surfaces.GetSize(); i++)
	{
		dist = DistFromPointToSurface(point, (IwBSplineSurface*)surfaces.GetAt(i), cPnt, paramUV);
		if ( dist < minDist )
		{
			minDist = dist;
			closestPoint = cPnt;
			param = paramUV;
			surfaceIndex = i;
		}
	}

	return minDist;
}

double DistFromPointToSurface(
	    IwPoint3d					point,
		IwBSplineSurface*			surface,
		IwPoint2d					guessParam,
		IwPoint3d					&closestPoint,
		IwPoint2d					&param)
{
	double distance = -1;

	IwBoolean found=FALSE;
	IwSolution sol;
	surface->LocalPointSolve(surface->GetNaturalUVDomain(), IW_SO_MINIMIZE, point, guessParam, found, sol);
	if ( !found ) return distance;

	param = IwPoint2d(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
	surface->EvaluatePoint(param, closestPoint);
	distance = sol.m_vStart.m_dSolutionValue;

	return distance;
}

double DistFromPointToEdge(
	    IwPoint3d					point,
		IwEdge*						edge,
		IwPoint3d					&closestPoint,
		IwVector3d*					optVector)
{
	double distance = -1;

	IwExtent1d interval = edge->GetInterval();
	IwCurve *crv = edge->GetCurve();

	IwSolutionArray sols;
	IwSolution sol;
	if ( optVector )
		crv->GlobalPointSolve(interval, IW_SO_3D_SIGNED_DIRECTED_MINIMIZE, point, 0.01, NULL, optVector, IW_SR_SINGLE, sols);
	else
		crv->GlobalPointSolve(interval, IW_SO_MINIMIZE, point, 0.01, NULL, optVector, IW_SR_SINGLE, sols);

	if ( sols.GetSize() == 0 ) return distance;

	sol = sols.GetAt(0);

	double param = sol.m_vStart.m_adParameters[0];
	crv->EvaluatePoint(param, closestPoint);
	distance = sol.m_vStart.m_dSolutionValue;

	return distance;
}

double DistFromPointToEdges(
	    IwPoint3d					point,
		IwTArray<IwEdge*>			edges,
		IwPoint3d					&closestPoint)
{
	double distance = -1;
	double minDist = 100000000;
	double dist;
	IwPoint3d pnt, minPnt;

	for (unsigned i=0; i<edges.GetSize(); i++)
	{
		dist = DistFromPointToEdge(point, edges.GetAt(i), pnt);
		if ( dist != -1 )
		{
			if ( dist < minDist )
			{
				minDist = dist;
				minPnt = pnt;
			}
		}
	}

	if ( minDist != 100000000 )
	{
		distance = minDist;
		closestPoint = minPnt;
	}

	return distance;
}

double DistFromPointToCurve
(
	    IwPoint3d					point,			// I:
		IwBSplineCurve*				curve,			// I:
		IwPoint3d					&closestPoint,	// O:
		double						&param,			// O:
		IwExtent1d*					searchInterval	// I:
)
{
	double distance = -1;

	IwExtent1d sInterval;
	if ( searchInterval )
		sInterval = *searchInterval;
	else
		sInterval = curve->GetNaturalInterval();

	IwSolutionArray sols;
	IwSolution sol;
	curve->GlobalPointSolve(sInterval, IW_SO_MINIMIZE, point, 0.01, NULL, NULL, IW_SR_SINGLE, sols);
	if ( sols.GetSize() == 0 ) return distance;

	sol = sols.GetAt(0);

	param = sol.m_vStart.m_adParameters[0];
	curve->EvaluatePoint(param, closestPoint);
	distance = sol.m_vStart.m_dSolutionValue;

	return distance;
}


double DistFromPointToCurve
(
	    IwPoint3d					point,			// I:
		IwBSplineCurve*				curve,			// I:
		double						guessParam,		// I:
		IwPoint3d					&closestPoint,	// O:
		double						&param			// O:
)
{
	double distance = -1;
	double distTol = 0.001, targetDist = 0.001;
	IwBoolean foundAnswer = FALSE;
	IwSolution sol;

	curve->LocalPointSolve(curve->GetNaturalInterval(), IW_SO_MINIMIZE, point, &distTol, &targetDist, NULL, guessParam, foundAnswer, sol);
	if ( !foundAnswer ) return distance;

	param = sol.m_vStart.m_adParameters[0];
	curve->EvaluatePoint(param, closestPoint);
	distance = sol.m_vStart.m_dSolutionValue;

	return distance;
}

double DistFromPointToCurves(
	    IwPoint3d					point,			// I:
		IwTArray<IwCurve*> const    &curves,		// I:
		IwPoint3d					&closestPoint,	// O:
		int							*crvIndex,		// O:
		double						*param			// O:
		)
{
	double dist, minDist = HUGE_DOUBLE;
	double t;
	IwPoint3d cPnt;
	IwBSplineCurve* crv;

	for (unsigned i=0; i<curves.GetSize(); i++)
	{
		crv = (IwBSplineCurve*)curves.GetAt(i);
		dist = DistFromPointToCurve(point, crv, cPnt, t);
		if ( dist != -1 )// got answer
		{
			if ( dist < minDist )
			{
				minDist = dist;
				closestPoint = cPnt;
				if (crvIndex)
					*crvIndex = i;
				if (param)
					*param = t;
			}
		}
	}
	return minDist;
}

double DistFromCurveToCurve(
	    IwBSplineCurve*				firstCurve,			// I:
		IwBSplineCurve*				secondCurve,		// I:
		IwPoint3d					&firstClosestPoint,	// O:
		double						&firstParam,		// O:
		IwPoint3d					&secondClosestPoint,// O:
		double						&secondParam,		// O:
		IwVector3d*					optVector			// I: optional, see IwCurve::GlobalCurveSolve() for details.
		)
{
	double distance = -1;

	IwSolutionArray sols;
	IwSolution sol;
	if ( optVector == NULL )
		firstCurve->GlobalCurveSolve(firstCurve->GetNaturalInterval(), *secondCurve, secondCurve->GetNaturalInterval(), IW_SO_MINIMIZE, 0.01, NULL, NULL, IW_SR_SINGLE, sols);
	else
		firstCurve->GlobalCurveSolve(firstCurve->GetNaturalInterval(), *secondCurve, secondCurve->GetNaturalInterval(), IW_SO_3D_SIGNED_DIRECTED_MINIMIZE, 0.01, NULL, optVector, IW_SR_SINGLE, sols);

	if ( sols.GetSize() == 0 ) return distance;

	sol = sols.GetAt(0);

	firstParam = sol.m_vStart.m_adParameters[0];
	firstCurve->EvaluatePoint(firstParam, firstClosestPoint);
	secondParam = sol.m_vStart.m_adParameters[1];
	secondCurve->EvaluatePoint(secondParam, secondClosestPoint);

	distance = sol.m_vStart.m_dSolutionValue;

	return distance;
}

bool DistFromCurveToCurve(
	    IwBSplineCurve*				firstCurve,			// I:
		IwBSplineCurve*				secondCurve,		// I:
		IwTArray<double>			&distances,			// O:
		IwTArray<IwPoint3d>			&firstClosestPoints,// O:
		IwTArray<double>			&firstParams,		// O:
		IwTArray<IwPoint3d>			&secondClosestPoints,// O:
		IwTArray<double>			&secondParams,		// O:
		double						*targetDist		// I:
		)
{
	distances.RemoveAll();
	firstClosestPoints.RemoveAll();
	firstParams.RemoveAll();
	secondClosestPoints.RemoveAll();
	secondParams.RemoveAll();

	IwSolutionArray sols;
	IwSolution sol;
	firstCurve->GlobalCurveSolve(firstCurve->GetNaturalInterval(), *secondCurve, secondCurve->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, targetDist, NULL, IW_SR_ALL, sols);
	if ( sols.GetSize() == 0 ) return false;

	double distance;
	double firstParam, secondParam;
	IwPoint3d firstClosestPoint, secondClosestPoint;

	for (unsigned i=0; i<sols.GetSize(); i++)
	{
		sol = sols.GetAt(i);

		distance = sol.m_vStart.m_dSolutionValue;
		firstParam = sol.m_vStart.m_adParameters[0];
		firstCurve->EvaluatePoint(firstParam, firstClosestPoint);
		secondParam = sol.m_vStart.m_adParameters[1];
		secondCurve->EvaluatePoint(secondParam, secondClosestPoint);

		distances.Add(distance);
		firstParams.Add(firstParam);
		firstClosestPoints.Add(firstClosestPoint);
		secondParams.Add(secondParam);
		secondClosestPoints.Add(secondClosestPoint);
	}

	return true;
}

double DistFromCurveToCurve
(
	IwBSplineCurve*				firstCurve,
	IwExtent1d&					firstGuessInterval,
	IwBSplineCurve*				secondCurve,
	IwExtent1d&					secondGuessInterval,
	IwPoint3d					&firstClosestPoint,	// O:
	double						&firstParam,		// O:
	IwPoint3d					&secondClosestPoint,// O:
	double						&secondParam		// O:
)
{
	double distance = -1;

	// first curve
	IwExtent1d crvInterval = firstCurve->GetNaturalInterval();
	double lowValue = firstGuessInterval.GetMin();
	if (lowValue < crvInterval.GetMin() )
		lowValue = crvInterval.GetMin();
	double highValue = firstGuessInterval.GetMax();
	if (highValue > crvInterval.GetMax() )
		highValue = crvInterval.GetMax();

	IwExtent1d firstCurveLocalInterval = IwExtent1d(lowValue, highValue);

	// second curve
	crvInterval = secondCurve->GetNaturalInterval();
	lowValue = secondGuessInterval.GetMin();
	if (lowValue < crvInterval.GetMin() )
		lowValue = crvInterval.GetMin();
	highValue = secondGuessInterval.GetMax();
	if (highValue > crvInterval.GetMax() )
		highValue = crvInterval.GetMax();

	IwExtent1d secondCurveLocalInterval = IwExtent1d(lowValue, highValue);

	// try GlobalCurveSolve()
	IwSolutionArray sols;
	firstCurve->GlobalCurveSolve(firstCurveLocalInterval, *secondCurve, secondCurveLocalInterval, IW_SO_MINIMIZE, 0.01, NULL, NULL, IW_SR_SINGLE, sols);
	if ( sols.GetSize() > 0 )
	{
		IwSolution sol = sols.GetAt(0);
		distance = sol.m_vStart.m_dSolutionValue;
		firstParam = sol.m_vStart.m_adParameters[0];
		firstCurve->EvaluatePoint(firstParam, firstClosestPoint);
		secondParam = sol.m_vStart.m_adParameters[1];
		secondCurve->EvaluatePoint(secondParam, secondClosestPoint);
	}

	return distance;
}

double DistFromCurveEndToCurveEnd
(
	IwBSplineCurve*				firstCurve,
	IwBSplineCurve*				secondCurve,
	IwPoint3d					&firstClosestPoint,	// O:
	IwPoint3d					&secondClosestPoint,// O:
	double						*firstParam,		// O:
	double						*secondParam		// O:
)
{
	double distance = -1;

	// first curve
	IwExtent1d crvInt1 = firstCurve->GetNaturalInterval();
	IwPoint3d sPnt1, ePnt1;
	firstCurve->EvaluatePoint(crvInt1.Evaluate(0), sPnt1);
	firstCurve->EvaluatePoint(crvInt1.Evaluate(1), ePnt1);
	// second curve
	IwExtent1d crvInt2 = secondCurve->GetNaturalInterval();
	IwPoint3d sPnt2, ePnt2;
	secondCurve->EvaluatePoint(crvInt2.Evaluate(0), sPnt2);
	secondCurve->EvaluatePoint(crvInt2.Evaluate(1), ePnt2);

	//
	double distSS, distSE, distES, distEE;
	distSS = sPnt1.DistanceBetween(sPnt2);
	distSE = sPnt1.DistanceBetween(ePnt2);
	distES = ePnt1.DistanceBetween(sPnt2);
	distEE = ePnt1.DistanceBetween(ePnt2);

	IwTArray<double> distances;
	distances.Add(distSS);
	distances.Add(distSE);
	distances.Add(distES);
	distances.Add(distEE);
	
	SortIwTArray(distances);

	// Now distances.GetAt(0) is the shortest distance
	if ( IS_EQ_TOL6(distances.GetAt(0),distSS) )
	{
		firstClosestPoint = sPnt1;
		secondClosestPoint = sPnt2;
		distance = distances.GetAt(0);
		if ( firstParam )
			*firstParam = crvInt1.Evaluate(0);
		if ( secondParam )
			*secondParam = crvInt2.Evaluate(0);
	}
	else if ( IS_EQ_TOL6(distances.GetAt(0),distSE) )
	{
		firstClosestPoint = sPnt1;
		secondClosestPoint = ePnt2;
		distance = distances.GetAt(0);
		if ( firstParam )
			*firstParam = crvInt1.Evaluate(0);
		if ( secondParam )
			*secondParam = crvInt2.Evaluate(1);
	}
	else if ( IS_EQ_TOL6(distances.GetAt(0),distES) )
	{
		firstClosestPoint = ePnt1;
		secondClosestPoint = sPnt2;
		distance = distances.GetAt(0);
		if ( firstParam )
			*firstParam = crvInt1.Evaluate(1);
		if ( secondParam )
			*secondParam = crvInt2.Evaluate(0);
	}
	else if ( IS_EQ_TOL6(distances.GetAt(0),distEE) )
	{
		firstClosestPoint = ePnt1;
		secondClosestPoint = ePnt2;
		distance = distances.GetAt(0);
		if ( firstParam )
			*firstParam = crvInt1.Evaluate(1);
		if ( secondParam )
			*secondParam = crvInt2.Evaluate(1);
	}

	return distance;
}

double DistFromFaceToFace
(
	IwFace* face0,		// I:
	IwFace* face1,		// I:
	IwPoint3d& pnt0,	// O: 
	IwPoint3d& pnt1,	// O:
	IwVector3d* optVector	// I: optional vector, see ShapeShapeSolve() for deatils.
)
{
	IwTArray<IwFace*> faces0, faces1;
	faces0.Add(face0);
	faces1.Add(face1);

	double distance = DistFromFacesToFaces(faces0, faces1, pnt0, pnt1);

	return distance;
}

//////////////////////////////////////////////////////////////////
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The Breps who own faces0 and faces1 have better to be ShrinkGeometry()
double DistFromFacesToFaces
(
	IwTArray<IwFace*> faces0,	// I:
	IwTArray<IwFace*> faces1,	// I:
	IwPoint3d& pnt0,			// O: 
	IwPoint3d& pnt1,			// O:
	IwVector3d* optVector,		// I: optional vector, see ShapeShapeSolve() for deatils.
	double* tol					// I:
)
{
	double distance = -1;
	double tolerance = 0.001;
	if ( tol )
		tolerance = *tol;

	// Create IwShape
	IwTArray<IwBrep*> sBreps0, sBreps1;
	IwTArray<IwFace*> sFaces0, sFaces1;
	IwTArray<IwEdge*> sEdges0, sEdges1;
	IwTArray<IwVertex*> sVertices0, sVertices1;

	sFaces0.Append(faces0);
	sFaces1.Append(faces1);

	IwShape shape0(sBreps0,sFaces0,sEdges0,sVertices0);
	IwShape shape1(sBreps1,sFaces1,sEdges1,sVertices1);

	IwSolutionArray sols;
	IwSolution sol;

	if ( optVector == NULL )
		IwTopologySolver::ShapeShapeSolve(&shape0, &shape1, IW_SO_MINIMIZE, IW_SR_SINGLE, tolerance, IW_BIG_DOUBLE, NULL, sols);
	else
		IwTopologySolver::ShapeShapeSolve(&shape0, &shape1, IW_SO_3D_SIGNED_DIRECTED_MINIMIZE, IW_SR_SINGLE, tolerance, IW_BIG_DOUBLE, optVector, sols);


	if ( sols.GetSize() > 0 )
	{
		sol = sols.GetAt(0);

		sol.GetPoint(0, pnt0);
		sol.GetPoint(1, pnt1);

		distance = sol.m_vStart.m_dSolutionValue;

		IwObject* pObject0 = (IwObject*)sol.m_apObjects[0];
		IwObject* pObject1 = (IwObject*)sol.m_apObjects[1];

		if (pObject0->IsKindOf(IwEdge_TYPE))
		{
			double edgeParam = sol.m_vStart.m_adParameters[0];
			IwEdge* myEdge = (IwEdge*)pObject0;
			IwCurve* myCrv = myEdge->GetCurve();
			myCrv->EvaluatePoint(edgeParam, pnt0);
		}
		else if (pObject0->IsKindOf(IwFace_TYPE))
		{
			IwVector2d faceUV(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
			IwFace* myFace = (IwFace*)pObject0;
			myFace->GetSurface()->EvaluatePoint(faceUV, pnt0);
		}
		else if (pObject0->IsKindOf(IwVertex_TYPE))
		{
			IwVertex* vertex = (IwVertex*)pObject0;
			pnt0 = vertex->GetPoint();
		}
		// pObject1
		if (pObject1->IsKindOf(IwEdge_TYPE))
		{
			double edgeParam;
			if (pObject0->IsKindOf(IwEdge_TYPE))// previous one
				edgeParam = sol.m_vStart.m_adParameters[1];
			else if (pObject0->IsKindOf(IwFace_TYPE))// previous one
				edgeParam = sol.m_vStart.m_adParameters[2];
			else if (pObject0->IsKindOf(IwVertex_TYPE)) // previous one
				edgeParam = sol.m_vStart.m_adParameters[0];
			IwEdge* myEdge = (IwEdge*)pObject1;
			IwCurve* myCrv = myEdge->GetCurve();
			myCrv->EvaluatePoint(edgeParam, pnt1);
		}
		else if (pObject1->IsKindOf(IwFace_TYPE))
		{
			IwVector2d faceUV;
			if (pObject0->IsKindOf(IwEdge_TYPE))// previous one
				faceUV = IwVector2d(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
			else if (pObject0->IsKindOf(IwFace_TYPE))// previous one
				faceUV = IwVector2d(sol.m_vStart.m_adParameters[2], sol.m_vStart.m_adParameters[3]);
			else if (pObject0->IsKindOf(IwVertex_TYPE)) // previous one
				faceUV = IwVector2d(sol.m_vStart.m_adParameters[0], sol.m_vStart.m_adParameters[1]);
			IwFace* myFace = (IwFace*)pObject1;
			myFace->GetSurface()->EvaluatePoint(faceUV, pnt1);
		}
		else if (pObject1->IsKindOf(IwVertex_TYPE))
		{
			IwVertex* vertex = (IwVertex*)pObject1;
			pnt1 = vertex->GetPoint();
		}
	}

	return distance;

}

//////////////////////////////////////////////////////////////////
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The Brep which owns faces has better to be ShrinkGeometry()
double DistFromFacesToCurve
(
	IwTArray<IwFace*> faces,	// I:
	IwCurve* curve,				// I:
	IwPoint3d& pnt0,			// O: 
	IwPoint3d& pnt1,			// O:
	IwVector3d* optVector		// I: optional vector, see ShapeCurveSolve() for deatils.
)
{
	double distance = -1;

	// Create IwShape
	IwTArray<IwBrep*> sBreps0;
	IwTArray<IwFace*> sFaces0;
	IwTArray<IwEdge*> sEdges0;
	IwTArray<IwVertex*> sVertices0;

	sFaces0.Append(faces);

	IwShape shape(sBreps0,sFaces0,sEdges0,sVertices0);

	IwSolutionArray sols;
	IwSolution sol;

	if ( optVector == NULL )
		IwTopologySolver::ShapeCurveSolve(&shape, *curve, curve->GetNaturalInterval(), IW_SO_MINIMIZE, IW_SR_SINGLE, 0.001, IW_BIG_DOUBLE, NULL, sols);
	else
		IwTopologySolver::ShapeCurveSolve(&shape, *curve, curve->GetNaturalInterval(), IW_SO_3D_SIGNED_DIRECTED_MINIMIZE, IW_SR_SINGLE, 0.001, IW_BIG_DOUBLE, optVector, sols);

	if ( sols.GetSize() > 0 )
	{
		sol = sols.GetAt(0);

		sol.GetPoint(0, pnt0);
		sol.GetPoint(1, pnt1);

		distance = sol.m_vStart.m_dSolutionValue;
	}

	return distance;
}

//////////////////////////////////////////////////////////////////
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The Brep which owns faces has better to be ShrinkGeometry()
double DistFromFaceToCurve
(
	IwFace* face,				// I:
	IwCurve* curve,				// I:
	IwPoint3d& pnt0,			// O: 
	IwPoint3d& pnt1,			// O:
	IwVector3d* optVector		// I: optional vector, see ShapeCurveSolve() for deatils.
)
{
	IwTArray<IwFace*> faces;
	faces.Add(face);
	double distance = DistFromFacesToCurve(faces, curve, pnt0, pnt1, optVector);

	return distance;
}

double DistFromPointToIwTArray(
	    IwPoint3d					point,			// I:
		IwTArray<IwPoint3d>			points,			// I:
		IwVector3d*					normal,			// I: 2d distance measured when viewing along this vector
		IwPoint3d					&closestPoint,	// O:
		int*						index)			// O:
{
	// search for the closest point
	double dist, minDist = HUGE_DOUBLE;
	IwPoint3d pnt, minPnt;
	int minIndex;
	IwVector3d tempVec;
	for ( unsigned i=0; i<points.GetSize(); i++ )
	{
		pnt = points.GetAt(i);
		tempVec = point - pnt;
		if ( normal )
			tempVec = tempVec.ProjectToPlane(*normal);
		dist = tempVec.Length();
		if ( dist < minDist )
		{
			minDist = dist;
			minPnt = pnt;
			minIndex = i;
		}
	}

	closestPoint = minPnt;
	if (index)
		*index = minIndex;

	return minDist;
}


double DistFromLineToIwTArray(
	    IwPoint3d					linePnt,		// I:
	    IwVector3d					lineVec,		// I:
		IwTArray<IwPoint3d>			points,			// I:
		IwVector3d*					normal,			// I: 2d distance measured when viewing along this vector
		IwPoint3d					&closestPoint,	// O:
		int*						index)			// O:
{
	// Line
	IwLine* theLine;
	IwLine::CreateCanonical(*iwContext, linePnt, lineVec, theLine);
	// search for the closest point
	double param, dist, minDist = HUGE_DOUBLE;
	IwPoint3d pnt, cPnt, minPnt;
	int minIndex;
	IwVector3d tempVec;
	for ( unsigned i=0; i<points.GetSize(); i++ )
	{
		pnt = points.GetAt(i);
		dist = DistFromPointToCurve(pnt, theLine, cPnt, param);
		if ( normal )
		{
			tempVec = cPnt - pnt;
			tempVec = tempVec.ProjectToPlane(*normal);
			dist = tempVec.Length();
		}
		if ( dist < minDist )
		{
			minDist = dist;
			minPnt = pnt;
			minIndex = i;
		}
	}

	closestPoint = minPnt;
	if (index)
		*index = minIndex;

	if (theLine)
		IwObjDelete(theLine);

	return minDist;
}

IwBSplineCurve*	ExtendCurve( 
	IwBSplineCurve*			pSpline, 
	int						iside,
	double					d )
	{
	IwBSplineCurve*			pCurve;
	IwBSplineCurve*			pNewCurve;
	int						i;

	iside++;

	pCurve = pSpline;

	for( i = 1; i < 3; i++ )
		{
		if( i ^ iside )
			{
			pCurve->CreateExtendedCurve( *iwContext, d, i, IW_CT_G1_G2, pNewCurve );
			pCurve = pNewCurve;
			}
		}

	return pCurve;
	}


void SetColorAttr( IwAObject* pObject, const CColor& color )
	{
	double					r = color[0] / 255.0;
	double					g = color[1] / 255.0;
	double					b = color[2] / 255.0;
	IwVector3d				c( r, g, b );

	IwVector3dAttribute*	pColorAttr = new ( pObject->GetContext() ) IwVector3dAttribute( IW_AI_COLOR, c );
    
	pObject->AddAttribute( pColorAttr );
	}



void SetLabelAttr( IwAObject* pObject, const QString& sLabel )
	{
	HwTSLibStringAndIntAttribute*	pLabelAttr = new ( pObject->GetContext() ) HwTSLibStringAndIntAttribute( IW_AIH_LABEL );
	char							label[1024];
	HwStdString						strLabel;

	QStringToCharArray( sLabel, label );

	strLabel = label;
    
	pLabelAttr->SetValue( strLabel, 42 );

	pObject->AddAttribute( pLabelAttr );
	}


void SetNameAttr( IwAObject* pObject, const QString& sName )
	{
	HwTSLibStringAttribute*			pNameAttr = new ( pObject->GetContext() ) HwTSLibStringAttribute( IW_AIH_NAME );
	char							name[1024];
	HwStdString						strName;

	QStringToCharArray( sName, name );

	strName = name;
    
	pNameAttr->SetValue( strName );

	pObject->AddAttribute( pNameAttr );
	}


void MarkFaceEdges( IwFace* pFace, int flag )
	{
	CEdgeArray				Edges;
	int						i, nEdges;

	pFace->GetEdges( Edges );

	nEdges = Edges.GetSize();

	for( i = 0; i < nEdges; i++ )
		{
		Edges[i]->SetUserIndex2( flag );
		}
	}


IwBSplineCurve* ApproximateCurve( 
	IwBSplineCurve*			pCurve, 
	double					tol, 
	bool					bDelete )
	{
	IwBSplineCurve*			pNewCurve;
	CDoubleArray			prms;
	double					tol2;

	prms.SetSize( 2 );
	GetCurveEndParams( pCurve, prms[0], prms[1] );

	pCurve->ApproximateCurve( *iwContext, IW_AA_HERMITE,  prms, tol, tol2, pNewCurve );

	if( bDelete )
		delete pCurve;

	return pNewCurve;
	}

IwBSplineCurve* ApproximateCurves( 
	    IwContext&					iwContext,
		IwTArray<IwBSplineCurve*>	curves, 
		double						tol)
{
	IwBSplineCurve* appCurve;
	double aTol;

	IwPoint3d sPnt0, ePnt0, sPnt1, ePnt1;
	IwBSplineCurve* crv = curves.GetAt(0);
	if ( curves.GetSize() == 1 )
	{
		IwCurve* aCurve;
		curves.GetAt(0)->Copy(iwContext, aCurve);
		crv = (IwBSplineCurve*)aCurve;
		return crv;
	}

	for (unsigned i=1; i<curves.GetSize(); i++)
	{
		crv->GetEnds(sPnt0, ePnt0);
		curves.GetAt(i)->GetEnds(sPnt1, ePnt1);
		if ( sPnt0.DistanceBetween(sPnt1) < tol )
			crv->JoinWith(0, curves.GetAt(i), 0);
		else if ( sPnt0.DistanceBetween(ePnt1) < tol )
			crv->JoinWith(0, curves.GetAt(i), 1);
		else if ( ePnt0.DistanceBetween(sPnt1) < tol )
			crv->JoinWith(1, curves.GetAt(i), 0);
		else if ( ePnt0.DistanceBetween(ePnt1) < tol )
			crv->JoinWith(1, curves.GetAt(i), 1);
	}

	IwExtent1d interval = crv->GetNaturalInterval();
	IwTArray<double> breakParams;
	breakParams.Add(interval.GetMin());
	breakParams.Add(interval.GetMax());
	crv->ApproximateCurve(iwContext, IW_AA_HERMITE, breakParams, tol, aTol, appCurve);

	return appCurve;
}

IwBSplineCurve* ProjectCurveOnPlane( 
	IwBSplineCurve*			pCurve, 
	const IwPoint3d&			pt,
	const IwVector3d&		nm,
	const IwVector3d&		vec,
	bool					bDelete )
	{
	IwCurve*				pNewCurve;

	pCurve->CreatePlaneProjection( *iwContext, IW_PT_PARALLEL, pt, nm, vec, pNewCurve );

	if( bDelete )
		delete pCurve;

	return (IwBSplineCurve*) pNewCurve;
	}


IwBSplineSurface* GetBrepSurface( IwBrep* pBrep )
	{
	CFaceArray				faces;
	IwBSplineSurface*		pSurface;

	pBrep->GetFaces( faces );
	
	pSurface = (IwBSplineSurface*) faces[0]->GetSurface();

	return pSurface;
	}


void CopyFaces( IwBrep* pBrepFrom, IwBrep* pBrepTo )
	{
	CFaceArray				faces;

	pBrepFrom->GetFaces( faces );

	pBrepFrom->CopyFaces( faces, pBrepTo );
	}


void SplitBrepFaces( 
	IwBrep*					pBrep, 
	CFaceArray&				Faces, 
	const IwPoint3d&			pt,
	const IwVector3d&		nm )
	{
	IwPlaneCutter			cutter( pt, nm );
	IwBrepCutting			cutting( pBrep );

	cutting.DoCut( &cutter, 0.0001, false, &Faces );
	}


void SplitBrepFace(
	IwBrep*					pBrep, 
	IwFace*					pFace, 
	const IwPoint3d&			pt,
	const IwVector3d&		nm )
	{
	CFaceArray				Faces;

	Faces.Add( pFace );

	SplitBrepFaces( pBrep, Faces, pt, nm );
	}


bool IntersectSurfaces(
	IwBSplineSurface*		pSurf1,				// I:
	IwBSplineSurface*		pSurf2,				// I:
	CCurveArray&			Curves,				// O:
	double					tol,				// I:
	double					angtol,				// I: radian
	IwTArray<IwCurve*>		*surface1UVCurves,	// O:
	IwTArray<IwCurve*>		*surface2UVCurves	// O:
	)
	{
	IwBoolean				bUseSurfaceEdges[2];
	IwExtent2d				domain1, domain2;
	long					rc;
	double					apporxTol(tol);

	bUseSurfaceEdges[0] = true;
	bUseSurfaceEdges[1] = true;

	domain1 = pSurf1->GetNaturalUVDomain();
	domain2 = pSurf2->GetNaturalUVDomain();

	rc = pSurf1->GlobalSurfaceIntersect(
				*iwContext,
				domain1, 
				*pSurf2,
				domain2,
				bUseSurfaceEdges,
				&apporxTol,
				&angtol,
				&Curves,
				surface1UVCurves, surface2UVCurves, 
				NULL, NULL );

	return ( rc == IW_SUCCESS );
	}

bool IntersectSurfaceByPlane
(
	IwBSplineSurface*		pSurf,				// I:
	IwPoint3d				pnt,				// I:
	IwVector3d				normal,				// I:
	CCurveArray&			Curves,				// O:
	double					tol,				// I::
	double					angtol,				// I: radian
	IwTArray<IwCurve*>		*surfaceUVCurves	// O:
)
{
	IwVector3d xAxis, yAxis, zAxis;
	normal.MakeUnitOrthoVectors(&normal, xAxis, yAxis, zAxis);
	IwExtent2d domain(-200, -200, 200, 200); 
	IwPlane* plane=NULL;
	IwAxis2Placement ori(pnt, yAxis, zAxis);
	IwPlane::CreateCanonical(*iwContext, ori, plane);
	plane->TrimWithDomain(domain);

	IwTArray<IwCurve*> planeUVCurves;

	bool bInt = IntersectSurfaces(pSurf, plane, Curves, tol, angtol, surfaceUVCurves, &planeUVCurves);

	if ( plane )
		IwObjDelete(plane);

	return bInt;
}

void ShowPoint( CDataDoc* pDoc, const IwPoint3d& pt, const CColor& color )
	{
	pDoc->AddTempPoint( pt, color );
	pDoc->GetView()->Redraw();
	}

void ShowCurve( CDataDoc* pDoc, IwCurve* pCurve, const CColor& color )
	{
	IwTArray<IwPoint3d>				pts;
	double					t0, t1;
	IwExtent1d				extent = pCurve->GetNaturalInterval();
	double length;

	pCurve->Length(extent, 0.01, length);
	int pntNo = (int)(2*length) + 2;

	t0 = extent.GetMin();
	t1 = extent.GetMax();

	ComputePointsOnCurve( pCurve, t0, t1, pntNo, pts );

	for (unsigned i=0; i<pts.GetSize(); i++)
		pDoc->AddTempPoint( pts.GetAt(i), color );
	
	pDoc->GetView()->Redraw();
	}


void ShowEdge( CDataDoc* pDoc, IwEdge* pEdge, const CColor& color )
	{
	IwBSplineCurve*			pCurve = (IwBSplineCurve*) pEdge->GetCurve();
	double					t0, t1;

	pCurve = CopyCurve( pCurve );

	GetEdgeEndParams( pEdge, t0, t1 );

	TrimCurve( pCurve, t0, t1 );

	ShowCurve( pDoc, pCurve, color );
	}


void ShowEdges( CDataDoc* pDoc, const CEdgeArray& edges, const CColor&				color )
	{
	int						i, nEdges;

	nEdges = edges.GetSize();

	for( i = 0; i < nEdges; i++ )
		{
		ShowEdge( pDoc, edges[i], color );
		}
	}

void ShowFace(
		CDataDoc*					pDoc,
		IwFace*						pFace,
		const CColor&				color)
{
	IwTArray<IwEdge*> edges;
	pFace->GetEdges(edges);

	ShowEdges(pDoc, edges, color);
}

void ShowFaces(
		CDataDoc*					pDoc,
		IwTArray<IwFace*>			pFaces,
		const CColor&				color)
{
	for (unsigned i=0; i<pFaces.GetSize(); i++)
		ShowFace(pDoc, pFaces.GetAt(i), color);
}

void ShowBrepEdge( 
	CDataDoc*				pDoc,
	IwBrep*					pBrep, 
	int						iEdge, 
	const CColor&			color )
	{
	CEdgeArray				Edges;

	pBrep->GetEdges( Edges );

	ShowEdge( pDoc, Edges[iEdge], color );
	}


CPart* ShowBrep( CDataDoc* pDoc, IwBrep* pBrep, const char* sPartName, const CColor& color )
	{
	CPart*					pPart = new CPart( pDoc, ENT_ROLE_NONE );

	pPart->SetIwBrep( pBrep );

	pPart->SetEntName( sPartName );
	
	pPart->SetColor( color );

	pPart->MakeTess();

	pDoc->AddPart( pPart );

	return pPart;
	}

void ShowSurface( CDataDoc*	pDoc, IwSurface* pSurf, const char* sName, const CColor& color )
	{
	CPart*					pPart = new CPart( pDoc, ENT_ROLE_NONE );

	// Make a copy of surface
	IwSurface* pSurfCopy;
	pSurf->Copy(pDoc->GetIwContext(), pSurfCopy);
	// Create a Brep
	IwFace* face;
	IwBrep* pBrep = new (pDoc->GetIwContext()) IwBrep();
	pBrep->CreateFaceFromSurface(pSurfCopy, pSurfCopy->GetNaturalUVDomain(), face);

	pPart->SetIwBrep( pBrep );

	pPart->SetEntName( sName );
	
	pPart->SetColor( color );

	pPart->MakeTess();

	pDoc->AddPart( pPart );

	return;
	}

/////////////////////////////////////////////////////////////
// This function does not display the Brep, but generates
// the display list only.
////////////////////////////////////////////////////////////
long GenerateIwBrepDisplayList
( 
	CDataDoc*			pDoc,				// I
	IwBrep*				pBrep,				// I:
	bool				bDisplayIsoCurves,	// I: 
	CColor				color,				// I:
	long&				faceDisplayList,	// O:
	long&				edgeDisplayList		// O:
)
{
	long isoCurveDisplayList = 0;
	faceDisplayList = 0;
	edgeDisplayList = 0;

	if ( pBrep != NULL )
	{
		CPart* pPart = new CPart(pDoc);
		pPart->SetIwBrep(pBrep);
		pPart->SetEntRole(ENT_ROLE_DISPLAY_ONLY);
		pPart->SetColor(color);
		if (bDisplayIsoCurves)
		{
			pPart->GenerateIsoCurvesOnlyDisplayList();
		}
		else
		{
			pPart->GenerateIsoCurvesOnlyDisplayList();
			pPart->MakeTess(false);
			pPart->Display();
		}
		faceDisplayList = pPart->GetFaceDisplayList();
		edgeDisplayList = pPart->GetEdgeDisplayList();
		isoCurveDisplayList = pPart->GetIsoCurveDisplayList();
		pPart->SetIwBrep(NULL);// By setting to NULL, the pBrep will not be deleted when delete pPart
		delete pPart;
	}
	return isoCurveDisplayList;
}


double ComputePointOnCurveAtDist(
	IwBSplineCurve*			pCurve,
	double					tStart,
	double					dist,
	IwPoint3d*				pt )
	{
	IwBoolean				bClampped;
	double					tol = 0.001;
	double					tEnd;
	IwExtent1d				extent;
	IwOrientationType		direction;

	extent = pCurve->GetNaturalInterval();

	direction = ( dist > 0 )?	IW_OT_SAME : IW_OT_OPPOSITE;

	dist = fabs( dist );

	pCurve->EuclidianStepOff( extent, tStart, direction, dist, tol, bClampped, tEnd );

	if( pt )
		EvalCurvePoint( pCurve, tEnd, *pt );

	return tEnd;
	}


void ComputeEquallySpacedPoints(
	IwBSplineCurve*			pCurve,
	double					t0,	
	double					t1,
	int						np,
	IwTArray<IwPoint3d>&	pts,
	IwTArray<double>*		params )
	{
	int						i;
	double					tol = 0.001;

	pCurve->EquallySpacedPoints( t0, t1, np - 1, tol, &pts, params );

	}


IwFace* GetEdgeOtherFace( IwEdge* pEdge, IwFace* pFace )
	{
	CFaceArray				Faces;
	int						nFaces;

	pEdge->GetFaces( Faces );

	nFaces = Faces.GetSize();

	if( nFaces < 2 )
		return NULL;

	return ( pFace == Faces[0] )?	Faces[1] : Faces[0];
	}

// This function only handle 4 sides of face
IwEdge* GetFaceOtherEdge( IwFace* pFace, IwEdge*pEdge )
{
	IwEdge* edge = NULL;
	IwTArray<IwEdge*> edges;
	pFace->GetEdges(edges);
	if (edges.GetSize() != 4) return NULL;

	for (unsigned i=0; i<edges.GetSize(); i++)
	{
		edge = edges.GetAt(i);
		if (pEdge == edge) 
		{
			continue;
		}
		
		if (pEdge->GetStartVertex() == edge->GetStartVertex() ||
			pEdge->GetStartVertex() == edge->GetOtherVertex(edge->GetStartVertex()) ||
			pEdge->GetOtherVertex(pEdge->GetStartVertex()) == edge->GetStartVertex() ||
			pEdge->GetOtherVertex(pEdge->GetStartVertex()) == edge->GetOtherVertex(edge->GetStartVertex())
			)
		{
			continue;
		}

		return edge;
	}

	return NULL;
}

// This function only handle 4 sides of face
// The posEdge and negEdge are connected to the pEdge
void GetFaceConnectedEdges( 
		IwFace*						pFace,		// I:
		IwEdge*						pEdge,		// I:
		IwEdge*&					posEdge,	// O:
		IwEdge*&					negEdge		// O:
		 )
{
	IwEdge* edge = NULL;
	IwTArray<IwEdge*> edges;
	pFace->GetEdges(edges);
	if (edges.GetSize() != 4) return;

	IwTArray<IwEdge*> connectedEdges;
	for (unsigned i=0; i<edges.GetSize(); i++)
	{
		edge = edges.GetAt(i);
		if (pEdge == edge) 
		{
			continue;
		}
		
		if (pEdge->GetStartVertex() == edge->GetStartVertex() ||
			pEdge->GetStartVertex() == edge->GetOtherVertex(edge->GetStartVertex()) ||
			pEdge->GetOtherVertex(pEdge->GetStartVertex()) == edge->GetStartVertex() ||
			pEdge->GetOtherVertex(pEdge->GetStartVertex()) == edge->GetOtherVertex(edge->GetStartVertex())
			)
		{
			connectedEdges.Add(edge);
		}
	}

	if (connectedEdges.GetSize() != 2) return;

	IwTArray<IwVertex*> vertexes0, vertexes1;
	IwPoint3d pnt0, pnt1;

	connectedEdges.GetAt(0)->GetVertices(vertexes0);
	connectedEdges.GetAt(1)->GetVertices(vertexes1);
	pnt0 = vertexes0.GetAt(0)->GetPoint() + vertexes0.GetAt(1)->GetPoint();
	pnt1 = vertexes1.GetAt(0)->GetPoint() + vertexes1.GetAt(1)->GetPoint();
	if (pnt0.y > pnt1.y)
	{
		posEdge = connectedEdges.GetAt(0);
		negEdge = connectedEdges.GetAt(1);
	}
	else
	{
		posEdge = connectedEdges.GetAt(1);
		negEdge = connectedEdges.GetAt(0);
	}

	return;
}

void TraceAdjacentFaces
(
		IwFace*						pFace,		// I:
		IwEdge*						pEdge,		// I:
		int							traceSteps,	// I:
		IwTArray<IwFace*>&			adjFaces	// O:
)
{
	IwFace* startFace = pFace;
	IwEdge* startEdge = pEdge;
	

	for (int i=0; i< traceSteps; i++)
	{
		IwFace* adjFace = GetEdgeOtherFace(startEdge, startFace);
		adjFaces.Add(adjFace);
		startFace = adjFace;
		startEdge = GetFaceOtherEdge(adjFace, startEdge);
	}

}

void GetFaceNeighbors( IwFace* pFace, CFaceArray& NeighborFaces )
	{
	CEdgeArray				Edges;
	int						i, nEdges;
	IwFace*					pNeighbor;

	pFace->GetEdges( Edges );

	nEdges = Edges.GetSize();

	for( i = 0; i < nEdges; i++ )
		{
		pNeighbor = GetEdgeOtherFace( Edges[i], pFace );

		NeighborFaces.Add( pNeighbor );
		}
	}


IwEdge* GetFacesCommonEdge( IwFace* pFace0, IwFace* pFace1 )
	{
	CEdgeArray				Edges, Edges0, Edges1;
	IwEdge*					pEdge = NULL;

	pFace0->GetEdges( Edges0 );
	pFace1->GetEdges( Edges1 );

	ArraysIntersection( Edges0, Edges1, Edges );

	if( Edges.GetSize() > 0 )
		pEdge = Edges[0];

	return pEdge;
	}


int FindCurveInflectionPoints( 
	IwCurve*				pCurve, 
	double					t0,
	double					t1,
	CDoubleArray&			params,
	CPointArray&			pts )
	{
	double					t_min = min( t0, t1 );
	double					t_max = max( t0, t1 );
	IwExtent1d				extent( t_min, t_max );
	IwSolutionArray			Sols;
	int						i, np;
	double					tol = 0.001;
	
	pCurve->GlobalPropertyAnalysis( extent, IW_CP_INFLECTION_POINTS, NULL, NULL, tol, Sols );

	np = Sols.GetSize();

	pts.SetSize( np );
	params.SetSize( np );

	for( i = 0; i < np; i++ )
		{
		params[i] = Sols[i].m_vStart.m_adParameters[0];

		EvalCurvePoint( pCurve, params[i], pts[i] );
		}

	return np;
	}


void ComputeBestCubicSeg(
	IwPoint3d&				pt0,
	IwPoint3d&				pt1,
	IwVector3d&				tn0,
	IwVector3d&				tn1 )
	{
	IwTArray< IwPoint3d >		points;
	IwTArray< IwVector3d >		tangents;
	long					rc;

	points.Add( pt0 );
	points.Add( pt1 );

	tangents.Add( Normalize( tn0 ) );
	tangents.Add( Normalize( tn1 ) );

	rc = IwBSplineCurve::FairBlendCurveDerivatives( points, tangents, NULL, true );

	if( rc != IW_SUCCESS )
		{
		int a = 1;
		}

	tn0 = tangents[0];
	tn1 = tangents[1];
	}

//////////////////////////////////////////////////////////////////
// Note - this function calls IwTopologySolver::ShapeShapeSolve().
// The pBrep has better to be ShrinkGeometry()
IwPoint3d DropPointOnBrep( IwBrep* pBrep, IwPoint3d& pt, double* uv, IwFace*& pFace )
	{
	IwStatus				rc;
	IwSolution				Sols[10];
	IwSolutionArray			Solutions( 10, Sols );
	double					tol = 0.001;
	double					best = 1000.0;
	int						nSols;
	IwPoint3d				pnt;

	rc = IwTopologySolver::BrepPointSolve( pBrep, pt, IW_SO_MINIMIZE, IW_SR_SINGLE, tol, best, NULL, Solutions );

	nSols = Solutions.GetSize();

	if( nSols == 0 )
		return pt;

	IwSolution&		rSol = Solutions[0];

	IwObject*		pObject = (IwObject*) rSol.m_apObjects[0]; 

	pFace = IW_CAST_PTR( IwFace, pObject );

	if( pFace == NULL )
		return pt;

	uv[0] = rSol.m_vStart[0];
	uv[1] = rSol.m_vStart[1];

	IwSurface*		pSur = pFace->GetSurface();
	
	pSur->EvaluatePoint( uv, pnt );

	return pnt;
	}


IwPoint3d DropPointOnBrep( IwBrep* pBrep, IwPoint3d& pt )
	{
	IwFace*					pFace;
	IwPoint3d				pnt;
	double					uv[2];

	pnt = DropPointOnBrep( pBrep, pt, uv, pFace );

	return pnt;
	}


void FindPointOnRayAtHeightAboveFemur(
	IwBrep*					pBrep,
	IwPoint3d				p0,
	IwVector3d				vec,
	double					height,
	IwPoint3d&				pt )
	{
	int						it;
	IwPoint3d				pnt;
	double					d;

	for( it = 0; it < 5; it++ )
		{
		pnt = DropPointOnBrep( pBrep, pt );
		d = dist( pt, pnt );

		pt += ( height - d ) * vec;
		}
	}

////////////////////////////////////////////////////////////
// Unfortunately, it only works when baseSurface is a plane.
bool ThickenPlane
(
		IwBSplineSurface*			basePlane,
		double						thickenDist,
		IwBrep*&					rBrep
)
{
	IwBrep* resultBrep = NULL;
	bool succeed = false;

	if ( !basePlane ) return succeed;

	//IwContext& iwContext = CTotalDoc::GetTotalDoc()->GetIwContext();
	IwTArray<IwSurface*> offsetSurfs;
	IwSurface* offPlane;

	basePlane->CreateOffsetSurface(*iwContext, thickenDist, 0.001, offsetSurfs);
	if (offsetSurfs.GetSize() != 1) return succeed;
	offPlane = (IwSurface*)offsetSurfs.GetAt(0);

	// Get the boundary curves to create side surfaces
	// Along U direction
	IwBSplineCurve *bMinUCurve, *bMaxUCurve;
	IwBSplineCurve *oMinUCurve, *oMaxUCurve;
	basePlane->CreateIsoBoundaries(*iwContext, IW_SP_U, 0.001, bMinUCurve, bMaxUCurve);
	offPlane->CreateIsoBoundaries(*iwContext, IW_SP_U, 0.001, oMinUCurve, oMaxUCurve);

	IwTArray<IwBSplineCurve*> minUCurves, maxUCurves;
	minUCurves.Add(bMinUCurve);
	minUCurves.Add(oMinUCurve);
	maxUCurves.Add(bMaxUCurve);
	maxUCurves.Add(oMaxUCurve);
	IwBSplineSurface *minUSurface=NULL, *maxUSurface=NULL;
	IwBSplineSurface::CreateSkinnedSurface(*iwContext, minUCurves, FALSE, IW_SP_U, 0.001, NULL, NULL, FALSE, NULL, NULL, minUSurface);
	IwBSplineSurface::CreateSkinnedSurface(*iwContext, maxUCurves, FALSE, IW_SP_U, 0.001, NULL, NULL, FALSE, NULL, NULL, maxUSurface);
	// Along V direction
	IwBSplineCurve *bMinVCurve, *bMaxVCurve;
	IwBSplineCurve *oMinVCurve, *oMaxVCurve;
	basePlane->CreateIsoBoundaries(*iwContext, IW_SP_V, 0.001, bMinVCurve, bMaxVCurve);
	offPlane->CreateIsoBoundaries(*iwContext, IW_SP_V, 0.001, oMinVCurve, oMaxVCurve);

	IwTArray<IwBSplineCurve*> minVCurves, maxVCurves;
	minVCurves.Add(bMinVCurve);
	minVCurves.Add(oMinVCurve);
	maxVCurves.Add(bMaxVCurve);
	maxVCurves.Add(oMaxVCurve);
	IwBSplineSurface *minVSurface=NULL, *maxVSurface=NULL;
	IwBSplineSurface::CreateSkinnedSurface(*iwContext, minVCurves, FALSE, IW_SP_U, 0.001, NULL, NULL, FALSE, NULL, NULL, minVSurface);
	IwBSplineSurface::CreateSkinnedSurface(*iwContext, maxVCurves, FALSE, IW_SP_U, 0.001, NULL, NULL, FALSE, NULL, NULL, maxVSurface);

	if ( minUSurface == NULL || maxUSurface == NULL || 
		 minVSurface == NULL || maxVSurface == NULL )
		 return succeed;

	IwFace* pFace;
	IwBrep* brep1 = new (*iwContext) IwBrep();
	brep1->CreateFaceFromSurface(basePlane, basePlane->GetNaturalUVDomain(), pFace);
	IwBrep* brep2 = new (*iwContext) IwBrep();
	brep2->CreateFaceFromSurface(offPlane, offPlane->GetNaturalUVDomain(), pFace);
	IwBrep* brep3 = new (*iwContext) IwBrep();
	brep3->CreateFaceFromSurface(minUSurface, minUSurface->GetNaturalUVDomain(), pFace);
	IwBrep* brep4 = new (*iwContext) IwBrep();
	brep4->CreateFaceFromSurface(maxUSurface, maxUSurface->GetNaturalUVDomain(), pFace);
	IwBrep* brep5 = new (*iwContext) IwBrep();
	brep5->CreateFaceFromSurface(minVSurface, minVSurface->GetNaturalUVDomain(), pFace);
	IwBrep* brep6 = new (*iwContext) IwBrep();
	brep6->CreateFaceFromSurface(maxVSurface, maxVSurface->GetNaturalUVDomain(), pFace);

	IwTArray<IwBrep*> breps;
	breps.Add(brep1);
	breps.Add(brep2);
	breps.Add(brep3);
	breps.Add(brep4);
	breps.Add(brep5);
	breps.Add(brep6);

	IwMerge::merge_breps(breps, 3, resultBrep);

	resultBrep->MakeManifold();

	rBrep = resultBrep;
	succeed = true;

	return succeed;
}

/////////////////////////////////////////////////////////////////////
// This function only works for a very smooth or almost flat surface.
bool ThickenSurface
(
		IwBSplineSurface*			baseSurface,
		double						thickenDist,
		IwBrep*&					rBrep
)
{
	bool succeed = false;

	if ( !baseSurface ) return succeed;

	IwSurface* surfCopy;
	baseSurface->Copy(*iwContext, surfCopy);
	IwBSplineSurface* baseSurfaceStartCap = (IwBSplineSurface*)surfCopy;
	baseSurface->Copy(*iwContext, surfCopy);
	IwBSplineSurface* baseSurfaceEndCap = (IwBSplineSurface*)surfCopy;

	// Determine normal, the direction to thicken
	IwExtent2d uvDom = baseSurfaceStartCap->GetNaturalUVDomain();
	IwVector3d normal;
	baseSurfaceStartCap->EvaluateNormal(uvDom.Evaluate(0.5, 0.5), FALSE, FALSE, normal);
	normal.Unitize();

	// Translate baseSurfaceStartCap into femur body
	IwAxis2Placement transM(-thickenDist*normal, IwVector3d(1,0,0), IwVector3d(0,1,0));
	baseSurfaceStartCap->Transform(transM);

	IwBrep *thickenBrep = new (*iwContext) IwBrep();
	IwFace* pFace;
	thickenBrep->CreateFaceFromSurface(baseSurfaceStartCap, uvDom, pFace); // Now thickenBrep has 1 face, start cap
	IwTArray<IwFace*> faces;
	faces.Add(pFace);
	IwPrimitiveCreation pPC(thickenBrep->GetInfiniteRegion());
	pPC.CreateLinearSweep(faces, normal, thickenDist, 1, FALSE);// Now thickenBrep has 1 + 4 faces, (4 side faces)
	// Add baseSurfaceEndCap
	thickenBrep->CreateFaceFromSurface(baseSurfaceEndCap, baseSurfaceEndCap->GetNaturalUVDomain(), pFace);// Now thickenBrep has 1 + 4 +1 faces.
	// Make a solid body
	thickenBrep->SewAndOrient();
	thickenBrep->MakeManifold();
	rBrep = thickenBrep;

	succeed = true;

	return succeed;
}

///////////////////////////////////////////////////////////////////
IwBSplineCurve* CreateArc
(
	IwContext&					iwContext,	// I:
	IwPoint3d&					startPoint, // I:
	IwVector3d&					startVector,// I:
	IwPoint3d&					endPoint,	// I:
	IwPoint3d&					arcCenter,	// O:
	double&						radius		// O:
)
{
	IwBSplineCurve* arc = NULL;

	// determine arc radius and arc center
	IwVector3d tempVec, tempVec2, sVec, towardArcCenter;
	double angle, chordLength;

	sVec = startVector;
	sVec.Unitize();
	tempVec = endPoint - startPoint;
	if (sVec.Dot(tempVec) < 0) // startVector should direction from startPoint toward endPoint
		sVec = -sVec;

	chordLength = tempVec.Length();
	tempVec.AngleBetween(sVec, angle);
	double R = 0.5*chordLength/cos(IW_PI/2.0-angle);

	// tempVec2 is perpendicular to both startVector and (endPoint - startPoint)
	tempVec2 =  sVec*tempVec;
	// toward arc center Vector 
	towardArcCenter = tempVec2*sVec;
	towardArcCenter.Unitize();

	IwPoint3d arcCtr = startPoint + R*towardArcCenter;
	// create an arc by the arc center, tip point, and passPoint
	IwBSplineCurve::CreateArcFromPoints(iwContext, 3, arcCtr, startPoint, endPoint, IW_CO_QUADRATIC, arc);

	arcCenter = arcCtr;

	radius = R;

	return arc;

}

///////////////////////////////////////////////////////////////////////
// startVector & upVector define the arc plane
IwBSplineCurve* CreateArc(
		IwContext&					iwContext,		// I:
		IwPoint3d&					startPoint,		// I:
		IwVector3d&					startVector,	// I:
		IwVector3d&					upVector,		// I: toward center 
		double						radius,			// I:
		double						angleDegrees	// I:
)
{
	IwBSplineCurve* arc = NULL;

	IwVector3d perpVec = startVector*upVector;
	IwVector3d towardCenterVec = perpVec*startVector;
	towardCenterVec.Unitize();

	IwPoint3d arcCenter = startPoint + radius*towardCenterVec;

	IwVector3d sVector = startVector;
	sVector.Unitize();

	IwAxis2Placement arcFrame(arcCenter, -towardCenterVec, startVector);

	IwBSplineCurve::CreateCircleSegment(iwContext, 3, arcFrame, radius, 0.0, angleDegrees, IW_CO_QUADRATIC, arc);

	return arc;
}

IwBSplineCurve* CreateArc3(
		IwContext&					iwContext,	// I:
		IwPoint3d&					startPoint, // I:
		IwPoint3d&					midPoint,	// I:
		IwPoint3d&					endPoint,	// I:
		IwPoint3d&					arcCenter,	// O:
		double&						radius		// O:
)
{
	IwBSplineCurve* arc = NULL;

	IwVector3d tempVec1 = midPoint - startPoint;
	IwVector3d tempVec2 = endPoint - midPoint;
	// if 3 points are linear, just return NULL.
	if ( tempVec1.IsParallelTo(tempVec2, 0.01) )
		return NULL;

	IwVector3d upVec = tempVec1*tempVec2;
	upVec.Unitize();

	IwVector3d prepVec1 = tempVec1*upVec;
	prepVec1.Unitize();

	IwVector3d prepVec2 = tempVec2*upVec;
	prepVec2.Unitize();

	IwPoint3d linePoint1 = 0.5*(startPoint+midPoint);
	IwPoint3d linePoint2 = 0.5*(endPoint+midPoint);

	IwLine *line1, *line2;
	IwLine::CreateCanonical(iwContext, linePoint1, prepVec1, line1);
	IwLine::CreateCanonical(iwContext, linePoint2, prepVec2, line2);

	IwSolutionArray sols;
	IwBoolean tryMore = TRUE;
	line1->IntersectWithLine(line1->GetNaturalInterval(), *line2, line2->GetNaturalInterval(), 0.1, tryMore, sols);

	if ( sols.GetSize() == 0 )
		return NULL;

	IwSolution sol = sols.GetAt(0);
	double param = sol.m_vStart.m_adParameters[0];

	line1->EvaluatePoint(param, arcCenter);
	radius = arcCenter.DistanceBetween(startPoint);

	IwBSplineCurve::CreateArcFromPoints(iwContext, 3, arcCenter, startPoint, endPoint, IW_CO_QUADRATIC, arc);

	if ( line1 ) IwObjDelete(line1);
	if ( line2 ) IwObjDelete(line2);

	return arc;
}

//////////////////////////////////////////////////////////////
// There are 2 possible arcs. Only the one whose center is
// closer to the plane point will be returned.
// The arc is always less 180 degrees.
IwBSplineCurve* CreateArcOnPlane(
		IwContext&					iwContext,	// I:
		IwPoint3d					startPoint, // I:
		IwPoint3d					endPoint,	// I:
		IwPoint3d					planePoint,	// I:
		IwVector3d					planeNormal,// I:
		double						radius,		// I:
		IwPoint3d&					centerPoint	// O:
		)
{
	// Ensure startPoint and endPoint are on the plane
	IwPoint3d sPoint = startPoint.ProjectPointToPlane(planePoint, planeNormal);
	IwPoint3d ePoint = endPoint.ProjectPointToPlane(planePoint, planeNormal);
	// Determine toward center vector
	IwVector3d tempVec0 = sPoint - ePoint;
	IwPoint3d midPoint = 0.5*(sPoint+ePoint);
	IwVector3d tempVec1 = planePoint - midPoint;
	IwVector3d towardCenter = tempVec0*planeNormal;
	towardCenter.Unitize();
	if ( tempVec1.Dot(towardCenter) < 0 )
		towardCenter = -towardCenter;
	// Determine the distance between midPoint to arc center
	double halfDistPP = 0.5*sPoint.DistanceBetween(ePoint);
	double heightSquare = radius*radius - halfDistPP*halfDistPP;
	if (heightSquare<0)
		return NULL;
	double height = sqrt(heightSquare);
	// arc center
	centerPoint = midPoint + height*towardCenter;
	// Create arc
	IwBSplineCurve* arc;
	IwBSplineCurve::CreateArcFromPoints(iwContext, 3, centerPoint, sPoint, ePoint, IW_CO_QUADRATIC, arc);

	return arc;
}

void SortIwTArray
(
	IwTArray<double>& iwArray,	// I/O:
	bool ascending				// I:
)
{
    double* pData = iwArray.GetDataArray();
    if (ascending)
        std::sort(pData, pData + iwArray.GetSize());
    else
        std::sort(pData, pData + iwArray.GetSize(), std::greater<double>());
}

struct PtLess
{
    int p;
    PtLess(int p) : p(p) {}
    bool operator()(IwPoint3d const& pt1, IwPoint3d const& pt2)
    {
        return pt1[p] < pt2[p];
    }
};

struct PtGreater
{
    int p;
    PtGreater(int p) : p(p) {}
    bool operator()(IwPoint3d const& pt1, IwPoint3d const& pt2)
    {
        return pt1[p] > pt2[p];
    }
};

void SortIwTArray
(
	IwTArray<IwPoint3d>& iwArray,	// I/O:
	int elementToSort,				// I: 0/1/2 element to sort
	bool ascending					// I:
)
{
    IwPoint3d* pData = iwArray.GetDataArray();
    if (ascending)
        std::sort(pData, pData + iwArray.GetSize(), PtLess(elementToSort));
    else
        std::sort(pData, pData + iwArray.GetSize(), PtGreater(elementToSort));
}

void EvenPointsSpace(IwTArray<IwPoint3d>& pointsToEven)
{
	if ( pointsToEven.GetSize() <=2 )
		return;
	//
	IwTArray<IwPoint3d> EvenSpacePoints;
	double totalLength=0;
	for (unsigned i=1; i<pointsToEven.GetSize(); i++)
		totalLength += pointsToEven.GetAt(i-1).DistanceBetween(pointsToEven.GetAt(i));
	double avgLength = totalLength / (pointsToEven.GetSize()-1);
	//
	EvenSpacePoints.Add(pointsToEven.GetAt(0));// Add first point
	//
	for (unsigned i=1; i<(pointsToEven.GetSize()-1); i++) // the points between
	{
		double expectedDistance = i*avgLength;
		double soFarDistance = 0;
		unsigned preIndex = 0, postIndex = 0;
		for (unsigned j=1; j<pointsToEven.GetSize(); j++)
		{
			soFarDistance += pointsToEven.GetAt(j-1).DistanceBetween(pointsToEven.GetAt(j));
			if ( soFarDistance >= expectedDistance )// first time soFarDistance >= expectedDistance
			{
				preIndex = j-1;
				postIndex = j;
				break;
			}
		}
		// determine the location should be, between preIndex and postIndex
		double remainingDistance = soFarDistance - expectedDistance;
		double currLength = pointsToEven.GetAt(preIndex).DistanceBetween(pointsToEven.GetAt(postIndex));
		double t = (currLength-remainingDistance)/currLength;
		IwVector3d vec = pointsToEven.GetAt(postIndex) - pointsToEven.GetAt(preIndex);
		vec.Unitize();
		IwPoint3d pnt = pointsToEven.GetAt(preIndex) + t*currLength*vec;
		EvenSpacePoints.Add(pnt);
	}
	//
	EvenSpacePoints.Add(pointsToEven.GetLast());// Add last point
	//
	pointsToEven.RemoveAll();
	pointsToEven.Append(EvenSpacePoints);

	return;
}

void ChainLoopConvexOffset
(
	IwContext& iwContext,					// I:
	IwTArray<IwBSplineCurve*> chainLoop,	// I:
	IwVector3d offsetNormal,				// I:
	double offsetDistance,					// I:
	IwTArray<IwBSplineCurve*>& offsetCurves	// O:
)
{
	double aTol;
	IwBSplineCurve* crv, *offsetCrv;
	for (unsigned i=0; i<chainLoop.GetSize(); i++)
	{
		crv = chainLoop.GetAt(i);
		crv->CreateSimpleOffset(iwContext, 0.01, offsetNormal, offsetDistance, offsetCrv, aTol);
		offsetCurves.Add(offsetCrv);
	}
	ChainLoopConvexCleanUp(offsetCurves);

}

//////////////////////////////////////////////////////////////////
// This function assumes the chain loop is pure convex. The curves
// restrictedly follow the order (start-end-start-end). The new 
// intersection points are close to the original ones. Only one 
// intersection point exists between two curves.
//////////////////////////////////////////////////////////////////
void ChainLoopConvexCleanUp
(
	IwTArray<IwBSplineCurve*>& chainLoop // I/O:
)
{

	// local data
	IwTArray<IwBSplineCurve*> localChainLoop;
	localChainLoop.Append(chainLoop);
	// determine direction
	bool forwardDir = true;
	IwBSplineCurve *crv, *nextCrv;
	IwPoint3d stPnt, edPnt;
	IwPoint3d stPntNext, edPntNext;

	crv = chainLoop.GetAt(0);
	crv->GetEnds(stPnt, edPnt);
	nextCrv = chainLoop.GetAt(0);
	nextCrv->GetEnds(stPntNext, edPntNext);
	if ( edPnt.DistanceBetween(stPntNext) < 0.1 )
		forwardDir = true;
	else
		forwardDir = false;

	// determine shortest curve, start from there
	int nSize = (int)localChainLoop.GetSize();
	int posIndex = 0;
	double length, minLength = 1000;
	for (int i=0; i<nSize; i++)
	{
		crv = localChainLoop.GetAt(i);
		crv->Length(crv->GetNaturalInterval(), 0.01, length);
		if ( length < minLength )
		{
			minLength = length;
			posIndex = i;
		}
	}
	// take the shortest curve out of array
	IwBSplineCurve* shortestCrv = localChainLoop.GetAt(posIndex);
	localChainLoop.RemoveAt(posIndex);
	// Now we trim the localChainloop
	IwPoint3d cPnt;
	bool bInt;
	double param, nextParam;
	IwExtent1d dom, nextDom;
	nSize = (int)localChainLoop.GetSize();
	for (int i=0; i<nSize; i++)
	{
		crv = localChainLoop.GetAt(i);
		nextCrv = localChainLoop.GetAt((i+1)%nSize);
		nextCrv->GetEnds(stPnt, edPnt);
		if ( forwardDir )
			bInt = IntersectCurves(crv, nextCrv, stPnt, cPnt, param, nextParam);
		else
			bInt = IntersectCurves(crv, nextCrv, edPnt, cPnt, param, nextParam);
		if ( bInt )
		{
			dom = crv->GetNaturalInterval();
			nextDom = nextCrv->GetNaturalInterval();
			if ( forwardDir )
				dom.SetMinMax(dom.GetMin(), param);
			else
				dom.SetMinMax(param, dom.GetMax());

			if ( forwardDir )
				nextDom.SetMinMax(nextParam, nextDom.GetMax());
			else
				nextDom.SetMinMax(nextDom.GetMin(), nextParam);
			crv->Trim(dom);
			nextCrv->Trim(nextDom);
		}
	}
	// Now we trim the localChainLoop with the shortest curve
	bool bIntShort0 = false;
	crv = localChainLoop.GetAt((posIndex-1+nSize)%nSize);
	shortestCrv->GetEnds(stPnt, edPnt);
	if ( forwardDir )
		bIntShort0 = IntersectCurves(crv, shortestCrv, stPnt, cPnt, param, nextParam);
	else
		bIntShort0 = IntersectCurves(crv, shortestCrv, edPnt, cPnt, param, nextParam);
	if ( bIntShort0 )
	{
		dom = crv->GetNaturalInterval();
		nextDom = shortestCrv->GetNaturalInterval();
		if ( forwardDir )
			dom.SetMinMax(dom.GetMin(), param);
		else
			dom.SetMinMax(param, dom.GetMax());

		if ( forwardDir )
			nextDom.SetMinMax(nextParam, nextDom.GetMax());
		else
			nextDom.SetMinMax(nextDom.GetMin(), nextParam);
		crv->Trim(dom);
		shortestCrv->Trim(nextDom);
	}
	// Now we trim the localChainLoop with the shortest curve
	bool bIntShort1 = false;
	nextCrv = localChainLoop.GetAt((posIndex+nSize)%nSize);
	nextCrv->GetEnds(stPnt, edPnt);
	if ( forwardDir )
		bIntShort1 = IntersectCurves(shortestCrv, nextCrv, stPnt, cPnt, param, nextParam);
	else
		bIntShort1 = IntersectCurves(shortestCrv, nextCrv, edPnt, cPnt, param, nextParam);
	if ( bIntShort1 )
	{
		dom = shortestCrv->GetNaturalInterval();
		nextDom = nextCrv->GetNaturalInterval();
		if ( forwardDir )
			dom.SetMinMax(dom.GetMin(), param);
		else
			dom.SetMinMax(param, dom.GetMax());

		if ( fabs(nextParam-nextDom.GetMin()) < fabs(nextParam-nextDom.GetMax()) )
			nextDom.SetMinMax(nextParam, nextDom.GetMax());
		else
			nextDom.SetMinMax(nextDom.GetMin(), nextParam);
		shortestCrv->Trim(dom);
		nextCrv->Trim(nextDom);
	}

	if ( bIntShort0 && bIntShort1 )
	{
		localChainLoop.InsertAt(posIndex, shortestCrv);
	}

	chainLoop.RemoveAll();
	chainLoop.Append(localChainLoop);

}

bool IsPointInsideTriangle(IwPoint3d point, IwPoint3d t0, IwPoint3d t1, IwPoint3d t2)
{
	// First test
	IwVector3d tempVec01 = t1 - t0; 
	IwVector3d tempVec02 = t2 - t0; 
	IwVector3d tempVec0p = point - t0; 

	IwVector3d prod12 = tempVec01*tempVec02;
	IwVector3d prod1p = tempVec01*tempVec0p;
	
	bool inside0 = prod12.Dot(prod1p) > 0 ? true : false;

	// second test
	IwVector3d tempVec10 = t0 - t1; 
	IwVector3d tempVec12 = t2 - t1; 
	IwVector3d tempVec1p = point - t1; 

	IwVector3d prod20 = tempVec12*tempVec10;
	IwVector3d prod2p = tempVec12*tempVec1p;
	
	bool inside1 = prod20.Dot(prod2p) > 0 ? true : false;

	// third test
	IwVector3d tempVec20 = t0 - t2; 
	IwVector3d tempVec21 = t1 - t2; 
	IwVector3d tempVec2p = point - t2; 

	IwVector3d prod01 = tempVec20*tempVec21;
	IwVector3d prod0p = tempVec20*tempVec2p;
	
	bool inside2 = prod01.Dot(prod0p) > 0 ? true : false;

	return (inside0 && inside1 && inside2);
}

IwBSplineCurve* OffsetCurveAlongSurfaceNormal
(
	IwContext& iwContext,	// I:
	IwBSplineSurface* surf, // I:
	IwBSplineCurve* uvCrv,	// I: UV curve on surface
	double dOffset,			// I:
	int intervalSize,		// I: if == 0 , using chord tol
	double decay			// I: [0~1] percentage at both ends to gradually reduce dOffset value
)
{
	IwBSplineCurve* offsetCrv = NULL;

	IwTArray<double> params;
	IwTArray<IwPoint3d> points;
	if ( intervalSize == 0 )
	{
		uvCrv->Tessellate(uvCrv->GetNaturalInterval(), 0.2, 0.5, 6, &params, &points, NULL);
	}
	else
	{
		IwExtent1d domCrv = uvCrv->GetNaturalInterval();
		uvCrv->EquallySpacedPoints(domCrv.GetMin(), domCrv.GetMax(), intervalSize, 0.01, &points, &params);
	}

	IwTArray<IwPoint3d> offsetPoints;
	IwPoint3d point, point3d, offsetPoint, cPnt;
	IwVector3d normal;
	IwPoint2d paramUV;
	//double dist;
	unsigned nSize = params.GetSize();
	double endSize = floor(decay*nSize);
	double offsetFactor = 1.0;
	for (unsigned i=0; i<nSize; i++)
	{
		point = points.GetAt(i);// (u,v,0)
		paramUV = IwPoint2d(point.x, point.y);
		surf->EvaluatePoint(paramUV, point3d);
		surf->EvaluateNormal(paramUV, FALSE, FALSE, normal);
		if ( decay == 0 )
		{
			offsetFactor = 1.0;
		}
		else
		{
			if ( i < endSize )
				offsetFactor = 1.0 - (endSize-i)/endSize;
			else if ( i > (nSize-1-endSize) )
				offsetFactor = 1.0 - (i-(nSize-1-endSize))/endSize;
			else
				offsetFactor = 1.0;
		}
		offsetPoint = point3d + offsetFactor*dOffset*normal;
		offsetPoints.Add(offsetPoint);
	}

	IwBSplineCurve::InterpolatePoints(iwContext, offsetPoints, &params, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, offsetCrv);

	return offsetCrv;
}

//////////////////////////////////////////////////
// The return curve is in XYZ space
IwBSplineCurve* OffsetCurveAlongSurfaceAndNormal
(
	IwContext& iwContext,	// I:
	IwBSplineSurface* surf, // I:
	IwBSplineCurve* uvCrv,	// I: UV curve on surface
	double sideOffsetStart,	// I: side distance in UV domain at start 
	double sideOffsetEnd,	// I: side distance in UV domain at end 
	double endExtension,	// I: extension at end
	double normalOffset,	// I: normal distance
	int intervalSize		// I: if == 0 , using chord tol
)
{
	IwBSplineCurve* offsetCrv = NULL;
	IwBSplineCurve* uvOffsetCrv = NULL;
	IwBSplineCurve* myUVCrv = NULL;
	if ( endExtension > 0.001 )
	{
		uvCrv->CreateExtendedCurve(iwContext, endExtension, 2, IW_CT_G1, myUVCrv);
	}
	else
	{
		myUVCrv = uvCrv;
	}

	IwTArray<double> params;
	IwTArray<IwPoint3d> points;

	if ( intervalSize == 0 )
	{
		myUVCrv->Tessellate(uvCrv->GetNaturalInterval(), 0.2, 0.5, 6, &params, &points, NULL);
	}
	else
	{
		IwExtent1d domCrv = myUVCrv->GetNaturalInterval();
		myUVCrv->EquallySpacedPoints(domCrv.GetMin(), domCrv.GetMax(), intervalSize, 0.01, &points, &params);
	}

	IwTArray<IwPoint3d> offsetPoints, normalPoints;
	IwPoint3d point, offsetPoint, normalPoint, surfPoint, cPnt;
	IwVector3d crossVec, tanVec, normalSurf, normalPlane=IwVector3d(0,0,1);
	IwPoint2d paramUV;
	double deltaOffset = (sideOffsetEnd - sideOffsetStart)/(params.GetSize()-1);
	double totalOffset;
	IwPoint3d evals[2];
	for (unsigned i=0; i<params.GetSize(); i++)
	{
		myUVCrv->Evaluate(params.GetAt(i), 1, FALSE, evals);
		tanVec = evals[1];
		tanVec.Unitize();
		crossVec = tanVec*normalPlane;
		point = points.GetAt(i);// (u,v,0)
		if ( i < 0.6*params.GetSize() )// constant offset for the first 60%
			totalOffset = sideOffsetStart;
		else// gradually increase offset for the other 40%
			totalOffset = (sideOffsetStart+4*(i-0.6*params.GetSize())*deltaOffset);
		offsetPoint = point + totalOffset*crossVec;
		offsetPoints.Add(offsetPoint);
		surf->EvaluatePoint(IwPoint2d(point.x, point.y), surfPoint);
		surf->EvaluateNormal(IwPoint2d(point.x, point.y), FALSE, FALSE, normalSurf);
		normalPoint = surfPoint + normalOffset*normalSurf;
		normalPoints.Add(normalPoint);
	}

	if ( fabs(normalOffset)<0.000001 )
	{
		IwBSplineCurve::InterpolatePoints(iwContext, offsetPoints, &params, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, uvOffsetCrv);
		// Convert to XYZ space
		IwCrvOnSurf* crvOnSurf = new (iwContext) IwCrvOnSurf(*uvOffsetCrv, *surf);

		if ( crvOnSurf )
		{
			IwTArray<double> breakParams;
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
			breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
			double aTol;
			crvOnSurf->ApproximateCurve(iwContext, IW_AA_HERMITE, breakParams, 0.05, aTol, offsetCrv, FALSE, FALSE);
		}
	}
	else
	{
		IwBSplineCurve::InterpolatePoints(iwContext, normalPoints, &params, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, offsetCrv);
	}

	if ( myUVCrv && endExtension > 0.001 )
		IwObjDelete(myUVCrv);

	return offsetCrv;
}

bool IntersectIwTArrayPointsByPlane( 
		IwTArray<IwPoint3d>			pnts, 
		IwPoint3d&					pt, 
		IwVector3d&					nm,
		double						distTol,
		IwPoint3d&					intPnt,		// O:
		int *index)								// O:
{
	IwVector3d vec0, vec1;
	double dist0, dist1;
	double minDist = HUGE_DOUBLE, dist3d;
	int minIndex;
	IwPoint3d minPnt;
	bool found = false;
	for (unsigned i=1; i<pnts.GetSize(); i++)
	{
		vec0 = pnts.GetAt(i-1) - pt;
		vec1 = pnts.GetAt(i) - pt;
		dist0 = vec0.Dot(nm);
		dist1 = vec1.Dot(nm);
		if ( dist0*dist1 <= 0 )// not on the same side of plane
		{
			if (fabs(dist0) < fabs(dist1))
			{
				if ( fabs(dist0) < distTol )
				{
					dist3d = pnts.GetAt(i-1).DistanceBetween(pt);
					if ( dist3d < minDist )
					{
						minDist = dist3d;
						minPnt = pnts.GetAt(i-1);
						minIndex = i-1;
						found = true;
					}
				}
			}
			else
			{
				if ( fabs(dist1) < distTol )
				{
					dist3d = pnts.GetAt(i).DistanceBetween(pt);
					if ( dist3d < minDist )
					{
						minDist = dist3d;
						minPnt = pnts.GetAt(i);
						minIndex = i;
						found = true;
					}
				}
			}
		}
	}

	if ( found )
	{
		intPnt = minPnt;
		if (index)
			*index = minIndex;
	}

	return found;
}

IwVector3d RotateVectorAlongVector
(
	IwVector3d inputVector,		// I: vector to be roatated
	IwPoint3d rotatingCenter,	// I: rotating center point
	IwVector3d rotatingAxis,	// I: rotating axis
	double rotatingAngleRadian	// I: rotating angle
)
{
	IwVector3d xRefAxis = inputVector;
	IwVector3d xAxis, yAxis, zAxis;
	xRefAxis.MakeUnitOrthoVectors(NULL, xAxis, yAxis, zAxis);
	IwAxis2Placement frame(rotatingCenter, xAxis, yAxis);

	frame.RotateAboutAxis(rotatingAngleRadian, rotatingAxis);

	IwVector3d rotatedXAxis = frame.GetXAxis();

	return rotatedXAxis;
}

////////////////////////////////////////////////////////////////////////////
// A work around function to avoid an issue in SMLib8617.
////////////////////////////////////////////////////////////////////////////
IwBrep* CreateLinearSweep(
		IwContext&					iwContext,		// I:
		IwTArray<IwCurve*>			profileCurves,	// I: cross profiles 
		IwVector3d					sweepVector,	// sweep vector, magnitude matters
		bool						bCapEnds,		// close ends or not
		CDataDoc*					pDoc			// For debug
		)		
{
	IwBrep* sweepBrep = NULL;

	IwBrep *profileBrep = new (iwContext) IwBrep();
	IwBrep *capEndBrep0 = NULL, *capEndBrep1 = NULL;

	// Create sweep surfaces
	IwFace* face;
	for (unsigned i=0; i<profileCurves.GetSize(); i++)
	{
		IwBSplineSurface* sweepSurf=NULL;
		IwBSplineCurve* curve = (IwBSplineCurve*)profileCurves.GetAt(i);
		IwBSplineSurface::CreateLinearSweep(iwContext, *curve, sweepVector, sweepSurf);
		profileBrep->CreateFaceFromSurface(sweepSurf, sweepSurf->GetNaturalUVDomain(), face);
	}
	profileBrep->SewAndOrient();
if(0)
	ShowBrep(pDoc, profileBrep, "profileBrep", blue);

	// Create both cap ends
	if ( bCapEnds )
	{
		IwAxis2Placement transM = IwAxis2Placement(sweepVector, IwVector3d(1,0,0), IwVector3d(0,1,0));
		// copy curves
		IwTArray<IwCurve*> profCurves0, profCurves1;
		IwCurve *copyCrv0, *copyCrv1;
		for (unsigned i=0; i<profileCurves.GetSize(); i++)
		{
			profileCurves.GetAt(i)->Copy(iwContext, copyCrv0);
			profCurves0.Add(copyCrv0);
			profileCurves.GetAt(i)->Copy(iwContext, copyCrv1);
			copyCrv1->Transform(transM);
			profCurves1.Add(copyCrv1);
		}

		// Create first cap face
		capEndBrep0 = new (iwContext) IwBrep();
		capEndBrep0->CreatePlanarFaceWith3DCurves(capEndBrep0->GetInfiniteRegion(), profCurves0, 0.0001, face);
		// Create 2nd cap face
		capEndBrep1 = new (iwContext) IwBrep();
		capEndBrep1->CreatePlanarFaceWith3DCurves(capEndBrep1->GetInfiniteRegion(), profCurves1, 0.0001, face);
if(0)
{
	ShowBrep(pDoc, capEndBrep0, "capEndBrep0", cyan);
	ShowBrep(pDoc, capEndBrep1, "capEndBrep1", cyan);
}

		// Merge
		IwBrep *rBrep0=NULL, *rBrep1=NULL;
		IwMerge merge0(iwContext, profileBrep, capEndBrep0, 0.001, 0.01);
		merge0.NonManifoldBoolean(IW_BO_MERGE, rBrep0);
		rBrep0->SewAndOrient();
		if ( rBrep0 )
		{
			IwMerge merge1(iwContext, rBrep0, capEndBrep1, 0.001, 0.01);
			merge1.NonManifoldBoolean(IW_BO_MERGE, rBrep1);
			rBrep1->SewAndOrient();
			if ( rBrep1 )
			{
				IwStatus error = rBrep1->MakeManifold();
				if ( error == IW_SUCCESS )
					sweepBrep = rBrep1;
			}
		}
	}
	else
	{
		sweepBrep = profileBrep;
	}
if(0)
	ShowBrep(pDoc, sweepBrep, "sweepBrep", green);

	return sweepBrep;
}

//////////////////////////////////////////////////////////
// Note, this is not a general way to test a conic surface.
bool IsConicSurface
(
	IwBSplineSurface* surf,		// I:
	bool* isCylinder,			// O:
	bool* isFillet,				// O:
	IwAxis2Placement* conicAxis	// O:
)
{
	bool isConic = false;
	if (isCylinder)
		*isCylinder = false;
	if (isFillet)
		*isFillet = false;

	IwExtent2d dom = surf->GetNaturalUVDomain();

	IwBSplineCurve *isoUmin, *isoUmax, *isoVmin, *isoVmax;
	surf->CreateIsoBoundaries(*iwContext, IW_SP_U, 0.001, isoUmin, isoUmax);
	surf->CreateIsoBoundaries(*iwContext, IW_SP_V, 0.001, isoVmin, isoVmax);

	IwAxis2Placement refAxisMin, refAxisMax;
	double radiusMin, radiusMax;
	double sAngle, eAngle;
	if ( isoUmin->IsArc(10, 0.01, refAxisMin, radiusMin, sAngle, eAngle) &&
		 isoUmax->IsArc(10, 0.01, refAxisMax, radiusMax, sAngle, eAngle) )
	{
		if (isFillet)
			*isFillet=true;
		if ( refAxisMin.GetZAxis().IsParallelTo(refAxisMax.GetZAxis(), 0.01) )
		{
			isConic = true;
			if (conicAxis)
				*conicAxis = refAxisMax;
		}
		if (isCylinder)
		{
			if ( IS_EQ_TOL6(radiusMin, radiusMax) )
				*isCylinder = true;
		}
	}

	if ( isoVmin->IsArc(10, 0.01, refAxisMin, radiusMin, sAngle, eAngle) &&
		 isoVmax->IsArc(10, 0.01, refAxisMax, radiusMax, sAngle, eAngle) )
	{
		if (isFillet)
			*isFillet=true;
		if ( refAxisMin.GetZAxis().IsParallelTo(refAxisMax.GetZAxis(), 0.01) )
		{
			isConic = true;
			if (conicAxis)
				*conicAxis = refAxisMax;
		}
		if (isCylinder)
		{
			if ( IS_EQ_TOL6(radiusMin, radiusMax) )
				*isCylinder = true;
		}
	}

	if ( isoUmin )
		IwObjDelete(isoUmin);
	if ( isoUmax )
		IwObjDelete(isoUmax);
	if ( isoVmin )
		IwObjDelete(isoVmin);
	if ( isoVmax )
		IwObjDelete(isoVmax);

	return isConic;
}
//////////////////////////////////////////////////////////
// Note, this is not a general way to test a ruled surface.
bool IsRuledSurface
(
	IwBSplineSurface* surf,		// I:
	IwVector3d* rulingVector	// I: optional, the surface's ruling lines have to be parallel to this vector
)
{
	bool isRuled = false;

	IwExtent2d dom = surf->GetNaturalUVDomain();

	IwBSplineCurve *isoUmin, *isoUmax, *isoUmid, *isoVmin, *isoVmax, *isoVmid;

	surf->CreateIsoParametricCurve(*iwContext, IW_SP_U, dom.Evaluate(0.01,0.5).x, 0.01, isoUmin);
	surf->CreateIsoParametricCurve(*iwContext, IW_SP_U, dom.Evaluate(0.5,0.5).x, 0.01, isoUmid);
	surf->CreateIsoParametricCurve(*iwContext, IW_SP_U, dom.Evaluate(0.99,0.5).x, 0.01, isoUmax);

	surf->CreateIsoParametricCurve(*iwContext, IW_SP_V, dom.Evaluate(0.5,0.01).y, 0.01, isoVmin);
	surf->CreateIsoParametricCurve(*iwContext, IW_SP_V, dom.Evaluate(0.5,0.5).y, 0.01, isoVmid);
	surf->CreateIsoParametricCurve(*iwContext, IW_SP_V, dom.Evaluate(0.5,0.99).y, 0.01, isoVmax);

	IwVector3d linePntMin, lineVectorMin, linePntMid, lineVectorMid, linePntMax, lineVectorMax, linePnt, lineVector;
	if (isoUmin->IsLine(10, 0.01, linePntMin, lineVectorMin) && 
		isoUmax->IsLine(10, 0.01, linePntMax, lineVectorMax) &&
		isoUmid->IsLine(10, 0.01, linePntMid, lineVectorMid) &&
		!isoVmin->IsLine(10, 0.01, linePnt, lineVector) && 
		!isoVmax->IsLine(10, 0.01, linePnt, lineVector) &&
		!isoVmid->IsLine(10, 0.01, linePnt, lineVector))
	{
		if ( rulingVector )
		{
			if (rulingVector->IsParallelTo(lineVectorMin, 0.1) &&
				rulingVector->IsParallelTo(lineVectorMid, 0.1) &&
				rulingVector->IsParallelTo(lineVectorMax, 0.1))
			{
				isRuled = true;
			}
		}
		else
		{
			isRuled = true;
		}
	}
	else if (!isoUmin->IsLine(10, 0.01, linePnt, lineVector) && 
		     !isoUmax->IsLine(10, 0.01, linePnt, lineVector) &&
		     !isoUmid->IsLine(10, 0.01, linePnt, lineVector) &&
			 isoVmin->IsLine(10, 0.01, linePntMin, lineVectorMin) && 
		     isoVmax->IsLine(10, 0.01, linePntMax, lineVectorMax) &&
		     isoVmid->IsLine(10, 0.01, linePntMid, lineVectorMid))
	{
		if ( rulingVector)
		{
			if (rulingVector->IsParallelTo(lineVectorMin, 0.1) &&
				rulingVector->IsParallelTo(lineVectorMid, 0.1) &&
				rulingVector->IsParallelTo(lineVectorMax, 0.1))
			{
				isRuled = true;
			}
		}
		else
		{
			isRuled = true;
		}
	}

	if ( isoUmin )
		IwObjDelete(isoUmin);
	if ( isoUmax )
		IwObjDelete(isoUmax);
	if ( isoUmid )
		IwObjDelete(isoUmid);
	if ( isoVmin )
		IwObjDelete(isoVmin);
	if ( isoVmax )
		IwObjDelete(isoVmax);
	if ( isoVmid )
		IwObjDelete(isoVmid);

	return isRuled;
}

////////////////////////////////////////////////////////////////
IwEdgeuse * GetFilletEdgeuse
(
    IwEdge* pEdge                 // I: target edge to query
)
{
  // skip lamina edges
  if (pEdge->IsLamina()) 
    {
      return NULL; // Skip all lamina edges
    }

  // get primary edgeuse and mate
  IwEdgeuse *pEU0 = pEdge->GetPrimaryEdgeuse();
  IwEdgeuse *pEU1 = pEU0->GetMate();

  // remember if primary edgeuse is from a shell or solid
  IwBoolean bIsShellFilleting = FALSE;

  // get an edgeuse with IW_OT_SAME orientation (from an interior region when working on a solid)
  if (pEU0->GetShell()->GetRegion() == pEU1->GetShell()->GetRegion()) 
    {
      // remember we are shell filleting
      bIsShellFilleting = TRUE;

      // get edgeuse with IW_OT_SAME orientation
      if (pEU0->GetOrientation() == IW_OT_OPPOSITE)
          pEU0 = pEU0->GetMate();

      // check state - must have an edgeuse
      if (!pEU0) return NULL;

    } // end working on a shell branch
  else // primary and mate edgeuses are in different regions
    {
      IwShell  * pShell       = pEU0->GetShell();
      IwRegion * pInfiniteReg = pShell->GetBrep()->GetInfiniteRegion();

      // get edgeuse for interior region
      if (pShell->GetRegion() == pInfiniteReg) 
        {
          pEU0 = pEU0->GetMate();
        }

      // get edgeuse with IW_OT_SAME orientation
      if (pEU0->GetOrientation() == IW_OT_OPPOSITE) 
        {
          pEU0 = pEU0->GetRadial();
        }

      // check state - edgeuse should be part of interior region with IW_OT_SAME orientation
      if (   !pEU0
          || pEU0->GetShell()->GetRegion() == pInfiniteReg 
          || pEU0->GetOrientation()        == IW_OT_OPPOSITE) 
        {
          return NULL;
        }
    } // working on a solid branch

  // get radial mate to selected edgeuse
  // pEU0 and peU1 now bound the sector to be filleted
  IW_ASSERT(pEU0 != NULL)
  pEU1 = pEU0->GetRadial();

  // Test to determine if edge bounds two tangential faces
  double dTestParam = pEdge->GetInterval().Evaluate(0.34);
  IwPoint3d sPnt, sPnt2;
  IwVector3d sBinVec2, sBinVec, sFaceuseNormal, sFaceuseNormal2;

  // get normalized normal and binormals from the two edgeuses at a common point
  SE(pEU0->EvaluateBinormal(dTestParam, FALSE, sPnt,  sBinVec,  NULL, &sFaceuseNormal));
  SE(pEU1->EvaluateBinormal(dTestParam, FALSE, sPnt2, sBinVec2, NULL, &sFaceuseNormal2));
  SE(sFaceuseNormal.Unitize());
  SE(sFaceuseNormal2.Unitize());
  SE(sBinVec.Unitize());
  SE(sBinVec2.Unitize());

  // set max nonTangent angle to 88 degrees
  double dMaxDotValue = iwos_Cosine(88.0*IW_PI/180.0);

  // when 1st edgeuse surface normal is perpendicular to 2nd edgeuse binormal or 
  //      1st edgeuse binormal       is perpendicular to 2nd edgeuse surface normal
  // the faces are tangent and can't be filleted
  double dDot1        = sBinVec2.Dot(sFaceuseNormal);
  double dDot2 = sBinVec.Dot(sFaceuseNormal2);
  if (   iwos_Fabs(dDot1) < dMaxDotValue
      || iwos_Fabs(dDot2) < dMaxDotValue) 
    {
      return NULL;
    }

  // when filleting a shell (not a solid) - get a sector which is not convex
  if (bIsShellFilleting) 
    {
      // evaluate pair of points close to edge on each face to be filleted
      double dStepoffDistance = 0.001;
      SE(pEU0->EvaluateBinormalStepOff(dTestParam, dStepoffDistance, sPnt,  sBinVec,  NULL, &sFaceuseNormal));
      SE(pEU1->EvaluateBinormalStepOff(dTestParam, dStepoffDistance, sPnt2, sBinVec2, NULL, &sFaceuseNormal2));
      SE(sFaceuseNormal.Unitize());
      SE(sFaceuseNormal2.Unitize());

      // get dot_product between stepOff point vector and Face normals
      IwVector3d sVec = sPnt2-sPnt;
      SE(sVec.Unitize());
      double dDot1 = sVec.Dot(sFaceuseNormal);
      sVec = -sVec;
      double dDot2 = sVec.Dot(sFaceuseNormal2);

      // when dot products are both negative, sector is convex 
      // switch to sector on other side of pEU0
      if (   dDot1 < 0.0 
          && dDot2 < 0.0) 
        {
          pEU0 = pEU0->GetMate();
          if (pEU0->GetOrientation() == IW_OT_OPPOSITE) 
            {
              pEU0 = pEU0->GetRadial();
            }
          pEU1 = pEU0->GetRadial();
        } // end need to switch sectors check
    } // end when filleting a shell check

  // all done
  return pEU0;

} // end my_get_fillet_edgeuse

IwPoint3d MirrorPointAlongLine(IwPoint3d pntToMirror, IwPoint3d linePnt, IwVector3d lineVector)
{
	lineVector.Unitize();
	IwVector3d vec0 = pntToMirror - linePnt;
	double length = vec0.Dot(lineVector);
	IwPoint3d projectedPnt = linePnt + length*lineVector;

	IwPoint3d mirrorPoint = 2*projectedPnt - pntToMirror;

	return mirrorPoint;
}

bool MarchToEqualDistance
(
	IwPoint3d pnt,				// I:
	IwVector3d marchDir,		// I:
	IwBSplineCurve* curve,		// I:
	IwPoint3d& equalDistPosition// O:
)
{
	IwPoint3d soFarPosition;
	IwPoint3d cPnt;
	double param;
	double soFarDistToCurve;
	bool gotIt = true;
	double soFarDist = DistFromPointToCurve(pnt, curve, cPnt, param)/2.0;
	for (int i=0; i<100; i++)
	{
		soFarPosition = pnt + soFarDist*marchDir;
		soFarDistToCurve = DistFromPointToCurve(soFarPosition, curve, cPnt, param);
		if ( soFarDistToCurve < 0 )
		{
			gotIt = false;
			break;
		}
		if ( fabs(soFarDist-soFarDistToCurve) > 0.05 )
		{
			soFarDist += 0.5*(soFarDistToCurve - soFarDist);
		}
		else
		{
			equalDistPosition = soFarPosition;
			break;
		}
	}

	return gotIt;
}

bool GetEdgeLengths
(
	IwBrep* pBrep,					// I:
	IwTArray<double>& edgeLengths,	// O: The sorted edge lengths
	IwTArray<IwEdge*>& edges		// O: The corresponding edges
)
{
	if (pBrep==NULL) return false;
	edgeLengths.RemoveAll();
	edges.RemoveAll();

	// 
	IwTArray<IwPoint3d> edgesInfo;

	// 
	double length;
	IwCurve* curve;
	IwEdge* edge;
	IwTArray<IwEdge*> allEdges;
	pBrep->GetEdges(allEdges);
	for (unsigned i=0; i<allEdges.GetSize(); i++)
	{
		edge = allEdges.GetAt(i);
		curve = edge->GetCurve();
		curve->Length(edge->GetInterval(), 0.01, length);
		edgesInfo.Add(IwPoint3d(length, i, 0)); // 0th: element length, 1st: edge index, 2nd: none
	}

	if ( edgesInfo.GetSize() == 0 ) 
		return false;

	// Sort lengths
	SortIwTArray(edgesInfo, 0, false); // sort based on the edge length, and descending
	for (unsigned i=0; i<edgesInfo.GetSize(); i++)
	{
		edgeLengths.Add(edgesInfo.GetAt(i).x); // lengths
		edges.Add(allEdges.GetAt(edgesInfo.GetAt(i).y));
	}

	return true;
}

// This is a very specific function to check whether a 
bool IsFileNameExistInFolder(QString folderName, QString fileNameExt, QString fileName0, QString fileName1, QString fileName2)
{
	QString fileExt = fileNameExt.remove("*");
	fileExt = fileExt.remove(".");
	QStringList filters;
	filters << ("*." + fileExt);

	bool bExist = false;
	QDir sDir(folderName);

	//QString fullFilename;
	QStringList listTmpDir = sDir.entryList(filters, QDir::Files);
	for (unsigned i=0; i<listTmpDir.size(); i++)
	{
		QString fileName = listTmpDir.at(i);
		if ( fileName.contains(fileName0, Qt::CaseInsensitive) &&
			 fileName.contains(fileName1, Qt::CaseInsensitive) &&
			 fileName.contains(fileName2, Qt::CaseInsensitive) )
		{
			bExist = true;
			return bExist; // Exist
		}
	}
	return bExist;
}