/****************************************************************************
** Meta object code from reading C++ file 'Viewport.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Viewport.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Viewport.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Viewport_t {
    QByteArrayData data[22];
    char stringdata0[264];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Viewport_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Viewport_t qt_meta_stringdata_Viewport = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Viewport"
QT_MOC_LITERAL(1, 9, 15), // "maximizeChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 9), // "FrontView"
QT_MOC_LITERAL(4, 36, 11), // "autoRotZ180"
QT_MOC_LITERAL(5, 48, 8), // "BackView"
QT_MOC_LITERAL(6, 57, 8), // "LeftView"
QT_MOC_LITERAL(7, 66, 9), // "RightView"
QT_MOC_LITERAL(8, 76, 7), // "TopView"
QT_MOC_LITERAL(9, 84, 10), // "BottomView"
QT_MOC_LITERAL(10, 95, 7), // "IsoView"
QT_MOC_LITERAL(11, 103, 11), // "IsoBackView"
QT_MOC_LITERAL(12, 115, 10), // "MedialView"
QT_MOC_LITERAL(13, 126, 11), // "LateralView"
QT_MOC_LITERAL(14, 138, 11), // "BestFitView"
QT_MOC_LITERAL(15, 150, 14), // "ToggleMaximize"
QT_MOC_LITERAL(16, 165, 28), // "ToggleShowAxisRotationRegion"
QT_MOC_LITERAL(17, 194, 14), // "ToggleShowAxis"
QT_MOC_LITERAL(18, 209, 15), // "AdjustBBoxDepth"
QT_MOC_LITERAL(19, 225, 21), // "SetCurrentCoordSystem"
QT_MOC_LITERAL(20, 247, 11), // "CoordSystem"
QT_MOC_LITERAL(21, 259, 4) // "axes"

    },
    "Viewport\0maximizeChanged\0\0FrontView\0"
    "autoRotZ180\0BackView\0LeftView\0RightView\0"
    "TopView\0BottomView\0IsoView\0IsoBackView\0"
    "MedialView\0LateralView\0BestFitView\0"
    "ToggleMaximize\0ToggleShowAxisRotationRegion\0"
    "ToggleShowAxis\0AdjustBBoxDepth\0"
    "SetCurrentCoordSystem\0CoordSystem\0"
    "axes"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Viewport[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,  140,    2, 0x0a /* Public */,
       3,    0,  143,    2, 0x2a /* Public | MethodCloned */,
       5,    1,  144,    2, 0x0a /* Public */,
       5,    0,  147,    2, 0x2a /* Public | MethodCloned */,
       6,    1,  148,    2, 0x0a /* Public */,
       6,    0,  151,    2, 0x2a /* Public | MethodCloned */,
       7,    1,  152,    2, 0x0a /* Public */,
       7,    0,  155,    2, 0x2a /* Public | MethodCloned */,
       8,    1,  156,    2, 0x0a /* Public */,
       8,    0,  159,    2, 0x2a /* Public | MethodCloned */,
       9,    1,  160,    2, 0x0a /* Public */,
       9,    0,  163,    2, 0x2a /* Public | MethodCloned */,
      10,    0,  164,    2, 0x0a /* Public */,
      11,    0,  165,    2, 0x0a /* Public */,
      12,    1,  166,    2, 0x0a /* Public */,
      12,    0,  169,    2, 0x2a /* Public | MethodCloned */,
      13,    1,  170,    2, 0x0a /* Public */,
      13,    0,  173,    2, 0x2a /* Public | MethodCloned */,
      14,    0,  174,    2, 0x09 /* Protected */,
      15,    0,  175,    2, 0x09 /* Protected */,
      16,    0,  176,    2, 0x09 /* Protected */,
      17,    0,  177,    2, 0x09 /* Protected */,
      18,    0,  178,    2, 0x09 /* Protected */,
      19,    1,  179,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,   21,

       0        // eod
};

void Viewport::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Viewport *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->maximizeChanged(); break;
        case 1: _t->FrontView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->FrontView(); break;
        case 3: _t->BackView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->BackView(); break;
        case 5: _t->LeftView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->LeftView(); break;
        case 7: _t->RightView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->RightView(); break;
        case 9: _t->TopView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->TopView(); break;
        case 11: _t->BottomView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->BottomView(); break;
        case 13: _t->IsoView(); break;
        case 14: _t->IsoBackView(); break;
        case 15: _t->MedialView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->MedialView(); break;
        case 17: _t->LateralView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->LateralView(); break;
        case 19: _t->BestFitView(); break;
        case 20: _t->ToggleMaximize(); break;
        case 21: _t->ToggleShowAxisRotationRegion(); break;
        case 22: _t->ToggleShowAxis(); break;
        case 23: _t->AdjustBBoxDepth(); break;
        case 24: _t->SetCurrentCoordSystem((*reinterpret_cast< const CoordSystem(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 24:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CoordSystem >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Viewport::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Viewport::maximizeChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Viewport::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Viewport.data,
    qt_meta_data_Viewport,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Viewport::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Viewport::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Viewport.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Viewport::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void Viewport::maximizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
