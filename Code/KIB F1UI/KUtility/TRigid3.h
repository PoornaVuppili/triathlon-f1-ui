///////////////////////////////////////////////////////////////////////
//	File:    TRigid3.h
//	Purpose: A template class for 3d rigid transformation (rotation + translation)
///////////////////////////////////////////////////////////////////////

#ifndef TRIGID3_H
#define TRIGID3_H

#include "TRotate3.h"
#include "TVector3.h"

template <typename T>
class TRigid3
{
protected:
    TRotate3<T> rotate;     // rotation
    TVector3<T> translate;   // translation

public:
    TRigid3() {}
    TRigid3(const TRotate3<T>& rot,const TVector3<T>& vec) : rotate(rot), translate(vec) {}

    // Get values
    const TRotate3<T>& GetRotationMatrix() const { return rotate; }
    const TVector3<T>& GetTranslationVector() const { return translate; }
    Quaternion<T> GetRotationQuaternion() const { return Quaternion<T>(rotate); }

    // Set values
    void SetRotationMatrix(const TRotate3<T>& rot) { rotate = rot; }
    void SetTranslationVector(const TVector3<T>& vec) { translate = vec; }
    void SetRotationQuaternion(const Quaternion<T>& quat) { rotate = quat.toRotationMatrix(); }

    // Get the matrix in the gl format
    void GetGLMatrix(T(&f)[16])
    {
        f[0] = rotate(0,0);
        f[1] = rotate(1,0);
        f[2] = rotate(2,0);
        f[3] = 0;

        f[4] = rotate(0,1);
        f[5] = rotate(1,1);
        f[6] = rotate(2,1);
        f[7] = 0;

        f[8]  = rotate(0,2);
        f[9]  = rotate(1,2);
        f[10] = rotate(2,2);
        f[11] = 0;

        f[12] = translate.GetX();
        f[13] = translate.GetY();
        f[14] = translate.GetZ();
        f[15] = 1;
    }

    // Get the rotation component in the gl format
    void GetGLRotMatrix(T(&f)[16])
    {
        f[0] = rotate(0,0);
        f[1] = rotate(1,0);
        f[2] = rotate(2,0);
        f[3] = 0;

        f[4] = rotate(0,1);
        f[5] = rotate(1,1);
        f[6] = rotate(2,1);
        f[7] = 0;

        f[8]  = rotate(0,2);
        f[9]  = rotate(1,2);
        f[10] = rotate(2,2);
        f[11] = 0;

        f[12] = 0;
        f[13] = 0;
        f[14] = 0;
        f[15] = 1;
    }

    // Set values
    void SetIdentity()
    {
        rotate.setIdentity();
        translate.setZero();
    }

    // Multiply the current transformation by a translation matrix
    void Translate(T x, T y, T z)
    {
        translate.x() += x;
        translate.y() += y;
        translate.z() += z;
    }

    void Translate(const TPoint3<T>& t)
    {
        translate += t;
    }

    // Multiply the current transformation by a rotation matrix
    void RotateX(T degree)
    {
        rotate.RotateX( degree );
        translate.RotateX( degree );
    }

    void RotateY(T degree)
    {
        rotate.RotateY( degree );
        translate.RotateY( degree );
    }

    void RotateZ(T degree)
    {
        rotate.RotateZ( degree );
        translate.RotateZ( degree );
    }

    void Rotate(const TRotate3<T>& r)
    {
        rotate = r * rotate;
        translate = r * translate;
    }

    TPoint3<T> operator*( const TPoint3<T>& pt ) const
    {
        return rotate * pt + translate;
    }

    // Get the inverse transform
    TRigid3<T> Inverse() const
    {
        TRotate3<T> invRot = rotate.Inverse();
        return TRigid3( invRot, -(invRot * translate));
    }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TRigid3<float> Rigid3Float;
typedef TRigid3<double> Rigid3Double;


#endif //TRIGID_H
