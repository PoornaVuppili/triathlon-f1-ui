#pragma once

#include <QGLWidget>
#include <QtGui>
#include "Cursor.h"
#include "Globals.h"
#include "..\KApp\Manager.h"
#include "..\KApp\ViewWidget.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"

class COpenGLView;

enum CManagerActivateType
	{
	MAN_ACT_TYPE_RESET,			// execute its Reset() function, create from scratch without optimization, no UI activated
	MAN_ACT_TYPE_REFINE,		// execute its Refine() function, optimize results, converge gradually, but no UI activated
	MAN_ACT_TYPE_REFINE_CONVERGE,	// execute its Refine() function and force to converge
	MAN_ACT_TYPE_EDIT,			// provide UI to allow user to edit parameters
	MAN_ACT_TYPE_REGENERATE,	// update data with existing parameters, no UI activated
	MAN_ACT_TYPE_VALIDATE,		// run validate function only
	MAN_ACT_TYPE_REVIEW			// for review, show UI but no UI editing.
	};

#define IsMotionStudioManager(pManager) ((pManager!=NULL) && (pManager->GetClassName()=="CMotionStudio" || pManager->GetClassName()=="CMotionStudioGrood"))
// Only "CMotionStudio" and "CMotionStudioGrood" do not use mouse events to interact with users.
#define IsAMouseEventManager(pManager) ((pManager!=NULL) && (pManager->GetClassName()!="CMotionStudio" && pManager->GetClassName()!="CMotionStudioGrood"))

class CManager : public Manager
	{
public:

	CManager( COpenGLView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT ); 

	virtual	~CManager();

    // Event processing
    //  cursor      : mouse cursor position in SCREEN coordinates
    //  keyModifier : key modifiers
    //  viewport    : the viewport where the mouse/keyboard event take place
    // Return:
    //  true  : the calling function should continue response
    //  false : ........................... stop passing along the message

    // Assuming: mouse tracking is on; direct both MouseLeftMove and MouseNoneMove to MouseMove
    virtual bool MouseLeftMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL ) { return MouseMove(cursor, keyModifier, vp); }
    virtual bool MouseMiddleMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL ) { return MouseMove(cursor, keyModifier, vp); }
    virtual bool MouseNoneMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool MouseDoubleClickLeft( const QPoint& cursor, Viewport* viewport=NULL )  { return true; }
	virtual bool keyPressEvent( QKeyEvent* event, Viewport* vp=NULL );
	virtual bool Reject(){return false;}
	virtual bool DisplayCaughtPoint(IwVector3d offset=IwVector3d(0,0,0));
	virtual void Dialog() {}
	virtual void UpdateColors(){}
	virtual void UpdateData(){}
	virtual void ViewCenterWasModified() {}
	virtual bool DontAdjustViewCenter(){return false;}
	bool DestroyMe(){return m_bDestroyMe;}
	void SetDestroyMe(){m_bDestroyMe=true;}
	COpenGLView*	GetView(){return m_pView;}
	ViewWidget*		GetViewWidget() {return viewWidget;};
	void ReDraw();
	virtual void InvalidateMouseLast(){m_bMouseLast = false;}
	void SetAltKeyState( bool bAltKeyDown ){m_bAltKeyDown = bAltKeyDown;}
	void SetCtrlKeyState( bool bCtrlKeyDown ){m_bCtrlKeyDown = ( bCtrlKeyDown != 0 );}
	void SetShftKeyState( bool bShftKeyDown ){m_bShftKeyDown = ( bShftKeyDown != 0 );}
	bool IsDialogOpen(){return m_bDialogOpen;}
	void SetDialogOpen( bool bDialogOpen ){m_bDialogOpen = bDialogOpen;}
	void GetCursorAndPrompt( CCursorType& eCursorType, QString& sPrompt );
	void SetCursorAndPrompt( CCursorType eCursorType, QString sPrompt );
	void ClearCursorAndPrompt();
	void SetPrompt( QString sPrompt );

	void SetSelectionMode( QString sPrompt, CCursorType eCursorType = CURSOR_SELECTION );
	void SetQuestionMode( QString sPrompt );
	void SetSketchMode( QString sPrompt );

	void EnableAction( QAction* action, bool bEnable );
	void SetActionAsTitle( QAction* action );
	void EnableCancelAction(bool val) {EnableAction(this->m_actCancel, val);};
	bool GetCancelActionExistence();

	QString GetClassName();

	// Search points
	void SetMouseMoveSearchingList(IwTArray<IwPoint3d>& searchList, int pixelDist = 5);
	const IwTArray<IwPoint3d>& GetMouseMoveSearchingList()	{	return m_mouseMoveSearchingList; };
	void SetMouseSearchingDistance(int pixelDist) {m_searchPixelDistance=pixelDist;};
	bool GetMouseMoveCaughtPoint(IwPoint3d& caughtPoint, int& caughtPointIndex);
	void RemoveMouseMoveCaughtPoint() {m_mouseMoveCaughtPointIndex=-1;};
	void MoveCaughtPoint(const QPoint& cursor, IwVector3d& movingVec = IwVector3d(0,0,0));

	QPoint GetPositionStart()						{	return m_posStart;		};
	void SetPositionStart(QPoint cursor)			{	m_posStart = cursor;	};
	QPoint GetPositionLast()						{	return m_posLast;		};
	void SetPositionLast(QPoint cursor)				{	m_posLast = cursor;		};
	int GetElapseTime();

protected slots:
	virtual bool OnReviewBack() {return false;}; // This function returns false to indicate no founction body in this class.
	virtual bool OnReviewNext() {return false;}; // The derived class function should return true to indicate there is function body and doing something.
	virtual bool OnReviewRework() {return false;}; // See how the return boolean is used in Manager.cpp.

protected:

	COpenGLView*			m_pView;

    bool				m_bDestroyMe;

	bool				m_bAltKeyDown;
	bool				m_bShftKeyDown;
	bool				m_bCtrlKeyDown;

	bool				m_bDialogOpen;

	bool				m_bMouseLast;

	CCursorType			m_eCursorType;
	QString				m_sPrompt;
	
	//QToolBar*			m_tbar;	
	QAction*			m_actAccept;
	QAction*			m_actCancel;

	QFont				m_fontNormal;
	QFont				m_fontBold;
	QFont				m_fontTitle;

	QString				m_sClassName;

	CManagerActivateType	m_manActType;

	int					m_searchPixelDistance;
	IwTArray<IwPoint3d> m_mouseMoveSearchingList;
	int					m_mouseMoveCaughtPointIndex;// which one (position index in m_mouseMoveSearchingList) is caught

    QPoint				m_posStart; // the button clicked-down point
	QPoint				m_posLast;  // the last mouse-move point

	int					m_mouseIdleTime;// seconds
	QDateTime			m_lastMouseMoveTime;
	QDateTime			m_beginningTime;
	};

