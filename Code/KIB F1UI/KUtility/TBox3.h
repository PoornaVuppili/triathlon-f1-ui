///////////////////////////////////////////////////////////////////////
//	TBox3.h
//	  - a template class for a 3D box in any orientation
///////////////////////////////////////////////////////////////////////

#ifndef TBOX3_H
#define TBOX3_H

#include "TPoint3.h"

template <typename T>
class TBox3
{
protected:
    TPoint3<T> ct, ax[3];  // center and three axes
    T sp[3];               // span along each axes in the positive & negative direction around the center

public:
    TBox3() {
        // default to be an axis-aligned empty box
        ax[0].SetXYZ(1,0,0);
        ax[1].SetXYZ(0,1,0);
        ax[2].SetXYZ(0,0,1);
        for(int i=0; i<3; i++)
            sp[i] = 0;
    }

    TBox3(const TPoint3<T>& c, const TPoint3<T>& ax0, const TPoint3<T>& ax1, const TPoint3<T>& ax2, T s0, T s1, T s2) {
        ct = c;
        ax[0] = ax0;
        ax[1] = ax1;
        ax[2] = ax2;
        sp[0] = s0;
        sp[1] = s1;
        sp[2] = s2;
    }

    TBox3(const TBox3& bx) {
        ct = bx.GetCenter();
        for(int i=0; i<3; i++)
        {
            ax[i] = bx.GetAxis(i);
            sp[i] = bx.GetSpan(i);
        }
    }

    // Get values
    TPoint3<T> GetCenter() const    { return ct;    }
    TPoint3<T> GetAxis(int i) const { return ax[i]; }
    T GetSpan(int i) const { return sp[i]; }

    // Set values
    void SetCenter(const TPoint3<T>& c) { ct = c; }
    void SetCenter(T x, T y, T z) { ct.SetXYZ(x,y,z); }
    void SetAxis(const TPoint3<T>& ax0, const TPoint3<T>& ax1, const TPoint3<T>& ax2) {
        ax[0] = ax0;
        ax[1] = ax1;
        ax[2] = ax2;
    }
    void SetSpan(T s0, T s1, T s2) {
        sp[0] = s0;
        sp[1] = s1;
        sp[2] = s2;
    }
    void SetSpan(const TPoint3<T>& c) { SetSpan(c.x(), c.y(), c.z()); }

    // assignment
    TBox3& operator= (const TBox3& bx)
    {
        if( this == &bx )
            return *this;
        ct = bx.GetCenter();
        for(int i=0; i<3; i++)
        {
            ax[i] = bx.GetAxis(i);
            sp[i] = bx.GetSpan(i);
        }
        return *this;
    }

    // Get the corner point (index = 0..7)
    TPoint3<T> GetCorner(int index)
    {
        switch(index)
        {
        case 0: return ct - ax[2]*sp[2] - ax[1]*sp[1] - ax[0]*sp[0];
        case 1: return ct - ax[2]*sp[2] - ax[1]*sp[1] + ax[0]*sp[0];
        case 2: return ct - ax[2]*sp[2] + ax[1]*sp[1] + ax[0]*sp[0];
        case 3: return ct - ax[2]*sp[2] + ax[1]*sp[1] - ax[0]*sp[0];
        case 4: return ct + ax[2]*sp[2] - ax[1]*sp[1] - ax[0]*sp[0];
        case 5: return ct + ax[2]*sp[2] - ax[1]*sp[1] + ax[0]*sp[0];
        case 6: return ct + ax[2]*sp[2] + ax[1]*sp[1] + ax[0]*sp[0];
        default: return ct + ax[2]*sp[2] + ax[1]*sp[1] - ax[0]*sp[0];
        }
    }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TBox3<int>      Box3Int;
typedef TBox3<float>    Box3Float;
typedef TBox3<double>   Box3Double;

#endif //TBOX3_H
