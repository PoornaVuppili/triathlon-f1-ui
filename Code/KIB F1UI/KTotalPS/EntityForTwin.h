#pragma once
#include "..\KAppTotal\DataDoc.h"
#include "..\KAppTotal\Basics.h"
#include "..\KAppTotal\Entity.h"
///////////////////////////////////////////////////////////////////////////////////
// This KEntityForTwin is a specific purpose class for the entity roles which have
// _2 extension (see entity.h). Any entity be declared as KEntityForTwin is a 
// "twin" paired with a corresponding entity. The twin usually belongs to different 
// modules. The corresponding entity is a regular entity which stores all data. 
// However, the regular entity will NOT be added to entPanel (item list). Instead,
// the KEntityForTwin will be added to entPanel (item list). 
// When users interact with entPanel, KEntityForTwin will receive signals first.
// The singals and the infomation will then be sent to its twin regular entity.
// Doing so, the sequencein the entPanel (item list) can follow the preferred design 
// order, and the ordered design entities can belong to different modules.
///////////////////////////////////////////////////////////////////////////////////
class KEntityForTwin : public CEntity
{
public:

	KEntityForTwin( CDataDoc* pDoc, CEntRole eEntRole, const QString& sFileName = "", int id = -1 );
	virtual ~KEntityForTwin();

	virtual bool	Save( const QString& sDir, bool incrementalSave) {return true;};// the entity data members will be saved via DocWriter() to header.txt
	virtual bool	Load( const QString& sDir ) {return true;};// the entity data members will be loaded via DocReader() from header.txt
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&) {};
	virtual CBounds3d		GetBounds() {return CBounds3d();};
	virtual CBounds3d		ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL ) {return CBounds3d();};

};

