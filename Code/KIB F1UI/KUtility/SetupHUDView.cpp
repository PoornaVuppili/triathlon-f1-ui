#include <IwAxis2Placement.h>
#include <IwExtent3d.h>

#include "..\KApp\Viewport.h"
#include "SetupHUDView.h"

SetupHUDView::SetupHUDView(int wid, int hei, Viewport *vp, IwAxis2Placement *axes, IwExtent3d *box, bool defaultRotation)
{
	double xAng, yAng, zAng;
	w = wid; h = hei;

	vp->GetViewport(minX, maxX, minY, maxY);
	IwVector3d center = box->GetMid();

	IwPoint3d o;
	IwVector3d x, y;
	IwAxis2Placement placement(*axes);
	placement.DecomposeToAngles(xAng, yAng, zAng);

	transform.SetIdentity();

	transform.Translate( -Point3Float(center.x, center.y, center.z) );
	transform.RotateZ(-zAng*180/IW_PI);
	transform.RotateY(-yAng*180/IW_PI);
	transform.RotateX(-xAng*180/IW_PI);

	if(defaultRotation)
	{
		transform.RotateZ( 90.0 );
		transform.RotateX(-80.0 );
	}

	transform.Translate( Point3Float(center.x, center.y, center.z) );

    Point3Float ax[] = { Point3Float(max((box->GetMax().x-box->GetMin().x)*.5, 1.0), 0, 0), 
						 Point3Float(0, max((box->GetMax().y-box->GetMin().y)*.5, 1.0), 0), 
						 Point3Float(0, 0, max((box->GetMax().z-box->GetMin().z)*.5, 1.0))};

	Point3Float cen(center.x, center.y, center.z);
    Point3Float ptT = transform * cen;
    Point3Float boundTransformMin = ptT;
    Point3Float boundTransformMax = ptT;

    for(int i=-1; i<=1; i+=2)
    {
        Point3Float pz = cen + ax[2]*i;
        for(int j=-1; j<=1; j+=2)
        {
            Point3Float py = pz + ax[1]*j;
            for(int k=-1; k<=1; k+=2)
            {
                ptT = transform * (py + ax[0]*k);

                if( ptT.GetX() < boundTransformMin.GetX() )
                    boundTransformMin.SetX( ptT.GetX() );
                if( ptT.GetY() < boundTransformMin.GetY() )
                    boundTransformMin.SetY( ptT.GetY() );
                if( ptT.GetZ() < boundTransformMin.GetZ() )
                    boundTransformMin.SetZ( ptT.GetZ() );

                if( ptT.GetX() > boundTransformMax.GetX() )
                    boundTransformMax.SetX( ptT.GetX() );
                if( ptT.GetY() > boundTransformMax.GetY() )
                    boundTransformMax.SetY( ptT.GetY() );
                if( ptT.GetZ() > boundTransformMax.GetZ() )
                    boundTransformMax.SetZ( ptT.GetZ() );
            }
        }
    }

    float xs = boundTransformMax.GetX() - boundTransformMin.GetX();
    float ys = boundTransformMax.GetY() - boundTransformMin.GetY();
    float zs = boundTransformMax.GetZ() - boundTransformMin.GetZ();
	float expandRatio = .1f;

    if( xs<=0 )
    {
        left  = -1;
        right =  1;
    }
    else
    {
        xs *= expandRatio;
        left   = boundTransformMin.GetX() - xs;
        right  = boundTransformMax.GetX() + xs;
    }

    if( ys<=0 )
    {
        bottom = -1;
        top    =  1;
    }
    else
    {
        ys *= expandRatio;
        bottom = boundTransformMin.GetY() - ys;
        top    = boundTransformMax.GetY() + ys;
    }

    if( zs<=0 )
    {
        fa  = 1;
        ne = -1;
    }
    else
    {
        fa  = -(boundTransformMin.GetZ() - zs);
        ne  = -(boundTransformMax.GetZ() + zs);
    }

	_vp = vp;
}

void SetupHUDView::Zoom(int viewX, int viewY, float zoomRatio)
{
    float rx = ((float)viewX - minX) / (maxX-minX);
    float ry = ((float)viewY - minY) / (maxY-minY);

    float xs = right - left;
    float ys = top - bottom;

    float cx = left + xs * rx;
    float cy = bottom  + ys * ry;

    float xsr = xs * zoomRatio;
    float ysr = ys * zoomRatio;

    left  = cx - xsr * rx;
    right = cx + xsr * (1-rx);
    bottom= cy - ysr * ry;
    top   = cy + ysr * (1-ry);
}

IwPoint3d SetupHUDView::GetProjectedPointToViewport(IwPoint3d pt)
{
	IwPoint3d temp = transform*pt;

	IwPoint3d retValue;
	retValue.x = (double)((float)minX   + (temp.x - left) * (float)w / (right-left));
	retValue.y = (double)((float)h - ((float)minY + (temp.y - bottom ) * (float)h / (top-bottom)));
	retValue.z = 0.0;

    return retValue;
}

void SetupHUDView::AdjustAspectRatio()
{
    float viewWid = right - left;
    float viewHei = top - bottom;
    float viewAspectRatio = viewWid / viewHei;

    float winAspectRatio = w / h; // TODO: verify the intention here!!!  BOTH 'w' and 'h' are 'int'.  Should at least one of them be cast to 'float' for the division?

    if( viewAspectRatio > winAspectRatio )
    {
        float h1 = viewWid / winAspectRatio;
        float h2 = (h1 - viewHei) * .5;
        top += h2;
        bottom -= h2;
    }
    else if( viewAspectRatio < winAspectRatio )
    {
        float w1 = viewHei * winAspectRatio;
        float w2 = (w1 - viewWid) * .5;
        left -= w2;
        right += w2;
    }
}

void SetupHUDView::ApplyView()
{
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glPushMatrix();

	AdjustAspectRatio();

	glOrtho(left, right, bottom, top, ne, fa);
    glViewport(minX + 2, maxY - h - 2, w, h);

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glPushMatrix();

    float f[16];
    transform.GetGLMatrix(f);
    glMultMatrixf( f );
}

void SetupHUDView::RestoreView()
{
    glMatrixMode( GL_PROJECTION );
	glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
}

SetupHUDView::~SetupHUDView()
{
	RestoreView();
	_vp->GetViewport(minX, maxX, minY, maxY);
	glViewport(minX, maxX, minY, maxY);
}