#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;

enum CMakeInnerSurfaceJigsErrorCodes
{
	MAKE_INNER_SURFACE_JIGS_ERROR_CODES_NONE,
	MAKE_INNER_SURFACE_JIGS_ERROR_CODES_FLIP_NORMAL
};

struct CMakeInnerSurfaceJigsErrorCodesEntry
{
	CMakeInnerSurfaceJigsErrorCodes	ErrorCodes;
	QString								ErrorMessages;
};

class TEST_EXPORT_TW CMakeInnerSurfaceJigs : public CManager
{
    Q_OBJECT

public:
					CMakeInnerSurfaceJigs( CTotalView* pView, CManagerActivateType manActType=MAN_ACT_TYPE_EDIT );
	virtual			~CMakeInnerSurfaceJigs();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool	MouseMove  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );

	static void		CreateInnerSurfaceJigsObject(CTotalDoc* pDoc);
	static void		DetermineAnteriorDroppingSurface(CTotalDoc*& pDoc, IwTArray<IwSurface*>& totalSurfaces, bool bTimeTest, IwBrep*& antDropBrep);
	static void		DetermineContactSurface(CTotalDoc*& pDoc, IwTArray<IwSurface*>& totalSurfaces, double dropOffset, bool bAnteriorOnly, bool bAccurate, double* troGroAddThickness, double* transitionWidthRatio, IwTArray<IwBSplineCurve*>* jumpCurves, IwBrep*& contactBrep, IwBSplineSurface* referenceSurface=NULL, double referenceDistance=0);
	static void		DroppingCurve(CTotalDoc* pDoc, IwTArray<IwSurface*>& totalSurfaces, IwPoint3d& startPnt, IwVector3d& downVec, IwVector3d& outVec, double offsetDist, IwTArray<IwPoint3d>& dropPnts, IwBSplineCurve** dropCurve=NULL);
	static void		DroppingCurveWithJump(CTotalDoc* pDoc, IwTArray<IwSurface*>& totalSurfaces, IwPoint3d& startPnt, IwVector3d& downVec, IwVector3d& outVec, double offsetDist, IwBSplineCurve* jumpCurve, bool bAccurate, double extendAlongOutVec, IwTArray<IwPoint3d>& dropPnts, IwBSplineCurve** dropCurve=NULL, IwBSplineSurface* referenceSurface=NULL, double referenceDistance=0);
	static void		ValidateContactSurface(CTotalDoc*& pDoc, IwBrep*& contactBrep, bool& flipNormal, IwTArray<IwPoint3d>& flipPoints);
	static IwPoint3d	DetermineJumpPoint(CTotalDoc* pDoc, IwTArray<IwSurface*>& totalSurfaces, IwPoint3d& startPnt, IwVector3d& downVec, IwVector3d& outVec, double offsetDist, IwBSplineCurve* jumpCurve);

private:
	void			Reset(bool activateUI=true);
	static void		EvenContactPointsSpace(CTotalDoc*& pDoc, IwTArray<IwSurface*>& totalSurfaces, bool bAccurate, IwTArray<IwPoint3d>& contactPnts, int APSize, int MLSize, double interferenceCheckDistance);
	static bool		EvenAPScanSpace(CTotalDoc*& pDoc, IwTArray<IwSurface*>& totalSurfaces, bool bAccurate, double ratio, IwTArray<IwPoint3d>& APScanPnts, IwTArray<bool>& pointsMoved);
	static bool		EvenMLScanSpace(CTotalDoc*& pDoc, IwTArray<IwPoint3d>& MLScanPnts, IwTArray<IwPoint3d>& MLScanNormals);
	// for error codes
	void			AddErrorCodes(CMakeInnerSurfaceJigsErrorCodes errorCode){m_errorCodes.AddUnique(errorCode);};
	void			GetErrorCodeMessage(CMakeInnerSurfaceJigsErrorCodes& errorCode, QString& errorMsg);
	void			DisplayErrorMessages();

private slots:
	void			OnReset();
	void			OnAccept();
	void			OnCancel();
	void			OnDispAntDrop();
	void			OnDispSurface();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewBack();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();

protected:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;

	QAction*					m_actMakeInnerSurfaceJigs;

	QAction*					m_actReset;
	QAction*					m_actDispSurface;
	QAction*					m_actDispAntDrop;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewRework;

	int							m_predefinedViewIndex;
	bool						m_bDispSurface;
	bool						m_bDispAntDrop;

	// For error messages
	IwTArray<CMakeInnerSurfaceJigsErrorCodes>	m_errorCodes;
	static CMakeInnerSurfaceJigsErrorCodesEntry	MakeInnerSurfaceJigsErrorCodes[];
	
};
