////////////////////////////////////////////////////////////////
// Functions related to measurement entity selection
////////////////////////////////////////////////////////////////

#include "Measure.h"
#include "..\KUtility\SMLibInterface.h"
#include "..\KUtility\TIntersect.h"
#include "..\KUtility\TContour.h"
#pragma warning( disable : 4996 )
#include <IwLine.h>
#include <IwFace.h>
#include <IwTess.h>
#include <IwLoop.h>
#pragma warning( default : 4996 )

bool MeasureEntity::Select(IwBrep* brep, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    bool found = false;
    Point3Float rayCen = ray.GetRayCen();
    double dist,mindist=1000000;

    // point selection
    if( mask & SelectPoint )
    {
        IwTArray<IwVertex*> vertexes;
        brep->GetVertices(vertexes);

        MeasureEntity e;
		if( Select(vertexes, ray, tolerance, e, mask, EntToAvoid) )
        {
            dist = rayCen.Distance(e.GetHitPoint());
            if( dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = e;
            }
        }
    }

    // edge selection
    if( mask & (SelectCurve | SelectEdge) )
    {
        IwTArray<IwEdge*> edges;
        brep->GetEdges(edges);

        MeasureEntity e;
		if( Select(edges, ray, tolerance, e, mask, EntToAvoid) )
        {
            dist = rayCen.Distance(e.GetHitPoint());
            if( dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = e;
            }
        }
    }

    // opaque face selection
    if( mask & (SelectSurface | SelectFace) )
    {
        IwTArray<IwFace*> faces;
        brep->GetFaces(faces);

        MeasureEntity e;
		if( Select(faces, ray, tolerance, e, mask, EntToAvoid) )
        {
            dist = rayCen.Distance(e.GetHitPoint());
            if( dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = e;
            }
        }
    }

    // transparent face selection
    if( mask & SelectFaceTransparent )
    {
        IwTArray<IwFace*> faces;
        brep->GetFaces(faces);

        MeasureEntity e;
		if( Select(faces, ray, tolerance, e, mask, EntToAvoid) )
        {
            dist = rayCen.Distance(e.GetHitPoint());
            if( dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = e;
            }
        }
    }

    return found;
}

bool MeasureEntity::Select(const IwTArray<IwVertex*>& vertexes, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    if( !(mask & SelectPoint) )
        return false;

    IwPoint3d  rayCen = ray.GetRayCen();
    IwVector3d rayDir = ray.GetRayDir();
    Line3Float rayLine = ray.GetRayLine();

    bool found = false;
    double dist,mindist=1000000;
	MeasureEntity thisEnt;

    for (unsigned j=0; j<vertexes.GetSize(); j++)
	{
        IwPoint3d pnt = vertexes.GetAt(j)->GetPoint();
        if( rayLine.Distance(pnt) < tolerance )
        {
			thisEnt = pnt;
			if ( EntToAvoid && *EntToAvoid == thisEnt )
				continue;
            dist = rayCen.DistanceBetween(pnt);
            if( dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = thisEnt;
            }
        }
	}

    return found;
}

bool MeasureEntity::Select(const IwTArray<IwPoint3d>& points,  const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    if( !(mask & SelectPoint) )
        return false;

    IwPoint3d  rayCen = ray.GetRayCen();
    IwVector3d rayDir = ray.GetRayDir();
    Line3Float rayLine = ray.GetRayLine();

    bool found = false;
    double dist,mindist=1000000;
	MeasureEntity thisEnt;

    for (unsigned j=0; j<points.GetSize(); j++)
	{
        IwPoint3d pnt = points.GetAt(j);
        if( rayLine.Distance(pnt) < tolerance )
        {
 			thisEnt = pnt;
			if ( EntToAvoid && *EntToAvoid == thisEnt )
				continue;
            dist = rayCen.DistanceBetween(pnt);
            if( dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = thisEnt;
            }
        }
	}

    return found;
}

bool MeasureEntity::Select(const IwTArray<IwCurve*>& curves, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    if( !(mask & SelectCurve) )
        return false;

    IwPoint3d  rayCen = ray.GetRayCen();
    IwVector3d rayDir = ray.GetRayDir();

    IwExtent3d bBox;
    double twoTol = 2*tolerance;

    IwLine* hitLine = NULL;
	IwLine::CreateCanonical(*iwContext, rayCen, rayDir, hitLine);

    bool found = false;
    double dist,mindist=1000000;
	MeasureEntity thisEnt;

    for (unsigned i=0; i<curves.GetSize(); i++)
    {
        IwCurve* crv = curves.GetAt(i);

        // check the mask for linearity/nonlinearity
        bool linear = crv->IsLinear();
        if( ((mask & SelectLinearPlanar)    && !linear) ||
            ((mask & SelectNonLinearPlanar) &&  linear) )
            continue;

        // Curve's bounding box needs to intersects with cursor extension line, 
		// otherwise the curve is too far away to be picked. 
        crv->CalculateBoundingBox(crv->GetNaturalInterval(), &bBox);
		bBox.ExpandAbsolute(10*tolerance);

        if ( bBox.IntersectsLine3d(rayCen, rayDir) )
		{
			IwSolutionArray sols;
			crv->GlobalCurveSolve(crv->GetNaturalInterval(), *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			for (unsigned j=0; j<sols.GetSize(); j++)
			{
				IwSolution sol = sols[j];
				if (sol.m_vStart.m_dSolutionValue >= tolerance)
                    continue;

                IwPoint3d pnt;
				crv->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt);
				thisEnt = MeasureEntity(crv,sol.m_vStart.m_adParameters[0],pnt);
				if ( EntToAvoid && *EntToAvoid==thisEnt )
					continue;
                dist = rayCen.DistanceBetween(pnt);
                if( dist<mindist )
                {
                    found = true;
                    mindist = dist;
                    ent = thisEnt;
                }
			}
		}
    }

    IwObjDelete delLine(hitLine);
    return found;
}

bool MeasureEntity::Select(const IwTArray<IwEdge*>& edges, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    if( !(mask & SelectEdge) )
        return false;

    IwPoint3d  rayCen = ray.GetRayCen();
    IwVector3d rayDir = ray.GetRayDir();

    IwExtent3d bBox;
    double twoTol = 2*tolerance;

    IwLine* hitLine = NULL;
	IwLine::CreateCanonical(*iwContext, rayCen, rayDir, hitLine);

    bool found = false;
    double dist,mindist=1000000;
	MeasureEntity thisEnt;

    for (unsigned i=0; i<edges.GetSize(); i++)
    {
        IwEdge* edge = edges.GetAt(i);
        IwCurve* crv = edge->GetCurve();

        // check the mask for linearity/nonlinearity
        bool linear = crv->IsLinear();
        if( ((mask & SelectLinearPlanar)    && !linear) ||
            ((mask & SelectNonLinearPlanar) &&  linear) )
            continue;

        // Edge's bounding box needs to intersects with cursor extension line, 
		// otherwise the curve is too far away to be picked. 
        edge->CalculateBoundingBox(&bBox);
		bBox.ExpandAbsolute(10*tolerance);

        if ( bBox.IntersectsLine3d(rayCen, rayDir) )
		{
			IwSolutionArray sols;
			crv->GlobalCurveSolve(edge->GetInterval(), *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			for (unsigned j=0; j<sols.GetSize(); j++)
			{
				IwSolution sol = sols[j];
				if (sol.m_vStart.m_dSolutionValue >= tolerance)
                    continue;

                IwPoint3d pnt;
				crv->EvaluatePoint(sol.m_vStart.m_adParameters[0], pnt);
				thisEnt = MeasureEntity(edge,sol.m_vStart.m_adParameters[0],pnt);
				if ( EntToAvoid && *EntToAvoid==thisEnt )
					continue;
                dist = rayCen.DistanceBetween(pnt);
                if( dist<mindist )
                {
                    found = true;
                    mindist = dist;
                    ent = thisEnt;
                }
			}
		}
    }

    IwObjDelete delLine(hitLine);
    return found;
}

bool MeasureEntity::Select(const IwTArray<IwSurface*>& surfaces, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    if( !(mask & SelectSurface) )
        return false;

    IwPoint3d  rayCen = ray.GetRayCen();
    IwVector3d rayDir = ray.GetRayDir();

    IwExtent3d bBox;
    double twoTol = 2*tolerance;

    IwLine* hitLine = NULL;
	IwLine::CreateCanonical(*iwContext, rayCen, rayDir, hitLine);

    bool found = false;
    double dist,mindist=1000000;
	MeasureEntity thisEnt;

    for (unsigned i=0; i<surfaces.GetSize(); i++)
    {
        IwSurface* surf = surfaces.GetAt(i);

        // check the mask for planarity/nonplanarity
        bool planar = surf->IsPlanar();
        if( ((mask & SelectLinearPlanar)    && !planar) ||
            ((mask & SelectNonLinearPlanar) &&  planar) )
            continue;

        // Surface's bounding box needs to intersects with cursor extension line, 
		// otherwise the curve is too far away to be picked. 
        IwExtent2d uv = surf->GetNaturalUVDomain();
        surf->CalculateBoundingBox(uv, &bBox);
		bBox.ExpandAbsolute(10*tolerance);

        if ( bBox.IntersectsLine3d(rayCen, rayDir) )
		{
			IwSolutionArray sols;
			surf->GlobalCurveSolve(uv, *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			for (unsigned j=0; j<sols.GetSize(); j++)
			{
				IwSolution sol = sols[j];
				if (sol.m_vStart.m_dSolutionValue >= tolerance)
                    continue;

                IwVector2d hitUV(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
                IwPoint3d pnt;
				surf->EvaluatePoint(hitUV, pnt);
				thisEnt = MeasureEntity(surf, hitUV.x, hitUV.y, pnt);
				if ( EntToAvoid && *EntToAvoid==thisEnt )
					continue;
                dist = rayCen.DistanceBetween(pnt);
                if( dist<mindist )
                {
                    found = true;
                    mindist = dist;
                    ent = thisEnt;
                }
			}
		}
    }

    IwObjDelete delLine(hitLine);
    return found;
}

bool MeasureEntity::Select(const IwTArray<IwFace*>& faces, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask, MeasureEntity* EntToAvoid)
{
    if( !(mask & (SelectFace | SelectFaceTransparent)) )
        return false;

    IwPoint3d  rayCen = ray.GetRayCen();
    IwVector3d rayDir = ray.GetRayDir();

    IwExtent3d bBox;
    double twoTol = 2*tolerance;

    IwLine* hitLine = NULL;
	IwLine::CreateCanonical(*iwContext, rayCen, rayDir, hitLine);

    bool found = false;
    double dist,mindist=1000000;
	MeasureEntity thisEnt;

    for (unsigned i=0; i<faces.GetSize(); i++)
    {
        IwFace* face = faces.GetAt(i);
        IwSurface* surf = face->GetSurface();

        // check the mask for planarity/nonplanarity
        bool planar = surf->IsPlanar();
        if( ((mask & SelectLinearPlanar)    && !planar) ||
            ((mask & SelectNonLinearPlanar) &&  planar) )
            continue;

        // Face's bounding box needs to intersects with cursor extension line, 
		// otherwise the curve is too far away to be picked. 
        face->CalculateBoundingBox(bBox);
		bBox.ExpandAbsolute(10*tolerance);

        if ( bBox.IntersectsLine3d(rayCen, rayDir) )
		{
			IwSolutionArray sols;
			surf->GlobalCurveSolve(surf->GetNaturalUVDomain(), *hitLine, hitLine->GetNaturalInterval(), IW_SO_MINIMIZE, 0.001, &twoTol, NULL, IW_SR_ALL, sols);
			for (unsigned j=0; j<sols.GetSize(); j++)
			{
				IwSolution sol = sols[j];
				if (sol.m_vStart.m_dSolutionValue >= tolerance)
                    continue;

                IwVector2d hitUV(sol.m_vStart.m_adParameters[1], sol.m_vStart.m_adParameters[2]);
                IwPointClassification pClass;
				face->PointClassify(hitUV, FALSE, TRUE, pClass);
				if (pClass.GetPointClass() == IW_PC_FACE)
				{
                    IwPoint3d pnt;
					surf->EvaluatePoint(hitUV, pnt);
					thisEnt = MeasureEntity(face, hitUV.x, hitUV.y, pnt);
					if ( EntToAvoid && *EntToAvoid==thisEnt )
						continue;
                    dist = rayCen.DistanceBetween(pnt);
                    if( dist<mindist )
                    {
                        found = true;
                        mindist = dist;
                        ent = thisEnt;
                    }
                }
			}
		}
    }

    IwObjDelete delLine(hitLine);
    return found;
}

// select 1 point
bool MeasureEntity::Select(const Point3Float& p1, 
    const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask)
{
    if( (mask & SelectPoint) && ray.GetRayLine().Distance(p1) < tolerance )
    {
        ent = p1;
        return true;
    }
    return false;
}

// select 2 points or line segment between 2 points
bool MeasureEntity::Select(const Point3Float& p1, const Point3Float& p2, 
    const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask)
{
    bool found = false;
    float dist,mindist=0;

    Line3Float line = ray.GetRayLine();
    Point3Float ct = ray.GetRayCen();

    if( mask & SelectPoint )
    {
        if( line.Distance(p1) < tolerance )
        {
            dist = p1.Distance(ct);
            if( !found || dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = p1;
            }
        }
        if( line.Distance(p2) < tolerance )
        {
            dist = p2.Distance(ct);
            if( !found || dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = p2;
            }
        }
        if( found )
            return true;
    }
    if( mask & SelectLine )
    {
        Point3Float cp;
        LineSegment3Float ls(p1,p2);
        if( Intersect(ls, line, cp, tolerance)==IntersectFlag::Intersect )
        {
            dist = cp.Distance(ct);
            if( !found || dist<mindist )
            {
                found = true;
                mindist = dist;
                ent = MeasureEntity(ls,cp);
            }
        }
    }
    return found;
}

// select 4 points, 4 line segments, or planar rectangle/parallelogram defined by 3 points
//  p0 --- p1         p0 --- p1
//   |     |     or    \      \
//  p2-----             p2-----
bool MeasureEntity::Select(const Point3Float& p1, const Point3Float& p2, const Point3Float& p3, 
    const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask)
{
    bool found = false;
    float dist,mindist=0;

    Line3Float line = ray.GetRayLine();
    Point3Float ct = ray.GetRayCen();

    Point3Float p[4] = {p1, p2, p2-p1+p3, p3};

    if(mask & SelectPoint)
    {
        for(int i=0; i<4; i++)
        {
            if( line.Distance(p[i]) < tolerance )
            {
                dist = p[i].Distance(ct);
                if( !found || dist<mindist )
                {
                    found = true;
                    mindist = dist;
                    ent = p[i];
                }
            }
        }
        if( found )
            return true;
    }
    if( mask & SelectLine )
    {
        int j=3;
        for(int i=0; i<4; i++)
        {
            Point3Float cp;
            LineSegment3Float ls(p[j],p[i]);
            if( Intersect(ls, line, cp, tolerance)==IntersectFlag::Intersect )
            {
                dist = cp.Distance(ct);
                if( !found || dist<mindist )
                {
                    found = true;
                    mindist = dist;
                    ent = MeasureEntity(ls,cp);
                }
            }
            j = i;
        }
        if( found )
            return true;
    }
    if( mask & SelectPlane )
    {
        Vector3Float nor = (p3-p1).cross(p2-p1).normalized();
        Point3Float cp;
        if( Intersect(Plane3Float(p1, nor), line, cp)==IntersectFlag::Intersect )
        {
            // check if inside
            ContourFloat contour;
            contour.SetCapacity(4);
            int i = 0;
            int j = 1; // drop z
            if( fabs(nor.x()) > fabs(nor.y()) )
            {
                if( fabs(nor.x()) > fabs(nor.z()) )
                {
                    // drop x
                    i = 1;
                    j = 2;
                }
            }
            else if( fabs(nor.y()) > fabs(nor.z()) )
            {
                // drop y
                i = 0;
                j = 2;
            }
            contour.Append( p[0][i], p[0][j] );
            contour.Append( p[1][i], p[1][j] );
            contour.Append( p[2][i], p[2][j] );
            contour.Append( p[3][i], p[3][j] );
            if( contour.IsInternal(cp[i],cp[j]) )
            {
                found = true;
                ent = MeasureEntity(p1,p2,p3,cp);
            }
        }
    }

    return found;
}

