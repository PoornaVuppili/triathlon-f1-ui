///////////////////////////////////////////////////////////////////////
//	File:    PointSet3.h
//           A 3D point set.
//	Author:	 Tong Zhang
//  Copyright 2008 Stryker, Inc.
///////////////////////////////////////////////////////////////////////

#ifndef POINTSET3_H
#define POINTSET3_H

#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
using namespace std;

#include "TPoint3.h"

// PointSet3 class
template<typename T> class PointSet3
{
protected:
    vector<T> vertex;  // x1,y1,z3,x2,y2,z3 ...

public:
    // Constructor
    PointSet3() {}

    // Destructor
    ~PointSet3() {}

    // Get the number of vertices
    size_t GetLength() const { return (vertex.size()/3); } 

    // Get the capacity of the buffer
    size_t GetCapacity() const { return vertex.capacity()/3; }

    // Set the capacity of the buffer
    void SetCapacity(int s) { vertex.reserve(s*3); }

    // Get the x coordinate of the specified coordinate index
    T  X(int p) const { return vertex[(p<<1)+p]; }

    // Get the y coordinate of the specified coordinate index
    T  Y(int p) const { return vertex[(p<<1)+p+1]; }

    // Get the z coordinate of the specified coordinate index
    T  Z(int p) const { return vertex[(p<<1)+p+2]; }

    // Set the x,y,z coordinate of the specified coordinate index
    void  SetXYZ(int p, T x, T y, T z)  { vertex[(p<<1)+p] = x; vertex[(p<<1)+p+1] = y; vertex[(p<<1)+p+2] = z;}

    // Comparison
    bool operator== (const PointSet3<T>& ps) const
    {
        int n = GetLength();
        if( n != ps.GetLength() )
            return false;
        for(int i=0; i<n; i++)
            if( X(i) != ps.X(i) || Y(i) != ps.Y(i) || Z(i) != ps.Z(i) )
                return false;
        return true;
    }

    bool operator!= (const PointSet3<T>& ps) const
    {
        int n = GetLength();
        if( n != ps.GetLength() )
            return true;
        for(int i=0; i<n; i++)
            if( X(i) != ps.X(i) || Y(i) != ps.Y(i) || Z(i) != ps.Z(i) )
                return true;
        return false;
    }

    // Get the pointer to the vertice coordinates: [x1,y1,x2,y2,x3,y3 ...
    T* GetVertices() { return vertex.size()==0 ? NULL : & vertex[0]; }

    // Append a new vertex at the end of the contour
    void Append(T x,T y, T z)
    {
        vertex.push_back( x );
        vertex.push_back( y );
        vertex.push_back( z );
    }

    void Append(TPoint3<T>& p)
    {
        vertex.push_back(p.GetX());
        vertex.push_back(p.GetY());
        vertex.push_back(p.GetZ());
    }

    // Insert a new vertex at the specified location
    void Insert(int p,T x,T y,T z)
    {
        int p3 = (p << 1)+p;
        vertex.insert( vertex.begin()+p3, 3, x);
        Y(p) = y;
        Z(p) = z;
    }

    // Remove a vertex at the specified location
    void Remove(int p)
    {
        int p3 = (p << 1)+p;
        vertex.erase(vertex.begin()+p3, vertex.begin()+p3+3);
    }

    // Resize the vertex buffer
    void Resize(int n)
    {
        vertex.resize((n<<1)+n);
    }

    // Empty the vertex buffer
    void Clear()
    {
        vertex.clear();
    }

    // Save to a file
    bool ToFile(const char* fileName)
    {
        ofstream output(fileName);
        if ( output.fail() )
            return false;
        output << *this;
        return true;
    }

    // Load from a file
    bool FromFile(const char* fileName)
    {
        ifstream input(fileName);
        if ( input.fail() )
            return false;
        input >> *this;
        return true;
    }
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

// A point set of integer coordinates
typedef PointSet3<int> PointSet3Int;

// A point set of float coordinates
typedef PointSet3<float> PointSet3Float;

// A point set of double coordinates
typedef PointSet3<double> PointSet3Double;


//////////////////////////////////////////
// Implementations
//////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Function name:    operator>>
// Function purpose: Input the content of a pointset from a stream
//   composed of several lines of comma-separated x, y coordinates
// Input:
//   output: the ostream reference
//   pt: a reference to a PointSet object
// Output:
//   the ostream reference
///////////////////////////////////////////////////////////////////////
template <typename T> istream& operator>>(istream& input, PointSet3<T>& ps)
{
    string str,str1,str2,str3;
    T x,y,z;
    ps.Clear();
    while( getline(input, str) )
    {
        int p = str.find(',');
        str1 = str.substr(0, p);
        str = str.substr(p+1);

        p = str.find(',');
        str2 = str.substr(0, p);
        str3 = str.substr(p+1);

        stringstream s1(str1);
        stringstream s2(str2);
        stringstream s3(str3);
        s1 >> x;
        s2 >> y;
        s3 >> z;
        ps.Append(x,y,z);
    }
    return input;
}


///////////////////////////////////////////////////////////////////////
// Function name:    operator<<
// Function purpose: Output the content of the point set to a stream
//   as several lines of comma-separated x, y coordinates
// Input:
//   output: the ostream reference
//   pt: a const reference to a PointSet object
// Output:
//   the ostream reference
///////////////////////////////////////////////////////////////////////
template <typename T> ostream& operator<<(ostream& output, const PointSet3<T>& ps)
{
    for(int i=0; i<ps.GetLength(); i++)
        output << ps.X(i) << ',' << ps.Y(i) << ',' << ps.Z(i) << std::endl;
    return output;
}

#endif //POINTSET3_H

