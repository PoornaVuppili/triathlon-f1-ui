#include <windows.h>
#include <QtGui>
#include "RangeSlider.h"


CRangeSlider::CRangeSlider( QWidget *parent ) : QWidget( parent )
	{
	QSize		sizeParent = parent->size();

	setGeometry( 0, 0, sizeParent.width(), 22 );

	QSize		size = this->size();

	m_width = size.width();
	m_height = size.height();

	m_left			= 0.0;
	m_right			= 1.0;

	m_nButtonWidth	= m_height * 3 / 4;
	m_eTrackMode	= TRACK_NONE;

	m_min			= m_nButtonWidth;
	m_max			= m_width - m_nButtonWidth;
	m_dx			= m_max - m_min;

	connect( this, SIGNAL( sigRangeChanged( double, double ) ), parent, SLOT( OnSliderRangeChange( double, double ) ) );
	}


CRangeSlider::~CRangeSlider()
	{
	}


void CRangeSlider::paintEvent( QPaintEvent* event )
	{
	bool		bLeftDeflate, bRightDeflate;
	QRect		rect;

	m_pPainter = new QPainter( this );

	m_left_x1 = m_min + m_left * m_dx;
	m_left_x0 = m_left_x1 - m_nButtonWidth;

	m_right_x0 = m_min + m_right * m_dx;
	m_right_x1 = m_right_x0 + m_nButtonWidth;

	bLeftDeflate = ( m_eTrackMode != TRACK_LEFT );
	bRightDeflate = ( m_eTrackMode != TRACK_RIGHT );

	rect.setTop(0);
	rect.setBottom( m_height );

	// Left zone
	m_pPainter->setPen( Qt::black );
	m_pPainter->setBrush( Qt::white );
	rect.setLeft( 0 );
	rect.setRight( m_left_x0 );
	m_pPainter->drawRect( rect );

	// Left button
	rect.setLeft( m_left_x0 );
	rect.setRight( m_left_x1 );
	DrawButton( rect, bLeftDeflate, true );	

	// Middle zone
	m_pPainter->setPen( Qt::black );
	m_pPainter->setBrush( Qt::darkBlue );
	rect.setLeft( m_left_x1 + 1 );
	rect.setRight( m_right_x0 - 1 );
	m_pPainter->drawRect( rect );

	// Rigt button
	rect.setLeft( m_right_x0 );
	rect.setRight( m_right_x1 );
	DrawButton( rect, bRightDeflate, false );	

	// Right zone
	m_pPainter->setPen( Qt::black );
	m_pPainter->setBrush( Qt::white );
	rect.setLeft( m_right_x1 );
	rect.setRight( m_width );
	m_pPainter->drawRect( rect );

	delete m_pPainter;
	}


void CRangeSlider::DrawButton( QRect rect, bool bDeflate, bool bLeft )
	{
	int			l, r, t, b, x0, y0, x1, y1;
	QPoint		ptsArrow[3];

	l = rect.left();
	r = rect.right();
	t = rect.top();
	b = rect.bottom();

	QBrush		brushButton = palette().brush( QPalette::Button );
	QColor		colorHighlight = Qt::white;
	QColor		colorShadow = Qt::lightGray;

	m_pPainter->setPen( Qt::black );
	m_pPainter->setBrush( brushButton );

	m_pPainter->drawRect( rect );

	m_pPainter->drawLine( l, b, r, b );
	m_pPainter->drawLine( r, b, r, t );
	m_pPainter->drawLine( r, t, l, t );
	m_pPainter->drawLine( l, t, l, b );

	if( bDeflate )
		{
		x0 = l + 1;
		x1 = r - 1;
		y0 = t + 1;
		y1 = b - 1;

		m_pPainter->setPen( colorHighlight );		

		m_pPainter->drawLine( x0, y0, x0, y1 );
		m_pPainter->drawLine( x0, y0, x1, y0 );

		m_pPainter->setPen( colorShadow );		

		m_pPainter->drawLine( x0, y1, x1, y1 );
		m_pPainter->drawLine( x1, y0, x1, y1 );
		}

	m_pPainter->setPen( Qt::black );
	m_pPainter->setBrush( Qt::black );

	if( bLeft )
		{
		ptsArrow[0] = QPoint( ( 2 * l + r ) / 3, ( 2 * b + t ) / 3 );
		ptsArrow[1] = QPoint( ( l + 3 * r ) / 4, ( b + t ) / 2 );
		ptsArrow[2] = QPoint( ( 2 * l + r ) / 3, ( b + 2 * t ) / 3 );
		}
	else
		{
		ptsArrow[0] = QPoint( ( l + 3 * r ) / 4, ( 2 * b + t ) / 3 );
		ptsArrow[1] = QPoint( ( 2 * l + r ) / 3, ( b + t ) / 2 );
		ptsArrow[2] = QPoint( ( l + 3 * r ) / 4, ( b + 2 * t ) / 3 );
		}

	m_pPainter->drawConvexPolygon( ptsArrow, 3 );
	}


void CRangeSlider::mousePressEvent( QMouseEvent *event )
	{
	int		x = event->pos().x();

	if( event->button() == Qt::LeftButton )
		{
		if( m_left_x0 <= x && x <= m_left_x1 )
			{
			m_eTrackMode = TRACK_LEFT;
			}

		if( m_left_x1 < x && x < m_right_x0 )
			{
			m_eTrackMode = TRACK_MIDDLE;
			}

		if( m_right_x0 <= x && x <= m_right_x1 )
			{
			m_eTrackMode = TRACK_RIGHT;
			}

		m_xLast = x;
		repaint( 0, 0, m_width, m_height );
		}
	}


void CRangeSlider::mouseReleaseEvent( QMouseEvent *event )
	{
	if( event->button() == Qt::LeftButton )
		{
		m_eTrackMode = TRACK_NONE;
		}

	repaint( 0, 0, m_width, m_height );
	}


void CRangeSlider::mouseMoveEvent( QMouseEvent *event )
	{
	int		x = event->pos().x();

	if( m_eTrackMode == TRACK_LEFT )
		{
		m_left += 1.0 * ( x - m_xLast ) / m_dx;
		m_left = std::min( m_left, m_right );
		m_left = std::max( m_left, 0.0 );
		}

	if( m_eTrackMode == TRACK_MIDDLE )
		{
		double	d = m_right - m_left;

		if( x < m_xLast )
			{
			m_left += 1.0 * ( x - m_xLast ) / m_dx;
			m_left = std::min( m_left, m_right );
			m_left = std::max( m_left, 0.0 );

			m_right = m_left + d;
			}
		else
			{
			m_right += 1.0 * ( x - m_xLast ) / m_dx;
			m_right = std::max( m_right, m_left );
			m_right = std::min( m_right, 1.0 );

			m_left = m_right - d;
			}
		}

	if( m_eTrackMode == TRACK_RIGHT )
		{
		m_right += 1.0 * ( x - m_xLast ) / m_dx;
		m_right = std::max( m_right, m_left );
		m_right = std::min( m_right, 1.0 );
		}

	m_xLast = x;
	repaint( 0, 0, m_width, m_height );

	emit sigRangeChanged( m_left, m_right );
	}


void CRangeSlider::SetRange( double left, double right )
	{
	m_left = left;
	m_right = right;
	}
