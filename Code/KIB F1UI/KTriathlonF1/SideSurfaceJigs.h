#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW CSideSurfaceJigs : public CPart
{
public:
	CSideSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_SIDE_SURFACE_JIGS, const QString& sFileName="", int nEntId=-1 );
	~CSideSurfaceJigs();

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );

private:
	void			SetSSJigsModifiedFlag( bool bModified );

private:
	bool			m_bSSJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bSSJigsDataModified;// Geometric entities have not been saved yet to the file. 

};