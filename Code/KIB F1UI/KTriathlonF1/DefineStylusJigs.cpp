#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "DefineStylusJigs.h"
#include "StylusJigs.h"
#include "FemoralPart.h"
#include "CartilageSurface.h"
#include "OutlineProfileJigs.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "SolidPositionJigs.h"
#include "MakeSolidPositionJigs.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "UndoRedoCmdJigs.h"
#include "AdvancedControlJigs.h"
#include "..\KApp\SwitchBoard.h"
#pragma warning( disable : 4996 4805 )
//#include <IwFilletExecutive.h>
//#include <IwFilletStandardSolver.h>
#pragma warning( default : 4996 4805 )

/////////////////////////////////////////////////////////////////////////////////////////
// The parameters in DefineStylusJigs are related to the stylus start part in SolidWorks.
// The TipPosition is the basis reference, which is the intersection point between
// the outer edge and the sagittal plane. 
// The stylusControlPosition is a point projected from TipPosition onto the stylus top face 
// (the most-y face). The control points are displayed there or 
// related to stylusControlPosition if StylusDelta applies. 
// The TipPosition and stylusControlPosition are fixed after Reset().
// The stylus cross section x-length is 5mm, y-length is 7mm. The default z-length from
// TipPosition to the stylus end is 22mm.
// The stylus cross section profile (rectangle) location is defined related to the TipPosition.
// Its x-position is centered to the TipPosition.
// Its y-position is defined by a "stylus height" which is from the stylus rectangle bottom
// (minimum y) to the TipPosition, i.e., "stylus height" = 7.0 - distance(TipPosition,stylusControlPosition)
// However, the Reset() has determined a proper stylusControlPosition 
// such that the stylus has 2mm clearance with osteophyte. 
// The StylusDelta defines the additional shifts/lengths along x, y, and z directions.
// The delta x and delta y will NOT affect the location of stylusControlPosition. 
/////////////////////////////////////////////////////////////////////////////////////////

CDefineStylusJigsErrorCodesEntry CDefineStylusJigs::DefineStylusJigsErrorCodes[] = 
{
	{ DEFINE_STYLUS_JIGS_ERROR_CODES_NONE,	"None."	},
	{ DEFINE_STYLUS_JIGS_ERROR_CODES_NOT_RELIABLE,		"Stylus length and clearance is not reliable.\nPlease proceed manual check."	}
};

CDefineStylusJigs::CDefineStylusJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
    EnableUndoStack(true);

	bool activateUI = (manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CDefineStylusJigs";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actUpdate = NULL;
	m_actCancel = NULL;
	m_actValidate = NULL;

	m_stylusJigsUpToDate = true;
	m_leftButtonDown = false;
	m_pickedRadius = 0;
	m_keyUpDown = false;
	m_currGapBetweenStylusToOuterSurface = 0.1;
	m_predefinedViewIndex = 0;

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Define Stylus");
		m_actDefineStylusJigs = m_toolbar->addAction("Stylus");
		SetActionAsTitle( m_actDefineStylusJigs );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );
		m_actUpdate = m_toolbar->addAction("Update");
		EnableAction( m_actUpdate, false );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");// No cancel 
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		m_pMainWindow->connect( m_actUpdate, SIGNAL( triggered() ), this, SLOT( OnUpdate() ) );
		m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );
	}
	else if (m_manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Define Stylus");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );

		m_actDefineStylusJigs = m_toolbar->addAction("Stylus");
		SetActionAsTitle( m_actDefineStylusJigs );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		m_pMainWindow->connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		m_pMainWindow->connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		m_pMainWindow->connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	CStylusJigs* stylusJigs = m_pDoc->GetStylusJigs();
	if ( stylusJigs == NULL ) // if no object 
	{
		if ( stylusJigs == NULL )
		{
			stylusJigs = new CStylusJigs( m_pDoc );
			m_pDoc->AddEntity( stylusJigs, true );
		}

		// Reset is the way to define the Stylus Jigs in default situation.
		Reset( activateUI );
	}
	else
	{
		// Load the parameters
		stylusJigs->GetStylusPosition(m_currStylusControlPosition, m_currTipPosition, m_currStylusDelta, m_currGapBetweenStylusToOuterSurface);
		stylusJigs->GetStylusPosition(m_oldStylusControlPosition, m_oldTipPosition, m_oldStylusDelta, m_oldGapBetweenStylusToOuterSurface);
		stylusJigs->GetFilletPointsRadii(m_currFilletPoints, m_currFilletRadii);
		stylusJigs->GetFilletPointsRadii(m_oldFilletPoints, m_oldFilletRadii);
		this->DetermineStylusCtrlPoints(m_stylusCtrlPoints);
		IwTArray<IwPoint3d> totalPoints;
		totalPoints.Append(m_currFilletPoints);
		totalPoints.Append(m_stylusCtrlPoints);
		totalPoints.RemoveLast();// the last ctrl point is not selectable

		SetMouseMoveSearchingList(totalPoints);

		SetStylusJigsUpToDate(true);

		if ( !stylusJigs->GetUpToDateStatus() )
			Update(activateUI, true); 

		OnValidate();
	}

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	// If any errors, display error messages.
	DisplayErrorMessages();

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

	m_pView->Redraw();
}

CDefineStylusJigs::~CDefineStylusJigs()
{
	CStylusJigs* stylusJigs = m_pDoc->GetStylusJigs();
	if ( stylusJigs != NULL )
	{
		int previoisDesignTime = stylusJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		stylusJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}
	SwitchBoard::removeSignalSender(this);
}

void CDefineStylusJigs::CreateStylusJigsObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetStylusJigs() == NULL )
	{
		CStylusJigs* pStylusJigs = new CStylusJigs(pDoc);
		pDoc->AddEntity(pStylusJigs, true);
	}

	return;
}

void CDefineStylusJigs::Display(Viewport* vp)
{		
	IwVector3d viewVec = m_pView->GetViewingVector();

	glDisable( GL_LIGHTING );

	IwPoint3d caughtPnt;
	int caughtIndex;
	if ( GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
	{
		if (m_leftButtonDown)
		{
			glPointSize(7);
			glColor3ub( 255, 255, 255 );
		}
		else
		{
			glPointSize(6);
			glColor3ub( 255, 255, 0 );
		}

		if ( caughtIndex == 6 && !m_pDoc->GetAdvancedControlJigs()->GetStylusJigsFullControl() )
		{
			// Do not display caught point
		}
		else
		{
			DisplayCaughtPoint(-50*viewVec);
		}

		if ( caughtIndex < 6 )
		{
			glColor3ub( 0, 0, 0 );
			QString str, totalStr;
			if ( m_leftButtonDown )
				str.setNum(m_pickedRadius, 'f', 2);
			else
				str.setNum(m_currFilletRadii.GetAt(caughtIndex), 'f', 2);
			totalStr = QString(" R: ") + str;
			QFont font( "Tahoma", 12);
			font.setBold(true);
			IwPoint3d pnt = m_currFilletPoints.GetAt(caughtIndex);
			pnt -= 20*viewVec;
			GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
		}
		else if ( caughtIndex < 9 )// 6, 7, 8 are stylus ctrl points
		{
			glColor3ub( 0, 0, 0 );
			QString deltaStr, str, totalStr;
			double stepSize = 0.1, delta;
			IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
			IwVector3d vec;
			if ( caughtIndex == 6 )
			{
				if ( m_pDoc->GetAdvancedControlJigs()->GetStylusJigsFullControl() )
				{
					vec = caughtPnt - m_currStylusControlPosition;
					delta = vec.Dot(wslAxes.GetXAxis());
					delta = stepSize*floor(delta/stepSize+0.5)-1.0;// round to nearest 0.1
					deltaStr = QString(" DeltaX: ");
					str.setNum(delta, 'f', 2);
					totalStr = deltaStr + str;
					QFont font( "Tahoma", 12);
					font.setBold(true);
					IwPoint3d pnt = caughtPnt - 20*viewVec;
					GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
				}
			}
			else if ( caughtIndex == 7 )
			{
				vec = caughtPnt - m_currStylusControlPosition;
				delta = vec.Dot(wslAxes.GetYAxis());
				delta = stepSize*floor(delta/stepSize+0.5)-1.0;// round to nearest 0.1
				deltaStr = QString(" DeltaY: ");
				str.setNum(delta, 'f', 2);
				totalStr = deltaStr + str;
				QFont font( "Tahoma", 12);
				font.setBold(true);
				IwPoint3d pnt = caughtPnt - 20*viewVec;
				GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
			}
			else if ( caughtIndex == 8 )
			{
				vec = caughtPnt - m_currStylusControlPosition;
				delta = vec.Dot(wslAxes.GetZAxis())-12;// 12 = 22-10
				delta = stepSize*floor(delta/stepSize+0.5);// round to nearest 0.1
				deltaStr = QString(" DeltaZ: ");
				str.setNum(delta, 'f', 2);
				totalStr = deltaStr + str;
				QFont font( "Tahoma", 12);
				font.setBold(true);
				IwPoint3d pnt = caughtPnt - 20*viewVec;
				GetViewWidget()->renderText(pnt.x, pnt.y, pnt.z, totalStr, font);
			}
		}
	}

	//////////////////////////////////
	glPointSize(6);
	glLineWidth(2);
	IwPoint3d pnt, pnt0, pnt1, pnt2, pnt3;
	// Display stylus ctrl points
	if ( m_stylusCtrlPoints.GetSize() == 4 )
	{
		pnt0 = m_stylusCtrlPoints.GetAt(0) -50*viewVec;
		pnt1 = m_stylusCtrlPoints.GetAt(1) -50*viewVec;
		pnt2 = m_stylusCtrlPoints.GetAt(2) -50*viewVec;
		pnt3 = m_stylusCtrlPoints.GetAt(3) -50*viewVec;
		if ( m_pDoc->GetAdvancedControlJigs()->GetStylusJigsFullControl() )
		{
			glColor3ub( 255, 0, 0 );
			glBegin( GL_POINTS );
				glVertex3d( pnt0.x, pnt0.y, pnt0.z );
			glEnd();
			glBegin( GL_LINES );
				glVertex3d( pnt0.x, pnt0.y, pnt0.z );
				glVertex3d( pnt3.x, pnt3.y, pnt3.z );
			glEnd();
		}
		glColor3ub( 0, 255, 0 );
		glBegin( GL_POINTS );
			glVertex3d( pnt1.x, pnt1.y, pnt1.z );
		glEnd();
		glBegin( GL_LINES );
			glVertex3d( pnt1.x, pnt1.y, pnt1.z );
			glVertex3d( pnt3.x, pnt3.y, pnt3.z );
		glEnd();
		glColor3ub( 0, 0, 255 );
		glBegin( GL_POINTS );
			glVertex3d( pnt2.x, pnt2.y, pnt2.z );
		glEnd();
		glBegin( GL_LINES );
			glVertex3d( pnt2.x, pnt2.y, pnt2.z );
			glVertex3d( pnt3.x, pnt3.y, pnt3.z );
		glEnd();
		glColor3ub( 0, 0, 0 );
		glBegin( GL_POINTS );
			glVertex3d( pnt3.x, pnt3.y, pnt3.z );
		glEnd();
	}
	// display fillet points
	glColor3ub( 0, 0, 0 );
	glBegin( GL_POINTS );

		for (unsigned i=0; i<m_currFilletPoints.GetSize(); i++)
		{
			pnt = m_currFilletPoints.GetAt(i) -50*viewVec;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}

	glEnd();

	glPointSize(1);
	glLineWidth(1);
	glEnable( GL_LIGHTING );


}

void CDefineStylusJigs::OnReset()
{
	Reset();

	// If any errors, display error messages.
	DisplayErrorMessages();
}

void CDefineStylusJigs::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CDefineStylusJigs::Reset()") );

	// Initialize stylus default location and delta
	double deltaZ=0;
	bool reliable = DetermineStylusPositionNLength(m_currStylusControlPosition, m_currTipPosition, deltaZ);
	if ( !reliable )
	{
		AddErrorCodes(DEFINE_STYLUS_JIGS_ERROR_CODES_NOT_RELIABLE);
	}
	m_currStylusDelta = IwPoint3d(0,0,deltaZ);

	// Initialize fillet radii.
	double initRadius = 3.0;
	m_currFilletRadii.RemoveAll();
	for (int i=0; i<6; i++)
	{
		m_currFilletRadii.Add(initRadius);
	}
	m_currFilletRadii.SetAt(0, 2.5);// Set the first one (the most anterior corner) to be 2.5mm.

	Update(activateUI, true);

	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_STYLUS_JIGS, m_validateStatus, m_validateMessage);
	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_STYLUS_JIGS, false, false); // therefore, Stylus Jigs become out of date

}

void CDefineStylusJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CDefineStylusJigs::OnAccept()") );

	if (m_pDoc->GetAdvancedControlJigs()->GetStylusJigsDisplayVirtualFeaturesOnly())
	{

	}
	else
	{
		if ( !m_stylusJigsUpToDate )
		{
			int		button;
			button = QMessageBox::warning( NULL, 
											tr("Warning Message!"), 
											tr("Stylus Jigs has to be updated before exiting.\n"), 
											QMessageBox::Ok );	

			if( button == QMessageBox::Ok ) return;
		}
	}


	CStylusJigs* pStylusJigs = m_pDoc->GetStylusJigs();

	// Need to check IsMemberDataChanged()
	if ( pStylusJigs )
	{
		if ( !pStylusJigs->GetUpToDateStatus() || pStylusJigs->IsMemberDataChanged(m_oldStylusControlPosition, m_oldStylusDelta, m_oldGapBetweenStylusToOuterSurface, m_oldFilletRadii) )
		{
			m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_STYLUS_JIGS, true, true);
		}
	}

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	if ( pStylusJigs )
	{
		if ( m_validateStatus == VALIDATE_STATUS_NOT_VALIDATE ) // use runtime m_validateStatus, rather than stylus->GetValidateStatus()
		{
			OnValidate();
		}
	}

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CDefineStylusJigs::OnUpdate()
{
	m_pDoc->AppendLog( QString("CDefineStylusJigs::OnUpdate()") );

	Update();

	OnValidate();
}

//////////////////////////////////////////////////////////////////////////
// This function update stylus, fillet points, and actual/virtual fillets.
void CDefineStylusJigs::Update(bool activateUI, bool updateStylusPosition)
{
	m_pDoc->AppendLog( QString("CDefineStylusJigs::Update()") );

	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if ( pOutlineProfileJigs == NULL )
		return;

	CStylusJigs* pStylusJigs = m_pDoc->GetStylusJigs();
	if ( pStylusJigs == NULL )
		return;

	CSolidPositionJigs *pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();
	if ( pSolidPositionJigs == NULL )
		return;

	QApplication::setOverrideCursor( Qt::WaitCursor );

	IwBrep* oldBrep = pStylusJigs->GetIwBrep();
	if ( oldBrep )
	{
		IwObjDelete(oldBrep);
	}

	IwBrep* solidPositionJigsBrep = pSolidPositionJigs->GetIwBrep();
	IwBrep* stylusJigsBrep = new (m_pDoc->GetIwContext()) IwBrep(*solidPositionJigsBrep);
	stylusJigsBrep->SewAndOrient();

	// if the stylus jig is out-of-date, we need to re-determine the StylusControlPosition and TipPosition
	if (updateStylusPosition)
	{
		double deltaZ=0;
		DetermineStylusPositionNLength(m_currStylusControlPosition, m_currTipPosition, deltaZ);
		// We do not update delta z here.
	}
	////
	IwTArray<IwEdge*> filletEdges;
	CMakeSolidPositionJigs::DetermineTabFilletEdges(m_pDoc, stylusJigsBrep, filletEdges, m_currFilletPoints);

	IwTArray<IwCurve*> virtualStylus;
	IwTArray<IwCurve*> virtualFillets;
	bool gotStylus=false;
	// if create virtual fillets
	if (m_pDoc->GetAdvancedControlJigs()->GetStylusJigsDisplayVirtualFeaturesOnly())
	{
		DoVirtualStylus(m_currStylusControlPosition, m_currStylusDelta, virtualStylus);
		DoVirtualFilleting(stylusJigsBrep, filletEdges, m_currFilletRadii, virtualFillets );
	}
	else// if create solid Fillets
	{
		gotStylus = DoStylus(stylusJigsBrep, m_currStylusControlPosition, m_currStylusDelta);
		if ( !gotStylus )
		{
			DoVirtualStylus(m_currStylusControlPosition, m_currStylusDelta, virtualStylus);
			DoVirtualFilleting(stylusJigsBrep, filletEdges, m_currFilletRadii, virtualFillets );
		}
	}

	pStylusJigs->SetStylusPosition(m_currStylusControlPosition, m_currTipPosition, m_currStylusDelta, m_currGapBetweenStylusToOuterSurface);
	pStylusJigs->SetFilletPointsRadii( m_currFilletPoints, m_currFilletRadii);
	pStylusJigs->SetVirtualStylusNFillets( virtualStylus, virtualFillets );
	pStylusJigs->SetIwBrep(stylusJigsBrep);
	pStylusJigs->MakeTess();

	//
	DetermineStylusCtrlPoints(m_stylusCtrlPoints);

	IwTArray<IwPoint3d> totalPnts;
	totalPnts.Append(m_currFilletPoints);
	totalPnts.Append(m_stylusCtrlPoints);
	totalPnts.RemoveLast();// the last ctrl point is not selectable
	SetMouseMoveSearchingList(totalPnts);

	QApplication::restoreOverrideCursor();

	SetStylusJigsUpToDate(true);

	m_pView->Redraw();

}

void CDefineStylusJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CDefineStylusJigs::OnCancel()") );

	if (m_pDoc->GetAdvancedControlJigs()->GetStylusJigsDisplayVirtualFeaturesOnly())
	{
		CStylusJigs* pStylusJigs = m_pDoc->GetStylusJigs();
		pStylusJigs->SetFilletPointsRadii(m_oldFilletPoints, m_oldFilletRadii);
	}
	else
	{
		int		button;
		button = QMessageBox::warning( NULL, 
										tr("Warning Message!"), 
										tr("Cancel is not possible. Geometry has to be updated before exiting.\n"), 
										QMessageBox::Ok );	

		if( button == QMessageBox::Ok ) return;
	}

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CDefineStylusJigs::OnValidate()
{
	if ( (m_manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW) && !m_stylusJigsUpToDate )
	{
		int button = QMessageBox::warning( NULL, 
										tr("Warning Message!"), 
										tr("Stylus Jigs has to be updated before validating.\n"), 
										QMessageBox::Ok );	
		if( button == QMessageBox::Ok ) return;
	}

	m_validateStatus = Validate(m_pDoc, m_validateMessage);
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_STYLUS_JIGS, m_validateStatus, m_validateMessage);
	m_pDoc->GetStylusJigs()->SetValidateStatus(m_validateStatus, m_validateMessage);
}

///////////////////////////////////////////////////////////////////////
// This function validates users settings.
///////////////////////////////////////////////////////////////////////
CEntValidateStatus CDefineStylusJigs::Validate
(
	CTotalDoc* pDoc,						// I:
	QString &message						// O: error messages
)
{
	pDoc->AppendLog( QString("CDefineStylusJigs::Validate()") );

	pDoc->AppendValidationReportJigsEntry("\n");
	pDoc->AppendValidationReportJigsEntry("**** Stylus Jig ****");

	// initialize data
	message.clear();
	int vStatus = 0; 

	if ( pDoc->GetValidateTableStatus("STYLUS JIGS STYLUS CLEARANCE STATUS") != -1 ||
		 pDoc->GetValidateTableStatus("STYLUS JIGS STYLUS CONTACT STATUS") != -1 )
	{
		// Check clearance here.
		double stylusContactClearance, stylusArmClearance;
		bool reliable = DetermineStylusClearance(pDoc, stylusContactClearance, stylusArmClearance);
		stylusContactClearance = floor(100*stylusContactClearance+0.5)/100.0;
		stylusArmClearance = floor(100*stylusArmClearance+0.5)/100.0;
		if ( reliable )
		{
			if ( pDoc->GetValidateTableStatus("STYLUS JIGS STYLUS CONTACT STATUS") != -1 )
			{
				if (stylusContactClearance > 0.50)
				{
					pDoc->AppendValidateMessage( QString("STYLUS JIGS STYLUS CONTACT"), vStatus, message );
					message += QString(" A gap: %1 mm. ").arg(stylusContactClearance, 0, 'f', 2); 
					pDoc->AppendValidationReportJigsEntry( QString("Fail: Stylus tip does not contact femur with a gap: %1 mm").arg(stylusContactClearance), false );
				}
				else
				{
					pDoc->AppendValidationReportJigsEntry( QString("Pass: Stylus tip contacts femur (gap <= 0.5 mm): %1 mm").arg(stylusContactClearance), true );
				}
			}
			if (pDoc->GetValidateTableStatus("STYLUS JIGS STYLUS CLEARANCE STATUS") != -1)
			{
				if ( stylusArmClearance < 2.00 )
				{
					pDoc->AppendValidateMessage( QString("STYLUS JIGS STYLUS CLEARANCE"), vStatus, message );
					message += QString(" Arm clearance: %1 mm. ").arg(stylusArmClearance, 0, 'f', 2); 
					pDoc->AppendValidationReportJigsEntry( QString("Fail: Stylus arm clearance (>= 2.0mm) is not valid: %1 mm").arg(stylusArmClearance), false );
				}
				else
				{
					pDoc->AppendValidationReportJigsEntry( QString("Pass: Stylus arm clearance (>= 2.0mm) is valid: %1 mm").arg(stylusArmClearance), true );
				}
			}
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for stylus clearance and contact.");	
			vStatus = max(vStatus, 2);
			pDoc->AppendValidationReportJigsEntry( QString("Fail: Software validation is not reliable. Please proceed manual checking for stylus clearance and contact."), false );
		}
	}

	if ( vStatus == 0 )
	{
		if ( message.isEmpty() )
			message = QString("All pass.");
		else
			message += QString("\nAll pass.");
	}

	return (CEntValidateStatus)vStatus;
}

void CDefineStylusJigs::SetValidateIcon(CEntValidateStatus vStatus, QString message)
{
	if ( m_actValidate )
	{
		m_actValidate->setToolTip( message );
		if ( vStatus == VALIDATE_STATUS_RED )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Red.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_YELLOW )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Yellow.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_GREEN )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Green.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			m_actValidate->setToolTip( QString("Not validate.") );
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate.png" ) );
		}
	}
}

bool CDefineStylusJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	bool continuousResponse = true;
    m_posStart = cursor;
	m_leftButtonDown = true;

	IwPoint3d caughtPoint;
	int caughtIndex;
	bool gotCaughtPoint = GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);
	
	if ( gotCaughtPoint )
	{
		m_pickedRadius = m_currFilletRadii.GetAt(caughtIndex);
		continuousResponse = false;
	}

	if (m_manActType == MAN_ACT_TYPE_REVIEW) 
	{
		m_leftButtonDown = false;// no response to MouseUp if under review
	}

	ReDraw();

	return continuousResponse;
}

bool CDefineStylusJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // 
	{
		// if the clicked-down is the same as the button-up point, change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}
	else if ( m_leftButtonDown )
	{
		IwPoint3d caughtPoint;
		int caughtIndex;
		bool gotCaughtPoint = GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);

		//// Update /////////////////
		if ( gotCaughtPoint )
		{
			CDefineStylusJigsUndoData prevUndoData, postUndoData;
			// Get the undo data before any changes
			GetDefineStylusJigsUndoData(prevUndoData);

			// update fillet radii
			if ( caughtIndex < 6 ) 
			{
				m_currFilletRadii.SetAt(caughtIndex, m_pickedRadius); // set radius
			}
			else if ( caughtIndex < 9 ) // 6, 7, 8 are the stylus ctrl points. Update the m_currStylusDelta
			{
				double stepSize = 0.1;
				IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
				IwVector3d vec;
				if ( caughtIndex == 6 )
				{
					vec = caughtPoint - m_currStylusControlPosition;
					double deltaX = vec.Dot(wslAxes.GetXAxis());
					deltaX = stepSize*floor(deltaX/stepSize+0.5);// round to nearest 0.1
					m_currStylusDelta.x = deltaX-1.0;// Remember, there is always 1mm between x,y ctrl points and m_currStylusControlPosition
				}
				else if ( caughtIndex == 7 )
				{
					vec = caughtPoint - m_currStylusControlPosition;
					double deltaY = vec.Dot(wslAxes.GetYAxis());
					deltaY = stepSize*floor(deltaY/stepSize+0.5);// round to nearest 0.1
					m_currStylusDelta.y = deltaY-1.0;// Remember, there is always 1mm between x,y ctrl points and m_currStylusControlPosition
				}
				else if ( caughtIndex == 8 )
				{
					vec = caughtPoint - m_currStylusControlPosition;
					double deltaZ = vec.Dot(wslAxes.GetZAxis())-12;// 12 = 22-10
					deltaZ = stepSize*floor(deltaZ/stepSize+0.5);// round to nearest 0.1
					m_currStylusDelta.z = deltaZ;
				}
			}

			// Get the undo data after changes
			GetDefineStylusJigsUndoData(postUndoData);

			QUndoCommand *defineStylusCmd = new DefineStylusJigsCmd( this, prevUndoData, postUndoData);
			undoStack->push( defineStylusCmd );
		}
	}

	m_leftButtonDown = false;
	m_keyUpDown = false;

	ReDraw();

	return true;
}

bool CDefineStylusJigs::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	bool continuousResponse = true;
	IwPoint3d caughtPoint;
	int caughtIndex;
	bool gotCaughtPoint = GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);
	if (m_leftButtonDown)
	{
		IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
		if ( gotCaughtPoint	)
		{
			if ( caughtIndex == 6 ) // move along x-axis
			{
				if ( m_pDoc->GetAdvancedControlJigs()->GetStylusJigsFullControl() )
				{
					this->MoveCaughtPoint(cursor, wslAxes.GetXAxis());
				}
			}
			else if ( caughtIndex == 7 ) // move along y-axis
			{
				this->MoveCaughtPoint(cursor, wslAxes.GetYAxis());
			}
			else if ( caughtIndex == 8 ) // move along Z-axis
			{
				this->MoveCaughtPoint(cursor, wslAxes.GetZAxis());
			}
			continuousResponse = false;
		}
		m_posLast = cursor;
	}
	else
	{
		// call the parent's MouseMove();
		CManager::MouseMove(cursor, keyModifier);
		if ( caughtIndex == 6 ) 
		{
			if ( !m_pDoc->GetAdvancedControlJigs()->GetStylusJigsFullControl() )
			{
				RemoveMouseMoveCaughtPoint();
			}
		}
	}

	ReDraw();

	return continuousResponse;
}

bool CDefineStylusJigs::keyPressEvent( QKeyEvent* event, Viewport* vp )
{
	bool continueResponse = true;

	switch (event->key())
	{
		case Qt::Key_Up:
			{
				IwPoint3d caughtPnt;
				int caughtIndex;
				if ( m_leftButtonDown && GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
				{
					double radiusIncrement = 0.10;
					m_pickedRadius += radiusIncrement;
					if ( m_pickedRadius > 6.0 )
						m_pickedRadius = 6.0;

					continueResponse = false;// return false because we do not want to continue response the subsequent keyPressEvent actions
					m_keyUpDown = true;
				}
			}
			break;

		case Qt::Key_Down:
			{
				IwPoint3d caughtPnt;
				int caughtIndex;
				if ( m_leftButtonDown && GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
				{
					double radiusIncrement = 0.10;
					m_pickedRadius -= radiusIncrement;
					if ( m_pickedRadius < 0.25 )
						m_pickedRadius = 0.25;

					continueResponse = false;// return false because we do not want to continue response the subsequent keyPressEvent actions
					m_keyUpDown = true;
				}
			}
			break;

		case Qt::Key_Left:
		case Qt::Key_Right:
			if ( m_leftButtonDown )
				continueResponse = false; // return false because we do not want to continue response the subsequent keyPressEvent actions
			break;

		default:
			break;

	}

	ReDraw();

	return continueResponse;
}

void CDefineStylusJigs::GetDefineStylusJigsUndoData
(
	CDefineStylusJigsUndoData& undoData		// O:
)
{
	undoData.filletRadii.RemoveAll();

	undoData.filletRadii.Append(m_currFilletRadii);
	undoData.stylusDelta = m_currStylusDelta;
}


void CDefineStylusJigs::SetDefineStylusJigsUndoData
(
	CDefineStylusJigsUndoData& undoData	// O:
)
{
	m_currFilletRadii.RemoveAll();
	m_currFilletRadii.Append(undoData.filletRadii);

	m_currStylusDelta = undoData.stylusDelta;

	CStylusJigs*		pStylusJigs = m_pDoc->GetStylusJigs();

	IwTArray<IwCurve*> virtualStylus, virtualFillets;
	if (m_pDoc->GetAdvancedControlJigs()->GetStylusJigsDisplayVirtualFeaturesOnly())
	{
		CSolidPositionJigs *pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();
		if ( pSolidPositionJigs == NULL )
			return;
		IwBrep* positionJigsBrep = pSolidPositionJigs->GetIwBrep();
		// Update stylus jigs info
		IwTArray<IwEdge*> filletEdges;
		IwTArray<IwPoint3d> filletPoints;
		CMakeSolidPositionJigs::DetermineTabFilletEdges(m_pDoc, positionJigsBrep, filletEdges, filletPoints);
		// create virtual fillets
		DoVirtualStylus(m_currStylusControlPosition, m_currStylusDelta, virtualStylus);
		DoVirtualFilleting( positionJigsBrep, filletEdges, m_currFilletRadii, virtualFillets );
		pStylusJigs->SetStylusPosition(m_currStylusControlPosition, m_currTipPosition, m_currStylusDelta, m_currGapBetweenStylusToOuterSurface);
		pStylusJigs->SetFilletPointsRadii( m_currFilletPoints, m_currFilletRadii);
		pStylusJigs->SetVirtualStylusNFillets( virtualStylus, virtualFillets );
	}
	else// if create solid Fillets
	{
		// create virtual fillets
		DoVirtualStylus(m_currStylusControlPosition, m_currStylusDelta, virtualStylus);
		pStylusJigs->SetStylusPosition(m_currStylusControlPosition, m_currTipPosition, m_currStylusDelta, m_currGapBetweenStylusToOuterSurface);
		pStylusJigs->SetFilletPointsRadii( m_currFilletPoints, m_currFilletRadii);
		pStylusJigs->SetVirtualStylusNFillets( virtualStylus, virtualFillets );
		SetStylusJigsUpToDate(false);
	}

	//
	this->DetermineStylusCtrlPoints(m_stylusCtrlPoints);

	IwTArray<IwPoint3d> totalPoints;
	totalPoints.Append(m_currFilletPoints);// Actually they never change.
	totalPoints.Append(m_stylusCtrlPoints);//
	totalPoints.RemoveLast();// the last ctrl point is not selectable

	SetMouseMoveSearchingList(totalPoints);

	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_STYLUS_JIGS, m_validateStatus, m_validateMessage);

	m_pView->Redraw();
}

void CDefineStylusJigs::SetStylusJigsUpToDate( bool flag )
{
	m_stylusJigsUpToDate = flag;

	if ( m_stylusJigsUpToDate )
	{
		EnableAction( m_actUpdate, false );
	}
	else
	{
		EnableAction( m_actUpdate, true );
	}

}

bool CDefineStylusJigs::DoTabFilleting
(
	IwBrep*& filletBrep,			// I/O:
	IwTArray<IwEdge*>& filletEdges, // I:
	IwTArray<double>& filletRadii,	// I:
	bool bSkipAntTab				// I:
)
{
	return false;

	//m_pDoc->AppendLog( QString("CDefineStylusJigs::DoTabFilleting()") );

	//IwContext& iwContext = m_pDoc->GetIwContext();

	//bool isSolid = filletBrep->IsManifoldSolid();

	//IwTArray<IwEdge*> prevEdges;
	//filletBrep->GetEdges(prevEdges);

	//////////////////////////////////////////////////////////
	//IwCircularCrossSectionFSG sFSG(TRUE);
	//IwMakeSurfaceBlendSIH sSIH(1.0);

	//IwFilletExecutive* pFilExec = new (iwContext) IwFilletExecutive(iwContext, filletBrep);
	//pFilExec->SetSelfIntersectionHandler(&sSIH);
	//pFilExec->SetDoGlobalMerge(FALSE);

	//unsigned startIndex = 0;
	//if ( bSkipAntTab )
	//	startIndex = 2;
	//for ( unsigned i=startIndex; i<filletEdges.GetSize(); i++ )
	//{
	//	IwEdgeuse* pEU = filletEdges.GetAt(i)->GetPrimaryEdgeuse();

	//	IwConstantRadiusFS* sFS = new (iwContext) IwConstantRadiusFS(iwContext, 0.05, 25.0*IW_PI/180.0, 15.0*IW_PI/180.0, filletRadii.GetAt(i), pEU);

	//	sFS->SetFilletSurfaceGenerator(&sFSG);

	//	pFilExec->LoadFilletSolver(sFS);

	//}

	//m_pDoc->AppendLog( QString("pFilExec->DoFilleting(), starting ...") );

	//pFilExec->CreateFilletCorners();

	//pFilExec->DoFilleting();

	//m_pDoc->AppendLog( QString("pFilExec->DoFilleting(), delete pFilExec.") );

	//delete pFilExec;

	//m_pDoc->AppendLog( QString("pFilExec->DoFilleting(), finished.") );

	//IwTArray<IwEdge*> postEdges;
	//filletBrep->GetEdges(postEdges);

	//if ( postEdges.GetSize() == prevEdges.GetSize() )
	//	return false;
	//else
	//	return true;
}

bool CDefineStylusJigs::DoVirtualFilleting
(
	IwBrep*& stylusJigsBrep,				// I:
	IwTArray<IwEdge*>& filletEdges,			// I:
	IwTArray<double>& filletRadii,			// I:
	IwTArray<IwCurve*>& filletArcs			// O:
)
{
	// Get inner surface jigs
	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if (pInnerSurfaceJigs==NULL)
		return false;
	IwBrep* innerBrep = pInnerSurfaceJigs->GetIwBrep();
	if ( innerBrep == NULL )
		return false;
	IwTArray<IwFace*> faces;
	innerBrep->GetFaces(faces);
	IwSurface* innerSurf = faces.GetAt(0)->GetSurface();

	// Get outer surface jigs
	COuterSurfaceJigs* pOuterSurfaceJigs = m_pDoc->GetOuterSurfaceJigs();
	if (pOuterSurfaceJigs==NULL)
		return false;
	IwBrep* outerBrep = pOuterSurfaceJigs->GetIwBrep();
	if ( outerBrep == NULL )
		return false;
	outerBrep->GetFaces(faces);
	IwSurface* outerSurf = faces.GetAt(0)->GetSurface();

	/////////////////////////////////////////////
	IwEdge* edge;
	IwTArray<IwFace*> adjacentFaces;
	IwSurface* surf0, *surf1;
	IwBSplineSurface* offSurf0, *offSurf1;
	IwTArray<IwSurface*> offSurfaces;
	IwTArray<IwCurve*> intCrvs;
	IwCurve* intCrv;
	IwPoint3d sPnt, ePnt, midPnt;
	IwVector3d edgeVec;
	IwTArray<IwVertex*> vertices;
	IwPoint2d param2d;
	IwBSplineCurve* filletArc;
	IwTArray<IwCurve*> projectedCurves;
	IwCurve* projectedCurve;
	IwFace *face0, *face1;
	IwFaceuse *faceuse0, *faceuse1;
	IwOrientationType oType;
	double offsetDist;
	for (unsigned i=0; i<filletEdges.GetSize(); i++)
	{
		// get edge info
		edge = filletEdges.GetAt(i);
		edge->GetVertices(vertices);
		sPnt = vertices.GetAt(0)->GetPoint();
		ePnt = vertices.GetAt(1)->GetPoint();
		edgeVec = ePnt - sPnt;
		// get adjacent faces
		edge->GetFaces(adjacentFaces);
		// offset adjacent surfaces
		face0 = adjacentFaces.GetAt(0);
		face0->GetFaceuses(faceuse0, faceuse1);
		oType = faceuse0->GetOrientation();
		if ( oType = IW_OT_SAME )
			offsetDist = filletRadii.GetAt(i);
		else
			offsetDist = -filletRadii.GetAt(i);
		surf0 = face0->GetSurface();
		surf0->CreateOffsetSurface(m_pDoc->GetIwContext(), offsetDist, 0.01, offSurfaces);
		offSurf0 = (IwBSplineSurface*)offSurfaces.GetAt(0);
		//
		face1 = adjacentFaces.GetAt(1);
		face1->GetFaceuses(faceuse0, faceuse1);
		oType = faceuse0->GetOrientation();
		if ( oType = IW_OT_SAME )
			offsetDist = filletRadii.GetAt(i);
		else
			offsetDist = -filletRadii.GetAt(i);
		surf1 = face1->GetSurface();
		surf1->CreateOffsetSurface(m_pDoc->GetIwContext(), offsetDist, 0.01, offSurfaces);
		offSurf1 = (IwBSplineSurface*)offSurfaces.GetAt(0);

		// Intersect both offset surfaces
		IntersectSurfaces(offSurf0, offSurf1, intCrvs, 0.001, 0.01);
		if ( intCrvs.GetSize() > 0 )
		{
			intCrv = intCrvs.GetAt(0);
			intCrv->EvaluatePoint(intCrv->GetNaturalInterval().Evaluate(0.5), midPnt);
			DistFromPointToSurface(midPnt, (IwBSplineSurface*)surf0, sPnt, param2d);
			DistFromPointToSurface(midPnt, (IwBSplineSurface*)surf1, ePnt, param2d);
			IwCircle::CreateArcFromPoints(m_pDoc->GetIwContext(), 3, midPnt, sPnt, ePnt, IW_CO_QUADRATIC, filletArc);
			// project onto inner surface
			innerSurf->CreateParallelProjectionCurves(m_pDoc->GetIwContext(), innerSurf->GetNaturalUVDomain(), *filletArc, edgeVec, NULL, NULL, NULL, &projectedCurves, NULL);
			projectedCurve = projectedCurves.GetAt(0);
			filletArcs.Add(projectedCurve);
			// project onto outerer surface
			projectedCurves.RemoveAll();
			outerSurf->CreateParallelProjectionCurves(m_pDoc->GetIwContext(), outerSurf->GetNaturalUVDomain(), *filletArc, edgeVec, NULL, NULL, NULL, &projectedCurves, NULL);
			projectedCurve = projectedCurves.GetAt(0);
			filletArcs.Add(projectedCurve);
			//
			if (filletArc)
				IwObjDelete(filletArc);
		}
		else
		{
			//Do nothing.
		}
		// delete objects
		if (offSurf0)
			IwObjDelete(offSurf0);
		if (offSurf1)
			IwObjDelete(offSurf1);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////
// This function determines the stylus default position. tipPosition is 
// the intersection point between the outer face edge and the sagittal plane.
// stylusControlPosition is the projected point of tipPosition onto stylus top face.
// deltaLength to control stylus tip contact on non-osteophyte shaft.
// This function will ensure no gap between stylus and outer surface and 
// at least 2mm clearance between the stylus and the osteophyte surface.
bool CDefineStylusJigs::DetermineStylusPositionNLength
(
	IwPoint3d& stylusControlPosition,	// O: the projected point on stylus top face. 
	IwPoint3d& tipPosition,				// O: the intersection point between the outer face edge and the sagittal plane.
	double& deltaLength					// O: 22mm+deltaLength to ensure stylus tip contact on non-osteophyte shaft.
)
{
	stylusControlPosition = IwPoint3d();// make it uninitialized.

	double defaultStylusLength = 22.0;
	double stylusWidth = 5.0;
	double maxDeviation = 0.50 - 0.05; // 0.05 as allowance

	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	IwBSplineSurface *femurMainSurface, *femurLeftSurface, *femurRightSurface;
	IFemurImplant::FemoralPart_GetSurfaces(false, femurMainSurface, femurLeftSurface, femurRightSurface);

	CFemoralPart *pOsteoSurface = m_pDoc->GetOsteophyteSurface();
	if ( pOsteoSurface==NULL)
		return false;
	IwBSplineSurface *osteoMainSurface = pOsteoSurface->GetSinglePatchSurface();

	CCartilageSurface* pCartiSurface = m_pDoc->GetCartilageSurface();
	if ( pCartiSurface == NULL )
		return false;
	IwBSplineSurface* cartiMainSurface = pCartiSurface->GetSinglePatchSurface();

	COutlineProfileJigs* pOutlineProfileJigs = m_pDoc->GetOutlineProfileJigs();
	if (pOutlineProfileJigs==NULL)
		return false;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//// tipPosition is the intersection point between the outer face edge and the sagittal plane ////
	IwFace* outerFace = CMakeSolidPositionJigs::GetMainFace(m_pDoc, false);
	if ( outerFace == NULL )
		return false;
	IwTArray<IwEdge*> edges;
	IwEdge* edge;
	IwCurve* crv;
	IwPoint3d cPnt3d;
	IwPoint3d cPnt;
	double param, dist, minDist = HUGE_DOUBLE;
	IwPoint3d targetPnt = wslAxes.GetOrigin() + 100*wslAxes.GetYAxis();
	IwPoint3d antTip;
	IwExtent1d edgeDom;
	outerFace->GetEdges(edges);
	bool gotIt;
	for (unsigned i=0; i<edges.GetSize(); i++)
	{
		edge = edges.GetAt(i);
		crv = edge->GetCurve();
		edgeDom = edge->GetInterval();
		gotIt = IntersectCurveByPlane(crv, targetPnt, wslAxes.GetXAxis(), cPnt3d, &param);
		if ( gotIt )
		{
			cPnt = cPnt3d;
			if ( edgeDom.ContainsValue(param) )// cPnt is in the edge
			{
				dist = cPnt.DistanceBetween(targetPnt);
				if ( dist < minDist )
				{
					antTip = cPnt;
					minDist = dist;
				}
			}
		}
	}
	tipPosition = antTip;

	///////////////////////////////////////////////////////////////////////////////////
	// deltaLength - looking for the non-osteophyte area
	// The default stylus z length is 22mm. The stylus "foot" is 7mm. 
	// We here are looking for the region from deltaLength=-defaultStylusLength+(stylusFootLength+6.5) 
	// to the shaft end where the deviation between the femur and osteophyte is less than 0.5mm.
	deltaLength = 0.0; // initialize
	double stylusFootLength = 7.0;
	IwPoint3d scanPnt0, scanPnt1, scanPnt2, scanPnt3, scanPnt4;
	IwPoint3d femurHitPnt0, femurHitPnt1, femurHitPnt2;
	IwPoint3d osteoHitPnt0, osteoHitPnt1, osteoHitPnt2, osteoHitPnt3, osteoHitPnt4;
	IwPoint3d cartiHitPnt0, cartiHitPnt1,cartiHitPnt2, cartiHitPnt3,cartiHitPnt4;
	double dist0, dist1, dist2, maxDist;
	double osteoDist0, osteoDist1, osteoDist2, osteoDist3, osteoDist4;
	double cartiDist0, cartiDist1, cartiDist2, cartiDist3, cartiDist4;
	double osteoMinDist, cartiMinDist;
	IwTArray<double> osteoFemurDistances;// For stylus contact (deltaLength determination)
	IwTArray<double> osteoCartiDistances;// For stylus clearance (stylusControlPosition determination)
	bool hit, foundRegion=false;
static int showDeviation = 0;
	for (double d=-(defaultStylusLength-1); d<50; d++)// "d=-(defaultStylusLength-1)", actually we scan almost from TipPosition
	{
		scanPnt0 = tipPosition + 100.0*wslAxes.GetYAxis() + (defaultStylusLength+d)*wslAxes.GetZAxis();// Note we move 100mm along y direction.
		scanPnt1 = scanPnt0 - 0.5*stylusWidth*wslAxes.GetXAxis();
		scanPnt2 = scanPnt0 + 0.5*stylusWidth*wslAxes.GetXAxis();
		scanPnt3 = scanPnt0 - 0.75*stylusWidth*wslAxes.GetXAxis(); // scan a little bit wider
		scanPnt4 = scanPnt0 + 0.75*stylusWidth*wslAxes.GetXAxis(); // scan a little bit wider
		// Intersect with femurMainSurface
		hit =        IntersectSurfaceByLine(femurMainSurface, scanPnt0, -wslAxes.GetYAxis(), femurHitPnt0);
		hit = hit && IntersectSurfaceByLine(femurMainSurface, scanPnt1, -wslAxes.GetYAxis(), femurHitPnt1);
		hit = hit && IntersectSurfaceByLine(femurMainSurface, scanPnt2, -wslAxes.GetYAxis(), femurHitPnt2);
		// Intersect with osteoMainSurface
		hit = hit && IntersectSurfaceByLine(osteoMainSurface, scanPnt0, -wslAxes.GetYAxis(), osteoHitPnt0);
		hit = hit && IntersectSurfaceByLine(osteoMainSurface, scanPnt1, -wslAxes.GetYAxis(), osteoHitPnt1);
		hit = hit && IntersectSurfaceByLine(osteoMainSurface, scanPnt2, -wslAxes.GetYAxis(), osteoHitPnt2);
		hit = hit && IntersectSurfaceByLine(osteoMainSurface, scanPnt3, -wslAxes.GetYAxis(), osteoHitPnt3);
		hit = hit && IntersectSurfaceByLine(osteoMainSurface, scanPnt4, -wslAxes.GetYAxis(), osteoHitPnt4);
		// Intersect with cartiMainSurface
		hit = hit && IntersectSurfaceByLine(cartiMainSurface, scanPnt0, -wslAxes.GetYAxis(), cartiHitPnt0);
		hit = hit && IntersectSurfaceByLine(cartiMainSurface, scanPnt1, -wslAxes.GetYAxis(), cartiHitPnt1);
		hit = hit && IntersectSurfaceByLine(cartiMainSurface, scanPnt2, -wslAxes.GetYAxis(), cartiHitPnt2);
		hit = hit && IntersectSurfaceByLine(cartiMainSurface, scanPnt3, -wslAxes.GetYAxis(), cartiHitPnt3);
		hit = hit && IntersectSurfaceByLine(cartiMainSurface, scanPnt4, -wslAxes.GetYAxis(), cartiHitPnt4);
		// whether already hit the shaft end
		if ( !hit )
			break;
		// Determine the distance between femurMainSurface & osteoMainSurface
		dist0 = femurHitPnt0.DistanceBetween(osteoHitPnt0);
		dist1 = femurHitPnt1.DistanceBetween(osteoHitPnt1);
		dist2 = femurHitPnt2.DistanceBetween(osteoHitPnt2);
		maxDist = max(dist0, max(dist1,dist2));
		osteoFemurDistances.Add(maxDist); // for stylus contact (deltaLength) determination 
if ( showDeviation )
{
if ( dist0 > maxDeviation )
	ShowPoint(m_pDoc, osteoHitPnt0, red);
if ( dist1 > maxDeviation )
	ShowPoint(m_pDoc, osteoHitPnt1, red);
if ( dist2 > maxDeviation )
	ShowPoint(m_pDoc, osteoHitPnt2, red);
}
		// Determine the distance between osteoMainSurface & scanPnt0/1/2/3/4
		osteoDist0 = osteoHitPnt0.DistanceBetween(scanPnt0);
		osteoDist1 = osteoHitPnt1.DistanceBetween(scanPnt1);
		osteoDist2 = osteoHitPnt2.DistanceBetween(scanPnt2);
		osteoDist3 = osteoHitPnt3.DistanceBetween(scanPnt3);
		osteoDist4 = osteoHitPnt4.DistanceBetween(scanPnt4);
		osteoMinDist = min(osteoDist0, min(osteoDist1, min(osteoDist2, min(osteoDist3, osteoDist4)))) - 100.0;// "-100.0" to compensate the previously move scanPnt0/1/2 100.0mm along y direction
		// Determine the distance between cartiMainSurface & scanPnt0/1/2/3/4
		cartiDist0 = cartiHitPnt0.DistanceBetween(scanPnt0);
		cartiDist1 = cartiHitPnt1.DistanceBetween(scanPnt1);
		cartiDist2 = cartiHitPnt2.DistanceBetween(scanPnt2);
		cartiDist3 = cartiHitPnt3.DistanceBetween(scanPnt3);
		cartiDist4 = cartiHitPnt4.DistanceBetween(scanPnt4);
		cartiMinDist = min(cartiDist0, min(cartiDist1,min(cartiDist2, min(cartiDist3, cartiDist4)))) - 100.0;// "-100.0" to compensate the previously move scanPnt0/1/2 100.0mm along y direction
		minDist = min(osteoMinDist, cartiMinDist);
		osteoCartiDistances.Add(minDist);// These are the osteophyte/cartilage "-y" distances related to tipPosition. (Below tipPosition get a positive value.)

		/////////////////////////////////////////////////////////////////////////////
		// If has scanned (stylusFootLength+6.5), and not found yet.
		if ( d > (-defaultStylusLength+(stylusFootLength+6.5)) && !foundRegion )
		{
			// if previous (stylusFootLength+2) are all < maxDeviation
			int nSize = osteoFemurDistances.GetSize();
			double maxValue = -1000;
			for (int i=(nSize-1); i>(nSize-1-(int)stylusFootLength-2); i--)
			{
				maxValue = max(osteoFemurDistances.GetAt(i), maxValue);
			}
			//
			if ( maxValue < maxDeviation )
			{
				foundRegion = true;
				deltaLength = d + 1.0;// Let it extend 1.0mm more
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// determine the minDistanceToTipPosition. The highest (along y) osteophyte surface around stylus.
	// Again, osteoDistances are the "-y" distances. Minimum value means the highest. 
	double minDistanceToTipPosition = 10.0;
	if ( osteoCartiDistances.GetSize() > 0 )
	{
		SortIwTArray(osteoCartiDistances);
		minDistanceToTipPosition = osteoCartiDistances.GetAt(0);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// stylusControlPosition which is the projected point from TipPosition onto stylus top face.
	//// Here we determine stylusControlPosition based on these 2 conditions:
	//// 1. Ensure no gap between stylus and positioning jig
	//// 2. Ensure at least 2.0mm clearance between stylus and osteophyte.

	/////////////////////////////////////////////
	// 1. Ensure no gap between stylus and positioning jig
	double xLength = 5.0;
	double yHeight = 7.0;
	double zLength = 7.0;// The length of connection between stylus and positioning jig

	// Get outer surface
	IwBSplineSurface* outerSurfaceJigs = (IwBSplineSurface*)outerFace->GetSurface();

	// scan the 5x7 rectangle along x-z to determine the related heights (along y) wrt TipPosition
	IwPoint3d cornerPnt = antTip + 0.5*xLength*wslAxes.GetXAxis();
	IwPoint3d farYPnt = cornerPnt + 20*wslAxes.GetYAxis();
	IwPoint3d samplePnt, pnt;
	IwVector3d vec;
	double noGapMinDist = HUGE_DOUBLE;
	IwPoint2d uvParam;
	bool bInt;
	// sample along z axis
	for (int i=0; i<(int)(zLength+1); i++)
	{
		// first side
		samplePnt = cornerPnt - i*wslAxes.GetZAxis();
		bInt = IntersectSurfaceByLine(outerSurfaceJigs, samplePnt, wslAxes.GetYAxis(), cPnt3d);
		if ( bInt )
			pnt = cPnt3d;
		else
			DistFromPointToSurface(samplePnt, outerSurfaceJigs, pnt, uvParam);
		vec = pnt - antTip;
		dist = vec.Dot(wslAxes.GetYAxis());
		if (dist < noGapMinDist)
			noGapMinDist = dist;
		// second side
		samplePnt = cornerPnt -xLength*wslAxes.GetXAxis() - i*wslAxes.GetZAxis();
		bInt = IntersectSurfaceByLine(outerSurfaceJigs, samplePnt, wslAxes.GetYAxis(), cPnt3d);
		if ( bInt )
			pnt = cPnt3d;
		else
			DistFromPointToSurface(samplePnt, outerSurfaceJigs, pnt, uvParam);
		vec = pnt - antTip;
		dist = vec.Dot(wslAxes.GetYAxis());
		if (dist < noGapMinDist)
			noGapMinDist = dist;
	}
	// sample along x axis
	for (int i=0; i<(int)(xLength+1); i++)
	{
		// first side
		samplePnt = cornerPnt - i*wslAxes.GetXAxis();
		bInt = IntersectSurfaceByLine(outerSurfaceJigs, samplePnt, wslAxes.GetYAxis(), cPnt3d);
		if ( bInt )
			pnt = cPnt3d;
		else
			DistFromPointToSurface(samplePnt, outerSurfaceJigs, pnt, uvParam);
		vec = pnt - antTip;
		dist = vec.Dot(wslAxes.GetYAxis());
		if (dist < noGapMinDist)
			noGapMinDist = dist;
		// second side
		samplePnt = cornerPnt -zLength*wslAxes.GetZAxis() - i*wslAxes.GetXAxis();
		bInt = IntersectSurfaceByLine(outerSurfaceJigs, samplePnt, wslAxes.GetYAxis(), cPnt3d);
		if ( bInt )
			pnt = cPnt3d;
		else
			DistFromPointToSurface(samplePnt, outerSurfaceJigs, pnt, uvParam);
		vec = pnt - antTip;
		dist = vec.Dot(wslAxes.GetYAxis());
		if (dist < noGapMinDist)
			noGapMinDist = dist;
	}
	// Now the noGapMinDist is the lowest point around the 5x7 rectangle wrt tipPosition.
	// Usually noGapMinDist is a negative value.
	noGapMinDist = 0.1*floor(noGapMinDist/0.1+0.5);// round up to the closest 0.1

	//////////////////////////////////////////////////////////////////////////////
	//// 2. Ensure at least 2.0mm clearance between stylus and (osteophyte+cartilage).
	// The -minDistanceToTipPosition is the highest (osteophyte+cartilage) point around stylus wrt tipPosition
	double stylusClearance = m_pDoc->GetAdvancedControlJigs()->GetStylusJigsClearance() + 0.5;// The extra "0.5mm" as allowance.
	minDistanceToTipPosition = 0.1*floor(minDistanceToTipPosition/0.1+0.5);// round up to the closest 0.1
	double maxDistanceToTipPosition = -minDistanceToTipPosition;// maxDistanceToTipPosition is the highest (osteophyte+cartilage) point around stylus wrt tipPosition
																// If maxDistanceToTipPosition is negative, the highest point is below tipPosition.

	if ( (noGapMinDist-1.5) > (maxDistanceToTipPosition+stylusClearance) )// "2.5mm" stylus clearance. 
	{
		// Adjust to ensure no gap and additional move down 1.5mm to have a well connection
		stylusControlPosition = tipPosition + (yHeight+noGapMinDist-1.5)*wslAxes.GetYAxis();
		m_currGapBetweenStylusToOuterSurface = 0.1; // 
	}
	else
	{
		// Adjust to ensure 2mm clearance
		stylusControlPosition = tipPosition + (yHeight+maxDistanceToTipPosition+stylusClearance)*wslAxes.GetYAxis();// "2.5mm" stylus clearance.
		m_currGapBetweenStylusToOuterSurface = (maxDistanceToTipPosition+stylusClearance) - noGapMinDist + 0.1;
		if ( m_currGapBetweenStylusToOuterSurface < 0.1 )
			m_currGapBetweenStylusToOuterSurface = 0.1;
	}

	return foundRegion;
}

bool CDefineStylusJigs::DoVirtualStylus
(
	IwPoint3d& stylusControlPosition,	// I: default position
	IwPoint3d& stylusDelta,				// I: delta along (x,y,z) axes
	IwTArray<IwCurve*>& stylusWireFrame	// O:
)
{
	IwContext& iwContext = m_pDoc->GetIwContext();
	// get axes
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	// Get solid position jigs 
	CSolidPositionJigs* pSolidPositionJigs = m_pDoc->GetSolidPositionJigs();
	if ( pSolidPositionJigs == NULL )
		return false;
	IwBrep* positionJigsBrep = pSolidPositionJigs->GetIwBrep();
	// Get Osteophyte surface
	CFemoralPart* pOsteophyteSurface = m_pDoc->GetOsteophyteSurface();
	if ( pOsteophyteSurface == NULL )
		return false;
	IwBSplineSurface* osteophyteMainSurface = pOsteophyteSurface->GetSinglePatchSurface();

	//// For Positive side /////////////////////////////////////////////////////////////
	IwTArray<IwBSplineCurve*> posStylusProfiles;
	CreateStylusProfiles(true, stylusControlPosition, stylusDelta, posStylusProfiles);
	// Join together
	IwTArray<IwBSplineCurve*> topCurves, bottomCurves;
	topCurves.Add(posStylusProfiles.GetAt(0));
	topCurves.Add(posStylusProfiles.GetAt(1));
	topCurves.Add(posStylusProfiles.GetAt(2));
	topCurves.Add(posStylusProfiles.GetAt(3));
	topCurves.Add(posStylusProfiles.GetAt(4));

	bottomCurves.Add(posStylusProfiles.GetAt(5));
	bottomCurves.Add(posStylusProfiles.GetAt(6));
	bottomCurves.Add(posStylusProfiles.GetAt(7));
	bottomCurves.Add(posStylusProfiles.GetAt(8));
	IwBSplineCurve *posTopWireFrame, *posBottomWireFrame;
	IwBSplineCurve::CreateByJoining(iwContext, topCurves, NULL, posTopWireFrame);
	IwBSplineCurve::CreateByJoining(iwContext, bottomCurves, NULL, posBottomWireFrame);
	// Trimmed by osteophyte surface
	IwTArray<IwPoint3d> intPnts;
	IwTArray<IwPoint2d> intUVParams;
	IwTArray<double> intParams;
	IwExtent1d posTopDom = posTopWireFrame->GetNaturalInterval();
	IwExtent1d posBottomDom = posBottomWireFrame->GetNaturalInterval();
	// trim top curve
	IntersectSurfaceByCurve(osteophyteMainSurface, posTopWireFrame, intPnts, intUVParams, intParams);
	posTopDom.SetMinMax(posTopDom.GetMin(), intParams.GetAt(0));
	// trimmed by solid position
	IwSolutionArray sols;
	IwTopologySolver::BrepCurveSolve(positionJigsBrep, *posTopWireFrame, posTopDom, IW_SO_INTERSECT, IW_SR_SINGLE, 0.01, 10, NULL, sols); 
	if ( sols.GetSize() > 0 )
	{
		IwSolution sol = sols.GetAt(0);
		posTopDom.SetMinMax(sol.m_vStart.m_adParameters[0], intParams.GetAt(0));
	}
	posTopWireFrame->Trim(posTopDom);
if (0)
ShowCurve(m_pDoc, posTopWireFrame, red);
	// trim bottom curve
	intPnts.RemoveAll();
	intUVParams.RemoveAll();
	intParams.RemoveAll();
	IntersectSurfaceByCurve(osteophyteMainSurface, posBottomWireFrame, intPnts, intUVParams, intParams);
	posBottomDom.SetMinMax(intParams.GetAt(0), posBottomDom.GetMax());
	IwTopologySolver::BrepCurveSolve(positionJigsBrep, *posBottomWireFrame, posBottomDom, IW_SO_INTERSECT, IW_SR_SINGLE, 0.01, 10, NULL, sols); 
	if ( sols.GetSize() > 0 )
	{
		IwSolution sol = sols.GetAt(0);
		posBottomDom.SetMinMax(intParams.GetAt(0), sol.m_vStart.m_adParameters[0]);
	}
	posBottomWireFrame->Trim(posBottomDom);
if (0)
ShowCurve(m_pDoc, posBottomWireFrame, red);

	//// For Negative side /////////////////////////////////////////////////////////////
	IwTArray<IwBSplineCurve*> negStylusProfiles;
	CreateStylusProfiles(false, stylusControlPosition, stylusDelta, negStylusProfiles);
	// Join together
	IwTArray<IwBSplineCurve*> negTopCurves, negBottomCurves;
	negTopCurves.Add(negStylusProfiles.GetAt(0));
	negTopCurves.Add(negStylusProfiles.GetAt(1));
	negTopCurves.Add(negStylusProfiles.GetAt(2));
	negTopCurves.Add(negStylusProfiles.GetAt(3));
	negTopCurves.Add(negStylusProfiles.GetAt(4));

	negBottomCurves.Add(negStylusProfiles.GetAt(5));
	negBottomCurves.Add(negStylusProfiles.GetAt(6));
	negBottomCurves.Add(negStylusProfiles.GetAt(7));
	negBottomCurves.Add(negStylusProfiles.GetAt(8));
	IwBSplineCurve *negTopWireFrame, *negBottomWireFrame;
	IwBSplineCurve::CreateByJoining(iwContext, negTopCurves, NULL, negTopWireFrame);
	IwBSplineCurve::CreateByJoining(iwContext, negBottomCurves, NULL, negBottomWireFrame);
	// Trimmed by osteophyte surface
	intPnts.RemoveAll();
	intUVParams.RemoveAll();
	intParams.RemoveAll();
	IwExtent1d negTopDom = negTopWireFrame->GetNaturalInterval();
	IwExtent1d negBottomDom = negBottomWireFrame->GetNaturalInterval();
	// trim top curve
	IntersectSurfaceByCurve(osteophyteMainSurface, negTopWireFrame, intPnts, intUVParams, intParams);
	negTopDom.SetMinMax(negTopDom.GetMin(), intParams.GetAt(0));
	// trimmed by solid position
	IwTopologySolver::BrepCurveSolve(positionJigsBrep, *negTopWireFrame, negTopDom, IW_SO_INTERSECT, IW_SR_SINGLE, 0.01, 10, NULL, sols); 
	if ( sols.GetSize() > 0 )
	{
		IwSolution sol = sols.GetAt(0);
		negTopDom.SetMinMax(sol.m_vStart.m_adParameters[0], intParams.GetAt(0));
	}
	negTopWireFrame->Trim(negTopDom);
if (0)
ShowCurve(m_pDoc, negTopWireFrame, red);
	// trim bottom curve
	intPnts.RemoveAll();
	intUVParams.RemoveAll();
	intParams.RemoveAll();
	IntersectSurfaceByCurve(osteophyteMainSurface, negBottomWireFrame, intPnts, intUVParams, intParams);
	negBottomDom.SetMinMax(intParams.GetAt(0), negBottomDom.GetMax());
	IwTopologySolver::BrepCurveSolve(positionJigsBrep, *negBottomWireFrame, negBottomDom, IW_SO_INTERSECT, IW_SR_SINGLE, 0.01, 10, NULL, sols); 
	if ( sols.GetSize() > 0 )
	{
		IwSolution sol = sols.GetAt(0);
		negBottomDom.SetMinMax(intParams.GetAt(0), sol.m_vStart.m_adParameters[0]);
	}
	negBottomWireFrame->Trim(negBottomDom);
if (0)
ShowCurve(m_pDoc, negBottomWireFrame, red);

	//// Add to stylusWireFrame
	stylusWireFrame.Add(posTopWireFrame);
	stylusWireFrame.Add(posBottomWireFrame);
	stylusWireFrame.Add(negTopWireFrame);
	stylusWireFrame.Add(negBottomWireFrame);

	return true;
}

bool CDefineStylusJigs::DoStylus
(
	IwBrep*& stylusJigsBrep,			// I/O:
	IwPoint3d& stylusControlPosition,	// I:
	IwPoint3d& stylusDelta				// I:
)
{
	bool succeed = false;
	IwContext& iwContext = m_pDoc->GetIwContext();
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	CFemoralPart* pOsteophyteSurface = m_pDoc->GetOsteophyteSurface();
	IwBrep* osteoBrep = new (iwContext) IwBrep(*pOsteophyteSurface->GetIwBrep());
	IwBSplineSurface* osteoMainSurface = pOsteophyteSurface->GetSinglePatchSurface();

	IwTArray<IwBSplineCurve*> SPs;
	IwTArray<IwCurve*> stylusProfiles;
	CreateStylusProfiles(true, stylusControlPosition, stylusDelta, SPs);

	for (unsigned i=0; i<SPs.GetSize(); i++)
		stylusProfiles.Add(SPs.GetAt(i));

	double xWidth = 5.0;
	IwBrep *stylusBrep = new (iwContext) IwBrep();
	IwPrimitiveCreation pPC(stylusBrep->GetInfiniteRegion());
	pPC.CreateLinearSweep(stylusProfiles, -wslAxes.GetXAxis(), xWidth, 1, TRUE);
	// Trimmed out by osteoBrep
	IwBrep* stylusBrepTrimmed = Boolean(IW_BO_DIFFERENCE, stylusBrep, osteoBrep, 0.001, 2.5/180.0*IW_PI);
	IwTArray<IwFace*> faces;
	if (stylusBrepTrimmed!=NULL)
		stylusBrepTrimmed->GetFaces(faces);

	// Determine 4 contact points with osteo surface (before merge)
	// And create the osteophyte surface only around the contact region
	IwBSplineSurface* stylusContactSurface=NULL;// osteophyte surface only around the stylus contact region.
	IwTArray<IwPoint2d> stylusContactUVPoints;
	IwTArray<IwVertex*> vertexes;
	stylusBrepTrimmed->GetVertices(vertexes);
	IwPoint3d cPnt, pnt;
	double dist;
	IwPoint2d uvParam;
	for (unsigned i=0; i<vertexes.GetSize(); i++)
	{
		pnt = vertexes.GetAt(i)->GetPoint();
		dist = DistFromPointToSurface(pnt, osteoMainSurface, cPnt, uvParam);
		if ( dist < 0.05 )
		{
			stylusContactUVPoints.Add(uvParam);
if(0)
ShowPoint(m_pDoc, cPnt, red);
		}
	}
	if ( stylusContactUVPoints.GetSize() == 4 )
	{
		IwExtent2d domain;
		for (unsigned i=0; i<stylusContactUVPoints.GetSize(); i++)
			domain.AddPoint2d(stylusContactUVPoints.GetAt(i));
		domain.ExpandAbsolute(2.0);
		IwSurface* copySurface;
		osteoMainSurface->Copy(m_pDoc->GetIwContext(), copySurface);
		stylusContactSurface = (IwBSplineSurface*)copySurface;
		stylusContactSurface->TrimWithDomain(domain);
if(0)
ShowSurface(m_pDoc, stylusContactSurface, "stylusContactSurface", blue);
	}
	CStylusJigs* pStylusJigs = m_pDoc->GetStylusJigs();
	if (pStylusJigs)
	{
		pStylusJigs->SetStylusContactSurface(stylusContactSurface);
	}

	IwBrep* stylusJigsBrepUnion = NULL;
	// Merge with stylusJigsBrep
	if ( faces.GetSize() > 0 )
		stylusJigsBrepUnion = Boolean(IW_BO_UNION, stylusJigsBrep, stylusBrepTrimmed, 0.001, 2.5/180.0*IW_PI);

	if (stylusJigsBrepUnion)
	{
		stylusJigsBrep = stylusJigsBrepUnion;
		succeed = true;
	}

	return succeed;
}

void CDefineStylusJigs::DetermineStylusCtrlPoints
(
	IwTArray<IwPoint3d>& stylusCtrlPoints	// O:
)
{
	stylusCtrlPoints.RemoveAll();

	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	IwPoint3d oPoint = m_currStylusControlPosition+m_currStylusDelta.x*wslAxes.GetXAxis()+m_currStylusDelta.y*wslAxes.GetYAxis();

	stylusCtrlPoints.Add(oPoint+wslAxes.GetXAxis());// 1mm additional along x-axis
	stylusCtrlPoints.Add(oPoint+wslAxes.GetYAxis());// 1mm additional along y-axis
	stylusCtrlPoints.Add(oPoint+(22-10+m_currStylusDelta.z)*wslAxes.GetZAxis());// 
	stylusCtrlPoints.Add(oPoint);
}

void CDefineStylusJigs::CreateStylusProfiles
(
	bool bPositiveSide,							// I:
	IwPoint3d& stylusControlPosition,			// I: default position
	IwPoint3d& stylusDelta,						// I: delta along (x,y,z) axes
	IwTArray<IwBSplineCurve*>& stylusProfiles	// O:
)
{
	stylusProfiles.RemoveAll();

	IwContext& iwContext = m_pDoc->GetIwContext();

	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	double xShift;
	if ( bPositiveSide )
		xShift = 2.5;
	else
		xShift = -2.5;

	double refSizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSizeRatio();
	double stylusDepth = 25.0 + 8.0*refSizeRatio;

	IwPoint3d pnt0(xShift+stylusDelta.x, -7+stylusDelta.y-m_currGapBetweenStylusToOuterSurface, -7);
	IwPoint3d pnt1(xShift+stylusDelta.x, -3+stylusDelta.y, -7);// starting arc;
	IwPoint3d pnt1ArcCtr(xShift+stylusDelta.x, -3+stylusDelta.y, -7+3);// arc center;
	IwPoint3d pnt2(xShift+stylusDelta.x, 0+stylusDelta.y, -4);
	IwPoint3d pnt3(xShift+stylusDelta.x, 0+stylusDelta.y, 22-10+stylusDelta.z);// starting arc;
	IwPoint3d pnt3ArcCtr(xShift+stylusDelta.x, -10+stylusDelta.y, 22-10+stylusDelta.z);// arc center;
	IwPoint3d pnt4(xShift+stylusDelta.x,-10+stylusDelta.y, 22+stylusDelta.z);
	IwPoint3d pnt5(xShift+stylusDelta.x, -stylusDepth+stylusDelta.y, 22+stylusDelta.z);
	IwPoint3d pnt6(xShift+stylusDelta.x, -stylusDepth+stylusDelta.y, 22-7+stylusDelta.z);
	IwPoint3d pnt7(xShift+stylusDelta.x, -7-5+stylusDelta.y, 22-7+stylusDelta.z);// starting arc;
	IwPoint3d pnt7ArcCtr(xShift+stylusDelta.x, -7-5+stylusDelta.y, 22-7-5+stylusDelta.z);// arc center;
	IwPoint3d pnt8(xShift+stylusDelta.x, -7+stylusDelta.y, 22-7-5+stylusDelta.z);
	IwPoint3d pnt9(xShift+stylusDelta.x, -7+stylusDelta.y, -0.5);
	IwPoint3d pnt10(xShift+stylusDelta.x, -7+stylusDelta.y-m_currGapBetweenStylusToOuterSurface, -0.5);
	IwPoint3d pnt11(xShift+stylusDelta.x, -7+stylusDelta.y-m_currGapBetweenStylusToOuterSurface, -7);
	
	IwLine *line01, *line23, *line45, *line56, *line67, *line89, *line910, *line1011;
	IwBSplineCurve *arc12, *arc34, *arc78;
	IwLine::CreateLineSegment(iwContext, 3, pnt0 , pnt1, line01);
	IwCircle::CreateArcFromPoints(iwContext, 3, pnt1ArcCtr, pnt1, pnt2, IW_CO_QUADRATIC, arc12);
	IwLine::CreateLineSegment(iwContext, 3, pnt2 , pnt3, line23);
	IwCircle::CreateArcFromPoints(iwContext, 3, pnt3ArcCtr, pnt3, pnt4, IW_CO_QUADRATIC, arc34);
	IwLine::CreateLineSegment(iwContext, 3, pnt4 , pnt5, line45);
	IwLine::CreateLineSegment(iwContext, 3, pnt5 , pnt6, line56);
	IwLine::CreateLineSegment(iwContext, 3, pnt6 , pnt7, line67);
	IwCircle::CreateArcFromPoints(iwContext, 3, pnt7ArcCtr, pnt7, pnt8, IW_CO_QUADRATIC, arc78);
	IwLine::CreateLineSegment(iwContext, 3, pnt8 , pnt9, line89);
	IwLine::CreateLineSegment(iwContext, 3, pnt9 , pnt10, line910);
	IwLine::CreateLineSegment(iwContext, 3, pnt10 , pnt11, line1011);

	// Add to stylusProfiles
	stylusProfiles.Add(line01);
	stylusProfiles.Add(arc12);
	stylusProfiles.Add(line23);
	stylusProfiles.Add(arc34);
	stylusProfiles.Add(line45);
	stylusProfiles.Add(line56);
	stylusProfiles.Add(line67);
	stylusProfiles.Add(arc78);
	stylusProfiles.Add(line89);
	stylusProfiles.Add(line910);
	stylusProfiles.Add(line1011);

	// Transform
	IwAxis2Placement transMat = IwAxis2Placement(stylusControlPosition, wslAxes.GetXAxis(), wslAxes.GetYAxis());
	for (unsigned i=0; i<stylusProfiles.GetSize(); i++)
	{
		stylusProfiles.GetAt(i)->Transform(transMat, NULL);
	}

	return;
}


void CDefineStylusJigs::DisplayErrorMessages()
{
	if ( m_manActType != MAN_ACT_TYPE_EDIT )
	{
		m_errorCodes.RemoveAll();
		return;
	}

	int		button;
	int		errorMsgNum = (int)m_errorCodes.GetSize();
	if (errorMsgNum == 0) return;

	QString totalErrorMessages(tr(""));
	for (int i=0; i<errorMsgNum; i++)
	{
		CDefineStylusJigsErrorCodes errorCode = m_errorCodes.GetAt(i);
		QString msg;
		GetErrorCodeMessage(errorCode, msg);
		totalErrorMessages += msg + "\n\n";
	}

	button = QMessageBox::critical( NULL, 
									tr("Error Messages!"), 
									totalErrorMessages, 
									QMessageBox::Ok, 0 );	

	m_errorCodes.RemoveAll();	
}

void CDefineStylusJigs::GetErrorCodeMessage(CDefineStylusJigsErrorCodes& errorCode, QString& errorMsg)
{
	int errorNum = sizeof( DefineStylusJigsErrorCodes ) / sizeof( CDefineStylusJigsErrorCodesEntry );

	errorMsg = tr("");
	for( int i = 0; i < errorNum; i++ )
	{
		if( DefineStylusJigsErrorCodes[i].ErrorCodes == errorCode )
		{
			errorMsg = DefineStylusJigsErrorCodes[i].ErrorMessages;
			break;
		}
	}

}

//////////////////////////////////////////////////////////////
bool CDefineStylusJigs::DetermineStylusClearance
(
	CTotalDoc* pDoc,					// I:
	double& stylusContactClearance,		// O:
	double& stylusArmClearance			// O:
)
{
	bool reliable = false;
	stylusContactClearance = 0;
	stylusArmClearance = 0;

	CStylusJigs* stylusJigs = pDoc->GetStylusJigs();
	if ( stylusJigs == NULL )
		return false;
	IwBSplineSurface* stylusContactSurf = stylusJigs->GetStylusContactSurface();
	if ( stylusContactSurf == NULL )// it could be an iTW v5 case. Try osteophyte main surface.
	{
		CFemoralPart* pOsteophyteSurface = pDoc->GetOsteophyteSurface();
		stylusContactSurf = pOsteophyteSurface->GetSinglePatchSurface();
	}
	if ( stylusContactSurf == NULL )
		return false;
	IwBrep* stylusJigsBrep = stylusJigs->GetIwBrep();
	if ( stylusJigsBrep == NULL )
		return false;
	IwPoint3d stylusCtrlPnt, stylusTipPnt, stylueDelta;
	double gapBetweenStylusToOuterSurface;
	stylusJigs->GetStylusPosition(stylusCtrlPnt, stylusTipPnt, stylueDelta, gapBetweenStylusToOuterSurface);

	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();

	IwBSplineSurface *femurMainSurface, *femurLeftSurface, *femurRightSurface;
	IFemurImplant::FemoralPart_GetSurfaces(false, femurMainSurface, femurLeftSurface, femurRightSurface);

	//////////////////////////////////////////////////////////////
	// Check stylusContactSurf max deviation from femur surface
	// First, get the 4 vertexes of stylusJigsBrep which contact the stylusContactSurf
	IwTArray<IwVertex*> vertexes;
	IwTArray<IwPoint3d> contactPoints;
	stylusJigsBrep->GetVertices(vertexes);
	IwPoint3d vec, vPnt, cPnt;
	IwPoint2d param2d;
	double dist, contactDist = 0.01;
	for (unsigned i=0; i<vertexes.GetSize(); i++)
	{
		vPnt = vertexes.GetAt(i)->GetPoint();
		vec = vPnt - stylusTipPnt;
		if ( vec.Dot(wslAxes.GetZAxis()) > 0 ) // the stylus contact verteces should be in the anterior (Z-axis) side of tip point.
		{
			dist = DistFromPointToSurface(vPnt, stylusContactSurf, cPnt, param2d);
			if ( dist < contactDist )
				contactPoints.Add(vPnt);
		}
	}
if (0)
{
	for (unsigned i=0; i<contactPoints.GetSize(); i++)
		ShowPoint(pDoc, contactPoints.GetAt(i), red);
}
	if ( contactPoints.GetSize() == 0 )
		return false;
	// Determine the max gap between femur and stylusContactSurf 
	// along the contactPoints
	for ( unsigned i=0; i<contactPoints.GetSize(); i++)
	{
		dist = DistFromPointToSurface(contactPoints.GetAt(i), femurMainSurface, cPnt, param2d);
		if ( dist > stylusContactClearance )
			stylusContactClearance = dist;
	}
	

	////////////////////////////////////////////////////
	// Check stylusArmClearance
	// Determine stylusLowerFace, which has 4 or 5 edges. 
	IwTArray<IwFace*> faces;
	IwFace* stylusLowerFace=NULL;
	stylusJigsBrep->GetFaces(faces);
	IwPoint3d pPnt, pNormal;
	IwTArray<IwEdge*> edges;
	double minDist = HUGE_DOUBLE;
	for (unsigned i=0; i<faces.GetSize(); i++)
	{
		if ( faces.GetAt(i)->GetSurface()->IsPlanar(0.01, &pPnt, &pNormal) )
		{
			if ( pNormal.IsParallelTo(wslAxes.GetYAxis(), 0.5) )
			{
				faces.GetAt(i)->GetEdges(edges);
				if ( 1 /*edges.GetSize() == 4 || edges.GetSize() == 5 || edges.GetSize() == 6 */ ) // The edge number is not reliable
				{
					vec = pPnt - stylusTipPnt;
					if ( vec.Dot(wslAxes.GetZAxis()) > 0 ) // the stylusLowerFace has to be in the positive-z side of stylusTipPosition
					{
						vec = pPnt - wslAxes.GetOrigin();
						dist = vec.Dot(wslAxes.GetYAxis());
						if ( dist < minDist )
						{
							stylusLowerFace = faces.GetAt(i);
							minDist = dist;
if (0)
ShowFace(pDoc, faces.GetAt(i), red);
						}
					}
				}
			}
		}
	}
	if ( stylusLowerFace == NULL )
		return false;
	// Create osteophyte main face
	CFemoralPart *pOsteoSurface = pDoc->GetOsteophyteSurface();
	if ( pOsteoSurface==NULL)
		return false;
	IwBSplineSurface *osteoMainSurface = pOsteoSurface->GetSinglePatchSurface();
	IwSurface* oSurf;
	osteoMainSurface->Copy(pDoc->GetIwContext(), oSurf);
	IwBrep* ocBrep = new ( pDoc->GetIwContext() ) IwBrep;
	IwFace* osteophyteMainFace;
	ocBrep->CreateFaceFromSurface(oSurf, oSurf->GetNaturalUVDomain(), osteophyteMainFace);
	// Create cartilage main face
	CCartilageSurface *pCartiSurface = pDoc->GetCartilageSurface();
	if ( pCartiSurface==NULL)
		return false;
	IwBSplineSurface *cartiMainSurface = pCartiSurface->GetSinglePatchSurface();
	IwSurface* cSurf;
	cartiMainSurface->Copy(pDoc->GetIwContext(), cSurf);
	IwFace* cartilageMainFace;
	ocBrep->CreateFaceFromSurface(cSurf, cSurf->GetNaturalUVDomain(), cartilageMainFace);

	//
	IwPoint3d sPnt, oPnt;
	IwVector3d yAxis = wslAxes.GetYAxis();
	double distToOsteo = DistFromFaceToFace(stylusLowerFace, osteophyteMainFace, sPnt, oPnt, &yAxis); 
	double distToCarti = DistFromFaceToFace(stylusLowerFace, cartilageMainFace, sPnt, cPnt, &yAxis); 
	stylusArmClearance = min(distToOsteo, distToCarti);
if (0)
{
	ShowFace(pDoc, stylusLowerFace, red);
	ShowPoint(pDoc, oPnt, green);
	ShowPoint(pDoc, cPnt, blue);
}
	if ( ocBrep )
		IwObjDelete(oBrep);

	return true;
}

bool CDefineStylusJigs::OnReviewNext()
{
	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_STYLUS_JIGS, true);
	return true;
}

bool CDefineStylusJigs::OnReviewBack()
{
	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_STYLUS_JIGS, false);
	return true;
}

bool CDefineStylusJigs::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;


	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_STYLUS_JIGS))
    	OnAccept();
	return true;
}

void CDefineStylusJigs::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 2;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 ) 
	{
		// Display stylue
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display femur
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display osteophyte
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Hide cartilage
		pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		vp->FrontView(false);
	}
	else if ( viewIndex == 1 ) 
	{
		// Display stylue
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display osteophyte
		pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Display cartilage
		pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Hide femur
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}

		vp->TopView(false);
	}

	m_predefinedViewIndex++;
}