#include "TotalDocPSReader.h"
#include "TotalDocPS.h"
#include "..\KAppTotal\Part.h"
#include "TotalViewPS.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\TotalDocDlg.h"
#include "QDialog.h"
#include <QtGui>

KTotalDocPSReader::KTotalDocPSReader( KTotalDocPS* pDoc, const QString& sDocFolder ) : CTotalDocReader(pDoc,sDocFolder )
{
	m_pDoc			= pDoc;
	m_sDocFolder	= sDocFolder;
}


KTotalDocPSReader::~KTotalDocPSReader()
{
}

////////////////////////////////////////////////////////////////////
// This function loads the PS entities described in header.txt
////////////////////////////////////////////////////////////////////
bool KTotalDocPSReader::LoadPSEntities(CEntRole startEntRole, CEntRole endEntRole)
{
	m_pDoc->AppendLog( QString("KTotalDocPSReader::LoadEntities()") );

	QString					sLine;
	QFile					File( m_sDocFolder + "\\header.txt" );
	QTextStream				in( &File );
	bool					bOK;
	CPart*					pPart;
	CEntity*				pEntity;
	QString					sTag, sValue;
	int						r, g, b;
	char					buf[160];
	CColor					color;
	int						m_nEntityId;

	bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	if( !bOK )
		return false;

	InitEntity();


	// Read through the file.
	while( !in.atEnd() )
	{
		sLine = in.readLine();

		if( sLine == "" )
			continue;

		if( sLine == "END ENTITY" )
		{
			// Only handle TriathlonF1 PS entities between startEnt & endEnt
			if ( m_eEntRole >= startEntRole && m_eEntRole <= endEntRole )
			{
				m_pDoc->AppendLog( QString("KTotalDocPSReader::LoadPSEntities(), loading %1" ).arg(CEntity::RoleName( m_eEntRole )) );
				switch( m_eEntityType )
				{
					case ENT_TYPE_PART:
					{
						break;
					}

					case ENT_TYPE_ADVANCED_CONTROL:
					{
						pEntity->SetDisplayMode( m_eDisplayMode );
						pEntity->SetUpToDateStatus(m_bUpToDateStatus);
						pEntity->SetValidateStatus(m_validateStatus, m_validateMessage);
						pEntity->SetColor( color );
						pEntity->SetDesignTime( m_designTime );

						bool bAddToEntPanel = false;
						m_pDoc->AddEntity( pEntity, bAddToEntPanel );

						pEntity->Load( m_sDocFolder );

						break;
					}

				}

				InitEntity();

				continue;
			}
		}

		bOK = ParseInputLine( sLine, sTag, sValue );

		// This LoadHeaderFile() only care about the following tags, which define an entity.
		if( sTag == "ENTITY ID" )
		{
			m_designTime = 0; // new parameter in iTW v6.0.7. Initalize here when start reading the entity info.
			m_nEntityId = sValue.toInt();
		}
		else if( sTag == "ENTITY ROLE" )
		{
			if ( sValue == QString("Box Cut") )
				sValue = QString("Box");// iTW v6 and iTW PS v2 change to "Box".
			m_eEntRole = CEntity::RoleByName( sValue );
		}
		else if( sTag == "ENTITY TYPE" )
		{
			m_eEntityType = CEntity::EntTypeByName( sValue );
		}
		else if( sTag == "FILE NAME" )
		{
			m_sFileName = sValue;
		}
		else if( sTag == "DISPLAY MODE" )
		{
			m_eDisplayMode = CEntity::DispModeByName( sValue );
		}
		else if( sTag == "UPTODATE STATUS" )
		{
			m_bUpToDateStatus = sValue.toInt() == 1 ? true : false;
		}
		else if( sTag == "VALIDATE STATUS" )
		{
			m_validateStatus = (CEntValidateStatus)sValue.toInt();
		}
		else if( sTag == "VALIDATE MESSAGE" )
		{
			m_validateMessage = sValue;
			QString testStr = m_validateMessage;
			testStr.remove(" "); // check whether all blank
			if (testStr.isEmpty())
				m_validateMessage = QString("");
			// Replace "#&" by "\n"
			m_validateMessage.replace(QString("#&"), QString("\n"), Qt::CaseSensitive);
		}
		else if( sTag == "ENTITY COLOR" )
		{
			QStringToCharArray(	sValue, buf );

			sscanf( buf, "%d  %d  %d", &r, &g, &b );

			color = CColor( r, g, b );
		}
		else if( sTag == "STATISTIC" )
		{
			m_designTime = (int)(sValue.toDouble()*997.0);// simple way to code/decode. See DocWriter.cpp
		}
		else if( sTag == "PART TYPE" )
		{
			if( sValue == "Brep" )
				m_ePartType = PART_TYPE_BREP;
			else if( sValue == "STL" )
				m_ePartType = PART_TYPE_STL;
		}
	}

	File.close();

	return true;

}

