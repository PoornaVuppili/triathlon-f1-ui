#pragma once
#include <QtGui>
#include "..\KTriathlonF1\TotalEntPanel.h"
#include "..\KTriathlonF1\TotalDoc.h"

class KTotalDocPS;
class KTotalViewPS;

class KTotalEntPanelPS : public CTotalEntPanel
{
	Q_OBJECT

public:
	KTotalEntPanelPS(QTreeView* view);
	~KTotalEntPanelPS();
	KTotalDocPS*	GetTotalDocPS() {return ((KTotalDocPS*)m_pDoc);};
	KTotalViewPS*	GetView() {return ((KTotalViewPS*)(m_pDoc->GetView()));};
    // Get the item id
    virtual QString GetID() const { return "FemoralPSItem"; }
	virtual void		Add( int id, const QString& sName, CEntRole eRole );
	virtual void        OnItemClicked(const QModelIndex&);
	virtual void        OnItemRightClicked(const QModelIndex&);

protected:
	void				DisplayContextMenuPS( int row );
	void				ItemClickedPS(const QModelIndex&);

public slots:
	void				OnRedefinePS();
	void				OnRegenerateAStepPS();
	void				OnRegenerateMultipleStepsPS();
	void				OnDeleteAllPS();
    void				NotifyOfPSFeaturesChange();

signals:

protected:
	QAction*			m_actTransFemur;
	QAction*			m_actTransNone;

private:

};
