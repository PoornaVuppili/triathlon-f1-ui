#ifndef IMPLANT_H
#define IMPLANT_H

// Implant information
#include <QObject>
#include <QImage>
#include <qstringlist.h>

#include <QDomDocument>
#include "Configuration.h"

class SelectImplant;
class Module;
class QMenu;

#include <vector>
using namespace std;

class TEST_EXPORT Implant : public QObject
{
    Q_OBJECT

public:
    Implant(QObject *parent=NULL);
    virtual ~Implant();

    // Initialize modules. If the module has menus, they are inserted before the menu 'before' if not NULL.
    // Otherwise, they are added at the end. If the module has toolbars, they are added to the main toolbar.
    virtual bool InitializeModules(QMenu* before=NULL) { return false; }
    // Release modules
    virtual void ReleaseModules() {}

    // Get the version info of all modules
    virtual QStringList GetModuleVersionInfo() { return QStringList(); }

    // Load the implant
    virtual bool LoadImplant(SelectImplant* si) = 0;

    // Save the implant
    virtual bool SaveImplant() = 0;

    // Save the Rework implant
	virtual bool SaveReworkImplant() {	return true; }

    // Close the implant
    virtual bool CloseImplant();

    // Whether an implant is open
    virtual bool HasImplant() const = 0;

    // Whether the implant is changed
    virtual bool IsImplantChanged() const = 0;

    // Get the implant info by name
    virtual QString GetInfo(const QString& name) const;

    // Get the module by name
    virtual Module* GetModule(const QString& name) { return NULL; }

    // Enable/disable actions of menus and toolbars
    virtual void EnableActions(bool enable) {}

    // For squish screen snapshot test
    bool IsSimilarImage(const QImage& img1, const QImage& img2, double thCor=0.999, int compareMode=0);

    // For squish measurement
    float GetMeasurement();

    virtual void HideAll() {}
    virtual void HideAllExcept(Module* pMod) {}

    virtual bool AllStepsComplete(std::string const &phase, std::string const &subphase) const { return true; }
    virtual bool OutputResults(std::string const &phase, std::string const &subphase) const = 0 /* require to be overridden!!! */ { return true; }
    virtual int CurrentStage(QString *name = nullptr) const { return -1; }
    virtual bool IsStageAvailableTo(Module* pMod) { return true; } // by default the inquiring module is active
    // virtual 

    // public interface for configuration handling
    bool LoadConfig(QString const &tableName, QString const &versionToCheck);

    int getConfigInt(QString const &tableName, QString const &name, int = 0);
    bool getConfigBoolean(QString const &tableName, QString const &name, bool = false);
    double getConfigDouble(QString const &tableName, QString const &name, double = DBL_MAX);
    QString getConfigString(QString const &tableName, QString const &name, QString const & = "");
    double getConfigDoubleWarn(QString const &tableName, QString const &name, double = DBL_MAX);
    QString getConfigStringWarn(QString const &tableName, QString const &name, QString const & = "");

private:
      // ATTENTION! When adding another ExecutionMode, keep them SEQUENTIAL
      enum ExecutionMode { ExM_UNSET = 0, ExM_DEVELOPMENT = 1, ExM_BETA, ExM_PRODUCTION, /* add here! */
                           ExM_ENDOFRANGE };
      // ATTENTION! When adding another ExecutionMode, keep them SEQUENTIAL
      ExecutionMode m_executionMode; // Keep PRIVATE!!!
      ExecutionMode doGetExecutionMode();
      ExecutionMode getExecMode() { return (m_executionMode != ExM_UNSET ? m_executionMode : doGetExecutionMode()); }

public:
    bool isProduction()  { return getExecMode() == ExM_PRODUCTION; }
    bool isDevelopment() { return getExecMode() == ExM_DEVELOPMENT; }
    bool isBetaTesting() { return getExecMode() == ExM_BETA; }

	QString strProdFolder();
	bool isInProdFolder();

signals:
   void ImplantSavedSuccessfully();

protected:
    // Add the module's menus and toolbars to the main window
    void AddModuleMenuToolbar(Module* mod, QMenu* before=NULL);

    // clear the module content
    virtual void ClearModule() {}

    typedef std::map<QString,Configuration*> config_t;

    config_t      m_configs;

    virtual Configuration* createConfig(QString const &tableName) = 0; // this what each Implant derive class should also provide

    Configuration* getOrAddConfiguration(QString const &tableName);
};

#endif // IMPLANT_H
