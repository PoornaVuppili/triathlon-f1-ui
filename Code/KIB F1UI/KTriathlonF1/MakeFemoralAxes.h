#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Basics.h"
#include "..\KAppTotal\Entity.h"

#include <QDoubleSpinBox>

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class CPart;
class CFemoralPart;
class IwBSplineCurve;

struct CMakeFemoralAxesUndoData
{
	IwPoint3d			hipPoint;			
	IwPoint3d			proximalPoint;			
	IwPoint3d			leftEpiCondylarPoint;			
	IwPoint3d			rightEpiCondylarPoint;		
	double				allFlexionAngle;
	double				antCutFlexionAngle;
	double				antCutObliqueAngle;
	double				femoralAxesFlexionAngle;
	double				femoralAxesYRotAngle;
	double				femoralAxesZRotAngle;
	IwPoint3d			antCutPlaneCenter;
};

class TEST_EXPORT_TW CMakeFemoralAxes : public CManager
{
    Q_OBJECT

public:
					CMakeFemoralAxes( CTotalView* pView, CManagerActivateType manActType );
	virtual			~CMakeFemoralAxes();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};
	void			GetMakeFemoralAxesUndoData(CMakeFemoralAxesUndoData& undoData);
	void			SetMakeFemoralAxesUndoData(CMakeFemoralAxesUndoData& undoData);
	virtual bool	MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp  ( const QPoint& cursor, Viewport* vp=NULL );

	virtual void	Display(Viewport* vp=NULL);
	void			DisplayBiSectLine(IwPoint3d caughtPnt, IwVector3d offset=IwVector3d(0,0,0));
	void			DisplayValidationLandMarks(IwVector3d offset=IwVector3d(0,0,0));

private:
	static bool		RefineFemoralOrigin(CTotalDoc* pDoc, IwPoint3d& proximalPoint, bool& bDeviated, bool& bBiSect, double tolDist=0.01);
	void			BisectFemoralOrigin(bool forceNotchToIdealLocation);
	void			Reset(bool activateUI=true);
	void			SetValidateIcon(CEntValidateStatus vStatus, QString message);
	void			Accept(bool activateUI=true);
	void			RotateFemur(bool CCW);

signals:
	void			SwitchToCoordSystem(QString name);

private slots:
	void			OnAccept();
	void			OnCancel();
	void			OnRotateCCW();
	void			OnRotateCW();
	virtual bool	OnReviewNext();
	virtual bool	OnReviewRework();
	void			OnReviewPredefinedView();

private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;
	CFemoralPart*				m_pFemur;
	IwBrep*						m_pFemurBrep;
	CPart*						m_pHip;
	IwBrep*						m_pHipBrep;

	bool						m_leftButtonDown;
	bool						m_oldFemuralAxesExist;
	int							m_predefinedViewIndex;

	IwPoint3d					m_hipPoint;
	IwPoint3d					m_proximalPoint;
	IwPoint3d					m_leftEpiCondylarPoint; // left side of the patient
	IwPoint3d					m_rightEpiCondylarPoint;// right side of the patient
	double						m_allFlexionAngle;
	double						m_antCutFlexionAngle;
	double						m_antCutObliqueAngle;
	double						m_femoralAxesFlexionAngle;
	double						m_femoralAxesYRotAngle;
	double						m_femoralAxesZRotAngle;
	IwPoint3d					m_antCutPlaneCenter;
	IwPoint3d					m_antCutPlaneRotationCenter;
	// Reference
	IwPoint3d					m_proximalPointReference;

	IwPoint3d					m_oldHipPoint;
	IwPoint3d					m_oldProximalPoint;
	IwPoint3d					m_oldLeftEpiCondylarPoint;
	IwPoint3d					m_oldRightEpiCondylarPoint;
	double						m_oldAllFlexionAngle;
	double						m_oldAntCutFlexionAngle;
	double						m_oldAntCutObliqueAngle;
	double						m_oldFemoralAxesFlexionAngle;
	double						m_oldFemoralAxesYRotAngle;
	double						m_oldFemoralAxesZRotAngle;
	IwPoint3d					m_oldAntCutPlaneCenter;
	IwPoint3d					m_oldAntCutPlaneRotationCenter;
	bool						m_oldUpToDateStatus;
	CEntValidateStatus			m_oldValidateStatus;
	QString						m_oldValidateMessage;

	//// For Validation ///////////////////////////////////////////////////////////////
	CEntValidateStatus			m_validateStatus;
	QString						m_validateMessage;
	IwPoint3d					m_hipPointValidation;
	IwPoint3d					m_proximalPointValidation;
	IwPoint3d					m_leftEpiCondylarPointValidation; 
	IwPoint3d					m_rightEpiCondylarPointValidation;
	double						m_antCutFlexionAngleValidation;
	double						m_antCutObliqueAngleValidation;
	double						m_femoralAxesFlexionAngleValidation;
	IwPoint3d					m_antCutPlaneCenterValidation;

	QAction*					m_actMakeFemoralAxes;
	QAction*					m_actRotateCCW;
	QAction*					m_actRotateCW;
	QAction*					m_actRefine;
	QAction*					m_actValidate;
	QAction*					m_actReviewNext;
	QAction*					m_actReviewBack;
	QAction*					m_actReviewRework;

	bool						m_valueChangedFromUndo;
	QDoubleSpinBox*				m_spinAllFlexionAngle;
	QDoubleSpinBox*				m_spinAntCutFlexionAngle;
	QDoubleSpinBox*				m_spinAntCutObliqueAngle;
	QDoubleSpinBox*				m_spinFemoralAxesFlexionAngle;
	QDoubleSpinBox*				m_spinFemoralAxesYRotAngle;
	QDoubleSpinBox*				m_spinFemoralAxesZRotAngle;

};
