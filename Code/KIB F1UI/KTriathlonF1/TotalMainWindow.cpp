#include <QtGui>
#include "TotalMainWindow.h"
#include "TotalView.h"
#include "TotalDoc.h"
#include "TotalEntPanel.h"
#include "TotalEntPanelJigs.h"
#include "AdvancedControl.h"
#include "AdvancedControlJigs.h"
#include "FemoralAxes.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\TotalDocDlg.h"
#include "..\KAppTotal\FileNewDlg.h"
#include "..\KAppTotal\InfoTextReader.h"
#include "..\KAppTotal\Utilities.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\EntityWidget.h"
#include "..\KApp\ViewWidget.h"
#include "..\KApp\Viewport.h"
#include "..\KApp\Implant.h"
#include "..\KUtility\KUtility.h"
#include "../KTriathlonF1/IFemurImplant.h"
#include "../KTriathlonF1/IFemurJigs.h"
#include "IFemurImplant.h"

#include "OutputResultsJigs.h"

CTotalMainWindow::CTotalMainWindow()
{
	JCOSToolbar = NULL;
}

CTotalMainWindow::~CTotalMainWindow()
{
    DeletePtr(m_pView);
    DeletePtr(m_pDoc);
    DeletePtr(m_pEntPanel);
}

QString CTotalMainWindow::WindowTitle()
{
	if ( m_pDoc->isProductionSoftware() )
		return QString( "TriathlonF1%1 %2.%3" ).arg(QChar(8482)).arg( ITOTAL_RELEASE ).arg( ITOTAL_REVISION );
	else
		return QString( "TriathlonF1%1 %2.%3.%4.%5" ).arg(QChar(8482)).arg( ITOTAL_RELEASE ).arg( ITOTAL_REVISION ).arg( ITOTAL_DATAFILE_VERSION ).arg( GetAppVersion() );
	
}

bool CTotalMainWindow::Initialize()
{
	//// Set main widgets
	m_pView = new CTotalView( this );
	m_pDoc = new CTotalDoc(this);
    m_pEntPanel = new CTotalEntPanel(entityWidget);
	m_pEntPanelJigs = new CTotalEntPanelJigs(entityWidget);

	CTotalDoc::SetTotalDoc((CTotalDoc*)m_pDoc);
	m_pDoc->SetView( m_pView );
	m_pDoc->SetEntPanel( m_pEntPanel );
	GetTotalDoc()->SetEntPanelJigs( m_pEntPanelJigs );

	m_pView->SetDoc( m_pDoc );
    m_pView->SetMainWindow( this );

	m_pEntPanel->SetDoc( m_pDoc );
	m_pEntPanelJigs->SetDoc( m_pDoc );

	/******************************************************/
    m_menuFemur = new QMenu("Femur");
    menus.push_back(m_menuFemur);

    /******************* MAKING MENU **********************/
	m_actFileNew			= m_menuFemur->addAction( "New" );
	m_actMakeFemoralAxes	= m_menuFemur->addAction( "Femoral Axes" );
 
	m_actDefineOutlineProfile	= new QAction(QIcon(":/KTriathlonF1/Resources/OutlineProfile.png"),"Outline", this);
	m_menuFemur->addAction( m_actDefineOutlineProfile );
	m_actMakeSolidImplant		= new QAction(QIcon(":/KTriathlonF1/Resources/SolidImplant.png"),"Solid Implant", this);
	m_actAddFillets			= m_menuFemur->addAction( "Outer Fillet" );
	m_actDefinePegs			= m_menuFemur->addAction( "Pegs" );
	m_actConvertCastingImplant	= m_menuFemur->addAction( "Cast Implant" );

	m_actSeparator			= m_menuFemur->addSeparator();
	m_actAdvancedControl	= m_menuFemur->addAction( "Adv Ctrl Implant");

	connect( m_menuFemur, SIGNAL( aboutToShow() ), this, SLOT( OnEnableMakingActions() ) );
	connect( m_actFileNew, SIGNAL( triggered() ), this, SLOT( OnFileNew() ) );
	connect( m_actMakeFemoralAxes, SIGNAL( triggered() ), m_pView, SLOT( OnHandleFemoralAxes() ) );
	connect( m_actDefineOutlineProfile, SIGNAL( triggered() ), m_pView, SLOT( OnDefineOutlineProfile() ) );

	connect( m_actAdvancedControl, SIGNAL( triggered() ), m_pView, SLOT( OnAdvancedControl() ) );
	
	// Create toolbar
	JCOSToolbar = new QToolBar("JCOS");

	JCOSToolbar->addAction(m_actDefineOutlineProfile);
	JCOSToolbar->setVisible(false);
	toolbars.push_back( JCOSToolbar );

	/******************* FILE MENU **********************/
	m_actSeparator				= m_menuFemur->addSeparator();
	m_menuFile					= m_menuFemur->addMenu( "&File" );
	m_actFileImportCADEntity	= m_menuFile->addAction( "Import CAD Entity" );

	char *saveNewTWFile = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (saveNewTWFile != NULL && QString("1") == saveNewTWFile)// under-development mode to optionally save new tw file only
	{
		m_actFileSaveNewFile		= m_menuFile->addAction( "Save New tw File" );
		connect( m_actFileSaveNewFile, SIGNAL( triggered() ), this, SLOT( OnSaveNewFile() ) );
	}

	m_menuFile->addSeparator();

	connect( m_actFileImportCADEntity, SIGNAL( triggered() ), this, SLOT( OnImportCADEntity() ) );

	/******************* VIEW MENU **********************/
	m_menuView			= m_menuFemur->addMenu( "&View" );
	m_menuViewTessellation		= m_menuView->addMenu( "Tessellation" );
	m_actTessellationHigh		= m_menuViewTessellation->addAction( "ScreenShot" );
	m_actTessellationFine		= m_menuViewTessellation->addAction( "Fine" );
	m_actTessellationNormal		= m_menuViewTessellation->addAction( "Normal" );
	m_actTessellationCoarse		= m_menuViewTessellation->addAction( "Coarse" );
	m_actTessellationFine->setCheckable(true);
	m_actTessellationNormal->setCheckable(true);
	m_actTessellationCoarse->setCheckable(true);
	m_groupTessellation = new QActionGroup( this );
	m_groupTessellation->addAction(m_actTessellationHigh);
	m_groupTessellation->addAction(m_actTessellationFine);
	m_groupTessellation->addAction(m_actTessellationNormal);
	m_groupTessellation->addAction(m_actTessellationCoarse);
	m_actTessellationFine->setChecked(true);
	m_menuViewTransparency			= m_menuView->addMenu( "Transparency" );
	m_actTransparencyTransparentTotal= m_menuViewTransparency->addAction( "TransparentTotal" );
	m_actTransparencyTransparentPlus= m_menuViewTransparency->addAction( "Transparent++" );
	m_actTransparencyTransparent	= m_menuViewTransparency->addAction( "Transparent+" );
	m_actTransparencyNormal			= m_menuViewTransparency->addAction( "Transparent" );
	m_actTransparencyOpaque			= m_menuViewTransparency->addAction( "Transparent-" );
	m_actTransparencyOpaquePlus		= m_menuViewTransparency->addAction( "Transparent--" );
	m_actTransparencyTransparentTotal->setCheckable(true);
	m_actTransparencyTransparentPlus->setCheckable(true);
	m_actTransparencyTransparent->setCheckable(true);
	m_actTransparencyNormal->setCheckable(true);
	m_actTransparencyOpaque->setCheckable(true);
	m_actTransparencyOpaquePlus->setCheckable(true);
	m_groupTransparency = new QActionGroup( this );
	m_groupTransparency->addAction(m_actTransparencyTransparentTotal);
	m_groupTransparency->addAction(m_actTransparencyTransparentPlus);
	m_groupTransparency->addAction(m_actTransparencyTransparent);
	m_groupTransparency->addAction(m_actTransparencyNormal);
	m_groupTransparency->addAction(m_actTransparencyOpaque);
	m_groupTransparency->addAction(m_actTransparencyOpaquePlus);
	m_actTransparencyNormal->setChecked(true);

	{
		m_actCleanTempDisplay	= m_menuView->addAction( "CleanUp Temp Display" );
		connect( m_actCleanTempDisplay, SIGNAL( triggered() ), this, SLOT( OnCleanUpTempDisplay() ) );
	}

	connect( m_actTessellationHigh, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationHigh() ) );
	connect( m_actTessellationFine, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationFine() ) );
	connect( m_actTessellationNormal, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationNormal() ) );
	connect( m_actTessellationCoarse, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationCoarse() ) );
	connect( m_actTransparencyTransparentTotal, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentTotal() ) );
	connect( m_actTransparencyTransparentPlus, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentPlus() ) );
	connect( m_actTransparencyTransparent, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparent() ) );
	connect( m_actTransparencyNormal, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyNormal() ) );
	connect( m_actTransparencyOpaque, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaque() ) );
	connect( m_actTransparencyOpaquePlus, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaquePlus() ) );

	///******************* TOOLS MENU **********************/
	m_menuTools					= m_menuFemur->addMenu( "&Tools" );
	m_actAnalyzeImplant			= m_menuTools->addAction( "Evaluate Implant" );
	m_actAnalyzeImplant->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_E) );
	m_actReviewImplant			= m_menuTools->addAction( "Review Implant" );
	m_actReviewImplant->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_W) );

	connect( m_actAnalyzeImplant, SIGNAL( triggered() ), m_pView, SLOT( OnAnalyzeImplant() ) );
	connect( m_actReviewImplant, SIGNAL( triggered() ), this, SLOT( OnFemoralImplantReview() ) );

	/******************* INFO MENU **********************/
	m_menuInfo					= m_menuFemur->addMenu( "&Info" );
	m_actUserHistory			= m_menuInfo->addAction( "File Info" );
	connect( m_actUserHistory, SIGNAL( triggered() ), m_pView, SLOT( OnFileInfo() ) );

    return true;
}


bool CTotalMainWindow::InitializeJigs()
{

	/*******************************************************/
    m_menuFemurJigs = new QMenu("Fem Jigs");
    menus.push_back(m_menuFemurJigs);

	/******************* MAKING JIGS MENU **********************/
	m_actImportOsteophytesJigs		= m_menuFemurJigs->addAction( "Osteophyte" );
	m_actDefineCartilageJigs		= m_menuFemurJigs->addAction( "Cartilage" );
	m_actDefineOutlineProfileJigs	= m_menuFemurJigs->addAction( "Outline" );
	m_actMakeInnerSurfaceJigs		= m_menuFemurJigs->addAction( "Inner Surface" );
	m_actMakeOuterSurfaceJigs		= m_menuFemurJigs->addAction( "Outer Surface" );
	m_actMakeSideSurfaceJigs		= m_menuFemurJigs->addAction( "Side Surface" );
	m_actMakeSolidPositionJigs		= m_menuFemurJigs->addAction( "Position Jigs" );
	m_actDefineStylusJigs			= m_menuFemurJigs->addAction( "Stylus Jigs" );
	m_actMakeAnteriorSurfaceJigs	= m_menuFemurJigs->addAction( "Anterior Surface" );

	m_actSeparator				= m_menuFemurJigs->addSeparator();
	m_actAdvancedControlJigs	= m_menuFemurJigs->addAction( "Adv Ctrl Jigs");
	m_actValidateJigs			= m_menuFemurJigs->addAction( "Validate Jigs" );
	m_actOutputJigs				= m_menuFemurJigs->addAction( "Output Jigs" );

	connect( m_menuFemurJigs, SIGNAL( aboutToShow() ), this, SLOT( OnEnableMakingActionsJigs() ) );
	connect( m_actImportOsteophytesJigs, SIGNAL( triggered() ), m_pView, SLOT( OnImportOsteophyteSurface() ) );
	connect( m_actDefineCartilageJigs, SIGNAL( triggered() ), m_pView, SLOT( OnHandleCartilage() ) );
	connect( m_actDefineOutlineProfileJigs, SIGNAL( triggered() ), m_pView, SLOT( OnDefineOutlineProfileJigs() ) );
	connect( m_actMakeInnerSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeInnerSurfaceJigs() ) );
	connect( m_actMakeOuterSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnHandleOuterSurfaceJigs() ) );
	connect( m_actMakeSideSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeSideSurfaceJigs() ) );
	connect( m_actMakeSolidPositionJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeSolidPositionJigs() ) );
	connect( m_actDefineStylusJigs, SIGNAL( triggered() ), m_pView, SLOT( OnDefineStylusJigs() ) );
	connect( m_actMakeAnteriorSurfaceJigs, SIGNAL( triggered() ), m_pView, SLOT( OnMakeAnteriorSurfaceJigs() ) );
	connect( m_actAdvancedControlJigs, SIGNAL( triggered() ), m_pView, SLOT( OnAdvancedControlJigs() ) );
	connect( m_actValidateJigs, SIGNAL( triggered() ), m_pView, SLOT( OnValidateJigs() ) );
	connect( m_actOutputJigs, SIGNAL( triggered() ), m_pView, SLOT( OnOutputJigs() ) );

	/******************* FILE MENU **********************/
	m_actSeparator				= m_menuFemurJigs->addSeparator();
	m_menuFileJigs				= m_menuFemurJigs->addMenu( "&File" );
	m_actFileJigsImportCADEntity	= m_menuFileJigs->addAction( "Import CAD Entity" );

	connect( m_actFileJigsImportCADEntity, SIGNAL( triggered() ), this, SLOT( OnImportCADEntityJigs() ) );

	/******************* VIEW MENU **********************/
	m_menuViewJigs				= m_menuFemurJigs->addMenu( "&View" );
	m_menuViewTessellationJigs		= m_menuViewJigs->addMenu( "Tessellation" );
	m_actTessellationHighJigs		= m_menuViewTessellationJigs->addAction( "ScreenShot" );
	m_actTessellationFineJigs		= m_menuViewTessellationJigs->addAction( "Fine" );
	m_actTessellationNormalJigs		= m_menuViewTessellationJigs->addAction( "Normal" );
	m_actTessellationCoarseJigs		= m_menuViewTessellationJigs->addAction( "Coarse" );
	m_actTessellationFineJigs->setCheckable(true);
	m_actTessellationNormalJigs->setCheckable(true);
	m_actTessellationCoarseJigs->setCheckable(true);
	m_groupTessellationJigs = new QActionGroup( this );
	m_groupTessellationJigs->addAction(m_actTessellationHighJigs);
	m_groupTessellationJigs->addAction(m_actTessellationFineJigs);
	m_groupTessellationJigs->addAction(m_actTessellationNormalJigs);
	m_groupTessellationJigs->addAction(m_actTessellationCoarseJigs);
	m_actTessellationFineJigs->setChecked(true);
	m_menuViewTransparencyJigs			= m_menuViewJigs->addMenu( "Transparency" );
	m_actTransparencyTransparentTotalJigs= m_menuViewTransparencyJigs->addAction( "TransparentTotal" );
	m_actTransparencyTransparentPlusJigs= m_menuViewTransparencyJigs->addAction( "Transparent++" );
	m_actTransparencyTransparentJigs	= m_menuViewTransparencyJigs->addAction( "Transparent+" );
	m_actTransparencyNormalJigs			= m_menuViewTransparencyJigs->addAction( "Transparent" );
	m_actTransparencyOpaqueJigs			= m_menuViewTransparencyJigs->addAction( "Transparent-" );
	m_actTransparencyOpaquePlusJigs		= m_menuViewTransparencyJigs->addAction( "Transparent--" );
	m_actTransparencyTransparentTotalJigs->setCheckable(true);
	m_actTransparencyTransparentPlusJigs->setCheckable(true);
	m_actTransparencyTransparentJigs->setCheckable(true);
	m_actTransparencyNormalJigs->setCheckable(true);
	m_actTransparencyOpaqueJigs->setCheckable(true);
	m_actTransparencyOpaquePlusJigs->setCheckable(true);
	m_groupTransparencyJigs = new QActionGroup( this );
	m_groupTransparencyJigs->addAction(m_actTransparencyTransparentTotalJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyTransparentPlusJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyTransparentJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyNormalJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyOpaqueJigs);
	m_groupTransparencyJigs->addAction(m_actTransparencyOpaquePlusJigs);
	m_actTransparencyNormalJigs->setChecked(true);

	connect( m_actTessellationHighJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationHigh() ) );
	connect( m_actTessellationFineJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationFine() ) );
	connect( m_actTessellationNormalJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationNormal() ) );
	connect( m_actTessellationCoarseJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTessellationCoarse() ) );
	connect( m_actTransparencyTransparentTotalJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentTotal() ) );
	connect( m_actTransparencyTransparentPlusJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparentPlus() ) );
	connect( m_actTransparencyTransparentJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyTransparent() ) );
	connect( m_actTransparencyNormalJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyNormal() ) );
	connect( m_actTransparencyOpaqueJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaque() ) );
	connect( m_actTransparencyOpaquePlusJigs, SIGNAL( triggered() ), m_pView, SLOT( OnTransparencyOpaquePlus() ) );

	///******************* TOOLS MENU **********************/
	m_menuToolsJigs					= m_menuFemurJigs->addMenu( "&Tools" );
	m_actReviewJigs					= m_menuToolsJigs->addAction( "Review Jigs" );

	connect( m_actReviewJigs, SIGNAL( triggered() ), this, SLOT( OnFemoralJigsReview() ) );

	/******************* INFO MENU **********************/

    return true;
}

void CTotalMainWindow::EnableActions( bool bEnable )
{
    bool hasEntity = !m_pDoc->IsEmpty();
	bool bHasEntityAndEnable = bEnable && hasEntity;

	// Menu
	m_menuFemur->setEnabled( bEnable );
	m_menuFemurJigs->setEnabled( bEnable );
	// Enable Tools 
	m_actAnalyzeImplant->setEnabled( bHasEntityAndEnable );
	//m_actValidateImplant->setEnabled( bHasEntityAndEnable );
	m_actReviewImplant->setEnabled( bHasEntityAndEnable );
	// Enable Info
	m_actUserHistory->setEnabled( hasEntity );
	// Enable view
	m_menuViewTessellation->setEnabled( hasEntity );
	m_menuViewTransparency->setEnabled( hasEntity );
	// Enable AdvancedControl 
	m_actAdvancedControl->setEnabled( bHasEntityAndEnable );
	m_actAdvancedControlJigs->setEnabled( bHasEntityAndEnable );

	// Reset to disabled, then based on the rules to enabled.
	EnableMakingActions( false );
	EnableMakingActions( bHasEntityAndEnable );
	EnableMakingActionsJigs( false );
	EnableMakingActionsJigs( bHasEntityAndEnable );

}

void CTotalMainWindow::EnableMakingActions( bool bEnable)
{
	// If femur not exist, the New should be always available
	if ( GetTotalDoc()->GetFemur() == NULL)
		m_actFileNew->setEnabled( true );
	else
		m_actFileNew->setEnabled( false );

	if ( bEnable )
	{
		// If femur/hip exist, the make femoral axes should be available
		if ( GetTotalDoc()->GetFemur()   != NULL &&
			 GetTotalDoc()->GetHip()     != NULL )
			m_actMakeFemoralAxes->setEnabled( bEnable );
	
		if (m_pDoc->isPosteriorStabilized())//PS
		{
		
		}
		else // CR
		{
		
		}


		// However, if any auto steps = false (manually create JOC steps), 
		// and femoral axes exist,
		// the JOC individual steps should be enabled.
		if ( !GetTotalDoc()->IsAutoCreateJOCSteps() && 
			 !GetTotalDoc()->IsAutoCreateMajorSteps() &&
			 !GetTotalDoc()->IsAutoCreateAllSteps() &&
			 GetTotalDoc()->GetFemoralAxes() )
		{
			//m_actDefineJCurves->setEnabled( bEnable );
			m_actDefineOutlineProfile->setEnabled( bEnable );
			//m_actCutFemur->setEnabled( bEnable );
		}
	}
	else
	{
		m_actMakeFemoralAxes->setEnabled( bEnable );
		
		if (m_actDefinePegs) // NO m_actDefinePegs for PS
			m_actDefinePegs->setEnabled( bEnable );
		if (m_actConvertCastingImplant) // NO m_actConvertCastingImplant for PS
			m_actConvertCastingImplant->setEnabled( bEnable );
		
	}

	if (m_pDoc->isPosteriorStabilized())
	{
	}
}

void CTotalMainWindow::EnableMakingActionsJigs( bool bEnable)
{

	if ( bEnable )
	{
		// whenever femoral implant is complete and up-to-date
		if ( true)
			m_actImportOsteophytesJigs->setEnabled( true );
		else
			m_actImportOsteophytesJigs->setEnabled( false );
		
		// If osteophyte exists, then define cartilage surface should be available
		if ( GetTotalDoc()->GetOsteophyteSurface() != NULL )
			m_actDefineCartilageJigs->setEnabled( bEnable );
		// If cartilage exists, define outline profile jigs should be available
		if ( GetTotalDoc()->GetCartilageSurface() != NULL )
			m_actDefineOutlineProfileJigs->setEnabled( bEnable );
		// If outline profiles jigs exist, make the inner surface jigs should be available
		if ( GetTotalDoc()->GetOutlineProfileJigs() != NULL )
			m_actMakeInnerSurfaceJigs->setEnabled( bEnable );
		// If inner surface jigs exist, make the outer surface jigs should be available
		if ( GetTotalDoc()->GetInnerSurfaceJigs() != NULL )
			m_actMakeOuterSurfaceJigs->setEnabled( bEnable );
		// If outer surface jigs exists, make the side surface jigs should be available
		if ( GetTotalDoc()->GetOuterSurfaceJigs() != NULL )
			m_actMakeSideSurfaceJigs->setEnabled( bEnable );
		// If side surface jigs exists, make the soild position jigs should be available
		if ( GetTotalDoc()->GetSideSurfaceJigs() != NULL )
			m_actMakeSolidPositionJigs->setEnabled( bEnable );
		// If solid position jigs exists, make the stylus jigs should be available
		if ( GetTotalDoc()->GetSolidPositionJigs() != NULL )
			m_actDefineStylusJigs->setEnabled( bEnable );
		// If stylus jigs exists, make the anterior surface jigs should be available
		// Or users want to output regardless.
		bool outputRegardless = false;
		if (GetTotalDoc()->GetAdvancedControlJigsPointer() && GetTotalDoc()->GetAdvancedControlJigs()->GetOutputAnteriorSurfaceRegardless() )
			outputRegardless = true;
		if ( GetTotalDoc()->GetStylusJigs() != NULL || outputRegardless )
			m_actMakeAnteriorSurfaceJigs->setEnabled( bEnable );
	}
	else
	{
		m_actImportOsteophytesJigs->setEnabled( bEnable );
		m_actDefineCartilageJigs->setEnabled( bEnable );
		m_actDefineOutlineProfileJigs->setEnabled( bEnable );
		m_actMakeInnerSurfaceJigs->setEnabled( bEnable );
		m_actMakeOuterSurfaceJigs->setEnabled( bEnable );
		m_actMakeSideSurfaceJigs->setEnabled( bEnable );
		m_actMakeSolidPositionJigs->setEnabled( bEnable );
		m_actDefineStylusJigs->setEnabled( bEnable );
		m_actMakeAnteriorSurfaceJigs->setEnabled( bEnable );
	}
	// If jig design is done, the output should be available
	if ( GetTotalDoc()->IsFemoralJigsDesignComplete() )
		m_actOutputJigs->setEnabled( true );
	else
		m_actOutputJigs->setEnabled( false );
}

QString CTotalMainWindow::GetDataFileVersionInfo()
{
	if (m_pDoc->isProductionSoftware())
		return QString("TriathlonF1 module production version: %1.%2").arg(ITOTAL_RELEASE).arg(ITOTAL_REVISION);
	else
		return QString("TriathlonF1 module beta version: %1.%2.%3.%4").arg(ITOTAL_RELEASE).arg(ITOTAL_REVISION).arg(ITOTAL_DATAFILE_VERSION).arg(GetAppVersion());
}

bool CTotalMainWindow::OnImportSurfaces()
{
    KWaitCursor wait;

    // Check if parts are available
    Implant* implant = mainWindow->GetImplant();

	IwBrep *femurBrep;
	Point3Float hipCenter;

	QString surfDir = mainWindow->GetImplant()->GetInfo("SurfDir");
	QString ImplantName = mainWindow->GetImplant()->GetInfo("ImplantName");
	QString fileName = surfDir + QDir::separator() + ImplantName + QString("_Femur_Inner.IGS");
	femurBrep = ReadIGES(fileName);

	std::map<QString, IwPoint3d> pointMap = IFemurImplant::FemoralAxesInp_PointsDataFromStrykerLayout();

	IwPoint3d hipPoint = pointMap["HIP Point"];

	if(hipPoint.IsInitialized())
	{
		hipCenter = Point3Float(hipPoint);
	}

	if( femurBrep == NULL || !hipPoint.IsInitialized() )
    {
        QMessageBox::information(mainWindow, "Error", "Missing the surface or sketch");
        return false;
    }

	double tessFactor = m_pDoc->GetTessellationFactor();
	m_pView->OnTessellationFine();// set fine tessellation
	bool bOK = m_pDoc->ImportPart( femurBrep, ENT_ROLE_FEMUR );
	if ( !bOK ) // Need to check whether the femoral surface template is loaded correctly.
	{
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Femoral surface error!"), 
										tr("Femoral surface does not exist, or\nfemoral surface template is not correct, or\nfemoral surface quality is not good."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return false;
	}
	m_pDoc->GetPart(ENT_ROLE_FEMUR)->SetColor(CColor( 255, 202, 128 ));

	// For Hip
    bOK = m_pDoc->ImportPart( IwPoint3d(hipCenter.GetX(), hipCenter.GetY(), hipCenter.GetZ()), ENT_ROLE_HIP );
	if ( !bOK )
	{
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Hip error!"), 
										tr("Hip center does not exist."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return false;
	}

	// Hide hip
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_HIP )->GetEntId(), false);
	m_pDoc->GetEntity( ENT_ROLE_HIP )->SetDisplayMode(DISP_MODE_HIDDEN);


	return true;
}

void CTotalMainWindow::OnImportCADEntity()
{
	m_pDoc->AppendLog( QString("CTotalMainWindow::OnImportCADEntity()") );

	IwAxis2Placement transM = GetImportCADEntityTransformationMatrix();

	// The default entRole=ENT_ROLE_NONE.
	CEntRole entRole = ENT_ROLE_NONE;
	CMainWindow::ImportCADEntity(entRole, &transM);


	return;
}

IwAxis2Placement CTotalMainWindow::GetImportCADEntityTransformationMatrix()
{
	////////////////////////////////////////////////////////////////////////////
	// Let's determine whether we need additional transformation 
	IwAxis2Placement transM;

	// Transform from world (0,0,0) to implant mechanical axis
	if ( GetTotalDoc()->GetAdvancedControl()->GetImportFromNormalized() && 
		 GetTotalDoc()->GetFemoralAxes() )
	{
		// Get femoral mech axis
		IwAxis2Placement mechAxis;
		GetTotalDoc()->GetFemoralAxes()->GetMechanicalAxes(mechAxis);
		double refSize = GetTotalDoc()->GetFemoralAxes()->GetEpiCondyleSize()/76.0;
		IwVector3d scale = IwVector3d(refSize,refSize,refSize);
		transM = mechAxis;
	}
	// A method to transform from a SolidWorks start part position to align with femoral implant
	else if ( !(GetTotalDoc()->GetAdvancedControl()->GetImportRotationXYZ()==IwVector3d(0,0,0)) ||
		      !(GetTotalDoc()->GetAdvancedControl()->GetImportTranslationXYZ()==IwVector3d(0,0,0)) )
	{
		IwAxis2Placement rotM = IwAxis2Placement();
		IwVector3d rotation = GetTotalDoc()->GetAdvancedControl()->GetImportRotationXYZ();
		// Rotate along x
		if ( !IS_EQ_TOL6(rotation.x, 0.0) )
		{
			IwAxis2Placement rotX;
			rotX.RotateAboutAxisAtPoint(rotation.x/180.0*IW_PI, IwPoint3d(0,0,0), IwVector3d(1,0,0));
			rotM = rotX;
		}
		// Rotate along y
		if ( !IS_EQ_TOL6(rotation.y, 0.0) )
		{
			IwAxis2Placement rotY;
			rotY.RotateAboutAxisAtPoint(rotation.y/180.0*IW_PI, IwPoint3d(0,0,0), IwVector3d(0,1,0));
			rotM.TransformAxis2Placement(rotY, rotM);//rotM3=rotY*rotM1 (rotM3=rotY*rotX)
		}
		// Rotate along z
		if ( !IS_EQ_TOL6(rotation.z, 0.0) )
		{
			IwAxis2Placement rotZ;
			rotZ.RotateAboutAxisAtPoint(rotation.z/180.0*IW_PI, IwPoint3d(0,0,0), IwVector3d(0,0,1));
			rotM.TransformAxis2Placement(rotZ, rotM);//rotM3=rotZ*rotM1 (rotM3=rotZ*rotY*rotX)
		}
		// Get implant axis info
		IwAxis2Placement wslAxes;
		GetTotalDoc()->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
		// transform to the alignment position
		IwVector3d translation = GetTotalDoc()->GetAdvancedControl()->GetImportTranslationXYZ();
		IwAxis2Placement transAlignment = IwAxis2Placement(translation, wslAxes.GetXAxis(), wslAxes.GetYAxis());

		rotM.TransformAxis2Placement(transAlignment, transM);//transM=transAlignment*rotM
	}
	// A general transformation matrix for import parts.
	else if ( !(GetTotalDoc()->GetAdvancedControl()->GetImportTransformationX()==IwVector3d(0,0,0)) ||
			  !(GetTotalDoc()->GetAdvancedControl()->GetImportTransformationY()==IwVector3d(0,0,0)) ||
		      !(GetTotalDoc()->GetAdvancedControl()->GetImportTransformationT()==IwVector3d(0,0,0)) )
	{
		transM = IwAxis2Placement(GetTotalDoc()->GetAdvancedControl()->GetImportTransformationT(),
								  GetTotalDoc()->GetAdvancedControl()->GetImportTransformationX(),
								  GetTotalDoc()->GetAdvancedControl()->GetImportTransformationY());
	}

	return transM;
}

void CTotalMainWindow::OnImportCADEntityJigs()
{
	m_pDoc->AppendLog( QString("CTotalMainWindow::OnImportCADEntityJigs()") );

	IwAxis2Placement transM = GetImportCADEntityTransformationMatrix();

	// The default entRole=ENT_ROLE_JIGS_NONE. 
	CEntRole entRole = ENT_ROLE_JIGS_NONE;
	CMainWindow::ImportCADEntity(entRole, &transM);


	return;
}


bool CTotalMainWindow::ImportIgesPart( CEntRole eEntRole, const QString& sTitle )
{
	QString					sFileName, sHomeDir, sPathDir, sCaseNumber;
	bool					bOK = false;

	sHomeDir = GetStringPref( "Last used part folder" );

	int length = sHomeDir.length();
	int lastIndex = sHomeDir.lastIndexOf(QDir::separator());
	sCaseNumber = sHomeDir.right(length-lastIndex-1);	

	sPathDir = sHomeDir + QDir::separator() + QString(tr("Surfaces")) + QDir::separator();

	if ( eEntRole == ENT_ROLE_FEMUR )
	{
		sFileName = sPathDir + sCaseNumber + QString(tr("_Femur_Inner.igs"));
	}
	else if ( eEntRole == ENT_ROLE_HIP )
	{
		sFileName = sPathDir + sCaseNumber + QString(tr("_hip.igs"));
	}
	else
		return bOK;

	m_pView->Redraw();

	SetStringPref( "Last used part folder", sHomeDir );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	bOK = m_pDoc->ImportPart( sFileName, eEntRole );

	QApplication::restoreOverrideCursor();

	if( !bOK )
		return bOK;

	m_pView->Redraw();

	EnableActions( true );

	return true;
}

bool CTotalMainWindow::ImportXMLPart( CEntRole eEntRole, const QString& sTitle )
{
	QString					sFileName, sHomeDir, sPathDir, sCaseNumber;
	bool					bOK = false;

	sHomeDir = GetStringPref( "Last used part folder" );

	int length = sHomeDir.length();
	int lastIndex = sHomeDir.lastIndexOf(QDir::separator());
	sCaseNumber = sHomeDir.right(length-lastIndex-1);	

	sPathDir = sHomeDir + QDir::separator() + QString(tr("Segmentation")) + QDir::separator();

	if ( eEntRole == ENT_ROLE_FEMUR )
		sFileName = sPathDir + sCaseNumber + QString(tr("_Femur_Inner.xml"));
	else if ( eEntRole == ENT_ROLE_HIP )
		sFileName = sPathDir + sCaseNumber + QString(tr("_Hip.xml"));
	else
		return bOK;

	m_pView->Redraw();

	SetStringPref( "Last used part folder", sHomeDir );

	bOK = m_pDoc->ImportPart( sFileName, eEntRole );

	if( !bOK )
		return bOK;

	m_pView->Redraw();

	EnableActions( true );

	return true;
}

void CTotalMainWindow::OnFileNew()
{
	if ( !m_pDoc->IsEmpty() )
	{
		bool closed = FileClose();
		if ( !closed ) 
		{
			m_pView->Redraw();

			Qt::CursorShape shape = Qt::ArrowCursor;
			QApplication::setOverrideCursor(QCursor(shape));
			return;
		}
	}

	FileNew();

	Qt::CursorShape shape = Qt::ArrowCursor;
	QApplication::setOverrideCursor(QCursor(shape));
}

bool CTotalMainWindow::FileNew()
{
	m_pDoc->InitializeLogFile();
	
	m_pDoc->AppendLog( QString("CTotalMainWindow::FileNew().") );

	// set the version info to the latest
	m_pDoc->SetOpeningFileReleaseNumber(ITOTAL_RELEASE);
	m_pDoc->SetOpeningFileRevisionNumber(ITOTAL_REVISION);
	m_pDoc->SetOpeningFileDataFileVersionNumber(ITOTAL_DATAFILE_VERSION);

	Viewport* vp = viewWidget->GetViewport();

	Implant* implant = mainWindow->GetImplant();
 
	m_pDoc->SetFileName("");
	m_pView->OnTessellationFine();
	bool succeedSurface = OnImportSurfaces();

	// Set home dir
    SetStringPref( "Last used part folder", implant->GetInfo("SolidWorksDir") /*+ QDir::separator() + QString("Femur")*/ );
    EnableActions( true );

	if ( !succeedSurface )
	{
		FileCloseSilent();// initialize again
		return false;
	}

    // Add entity panel to the entity widget; and view to the viewport, if not yet
	if ( !entityWidget->IsItemExist(m_pEntPanel) )
	{
		entityWidget->AddItem( m_pEntPanel );
	}
	if ( !vp->IsViewExist(m_pView) )
	{
		vp->AddView( m_pView );
	}

	// 
    CBounds3d bnd = m_pDoc->ComputeBounds(CTransform());
	if( (bnd.pt0-bnd.pt1).Length()<0.001 ) 
	{
        QTimer::singleShot(3000, this, SLOT(OnBestFit()));
	}
	else
	{
		OnBestFit();
	}

	return true;
}

void CTotalMainWindow::OnBestFit()
{
	Viewport* vp = viewWidget->GetViewport();
	vp->BestFit();
	m_pView->Redraw();
}

void CTotalMainWindow::OnSaveNewFile()
{
    if( m_pDoc->IsEmpty() )
        return;
	m_pDoc->AppendLog( QString("CTotalMainWindow::OnSaveNewFile()") );

	QString sDir = GetStringPref( "Last used part folder" );
	QString filter = QString("TriathlonF1 Documents (*.tw)");
	QString defaultFileName = GetFileDefaultName();
	QString	sDocName;
	CTotalDocDlg dlg( mainWindow, SAVE_DOC, sDir, filter );
	dlg.selectFile(defaultFileName);
	if( dlg.exec() )
	{
		sDocName = dlg.selectedFiles()[0];
	}
	if ( sDocName.isEmpty() )
		return;

	if( !sDocName.endsWith( ".tw" ) )
		sDocName += ".tw";

	bool incrementSave = false;
	m_pDoc->Save( sDocName, incrementSave );// new file, incremental should be false

}

void CTotalMainWindow::OnFileSave(bool bWarning)
{
    if( m_pDoc->IsEmpty() )
        return;

	m_pDoc->AppendLog( QString("CTotalMainWindow::OnFileSave()") );

	QString					sDocName = m_pDoc->GetFileName();
	QString					sDir = m_pDoc->GetFileFolder();

	if ( m_pDoc->IsReadOnlyFile() )
	{
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Read only file!"), 
										tr("File is read only. Can not save the file."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return;
	}

	if ( sDocName != "" )// existing files
	{
		// Check valid file name and folder, in case
		if( sDir.endsWith( ".tw" ) )
		{
			int		button;
			button = QMessageBox::question( mainWindow, 
											tr("Not a valid file name!"), 
											QString(tr("This is not a valid file name.\n"
											"%1\n"
											"Can not save the file.")).arg(sDocName), 
											QMessageBox::Abort );	

			if( button == QMessageBox::Abort ) return;
		}

		// Check whether still in desiging (in a design step)
		CManager *pManager = m_pView->GetManager();
		if ( pManager )
		{
			if ( bWarning && pManager->GetCancelActionExistence() ) // if the manager have a cancel button, warn the users before saving the file
			{
				int		button;
				button = QMessageBox::warning( mainWindow, 
												tr("Saving warning!"),
												tr("Saving data inside %1\nwill make the cancel button disabled.\nDo you want to proceed saving?").arg(pManager->GetClassName()), 
												QMessageBox::Ok, QMessageBox::Abort);	

				if( button == QMessageBox::Abort ) 
				{
					return;
				}
				else
				{
					pManager->EnableCancelAction(false);
				}
			}
		}

		bool incrementSave = true;
		m_pDoc->Save( sDocName, incrementSave);
	}
	else // new files
	{
		sDir = GetStringPref( "Last used part folder" );
		QString filter = QString("TriathlonF1 Documents (*.tw)");//QString("*TriathlonF1 Documents .tw");
		CTotalDocDlg				dlg( mainWindow, SAVE_DOC, sDir, filter );

		QString defaultFileName = GetFileDefaultName();

		char *specifyAFileName = getenv("ITOTAL_UNDERDEVELOPMENT");
		if (0 && specifyAFileName != NULL && QString("1") == specifyAFileName)// under-development mode to optionally save with different names
		{
			dlg.selectFile(defaultFileName);
			if( dlg.exec() )
			{		
				sDocName = dlg.selectedFiles()[0];
			}
		}
		else
		{
			sDocName = sDir + QDir::separator() + defaultFileName;// production mode always uses default file name.
		}

		if( !sDocName.endsWith( ".tw" ) )
			sDocName += ".tw";

		if ( m_pDoc->FolderExists(sDocName) )
		{
			int		button;
			button = QMessageBox::question( mainWindow, 
											tr("Save the current document?"), 
											tr("File name has already existed.\n"
											"Do you want to overwrite it?"), 
											QMessageBox::Yes, QMessageBox::Cancel );	

			if( button == QMessageBox::Cancel ) return;
		}

		sDir = GetFileDir( sDocName );

		// Check valid file name and folder, in case
		if( sDir.endsWith( ".tw" ) )
		{
			int		button;
			button = QMessageBox::question( mainWindow, 
											tr("Not a valid file name!"), 
											QString(tr("This is not a valid file name.\n"
											"%1\n"
											"Can not save the file.")).arg(sDocName), 
											QMessageBox::Abort );	

			if( button == QMessageBox::Abort ) return;
		}

		SetStringPref( "Last used part folder", sDir );

		SetStringPref( "Last used part name", sDocName );

		QString strSolidWorkPath = "" /*GetFileDir(sDir)*/;

		strSolidWorkPath = sDir +QDir::separator() + defaultFileName;

		bool incrementSave = false;
		m_pDoc->Save( strSolidWorkPath, incrementSave );// new file, incremental should be false

		// Change log file directory to the *.tw  
		m_pDoc->SetLogFileDir(sDocName);
	}
}

void CTotalMainWindow::FileSaveWithDefaultName(QString sDir)
{
	m_pDoc->AppendLog(QString("CTotalMainWindow::FileSaveWithDefaultName()"));

	QString defaultFileName = sDir + QDir::separator() + GetFileDefaultName();

	m_pDoc->AppendLog("File Name: " + defaultFileName);

	m_pDoc->Save( defaultFileName, false );
}

QString	CTotalMainWindow::GetFileDefaultName()
{
	// Get side info
	QString sideName, sideLR;
	m_pDoc->GetImplantSide(sideName);
	if ( sideName == "Left" )
	{
		sideLR = "L";
	}
	else if (sideName == "Right" )
	{
		sideLR = "R";
	}

	// 
	QString defaultFileName = m_pDoc->GetPatientId() + QString(" ") + sideLR + QString(" ") + QString("Femoral Implant.tw");

	return defaultFileName;
}

void CTotalMainWindow::OnFileSaveACopy()
{
	m_pDoc->AppendLog( QString("CTotalMainWindow::OnFileSaveACopy()") );

	QString					sDocName = m_pDoc->GetFileName();
	QString					sDir = m_pDoc->GetFileFolder();
	QString					sNewDocName, sNewDir;

	if ( m_pDoc->IsReadOnlyFile() )
	{
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Read only file!"), 
										tr("File is read only. Can not save a copy."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return;
	}

	if ( sDocName.isEmpty() )
	{
		int		button;
		button = QMessageBox::critical( mainWindow, 
										tr("Save file"), 
										tr("No file name yet. Need to save the file before saving a copy."), 
										QMessageBox::Abort);	

		if( button == QMessageBox::Abort ) return;
	}

	QString filter = QString("TriathlonF1 Documents (*.tw)");
	CTotalDocDlg				dlg( mainWindow, SAVE_DOC, sDir, filter );

	if( dlg.exec() )
	{
		sNewDocName = dlg.selectedFiles()[0];

		if( !sNewDocName.endsWith( ".tw" ) )
			sNewDocName += ".tw";

		if ( m_pDoc->FolderExists(sNewDocName) )
		{
			int		button;
			button = QMessageBox::question( mainWindow, 
											tr("Save the current document?"), 
											tr("File name has already existed.\n"
											"Do you want to overwrite it?"), 
											QMessageBox::Yes, QMessageBox::Cancel );	

			if( button == QMessageBox::Cancel ) return;
		}

		sNewDir = GetFileDir( sNewDocName );

		// Something wrong
		if( sNewDir.endsWith( ".tw" ) )
		{
			int		button;
			button = QMessageBox::question( mainWindow, 
											tr("Not a valid file name!"), 
											QString(tr("This is not a valid file name.\n"
											"%1\n"
											"Can not save the file.")).arg(sNewDocName), 
											QMessageBox::Ok );	

			if( button == QMessageBox::Ok ) return;
		}

		// Save itself first
		OnFileSave();

		QApplication::setOverrideCursor( Qt::WaitCursor );

		// Copy all files to target folder
		m_pDoc->SaveAnotherCopy(sDocName, sNewDocName);

		QApplication::restoreOverrideCursor();

	}
}

void CTotalMainWindow::OnFileOpen(QString &elem)
{
	if ( !m_pDoc->IsEmpty() )
	{
		bool closed = FileClose();
		if ( !closed ) 
		{
			m_pView->Redraw();
			return;
		}
	}

	bool succeed = FileOpen(elem);
	if ( !succeed ) 
	{
		FileCloseSilent();// initialize doc
	}

//	this->EnableActions(true);
	m_pDoc->SetReadOnlyFile(false);
	m_pDoc->SetFileSaveAuto(false);

}

void CTotalMainWindow::OnFileOpenWithOptions(QString &elem, QString options)
{
	// onlyLoad indicates which module data to be loaded? ""=all or "FemBasis", "FemPS", "FemJigs"

	// if load all or load fem basis, then m_pDoc should be empty.
	if ( options == "" || options == "FemBasis" )
	{
		if ( !m_pDoc->IsEmpty() )
		{
			bool closed = FileClose();
			if ( !closed ) 
			{
				m_pView->Redraw();
				return;
			}
		}
	}

	bool succeed = FileOpen(elem, options);
	if ( !succeed ) 
	{
		FileCloseSilent();// initialize doc
	}

//	this->EnableActions(true);
	m_pDoc->SetReadOnlyFile(false);
	m_pDoc->SetFileSaveAuto(false);

}


void CTotalMainWindow::OnFileOpenAsReadOnly()
{
	if ( !m_pDoc->IsEmpty() )
	{
		bool closed = FileClose();
		if ( !closed ) 
		{
			m_pView->Redraw();
			return;
		}
	}

	bool succeed = FileOpen();
	if ( !succeed ) 
		FileCloseSilent();// initialize doc

	m_pDoc->SetReadOnlyFile(true);
	m_pDoc->SetFileSaveAuto(false);
//	m_actFileSaveACopy->setEnabled(false);
}

//////////////////////////////////////////////////////////////
// sDocName can be given.
//////////////////////////////////////////////////////////////
bool CTotalMainWindow::FileOpen(QString &elem, QString options)
{
	// if load all or load fem basis, then log file should be Initialized.
	if ( options == "" || options == "FemBasis" )
		m_pDoc->InitializeLogFile(elem);

	//m_pDoc->InitializeTimeLog();

	QString					sDocName;
	QString					sDir;
	bool					bOK;
    // get the TotalWorks file name from the xml node
    if( elem == "" )
		return false;
	else
    	sDocName = elem;

	if( !sDocName.endsWith( ".tw" ) )
		sDocName += ".tw";
		
	sDir = GetFileDir( sDocName );

	SetStringPref( "Last used part folder", sDir );

	SetStringPref( "Last used part name", sDocName );

	m_pDoc->SetFileName(sDocName);

	if( m_sRecordDir == "" )
		m_sRecordDir = sDir;

	bOK = m_pDoc->LoadWithOptions( sDocName, options );

	if( !bOK )
	{
		return false;
	}

    // Add entity panel to the entity widget; and view to the viewport, if not yet
	if ( !entityWidget->IsItemExist(m_pEntPanel) )
	{
		entityWidget->AddItem( m_pEntPanel );
	}
	if ( !viewWidget->GetViewport()->IsViewExist(m_pView) )
	{
		viewWidget->GetViewport()->AddView( m_pView );
	}
 		
	m_pView->Redraw();

	m_pDoc->SetModified(false);// initialize modified flag

	return true;
        
}

void CTotalMainWindow::OnFileClose()
{
	FileClose();
}

bool CTotalMainWindow::FileClose(bool withCancel)
{
	m_pDoc->AppendLog( QString("CTotalMainWindow::FileClose()") );

	// if auto file save, just save the file.
	if ( !m_pDoc->IsReadOnlyFile() && m_pDoc->IsFileSaveAuto() )
		OnFileSave();

	FileCloseSilent();

	return true;
}
/////////////////////////////////////////////////////////////////
// Close the file without any error messages
/////////////////////////////////////////////////////////////////

void CTotalMainWindow::FileCloseSilent()
{
    m_pDoc->AppendLog( QString("CTotalMainWindow::FileCloseSilent()") );
    QApplication::setOverrideCursor( Qt::WaitCursor );

	ResetViewDefaultSettings();

	m_pView->ClearView();

	m_pView->Redraw();

	m_pDoc->TerminateLogFile();

	m_pDoc->Clear();

	QApplication::restoreOverrideCursor();

	EnableActions( false );

	m_menuFemur->setEnabled( true ); // after close file, make "Femural Implant" menu enable.
	m_menuFemurJigs->setEnabled( true ); // after close file, make "Femural Jig" menu enable.

}

void CTotalMainWindow::OnEnableMakingActions()
{
	bool managerExists = false;
	if ( m_pView->GetManager() )
		managerExists = true;

	EnableMakingActions( false );
	EnableMakingActions( !managerExists );
	// Handle tessellation & transparency
	UpdateTessellationAndTransparencyToggles();

}

void CTotalMainWindow::OnEnableMakingActionsJigs()
{
	bool managerExists = m_pView->GetManager();

	bool allComplete;
	if (m_pDoc->isPosteriorStabilized()) // PS
		allComplete = IFemurImplant::IsFemurImplantComplete();//PSV--- PlanB
	else // CR
		allComplete = IFemurImplant::IsFemurImplantComplete();

	EnableMakingActionsJigs(false);
	EnableMakingActionsJigs( allComplete && !managerExists );

	// Handle tessellation & transparency
	UpdateTessellationAndTransparencyTogglesJigs();
}

void CTotalMainWindow::OnCleanUpTempDisplay()
{
	m_pDoc->CleanUpTempPoints();
}

void CTotalMainWindow::LoadingFileFinished()
{
	m_menuFile->setEnabled(true);

	m_pView->Redraw();
}

void CTotalMainWindow::SetJCOSToolbarVisible(bool bVisible) 
{
	if (JCOSToolbar) JCOSToolbar->setVisible(bVisible);
};

void CTotalMainWindow::OnFemoralImplantReview()
{
	Implant* implant = mainWindow->GetImplant();
	// Hide all entities
	implant->HideAll();
	// Change the viewport layout to 3D view only
	// Start Review
	GetView()->OnActivateNextReviewManager();
}

void CTotalMainWindow::OnFemoralJigsReview()
{
	mainWindow->GetImplant()->HideAll();
	GetView()->OnActivateNextJigsReviewManager();
}

bool CTotalMainWindow::AllStepsComplete(std::string const &phase, std::string const &subphase) const
{
   // this is where all the items are checked to be "ready"
   if (phase == "Implant" /*"Profile"*/ && subphase == "Design")
   {
      if (CTotalDoc* pDoc = const_cast<CTotalMainWindow*>(this)->GetTotalDoc())
         return pDoc->IsFemoralImplantDesignComplete()
               && pDoc->IsFemoralJigsDesignComplete();
   }

   return true;
}

bool CTotalMainWindow::OutputResults() const // virtual -- overridden
{
    bool rv = false;

    if (CTotalDoc* pDoc = dynamic_cast<CTotalDoc*>(m_pDoc))
    {
        if (!pDoc->GetFileName().isEmpty())
        {
            QString dummy;
            QString folder = mainWindow->GetImplant()->GetInfo("ExpLocFemur");
            // QString folder = pDoc->GetFileName() + "_Outputs";
            QDir dir(folder);
            if ( !dir.exists() )
                dir.mkdir(folder);

            if (!folder.endsWith('\\'))
                folder.append('\\');

			bool rv1;
            QString folderJigs = mainWindow->GetImplant()->GetInfo("ExpLocJigs");
            QDir dir_jigs(folderJigs);
            if (!dir_jigs.exists())
                dir_jigs.mkdir(folderJigs);

            if (!folderJigs.endsWith('\\'))
                folderJigs.append('\\');

            unique_ptr<OutputterOfResultsJigs> pOutJigs(new OutputterOfResultsJigs(pDoc));
            bool rv2 = pOutJigs->DoValidate(folderJigs, dummy) && pOutJigs->DoOutput(folderJigs);

            rv = rv1 && rv2;
        }
    }

    return rv;
}

void CTotalMainWindow::HideAllEntities()
{
	GetTotalDoc()->HideAllFemurImplantEntities();
	GetTotalDoc()->HideAllFemurJigsEntities();
}

void CTotalMainWindow::emitDesignStageCommenced(int i)
{
    emit DesignStageCommenced(this, i);
}

void CTotalMainWindow::emitDesignStageComplete(int i)
{
    emit DesignStageComplete(this, i);
}

bool CTotalMainWindow::IsAllProfileDesignComplete()
{
	bool allComplete;
	if (m_pDoc->isPosteriorStabilized()) // PS
		allComplete = IFemurImplant::IsFemurImplantComplete()
					  && IFemurJigs::IsFemurJigsComplete();
	else // CR
		allComplete = IFemurImplant::IsFemurImplantComplete()
					   && IFemurJigs::IsFemurJigsComplete();

	return allComplete;
}

void CTotalMainWindow::UpdateTessellationAndTransparencyToggles()
{
	if ( m_pDoc == NULL)
		return;

	if ( IS_EQ_TOL6(0.1, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationHigh->setChecked(true);
	}
	else if ( IS_EQ_TOL6(0.33, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationFine->setChecked(true);
	}
	else if ( IS_EQ_TOL6(1.0, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationNormal->setChecked(true);
	}
	else if ( IS_EQ_TOL6(3.0, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationCoarse->setChecked(true);
	}
	
	if ( IS_EQ_TOL6(0, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyTransparentTotal->setChecked(true);
	}
	else if ( IS_EQ_TOL6(16, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyTransparentPlus->setChecked(true);
	}
	else if ( IS_EQ_TOL6(32, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyTransparent->setChecked(true);
	}
	else if ( IS_EQ_TOL6(64, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyNormal->setChecked(true);
	}
	else if ( IS_EQ_TOL6(144, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyOpaque->setChecked(true);
	}
	else if ( IS_EQ_TOL6(224, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyOpaquePlus->setChecked(true);
	}

}

void CTotalMainWindow::UpdateTessellationAndTransparencyTogglesJigs()
{
	if ( m_pDoc == NULL)
		return;

	if ( IS_EQ_TOL6(0.1, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationHighJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(0.33, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationFineJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(1.0, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationNormalJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(3.0, m_pDoc->GetTessellationFactor()) )
	{
		m_actTessellationCoarseJigs->setChecked(true);
	}
	
	if ( IS_EQ_TOL6(0, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyTransparentTotalJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(16, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyTransparentPlusJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(32, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyTransparentJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(64, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyNormalJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(144, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyOpaqueJigs->setChecked(true);
	}
	else if ( IS_EQ_TOL6(224, m_pDoc->GetTransparencyFactor() ) )
	{
		m_actTransparencyOpaquePlusJigs->setChecked(true);
	}
}
