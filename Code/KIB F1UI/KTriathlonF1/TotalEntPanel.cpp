#include "TotalEntPanel.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "AdvancedControl.h"
#include "FemoralAxes.h"
#include "FemoralPart.h"
#include "..\KApp\SwitchBoard.h"
#include "..\KApp\EntityWidget.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\MainWindow.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\Utilities.h"
#include <assert.h>

CTotalEntPanel::CTotalEntPanel(QTreeView* view) : CEntPanel(view)
{
	SwitchBoard::addSignalSender(SIGNAL(AlignedFeaturesNeedToChange()), this);
	SwitchBoard::addSignalReceiver(SIGNAL(SubdivMeshChanged(QString)), this, SLOT(OnSubdivMeshChanged(QString)));
	SwitchBoard::addSignalReceiver(SIGNAL(SketchSliceChanged(QString)), this, SLOT(OnSketchSliceChanged(QString)));
	
}

CTotalEntPanel::~CTotalEntPanel()
{
	SwitchBoard::removeSignalSender(this);
	SwitchBoard::removeSignalReceiver(this);
}

void CTotalEntPanel::Add( int id, const QString& sName, CEntRole eRole )
{
    // create the 'Femoral Implant' root item
    if( items.size()==0 )
    {
        QStandardItem* item = NewItem("Femoral Implant", false, false);

        QFont font = item->font();
        font.setBold(true);
        font.setItalic(true);
        item->setFont(font);

        items.append( item );

		if ( !entityWidget->IsItemExist(this) )
			entityWidget->AddItem( this );
    }

	CPanelElem		el;

	el.id			= id;
	el.eEntRole		= eRole;
	el.sEntName		= sName;

	m_list.append( el );

	QStandardItem*		itemId = NewItem("", true, false, true);

	CEntity*	pEnt = m_pDoc->GetEntityById( id );

	if ( pEnt )
		itemId->setFlags( Qt::ItemIsEnabled );
	else
		itemId->setFlags( Qt::NoItemFlags );

	QString sEntName = sName;
	if ( pEnt && !pEnt->GetUpToDateStatus())
	{
		sEntName = "*" + sEntName;// Add "*" if entity is out of date
	}

	itemId->setData( sEntName, 0 );
	itemId->setFlags( Qt::ItemIsEnabled );
	itemId->setSelectable(false);

	if (eRole == ENT_ROLE_FEMORAL_AXES ||
		eRole == ENT_ROLE_FEMORAL_CUTS ||
		eRole == ENT_ROLE_OUTLINE_PROFILE )
	{
		QFont font = itemId->font();
		font.setBold(true);
		itemId->setFont(font);
		QString message;
		CEntValidateStatus validateStatus = pEnt->GetValidateStatus(message);
		if ( !message.isEmpty() )
			itemId->setToolTip(message);
		if ( validateStatus == VALIDATE_STATUS_RED )// red
			itemId->setForeground( QBrush( QColor(darkRed[0],darkRed[1],darkRed[2]) ) );
		else if ( validateStatus == VALIDATE_STATUS_YELLOW ) // yellow
			itemId->setForeground( QBrush(QColor(darkYellow[0],darkYellow[1],darkYellow[2])) ); 
		else if ( validateStatus == VALIDATE_STATUS_GREEN ) // green
			itemId->setForeground( QBrush(QColor(darkGreen[0],darkGreen[1],darkGreen[2])) );
		else	// black
			itemId->setForeground( QBrush(QColor(black[0],black[1],black[2])) );		
	}

	if(pEnt && pEnt->Has2ndItem())
		items[0]->appendRow( QList<QStandardItem*>() << itemId << pEnt->Get2ndItem());
	else
		items[0]->appendRow( QList<QStandardItem*>() << itemId << NewItem(""));

	if ( pEnt )
	{
		if( pEnt->GetDisplayMode() != DISP_MODE_NONE && pEnt->GetDisplayMode() != DISP_MODE_HIDDEN )
			itemId->setCheckState( Qt::Checked );
		else
			itemId->setCheckState( Qt::Unchecked );
	}

}

void CTotalEntPanel::OnItemClicked(const QModelIndex& index)
{
    if( items[0]->index() == index )
        return; // root node

    // sub node
    int row = index.row();
    assert( row >= 0 && row < m_list.size() );

	ItemClicked( index );
}

void CTotalEntPanel::OnItemRightClicked(const QModelIndex& index)
{
    if( items[0]->index() == index )
        return; // root node

    // sub node
    int row = index.row();
    assert( row >= 0 && row < m_list.size() );

	m_id = m_list[row].id;
	CEntity*	pEntity = m_pDoc->GetEntityById( m_id );
	if ( pEntity == NULL )
		return;

	// the items for femoral CR implant 
	if ( pEntity->GetEntRole() >= ENT_ROLE_NONE &&
		 pEntity->GetEntRole() <= ENT_ROLE_IMPLANT_END )
	{
		DisplayContextMenu( row );
	}
}

void CTotalEntPanel::OnItemDoubleClicked(const QModelIndex& index)
{
}

void CTotalEntPanel::DisplayContextMenu( int row ) 
{
	// initial data
	m_actDisplay = NULL;
	m_actRedefine = NULL;
	m_actRegenerateAStep = NULL;
	m_actRegenerateMultipleSteps = NULL;
	m_actRetessellate = NULL;
	m_actRegenerateAStep = NULL;

	CEntity*	pEntity;

	m_id = m_list[row].id;
	pEntity = m_pDoc->GetEntityById( m_id );

	if ( pEntity == NULL )
		return;

	// skip if the item is not for femoral implant
	if ( pEntity->GetEntRole() < ENT_ROLE_NONE ||
		 pEntity->GetEntRole() > ENT_ROLE_IMPLANT_END )
	{
		return;
	}

	QMenu		Menu;
	QMenu*		pMenuDisplayMode( &Menu );

	// Check whether any manager is active.
	bool managerExists = false;
	if ( m_pDoc->GetView()->GetManager() || m_pDoc->GetView()->GetGlobalManager() )
		managerExists = true;

	// Use environment variable to activate ExportIGES
	bool enableExportIGES = false;
	char *envExportIGES = getenv("ITOTAL_UNDERDEVELOPMENT");
	if (envExportIGES != NULL && QString("1") == envExportIGES)
	{
		enableExportIGES = true;
	}

	switch( pEntity->GetEntType() )
	{
		case ENT_TYPE_PART:
		case ENT_TYPE_FEMORAL_PART:
			{
			if( pEntity->GetEntRole() == ENT_ROLE_FEMUR ||
				pEntity->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE )
			{
				//if ( !managerExists )
				//{
				//	m_actRedefine		= Menu.addAction( "Edit" );
				//	connect( m_actRedefine,		SIGNAL( triggered() ),	this, SLOT( OnRedefine() ) );
				//}
			}
			if( pEntity->GetEntRole() == ENT_ROLE_OUTLINE_PROFILE)
			{
				if ( !managerExists )
				{
					//m_actRegenerateAStep	= Menu.addAction( "Regen Itself" );
					//connect( m_actRegenerateAStep,		SIGNAL( triggered() ),	this, SLOT( OnRegenerateAStep() ) );

					//// if itself is update-to-date or the previous one is out-of-date
					//if ( pEntity->GetUpToDateStatus() ||
					//	!m_pDoc->GetEntity( (CEntRole)(pEntity->GetEntRole()-1) )->GetUpToDateStatus())
					//{
					//	m_actRegenerateAStep->setEnabled(false);
					//}

					//m_actRegenerateMultipleSteps	= Menu.addAction( "Regen Through" );
					//connect( m_actRegenerateMultipleSteps,		SIGNAL( triggered() ),	this, SLOT( OnRegenerateMultipleSteps() ) );
					//// if itself is update-to-date or the previous one is out-of-date
					//if ( pEntity->GetUpToDateStatus() ||
					//	!m_pDoc->GetEntity( (CEntRole)(pEntity->GetEntRole()-1) )->GetUpToDateStatus())
					//{
					//	m_actRegenerateMultipleSteps->setEnabled(false);
					//}
				}
			}
			
			pMenuDisplayMode	= Menu.addMenu( "Display Mode" );
			m_actDisplay		= pMenuDisplayMode->addAction( "Display" );
			m_actTransparency	= pMenuDisplayMode->addAction( "Transparency" );
			m_actWireframe		= pMenuDisplayMode->addAction( "Wireframe" );

			m_actDisplay->setCheckable( true );
			m_actTransparency->setCheckable( true );
			m_actWireframe->setCheckable( true );

			// Only this 1 entity (and 5 entities in jigs) can display iso curves. 
			if ( pEntity->GetEntRole() == ENT_ROLE_FEMUR )
			{
				m_actDisplayNet			= pMenuDisplayMode->addAction( "Display Net" );
				m_actTransparencyNet	= pMenuDisplayMode->addAction( "Transparency Net" );
				m_actDisplayNet->setCheckable( true );
				m_actTransparencyNet->setCheckable( true );
  				connect( m_actDisplayNet,		SIGNAL( triggered() ),	this, SLOT( OnDisplayNet() ) );
  				connect( m_actTransparencyNet,	SIGNAL( triggered() ),	this, SLOT( OnTransparencyNet() ) );
			}

			m_actHide			= pMenuDisplayMode->addAction( "Hide" );
			m_actHide->setCheckable( true );

			switch( pEntity->GetDisplayMode() )
				{
				case DISP_MODE_DISPLAY:			m_actDisplay->setChecked( true );		break;
				case DISP_MODE_TRANSPARENCY:	m_actTransparency->setChecked( true );	break;
				case DISP_MODE_WIREFRAME:		m_actWireframe->setChecked( true );		break;
				case DISP_MODE_HIDDEN:			m_actHide->setChecked( true );			break;
				}

  			connect( m_actDisplay,		SIGNAL( triggered() ),	this, SLOT( OnDisplay() ) );
  			connect( m_actTransparency,	SIGNAL( triggered() ),	this, SLOT( OnTransparency() ) );
  			connect( m_actWireframe,	SIGNAL( triggered() ),	this, SLOT( OnWireframe() ) );
  			connect( m_actHide,			SIGNAL( triggered() ),	this, SLOT( OnHide() ) );

			m_actColor			= Menu.addAction( "Color" );
  			connect( m_actColor,		SIGNAL( triggered() ),	this, SLOT( OnColor() ) );

			if ( !managerExists )
			{
				m_actRetessellate		= Menu.addAction( "Retessellate" );
				connect( m_actRetessellate,		SIGNAL( triggered() ),	this, SLOT( OnRetessellate() ) );
			}

			if (enableExportIGES)
			{
				m_actExportStl		= Menu.addAction( "Export as STL" );
  				connect( m_actExportStl,	SIGNAL( triggered() ),	this, SLOT( OnExportStl() ) );
				//
				if (pEntity->GetEntRole() == ENT_ROLE_FEMUR)
				{
					QAction* actExportIges3Surfaces = Menu.addAction( "Export 3 surfaces as IGES" );
					connect( actExportIges3Surfaces,	SIGNAL( triggered() ),	this, SLOT( OnExportIges3Surfaces() ) );
				}
			}

			if (!managerExists && pEntity->GetEntRole() == ENT_ROLE_NONE)
			{
				m_actDelete		= Menu.addAction( "Delete" );
				connect( m_actDelete,		SIGNAL( triggered() ),	this, SLOT( OnDelete() ) );
			}

			break;
			}

		case ENT_TYPE_FEMORAL_AXES:
			{
			if ( !managerExists )
			{
				m_actRedefine		= Menu.addAction( "Edit" );
				m_actRegenerateAStep		= Menu.addAction( "Regen Itself" );
				connect( m_actRedefine,		SIGNAL( triggered() ),	this, SLOT( OnRedefine() ) );
				connect( m_actRegenerateAStep,		SIGNAL( triggered() ),	this, SLOT( OnRegenerateAStep() ) );

				if ( pEntity->GetUpToDateStatus() )
					m_actRegenerateAStep->setEnabled(false);
			}
			pMenuDisplayMode	= Menu.addMenu( "Display Mode" );
			m_actDisplay		= pMenuDisplayMode->addAction( "Display" );
			m_actTransparency	= pMenuDisplayMode->addAction( "Transparency" );
			m_actHide			= pMenuDisplayMode->addAction( "Hide" );

			m_actDisplay->setCheckable( true );
			m_actTransparency->setCheckable( true );
			m_actHide->setCheckable( true );

			switch( pEntity->GetDisplayMode() )
			{
				case DISP_MODE_DISPLAY:			m_actDisplay->setChecked( true );		break;
				case DISP_MODE_TRANSPARENCY:	m_actTransparency->setChecked( true );	break;
				case DISP_MODE_HIDDEN:			m_actHide->setChecked( true );			break;
			}

  			connect( m_actDisplay,		SIGNAL( triggered() ),	this, SLOT( OnDisplay() ) );
			connect( m_actTransparency,	SIGNAL( triggered() ),	this, SLOT( OnTransparency() ) );
  			connect( m_actHide,			SIGNAL( triggered() ),	this, SLOT( OnHide() ) );
			break;
			}
		
		case ENT_TYPE_FEMORAL_CUTS:
		{
			switch ( pEntity->GetEntRole() )
			{
				case ENT_ROLE_FEMORAL_CUTS:
				{
					if ( !managerExists )
					{
						//m_actRedefine		= Menu.addAction( "Edit" );
						//m_actRegenerateAStep		= Menu.addAction( "Regen Itself" );
						//m_actRegenerateMultipleSteps		= Menu.addAction( "Regen Through" );
						connect( m_actRedefine,		SIGNAL( triggered() ),	this, SLOT( OnRedefine() ) );
						//connect( m_actRegenerateAStep,	SIGNAL( triggered() ),	this, SLOT( OnRegenerateAStep() ) );
						//connect( m_actRegenerateMultipleSteps,	SIGNAL( triggered() ),	this, SLOT( OnRegenerateMultipleSteps() ) );
						//if ( pEntity->GetUpToDateStatus() ||
						//	!m_pDoc->GetEntity( (CEntRole)(pEntity->GetEntRole()-1) )->GetUpToDateStatus())
						//{
						//	m_actRegenerateAStep->setEnabled(false);
						//	m_actRegenerateMultipleSteps->setEnabled(false);
						//}
					}

					pMenuDisplayMode	= Menu.addMenu( "Display Mode" );
					m_actDisplay		= pMenuDisplayMode->addAction( "Display" );
					m_actTransparency	= pMenuDisplayMode->addAction( "Transparency" );
					m_actWireframe		= pMenuDisplayMode->addAction( "Wireframe" );
					m_actHide			= pMenuDisplayMode->addAction( "Hide" );

					m_actDisplay->setCheckable( true );
					m_actTransparency->setCheckable( true );
					m_actWireframe->setCheckable( true );
					m_actHide->setCheckable( true );

					switch( pEntity->GetDisplayMode() )
					{
						case DISP_MODE_DISPLAY:			m_actDisplay->setChecked( true );		break;
						case DISP_MODE_TRANSPARENCY:	m_actTransparency->setChecked( true );	break;
						case DISP_MODE_WIREFRAME:		m_actWireframe->setChecked( true );		break;
						case DISP_MODE_HIDDEN:			m_actHide->setChecked( true );			break;
					}

  					connect( m_actDisplay,		SIGNAL( triggered() ),	this, SLOT( OnDisplay() ) );
  					connect( m_actTransparency,	SIGNAL( triggered() ),	this, SLOT( OnTransparency() ) );
   					connect( m_actWireframe,	SIGNAL( triggered() ),	this, SLOT( OnWireframe() ) );
 					connect( m_actHide,			SIGNAL( triggered() ),	this, SLOT( OnHide() ) );

					m_actColor			= Menu.addAction( "Color" );
  					connect( m_actColor,		SIGNAL( triggered() ),	this, SLOT( OnColor() ) );

					if ( !managerExists )
					{
						m_actRetessellate		= Menu.addAction( "Retessellate" );
						connect( m_actRetessellate,		SIGNAL( triggered() ),	this, SLOT( OnRetessellate() ) );
					}

					if (enableExportIGES)
					{
						m_actExportStl		= Menu.addAction( "Export as STL" );
  						connect( m_actExportStl,	SIGNAL( triggered() ),	this, SLOT( OnExportStl() ) );
					}

					break;
				}
			}
			break;
		}

	}

	

	// Open, Regenerate also depend on the edit/review mode
	if ( mainWindow->IsReviewMode() )  
	{
		// disable all entities belong to implant design
		if ( m_actRedefine )
			m_actRedefine->setEnabled(false);
		if ( m_actRegenerateAStep )
			m_actRegenerateAStep->setEnabled(false);
		if ( m_actRegenerateMultipleSteps ) 
			m_actRegenerateMultipleSteps->setEnabled(false);
	}
  
	QPoint		pos = QCursor::pos();

	pos.setY( pos.y() + 10 );

	Menu.exec( pos );
}

void CTotalEntPanel::OnRedefine()
{
	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	CTotalView*	pView = (CTotalView*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("CTotalEntPanel::OnRedefine(), entity name: %1").arg(pEnt->GetEntName()) );

	switch( eEntRole )
	{
		case ENT_ROLE_FEMUR:
		{
			pView->OnImportFemurSurface();
			break;
		}
		case ENT_ROLE_FEMORAL_AXES:
		{
			pView->OnMakeFemoralAxes();
			break;
		}

		case ENT_ROLE_FEMORAL_CUTS:
		{
			pView->OnCutFemur();
			break;
		}

		case ENT_ROLE_OUTLINE_PROFILE:
		{
			pView->OnDefineOutlineProfile();
			break;
		}
	}
}

void CTotalEntPanel::OnRegenerateAStep()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	CTotalView*	pView = (CTotalView*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("CTotalEntPanel::OnRegenerateAStep(), entity name: %1").arg(pEnt->GetEntName()) );

	switch( eEntRole )
		{
		case ENT_ROLE_FEMORAL_AXES:
			{
				pView->OnMakeFemoralAxes( MAN_ACT_TYPE_REGENERATE );
				
				break;
			}
		
		case ENT_ROLE_OUTLINE_PROFILE:
			{
				pView->OnDefineOutlineProfile( MAN_ACT_TYPE_REGENERATE );
				
				break;
			}
		
		case ENT_ROLE_FEMORAL_CUTS:
			{
				pView->OnCutFemur( MAN_ACT_TYPE_REGENERATE );// for femur cuts, "Regenerate" means "update cuts".
				
				break;
			}
		
		}

}

void CTotalEntPanel::OnRegenerateMultipleSteps()
{

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	CTotalView*	pView = (CTotalView*)m_pDoc->GetView();

	m_pDoc->AppendLog( QString("CTotalEntPanel::OnRegenerateMultipleSteps(), entity name: %1").arg(pEnt->GetEntName()) );

	bool regenerateOnly = true;

	switch( eEntRole )
		{
		case ENT_ROLE_FEMORAL_AXES:
			{
				pView->OnMakeFemoralAxes( MAN_ACT_TYPE_REGENERATE );

				break;
			}
		
		case ENT_ROLE_FEMORAL_CUTS:
			{
				pView->OnCutFemur( MAN_ACT_TYPE_REGENERATE );// for femur cuts, "Regenerate" means "update cuts".
				
				if ( m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE ) )
					pView->OnDefineOutlineProfile( MAN_ACT_TYPE_REGENERATE );
				
				break;
			}
		
		case ENT_ROLE_OUTLINE_PROFILE:
			{
				pView->OnDefineOutlineProfile( MAN_ACT_TYPE_REGENERATE );
				
				break;
			}

		}

}

void CTotalEntPanel::OnSubdivMeshChanged(QString meshName)
{
   // we only watch for the femur changes now
   if ( meshName.compare("Femur_Inner", Qt::CaseInsensitive) == 0 ) // Equal == 0
   {
		GetTotalDoc()->UpdateEntPanelStatusMarker(ENT_ROLE_FEMUR, false); // not up to date now
   }
   else if ( meshName.compare("Patella", Qt::CaseInsensitive) == 0 )// Equal == 0
   {

   }
   else if (meshName.compare("Femur_Inner_jig", Qt::CaseInsensitive) == 0 )// Equal == 0
   {
		GetTotalDoc()->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_OSTEOPHYTE_SURFACE, false); // not up to date now
   }
}

void CTotalEntPanel::OnSketchSliceChanged(QString sketchName)
{
	if (sketchName.compare("Hip", Qt::CaseInsensitive) == 0 )// Equal == 0
	{
		GetTotalDoc()->UpdateEntPanelStatusMarker(ENT_ROLE_HIP, false); // not up to date now
	}
}

void CTotalEntPanel::OnExportIges3Surfaces()
{
	QString					sFileTypes = "IGES files (*.igs);;";
	QFileDialog::Options	options = QFileDialog::DontUseNativeDialog;
	QString					sFileName, sDir;
	CFemoralPart*			pPart;
	QFileDialog				Dlg;

	CEntity*	pEnt = m_pDoc->GetEntityById( m_id );
	CEntRole	eEntRole = pEnt->GetEntRole();
	if ( eEntRole != ENT_ROLE_FEMUR )
		return;

	sDir = m_pDoc->GetFileFolder();

	sFileName = Dlg.getSaveFileName( NULL, "Export as IGES", sDir, sFileTypes, 0, options  );

	if( sFileName.isNull() )
		return;

	if( !sFileName.endsWith( "igs", Qt::CaseInsensitive ) )
		sFileName += ".igs";

	sDir = GetFileDir( sFileName );

	pPart = (CFemoralPart*)m_pDoc->GetPartById( m_id );

	m_pDoc->AppendLog( QString("CTotalEntPanel::OnExportIges3Surfaces(), entity name: %1").arg(pPart->GetEntName()) );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Get 3 surfaces
	IwBSplineSurface* mainSurf = pPart->GetSinglePatchSurface();
	IwBSplineSurface *leftSurf, *rightSurf;
	pPart->GetSideSurfaces(leftSurf, rightSurf);
	CSurfArray pSurfArray;
	pSurfArray.Add(mainSurf);
	pSurfArray.Add(leftSurf);
	pSurfArray.Add(rightSurf);
	// WriteIges
	WriteIges(sFileName, NULL, NULL, &pSurfArray, NULL, NULL);

	QApplication::restoreOverrideCursor();
}
