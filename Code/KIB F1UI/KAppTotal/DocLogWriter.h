#pragma once

#include <QtGui>

class CDocLogWriter
	{
public:
	CDocLogWriter( );
	~CDocLogWriter();

	QString	GetFileCrashName(bool completePath=true);
	QString	GetFilePreviousCrashName();
	QString	GetFileLogName();
	QString	GetFilePreviousLogName();
	QString	GetFileDir();
	void		SetFileDir(QString fileDir);
	void		DeleteOldCrashFiles();
	void		AppendLog(const QString& appendLogString, bool newLine=true);
	void		InitializeLogFile(QString patientID="");
	void		TerminateLogFile();

private:
	QString			m_fileDir;
	};
