#pragma once

#include <QtGui>
#include "valgebra.h"

class CDataDoc;
class CBounds3d;
class IwBrep;
class IwSurface;
class IwCurve;
class IwPoint3d;

#include "EntityEnums.h"

typedef IwTArray< CEntRole >	CEntRoleArray;


struct CEntTypeEntry
	{
	CEntType			eEntType;
	QString				sEntType;
	};


struct CEntRoleEntry
	{
	CEntRole			eEntRole;
	QString				sEntRole;
	};


struct CDispModeEntry
	{
	CDisplayMode		eDispMode;
	QString				sDispMode;
	};


class CEntity : public QObject
	{
public:

	CEntity();
	CEntity( CDataDoc* pDoc, CEntRole eEntRole, const QString& sFileName = "", int id = -1 );
	virtual ~CEntity();

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave) = 0;// the entity data members will be saved via DocWriter() to header.txt
	virtual bool	Load( const QString& sDir ) = 0;// the entity data members will be loaded via DocReader() from header.txt
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&) = 0;
	virtual CBounds3d		GetBounds() = 0;
	virtual CBounds3d		ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL ) = 0;

	void			SetEntId( int id );
	int				GetEntId() const;

	void			SetEntType( CEntType eEntType );
	CEntType		GetEntType();

	void			SetEntRole( CEntRole eEntRole );
	CEntRole		GetEntRole() const;

	void			SetEntName( const QString& sEntName );
	QString			GetEntName() const;

	void			SetFileName( const QString& sFileName );
	QString			GetFileName();

	void			SetDisplayMode( CDisplayMode eDisplayMode );
	CDisplayMode	GetDisplayMode();

	void			SetColor( const CColor& color );
	CColor			GetColor();

	void			SetUpToDateStatus(bool updateToDate) {m_bUdateToDate = updateToDate;};
	virtual bool	GetUpToDateStatus() const { return m_bUdateToDate; };

	bool			Is2ndItemOn()			{ return m_2ndItem && m_2ndItem->checkState() == Qt::Checked; };
	bool			Has2ndItem()		{ return m_2ndItem != NULL; };
	QStandardItem*	Get2ndItem()			{ return m_2ndItem; };

	void			SetValidateStatus(CEntValidateStatus vStatus, QString const& message);
	CEntValidateStatus	GetValidateStatus(QString& message) const {message=m_sValidateMessage;return m_validateStatus;}

	CDataDoc*		GetDoc();

	void			FormFileName();

	static QString			EntTypeName( CEntType eEntType );
	static CEntType			EntTypeByName( const QString& sEntType );

	static QString			RoleName( CEntRole eEntRole );
	static CEntRole			RoleByName( const QString& sEntRole );

	static QString			DispModeName( CDisplayMode eDispMode );
	static CDisplayMode		DispModeByName( const QString& sDispMode );

	int						GetDesignTime();
	void					SetDesignTime(int designTime);

protected:

	QStandardItem*			m_2ndItem;

	int						m_id;
	CEntRole				m_eEntRole;
	QString					m_sEntName;	
	CEntType				m_eEntType;
	QString					m_sFileName;

	CDataDoc*				m_pDoc;

	CDisplayMode			m_eDisplayMode;
	CColor					m_color;
	bool					m_bUdateToDate; // The overall design status, it could be out-of-date by its parameters being modified and/or its prior entities being modified.
											// Usually m_bUdateToDate=true when users "Accept" the design step.
	CEntValidateStatus		m_validateStatus;
	QString					m_sValidateMessage;
	int						m_designTime;// seconds

	static CEntTypeEntry	EntTypes[];
	static CEntRoleEntry	EntRoles[];
	static CDispModeEntry	DispModes[];
	};
