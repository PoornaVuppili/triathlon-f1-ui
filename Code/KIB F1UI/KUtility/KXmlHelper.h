#ifndef KXMLHELPER_H
#define KXMLHELPER_H

#include <QDomDocument>
#include <qcolor.h>
#include "TRotate3.h"
#include "TBox3.h"
#include <vector>

// Functions for save a status node to 'elem' in the XML file
void AppendNode(QDomElement& elem, const QString& tagName, int v);
void AppendNode(QDomElement& elem, const QString& tagName, float v);
void AppendNode(QDomElement& elem, const QString& tagName, double v);
void AppendNode(QDomElement& elem, const QString& tagName, const QString& s);
void AppendNode(QDomElement& elem, const QString& tagName, const QColor& color);
void AppendNode(QDomElement& elem, const QString& tagName, const std::vector<int>& vs);
void AppendNode(QDomElement& elem, const QString& tagName, const std::vector<float>& vs);
void AppendNode(QDomElement& elem, const QString& tagName, const Box3Float& box);

template<class T> void AppendNode(QDomElement& elem, const QString& tagName, const TPoint3<T>& pt)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(
        QString::number(pt.x(), 'g', 12) + "," + 
        QString::number(pt.y(), 'g', 12) + "," + 
        QString::number(pt.z(), 'g', 12) ));
}

template<class T> void AppendNode(QDomElement& elem, const QString& tagName, const TRotate3<T>& r)
{
    QDomDocument doc = elem.ownerDocument();
    QDomElement tag = doc.createElement(tagName);
    elem.appendChild(tag);
    tag.appendChild( doc.createTextNode(
          QString::number( r(0,0), 'g', 12 ) + "," + 
          QString::number( r(0,1), 'g', 12 ) + "," + 
          QString::number( r(0,2), 'g', 12 ) + "," + 
          QString::number( r(1,0), 'g', 12 ) + "," +
          QString::number( r(1,1), 'g', 12 ) + "," + 
          QString::number( r(1,2), 'g', 12 ) + "," + 
          QString::number( r(2,0), 'g', 12 ) + "," + 
          QString::number( r(2,1), 'g', 12 ) + "," + 
          QString::number( r(2,2), 'g', 12 ) ));
}

// Functions for parsing a status node 'elem' in the XML file. Return true/false if succeeds/fails.
// - parse 'elem' to an integer/float/double
bool ParseNode(const QDomElement& elem, int& v);
bool ParseNode(const QDomElement& elem, float& v);
bool ParseNode(const QDomElement& elem, double& v);
bool ParseNode(const QDomElement& elem, QString& s);
// - parse 'elem' to color if it is a comma-separated text node of R,G,B. 
bool ParseNode(const QDomElement& elem, QColor& color);
// - parse 'elem' to a row-major 3x3 matrix if it is a comma-separated text node of 9 values.
bool ParseNode(const QDomElement& elem, Rotate3Float& r);
// - parse 'elem' to a point if it is a comma-separated text node of 3 values.
bool ParseNode(const QDomElement& elem, Point3Float& pt);
// - parse 'elem' to an integer vector if it is a comma-separated text node of integers.
bool ParseNode(const QDomElement& elem, std::vector<int>& vs);
// - parse 'elem' to an integer/float vector if it is comma-separated text node of 'size' integers/floats
bool ParseNode(const QDomElement& elem, int size, std::vector<int>& vs);
bool ParseNode(const QDomElement& elem, int size, std::vector<float>& vs);
// - parse 'elem' to a box if it is a comma-separated text node of 15 floats
bool ParseNode(const QDomElement& elem, Box3Float& box);

QDomNode GetFirstChildNodeByTag(const QDomNode &node, const QString &tag);
QDomNode GetFirstChildNodeByAttribute(const QDomNode &node, 
										const QString &attName, const QString &attValue);
#endif // KXMLHELPER_H