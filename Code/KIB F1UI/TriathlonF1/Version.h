#ifndef VERSION_H_INCLUDED
#define VERSION_H_INCLUDED

#define PRODUCT_STR "Triathlon F1"

#define PRODUCT_DESC "Triathlon F1 Knee Implant Design"
#include "LastChangelist.h"

////////////////////////////////////////////////////////
// Please check whether you need to synchronize 
// the definitions in /KAppTotal/TriathlonF1Definitions.h
////////////////////////////////////////////////////////
#define VERSION_MAJOR_NUM 1
#define VERSION_MINOR_NUM 0
#define VERSION_BUILD_NUM  0
#define VERSION_LETTER "a"

////////////////////////////////////////// do not change the lines below without a special reason
#define wSTR(x) L#x
#define wiSTR(x) wSTR(x)
#define wWIDE(x) L ## x
#define wide(x) wWIDE(x)
#define wSTRSTR(a,b) a ## b
#define wSTRdotSTR(a,b) a ## L"." ## b
#define wSTRdotSTRdotSTR(a,b,c) a ## L"." ## b ## L"." ## c
#define wAPPENDltr(str, ltr) str ## ltr

#define nSTR(x) #x
#define niSTR(x) nSTR(x)
#define nSTRSTR(a,b) a ## b
#define nSTRdotSTR(a,b) a ## "." ## b
#define nSTRdotSTRdotSTR(a,b,c) a ## "." ## b ## "." ## c

#ifdef __cplusplus
   #define VERSION_STR "0.0.002"// nSTRdotSTR(nSTRSTR(nSTRdotSTRdotSTR(niSTR(VERSION_MAJOR_NUM), niSTR(VERSION_MINOR_NUM), niSTR(VERSION_BUILD_NUM)),VERSION_LETTER), niSTR(LASTSUBMITTEDCHANGELIST))
#else
   #define VERSION_STR "0.0.002"//VERSION_STR wSTRdotSTR(wAPPENDltr(wSTRdotSTRdotSTR(wiSTR(VERSION_MAJOR_NUM), wiSTR(VERSION_MINOR_NUM), wiSTR(VERSION_BUILD_NUM)), wide(VERSION_LETTER)),wiSTR(LASTSUBMITTEDCHANGELIST))
#endif


#endif // VERSION_H_INCLUDED
