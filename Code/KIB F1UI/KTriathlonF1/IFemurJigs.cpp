#include "IFemurJigs.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "TotalMainWindow.h"

IFemurJigs::IFemurJigs()
{

}

IFemurJigs::~IFemurJigs()
{

}

///////////////////////////////////////////////////////////////////
// This is a service function. 
bool IFemurJigs::IsFemurJigsComplete()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return false;

	bool isComplete = pDoc->IsFemoralJigsDesignComplete();

	return isComplete;
}

///////////////////////////////////////////////////////////////////
// This is a service function. 
void IFemurJigs::HideAllFemurJigsEntities()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return;

	pDoc->HideAllFemurJigsEntities();
		
}

///////////////////////////////////////////////////////////////////
// This is a service function. 
void IFemurJigs::Review_StartReviewJigs()
{
	CTotalDoc* pDoc = CTotalDoc::GetTotalDoc();
	if ( pDoc == NULL )
		return;

	pDoc->GetView()->GetMainWindow()->OnFemoralJigsReview();

}
