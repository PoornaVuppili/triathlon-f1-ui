#include <qaction.h>
#include <qcombobox.h>

#include "PredefinedViewToolBar.h"
#include "SwitchBoard.h"

PredefinedViewToolBar::PredefinedViewToolBar()
	: QToolBar("Axes aligned View")
{
	ResetToggles();

	axesList = new QComboBox();
	axesListAction = addWidget(axesList);
	axesListAction->setVisible(false);

	SwitchBoard::addSignalReceiver(SIGNAL(BroadcastCoordSystem(const CoordSystem&)), this, SLOT(OnCoordSystem(const CoordSystem&)));
    SwitchBoard::addSignalReceiver(SIGNAL(BroadcastCoordSystem(const CoordSystem&, bool)), this, SLOT(OnCoordSystem(const CoordSystem&, bool)));
	SwitchBoard::addSignalReceiver(SIGNAL(RemoveCoordSystem(QString)), this, SLOT(OnRemoveCoordSystem(QString)));
	SwitchBoard::addSignalReceiver(SIGNAL(SwitchToCoordSystem(QString)), this, SLOT(OnSetCurrentCoordSystem(QString)));
	SwitchBoard::addSignalSender(SIGNAL(BroadcastCurrentCoordSystem(const CoordSystem&)), this);

	connect(axesList, SIGNAL(currentIndexChanged(int)), this, SLOT(handleSelectionChanged(int)));
}

PredefinedViewToolBar::~PredefinedViewToolBar()
{
	SwitchBoard::removeSignalReceiver(this);
	SwitchBoard::removeSignalSender(this);
}

IwAxis2Placement PredefinedViewToolBar::GetCurrentSystem()
{
	QVariant itemData = axesList->itemData(axesList->currentIndex());
	IwAxis2Placement system = itemData.value<CoordSystem>();

	return system;
}

void PredefinedViewToolBar::OnCoordSystem(const CoordSystem& system, bool switchCurrent)
{
    QString name = system.GetName();
	int index = axesList->findText(name);
	QVariant itemData;
	itemData.setValue(system);
	
	if(index == -1)
    {
		axesList->addItem(name, itemData);
        index = axesList->count() - 1;
    }
	else
		axesList->setItemData(index, itemData);

	axesListAction->setVisible(axesList->count() > 1);

    if( axesList->currentIndex() != index )
    {
        if( switchCurrent )
            axesList->setCurrentIndex( index );
    }
    else
    {
        emit BroadcastCurrentCoordSystem(system);
    }
}

void PredefinedViewToolBar::SetCurrentSystemName(const QString& name)
{
    int index = axesList->findText(name);
    if( index == axesList->currentIndex() )
        handleSelectionChanged(index);
    else if( index >= 0 )
        axesList->setCurrentIndex(index);
}

void PredefinedViewToolBar::OnRemoveCoordSystem(QString name)
{
	int index = axesList->findText(name);

	if( index == -1)
		return;

	axesList->removeItem(index);
	axesList->setCurrentIndex(0);

	QVariant itemData = axesList->itemData(axesList->currentIndex());
	CoordSystem system = itemData.value<CoordSystem>();

	emit BroadcastCurrentCoordSystem(system);

	axesListAction->setVisible(axesList->count() > 1);
}

void PredefinedViewToolBar::OnSetCurrentCoordSystem(QString name)
{
	axesList->setCurrentIndex(axesList->findText(name));

	handleSelectionChanged(axesList->findText(name));
}

void PredefinedViewToolBar::handleSelectionChanged(int index)
{
	QVariant itemData = axesList->itemData(index);
	CoordSystem system = itemData.value<CoordSystem>();

	emit BroadcastCurrentCoordSystem(system);
}

void PredefinedViewToolBar::ResetToggles(bool keepOrientationInfo)
{
	frontView = false;
	backView = false;
	leftView = false;
	rightView = false;
	topView = false;
	bottomView = false;
	isoView = false;
	isoBackView = false;

	// If is called by zoom() or shift(), the view orientation does not change actually.
	if ( !keepOrientationInfo ) 
	{
		frontViewOrientation = false;
		backViewOrientation = false;
		leftViewOrientation = false;
		rightViewOrientation = false;
		topViewOrientation = false;
		bottomViewOrientation = false;
		isoViewOrientation = false;
		isoBackViewOrientation = false;
	}
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleFrontView(bool autoZRot180)		
{ 
	bool b;
	if (autoZRot180) 
		b = !frontView;
	else
		b = true;
	ResetToggles(); 
	frontView  = b; 
	frontViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleBackView(bool autoZRot180)		
{ 
	bool b;
	if (autoZRot180)
		b = !backView;
	else
		b = true;
	ResetToggles(); 
	backView   = b;
	backViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleLeftView(bool autoZRot180)
{
	bool b;
	if (autoZRot180)
		b = !leftView;
	else
		b = true;
	ResetToggles(); 
	leftView = b;
	leftViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleRightView(bool autoZRot180)
{
	bool b;
	if (autoZRot180)
		b = !rightView;
	else
		b = true;
	ResetToggles(); 
	rightView   = b;
	rightViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleTopView(bool autoZRot180)
{
	bool b;
	if (autoZRot180)
		b = !topView;
	else
		b = true;
	ResetToggles(); 
	topView   = b;
	topViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleBottomView(bool autoZRot180)
{
	bool b;
	if (autoZRot180)
		b = !bottomView;
	else
		b = true;
	ResetToggles(); 
	bottomView   = b;
	bottomViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleIsoView(bool autoZRot180)
{
	bool b;
	if (autoZRot180)
		b = !isoView;
	else
		b = true;
	ResetToggles(); 
	isoView   = b;
	isoViewOrientation = true;
}

// If autoZRot180 = true, to keep the behavior that automatically rotates  
// along z 180 degrees when click the same predefined view 2nd time. 
void PredefinedViewToolBar::ToggleIsoBackView(bool autoZRot180)
{
	bool b;
	if (autoZRot180)
		b = !isoBackView;
	else
		b = true;
	ResetToggles(); 
	isoBackView   = b;
	isoBackViewOrientation = true;
}
