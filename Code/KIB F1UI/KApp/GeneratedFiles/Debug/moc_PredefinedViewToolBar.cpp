/****************************************************************************
** Meta object code from reading C++ file 'PredefinedViewToolBar.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../PredefinedViewToolBar.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PredefinedViewToolBar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PredefinedViewToolBar_t {
    QByteArrayData data[13];
    char stringdata0[181];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PredefinedViewToolBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PredefinedViewToolBar_t qt_meta_stringdata_PredefinedViewToolBar = {
    {
QT_MOC_LITERAL(0, 0, 21), // "PredefinedViewToolBar"
QT_MOC_LITERAL(1, 22, 27), // "BroadcastCurrentCoordSystem"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 11), // "CoordSystem"
QT_MOC_LITERAL(4, 63, 4), // "axes"
QT_MOC_LITERAL(5, 68, 13), // "OnCoordSystem"
QT_MOC_LITERAL(6, 82, 6), // "system"
QT_MOC_LITERAL(7, 89, 13), // "switchCurrent"
QT_MOC_LITERAL(8, 103, 19), // "OnRemoveCoordSystem"
QT_MOC_LITERAL(9, 123, 4), // "name"
QT_MOC_LITERAL(10, 128, 23), // "OnSetCurrentCoordSystem"
QT_MOC_LITERAL(11, 152, 22), // "handleSelectionChanged"
QT_MOC_LITERAL(12, 175, 5) // "index"

    },
    "PredefinedViewToolBar\0BroadcastCurrentCoordSystem\0"
    "\0CoordSystem\0axes\0OnCoordSystem\0system\0"
    "switchCurrent\0OnRemoveCoordSystem\0"
    "name\0OnSetCurrentCoordSystem\0"
    "handleSelectionChanged\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PredefinedViewToolBar[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    2,   47,    2, 0x09 /* Protected */,
       5,    1,   52,    2, 0x29 /* Protected | MethodCloned */,
       8,    1,   55,    2, 0x09 /* Protected */,
      10,    1,   58,    2, 0x09 /* Protected */,
      11,    1,   61,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Bool,    6,    7,
    QMetaType::Void, 0x80000000 | 3,    6,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::Int,   12,

       0        // eod
};

void PredefinedViewToolBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PredefinedViewToolBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->BroadcastCurrentCoordSystem((*reinterpret_cast< const CoordSystem(*)>(_a[1]))); break;
        case 1: _t->OnCoordSystem((*reinterpret_cast< const CoordSystem(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 2: _t->OnCoordSystem((*reinterpret_cast< const CoordSystem(*)>(_a[1]))); break;
        case 3: _t->OnRemoveCoordSystem((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->OnSetCurrentCoordSystem((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->handleSelectionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CoordSystem >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CoordSystem >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CoordSystem >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PredefinedViewToolBar::*)(const CoordSystem & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PredefinedViewToolBar::BroadcastCurrentCoordSystem)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PredefinedViewToolBar::staticMetaObject = { {
    QMetaObject::SuperData::link<QToolBar::staticMetaObject>(),
    qt_meta_stringdata_PredefinedViewToolBar.data,
    qt_meta_data_PredefinedViewToolBar,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PredefinedViewToolBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PredefinedViewToolBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PredefinedViewToolBar.stringdata0))
        return static_cast<void*>(this);
    return QToolBar::qt_metacast(_clname);
}

int PredefinedViewToolBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void PredefinedViewToolBar::BroadcastCurrentCoordSystem(const CoordSystem & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
