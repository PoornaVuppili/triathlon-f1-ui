#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW CInnerSurfaceJigs : public CPart
{
public:
	CInnerSurfaceJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_INNER_SURFACE_JIGS, const QString& sFileName="", int nEntId=-1 );
	~CInnerSurfaceJigs();
	
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );
	void			DisplayAntDrop();

	void			SetAnteriorDropping(IwBrep* antDropping);
	IwBrep*			GetAnteriorDropping() {return m_anteriorDropping;};
	void			SetDispAntDrop(bool val) {m_bDispAntDrop=val;};
	void			SetDispSurface(bool val) {m_bDispSurface=val;};
	void			SetOffsetOsteophyteBrep(IwBrep*& osteoBrep);
	bool			GetOffsetOsteophyteBrep(IwBrep*& osteoBrep);
	unsigned		GetOsteophyteTimeStamp() {return m_osteophyteTimeStamp;};
	void			SetOsteophyteTimeStamp(unsigned val) {m_osteophyteTimeStamp=val;SetISJigsModifiedFlag(true);};

private:
	void			SetISJigsModifiedFlag( bool bModified );

protected:
	bool			m_bDispAntDrop;
	bool			m_bDispSurface;

private:
	bool			m_bISJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bISJigsDataModified;// Geometric entities have not been saved yet to the file. 

	long			m_glAntDropFaceList;
	long			m_glAntDropEdgeList;
	
	IwBrep*			m_anteriorDropping;
	IwTArray<double>	m_tabDropOffsetDistances;// The offset distances for the individual tabs. Also see m_innerSurfaceDropOffsetDistance in advanced control jigs.
												 // The sequence: [0]:anterior tab, [1]:middle tab, [2]:peg tab.
	IwBrep*			m_offsetOsteophyteBrep;
	unsigned		m_osteophyteTimeStamp;	// When create m_offsetOsteophyteBrep, we need to check the time stamp
											// in osteophyte surface is the same as the time stamp here.
											// If the same (indicate users never update the osteophyte surface
											// since last time create m_offsetOsteophyteBrep), just directly use
											// the m_offsetOsteophyteBrep here. Otherwise (users had updated
											//  the osteophyte surface), we need to create the m_offsetOsteophyteBrep.

};