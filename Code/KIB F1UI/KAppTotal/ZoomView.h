#pragma once

#include "Manager.h"

class OpenGLView;

class CZoomView : public CManager
	{
public:
					CZoomView( COpenGLView* pView );

    virtual bool	MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
    virtual bool	MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp  ( const QPoint& cursor, Viewport* vp=NULL );
	bool			Reject();

private:

	void			DrawCursor();

	enum 
		{ 
		INDICATE_LOWER_LEFT_POINT, 
		INDICATE_UPPER_RIGHT_POINT 
		}			
				m_eStatus;

	QPoint		m_mouseStart;
	QPoint		m_mouseLast;
	};
