#pragma once

#include <QWidget>
#include <QDialog>

class QGridLayout;
class QDoubleSpinBox;
class QSpinBox;
class QCheckBox;
class QLineEdit;
class QButtonGroup;
class QRadioButton;

class CAdvancedControlDlg : public QDialog
	{
	Q_OBJECT

public:
	CAdvancedControlDlg( QWidget* parent, Qt::WindowFlags fl, int forCRPS );

	// Auto JOC Steps
	bool	GetAutoJOCSteps();
	void	SetAutoJOCSteps(bool flag);
	bool	GetAutoMajorSteps();
	void	SetAutoMajorSteps(bool flag);
	bool	GetAutoAllSteps();
	void	SetAutoAllSteps(bool flag);
	int		GetAutoAdditionalIterations();
	void	SetAutoAdditionalIterations(int val);

	// Femoral Axes
	bool	GetFemoralAxesEnableRefine();
	void	SetFemoralAxesEnableRefine(bool val);
	double	GetFemoralImplantCoronalRadius();
	void	SetFemoralImplantCoronalRadius(double val);
	double	GetTrochlearGrooveRadius();
	void	SetTrochlearGrooveRadius(double val);
	bool	GetFemoralAxesDisplayMechAxes();
	void	SetFemoralAxesDisplayMechAxes(bool val);
	
	// Femoral Cuts
	bool	GetFemoralCutsEnableReset();
	void	SetFemoralCutsEnableReset(bool val);
	bool	GetFemoralCutsOnlyMoveNormal();
	void	SetFemoralCutsOnlyMoveNormal(bool val);
	double	GetViewProfileOffsetDistance();
	void	SetViewProfileOffsetDistance(double val);
	double	GetCutLineDisplayLength();
	void	SetCutLineDisplayLength(double val);
	bool	GetAchieveDesiredPostCuts();
	void	SetAchieveDesiredPostCuts(bool val);
	
	double	GetCutsExtraAllowance();
	void	SetCutsExtraAllowance(double val);

	// Outline Profile
	bool	GetOutlineProfileEnableReset();
	void	SetOutlineProfileEnableReset(bool val);
	double	GetOutlineProfilePosteriorCutFilterRadiusRatio();
	void	SetOutlineProfilePosteriorCutFilterRadiusRatio(double val);
	double	GetOutlineProfileSketchSurfaceAntExtension();
	void	SetOutlineProfileSketchSurfaceAntExtension(double val);
	double	GetOutlineProfileSketchSurfacePostExtension();
	void	SetOutlineProfileSketchSurfacePostExtension(double val);
	double	GetOutlineProfileAntAirBallDistance();
	void	SetOutlineProfileAntAirBallDistance(double val);

	// Output Results

	bool	GetOutputNormalized();
	void	SetOutputNormalized(bool flag);
	bool	GetImportFromNormalized();
	void	SetImportFromNormalized(bool flag);
	bool	GetOutputWithTransformation();
	void	SetOutputWithTransformation(bool flag);
	
	double	GetImportRotationX();
	void	SetImportRotationX(double val);
	double	GetImportRotationY();
	void	SetImportRotationY(double val);
	double	GetImportRotationZ();
	void	SetImportRotationZ(double val);
	double	GetImportTranslationX();
	void	SetImportTranslationX(double val);
	double	GetImportTranslationY();
	void	SetImportTranslationY(double val);
	double	GetImportTranslationZ();
	void	SetImportTranslationZ(double val);
	double	GetImportTransformationXx();
	void	SetImportTransformationXx(double val);
	double	GetImportTransformationXy();
	void	SetImportTransformationXy(double val);
	double	GetImportTransformationXz();
	void	SetImportTransformationXz(double val);
	double	GetImportTransformationYx();
	void	SetImportTransformationYx(double val);
	double	GetImportTransformationYy();
	void	SetImportTransformationYy(double val);
	double	GetImportTransformationYz();
	void	SetImportTransformationYz(double val);
	double	GetImportTransformationTx();
	void	SetImportTransformationTx(double val);
	double	GetImportTransformationTy();
	void	SetImportTransformationTy(double val);
	double	GetImportTransformationTz();
	void	SetImportTransformationTz(double val);


protected:

private slots:
//	void	OnEnableOutputRegardless();

private:
	QGridLayout *createDataLayout();
	bool	IsCR() {return m_forCRPS==1;};
	bool	IsPS() {return m_forCRPS==2;};

private:

	// Auto JOC Steps
	QButtonGroup*	m_buttonGroupAutoJOC;
	QRadioButton*	m_chkAutoJOCSteps;
	QRadioButton*	m_chkAutoMajorSteps;
	QRadioButton*	m_chkAutoAllSteps;
	QRadioButton*	m_chkAutoNoneSteps;
	QSpinBox*		m_spinAutoAdditionalIterations;

	// Femoral Axes
	QCheckBox*		m_chkFemoralAxesEnableRefine;
	QDoubleSpinBox* m_spinFemoralImplantCoronalRadius;
	QDoubleSpinBox* m_spinTrochlearGrooveRadius;
	QCheckBox*		m_chkFemoralAxesDisplayMechAxes;

	// Femoral Cuts
	QCheckBox*		m_chkFemoralCutsEnableReset;
	QCheckBox*		m_chkFemoralCutsOnlyMoveNormal;
	QDoubleSpinBox* m_spinViewProfileOffsetDistance;
	QDoubleSpinBox* m_spinCutLineDisplayLength;
	QCheckBox*		m_chkAchieveDesiredPostCuts;
	QDoubleSpinBox*	m_spinCutsExtraAllowance;


	// Outline Profile
	QCheckBox*		m_chkOutlineProfileEnableReset;
	QDoubleSpinBox*	m_spinOutlineProfilePosteriorCutFilterRadiusRatio;
	QDoubleSpinBox*	m_spinOutlineProfileSketchSurfaceAntExtension;
	QDoubleSpinBox*	m_spinOutlineProfileSketchSurfacePostExtension;
	QDoubleSpinBox*	m_spinOutlineProfileAntAirBallDistance;

	// Output Results
	
	QCheckBox*	m_chkOutputNormalized;
	QCheckBox*	m_chkImportFromNormalized;
	QCheckBox*	m_chkOutputWithTransformation;

	QDoubleSpinBox* m_chkImportRotationX;
	QDoubleSpinBox* m_chkImportRotationY;
	QDoubleSpinBox* m_chkImportRotationZ;
	QDoubleSpinBox* m_chkImportTranslationX;
	QDoubleSpinBox* m_chkImportTranslationY;
	QDoubleSpinBox* m_chkImportTranslationZ;
	QDoubleSpinBox* m_chkImportTransformationXx;
	QDoubleSpinBox* m_chkImportTransformationXy;
	QDoubleSpinBox* m_chkImportTransformationXz;
	QDoubleSpinBox* m_chkImportTransformationYx;
	QDoubleSpinBox* m_chkImportTransformationYy;
	QDoubleSpinBox* m_chkImportTransformationYz;
	QDoubleSpinBox* m_chkImportTransformationTx;
	QDoubleSpinBox* m_chkImportTransformationTy;
	QDoubleSpinBox* m_chkImportTransformationTz;


	//
	int			m_forCRPS; // 1: for CR, 2: for PS.
};
