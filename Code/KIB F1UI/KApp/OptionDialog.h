#ifndef OPTIONDIALOG_H
#define OPTIONDIALOG_H

#include <QDialog>
namespace Ui {class OptionDialog;};

class OptionPage;
class QTreeWidgetItem;

class OptionDialog : public QDialog
{
    Q_OBJECT

public:
    OptionDialog(QWidget *parent = 0);
    ~OptionDialog();

    // Add an option page
    //  pg     : the option page to add, whose 'windowTitle' will be added as an item in the tree widget
    //  global : whether the setting is for 'global' (default) or per 'implant'.
    void AddPage(OptionPage* pg, bool global=true);

public slots:
    void On_OK_Clicked();
    void On_Cancel_Clicked();

private slots:
    void OnTreeItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);

private:
    typedef QPair<QTreeWidgetItem*, OptionPage*> ItemPagePair;

    Ui::OptionDialog *ui;
    QTreeWidgetItem *rootGlobal, *rootImplant;          // the root items of 'Global' & 'Implant'
    std::vector<ItemPagePair> pairGlobal, pairImplant;  // option item & page pairs
};

#endif // OPTIONDIALOG_H
