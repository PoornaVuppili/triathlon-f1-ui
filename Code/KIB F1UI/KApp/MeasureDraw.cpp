////////////////////////////////////////////////////////////////
// Functions related to drawing measurement entities & results
////////////////////////////////////////////////////////////////

#include "Measure.h"
#include "Viewport.h"
#include <qgl.h>
#include "..\KUtility\SMLibInterface.h"
#include "..\KUtility\Eigen\Geometry"
#include "..\KUtility\TRotate3.h"
#include "..\KUtility\KStatusIndicator.h"
#include "..\ViewWidget.h"
#pragma warning( disable : 4996 )
#include <IwLine.h>
#include <IwFace.h>
#include <IwTess.h>
#include <IwLoop.h>
#pragma warning( default : 4996 )

// Draw the measure entity
void MeasureEntity::Draw(double onePixelSize)
{
    switch(selectType)
    {
    case SelectPoint:    // point
        glBegin(GL_POINTS);
        glVertex3d(selectValue.point.x, selectValue.point.y, selectValue.point.z);
        glEnd();
        break;

    case SelectLine:
        glBegin(GL_LINES);
        glVertex3d(selectValue.line.xs, selectValue.line.ys, selectValue.line.zs);
        glVertex3d(selectValue.line.xe, selectValue.line.ye, selectValue.line.ze);
        glEnd();
        break;

    case SelectCircle:
        {
            Rotate3Double rot( Eigen::Quaternion<double>::FromTwoVectors(Vector3d::UnitZ(), Map<const Vector3d>(&selectValue.circle.nx)) );
            Map<const Vector3d> ct(&selectValue.circle.x);
            glBegin(GL_LINE_LOOP);
            for(int i=0; i<60; i++)
            {
                double ang = i*PI/30;
                Vector3d p = rot * Vector3d(cos(ang), sin(ang), 0) * selectValue.circle.rad + ct;
                glVertex3d(p.x(), p.y(), p.z());
            }
            glEnd();
        }
        break;

    case SelectPlane:
        {
            Vector3d p = Map<const Vector3d>(&selectValue.plane.ux) - 
                         Map<const Vector3d>(&selectValue.plane.cx) + 
                         Map<const Vector3d>(&selectValue.plane.vx);
            glBegin(GL_LINE_LOOP);
            glVertex3dv(&selectValue.plane.cx);
            glVertex3dv(&selectValue.plane.ux);
            glVertex3d(p.x(), p.y(), p.z());
            glVertex3dv(&selectValue.plane.vx);
            glEnd();
        }
        break;

    case SelectCurve:    // curve
        if( !glIsList(selectValue.curve.dl) )
        {
            selectValue.curve.dl = glGenLists(1);
            glNewList(selectValue.curve.dl, GL_COMPILE);
 			if ( IsUseHitPointToMeasure() )
				HighLightHitPoint(hitPoint);
			else
				HighLightCurve(selectValue.curve.curve);
            glEndList();
        }
        if( glIsList(selectValue.curve.dl) )
        {
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glPushAttrib( GL_ENABLE_BIT);
            glCallList(selectValue.curve.dl);
            glPopAttrib();
            glPopMatrix();
        }
        break;

    case SelectEdge:    // edge
        if( !glIsList(selectValue.edge.dl) )
        {
            selectValue.edge.dl = glGenLists(1);
            glNewList(selectValue.edge.dl, GL_COMPILE);
			if ( IsUseHitPointToMeasure() )
				HighLightHitPoint(hitPoint);
			else
				HighLightEdge(selectValue.edge.edge);
            glEndList();
        }
        if( glIsList(selectValue.edge.dl) )
        {
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glPushAttrib( GL_ENABLE_BIT);
            glCallList(selectValue.edge.dl);
            glPopAttrib();
            glPopMatrix();
        }
        break;

    case SelectSurface:  // surface
        if( !glIsList(selectValue.surface.dl) )
        {
            selectValue.surface.dl = glGenLists(1);
            glNewList(selectValue.surface.dl, GL_COMPILE);
			if ( IsUseHitPointToMeasure() )
				HighLightHitPoint(hitPoint);
			else
				HighLightSurface(selectValue.surface.surface);
            glEndList();
        }
        if( glIsList(selectValue.surface.dl) )
        {
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glPushAttrib( GL_ENABLE_BIT);
            glCallList(selectValue.surface.dl);
            glPopAttrib();
            glPopMatrix();
        }
        break;

    case SelectFace:    // face
        if( !glIsList(selectValue.face.dl) )
        {
            selectValue.face.dl = glGenLists(1);
            glNewList(selectValue.face.dl, GL_COMPILE);
			if ( IsUseHitPointToMeasure() )
				HighLightHitPoint(hitPoint);
			else
				HighLightFace(selectValue.face.face, onePixelSize);
            glEndList();
        }
        if( glIsList(selectValue.face.dl) )
        {
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glPushAttrib( GL_ENABLE_BIT);
            glCallList(selectValue.face.dl);
            glPopAttrib();
            glPopMatrix();
        }
        break;

    default:
        kStatus.ShowMessage("Error Measurement Draw: unknown entity type");
    }
}

void MeasureEntity::HighLightHitPoint(Point3Float point)
{
	glBegin(GL_POINTS);
		glVertex3d(point.GetX(), point.GetY(), point.GetZ());
	glEnd();
}

void MeasureEntity::HighLightCurve(IwCurve* curve)
{
	if (curve==NULL) return;

	IwPoint3d	pt;
	IwPoint3d	sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	IwExtent1d sIvl = curve->GetNaturalInterval();

	double	dChordHeightTol = 0.75;
	double	dAngleTolInDegrees = 8.0;

	IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

	sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

	int nPoints = sPnts.GetSize();

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
	{
		pt = sPnts.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();
}

void MeasureEntity::HighLightEdge(IwEdge* eg)
{
	if ( eg == NULL) return;

	IwPoint3d	pt;
	IwPoint3d	sPData[64];
	IwTArray<IwPoint3d> sPnts( 64, sPData );

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	IwExtent1d sIvl = eg->GetInterval();

	double	dChordHeightTol = 0.75;
	double	dAngleTolInDegrees = 8.0;

	IwCurveTessDriver sCrvTess( 0, dChordHeightTol, dAngleTolInDegrees);

	IwCurve* curve = eg->GetCurve();
	sCrvTess.TessellateCurve( *curve, sIvl, NULL, &sPnts );

	int nPoints = sPnts.GetSize();

	glBegin( GL_LINE_STRIP );

	for( ULONG k = 0; k < sPnts.GetSize(); k++ ) 
	{
		pt = sPnts.GetAt(k);

		glVertex3d( pt.x, pt.y, pt.z );
	}

	glEnd();
}

void MeasureEntity::HighLightFaceEdge(IwEdgeuse* edgeUse, double onePixelSize)
{
	if ( onePixelSize < 0.000001 )
		onePixelSize = 0.000001;

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	int displayNo = 0;

	IwCurve* curve = edgeUse->GetEdge()->GetCurve();
	IwExtent1d domEdge = edgeUse->GetEdge()->GetInterval();
	double length = 0;
	curve->Length(domEdge, 0.01, length);
	displayNo = (int)ceil(length/(50*onePixelSize));
	if (displayNo > 50) displayNo = 50;
	double delta = domEdge.GetLength()/(displayNo+1);

	IwPoint3d edgePnt, edgePlusPnt;
	IwVector3d binormal, offVec;

	
	for (double param = domEdge.GetMin()+0.5*delta; param < (domEdge.GetMax()-0.25*delta); param+=delta)
	{
		edgeUse->EvaluateBinormal(param, FALSE, edgePnt, binormal);
		edgePlusPnt = edgePnt + 5*onePixelSize*binormal;

		glBegin( GL_LINES );
			glVertex3d( edgePnt.x, edgePnt.y, edgePnt.z );
			glVertex3d( edgePlusPnt.x, edgePlusPnt.y, edgePlusPnt.z );
		glEnd();
	}
}

void MeasureEntity::HighLightSurface(IwSurface* surf)
{
    if (surf == NULL) return;

    glBegin( GL_LINE_LOOP );
    IwExtent2d uv = surf->GetNaturalUVDomain();
    const int N = 50;
    const double invN = 1.0 / N;
    IwPoint3d pnt;
    int i;

    for(i=0; i<N; i++)
    {
        surf->EvaluatePoint(uv.Evaluate(i*invN, 0),pnt);
        glVertex3d( pnt.x, pnt.y, pnt.z );
    }
    for(i=0; i<N; i++)
    {
        surf->EvaluatePoint(uv.Evaluate(1, i*invN),pnt);
        glVertex3d( pnt.x, pnt.y, pnt.z );
    }
    for(i=0; i<N; i++)
    {
        surf->EvaluatePoint(uv.Evaluate(1-i*invN, 1),pnt);
        glVertex3d( pnt.x, pnt.y, pnt.z );
    }
    for(i=0; i<N; i++)
    {
        surf->EvaluatePoint(uv.Evaluate(0, 1-i*invN),pnt);
        glVertex3d( pnt.x, pnt.y, pnt.z );
    }
    glEnd();
}

void MeasureEntity::HighLightFace(IwFace* fc, double onePixelSize)
{
	if (fc == NULL) return;

	IwTArray<IwEdge*> edgeList;
	IwEdge* eg;
	fc->GetEdges(edgeList);

	// Highlight edges
	for (unsigned i=0; i<edgeList.GetSize(); i++)
	{
		eg = edgeList.GetAt(i);
		HighLightEdge(eg);
	}

	// Highlight face edges
	IwTArray<IwLoop*> loops;
	fc->GetLoops(loops);
	IwLoop* firstLoop = loops.GetAt(0);
	IwLoopuse *pLU1, *pLU2;
	firstLoop->GetLoopuses(pLU1,pLU2);
	IwTArray<IwEdgeuse*> edgeUses;
	pLU1->GetEdgeuses(edgeUses);
	for (unsigned i=0; i< edgeUses.GetSize(); i++)
	{
		IwEdgeuse* edgeuse = edgeUses.GetAt(i);
		HighLightFaceEdge(edgeuse, onePixelSize);
	}
}

// Draw the measurement result
void MeasureResult::Draw(double onePixelSize)
{
    if( !valid )
        return;

    QFont font("Tahoma", 12); 
	font.setBold(true);

    switch( mode )
    {
    case EntityDistance:
    case ScreenDistance:
        glBegin(GL_POINTS);
        glVertex3d(value.distance.x1, value.distance.y1, value.distance.z1);
        glVertex3d(value.distance.x2, value.distance.y2, value.distance.z2);
        glEnd();
        glBegin(GL_LINES);
        glVertex3d(value.distance.x1, value.distance.y1, value.distance.z1);
        glVertex3d(value.distance.x2, value.distance.y2, value.distance.z2);
        glEnd();
        glDisable(GL_DEPTH_TEST);
        viewWidget->renderText( 
            (value.distance.x1 + value.distance.x2)*.5,
            (value.distance.y1 + value.distance.y2)*.5,
            (value.distance.z1 + value.distance.z2)*.5,
            QString::number(value.distance.distance, 'f', 3) + "mm" + 
            (value.distance.isMax ? " (Max)" : "") +
            (mode==EntityDistance ? " [E]" : " [S]"), 
            font);
        break;

    case EntityAngle:
    case ScreenAngle:
		{
			glBegin(GL_LINE_STRIP);
			glVertex3d(value.angle.x1, value.angle.y1, value.angle.z1);
			glVertex3d(value.angle.xc, value.angle.yc, value.angle.zc);
			glVertex3d(value.angle.x2, value.angle.y2, value.angle.z2);
			glEnd();

			Map<const Vector3d> vc(&value.angle.xc);
			Vector3d v1 = Map<const Vector3d>(&value.angle.x1) - vc;
			Vector3d v2 = Map<const Vector3d>(&value.angle.x2) - vc;

			double d = min(onePixelSize*20, min(v1.norm(), v2.norm()));

			v1.normalize();
			v2.normalize();

			glBegin(GL_LINE_STRIP);
			Vector3d v = vc + v1 * d;
			glVertex3d(v.x(), v.y(), v.z());
			if( fabs(value.angle.degree-90) < 0.001 )
			{
				v = vc + (v1 + v2)*d;
				glVertex3d(v.x(), v.y(), v.z());
			}
			else
			{
				int n = max(0,(int)round(.2 * PI * value.angle.degree * d / (180 * onePixelSize) - 1));
				Vector3d vn = ( value.angle.degree>=180 ) ? NormalTo(v1).normalized() : v1.cross(v2).normalized();
				for(int i=0; i<n; i++)
				{
					double t = PI * value.angle.degree * (i+1.0)/(n+1.0) / 180;
					v = vc + AngleAxisd(t, vn) * v1 * d;
					glVertex3d(v.x(), v.y(), v.z());
				}
			}
			v = vc + v2 * d;
			glVertex3d(v.x(), v.y(), v.z());
			glEnd();
			
			glDisable(GL_DEPTH_TEST);
			viewWidget->renderText( 
				(value.angle.x1 + value.angle.x2)*.25 + value.angle.xc*.5,
				(value.angle.y1 + value.angle.y2)*.25 + value.angle.yc*.5,
				(value.angle.z1 + value.angle.z2)*.25 + value.angle.zc*.5,
				QString::number(value.angle.degree, 'f', 3) + "�" +
				(mode==EntityAngle ? " [E]" : " [S]"), 
				font);
		}
        break;

    case EntityRadius:
    case ScreenRadius:
        {
        Rotate3Double rot( Eigen::Quaternion<double>::FromTwoVectors(Vector3d::UnitZ(), Map<const Vector3d>(&value.radius.nx)) );
        Map<const Vector3d> ct(&value.radius.xc);
        glBegin(GL_LINE_LOOP);
        for(int i=0; i<36; i++)
        {
            double ang = i*PI/18;
            Vector3d p = rot * Vector3d(cos(ang), sin(ang), 0) * value.radius.radius + ct;
            glVertex3d(p.x(), p.y(), p.z());
        }
        glEnd();
        glDisable(GL_DEPTH_TEST);
        viewWidget->renderText( 
            value.radius.xc, value.radius.yc, value.radius.zc,
            QString::number(value.radius.radius, 'f', 3) + "mm" +
            (mode==EntityRadius ? " [E]" : " [S]"), 
            font);
        }
        break;
    }
}

