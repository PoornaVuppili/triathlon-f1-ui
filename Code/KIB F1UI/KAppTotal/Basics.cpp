#include <math.h>
#include "Basics.h"
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )


//**************************  CTransform  *************************
CTransform::CTransform()
	{
	matrix.m00 = 1.0;  matrix.m01 = 0.0;  matrix.m02 = 0.0;  shift[0] = 0.0;
	matrix.m10 = 0.0;  matrix.m11 = 1.0;  matrix.m12 = 0.0;  shift[1] = 0.0;
	matrix.m20 = 0.0;  matrix.m21 = 0.0;  matrix.m22 = 1.0;  shift[2] = 0.0;
	}


CTransform::CTransform( double matr[3][4] )
	{
	double	m[3][3];
	double	v[3];
	
	for( int i = 0; i < 3; i++ )
		{
		for( int j = 0; j < 3; j++ )
			m[i][j] = matr[i][j];

		v[i] = matr[i][3];
		}

	matrix = CMatrix( m );
	shift  = CVector3d( v );
	}


CTransform::CTransform( const CMatrix& _matrix, const CVector3d& _shift )
	{
	matrix	= _matrix;
	shift	= _shift;
	}


CTransform::~CTransform()
	{
	}


CTransform CTransform::operator * ( const CTransform& trf ) const
	{
	CTransform	out;
		
	out.shift = matrix * trf.shift + shift;

	out.matrix = matrix * trf.matrix;

	return out;
	}


void CTransform::Translate( double x, double y, double z )
	{
	shift[0] += x;
	shift[1] += y;
	shift[2] += z;
	}


void CTransform::Translate( const CVector3d& vec )
	{
	shift += vec;
	}


void CTransform::Scale( double factor )
	{
	matrix	*= factor;
	shift	*= factor;
	}


void CTransform::ScaleMatrix( double factor )
	{
	matrix	*= factor;
	}


void CTransform::Rotate( double angle, double x, double y, double z )
	{
	if( angle == 0.0 || x == 0.0 && y == 0.0 && z == 0.0 )
		return;

	CMatrix matRotation( angle, x, y, z );

	matrix = matRotation * matrix;
	shift = matRotation * shift;
	}


void CTransform::Rotate( double angle, const CVector3d& vec )
	{
	Rotate( angle, vec[0], vec[1], vec[2] );
	}


bool CTransform::IsMirrored()
	{
	double	det = matrix.Determinant();

	return det < 0.0;
	}


void CTransform::MakeGLMatrix( double glMatrix[16] ) const
	{
	matrix.MakeGLMatrix( glMatrix );

	glMatrix[3]  = 0.0;
	glMatrix[7]  = 0.0;
	glMatrix[11] = 0.0;

	glMatrix[12] = shift[0];
	glMatrix[13] = shift[1];
	glMatrix[14] = shift[2];

	glMatrix[15] = 1.0;
	}


void CTransform::GetRotationMatrix( CMatrix& matr )
	{
	matr = matrix;
	}


CVector3d& CTransform::GetShiftVector()
	{
	return shift;
	}


void CTransform::SetRotationMatrix( CMatrix& matr )
	{
	matrix = matr;
	}


void CTransform::SetShiftVector( CVector3d& vec )
	{
	shift = vec;
	}


bool CTransform::ComputeInverseTrf( CTransform& InverseTrf )
	{
	CMatrix		Matr, InverseMatr;
	CVector3d	InverseShift;

	bool ok = matrix.Inverse( InverseMatr );

	if( !ok )
		return false;

	InverseShift = -( InverseMatr * shift );

	InverseTrf = CTransform( InverseMatr, InverseShift );

	return true;
	}


bool CTransform::operator == ( const CTransform& trf ) const
	{
	return ( matrix	== trf.matrix && shift	== trf.shift );
	}


bool CTransform::operator != ( const CTransform& trf ) const
	{
	return !( matrix == trf.matrix && shift == trf.shift );
	}

CTransform CTransform::IwAxis2Placement2CTransform(IwAxis2Placement const& iwxform)
	{
		IwPoint3d o = iwxform.GetOrigin();
		IwVector3d x = iwxform.GetXAxis();
		IwVector3d y = iwxform.GetYAxis();
		IwVector3d z = iwxform.GetZAxis();
		double xform[3][4];
		for(int j = 0; j < 3; ++j)
		{
			xform[j][0] = x[j];
			xform[j][1] = y[j];
			xform[j][2] = z[j];

			xform[j][3] = o[j];
		}
		return CTransform(xform);
	}

//******************** CBounds3d ****************************
CBounds3d::CBounds3d() : bInitiated( false )
	{
	}


CBounds3d::~CBounds3d()
	{
	}


CBounds3d::CBounds3d( const CBounds3d& bounds )
	{
	pt0 = bounds.pt0;
	pt1 = bounds.pt1;
	bInitiated = bounds.bInitiated;
	}


void CBounds3d::Reset() 
	{ 
	bInitiated = false;
	pt0 = CPoint3d( 0.0, 0.0, 0.0 );
	pt1 = CPoint3d( 0.0, 0.0, 0.0 );
	}


CBounds3d& CBounds3d::operator= ( const CBounds3d& bounds )
	{ 
	pt0 = bounds.pt0;
	pt1 = bounds.pt1;
	bInitiated = bounds.bInitiated;

	return *this;
	}


void CBounds3d::Expand( const CPoint3d& pt )
	{
	if( !bInitiated )
		{
		pt0 = pt - CPoint3d(0.001, 0.001, 0.001); // "0.001" is the min bounds size 
		pt1 = pt + CPoint3d(0.001, 0.001, 0.001);
		bInitiated = true;
		}
	else
		{
		pt0[0] = std::min( pt0[0], pt[0] );
		pt0[1] = std::min( pt0[1], pt[1] );
		pt0[2] = std::min( pt0[2], pt[2] );
			
		
		pt1[0] = std::max( pt1[0], pt[0] );
		pt1[1] = std::max( pt1[1], pt[1] );
		pt1[2] = std::max( pt1[2], pt[2] );
		}
	}


void CBounds3d::Expand( const CBounds3d& bounds )
	{
	if ( !bounds.bInitiated ) return;

	CPoint3d inputBoundsSize = bounds.pt0 - bounds.pt1;
	double boundsSize = inputBoundsSize.Length();

	if ( boundsSize < 0.001 ) return; // is considered as empty

	if( !bInitiated )
		{
		*this = bounds;
		bInitiated = true;
		}
	else
		{
		pt0[0] = std::min( pt0[0], bounds.pt0[0] );
		pt0[1] = std::min( pt0[1], bounds.pt0[1] );
		pt0[2] = std::min( pt0[2], bounds.pt0[2] );

		pt1[0] = std::max( pt1[0], bounds.pt1[0] );
		pt1[1] = std::max( pt1[1], bounds.pt1[1] );
		pt1[2] = std::max( pt1[2], bounds.pt1[2] );
		}
	}
	

void CBounds3d::Expand( double delta )
	{
	int		i;

	for( i = 0; i < 3; i++ )
		{
		pt0[i] -= delta;
		pt1[i] += delta;
		}
	}


bool CBounds3d::Intersects( const CBounds3d& Bounds ) const
	{
	for( int i = 0; i < 3; i++ )
		{
		if( pt0[i] > Bounds.pt1[i] )
			return false;

		if( pt1[i] < Bounds.pt0[i] )
			return false;
		}

	return true;
	}


bool CBounds3d::Contains( const CPoint3d& pt )
	{
	for( int i = 0; i < 3; i++ )
		{
		if( pt[i] < pt0[i] || pt[i] > pt1[i] )
			return false;
		}

	return true;
	}


bool CBounds3d::Contains( const CPoint3d& pt, int viewPlane, bool b2dSpace )
	{
	int	idx[4] = { -1, 2, 1, 0 }; 
	int	skip = idx[ viewPlane ];
	bool	bCheckIn2d = !b2dSpace;

	for( int i = 0; i < 3; i++ )
		{
		if( bCheckIn2d && i == skip )
			continue;
		
		if( pt[i] < pt0[i] || pt[i] > pt1[i] )
			return false;
		}

	return true;
	}


//******************** CBounds2d ****************************
CBounds2d::CBounds2d() : bInitiated( false )
	{
	}


CBounds2d::~CBounds2d()
	{
	}


CBounds2d::CBounds2d( double x0, double y0, double x1, double y1 ) :
					  pt0( x0, y0 ), pt1( x1, y1 )
	{
	}


CBounds2d::CBounds2d( const CBounds2d& bounds )
	{
	pt0 = bounds.pt0;
	pt1 = bounds.pt1;
	bInitiated = bounds.bInitiated;
	}


void CBounds2d::Reset() 
	{ 
	bInitiated = false;
	}


CBounds2d& CBounds2d::operator= ( const CBounds2d& bounds )
	{ 
	pt0 = bounds.pt0;
	pt1 = bounds.pt1;
	bInitiated = bounds.bInitiated;

	return *this;
	}


CBounds2d& CBounds2d::Resize( double x0, double y0, double x1, double y1 )
	{
	pt0[0] = std::min( x0, x1 );
	pt0[1] = std::min( y0, y1 );

	pt1[0] = std::max( x0, x1 );
	pt1[1] = std::max( y0, y1 );

	return *this;
	}



void CBounds2d::Expand( const CPoint2d& pt )
	{
	if( !bInitiated )
		{
		pt0 = pt;
		pt1 = pt;
		bInitiated = true;
		}
	else
		{
		pt0[0] = std::min( pt0[0], pt[0] );
		pt0[1] = std::min( pt0[1], pt[1] );
			
		
		pt1[0] = std::max( pt1[0], pt[0] );
		pt1[1] = std::max( pt1[1], pt[1] );
		}
	}


void CBounds2d::Expand( const CBounds2d& bounds )
	{
	if( !bInitiated )
		{
		*this = bounds;
		bInitiated = true;
		}
	else
		{
		pt0[0] = std::min( pt0[0], bounds.pt0[0] );
		pt0[1] = std::min( pt0[1], bounds.pt0[1] );

		pt1[0] = std::max( pt1[0], bounds.pt1[0] );
		pt1[1] = std::max( pt1[1], bounds.pt1[1] );
		}
	}
	

void CBounds2d::Extend( double epsX, double epsY )
	{
	pt0[0] -= epsX;
	pt0[1] -= epsY;

	pt1[0] += epsX;
	pt1[1] += epsY;
	}


bool CBounds2d::Intersects( const CBounds2d& Bounds ) const
	{
	for( int i = 0; i < 2; i++ )
		{
		if( pt0[i] > Bounds.pt1[i] )
			return false;

		if( pt1[i] < Bounds.pt0[i] )
			return false;
		}

	return true;
	}


bool CBounds2d::Contains( const CPoint2d& pt )
	{
	for( int i = 0; i < 2; i++ )
		{
		if( pt[i] < pt0[i] || pt[i] > pt1[i] )
			return false;
		}

	return true;
	}


