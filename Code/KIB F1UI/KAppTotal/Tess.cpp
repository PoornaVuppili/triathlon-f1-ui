#include <windows.h>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "Tess.h"
#include "Part.h"

#ifdef SM_VERSION_STRING
#if SM_VERSION_NUMBER >= 80617
#include <IwGfxVertexArray.h>
#endif
#endif


CTess::CTess( CPart* pPart ) : IwPolygonOutputCallback()
	{
	m_pPart = pPart;
	}


CTess::~CTess()
	{
	}


IwStatus CTess::OutputPolygon(
	ULONG			lPolygonType,
	ULONG			lNumPoints, 
	IwPoint3d*		aPolygonPoints, 
	IwVector3d*		aPolygonNormals,
	IwPoint2d*		aPolygonUVPoints,
	IwSurface*		pSurface,
	IwFace*			pFace
#ifdef SM_VERSION_STRING
#if SM_VERSION_NUMBER >= 80617
   , IwGfxArraySet *pArraySet
#endif
#endif
   )
	{
	int				i, j;
	CFacet			Facet;

	for( i = 0; i < 3; i++ )
		{
		for( j = 0; j < 3; j++ )
			{
			Facet.points[i][j]		= aPolygonPoints[i][j];
			Facet.normals[i][j]		= aPolygonNormals[i][j];
			}
		}

	m_pPart->AddFacet( Facet );

	m_pPart->UpdateBounds( Facet.points[0] );
	m_pPart->UpdateBounds( Facet.points[1] );
	m_pPart->UpdateBounds( Facet.points[2] );

	return IW_SUCCESS;
	}
