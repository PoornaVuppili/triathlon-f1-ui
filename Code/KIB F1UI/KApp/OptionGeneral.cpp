#include "OptionGeneral.h"
#include "ViewWidget.h"
#include "ui_OptionGeneral.h"
#include <QColorDialog>
#include <QMessageBox>
#include <QSettings>

#include "..\KUtility\KOracleDatabase.h"

OptionGeneral::OptionGeneral(QWidget *parent)
    : OptionPage(parent)
{
    ui = new Ui::OptionGeneral();
    ui->setupUi(this);

    // viewWidget background color configuration
    topColor = topColorX = viewWidget->GetTopColor();
    bottomColor = bottomColorX = viewWidget->GetBottomColor();
    colorChanged = false;

    ui->listWidgetColor->addItems( QStringList() << "Background top color default(50,140,240)" << "Background bottom color default(200,255,255)" );
    ui->listWidgetColor->setCurrentRow(0);

    ui->labelColor->setAutoFillBackground(true);
    QPalette pal = ui->labelColor->palette();
    pal.setColor(QPalette::Window, topColor);
    ui->labelColor->setPalette(pal);

    connect(ui->listWidgetColor, SIGNAL(currentRowChanged(int)), this, SLOT(SetColorIndex(int)));

    // database setting
	QSettings settings;

    DBName = "";//settings.value("DatabaseName", "CMISPRD").toString();
    DBHost = "";//settings.value("DatabaseHostname", "ODAP0.Stryker.local").toString();
    DBPort = 0;// settings.value("DatabasePort", 1521).toInt();
    DBUser = "";// settings.value("DatabaseUsername", "confsoft").toString();
    DBPassword = "";// settings.value("DatabasePassword", "apps_readonly").toString();

    // Oracle database connection
    ui->lineEditDatabaseName->setText( DBName );
    ui->lineEditHostname->setText( DBHost );
    ui->lineEditPort->setText( ""/*QString::number(DBPort)*/ );
    ui->lineEditUsername->setText( DBUser );
    ui->lineEditPassword->setText( DBPassword );

    ui->pushButtonTestOracle->setEnabled(false);
    connect(ui->lineEditDatabaseName, SIGNAL(textEdited(const QString&)), this, SLOT(on_DatabaseSettingChanged()));
    connect(ui->lineEditHostname, SIGNAL(textEdited(const QString&)), this, SLOT(on_DatabaseSettingChanged()));
    connect(ui->lineEditPort, SIGNAL(textEdited(const QString&)), this, SLOT(on_DatabaseSettingChanged()));
    connect(ui->lineEditUsername, SIGNAL(textEdited(const QString&)), this, SLOT(on_DatabaseSettingChanged()));
    connect(ui->lineEditPassword, SIGNAL(textEdited(const QString&)), this, SLOT(on_DatabaseSettingChanged()));
}

OptionGeneral::~OptionGeneral()
{
    delete ui;
}

void OptionGeneral::SetColorIndex(int index)
{
    QPalette pal = ui->labelColor->palette();
    if( index==0 )
        pal.setColor(QPalette::Window, topColor);
    else if( index==1 )
        pal.setColor(QPalette::Window, bottomColor);
    ui->labelColor->setPalette(pal);
}

void OptionGeneral::on_pushButtonColor_clicked()
{
    int index = ui->listWidgetColor->currentRow();
    if( index==0 )
    {
        QColor c = QColorDialog::getColor(topColor);
        if( c.isValid() && c != topColor )
        {
            topColor =  c;
            viewWidget->SetTopColor(topColor);
        }
    }
    else if( index==1 )
    {
        QColor c = QColorDialog::getColor(bottomColor);
        if( c.isValid() && c != bottomColor )
        {
            bottomColor = c;
            viewWidget->SetBottomColor(bottomColor);
        }
    }
    colorChanged = (topColor != topColorX || bottomColor != bottomColorX);
    SetColorIndex(index);
}

// Test if the database settings on the UI have changed
// Return: true - changed; false - otherwise
bool OptionGeneral::IsDatabaseSettingChanged()
{
    return  ui->lineEditDatabaseName->text() != DBName ||
            ui->lineEditHostname->text()     != DBHost ||
            ui->lineEditPort->text().toInt() != DBPort ||
            ui->lineEditUsername->text()     != DBUser ||
            ui->lineEditPassword->text()     != DBPassword;
}

void OptionGeneral::on_DatabaseSettingChanged()
{
    ui->pushButtonTestOracle->setEnabled(IsDatabaseSettingChanged());
}

void OptionGeneral::on_pushButtonTestOracle_clicked()
{
    KOracleDatabase db;

    QString txt = (db.SetParameters(
        ui->lineEditDatabaseName->text(),
        ui->lineEditHostname->text(),
        ui->lineEditPort->text().toInt(),
        ui->lineEditUsername->text(),
        ui->lineEditPassword->text()) &&
        db.TestConnection()) ? "Connection is successful" : "Connection failed";

    QMessageBox::information(this, "Test database connection", txt);
}

void OptionGeneral::OnOK()
{
    // save the oracle settings if changed and valid
    if( IsDatabaseSettingChanged() )
    {
        QString name = ui->lineEditDatabaseName->text();
        QString host = ui->lineEditHostname->text();
        int port = ui->lineEditPort->text().toInt();
        QString user = ui->lineEditUsername->text();
        QString password = ui->lineEditPassword->text();

        KOracleDatabase db;

        if( db.SetParameters(name,host,port,user,password) &&
            db.TestConnection() )
        {
            QSettings settings;

            settings.setValue("DatabaseName", name);
            settings.setValue("DatabaseHostname", host);
            settings.setValue("DatabasePort", port);
            settings.setValue("DatabaseUsername", user);
            settings.setValue("DatabasePassword", password);
        }
        else
            QMessageBox::information(this, "Error", "Oracle settings are invalid");
    }

    // change the color
    if( colorChanged )
    {
        if( topColor != topColorX )
            viewWidget->SetTopColor(topColor);
        if( bottomColor != bottomColorX )
            viewWidget->SetBottomColor(bottomColor);
    }
}

void OptionGeneral::OnCancel()
{
    // restore the color if changed
    if( colorChanged )
    {
        if( topColor != topColorX )
            viewWidget->SetTopColor(topColorX);
        if( bottomColor != bottomColorX )
            viewWidget->SetBottomColor(bottomColorX);
    }
}
