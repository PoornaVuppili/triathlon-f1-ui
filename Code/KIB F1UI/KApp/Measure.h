#pragma once
#include "..\KUtility\TPickRay.h"
#include "..\KUtility\TLineSegment3.h"
#include "..\KUtility\TPlane3.h"
#include "..\KUtility\TCircle3.h"
#include <qgl.h>

class IwCurve;
class IwEdge;
class IwEdgeuse;
class IwVertex;
class IwSurface;
class IwFace;
class IwBSplineCurve;
class IwCircle;
class IwBSplineSurface;

enum MeasureMode
{
    None,
    EntityDistance,
    EntityAngle,
    EntityRadius,
    ScreenDistance,
    ScreenAngle,
    ScreenRadius
};

extern double MeasureCloseTolerance;

// Selected object type for measurement
enum MeasureSelectType
{
    SelectNone				= 0x000,       // nothing 
    SelectPoint				= 0x001,       // point
    SelectLine				= 0x010,       // line
    SelectCircle			= 0x020,       // circle
    SelectPlane				= 0x040,       // plane
    SelectCurve				= 0x080,       // curve
    SelectEdge				= 0x100,       // edge (curve with additional topology info)
    SelectSurface			= 0x200,       // surface
    SelectFace				= 0x400,       // opaque face (surface with additional topology info)
    SelectFaceTransparent   = 0x800,// transparent face (surface with additional topology info)

    // The select types can be bitwise-OR'ed as masks to decide what entities can be selected
    // - if set, only linear curve or planar surface can selected
    SelectLinearPlanar      = 0x1000000,
    // - if set, only non-linear curve or non-planar surface can selected
    SelectNonLinearPlanar   = 0x2000000,
    // - (non-)linear curve or edge
    SelectLinearCurveEdge       = SelectLine | SelectCurve | SelectEdge | SelectLinearPlanar,
    SelectNonLinearCurveEdge    = SelectCircle | SelectCurve | SelectEdge | SelectNonLinearPlanar,
    // - (non-)planar surface or face
    SelectPlanarSurfaceFace     = SelectCircle | SelectPlane | SelectSurface | SelectFace | SelectLinearPlanar,
    SelectNonPlanarSurfaceFace  = SelectSurface | SelectFace | SelectNonLinearPlanar,
    // - everything
    SelectAll               = SelectPoint | SelectLine | SelectCircle | SelectPlane | SelectCurve | SelectEdge | SelectSurface | SelectFace| SelectFaceTransparent
};

inline MeasureSelectType operator|(MeasureSelectType a, MeasureSelectType b)
{ return static_cast<MeasureSelectType>(static_cast<int>(a) | static_cast<int>(b)); }

// Entity for measurement
class MeasureEntity
{
public:
    // Constructor: default to no selected entity
    MeasureEntity();

    // Constructor from TPoint3<>,IwPoint3d
    template<class T> MeasureEntity(const TPoint3<T>& pt);
    MeasureEntity(const IwPoint3d& pt);

    // Constructor line from TPoint3<>, TPoint3<> or TLineSegment3 + hit point
    template<class T> MeasureEntity(const TPoint3<T>& pt1, const TPoint3<T>& pt2, const TPoint3<T>& hitpoint);
    template<class T> MeasureEntity(const TLineSegment3<T>& ls, const TPoint3<T>& hitpoint);

    // Constructor circle from center, radius, and normal + hit point
    template<class T> MeasureEntity(const TPoint3<T>& center, T radius, const TPoint3<T>& nor, const TPoint3<T>& hitpoint);

    // Constructor plane from TPoint3<>, TPoint3<>, TPoint3<> + hit point
    template<class T> MeasureEntity(const TPoint3<T>& p1, const TPoint3<T>& p2, const TPoint3<T>& p3, const TPoint3<T>& hitpoint);

    // Constructor from IwCurve*, IwEdge*, IwSurface*, IwFace*
    MeasureEntity(IwCurve* c, double hitU, const IwPoint3d& hitpoint);
    MeasureEntity(IwEdge* e, double hitU, const IwPoint3d& hitpoint);
    MeasureEntity(IwSurface* s, double hitU, double hitV, const IwPoint3d& hitpoint);
    MeasureEntity(IwFace* f, double hitU, double hitV, const IwPoint3d& hitpoint);

    ~MeasureEntity();

    // Copy constructor and assignment operator 
    // Note: use const reference so it works with vector<> etc., 
    //       but internally we transfer the pointers and 
    //       display lists, which modifies 'e'. 
    MeasureEntity(const MeasureEntity& e);
    MeasureEntity& operator= (const MeasureEntity& e);

    // operator == , return true if
    //   points : the coordinates are close
    //   line   : two end points are close (allow flipping)
    //   circle : the center and radius are close + normal in the same/opposite direction
    //   plane  : four corners are all close
    //   curve/edge/survace/face : the same object Iw*
    bool operator==(const MeasureEntity& ent);
    bool operator!=(const MeasureEntity& ent) { return !operator==(ent); }

    // Draw the measure entity
    void Draw(double onePixelSize=0.000001);

    // Helper routines for selecting entities
    //  (I) ray         : the pick ray
    //  (I) tolerance   : the selection tolerance
    //  (O) ent         : the selected entity
    //  (O) pt          : the picked point on the selected entity 
    //  (I,optional)mask: a bitwise-OR'ed mask of entity types that can be selected
    // select 1 point
    static bool Select(const Point3Float& p1, 
                       const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll);
    // select 2 points or line segment between 2 points
    static bool Select(const Point3Float& p1, const Point3Float& p2, 
                       const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll);
    // select 4 points, 4 line segments, or planar rectangle/parallelogram defined by 3 points
    //  p0 --- p1         p0 --- p1
    //   |     |     or    \      \
    //  p2-----             p2-----
    static bool Select(const Point3Float& p1, const Point3Float& p2, const Point3Float& p3, 
                       const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll);

    static bool Select(IwBrep* brep,                        const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
    static bool Select(const IwTArray<IwVertex*>& vertexes, const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
    static bool Select(const IwTArray<IwPoint3d>& points,   const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
    static bool Select(const IwTArray<IwCurve*>& curves,    const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
    static bool Select(const IwTArray<IwEdge*>& edges,      const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
    static bool Select(const IwTArray<IwSurface*>& surfaces,const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);
    static bool Select(const IwTArray<IwFace*>& faces,      const PickRayFloat& ray, const float tolerance, MeasureEntity& ent, MeasureSelectType mask=SelectAll, MeasureEntity* EntToAvoid=NULL);

    // Get the selected entity type
    MeasureSelectType GetSelectType() const { return selectType; }

    // Get the cursor hip point
    const Point3Float& GetHitPoint() { return hitPoint; }

    // To be removed; compatible with TriathlonF1(PS)
    //IwCurve* GetCurve() { return selectType == SelectCurve ? selectValue.curve.curve : NULL; }
	void Transform(IwAxis2Placement& transMatrix);

	void UseHitPointToMeasure(bool val) {bUseHitPointToMeasure=val;};
	bool IsUseHitPointToMeasure() {return bUseHitPointToMeasure;};

private:
    void			HighLightHitPoint(Point3Float point);
    void			HighLightCurve(IwCurve* curve);
	void			HighLightEdge(IwEdge* eg);
    void			HighLightSurface(IwSurface* surf);
	void			HighLightFaceEdge(IwEdgeuse* edgeUse, double onePixelSize=0.000001);
	void			HighLightFace(IwFace* fc, double onePixelSize=0.000001);

    MeasureSelectType           selectType;

    union MeasureSelectValue
    {
        struct {
            double x,y,z;
        }                       point;
        struct {
            double xs,ys,zs;    // start point
            double xe,ye,ze;    // end point
        }                       line;
        struct {
            double x,y,z;       // center
            double nx,ny,nz;    // normal
            double rad;         // radius
        }                       circle;
        struct {
            double cx,cy,cz;    // orgin
            double ux,uy,uz;    // end point along u
            double vx,vy,vz;    // end point along v
        }                       plane;
        struct {
            IwCurve* curve;
            double   hitU;      // hit parameter
            GLuint   dl;        // display list for drawing
			IwCurve* originalCurvePointer;// If the scene is transformed by , the selected curve
										  // need to make a 2nd copy and transformed. The "curve" is the 2nd copy 
										  // transformed curve. The "originalCurvePointer" is the original 
										  // un-transformed curve. The pointer helps to determine the curve 
										  // has been selected or not. 
        }                       curve;
        struct {
            IwEdge*  edge;
            double   hitU;      // hit parameter
            GLuint   dl;        // display list for drawing
			IwEdge*  originalEdgePointer;// See "curve" comments above.
        }                       edge;
        struct {
            IwSurface*	surface;
            double   hitU,hitV; // hit parameter
            GLuint   dl;        // display list for drawing
			IwSurface* originalSurfacePointer;// See "curve" comments above.
        }                       surface;
        struct {
            IwFace*  face;
            double   hitU,hitV; // hit parameter
            GLuint   dl;        // display list for drawing
			IwFace*  originalFacePointer;// See "curve" comments above.
        }                       face;
    }                           selectValue;

    Point3Float         hitPoint;				// cursor hit point
	bool				bUseHitPointToMeasure;	// use hit point to measure properties

    void Reset();
    void CopyTransfer(MeasureEntity& e);

    friend class MeasureResult;
};

// Constructor: point
template<class T> MeasureEntity::MeasureEntity(const TPoint3<T>& pt)
{
    selectType = SelectPoint;
    selectValue.point.x = pt.x();
    selectValue.point.y = pt.y();
    selectValue.point.z = pt.z();
    hitPoint.SetXYZ(pt.x(), pt.y(), pt.z());
}

// Constructor: line
template<class T> MeasureEntity::MeasureEntity(const TPoint3<T>& pt1, const TPoint3<T>& pt2, const TPoint3<T>& hit)
{
    selectType = SelectLine;
    selectValue.line.xs = pt1.x(); selectValue.line.ys = pt1.y(); selectValue.line.zs = pt1.z();
    selectValue.line.xe = pt2.x(); selectValue.line.ye = pt2.y(); selectValue.line.ze = pt2.z();
    hitPoint.SetXYZ(hit.x(), hit.y(), hit.z());
}
template<class T> MeasureEntity::MeasureEntity(const TLineSegment3<T>& ls, const TPoint3<T>& hit)
{
    selectType = SelectLine;
    selectValue.line.xs = ls.GetX1(); selectValue.line.ys = ls.GetY1(); selectValue.line.zs = ls.GetZ1();
    selectValue.line.xe = ls.GetX2(); selectValue.line.ye = ls.GetY2(); selectValue.line.ze = ls.GetZ2();
    hitPoint.SetXYZ(hit.x(), hit.y(), hit.z());
}

// Constructor: circle
template<class T> MeasureEntity::MeasureEntity(const TPoint3<T>& center, T radius, const TPoint3<T>& nor, const TPoint3<T>& hit)
{
    selectType = SelectCircle;
    selectValue.circle.x = center.x(); selectValue.circle.y = center.y(); selectValue.circle.z = center.z();
    selectValue.circle.nx = nor.x();   selectValue.circle.ny = nor.y();   selectValue.circle.nz = nor.z();
    selectValue.circle.rad = radius;
    hitPoint.SetXYZ(hit.x(), hit.y(), hit.z());
}

template<class T> MeasureEntity::MeasureEntity(const TPoint3<T>& p1, const TPoint3<T>& p2, const TPoint3<T>& p3, const TPoint3<T>& hit)
{
    selectType = SelectPlane;
    selectValue.plane.cx = p1.x(); selectValue.plane.cy = p1.y(); selectValue.plane.cz = p1.z();
    selectValue.plane.ux = p2.x(); selectValue.plane.uy = p2.y(); selectValue.plane.uz = p2.z();
    selectValue.plane.vx = p3.x(); selectValue.plane.vy = p3.y(); selectValue.plane.vz = p3.z();
    hitPoint.SetXYZ(hit.x(), hit.y(), hit.z());
}

// Entity measurement result
class MeasureResult
{
public:
    MeasureResult() : valid(false) {}
    ~MeasureResult() {}

    void Draw(double onePixelSize=0.000001);
    void Reset();
    bool IsValid() const { return valid; }

    // Compute the measurement result based on mode and entities
    static MeasureResult Compute(MeasureMode measureMode, std::vector<MeasureEntity>& entities);

    // Get the measurement result if valid. NaN otherwise.
    double GetResult();

private:
    static MeasureResult ComputeEntityDistance(MeasureEntity& e0, MeasureEntity& e1);
    // point -> point, line, circle, plane, curve, edge, surface, face
    static MeasureResult ComputeEntityDistance(const Point3Double& pt, MeasureEntity& e);
    // line  -> line, circle, plane, curve, edge, surface, face
    static MeasureResult ComputeEntityDistance(const LineSegment3Double& ln, const Point3Float& hitPt, MeasureEntity& e);
    // circle -> entity
    static MeasureResult ComputeEntityDistance(const Circle3Double& circle, const Point3Float& hitPt, MeasureEntity& e);
    // plane (3 points) -> entity
    static MeasureResult ComputeEntityDistance(const Point3Double& pc, const Point3Double& pu, const Point3Double& pv, const Point3Float& hitPt, MeasureEntity& e);
    // curve/edge -> entity
    static MeasureResult ComputeEntityDistance(IwCurve* crv, const IwExtent1d& crvIntv, double crvHitU, const Point3Float& hitPt, MeasureEntity& e);
    
    static MeasureResult ComputeEntityAngle(MeasureEntity& e0, MeasureEntity& e1);
    static MeasureResult ComputeEntityAngle(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2);
    static MeasureResult ComputeEntityAngle(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2, MeasureEntity& e3);

    static MeasureResult ComputeEntityRadius(MeasureEntity& e0);

    static MeasureResult ComputeScreenDistance(MeasureEntity& e0, MeasureEntity& e1);
    static MeasureResult ComputeScreenAngle(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2, MeasureEntity& e3);
    static MeasureResult ComputeScreenRadius(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2);

    // functions for distance measure
    // point -> point, line, plane (including circle), curve, edge, surface & face
    static MeasureResult Distance(const Point3Double& pt, const Point3Double& qt);
    static MeasureResult Distance(const Point3Double& pt, const LineSegment3Double& ln);
    static MeasureResult Distance(const Point3Double& pt, const Plane3Double& pl);
    static MeasureResult Distance(const Point3Double& pt, IwCurve* crv);
    static MeasureResult Distance(const Point3Double& pt, IwEdge* edge);
    static MeasureResult Distance(const Point3Double& pt, IwCurve* crv, const IwExtent1d& intv);
    static MeasureResult Distance(const Point3Double& pt, IwSurface* surf);
    static MeasureResult Distance(const Point3Double& pt, IwFace* face);
    // line -> line, plane (including circle), curve, edge, surface & face
    static MeasureResult Distance(const LineSegment3Double& ln1, const LineSegment3Double& ln2);
    static MeasureResult Distance(const LineSegment3Double& ln, const Plane3Double& pl);
    static MeasureResult Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwCurve* crv, double hitU);
    static MeasureResult Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwEdge* edge, double hitU);
    static MeasureResult Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwCurve* crv, const IwExtent1d& intv, double hitU);
    static MeasureResult Distance(IwCurve* crv1, const IwExtent1d& intv1, double hitU1, IwCurve* crv2, const IwExtent1d& intv2, double hitU2);
    static MeasureResult Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwSurface* surf, double hitU, double hitV);
    static MeasureResult Distance(const LineSegment3Double& ln, const Point3Float& hitPt, IwFace* face, double hitU, double hitV);
    static bool LocalDistance(IwCurve* crv, const IwExtent1d& intv, double hit, IwSurface* surf, const IwExtent2d& ints, double hitU, double hitV, MeasureResult& res);
    static bool LocalDistance(IwCurve* crv, const IwExtent1d& intv, double hit, IwFace* face, double hitU, double hitV, MeasureResult& res);
    static bool GlobalDistance(IwCurve* crv, const IwExtent1d& intv, IwSurface* surf, MeasureResult& r);
    static bool GlobalDistance(IwCurve* crv, const IwExtent1d& intv, IwFace* face, MeasureResult& r);
   // circle -> plane (including circle), curve, edge, surface & face
    static MeasureResult Distance(const Circle3Double& cr, const Plane3Double& pl);
    static MeasureResult Distance(const Circle3Double& cr, const Point3Float& hitPt, IwCurve* crv, double hitU);
    static MeasureResult Distance(const Circle3Double& cr, const Point3Float& hitPt, IwEdge* edge, double hitU);
    static MeasureResult Distance(const Circle3Double& cr, const Point3Float& hitPt, IwSurface* surf, double hitU, double hitV);
    static MeasureResult Distance(const Circle3Double& cr, const Point3Float& hitPt, IwFace* face, double hitU, double hitV);
    // plane -> curve, edge, surface & face
    static MeasureResult Distance(IwSurface* surf, double hitU, double hitV, IwCurve* crv, double hit);
    static MeasureResult Distance(IwSurface* surf, double hitU, double hitV, IwEdge* edge, double hit);
    static MeasureResult Distance(IwSurface* surf1, double hitU1, double hitV1, IwSurface* surf2, double hitU2, double hitV2);
    static MeasureResult Distance(IwFace* face1, double hitU1, double hitV1, IwFace* face2, double hitU2, double hitV2);
    static bool LocalDistance(IwSurface* surf1, const IwExtent2d& uv1, const IwPoint2d& hitUV1, IwSurface* surf2, const IwExtent2d& uv2, const IwPoint2d& hitUV2, MeasureResult& res);
    static bool LocalDistance(IwFace* face1, const IwExtent2d& uv1, const IwPoint2d& hitUV1, IwFace* face2, const IwExtent2d& uv2, const IwPoint2d& hitUV2, MeasureResult& res);
    static bool GlobalDistance(IwFace* face1, IwFace* face2, MeasureResult& r);
    static bool GlobalDistance(IwSurface* surface1, IwSurface* surface2, MeasureResult& r);

    // functions for angle measure
    static MeasureResult Angle(const LineSegment3Double& L1, const LineSegment3Double& L2);
    static MeasureResult Angle(const LineSegment3Double& ls, const Plane3Double& pn);
    static MeasureResult Angle(const Plane3Double& pn1, const Point3Double& hit1, const Plane3Double& pn2, const Point3Double& hit2);
    static bool IsLinear(MeasureEntity& e);
    static bool IsPlanar(MeasureEntity& e);
    static LineSegment3Double GetLineSegment(MeasureEntity& e);
    static Plane3Double       GetPlane(MeasureEntity& e);
    // get one vector (among many) that's normal to 'v' with the same norm
    static Vector3Double NormalTo(const Vector3Double& v);

    // create curve & map hit point (caller responsible for deleting the returned value)
    static IwBSplineCurve* CreateCurve(const LineSegment3Double& ln, const Point3Float& hitPt, double& hitV);
    static IwCircle* CreateCircle(const Circle3Double& cr, const Point3Float& hitPt, double& hitV);
    static IwBSplineSurface* CreateSurface(const Point3Double& pc,const Point3Double& p1,const Point3Double& p2, const Point3Float& hitPt, double& hitU, double& hitV);

    union
    {
        // distance related
        struct {
            double distance;    // distance in mm
            double x1,y1,z1;    // a line between two points to visualize the distance
            double x2,y2,z2;
            bool   isMax;       // if true, 'distance' is max distance (default is min)
        } distance;
        // angle related 
        struct {
            double degree;      // angle in degrees
            double xc,yc,zc;    // triangle to visualize the angle
            double x1,y1,z1;
            double x2,y2,z2;
        } angle;
        // radius related
        struct {
            double radius;      // radius in mm
            double xc,yc,zc;    // circle to visualize the radius
            double nx,ny,nz;
        } radius;
    } value;

    MeasureMode mode;
    bool valid;
};
