#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "MeasureScreenRadius.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"

CMeasureScreenRadius::CMeasureScreenRadius( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMeasureScreenRadius";

	m_toolbar = new QToolBar("Measure Radius");

	QLabel* labelResultRadius = new QLabel(tr("Radius:"));
	labelResultRadius->setFont(m_fontBold);
	m_toolbar->addWidget(labelResultRadius);

	m_textMeasuredRadius = new QLineEdit(tr(" "));
	m_textMeasuredRadius->setReadOnly(true);
	m_textMeasuredRadius->setFixedWidth(62);
	m_textMeasuredRadius->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMeasuredRadius);

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	//m_toolbar->setMovable( false );	
	m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	// Measure Angle here
	m_mouseMiddleDown = false;
	m_mouseLeftDown = false;
	m_mouseClickedCount = 0;
	m_mouseFirstDownPoint = IwPoint3d();
	m_mouseSecondDownPoint = IwPoint3d();
	m_mouseThirdDownPoint = IwPoint3d();
	m_circle = NULL;

}

CMeasureScreenRadius::~CMeasureScreenRadius()
{
	if (m_circle) IwObjDelete(m_circle);
}

bool CMeasureScreenRadius::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseLeftDown = true;

	m_pView->Redraw();

	return true;
}

bool CMeasureScreenRadius::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseMiddleDown = true;

	m_mouseClickedCount = 0;
	m_mouseFirstDownPoint = IwPoint3d();
	m_mouseSecondDownPoint = IwPoint3d();
	m_mouseThirdDownPoint = IwPoint3d();
	if (m_circle) IwObjDelete(m_circle);
	m_circlePnts.RemoveAll();


	m_pView->Redraw();

	return true;
}
bool CMeasureScreenRadius::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_mouseLeftDown)
	{
		int uvw[3];
		uvw[0] = cursor.x();
		uvw[1] = cursor.y();
		uvw[2] = 0;
		IwPoint3d pnt;
		m_pView->UVWtoXYZ(uvw, pnt);
		if (m_mouseClickedCount%3 == 0)
		{
			// Also initialize other points
			m_mouseFirstDownPoint = pnt;
			m_mouseSecondDownPoint = IwPoint3d();
			m_mouseThirdDownPoint = IwPoint3d();
			m_textMeasuredRadius->clear();
			if (m_circle) IwObjDelete(m_circle);
			m_circlePnts.RemoveAll();
		}
		else if (m_mouseClickedCount%3 == 1)
			m_mouseSecondDownPoint = pnt;
		else if (m_mouseClickedCount%3 == 2)
			m_mouseThirdDownPoint = pnt;


		// calculate radius
		if (m_mouseClickedCount%3 == 2)
		{
			IwVector3d vec0 = m_mouseSecondDownPoint - m_mouseFirstDownPoint;
			IwVector3d vec1 = m_mouseThirdDownPoint - m_mouseFirstDownPoint;
			QString radiusText = QString("No solution");
			if ( !vec0.IsParallelTo(vec1, 0.001) )
			{
				IwContext& iwContext = m_pDoc->GetIwContext();
				m_circle = new (iwContext) IwCircle(m_mouseFirstDownPoint, m_mouseSecondDownPoint, m_mouseThirdDownPoint, 3, TRUE, &iwContext);
				double radius = m_circle->GetRadius();
				IwExtent1d dom = m_circle->GetNaturalInterval();
				double domMin = dom.GetMin();
				double domMax = dom.GetMax();
				int seg = 120;
				if (radius > 50)
				{
					IwAxis2Placement origin;
					double r;
					m_circle->GetCanonical(origin, r);
					IwVector3d vec11 = m_mouseFirstDownPoint - origin.GetOrigin();
					IwVector3d vec22 = m_mouseThirdDownPoint - origin.GetOrigin();
					double angle;
					vec11.AngleBetween(vec22, angle);
					domMax = angle/(2*IW_PI);
					seg = 60;
					m_circle->EquallySpacedPoints( domMin, domMax, seg, 0.001, &m_circlePnts, NULL);
					m_circlePnts.Add(m_mouseThirdDownPoint);
				}
				else
				{
					m_circle->EquallySpacedPoints( domMin, domMax, seg, 0.001, &m_circlePnts, NULL);

				}
				radiusText.setNum(radius, 'f', 3);
				m_textMeasuredRadius->clear();
			}
			m_textMeasuredRadius->setText(radiusText);
		}

		m_mouseClickedCount++;

	}
	m_mouseLeftDown = false;
	m_mouseMiddleDown = false;

	m_pView->Redraw();


	return true;
}


bool CMeasureScreenRadius::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{

	if ( m_mouseMiddleDown )
	{
		m_mouseClickedCount = 0;
		m_mouseFirstDownPoint = IwPoint3d();
		m_mouseSecondDownPoint = IwPoint3d();
		m_mouseThirdDownPoint = IwPoint3d();
		if (m_circle) IwObjDelete(m_circle);
		m_circlePnts.RemoveAll();
	}

	m_pView->Redraw();

	return true;
}


bool CMeasureScreenRadius::MouseRight( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseClickedCount = 0;
	m_mouseFirstDownPoint = IwPoint3d();
	m_mouseSecondDownPoint = IwPoint3d();
	m_mouseThirdDownPoint = IwPoint3d();
	if (m_circle) IwObjDelete(m_circle);
	m_circlePnts.RemoveAll();

	m_pView->Redraw();
	return true;
}

bool CMeasureScreenRadius::keyPressEvent( QKeyEvent* event, Viewport* viewport )
{
	if ( event->key() == Qt::Key_Escape )
		Reject();

	return false; // false: do not execute subsequent actions.
}

void CMeasureScreenRadius::Display(Viewport* vp)
{		
	glDrawBuffer( GL_BACK );
	glDisable( GL_LIGHTING );

	IwPoint3d pnt;
	IwVector3d offset = m_pView->GetLayerOffsetVector(1);
	glPointSize(5);
	// Display points
	if (m_mouseFirstDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseFirstDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if (m_mouseSecondDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseSecondDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if (m_mouseThirdDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseThirdDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	//// Display  circle 
	if ( m_circlePnts.GetSize() > 1 )
	{
		glLineWidth( 1.0 );
		glBegin( GL_LINES );
			for (unsigned i=1; i < m_circlePnts.GetSize(); i++)
			{
				pnt = m_circlePnts.GetAt(i-1) + offset;
				glVertex3d( pnt.x, pnt.y, pnt.z );
				pnt = m_circlePnts.GetAt(i) + offset;
				glVertex3d( pnt.x, pnt.y, pnt.z );
			}
		glEnd();
	}


	glPointSize(1);
	glEnable( GL_LIGHTING );
}

void CMeasureScreenRadius::OnAccept()
{
	m_bDestroyMe = true;
	m_pView->Redraw();
}
