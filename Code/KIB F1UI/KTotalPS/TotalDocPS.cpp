#include "TotalDocPS.h"
#include "TotalViewPS.h"
#include "TotalMainWindowPS.h"
#include "TotalEntPanelPS.h"
#include "TotalDocPSReader.h"
//#include "AdvancedControlPS.h"
#include "..\KAppTotal\DocWriter.h"
#include "..\KApp\Implant.h"
#include "..\KApp\MainWindow.h"
#include "..\KApp\EntityWidget.h"
#include "..\KTriathlonF1\AnteriorSurfaceJigs.h"
#include "..\KTotalPS\IFemurImplantPS.h"

KTotalDocPS* KTotalDocPS::m_pTotalDocPS = NULL;

KTotalDocPS::KTotalDocPS(QObject *parent)
	: CTotalDoc(parent)
{
	m_variableTableFileName = QString("variableTable_PS.txt");
	m_validationTableFileName = QString("validateTable_PS.txt");

}

KTotalDocPS::~KTotalDocPS()
{

}

void KTotalDocPS::Clear()
{
	CTotalDoc::Clear();
	m_pEntPanelPSFeatures->Clear();
}

void KTotalDocPS::AddEntity( CEntity* pEntity, bool bAddToEntPanel )
{
	CEntRole		eRole = pEntity->GetEntRole();
	int				id = pEntity->GetEntId();
	QString			sName = pEntity->GetEntName();
	bool			bOriginalAddToEntPanel = bAddToEntPanel;
	bool			bBelongToPSFeatures = false;

	if ( eRole >= ENT_ROLE_IMPLANT_PS_NONE &&
		 eRole <= ENT_ROLE_IMPLANT_PS_END )
	{
		bAddToEntPanel = false; // Femoral PS entities never been added to m_pEntPanel, but m_pEntPanelPSFeatures
		bBelongToPSFeatures = true;
	}

	CTotalDoc::AddEntity(pEntity, bAddToEntPanel);

	if( bBelongToPSFeatures && bOriginalAddToEntPanel )
	{
		m_pEntPanelPSFeatures->Add( id, sName, eRole );
	}
}

// virtual
bool KTotalDocPS::DeleteEntityById(int id, bool bDestroyEntity /* = true*/) // override
{
    if (CEntity *pEntity = GetEntityById(id))
    {
	    CEntRole eRole = pEntity->GetEntRole();
        bool bDeleteInTheEnd = bDestroyEntity; // copying the request because we'll override it for PS entities

	    if ( eRole >= ENT_ROLE_IMPLANT_PS_NONE &&
		     eRole <= ENT_ROLE_IMPLANT_PS_END )
        {
    		m_pEntPanelPSFeatures->DeleteById(id);
            bDestroyEntity = false; // if it was requested, we will do it ourselves
        }
        else
            bDeleteInTheEnd = false; // overriding a possible deletion if was requested -- it shall be done by the base class

        // need to keep it alive 
        CTotalDoc::DeleteEntityById(id, bDestroyEntity); // delete it from the base class entity list as well

        if (bDeleteInTheEnd)
            delete pEntity;

        return true;
    }
    else // not known to the base class
    {
        return false;
    }
}

bool KTotalDocPS::Save( const QString& sDocFileName, bool incrementalSave )
{
	if ( !this->IsModified() )// if never modified, skip saving. 
		return true;

    QApplication::setOverrideCursor( Qt::WaitCursor );

	CDocWriter*		pDocWriter = new CDocWriter( this, sDocFileName );

	bool			bOK = pDocWriter->SaveFile( incrementalSave, QString("PS") );

	delete pDocWriter;

	m_fileName = sDocFileName;

	m_bModified = false;

	SavePostProcessing(sDocFileName);

	QApplication::restoreOverrideCursor();

	return bOK;
}

bool KTotalDocPS::Load( const QString& sDocFileName )
{
	AppendLog( QString("CTotalDocPS::Load(). File name: " + sDocFileName ) );

	int						button;

	if( m_bModified )
	{
		button = QMessageBox::question( NULL, 
										"Save the current document?", 
										"The current file has been modified.\n"
										"You need to save it before open another file."
										"Do you want to save the current file?", 
										QMessageBox::Yes, QMessageBox::Cancel );	

		if( button == QMessageBox::Cancel )
			return false;

		if( button == QMessageBox::Yes )
		{
			GetView()->GetMainWindow()->OnFileSave();
		}
	}

	// Whenever open a file, the previous user's names are initialized.
	m_previousUserInfo.clear();

    QApplication::setOverrideCursor( Qt::WaitCursor );

	Clear();

	m_fileName = sDocFileName;

	bool previouslyIncomplete = LoadPreProcessing(sDocFileName);

	KTotalDocPSReader* pTotalDocPSReader = new KTotalDocPSReader( this, sDocFileName );

	// get the version number
	int rel, rev, ver;
	QString type;
	bool bGotVersion = pTotalDocPSReader->ReadDataFileVersionInfo(type, rel, rev, ver);
	if ( bGotVersion )
	{
		if ( type == QString("CR") )// TriathlonF1CR file
		{
			char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
			if (underDevelopment != NULL && QString("1") == underDevelopment)
			{
				button = QMessageBox::information( NULL, 
												"File Info", 
												"TriathlonF1 PS should not read TriathlonF1 (CR) data file.\nDo you still want to continue?", 
												QMessageBox::Ok, QMessageBox::No );
				if (button == QMessageBox::No) 
					return false;
			}
			else
			{
				button = QMessageBox::information( NULL, 
												"File Info", 
												"TriathlonF1 PS can not read TriathlonF1 (CR) data file.", 
												QMessageBox::Abort );
				return false;
			}
		}
	}

	// Load the file info
	bool bOK = pTotalDocPSReader->LoadHeaderFileInfo();
	// Load the CR femoral implant entities
	bOK = bOK && pTotalDocPSReader->LoadEntities(ENT_ROLE_NONE, ENT_ROLE_IMPLANT_END);
	// Load the PS femoral implant entities
	bOK = bOK && pTotalDocPSReader->LoadPSEntities(ENT_ROLE_IMPLANT_PS_NONE, ENT_ROLE_IMPLANT_PS_END);
	// Load the CR femoral jigs entities
	bOK = bOK && pTotalDocPSReader->LoadEntities(ENT_ROLE_JIGS_NONE, ENT_ROLE_JIGS_END);
	//// Load the PS femoral jigs entities
	//bOK = bOK && pTotalDocPSReader->LoadPSEntities(ENT_ROLE_JIGS_PS_NONE, ENT_ROLE_JIGS_PS_END);
	// Sepcial check for cartilage thickness
	SpecialCheckForCartilageThickness();

	delete pTotalDocPSReader;

	QApplication::restoreOverrideCursor();

	if( previouslyIncomplete )
	{
		// if previouslyIncomplete, make all design steps out of date.
		UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false, false);

		button = QMessageBox::information( NULL, 
										"File Info", 
										"File failed to be saved correctly.\n"
										"The current opened file is a backup.\n"
										"Please check any data loss.", 
										QMessageBox::Ok );	
	}

	return bOK;
}

bool KTotalDocPS::LoadWithOptions( const QString& sDocFileName, QString options)
{
	AppendLog( QString("CTotalDocPS::Load(). File name: ") + sDocFileName + QString(". options:") + options );

	int						button;

	// Whenever open a file, the previous user's names are initialized.
	m_previousUserInfo.clear();

    QApplication::setOverrideCursor( Qt::WaitCursor );

	// if load all or load fem basis, then m_pDoc should be clear.
	if ( options == "" || options == "FemBasis" )
		Clear();

	m_fileName = sDocFileName;

	bool previouslyIncomplete = LoadPreProcessing(sDocFileName);

	KTotalDocPSReader* pTotalDocPSReader = new KTotalDocPSReader( this, sDocFileName );

	// get the version number
	int rel, rev, ver;
	QString type;
	bool bGotVersion = pTotalDocPSReader->ReadDataFileVersionInfo(type, rel, rev, ver);
	if ( bGotVersion )
	{
		if ( type == QString("CR") )// TriathlonF1CR file
		{
			char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
			if (underDevelopment != NULL && QString("1") == underDevelopment)
			{
				button = QMessageBox::information( NULL, 
												"File Info", 
												"TriathlonF1 PS should not read TriathlonF1 (CR) data file.\nDo you still want to continue?", 
												QMessageBox::Ok, QMessageBox::No );
				if (button == QMessageBox::No) 
					return false;
			}
			else
			{
				button = QMessageBox::information( NULL, 
												"File Info", 
												"TriathlonF1 PS can not read TriathlonF1 (CR) data file.", 
												QMessageBox::Abort );
				return false;
			}
		}
	}

	// Load the file info
	bool bOK = pTotalDocPSReader->LoadHeaderFileInfo();

	if ( options == "" || options == "FemBasis" )
	{
		// Load the femoral basis implant entities
		bOK = bOK && pTotalDocPSReader->LoadEntities(ENT_ROLE_NONE, ENT_ROLE_IMPLANT_END);
	}
	if ( options == "" || options == "FemPS" )
	{
		// Load the PS femoral implant entities
		bOK = bOK && pTotalDocPSReader->LoadPSEntities(ENT_ROLE_IMPLANT_PS_NONE, ENT_ROLE_IMPLANT_PS_END);
	}
	if ( options == "" || options == "FemJigs" )
	{
		// Load the CR femoral jigs entities
		bOK = bOK && pTotalDocPSReader->LoadEntities(ENT_ROLE_JIGS_NONE, ENT_ROLE_JIGS_END);
		// Sepcial check for cartilage thickness
		SpecialCheckForCartilageThickness();
	}
	delete pTotalDocPSReader;

	QApplication::restoreOverrideCursor();

	if( previouslyIncomplete )
	{
		// if previouslyIncomplete, make all design steps out of date.
		UpdateEntPanelStatusMarker(ENT_ROLE_FEMORAL_AXES, false, false);

		button = QMessageBox::information( NULL, 
										"File Info", 
										"File failed to be saved correctly.\n"
										"The current opened file is a backup.\n"
										"Please check any data loss.", 
										QMessageBox::Ok );	
	}

	return bOK;
}

void KTotalDocPS::SetEntPanelStatusMarker( CEntRole eEntRole, bool status )
{
	CEntity*	pEntity;

	pEntity = GetEntity( eEntRole );

	if (pEntity == NULL ) return;

	// EntPanel "*" sign should synchronize with entity UpToDateStatus
	pEntity->SetUpToDateStatus(status);

	if ( eEntRole <= ENT_ROLE_JIGS_END )
	{
		// Call SetEntPanelStatusMarker() in CTotalDoc
		CTotalDoc::SetEntPanelStatusMarker(eEntRole, status);
	}
	else 
	{
		m_pEntPanelPSFeatures->SetModifiedMark( pEntity->GetEntId(), status );
		if ( !status )// not up-to-date
		{
			CEntValidateStatus validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
			QString validateMessage = "";
			
			pEntity->SetValidateStatus(validateStatus, validateMessage);
			m_pEntPanelPSFeatures->SetValidateStatus( pEntity->GetEntId(), validateStatus, validateMessage);
		}
	}

	m_bModified = true;

}

void KTotalDocPS::UpdateEntPanelStatusMarker( CEntRole eEntRole, bool bUpToDate, bool implicitlyUpdate )
{
	// Call CTotalDoc::UpdateEntPanelStatusMarker() to update all common CR design steps. 
	CTotalDoc::UpdateEntPanelStatusMarker(eEntRole, bUpToDate, implicitlyUpdate);
		
	GetView()->GetMainWindow()->EnableActions(true);

	GetEntPanel()->NotifyOfPSFeaturesChange();
}

///////////////////////////////////////////////////////
bool KTotalDocPS::IsFemoralSidePSFeaturesDesignComplete()
{
	bool upToDate = false;

	return upToDate;
}

int KTotalDocPS::CountEntitiesBelongToPS()
{
	int		iEnt, nEnts, idEnt;
	CEntRole	eRole;
	
	nEnts = m_EntList.count();

	int countPS = 0;

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		idEnt = pEntity->GetEntId();
		eRole = pEntity->GetEntRole();

		if( eRole > ENT_ROLE_IMPLANT_PS_NONE && // Total PS design roles
			eRole < ENT_ROLE_JIGS_PS_END )
		{
			countPS++;
		}
	}

	return countPS;
}

void KTotalDocPS::ToggleEntPanelItemByID(int id, bool On)
{
	CEntity* pEntity = GetEntityById(id);
	if ( pEntity )
	{
		if ( pEntity->GetEntRole() <= ENT_ROLE_JIGS_END )
		{
			CTotalDoc::ToggleEntPanelItemByID(id, On);
		}
		else 
		{
			m_pEntPanelPSFeatures->ToggleById(id, On);
		}
	}
}

void KTotalDocPS::RemoveEntityFromEntPanel( CEntRole eEntRole )
{
	CEntity*	pEntity = GetEntity( eEntRole );
	if( pEntity )
	{
		if ( pEntity->GetEntRole() <= ENT_ROLE_JIGS_END )
			CTotalDoc::RemoveEntityFromEntPanel(eEntRole);
		else 
		{
			m_pEntPanelPSFeatures->DeleteById( pEntity->GetEntId() );
		}
	}
}

void KTotalDocPS::SetEntPanelValidateStatus( CEntRole eEntRole, CEntValidateStatus status, QString message )
{
	CEntity*	pEntity = GetEntity( eEntRole );

	if (pEntity)
	{
		if ( eEntRole <= ENT_ROLE_JIGS_END )
			CTotalDoc::SetEntPanelValidateStatus(eEntRole, status, message);
		else 
		{
			m_pEntPanelPSFeatures->SetValidateStatus( pEntity->GetEntId(), status, message );
		}
	}

}

void KTotalDocPS::SetEntPanelFontColor(CEntRole entRole, CColor color)
{
	CEntity*	pEntity = GetEntity( entRole );

	if (pEntity)
	{
		if ( entRole <= ENT_ROLE_JIGS_END )
			CTotalDoc::SetEntPanelFontColor(entRole, color);
		else 
		{
			m_pEntPanelPSFeatures->SetFontColor( pEntity->GetEntId(), color );
		}
	}

}

void KTotalDocPS::SetEntPanelToolTip(CEntRole entRole, QString message)
{
	CEntity*	pEntity = GetEntity( entRole );

	if (pEntity)
	{
		if ( entRole <= ENT_ROLE_JIGS_END )
			CTotalDoc::SetEntPanelToolTip(entRole, message);
		else 
		{
			m_pEntPanelPSFeatures->SetToolTip( pEntity->GetEntId(), message );
		}
	}
}

bool KTotalDocPS::GetMotionPatternsExtendToolbar()
{
	return IFemurImplantPS::MotionPatternsExtendToolbar();
}

void KTotalDocPS::DeletePSEntityById( int id, bool bDestroyEntity )
{
	int		iEnt, nEnts, idEnt;
	CEntRole	eRole;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];
		idEnt = pEntity->GetEntId();
		eRole = pEntity->GetEntRole();

		if( eRole >= ENT_ROLE_IMPLANT_PS_NONE && // femoral PS features design roles, in case
			eRole <= ENT_ROLE_IMPLANT_PS_END )
		{
			if( idEnt == id )
			{
				m_EntList.removeAt( iEnt );
			
				if( bDestroyEntity )
					delete pEntity;
			
				iEnt--;
				nEnts--;

				break; // id is unique. Just break out when found it
			}
		}
	}

	m_pEntPanelPSFeatures->DeleteById( id );

	m_bModified = true;
}

void KTotalDocPS::DeleteEntitiesBelongToPSFeatures()
{
	// Delete entities belong to KTotalPS
	int		iEnt, nEnts, idEnt;
	CEntRole	eRole;
	
	nEnts = m_EntList.count();

	for( iEnt = 0; iEnt < nEnts; iEnt++ )
	{
		CEntity*		pEntity = m_EntList[iEnt];

		idEnt = pEntity->GetEntId();
		eRole = pEntity->GetEntRole();

		if( eRole >= ENT_ROLE_IMPLANT_PS_NONE && // femur PS Feature roles
			eRole <= ENT_ROLE_IMPLANT_PS_END )
		{
			m_EntList.removeAt( iEnt );
			
			delete pEntity;
			
			iEnt--;
			nEnts--;

			m_pEntPanelPSFeatures->DeleteById( idEnt );
		}
	}

	m_pEntPanelPSFeatures->Clear();
	if ( entityWidget->IsItemExist(m_pEntPanelPSFeatures) )
		entityWidget->RemoveItem( m_pEntPanelPSFeatures );

	m_bModified = true;
}

void KTotalDocPS::InitializeValidationReportPS()
{
	AppendLog( QString("KTotalDocPS::InitializeValidationReportPS()") );

	// clear all messages
	m_validationReportPS.clear();
	m_validationPSAllGoodResults = true;
	
	// Title
	QString title = QString("TriathlonF1 PS Feature Design E-DHR");
	AppendValidationReportPSEntry(title);

	// Version info
	QString CRPS;
	if (this->isPosteriorStabilized())
		CRPS = QString("PS");
	else
		CRPS = QString("CR");
	bool bBeta = !this->isProductionSoftware();
	QString appVersion = m_pView->GetMainWindow()->GetAppVersion();
	QString msg;
	if (bBeta)
		msg = QString("Software: TriathlonF1 %1 beta version %2").arg(CRPS).arg(appVersion);
	else
		msg = QString("Software: TriathlonF1 %1 production version %2").arg(CRPS).arg(appVersion);

	AppendValidationReportPSEntry(msg);

	// Date
	QString			sDate = QString("Time: ") + QDateTime::currentDateTime().toString();
	AppendValidationReportPSEntry(sDate);

	// User info
	QString			userInfo = QString( "User: %1").arg( GetCurrentUserName());
	AppendValidationReportPSEntry(userInfo);

	// Workstation
	QString			workstation = QString( "Workstation: %1").arg( GetCurrentComputerName());
	AppendValidationReportPSEntry(workstation);

	// Patient ID
	QString patientID = this->GetPatientId();
	AppendValidationReportPSEntry("Patient ID: " + patientID);

	// All pass or not
    if (auto pTE = GetView()->GetValidationReportPSTextEdit())
    {
        pTE->setTextColor(QColor(255, 0, 0));
	    AppendValidationReportPSEntry("**** NOT ALL PASS ****");
	    pTE->setTextColor(QColor(0, 0, 0));
    }
}

void KTotalDocPS::AppendValidationReportPSEntry(QString const& entry, bool goodResult)
{
	QString str = entry;
	
	if ( entry == QString("\n") )
		str = QString("");

	if ( !goodResult )
	{
		m_validationPSAllGoodResults = false;
		m_validationReportPS.append(str+QString("\n"));
		if ( GetView()->GetValidationReportPSTextEdit() )
		{
			GetView()->GetValidationReportPSTextEdit()->setTextColor(QColor(255, 0, 0));
			GetView()->GetValidationReportPSTextEdit()->append(str);
			GetView()->GetValidationReportPSTextEdit()->setTextColor(QColor(0, 0, 0));
		}
	}
	else
	{
		m_validationReportPS.append(str+QString("\n"));
		if ( GetView()->GetValidationReportPSTextEdit() )
			GetView()->GetValidationReportPSTextEdit()->append(str);
	}
}

QString KTotalDocPS::GetValidationReportPS(bool& allGoodResults)
{
	allGoodResults=m_validationPSAllGoodResults;
	return m_validationReportPS;
}
