#pragma once

#include <windows.h>
#include <QString>

// Use the Windows API LockFileEx to create a single lock file so when
// multiple users access the same implant, a warning message can be provided.
class KLockSingleFile
{
public:
    KLockSingleFile(bool removeWhenRelease = true);
    ~KLockSingleFile();

    // Generate a lock file of the specified path. The content of the file is the user name.
    // If previously a lock file is generated at a different path, the previous lock is released
    // after the current lock is generated successfully. 
    bool Create(QString path);
    // Release the lock
    bool Release();

private:
    bool remove;
    HANDLE handle;
    QString filePath;
};
