#pragma once

#include <QtGui>
#include "Basics.h"
#include "Entity.h"
#pragma warning( disable : 4996 4805 )
#include "IwContext.h"
#include "IwBrep.h"
#pragma warning( default : 4996 4805 )
#include "Globals.h"

#pragma warning(disable: 4244)

class CDataDoc;

enum CPartType
	{
	PART_TYPE_NONE,
	PART_TYPE_STL,
	PART_TYPE_BREP
	};


struct CFacet
	{
	IwPoint3d	points[3];
	IwVector3d	normals[3];
	};


struct CSTLPoint
	{
	float		x;
	float		y;
	float		z;
	};


#pragma pack(2)
struct CSTLFacet
	{
	CSTLPoint	norm;		// Explicit normal of the facet
	CSTLPoint	pt[3];		// Three vertices of the facet
	char		dummy[2];	// Two generally unused bytes kept with every facet in STL file
	};
#pragma pack()




class CPart : public CEntity
	{
public:		
					CPart(	CDataDoc*		pDoc, 
							CEntRole		eEntRole = ENT_ROLE_NONE, 
							const QString&	sFileName = "", 
							int				nEntId = -1 );

					CPart( const CPart* pPart );
					
					virtual ~CPart();

	void			Init();	

	bool			ImportPart( const QString& sFileName );
	bool			LoadSTL( const QString& sFileName );	
	bool			LoadIGES( const QString& sFileName );	
	bool			LoadXML( const QString& sFileName );	
	bool			ImportBrep( const QString& sFileName );

	void			SetPartType( CPartType ePartType );
	CPartType		GetPartType();

	virtual void	SetIwBrep( IwBrep* pBrep );
	IwBrep*			GetIwBrep();
	void			GetPoints(IwTArray<IwPoint3d>& points);
	virtual void					SetIwCurves( IwTArray<IwCurve*> const& curves );
	IwTArray<IwCurve*>		GetIwCurves();
	virtual void					SetIwPoints( IwTArray<IwPoint3d> const& points );
	IwTArray<IwPoint3d>		GetIwPoints();
	void					GetIwData(IwBrep*& pBrep, IwTArray<IwCurve*>& curves, IwTArray<IwPoint3d>& points );

	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );
	virtual void	Display();
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);

	void			GenerateIsoCurvesOnlyDisplayList();
	void			InvalidateEdges();

	CBounds3d		GetBounds();
	virtual CBounds3d ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds = NULL );
	void			UpdateBounds( CBounds3d& Bounds );
	void			UpdateBounds( IwPoint3d& pt );
	void			ResetBounds() {m_Bounds.Reset();};

	void			AddFacet( CFacet& Facet );

	int				GetNumFacets();
	void			GetFacetPoints( int iFacet, IwPoint3d pts[3] );

	void			MakeTess(bool updateView = true);
	void			ClearTess();

	void			TestFillet();

	bool			IsModified();

	bool			ExportSTL( const QString& sFileName );
	void			ExportIGES( const QString& sFileName );

	void			SetModifiedFlag( bool bModified );

	long			GetFaceDisplayList() {return m_glFaceList;};
	long			GetEdgeDisplayList() {return m_glEdgeList;};
	long			GetIsoCurveDisplayList() {return m_glIsoCurveList;};

	QList<CFacet>	GetFacets() {return m_Facets;};

private:
	void			DisplayFaces();
	void			DisplayEdges();
	void			DisplayIsoCurves(bool useNormal=false);
	void			DisplayCurves();
	void			DisplayPoints();

protected:

	bool			WriteTess( const QString& sFileName );
	bool			ReadTess( const QString& sFileName );

	int				NewDisplayList();
	void			DeleteDisplayList( long& glList );

	CPartType				m_ePartType;

	// Main data
	IwBrep*					m_pBrep;
	// But the "Part" geometry may degenerate into curves or may need to contain curves
	IwTArray<IwCurve*>		m_curves;
	// Or degenerate into points or contain points
	IwTArray<IwPoint3d>		m_points;

	QList< CFacet >			m_Facets;

	CBounds3d				m_Bounds;

	long					m_glFaceList;
	long					m_glEdgeList;
    long					m_glIsoCurveList;
	long					m_glIsoCurveListWithNormal;

	bool					m_bModified;// Parameters being modified, but geometry has not been updated yet to the screen. See FemoralCuts as an example.
	bool					m_bDataModified;// Parameters being modified, but have not been saved yet to the file. 
	};

