#include "OutlineProfileMgrUI3D.h"
#include "OutlineProfile.h"
#include "FemoralAxes.h"
#include "AdvancedControl.h"
//#include "SolidImplant.h"
#include "FemoralCuts.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "OutlineProfileMgrModel.h"
#include "UndoRedoCmd.h"
#include "..\KApp\SwitchBoard.h"

#include <QApplication>

OutlineProfileMgrUI3D::OutlineProfileMgrUI3D(CTotalDoc* doc, CManagerActivateType manActType, CManager *owner, OutlineProfileMgrModel *model)
	: OutlineProfileMgrUIBase(doc, manActType, owner, model),
	m_bMouseMiddleDown(false)
{
	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT || manActType == MAN_ACT_TYPE_REVIEW );

	//////////////////////////////////////////////////////////////////////////////////
	///////////////////ACTIONS taken in the base class
	//Before any computation can happen there needs to be an updated SketchSurface
	//That update happens in the base class
	//
	//If the activation type is INITIALIZE
	//mgrModel (aka the geometry to work on) is initialized in the base class
	//
	//If the activation type is RESET or outlineProfile object does exist
	//	then the outlineProfile Entity is created and the reset action 
	//	happens in the base class
	////ATTENTION: If the actions in the base class change, please update this comment
	//////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////
	//After the base class prepares the data
	// the following happens
	m_predefinedViewIndex = 0;
	// Get the runtime data
	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();

	if ( outlineProfile == NULL || // if no object
		 manActType == MAN_ACT_TYPE_RESET )
	{
		// Had been done in Base class. Do nothing here.
	}
	else 
	{
		// convert feature points from UV domain to xyz space
		SetMouseMoveSearchingList();

		IwBSplineSurface *sketchSurf = NULL;
		outlineProfile->GetSketchSurface(sketchSurf);
		CalculateSketchSurfaceNormalInfo(sketchSurf);

		if ( manActType == MAN_ACT_TYPE_REFINE ||
			 manActType == MAN_ACT_TYPE_REFINE_CONVERGE )
		{
			Refine();
			// Note: OutlineProfileMgrUIBase::Refine() only executes m_owner->GetUndoStack()->push() if activateUI=true (i.e., not REFINE mode)
			// Thus, no way to initialize validateStatus to VALIDATE_STATUS_NOT_VALIDATE. We need to initialize here.
			// Need to fix later.
			m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
			m_validateMessage = QString("Not validate.");
			((CDefineOutlineProfile*)m_owner)->SetValidateIcon(m_validateStatus, m_validateMessage);
			m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_OUTLINE_PROFILE, m_validateStatus, m_validateMessage);
		}
		if ( manActType == MAN_ACT_TYPE_VALIDATE )
		{
			OnValidate();
		}
		else if ( (manActType == MAN_ACT_TYPE_EDIT || manActType == MAN_ACT_TYPE_REVIEW || manActType == MAN_ACT_TYPE_REGENERATE) &&
			       !outlineProfile->GetUpToDateStatus() )
		{
			OnValidate();
		}
	}

	// Display the viewing normal region
	if ( activateUI )
	{
		DetermineViewingNormalRegion();
		DetermineNotchEighteenWidthPoints();
		DetermineAnteriorPosteriorCutNormal();
		outlineProfile->SetDisplayOutlineProfileBoth(true);
	}
	// Display m_criticalTipThicknessPoints
	if (manActType == MAN_ACT_TYPE_EDIT)
	{
		DetermineCriticalTipThicknessPoints();
		DetermineAnteriorAirBallDepthReferencePlaneAndMedialReferencePoint();
	}

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

}

OutlineProfileMgrUI3D::~OutlineProfileMgrUI3D()
{
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();

	if (outlineProfile)
	{
		if ( m_oldOutlineProfileExist )
			outlineProfile->SetDisplayOutlineProfileRuler(false);

		outlineProfile->SetDisplayOutlineProfileBoth(false);
	}

	SwitchBoard::removeSignalSender(this);
}

void OutlineProfileMgrUI3D::SetMouseMoveSearchingList()
{
	IwTArray<IwPoint3d> xyzPoints;
	m_pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(const_cast<IwTArray<IwVector3d>&>(m_mgrModel->FeaturePoints()), xyzPoints);
	xyzPoints.RemoveLast();// Note, the last one is never selectable.
	m_owner->SetMouseMoveSearchingList(xyzPoints);
}

bool OutlineProfileMgrUI3D::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_bMouseMiddleDown = true;
	m_owner->GetView()->Redraw();

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// This function here is only for redraw (update screen) purpose
bool OutlineProfileMgrUI3D::keyPressEvent( QKeyEvent* event, Viewport* vp )
{
	bool continueResponse = true;

	switch (event->key())
	{
		case Qt::Key_Up:
		case Qt::Key_Down:
			{
			}
			break;

		case Qt::Key_Left:
		case Qt::Key_Right:
			{
			}
			break;

		default:
			break;

	}

	return continueResponse;
}

bool OutlineProfileMgrUI3D::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	bool continuousResponse = OutlineProfileMgrUIBase::MouseMove(cursor, keyModifier, vp);

	if (!m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		// If there is any caught point, we need to check whether it is within viewing normal region.
		if ( m_owner->GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			IwPoint3d featUVPnt = m_mgrModel->FeaturePoints().GetAt(caughtIndex);
			IwPoint3d xyzPnt;
			IwVector3d normal;
			m_pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(featUVPnt, xyzPnt, normal);
			IwVector3d viewVec = m_owner->GetView()->GetViewingVector();
			if ( viewVec.Dot(normal) > -0.93969262) // > 20 degrees
				m_owner->RemoveMouseMoveCaughtPoint();
		}
	}
    
	m_owner->GetView()->Redraw();

	return continuousResponse;
}

bool OutlineProfileMgrUI3D::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // Remember if under review, m_leftButtonDown is always false.
	{
		// if the clicked-down is the same as the button-up point, 
		// change to the next review predefined view
		if (m_owner->GetPositionStart() == cursor)
			OnReviewPredefinedView();
	}
	else if ( m_leftButtonDown) 
	{
        Qt::KeyboardModifiers keyModifier = QApplication::keyboardModifiers();
		IwPoint3d caughtPoint;
		int caughtIndex;
		bool gotCaughtPoint = m_owner->GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);
		COutlineProfile*		outlineProfile = m_pDoc->GetOutlineProfile(); 
		bool gotSketchSurf=false;
		IwBSplineSurface* skchSurf=NULL;
		if (outlineProfile)
			gotSketchSurf = outlineProfile->GetSketchSurface(skchSurf);

		//// Update the mouse move caught point /////////////////
		if ( m_addDeleteMode == 0 && gotCaughtPoint)
		{
			m_pDoc->AppendLog( QString("OutlineProfileMgrUI3D::MouseUp(), m_addDeleteMode = move, caughtIndex: %1, caughtPoint: %2 %3 %4").arg(caughtIndex).arg(caughtPoint.x).arg(caughtPoint.y).arg(caughtPoint.z) );

			// if the clicked-down is the same as the button-up point, do nothing
			// **** NOTE **** DO NOT CHANGE THIS CONDITION UNLESS YOU SEE THE MouseDoubleClickLeft().
			if (m_owner->GetPositionStart() == cursor)
			{
				m_leftButtonDown = false;
				return false;
			}

			if (gotSketchSurf)
			{
				IwVector3d viewVec = m_owner->GetView()->GetViewingVector();
				IwPoint3d backWardPnt = caughtPoint - viewVec; // move the caught point closer to the viewer, in case no intersection with the sketch surface
				IwPoint3d interPnt;
				double uvPnt[2];
				if (IntersectSurfaceByLine((IwSurface*)skchSurf, backWardPnt, viewVec, interPnt, uvPnt))
					MovePoint(caughtIndex, IwVector2d(uvPnt[0], uvPnt[1]));
			}
		}
		//// Update the adding point /////////////////
		else if (m_addDeleteMode == 1)
		{
			m_pDoc->AppendLog( QString("OutlineProfileMgrUI3D::MouseUp(), m_addDeleteMode = add, cursor: %1 %2").arg(cursor.x()).arg(cursor.y()) );

			//
			IwPoint3d curPnt;
			m_owner->GetView()->UVtoXYZ(cursor, curPnt);
			IwVector3d viewVec = m_owner->GetView()->GetViewingVector();
			IwPoint3d backWardPnt = curPnt - 200*viewVec; // move the caught point close to the viewer, in case no intersection with the sketch surface
			IwPoint3d interPnt;
			double uvPnt[2];
			if (IntersectSurfaceByLine((IwSurface*)skchSurf, backWardPnt, viewVec, interPnt, uvPnt))
				AddPoint(IwVector2d(uvPnt[0], uvPnt[1]));
		}
		//// Update the deleting point /////////////////
		else if (m_addDeleteMode == 2)
		{
			m_pDoc->AppendLog( QString("OutlineProfileMgrUI3D::MouseUp(), m_addDeleteMode = delete, cursor: %1 %2").arg(cursor.x()).arg(cursor.y()) );

			DeletePoint(caughtIndex);
		}
	}

	m_bMouseMiddleDown = false;

	return OutlineProfileMgrUIBase::MouseUp(cursor, vp);
}

void OutlineProfileMgrUI3D::Display(Viewport* vp)
{
	OutlineProfileMgrUIBase::Display(vp);

	glDisable( GL_LIGHTING );

	IwVector3d viewVec = m_owner->GetView()->GetViewingVector();

	if ( !viewVec.IsParallelTo(m_viewingNormal, 1.0) )
		DetermineViewingNormalRegion();

	bool bHighLightViewingNormalRegion = viewVec.IsParallelTo(m_anteriorCutNormal, 2.5) || viewVec.IsParallelTo(m_posteriorCutNormal, 2.5);
	DisplayViewingNormalRegion(bHighLightViewingNormalRegion);
	DisplayCriticalTipThicknessPoints(-5*viewVec);

	if (m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( m_owner->GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			glPointSize(7);
			glColor3ub( 255, 255, 255 );

			m_owner->DisplayCaughtPoint(-50*viewVec);

			glPointSize(1);
		}
	}
	else
	{
		glPointSize(6);
		glColor3ub( 255, 255, 0 );

		m_owner->DisplayCaughtPoint(-50*viewVec);

		glPointSize(1);

		// For debug purpose
		bool underDev = false;
		char *underDevelopment = getenv("ITOTAL_UNDERDEVELOPMENT");
		if (underDevelopment != NULL && QString("1") == underDevelopment)
		{
			IwPoint3d caughtPoint;
			int caughtIndex;
			bool gotCaughtPoint = m_owner->GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);
			if (gotCaughtPoint)
			{
				glColor3ub( 0, 0, 0 );
				// Get remap info from Model class, then display to developer.
				double edgeInfo = m_mgrModel->GetControlPointReferenceEdgeInfo(caughtIndex);
				QFont font( "Tahoma", 12);
				font.setBold(true);
				QString str;
				str.setNum(edgeInfo, 'f', 4);
				QString totalStr = QString("  Edge info: ") + str;
				m_owner->GetViewWidget()->renderText(caughtPoint.x, caughtPoint.y, caughtPoint.z, totalStr, font);
			}
		}
	}

	if (viewVec.IsParallelTo(m_anteriorCutNormal, 9.0))// When viewing normal to m_anteriorCutNormal
		DisplayAnteriorAirBallDepthReferencePlane();

	glEnable( GL_LIGHTING );
}

void OutlineProfileMgrUI3D::DisplayViewingNormalRegion(bool bHighLight)
{

	// Display viewing normal region
	glLineWidth( 2.0 );
	if ( bHighLight )
		glLineWidth( 4.0 );

	glColor3d( 1.0, 1.0, 0.0 );
	if (m_viewingNormalRegion.GetSize() > 0)
	{
		glBegin( GL_LINE_LOOP );
			glVertex3d(m_viewingNormalRegion.GetAt(0).x, m_viewingNormalRegion.GetAt(0).y, m_viewingNormalRegion.GetAt(0).z);
			glVertex3d(m_viewingNormalRegion.GetAt(1).x, m_viewingNormalRegion.GetAt(1).y, m_viewingNormalRegion.GetAt(1).z);
			glVertex3d(m_viewingNormalRegion.GetAt(2).x, m_viewingNormalRegion.GetAt(2).y, m_viewingNormalRegion.GetAt(2).z);
			glVertex3d(m_viewingNormalRegion.GetAt(3).x, m_viewingNormalRegion.GetAt(3).y, m_viewingNormalRegion.GetAt(3).z);
		glEnd();
	}
	// Display viewing normal region
	glLineWidth( 1.0 );
	glColor3d( 1.0, 0.75, 0.0 );
	if (m_viewingNormalRegion.GetSize() > 0)
	{
		glBegin( GL_LINE_LOOP );
			glVertex3d(m_manipulatingNormalRegion.GetAt(0).x, m_manipulatingNormalRegion.GetAt(0).y, m_manipulatingNormalRegion.GetAt(0).z);
			glVertex3d(m_manipulatingNormalRegion.GetAt(1).x, m_manipulatingNormalRegion.GetAt(1).y, m_manipulatingNormalRegion.GetAt(1).z);
			glVertex3d(m_manipulatingNormalRegion.GetAt(2).x, m_manipulatingNormalRegion.GetAt(2).y, m_manipulatingNormalRegion.GetAt(2).z);
			glVertex3d(m_manipulatingNormalRegion.GetAt(3).x, m_manipulatingNormalRegion.GetAt(3).y, m_manipulatingNormalRegion.GetAt(3).z);
		glEnd();
	}
}

void OutlineProfileMgrUI3D::DisplayCriticalTipThicknessPoints(IwVector3d offsetVector)
{
	IwPoint3d pnt;
	glPointSize(15);
	glColor3ub( 255, 0, 0 );
	for (unsigned i=0; i<m_criticalTipThicknessPoints.GetSize(); i++)
	{
		glBegin( GL_POINTS );
			pnt = m_criticalTipThicknessPoints.GetAt(i) + offsetVector;
			glVertex3d(pnt.x, pnt.y, pnt.z);
		glEnd();
	}
}

void OutlineProfileMgrUI3D::Reset()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUI3D::Reset()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	OutlineProfileMgrUIBase::Reset();

	// update search list
	IwTArray<IwPoint3d> xyzPoints;
	m_pDoc->GetOutlineProfile()->ConvertSketchSurfaceUVtoXYZ(const_cast<IwTArray<IwVector3d>&>(m_mgrModel->FeaturePoints()), xyzPoints);
	xyzPoints.RemoveLast();// Note, the last one is never selectable.
	m_owner->SetMouseMoveSearchingList(xyzPoints);

	QApplication::restoreOverrideCursor();
}

void OutlineProfileMgrUI3D::OnDispRuler()
{
	m_pDoc->AppendLog( QString("OutlineProfileMgrUI3D::OnDispRuler()") );

	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();

	OutlineProfileMgrUIBase::OnDispRuler();

	if(outlineProfile && m_displayRuler)
		outlineProfile->DetermineRulerCurves();

	m_owner->GetView()->Redraw();
}

void OutlineProfileMgrUI3D::SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData)
{
	OutlineProfileMgrUIBase::SetDefineOutlineProfileUndoData(undoData);

	// Also set the m_currFeaturePoints to the SetMouseMoveSearchingList() for mouse searching;
	// convert feature points from UV domain to xyz space
	COutlineProfile*		outlineProfile = m_pDoc->GetOutlineProfile();
	if (outlineProfile)
	{
		// We need to call SetOutlineProfileFeaturePoints() and GetOutlineProfileFeaturePoints()
		// such that all the m_currFeaturePoints are in the correct positions, especially for
		// the arc end points.
		//The SetOutlineProfileFeaturePoints()
		//Happens in the base class
		m_mgrModel->GetOutlineProfileFeaturePoints();
		IwTArray<IwPoint3d> xyzPoints;
		outlineProfile->ConvertSketchSurfaceUVtoXYZ(const_cast<IwTArray<IwVector3d>&>(m_mgrModel->FeaturePoints()), xyzPoints);
		xyzPoints.RemoveLast();// Note, the last one is never selectable.
		m_owner->SetMouseMoveSearchingList(xyzPoints);
		//update AirBallReference
		DetermineAnteriorAirBallDepthReferencePlaneAndMedialReferencePoint();  
	}
	m_owner->GetView()->Redraw();
}

void OutlineProfileMgrUI3D::CalculateSketchSurfaceNormalInfo(IwBSplineSurface*& skchSurface)
{
	if (skchSurface == NULL) return;
	// the sketch surface normal and points are repeatedly referred
	// when determine the viewing normal region. 
	// Here we need to pre-determine the surface normal and points
	// to save CPU time.
	m_outlineSurfaceNormal.RemoveAll();
	m_outlineSurfacePointsMinU.RemoveAll();
	m_outlineSurfacePointsMaxU.RemoveAll();
	IwExtent2d surfDomain = skchSurface->GetNaturalUVDomain();
	int nNum = 400, nNumPlusOne = nNum+1;
	double minV = surfDomain.GetMin().y;
	double maxV = surfDomain.GetMax().y;
	double deltaT = (maxV - minV)/nNum;
	double tV = minV;
	double tMidU = 0.5*(surfDomain.GetMin().x+surfDomain.GetMax().x);
	double tMinU = surfDomain.GetMin().x;
	double tMaxU = surfDomain.GetMax().x;
	IwPoint3d pointMinU, pointMaxU;
	IwVector3d normal;
	for (int i=0; i<nNumPlusOne; i++)
	{
		skchSurface->EvaluateNormal(IwVector2d(tMidU, tV), FALSE, FALSE, normal);
		normal.Unitize();
		m_outlineSurfaceNormal.Add(normal);
		skchSurface->EvaluatePoint(IwVector2d(tMinU, tV), pointMinU);
		m_outlineSurfacePointsMinU.Add(pointMinU);
		skchSurface->EvaluatePoint(IwVector2d(tMaxU, tV), pointMaxU);
		m_outlineSurfacePointsMaxU.Add(pointMaxU);
		tV += deltaT;
	}
}

void OutlineProfileMgrUI3D::DetermineViewingNormalRegion()
{
	m_viewingNormalRegion.RemoveAll();
	m_manipulatingNormalRegion.RemoveAll();

	IwVector3d towardEyesVector = -m_owner->GetView()->GetViewingVector();
	towardEyesVector.Unitize();
	m_viewingNormal = towardEyesVector;

	double dot;
	int firstPos = -1;
	int lastPos = -1;
	for (unsigned i=0; i<m_outlineSurfaceNormal.GetSize(); i++)
	{
		dot = towardEyesVector.Dot(m_outlineSurfaceNormal.GetAt(i));
		if (dot > 0.994522) //6 degrees
		{
			if (firstPos == -1)
				firstPos = i;
			lastPos = i;
		}
	}

	if (firstPos < lastPos)
	{
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(firstPos));
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(firstPos));
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(lastPos));// "Max" follows the direction
		m_viewingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(lastPos));// "Min"
	}

	firstPos = -1;
	lastPos = -1;
	for (unsigned i=0; i<m_outlineSurfaceNormal.GetSize(); i++)
	{
		dot = towardEyesVector.Dot(m_outlineSurfaceNormal.GetAt(i));
		if (dot > 0.93969262) //20 degrees
		{
			if (firstPos == -1)
				firstPos = i;
			lastPos = i;
		}
	}

	if (firstPos < lastPos)
	{
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(firstPos));
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(firstPos));
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMaxU.GetAt(lastPos));// "Max" follows the direction
		m_manipulatingNormalRegion.Add(m_outlineSurfacePointsMinU.GetAt(lastPos));// "Min"
	}
}

void OutlineProfileMgrUI3D::UpdateSketchSurface()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	OutlineProfileMgrUIBase::UpdateSketchSurface();

	IwBSplineSurface* skchSurf=NULL;
	COutlineProfile* outlineProfile = m_pDoc->GetOutlineProfile();
	if ( outlineProfile != NULL && outlineProfile->GetSketchSurface(skchSurf))
		CalculateSketchSurfaceNormalInfo(skchSurf);

	QApplication::restoreOverrideCursor();
}

void OutlineProfileMgrUI3D::OnReviewPredefinedView()
{
	Viewport* vp = m_owner->GetView()->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 2;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0) // anterior view
	{
		// display cut femur
		CEntity *pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);	
		}
		
		vp->FrontView(false);
	}
	else if ( viewIndex == 1) //
	{
		// display cut femur
		CEntity *pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);	
		}
		
		vp->FrontView(false);
	}

	m_predefinedViewIndex++;
}

void OutlineProfileMgrUI3D::DetermineCriticalTipThicknessPoints()
{
	return;
}

////////////////////////////////////////////////////////////////////
// To determine the anterior cut normal and posterior cut normal 
// to highlight the viewNormalRegion.
void OutlineProfileMgrUI3D::DetermineAnteriorPosteriorCutNormal()
{
	// Get femoral cuts information
	CFemoralCuts*		femoralCuts = m_pDoc->GetFemoralCuts();
	if (femoralCuts == NULL) return;

	IwTArray<IwPoint3d> cutPoints, cutOrientations;
	femoralCuts->GetCutsInfo(cutPoints, cutOrientations);

	if ( cutPoints.GetSize() == 0 )
		return;

	m_anteriorCutNormal = cutOrientations.GetAt(9);
	m_posteriorCutNormal = cutOrientations.GetAt(0);

	return;
}

void OutlineProfileMgrUI3D::DetermineAnteriorAirBallDepthReferencePlaneAndMedialReferencePoint()
{
	m_antAirBallReferencePlaneCorners.RemoveAll();
	m_antMedialProfileReferencePoints.RemoveAll();

	double airBallDistance = m_pDoc->GetAdvancedControl()->GetOutlineProfileAntAirBallDistance();

	CFemoralAxes* pFemoralAxes = m_pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return;

	//// Ant cut plane ////////////////////////////////////////
	IwTArray<IwPoint3d> antCutPlaneCorners;
	pFemoralAxes->GetCutPlaneCorners(antCutPlaneCorners);
	if ( antCutPlaneCorners.GetSize() < 5 )
		return;

	IwPoint3d pnt;
	IwVector3d normal = antCutPlaneCorners.GetAt(4); // last one is the normal
	normal.Unitize();
	if ( airBallDistance > 0 )
	{
		for (int i=0; i<4; i++) 
		{
			pnt = antCutPlaneCorners.GetAt(i) + airBallDistance*normal;
			m_antAirBallReferencePlaneCorners.Add(pnt);
		}
		// for normal
		m_antAirBallReferencePlaneCorners.Add(normal);
	}

	//
	IwPoint3d refPnt;
	double refRadius;
	DetermineAntMedialProfileReferencePoint(m_pDoc, refPnt, refRadius, &m_antMedialProfileReferencePoints);

}

void OutlineProfileMgrUI3D::DetermineAntMedialProfileReferencePoint
(
	CTotalDoc* pDoc,	// I:
	IwPoint3d& refPnt,	// O:
	double& refRadius,	// O:
	IwTArray<IwPoint3d>* refCirclePnts	// O: optional
)
{
	if (refCirclePnts)
		refCirclePnts->RemoveAll();
	CFemoralAxes* pFemoralAxes = pDoc->GetFemoralAxes();
	if ( pFemoralAxes == NULL )
		return;

	CFemoralCuts*		femoralCuts = pDoc->GetFemoralCuts();
	if (femoralCuts == NULL) return;
	
	COutlineProfile* pOutlineProfile = pDoc->GetOutlineProfile();
	if ( pOutlineProfile == NULL )
		return;

	//// Ant cut plane ////////////////////////////////////////
	IwTArray<IwPoint3d> antCutPlaneCorners;
	pFemoralAxes->GetCutPlaneCorners(antCutPlaneCorners);
	if ( antCutPlaneCorners.GetSize() < 5 )
		return;

	IwVector3d normal = antCutPlaneCorners.GetAt(4); // last one is the normal
	normal.Unitize();

	IwTArray<IwPoint3d> cutFeaturePoints;
	femoralCuts->GetFeaturePoints(cutFeaturePoints, NULL);

	IwAxis2Placement wlsAxis;
	pFemoralAxes->GetWhiteSideLineAxes(wlsAxis);
	double refSizeRatio = pFemoralAxes->GetEpiCondyleSize()/76.0;

	// determine the outline curve
	IwBSplineCurve *crvOnSurf, *uvCurve;
	pDoc->GetOutlineProfile()->GetCurveOnSketchSurface(crvOnSurf, uvCurve, false);
	IwVector3d towardAnterior = -wlsAxis.GetXAxis()*normal;
	IwPoint3d farAntPoint = antCutPlaneCorners.GetAt(0) + 10000*towardAnterior;
	IwPoint3d lateralPeak;
	double param;
	DistFromPointToCurve(farAntPoint, crvOnSurf, lateralPeak, param);
	double length;
	IwPoint3d medialAntCutPnt, lateralAntCutPnt;
	if ( pDoc->IsPositiveSideLateral() )
	{
		length = -25.0*refSizeRatio;
		lateralAntCutPnt = cutFeaturePoints.GetAt(18);
		medialAntCutPnt = cutFeaturePoints.GetAt(19);
	}
	else
	{
		length = 25.0*refSizeRatio;
		lateralAntCutPnt = cutFeaturePoints.GetAt(19);
		medialAntCutPnt = cutFeaturePoints.GetAt(18);
	}
	// Determine reference center point
	refRadius = (7.0 - 2.0)/2.0;
	double refAPDist = 2.0 + refRadius; // 4.5 from lateralPeak toward reference center point
	IwPoint3d refMLPnt = 0.70*medialAntCutPnt + 0.30*lateralAntCutPnt;
	refPnt = lateralPeak - refAPDist*towardAnterior;
	refPnt = refPnt.ProjectPointToPlane(refMLPnt, wlsAxis.GetXAxis());

	// Create a circle
	if ( refCirclePnts )
	{
		IwCircle *refCircle = NULL;
		IwAxis2Placement cirAxis = IwAxis2Placement(refPnt, wlsAxis.GetXAxis(), towardAnterior);
		IwCircle::CreateCanonical(pDoc->GetIwContext(), cirAxis, refRadius, refCircle);
		if (refCircle)
		{
			IwExtent1d dom = refCircle->GetNaturalInterval();
			refCircle->EquallySpacedPoints(dom.GetMin(), dom.GetMax(), 25, 0.01, refCirclePnts, NULL);
			IwObjDelete(refCircle);
		}
	}
}

void OutlineProfileMgrUI3D::DisplayAnteriorAirBallDepthReferencePlane()
{
	if ( !COutlineProfile::IsAsymmetricEarShaped(m_pDoc) )// No need to display if symmetricEarShaped.
		return;

	glDisable( GL_LIGHTING );
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	// Display reference lines ////////////
	glColor3ub( 172, 0, 255 );
	unsigned nSize = m_antMedialProfileReferencePoints.GetSize();
	glLineWidth( 2.0 );
	glBegin( GL_LINE_LOOP );
	for (unsigned i=0; i<nSize; i++)
	{
		glVertex3d( m_antMedialProfileReferencePoints.GetAt(i).x, m_antMedialProfileReferencePoints.GetAt(i).y, m_antMedialProfileReferencePoints.GetAt(i).z );
	}
	glEnd();

	// Display air ball reference plane
	if ( m_antAirBallReferencePlaneCorners.GetSize() > 0)
	{
		glColor4ub( gray[0], gray[1], gray[2], m_pDoc->GetTransparencyFactor()/2.0); // Make it more transparent
		glBegin( GL_QUADS );
			glNormal3d( m_antAirBallReferencePlaneCorners.GetAt(4).x, m_antAirBallReferencePlaneCorners.GetAt(4).y, m_antAirBallReferencePlaneCorners.GetAt(4).z );

			glVertex3d( m_antAirBallReferencePlaneCorners.GetAt(0).x, m_antAirBallReferencePlaneCorners.GetAt(0).y, m_antAirBallReferencePlaneCorners.GetAt(0).z );
			glVertex3d( m_antAirBallReferencePlaneCorners.GetAt(1).x, m_antAirBallReferencePlaneCorners.GetAt(1).y, m_antAirBallReferencePlaneCorners.GetAt(1).z );
			glVertex3d( m_antAirBallReferencePlaneCorners.GetAt(2).x, m_antAirBallReferencePlaneCorners.GetAt(2).y, m_antAirBallReferencePlaneCorners.GetAt(2).z );
			glVertex3d( m_antAirBallReferencePlaneCorners.GetAt(3).x, m_antAirBallReferencePlaneCorners.GetAt(3).y, m_antAirBallReferencePlaneCorners.GetAt(3).z );
		glEnd();
	}


	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glEnable( GL_LIGHTING );
}