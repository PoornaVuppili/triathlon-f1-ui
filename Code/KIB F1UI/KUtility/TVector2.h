#ifndef TVECTOR2_H
#define TVECTOR2_H

#include "TPoint2.h"

#define TVector2 TPoint2
//switch to template alias in VS2013
//template<typename T> using TVector2 = TPoint2<T>;

typedef Point2Int    Vector2Int;
typedef Point2Float  Vector2Float;
typedef Point2Double Vector2Double;

#endif //TVECTOR2_H
