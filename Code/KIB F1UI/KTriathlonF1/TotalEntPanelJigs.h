#pragma once

#include <QtGui>
#include "..\KAppTotal\EntPanel.h"
#include "..\KAppTotal\Entity.h"
#include "..\KUtility\TPoint3.h"
#include "..\KUtility\TVector3.h"

class CTotalDoc;

class TEST_EXPORT_TW CTotalEntPanelJigs : public CEntPanel
{
	Q_OBJECT

public:
	CTotalEntPanelJigs(QTreeView* view);
	~CTotalEntPanelJigs();
	CTotalDoc*	GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

    virtual QString GetID() const { return "FemoralCRItem"; }
	virtual void		Add( int id, const QString& sName, CEntRole eRole );
    virtual void        OnItemClicked(const QModelIndex&);
    virtual void        OnItemRightClicked(const QModelIndex&);
    virtual void        OnItemDoubleClicked(const QModelIndex&);
	void				DisplayContextMenuJigs( int row );

protected:

public slots:
	void				OnRedefine();
	void				OnRegenerateAStep();
	void				OnRegenerateMultipleSteps();
	void				OnDeleteAllJigs();
	void				OnExportIges();
	void				OnDelete();

protected:
	QAction*			m_actRedefine;
	QAction*			m_actRegenerateAStep;
	QAction*			m_actRegenerateMultipleSteps;
};
