#ifndef OPTIONGENERAL_H
#define OPTIONGENERAL_H

#include "OptionPage.h"
namespace Ui {class OptionGeneral;};

class OptionGeneral : public OptionPage
{
    Q_OBJECT

public:
    OptionGeneral(QWidget *parent = 0);
    virtual ~OptionGeneral();

    virtual void OnOK();
    virtual void OnCancel();

private:
    Ui::OptionGeneral *ui;

    QColor topColorX, bottomColorX; // old color
    QColor topColor, bottomColor;   // current color
    bool colorChanged;              // if color is changed

    // Test if the database settings on the UI have changed
    // Return: true - changed; false - otherwise
    bool IsDatabaseSettingChanged();

    // database setting
    QString DBName, DBHost, DBUser, DBPassword;
    int     DBPort;

private slots:
    void SetColorIndex(int);
    void on_pushButtonColor_clicked();
    void on_pushButtonTestOracle_clicked();
    void on_DatabaseSettingChanged();
};

#endif // OPTIONGENERAL_H
