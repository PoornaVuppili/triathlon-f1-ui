#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class CAdvancedControlDlgJigs;
class CAdvancedControlJigs;

class TEST_EXPORT_TW CDisplayAdvancedControlJigs : public CManager
{
    Q_OBJECT

public:
					CDisplayAdvancedControlJigs( CTotalView* pView );
	virtual			~CDisplayAdvancedControlJigs();


private:

private slots:
	void			OnDispDlg(CAdvancedControlJigs* advCtrl);
	void			OnAccept();

public:
	//static CDisplayAdvancedControlJigs *pCurDispAdvCtrl;


private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;
	QAction*					m_actAdvancedControl;
	QAction*					m_actDispDlg;
	CAdvancedControlDlgJigs*	pDlg;
};
