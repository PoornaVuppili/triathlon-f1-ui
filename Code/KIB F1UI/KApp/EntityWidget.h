#ifndef ENTITYWIDGET_H
#define ENTITYWIDGET_H

#include <QTreeView>
#include <QStandardItemModel>
#include <QDomDocument>

class EntityItem;
class QResizeEvent;

#include <vector>
using namespace std;

class EntityWidget : public QTreeView
{
    Q_OBJECT

public:
    EntityWidget(QWidget *parent = 0);
    ~EntityWidget();

    // Clear everything 
    void ClearContent();

    // Whether allow an item to be closed. 
    // - By default, an item should be allowed to close.
    // - When a manager is active, give the item the option to disallow closing.
    void AllowClose(bool canClose);

    // Add/remove an item
    void AddItem(EntityItem* ei);
    void RemoveItem(EntityItem* ei);
	bool IsItemExist(EntityItem* ei);

	void SetAllChecked(bool check);
    void SetStatus(QDomElement);
    void GetStatus(QDomElement);

protected slots:
    void OnItemClicked(const QModelIndex&);
    void OnItemDoubleClicked(const QModelIndex&);
    void OnModelItemChanged(QStandardItem* item);

protected:
    virtual void resizeEvent ( QResizeEvent * event );
    virtual void mousePressEvent ( QMouseEvent * event );

private:
    // get the root row of the index
    int GetRootRow(const QModelIndex& index);

	void SetAllChecked(QStandardItem *item, bool check);

    // list of items
    vector<EntityItem*> items;

    // model containing the items
    QStandardItemModel model;
};

// global instance
extern EntityWidget* entityWidget;

#endif // ENTITYWIDGET_H
