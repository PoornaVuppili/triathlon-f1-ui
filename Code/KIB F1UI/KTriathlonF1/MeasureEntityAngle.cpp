#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwTess.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "MeasureEntityAngle.h"
#include "MeasureEntityDistance.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"


///////////////////////////////////////////////////////////////////////////////////////////
// Methods for CMeasureEntityAngle
///////////////////////////////////////////////////////////////////////////////////////////
CMeasureEntityAngle::CMeasureEntityAngle( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMeasureEntityAngle";

	m_toolbar = new QToolBar("Measure Angle");

	QLabel* labelResultAngle = new QLabel(tr("Angle degrees:"));
	labelResultAngle->setFont(m_fontBold);
	m_toolbar->addWidget(labelResultAngle);

	m_textMeasuredAngle = new QLineEdit(tr(" "));
	m_textMeasuredAngle->setReadOnly(true);
	m_textMeasuredAngle->setFixedWidth(62);
	m_textMeasuredAngle->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMeasuredAngle);

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	//m_toolbar->setMovable( false );	
	m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	m_mouseLeftDown = false;
	m_hitCount = 0;
	m_firstHitEntity.Empty();
	m_secondHitEntity.Empty();
	m_thirdHitEntity.Empty();
	m_fourthHitEntity.Empty();
	m_glEdgeDisplayList = 0;
	m_bEdgeModified = false;

}

CMeasureEntityAngle::~CMeasureEntityAngle()
{
}

bool CMeasureEntityAngle::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseLeftDown = true;

	m_selectableBreps.RemoveAll();
	m_selectableBrepsAreOpaque.RemoveAll();
	m_selectableCurves.RemoveAll();
	m_selectablePoints.RemoveAll();
	// whenever left button down, always update the selectable entities.
	if (CTotalDoc::GetTotalDoc())
		CTotalDoc::GetTotalDoc()->GetSelectableEntities(m_selectableBreps, m_selectableBrepsAreOpaque, m_selectableCurves, m_selectablePoints);
	
	// select the entities
	if (m_hitCount%4 == 0)
	{
		m_firstHitEntity.Empty();
		m_secondHitEntity.Empty();
		m_thirdHitEntity.Empty();
		m_fourthHitEntity.Empty();
		m_textMeasuredAngle->clear();
		SelectEntities(cursor, m_firstHitEntity);
		if ( !m_firstHitEntity.IsEmpty() )
		{
			if ( !m_firstHitEntity.GetPoint().IsInitialized() )
				m_hitCount += 2; // we got line/planar face, do not need to select m_secondHitEntity
			else
				m_hitCount++;	// we got point/vertex, go to select m_secondHitEntity
		}
	}
	else if (m_hitCount%4 == 1)
	{
		SelectEntities(cursor, m_secondHitEntity, NULL, true);// only select point/vertex
		if ( !m_secondHitEntity.IsEmpty() )
			m_hitCount++;

		if ( m_hitCount%4 == 2 )// To display vector line
		{
			IwPoint3d firstPoint, secondPoint;
			// this function will trigger drawing a line
			m_firstHitEntity.VectorBetween(m_secondHitEntity, firstPoint, secondPoint);		
		}
	}
	else if (m_hitCount%4 == 2)
	{
		CSelectedEntity* exclusiveEntity=NULL;
		// If m_firstHitEntity is line or planar face, do not select again.
		if ( !m_firstHitEntity.GetPoint().IsInitialized() )
			exclusiveEntity = &m_firstHitEntity;

		SelectEntities(cursor, m_thirdHitEntity, exclusiveEntity);
		if ( !m_thirdHitEntity.IsEmpty() )
		{
			if ( !m_thirdHitEntity.GetPoint().IsInitialized() )
				m_hitCount += 2; // we got line/planar face, do not need to select m_fourthHitEntity
			else
				m_hitCount++;	// we got point/vertex, go to select m_secondHitEntity
		}
	}
	else if (m_hitCount%4 == 3)
	{
		SelectEntities(cursor, m_fourthHitEntity, NULL, true);// only select point/vertex
		if ( !m_fourthHitEntity.IsEmpty() )
			m_hitCount++;
	}

	if ( m_hitCount%4 == 0 )
	{
		// determine the angle
		IwVector3d firstVec, secondVec;
		// if first & second are points/vertexes
		if ( m_firstHitEntity.GetPoint().IsInitialized() &&
			m_secondHitEntity.GetPoint().IsInitialized() )
		{
			IwPoint3d firstPoint, secondPoint;
			firstVec = m_firstHitEntity.VectorBetween(m_secondHitEntity, firstPoint, secondPoint);
		}
		// linear line
		else if (m_firstHitEntity.GetCurve() != NULL) 
		{
			m_firstHitEntity.GetCurve()->IsLinear( 0.01, NULL, &firstVec );
		}
		// planar face
		else if (m_firstHitEntity.GetSurface() != NULL)
		{
			m_firstHitEntity.GetSurface()->IsPlanar( 0.01, NULL, &firstVec );
		}

		// if third & fourth are points/vertexes
		if ( m_thirdHitEntity.GetPoint().IsInitialized() &&
			m_fourthHitEntity.GetPoint().IsInitialized() )
		{
			IwPoint3d thirdPoint, fourthPoint;
			secondVec = m_thirdHitEntity.VectorBetween(m_fourthHitEntity, thirdPoint, fourthPoint);
		}
		// linear line
		else if (m_thirdHitEntity.GetCurve() != NULL) 
		{
			m_thirdHitEntity.GetCurve()->IsLinear( 0.01, NULL, &secondVec );
			if ( firstVec.Dot(secondVec) < 0 )
				secondVec = -secondVec;
		}
		// planar face
		else if (m_thirdHitEntity.GetSurface() != NULL)
		{
			m_thirdHitEntity.GetSurface()->IsPlanar( 0.01, NULL, &secondVec );
			if ( firstVec.Dot(secondVec) < 0 )
				secondVec = -secondVec;
		}
		
		if (firstVec.Length() > 0.0001 && secondVec.Length() > 0.0001)
		{
			QString angleText;
			double angle;
			firstVec.AngleBetween(secondVec, angle);
			angleText.setNum(angle*180/IW_PI, 'f', 3);
			m_textMeasuredAngle->clear();
			m_textMeasuredAngle->setText(angleText);
		}
	}

	m_bEdgeModified = true;

	m_pView->Redraw();

	return true;
}

bool CMeasureEntityAngle::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{	
	m_pView->Redraw();

	return true;
}
bool CMeasureEntityAngle::MouseUp( const QPoint& cursor, Viewport* vp )
{

	m_pView->Redraw();


	return true;
}


bool CMeasureEntityAngle::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_pView->Redraw();

	return true;
}


bool CMeasureEntityAngle::MouseRight( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_pView->Redraw();
	return true;
}

bool CMeasureEntityAngle::keyPressEvent( QKeyEvent* event, Viewport* viewport )
{
	if ( event->key() == Qt::Key_Escape )
		Reject();

	return false; // false: do not execute subsequent actions.
}

void CMeasureEntityAngle::Display(Viewport* vp)
{		
	// determine one pixel size
	int UV0[2];
	UV0[0] = 0;
	UV0[1] = 0;
	int UV5[2];
	UV5[0] = 1;
	UV5[1] = 1;
	IwPoint3d cpt0, cpt1, cpt;
	m_pView->UVtoXYZ(UV0, cpt0);
	m_pView->UVtoXYZ(UV5, cpt1);
	cpt = cpt1 - cpt0;
	double onePixelSize = cpt.Length();

	IwVector3d viewVec(0,0,0);

	glDrawBuffer( GL_BACK );
	glDisable( GL_LIGHTING );

	glPointSize(5);
	glLineWidth(3.0);

	// Display closest points
	glColor3ub( 255, 255, 0 );

	if( m_bEdgeModified && m_glEdgeDisplayList > 0 )
	{
		if( glIsList( m_glEdgeDisplayList ) )
			glDeleteLists( m_glEdgeDisplayList, 1 );

		m_glEdgeDisplayList = 0;
	}

	if( m_glEdgeDisplayList == 0 )
	{
		QApplication::setOverrideCursor( Qt::WaitCursor );

		m_glEdgeDisplayList = m_pDoc->GetAvlDisplayList();
		glNewList( m_glEdgeDisplayList, GL_COMPILE_AND_EXECUTE );

		if ( !m_firstHitEntity.IsEmpty() )
			m_firstHitEntity.Display(viewVec, onePixelSize);

		if ( !m_secondHitEntity.IsEmpty() )
			m_secondHitEntity.Display(viewVec, onePixelSize);

		if ( !m_thirdHitEntity.IsEmpty() )
			m_thirdHitEntity.Display(viewVec, onePixelSize);

		if ( !m_fourthHitEntity.IsEmpty() )
			m_fourthHitEntity.Display(viewVec, onePixelSize);

		glEndList();

		QApplication::restoreOverrideCursor();
	}
	else
	{
		glCallList( m_glEdgeDisplayList );
	}

	// Display  lines 
	if ( m_firstHitEntity.IsClosestPointValid() &&
		 m_secondHitEntity.IsClosestPointValid() )
	{
		IwPoint3d firstPnt = m_firstHitEntity.GetClosestPoint();
		IwPoint3d secondPnt = m_secondHitEntity.GetClosestPoint();
		glBegin( GL_LINES );
			glVertex3d( firstPnt.x, firstPnt.y, firstPnt.z );
			glVertex3d( secondPnt.x, secondPnt.y, secondPnt.z );
		glEnd();
	}
	if ( m_thirdHitEntity.IsClosestPointValid() &&
		 m_fourthHitEntity.IsClosestPointValid() )
	{
		IwPoint3d thirdPnt = m_thirdHitEntity.GetClosestPoint();
		IwPoint3d fourthPnt = m_fourthHitEntity.GetClosestPoint();
		glBegin( GL_LINES );
			glVertex3d( thirdPnt.x, thirdPnt.y, thirdPnt.z );
			glVertex3d( fourthPnt.x, fourthPnt.y, fourthPnt.z );
		glEnd();
	}

	m_bEdgeModified = false;

	glPointSize(1);
	glLineWidth(1.0);
	glEnable( GL_LIGHTING );
}

void CMeasureEntityAngle::OnAccept()
{
	m_bDestroyMe = true;
	m_pView->Redraw();
}

/////////////////////////////////////////////////////////////////////////////////
// This function selects either points and vertexes, or lines, or planar faces.
/////////////////////////////////////////////////////////////////////////////////
void CMeasureEntityAngle::SelectEntities
(
	const QPoint&		cursor,			// I: cursor position 
	CSelectedEntity&	hitEntity,		// O:
	CSelectedEntity*	exclusiveEntity,// I: the entity to avoid (has been selected before).
	bool				pointOnly		// I: select point/vertex only
)
{

	// Get viewing vector from eyes to screen
	IwVector3d viewVec = m_pView->GetViewingVector();
	// determine the selection tolerance - 5 pixels
	int UV0[2];
	UV0[0] = cursor.x();
	UV0[1] = cursor.y();
	int UV5[2];
	UV5[0] = cursor.x()+5;
	UV5[1] = cursor.y();
	IwPoint3d cpt0, cpt5, cpt;
	m_pView->UVtoXYZ(UV0, cpt0);
	m_pView->UVtoXYZ(UV5, cpt5);
	cpt = cpt5 - cpt0;
	double sTol = cpt.Length();
	IwPoint3d viewPoint = (IwPoint3d)cpt0;

	if (1)
	{
		// find the hit points and vertexes
		// if found one, just return.
		IwPoint3d pointHitPoint;
		double pointHeight;
		bool bPntHit = CMeasureEntityDistance::HitPoints(m_selectablePoints, viewPoint, viewVec, sTol, pointHitPoint, pointHeight);
		IwPoint3d vertexHitPoint;
		double vertexHeight;
		bool bVerHit = CMeasureEntityDistance::HitVertexes(m_selectableBreps, viewPoint, viewVec, sTol, vertexHitPoint, vertexHeight);
		if (bPntHit && bVerHit)
		{
			if (pointHeight<vertexHeight)
				hitEntity.AddPoint(pointHitPoint);
			else
				hitEntity.AddPoint(vertexHitPoint);
			
			return;
		}
		else if (bPntHit)
		{
			hitEntity.AddPoint(pointHitPoint);
			return;
		}
		else if (bVerHit)
		{
			hitEntity.AddPoint(vertexHitPoint);
			return;
		}
	}

	// try to search for linear curves and planar faces, if got nothing yet
	if ( !pointOnly )
	{
		/////////////////////////////////////////////////////////
		// Search for linear curves
		/////////////////////////////////////////////////////////
		IwCurve* exclusiveCurve = NULL;
		IwEdge* exclusiveEdge = NULL;
		if (exclusiveEntity)
		{

			bool curveOnly = true;
			exclusiveCurve = exclusiveEntity->GetCurve(curveOnly);
			exclusiveEdge = exclusiveEntity->GetEdge();
		}
		// find the hit curves and edges
		IwCurve* curveHitCurve = NULL;
		IwPoint3d curveHitPoint;
		double curveHitParam;
		double curveHeight;
		bool bCrvHit = CMeasureEntityDistance::HitCurves(m_selectableCurves, viewPoint, viewVec, sTol, curveHitCurve, curveHitPoint, curveHitParam, curveHeight, exclusiveCurve);
		// Check whether the selected curve is linear
		if ( bCrvHit )
		{
			if ( !curveHitCurve->IsLinear(0.01) )
				bCrvHit = false;
		}
		IwEdge* edgeHitEdge = NULL;
		IwPoint3d edgeHitPoint;
		double edgeHitParam;
		double edgeHeight;
		bool bEdgeHit = CMeasureEntityDistance::HitEdges(m_selectableBreps, viewPoint, viewVec, sTol, edgeHitEdge, edgeHitPoint, edgeHitParam, edgeHeight, exclusiveEdge);
		// Check whether the selected edge is linear
		if ( bEdgeHit )
		{
			if ( !edgeHitEdge->GetCurve()->IsLinear(0.01) )
				bEdgeHit = false;
		}
		// Assign the selected curve/edge to hitEntity
		if (bCrvHit && bEdgeHit)
		{
			if (curveHeight<edgeHeight)
				hitEntity.AddCurve(curveHitCurve, curveHitPoint, curveHitParam);
			else
				hitEntity.AddEdge(edgeHitEdge, edgeHitPoint, edgeHitParam);
			
			// if found one, return.
			return;
		}
		else if (bCrvHit)
		{
			hitEntity.AddCurve(curveHitCurve, curveHitPoint, curveHitParam);
			return;
		}
		else if (bEdgeHit)
		{
			hitEntity.AddEdge(edgeHitEdge, edgeHitPoint, edgeHitParam);
			return;
		}

		//////////////////////////////////////////////////////////////
		// Search for planar faces
		//////////////////////////////////////////////////////////////
		IwFace* exclusiveFace = NULL;
		if (exclusiveEntity)
		{
			exclusiveFace = exclusiveEntity->GetFace();
		}
		// find the hit faces
		IwFace* hitFace;
		IwPoint3d hitPoint;
		IwPoint2d hitParam;
		double faceHeight;
		bool bFaceHit = CMeasureEntityDistance::HitFaces(m_selectableBreps, viewPoint, viewVec, sTol, hitFace, hitPoint, hitParam, faceHeight, exclusiveFace);
		// Check whether the selected face is planar
		if ( bFaceHit && !hitFace->GetSurface()->IsPlanar(0.01) )
			bFaceHit = false;

		// if found one, return.
		if (bFaceHit)
		{
			hitEntity.AddFace(hitFace, hitPoint, hitParam);
			return;
		}
	}

}
