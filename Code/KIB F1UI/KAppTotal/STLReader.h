#pragma once

class CPart;

#pragma pack(2)
struct CSTLFace
	{
	float		norm[3];	// Explicit normal of the facet
	float		pt[3][3];	// Three vertices of the facet
	char		dummy[2];	// Two generally unused bytes kept with every facet in STL file
	};
#pragma pack()


class CSTLReader
	{
public:
	// Constructor by file name
	CSTLReader( CPart* pPart, const QString& sFileName );

	// Destructor
	~CSTLReader();

	bool LoadFile();

private:
	enum { HEADER_SIZE = 84, FACET_SIZE = 50 };
	enum { N_FACETS_IN_BUFFER = 10000 };

	CPart*					m_pPart;
	QString					m_sFileName;
	};
