#include "TotalDocReader.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Part.h"
#include "FemoralPart.h"
#include "TotalView.h"
#include "..\KAppTotal\Globals.h"
#include "FemoralAxes.h"
#include "FemoralPart.h"
#include "FemoralCuts.h"
#include "OutlineProfile.h"
#include "AdvancedControl.h"
#include "AdvancedControlJigs.h"
#include "CartilageSurface.h"
#include "OutlineProfileJigs.h"
#include "InnerSurfaceJigs.h"
#include "OuterSurfaceJigs.h"
#include "SideSurfaceJigs.h"
#include "SolidPositionJigs.h"
#include "StylusJigs.h"
#include "AnteriorSurfaceJigs.h"
#include "..\KAppTotal\TotalDocDlg.h"
#include "QDialog.h"
#include <QtGui>

CTotalDocReader::CTotalDocReader( CTotalDoc* pDoc, const QString& sDocFolder ) : CDocReader(pDoc,sDocFolder )
{
	m_pDoc			= pDoc;
	m_sDocFolder	= sDocFolder;
}


CTotalDocReader::~CTotalDocReader()
{
}

////////////////////////////////////////////////////////////////////
// This function loads the entities described in header.txt
////////////////////////////////////////////////////////////////////
bool CTotalDocReader::LoadEntities(CEntRole startEntRole, CEntRole endEntRole)
{
	m_pDoc->AppendLog( QString("CTotalDocReader::LoadEntities()") );

	QString					sLine;
	QFile					File( m_sDocFolder + "\\header.txt" );
	QTextStream				in( &File );
	bool					bOK;
	CPart*					pPart;
	CEntity*				pEntity;
	QString					sTag, sValue;
	int						r, g, b;
	char					buf[160];
	CColor					color;
	int						m_nEntityId;

	// initialize 
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE; // this parameter was introduced in v4.0.0 Old files do not have this value.

	bOK = File.open( QIODevice::ReadOnly | QIODevice::Text );

	if( !bOK )
		return false;

	InitEntity();


	// Read through the file.
	while( !in.atEnd() )
	{
		sLine = in.readLine();

		if( sLine == "" )
			continue;

		if( sLine == "END ENTITY" )
		{
			// Only handle CR entities in between startEntRole & endEntRole
			if ( m_eEntRole >= startEntRole && m_eEntRole <= endEntRole ) 
			{
				m_pDoc->AppendLog( QString("KTotalDocReader::LoadEntities(), loading %1" ).arg(CEntity::RoleName( m_eEntRole )) );
				switch( m_eEntityType )
				{
					case ENT_TYPE_PART:
					case ENT_TYPE_FEMORAL_PART:
					case ENT_TYPE_FEMORAL_CUTS:
					{
						if ( m_eEntRole == ENT_ROLE_FEMUR )
							pPart = new CFemoralPart( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_OSTEOPHYTE_SURFACE )
						{
							pPart = new CFemoralPart( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
							if ( m_pDoc->IsOpeningFileRightVersion(6,0,0) )
								((CFemoralPart*)pPart)->SetDisplayViewProfiles(false); // Need to synchronize with "JIGS TAB LENGTH REFER TO DISTAL CUTS"
						}
						else if ( m_eEntRole == ENT_ROLE_OUTLINE_PROFILE )
							pPart = new COutlineProfile( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_FEMORAL_CUTS )
							pPart = new CFemoralCuts( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_CARTILAGE_SURFACE )
							pPart = new CCartilageSurface( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_OUTLINE_PROFILE_JIGS )
							pPart = new COutlineProfileJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_INNER_SURFACE_JIGS )
							pPart = new CInnerSurfaceJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_OUTER_SURFACE_JIGS )
							pPart = new COuterSurfaceJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_SIDE_SURFACE_JIGS )
							pPart = new CSideSurfaceJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_SOLID_POSITION_JIGS )
							pPart = new CSolidPositionJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_STYLUS_JIGS )
							pPart = new CStylusJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_ANTERIOR_SURFACE_JIGS )
							pPart = new CAnteriorSurfaceJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else
							pPart = new CPart( m_pDoc, m_eEntRole, m_sFileName, m_nEntityId );

						pPart->SetUpToDateStatus(m_bUpToDateStatus);
						pPart->SetValidateStatus(m_validateStatus, m_validateMessage);
						pPart->SetPartType( m_ePartType );
						pPart->SetDisplayMode( m_eDisplayMode );
						pPart->SetColor( color );
						pPart->SetDesignTime( m_designTime );

						pPart->Load( m_sDocFolder );

						m_pDoc->AddPart( pPart );

						break;
					}

					case ENT_TYPE_FEMORAL_AXES:
					case ENT_TYPE_ADVANCED_CONTROL:
					{
						if ( m_eEntRole == ENT_ROLE_FEMORAL_AXES )
							pEntity = new CFemoralAxes( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
				
						else if ( m_eEntRole == ENT_ROLE_ADVANCED_CONTROL )
							pEntity = new CAdvancedControl( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
						else if ( m_eEntRole == ENT_ROLE_ADVANCED_CONTROL_JIGS )
							pEntity = new CAdvancedControlJigs( GetTotalDoc(), m_eEntRole, m_sFileName, m_nEntityId );
												
						pEntity->SetDisplayMode( m_eDisplayMode );

						pEntity->SetUpToDateStatus(m_bUpToDateStatus);

						pEntity->SetValidateStatus(m_validateStatus, m_validateMessage);

						pEntity->SetColor( color );

						pEntity->SetDesignTime( m_designTime );

						pEntity->Load( m_sDocFolder );

						bool bAddToEntPanel = true;
						if ( m_eEntRole == ENT_ROLE_ADVANCED_CONTROL ||
							 m_eEntRole == ENT_ROLE_ADVANCED_CONTROL_JIGS)
							bAddToEntPanel = false;
						m_pDoc->AddEntity( pEntity, bAddToEntPanel );

						break;
					}

				}

				InitEntity();

				continue;
			}
		}

		bOK = ParseInputLine( sLine, sTag, sValue );

		if( sTag == "ENTITY ID" )
		{
			m_designTime = 0; // new parameter in iTW v6.0.7. Initalize here when start reading the entity info.
			m_nEntityId = sValue.toInt();
		}
		else if( sTag == "ENTITY ROLE" )
		{
			if ( sValue == QString("Box Cut") )
				sValue = QString("Box");// iTW v6 and iTW PS v2 change to "Box".
			m_eEntRole = CEntity::RoleByName( sValue );
		}
		else if( sTag == "ENTITY TYPE" )
		{
			m_eEntityType = CEntity::EntTypeByName( sValue );
		}
		else if( sTag == "FILE NAME" )
		{
			m_sFileName = sValue;
		}
		else if( sTag == "DISPLAY MODE" )
		{
			m_eDisplayMode = CEntity::DispModeByName( sValue );
		}
		else if( sTag == "UPTODATE STATUS" )
		{
			m_bUpToDateStatus = sValue.toInt() == 1 ? true : false;
		}
		else if( sTag == "VALIDATE STATUS" )
		{
			m_validateStatus = (CEntValidateStatus)sValue.toInt();
		}
		else if( sTag == "VALIDATE MESSAGE" )
		{
			m_validateMessage = sValue;
			QString testStr = m_validateMessage;
			testStr.remove(" "); // check whether all blank
			if (testStr.isEmpty())
				m_validateMessage = QString("");
			// Replace "#&" by "\n"
			m_validateMessage.replace(QString("#&"), QString("\n"), Qt::CaseSensitive);
		}
		else if( sTag == "ENTITY COLOR" )
		{
			QStringToCharArray(	sValue, buf );

			sscanf( buf, "%d  %d  %d", &r, &g, &b );

			color = CColor( r, g, b );
		}
		else if( sTag == "STATISTIC" )
		{
			m_designTime = (int)(sValue.toDouble()*997.0+0.5);// simple way to decode. See DocWriter.cpp
		}
		else if( sTag == "PART TYPE" )
		{
			if( sValue == "Brep" )
				m_ePartType = PART_TYPE_BREP;
			else if( sValue == "STL" )
				m_ePartType = PART_TYPE_STL;
		}
	}

	File.close();

	return true;

}

