#ifndef VIEWWIDGET_H
#define VIEWWIDGET_H

#include <QGLWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QDomDocument>
#include <QStack>
#include "..\KUtility\TContour.h"
#include "Viewport.h"
#include <vector>
using namespace std;

class Manager;

class ViewWidget : public QGLWidget
{
    Q_OBJECT

public:
    ViewWidget(QWidget *parent = 0);
    ~ViewWidget();

    // Clear the content
    void ClearContent();

    // Create the 3D viewport
    // Return:
    //   the 3d viewport if successful; otherwise NULL if failed or the viewport already exists.
    Viewport* CreateViewport(const QString& id = QString());

    // Get the 3d viewport
    Viewport* GetViewport() { return view3d; }

    // Set the view background color
    QColor GetTopColor() { return topColor; }
    QColor GetBottomColor() { return bottomColor; }
    void SetTopColor(QColor c);
    void SetBottomColor(QColor c);

    // Set the viewing status based on the content of 'docElem'
    void SetStatus(QDomElement docElem);

    // Get the viewing status and save under 'docElem'
    bool GetStatus(QDomElement& docElem);

    // Get the quadric object
    GLUquadricObj* GetQuadric() { return quad; }

    // Start, draw, and end the overlay drawing
    void StartOverlay();
    void DrawOverlay(QColor& color, ContourInt& contour);
    void EndOverlay();

    // Add the manager for 3D viewport or 2D viewport image. Default to viewport only.
    //  inViewport          : true/false = set the manager to/not to the 3D viewport
    void AddManager(Manager* mgr, bool inViewport=true/*, int inViewportImageFirst=-1, int inViewportImageCount=0*/);

    // Remove the manager from all viewports if being used
    void RemoveManager(Manager* mgr);

    // Get the current manager. Null if not available.
    Manager* GetManager() { return managerStack.isEmpty() ? NULL : managerStack.top(); }
	Manager* GetManager(QString id);

    // Set the measure mode
    void SetMeasureMode(MeasureMode m);

    // Get measurement result of the active view
    float GetActiveViewMeasure();

    void RenderText(double x, double y, double z, const QString & str, int off_x = 0, int off_y = 0);
   void RenderTextBg(double x, double y, double z, const QString & str, COLORREF bgcolor, int off_x = 0, int off_y = 0);
   void RenderTextBg(double x, double y, double z, const QString & str, COLORREF bgcolor, QFont const & otherfont, int off_x = 0, int off_y = 0);

public slots:
    // Unset the active view
    void UnsetActiveView() { activeView=NULL; updateGL(); }

	// Set active view
	void SetActiveView(Viewport* v) { activeView = v; }

    void ZoomIn();
    void ZoomOut();
    void ZoomFit();

    void OnShowViewportChanged();

protected slots:
    void MaximizeRestoreViewport();

protected:
    // OpenGL initialization
    void initializeGL();

    // Handle the resize of the widget
    void resizeGL(int width, int height);

    // Paint the widget client area
    virtual void paintGL();
    virtual void glDraw();

    // Called when a mouse button is pressed. It currently handles:
    //   Left button: pick an object 
    //   Middle button: initialize rotation 
    virtual void mousePressEvent ( QMouseEvent * event );

    // Called when a mouse is dragging. It currently handles:
    //   Middle button: rotate the object.
    virtual void mouseMoveEvent ( QMouseEvent * event );

    // Called when a mouse button is released
    virtual void mouseReleaseEvent ( QMouseEvent * event );

    // Called when a mouse button is double clicked
    virtual void mouseDoubleClickEvent ( QMouseEvent * event );
    
    // Called when a mouse wheel is scrolled, which is 
    // used to zoom in and out the 3d view.
    virtual void wheelEvent ( QWheelEvent * event );

    // Called when a key is pressed. It currently handles:
    //   Left: if a slice is picked, go to the previous slice
    //   Right: .............................. next ......... 
    //   Esc:  ...................., de-pick the slice
    //   Left,Right,Up,Down: If no slice is picked, translate the volume.
    virtual void keyPressEvent(QKeyEvent * event);

    virtual void keyReleaseEvent(QKeyEvent *ev);

	//Called when widget gets focus
    virtual void focusInEvent(QFocusEvent *event);

	//Called when widget loses focus
    virtual void focusOutEvent(QFocusEvent *event);

    // Draw the background
    void DrawBackground();

    // Draw the viewport divider
    void DrawViewportDivider();

    // Test if (x,y) is on a divider
    bool IsOnDivider(int x, int y);

    void UpdateViewports();
    void DivideViewports();

private:
    // Viewports
    Viewport* view3d;

    // Manager stack
	QStack< Manager* > managerStack;

    // Divider
    int rowDivider;         // number of divider rows
    vector<int> vDivide;    // vertical divider position
    vector<int> hDivide[2]; // horizontal divider positions (maximum of two rows)

    enum DividerType
    {
        dividerNone,
        dividerVer,
        dividerHor,
        dividerBoth
    };

    DividerType onDividerType;
    int onHDividerIndex, onVDividerIndex;
    bool modifyDivider;

    // Active viewport
    Viewport* activeView;

    // Quadric object for drawing
    GLUquadricObj* quad;

    // Widget background color 
    QColor topColor, bottomColor;
    QFont font;

    // overlay related
    QImage* overlayImage;
};

// Global instance
extern ViewWidget* viewWidget;

#endif // VIEWWIDGET_H
