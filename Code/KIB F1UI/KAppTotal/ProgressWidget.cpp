#include "ProgressWidget.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QProgressBar>

CProgressWidget::CProgressWidget( QWidget* pParent, const QString& sTitle )
	{
	m_pParent = pParent;

	m_pLayout = new QVBoxLayout( this );
	m_pLabel = new QLabel( sTitle );
	m_pProgressBar = new QProgressBar( this );

	m_pLayout->addWidget( m_pLabel );
	m_pLayout->addWidget( m_pProgressBar );

	show();
	}


CProgressWidget::~CProgressWidget()
	{
	delete m_pLabel;
	delete m_pProgressBar;
	delete m_pLayout;
	}


void CProgressWidget::Start( int nTotal )
	{
	m_pLabel->repaint();

	m_nTotal = nTotal;
	
	m_pProgressBar->reset();
	m_pProgressBar->setRange( 0, nTotal );
	}
	

void CProgressWidget::Update( int TriathlonF1 )
	{
	m_pProgressBar->setValue( TriathlonF1 );
	}
	

void CProgressWidget::Reset()
	{
	m_pProgressBar->reset();
	}


void CProgressWidget::showEvent( QShowEvent* event )
	{
	QSize	sizeParent = m_pParent->size();
	QSize	sizeThis = this->size();

	int		x = ( sizeParent.width() - sizeThis.width() ) / 2;
	int		y = ( sizeParent.height() - sizeThis.height() ) / 2;

	move( x, y );

	show();
	}