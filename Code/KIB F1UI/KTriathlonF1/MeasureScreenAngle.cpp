#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "MeasureScreenAngle.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"

CMeasureScreenAngle::CMeasureScreenAngle( CTotalView* pView ) : CManager( pView )
{

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMeasureScreenAngle";

	m_toolbar = new QToolBar("Measure Angle");

	QLabel* labelResultAngle = new QLabel(tr("Angle degrees:"));
	labelResultAngle->setFont(m_fontBold);
	m_toolbar->addWidget(labelResultAngle);

	m_textMeasuredAngle = new QLineEdit(tr(" "));
	m_textMeasuredAngle->setReadOnly(true);
	m_textMeasuredAngle->setFixedWidth(62);
	m_textMeasuredAngle->setAlignment(Qt::AlignRight);
	m_toolbar->addWidget(m_textMeasuredAngle);

	m_actTwoPointsCalculation = m_toolbar->addAction("Horizontal Angle");
	EnableAction( m_actTwoPointsCalculation, false );

	m_actAccept = m_toolbar->addAction("Accept");
	EnableAction( m_actAccept, true );

	//m_toolbar->setMovable( false );	
	m_toolbar->setVisible( true );

	m_pMainWindow->connect( m_actTwoPointsCalculation, SIGNAL( triggered() ), this, SLOT( OnTwoPointsCalculation() ) );
	m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );

	// Measure Angle here
	m_mouseMiddleDown = false;
	m_mouseLeftDown = false;
	m_mouseClickedCount = 0;
	m_mouseFirstDownPoint = IwPoint3d();
	m_mouseSecondDownPoint = IwPoint3d();
	m_mouseThirdDownPoint = IwPoint3d();
	m_mouseFourthDownPoint = IwPoint3d();

}

CMeasureScreenAngle::~CMeasureScreenAngle()
{
}

bool CMeasureScreenAngle::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseLeftDown = true;

	m_pView->Redraw();

	return true;
}

bool CMeasureScreenAngle::MouseMiddle( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseMiddleDown = true;

	m_mouseClickedCount = 0;
	m_mouseFirstDownPoint = IwPoint3d();
	m_mouseSecondDownPoint = IwPoint3d();
	m_mouseThirdDownPoint = IwPoint3d();
	m_mouseFourthDownPoint = IwPoint3d();
	
	m_pView->Redraw();

	return true;
}
bool CMeasureScreenAngle::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_mouseLeftDown)
	{
		EnableAction( m_actTwoPointsCalculation, false );

		int uvw[3];
		uvw[0] = cursor.x();
		uvw[1] = cursor.y();
		uvw[2] = 0;
		IwPoint3d pnt;
		m_pView->UVWtoXYZ(uvw, pnt);
		if (m_mouseClickedCount%4 == 0)
		{
			// Also initialize other points
			m_mouseFirstDownPoint = pnt;
			m_mouseSecondDownPoint = IwPoint3d();
			m_mouseThirdDownPoint = IwPoint3d();
			m_mouseFourthDownPoint = IwPoint3d();
			m_textMeasuredAngle->clear();
		}
		else if (m_mouseClickedCount%4 == 1)
		{
			m_mouseSecondDownPoint = pnt;
			EnableAction( m_actTwoPointsCalculation, true ); // Enable only when two points are clicked.
		}
		else if (m_mouseClickedCount%4 == 2)
			m_mouseThirdDownPoint = pnt;
		else if (m_mouseClickedCount%4 == 3)
			m_mouseFourthDownPoint = pnt;


		// calculate angle
		if (m_mouseClickedCount%4 == 3)
		{
			IwVector3d firstVec = m_mouseSecondDownPoint - m_mouseFirstDownPoint;
			IwVector3d secondVec = m_mouseFourthDownPoint - m_mouseThirdDownPoint;
			if (firstVec.Length() > 0.0001 && secondVec.Length() > 0.0001)
			{
				QString angleText;
				double angle;
				firstVec.AngleBetween(secondVec, angle);
				angleText.setNum(angle*180/IW_PI, 'f', 3);
				m_textMeasuredAngle->clear();
				m_textMeasuredAngle->setText(angleText);
			}
		}

		m_mouseClickedCount++;

	}
	m_mouseLeftDown = false;
	m_mouseMiddleDown = false;

	m_pView->Redraw();


	return true;
}


bool CMeasureScreenAngle::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{

	if ( m_mouseMiddleDown )
	{
		m_mouseClickedCount = 0;
		m_mouseFirstDownPoint = IwPoint3d();
		m_mouseSecondDownPoint = IwPoint3d();
		m_mouseThirdDownPoint = IwPoint3d();
		m_mouseFourthDownPoint = IwPoint3d();
	}

	m_pView->Redraw();

	return true;
}


bool CMeasureScreenAngle::MouseRight( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_mouseClickedCount = 0;
	m_mouseFirstDownPoint = IwPoint3d();
	m_mouseSecondDownPoint = IwPoint3d();
	m_mouseThirdDownPoint = IwPoint3d();
	m_mouseFourthDownPoint = IwPoint3d();

	m_pView->Redraw();
	return true;
}

bool CMeasureScreenAngle::keyPressEvent( QKeyEvent* event, Viewport* viewport )
{
	if ( event->key() == Qt::Key_Escape )
		Reject();

	return false; // false: do not execute subsequent actions.
}

void CMeasureScreenAngle::Display(Viewport* vp)
{		
	glDrawBuffer( GL_BACK );
	glDisable( GL_LIGHTING );
	IwPoint3d pnt;
	IwVector3d offset = m_pView->GetLayerOffsetVector(1);
	glPointSize(5);
	// Display points
	if (m_mouseFirstDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseFirstDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if (m_mouseSecondDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseSecondDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if (m_mouseThirdDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseThirdDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if (m_mouseFourthDownPoint.IsInitialized())
	{
		glBegin( GL_POINTS );
			glColor3ub( 255, 255, 0 );
			pnt = m_mouseFourthDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	//// Display  lines 
	if ( m_mouseFirstDownPoint.IsInitialized() &&
		 m_mouseSecondDownPoint.IsInitialized() &&
		 m_mouseFirstDownPoint != m_mouseSecondDownPoint )
	{
		glLineWidth( 1.0 );
		glBegin( GL_LINES );
			pnt = m_mouseFirstDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = m_mouseSecondDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}
	if ( m_mouseThirdDownPoint.IsInitialized() &&
		 m_mouseFourthDownPoint.IsInitialized() &&
		 m_mouseThirdDownPoint != m_mouseFourthDownPoint )
	{
		glLineWidth( 1.0 );
		glBegin( GL_LINES );
			pnt = m_mouseThirdDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
			pnt = m_mouseFourthDownPoint + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
	}


	glPointSize(1);
	glEnable( GL_LIGHTING );
}

void CMeasureScreenAngle::OnTwoPointsCalculation()
{
	// calculate angle
	if (m_mouseClickedCount%4 == 2)
	{
		int uvw[3];
		uvw[0] = 0;
		uvw[1] = 0;
		uvw[2] = 0;
		IwPoint3d pnt0;
		m_pView->UVWtoXYZ(uvw, pnt0);
		uvw[0] = 1;
		uvw[1] = 0;
		uvw[2] = 0;
		IwPoint3d pnt1;
		m_pView->UVWtoXYZ(uvw, pnt1);
		IwVector3d viewVec = m_pView->GetViewingVector();
		IwVector3d firstVec = m_mouseSecondDownPoint - m_mouseFirstDownPoint;
		IwVector3d secondVec = (IwVector3d)(pnt1 - pnt0);
		if ( firstVec.Length() > 0.0001 )
		{
			QString angleText;
			double angle;
			firstVec.AngleBetween(secondVec, angle);
			if ( viewVec.Dot(secondVec*firstVec) > 0 )
				angle = -angle;
			angleText.setNum(angle*180/IW_PI, 'f', 3);
			m_textMeasuredAngle->clear();
			m_textMeasuredAngle->setText(angleText);
		}
		EnableAction( m_actTwoPointsCalculation, false );
		m_mouseClickedCount = 0;// reset to 0.
	}

}

void CMeasureScreenAngle::OnAccept()
{
	m_bDestroyMe = true;
	m_pView->Redraw();
}

