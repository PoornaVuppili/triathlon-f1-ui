#include "DefineOutlineProfile.h"
#include "OutlineProfileMgrUI3D.h"
#include "OutlineProfileMgrUI2D.h"
#include "OutlineProfileMgrModel.h"
#include "OutlineProfile.h"
#include "TotalDoc.h"
#include "..\KAppTotal\Utilities.h"
#include "FemoralAxes.h"
#include "FemoralPart.h"
#include "FemoralCuts.h"
#include "TotalView.h"
#include "TotalMainWindow.h"
#include "AdvancedControl.h"

#include <QApplication>
#include <QMessageBox>

CDefineOutlineProfile::CDefineOutlineProfile(CTotalView* pView, CManagerActivateType manActType)
	: CManager(pView, manActType),
	m_actReset(NULL),
	m_actValidate(NULL),
	m_mgrModel(NULL),
	m_mgrUICurrent(NULL),
	m_mgrUI3D(NULL),
	m_mgrUI2D(NULL)
{
	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT || manActType == MAN_ACT_TYPE_REVIEW );
	m_pDoc = pView->GetTotalDoc();

	EnableUndoStack(true);

	m_sClassName = "CDefineOutlineProfile";

	m_mgrModel = new OutlineProfileMgrModel(m_pDoc);

	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();
	if( manActType == MAN_ACT_TYPE_EDIT ||
		manActType == MAN_ACT_TYPE_REFINE )
		m_mgrModel->InitializeFeaturePoints();

	m_bUIType = activateUI;
	m_mgrUI3D = new OutlineProfileMgrUI3D(m_pDoc, manActType, this, m_mgrModel);
	m_mgrUICurrent = m_mgrUI3D;

	//
	m_actReviewBack = NULL;
	m_actReviewNext = NULL;
	m_actReviewRework = NULL;

	if (  manActType == MAN_ACT_TYPE_EDIT )
	{
		CAdvancedControl* advCtrl = m_pDoc->GetAdvancedControl();

		m_toolbar = new QToolBar("Outline Profile");

		m_actDefineOutlineProfile = m_toolbar->addAction("Outline");
		SetActionAsTitle( m_actDefineOutlineProfile );
		
		if ( advCtrl->GetOutlineProfileEnableReset() || advCtrl->GetAutoNone() )
		{
			m_actReset = m_toolbar->addAction("Reset");
			EnableAction( m_actReset, true );
		}
		m_actRefine = m_toolbar->addAction("Refine");
		EnableAction( m_actRefine, true );
		m_actDispRuler = m_toolbar->addAction("Ruler");
		EnableAction( m_actDispRuler, true );
		if(!m_bUIType)
		{
			m_actToggleMode = m_toolbar->addAction("3D");
			m_actToggleMode->setToolTip("Switch to 3D mode");
		}
		else
		{
			m_actToggleMode = m_toolbar->addAction("2D");
			m_actToggleMode->setToolTip("Switch to 2D mode");
		}
		EnableAction(m_actToggleMode, true);
		m_actAdd = m_toolbar->addAction(QIcon( ":/KTriathlonF1/Resources/Plus.png" ),QString(""));
		m_actAdd->setToolTip("Add a control point");
		m_actAdd->setShortcut(QKeySequence("Ctrl+P"));
		EnableAction( m_actAdd, true );
		m_actDel = m_toolbar->addAction(QIcon( ":/KTriathlonF1/Resources/Minus.png" ),QString(""));
		m_actDel->setToolTip("Delete a control point");
		m_actDel->setShortcut(QKeySequence("Alt+M"));
		EnableAction( m_actDel, true );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction(m_actValidate, true);
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		m_actCancel = m_toolbar->addAction("Cancel");
		EnableAction( m_actCancel, true );

		ConnectMgrUI();
		connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );
		connect( m_actToggleMode, SIGNAL( triggered() ), this, SLOT( OnToggleMode() ) );

		if (outlineProfile)
		{
			QString validateMessage;
			CEntValidateStatus validateStatus = outlineProfile->GetValidateStatus(validateMessage);
			SetValidateIcon(validateStatus, validateMessage);
		}

	}
	else if (m_manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Outline Profile");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );

		m_actDefineOutlineProfile = m_toolbar->addAction("Outline");
		SetActionAsTitle( m_actDefineOutlineProfile );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		QString validateMessage;
		CEntValidateStatus validateStatus = outlineProfile->GetValidateStatus(validateMessage);
		SetValidateIcon(validateStatus, validateMessage);
		EnableAction(m_actValidate, true);
		connect( m_actValidate, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnValidate() ) );
		connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	if ( !activateUI )
		OnAccept();
}

CDefineOutlineProfile::~CDefineOutlineProfile()
{
	COutlineProfile*	outlineProfile = m_pDoc->GetOutlineProfile();
	if ( outlineProfile != NULL )
	{
		int previoisDesignTime = outlineProfile->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		outlineProfile->SetDesignTime(previoisDesignTime+thisDesignTime);
	}

	delete m_mgrUI3D;
	delete m_mgrUI2D;
	delete m_mgrModel;
}

void CDefineOutlineProfile::ConnectMgrUI()
{
	CAdvancedControl* advCtrl = m_pDoc->GetAdvancedControl();

	if ( advCtrl->GetOutlineProfileEnableReset() || advCtrl->GetAutoNone() )
		connect( m_actReset, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnReset() ) );
	connect( m_actRefine, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnRefine() ) );
	connect( m_actAdd, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnAdd() ) );
	connect( m_actDel, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnDel() ) );
	connect( m_actDispRuler, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnDispRuler() ) );
	connect( m_actValidate, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnValidate() ) );
}

void CDefineOutlineProfile::DisconnectMgrUI()
{
	CAdvancedControl* advCtrl = m_pDoc->GetAdvancedControl();

	if ( advCtrl->GetOutlineProfileEnableReset() || advCtrl->GetAutoNone() )
		disconnect( m_actReset, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnReset() ) );
	disconnect( m_actRefine, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnRefine() ) );
	disconnect( m_actAdd, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnAdd() ) );
	disconnect( m_actDel, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnDel() ) );
	disconnect( m_actDispRuler, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnDispRuler() ) );
	disconnect( m_actValidate, SIGNAL( triggered() ), m_mgrUICurrent, SLOT( OnValidate() ) );
}

void CDefineOutlineProfile::Display(Viewport* vp)
{
	if(m_mgrUICurrent)
		m_mgrUICurrent->Display(vp);
}

void CDefineOutlineProfile::CalculateSketchSurfaceNormalInfo(IwBSplineSurface*& skchSurface)
{
	m_mgrUICurrent->CalculateSketchSurfaceNormalInfo(skchSurface);
}

void CDefineOutlineProfile::DetermineViewingNormalRegion()
{
	m_mgrUICurrent->DetermineViewingNormalRegion();
}

void CDefineOutlineProfile::RefineAnteriorOuterPoints(IwTArray<IwPoint3d>& cutFeatPnts, IwTArray<IwPoint3d>& antOuterPnts)
{
	m_mgrUICurrent->RefineAnteriorOuterPoints(cutFeatPnts, antOuterPnts);
}

bool CDefineOutlineProfile::MouseLeft(const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	return m_mgrUICurrent->MouseLeft(cursor, keyModifier, vp);
}

bool CDefineOutlineProfile::MouseUp(const QPoint& cursor, Viewport* vp)
{
	return m_mgrUICurrent->MouseUp(cursor, vp);
}

bool CDefineOutlineProfile::MouseMove(const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	return m_mgrUICurrent->MouseMove(cursor, keyModifier, vp);
}

bool CDefineOutlineProfile::BaseMouseMove(const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	return CManager::MouseMove(cursor, keyModifier, vp);
}

bool CDefineOutlineProfile::MouseDoubleClickLeft(const QPoint& cursor, Viewport* vp)
{
	return m_mgrUICurrent->MouseDoubleClickLeft(cursor, vp);
}

bool CDefineOutlineProfile::MouseMiddle(const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	return m_mgrUICurrent->MouseMiddle(cursor, keyModifier, vp);
}

bool CDefineOutlineProfile::keyPressEvent( QKeyEvent* event, Viewport* vp)
{
	bool continueResponse = CManager::keyPressEvent(event, vp);
	if ( !continueResponse )
		return false;

	return m_mgrUICurrent->keyPressEvent(event, vp);
}

void CDefineOutlineProfile::DetermineOutlineProfile()
{
	m_mgrUICurrent->DetermineOutlineProfile();
}

void CDefineOutlineProfile::GetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData)
{
	m_mgrUICurrent->GetDefineOutlineProfileUndoData(undoData);
}

void CDefineOutlineProfile::SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData)
{
	m_mgrUICurrent->SetDefineOutlineProfileUndoData(undoData);
}

void CDefineOutlineProfile::OnToggleMode()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	DisconnectMgrUI();

	if(m_mgrUI2D == NULL)
		m_mgrUI2D = new OutlineProfileMgrUI2D(m_pDoc, m_manActType, this, m_mgrModel);

	m_bUIType = !m_bUIType;

	//m_pDoc->GetInnerSurface()->Enable2DView(!m_bUIType);
	m_pDoc->GetOutlineProfile()->Set2DEditing(!m_bUIType);

	if(!m_bUIType)
	{
		m_mgrUICurrent = m_mgrUI2D;
		m_actToggleMode->setText("3D");
		m_actToggleMode->setToolTip("Switch to 3D mode");

		CEntity *pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}

		GetView()->GetViewWidget()->GetViewport()->BottomView(false);
	}
	else
	{
		m_mgrUICurrent = m_mgrUI3D;
		m_actToggleMode->setText("2D");
		m_actToggleMode->setToolTip("Switch to 2D mode");

		CEntity *pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}

		GetView()->GetViewWidget()->GetViewport()->BottomView(false);
	}

	ConnectMgrUI();

	m_mgrUICurrent->SetMouseMoveSearchingList();

	ReDraw();
	GetView()->GetViewWidget()->GetViewport()->BestFit(.5);

	QApplication::restoreOverrideCursor();
}

void CDefineOutlineProfile::OnAccept()
{
	m_mgrUICurrent->OnAccept();

	if ( m_manActType == MAN_ACT_TYPE_EDIT )	
	{

		// Make JCOSToolbar visible
		((CTotalMainWindow*)m_pView->GetMainWindow())->SetJCOSToolbarVisible(true);
	}

	m_bDestroyMe = true;
	m_pView->Redraw();
}

void CDefineOutlineProfile::OnCancel()
{
	m_mgrUICurrent->OnCancel();

	// Make JCOSToolbar visible
	if ( m_manActType == MAN_ACT_TYPE_EDIT )
	{
		((CTotalMainWindow*)m_pView->GetMainWindow())->SetJCOSToolbarVisible(true);
	}

	m_bDestroyMe = true;
	m_pView->Redraw();
}

bool CDefineOutlineProfile::OnReviewNext()
{
	GetView()->OnActivateNextReviewManager(this, ENT_ROLE_OUTLINE_PROFILE, true);
	return true;
}

bool CDefineOutlineProfile::OnReviewBack()
{
	GetView()->OnActivateNextReviewManager(this, ENT_ROLE_OUTLINE_PROFILE, false);
	return true;
}

bool CDefineOutlineProfile::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;


	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_OUTLINE_PROFILE))
    	OnAccept();
	return true;
}

void CDefineOutlineProfile::SetValidateIcon(CEntValidateStatus vStatus, QString message)
{
	if ( m_actValidate )
	{
		m_actValidate->setToolTip( message );
		if ( vStatus == VALIDATE_STATUS_RED )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Red.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_YELLOW )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Yellow.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_GREEN )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Green.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			m_actValidate->setToolTip( QString("Not validate.") );
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate.png" ) );
		}
	}
}

#pragma region Static_Methods
//////////////////////////////////////////////////////////////
//////////STATIC methods should probably be on their own somewhere
//////////////////////////////////////////////////////////////
void CDefineOutlineProfile::CreateOutlineProfileObject(CTotalDoc* pDoc)
{
	// Create OutlineProfile object
	if ( pDoc->GetOutlineProfile() == NULL )
	{
		COutlineProfile* pOutlineProfile = new COutlineProfile(pDoc);
		pDoc->AddEntity(pOutlineProfile, true);
	}
}

//void CDefineOutlineProfile::InitializeNotchFeatures(CTotalDoc* pDoc)
//{
//	//// Step# 1, offset outer surface based on estimated implant ML size
//	//double implantEstimatedMLSize = pDoc->GetOutlineProfile()->GetEstimatedImplantMLSize();
//	//double criticalThickness = CAnalyzeImplant::DetermineCriticalThickness(pDoc, implantEstimatedMLSize);
//	//// The criticalThickness has to be modified since the thickness is not measured along outer surface normal,
//	//// but (almost) along the cut plane. Here we assume the outer surface has 15 degrees slant near the notch feature points.
//	//double offsetDist = criticalThickness*cos(15.0/180*IW_PI);
//	//IwTArray<IwSurface*> oSurfs;
//	//IwBSplineSurface *oSurface, *outerSurface, *offsetSurface;
//	//IwSurface *copySurface;;
//	//pDoc->GetOuterSurface()->GetIwBrep()->GetSurfaces(oSurfs);
//	//oSurface = (IwBSplineSurface*)oSurfs.GetAt(0);
//	//oSurface->Copy(pDoc->GetIwContext(), copySurface);
//	//outerSurface = (IwBSplineSurface*) copySurface;
//	//outerSurface->ReparametrizeWithArcLength();
//	//// Although the given tolerance is 0.25mm, the offsetSurface is actually accurate within 0.03mm for Case7275.
//	//outerSurface->ApproximateOffsetSurface(pDoc->GetIwContext(), -offsetDist, 0.25, offsetSurface);
//
//	//// Step# 2, determine notch feature profile
//	//IwBSplineCurve *notchProfile, *lateralEnghteenCurve, *medialEnghteenCurve;
//	//CDefineOutlineProfile::DetermineNotchFeatureProfiles(pDoc, offsetSurface, notchProfile, lateralEnghteenCurve, medialEnghteenCurve);
//
//	//// Step# 3, determine outline profile feature points
//	//IwTArray<IwBSplineCurve*> notchOutlineProfileCurves;
//	//IwTArray<IwPoint3d> notchOutlineProfileFeaturePoints;
//	//CDefineOutlineProfile::EstimateNotchOutlineProfileFeaturePoints(pDoc, offsetSurface, notchProfile, lateralEnghteenCurve, medialEnghteenCurve, notchOutlineProfileCurves, notchOutlineProfileFeaturePoints);
//
//	//pDoc->GetOutlineProfile()->SetEstimatedNotchOutlineProfileFeatures(notchOutlineProfileCurves, notchOutlineProfileFeaturePoints);
//}

///////////////////////////////////////////////////////////////////
// See comments in output parameters
///////////////////////////////////////////////////////////////////
bool CDefineOutlineProfile::DetermineNotchFeatureProfiles
(
	CTotalDoc* pDoc,						// I:
	IwBSplineSurface*& thicknessSurface,	// I:
	IwBSplineCurve*& notchProfile,			// O: intersection between femur surface and thicknessSurface
	IwBSplineCurve*& medialEnghteenCurve,	// O: intersection between thicknessSurface and 8.5mm offset surface
	IwBSplineCurve*& lateralEnghteenCurve	// O: intersection between thicknessSurface and 9.5mm offset surface
)
{

//	// Get femoral axes
//	IwAxis2Placement fAxes;
//	pDoc->GetFemoralAxes()->GetSweepAxes(fAxes);
//	// get side info 
//	// Get implant side information
//	QString sideInfo;
//	CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);
//	bool positiveSideIsLateral;
//	double lOffsetDist, mOffsetDist;
//	// 
//	if (implantSide == IMPLANT_SIDE_RIGHT)
//	{
//		positiveSideIsLateral = true;
//		lOffsetDist = 8.5;
//		mOffsetDist = -9.5;
//	}
//	else
//	{
//		positiveSideIsLateral = false;
//		lOffsetDist = -8.5;
//		mOffsetDist = 9.5;
//	}
//	// Create offset tCurve to lateral side with 8.5mm offset distance
//	IwBSplineCurve* curve2 = (IwBSplineCurve*)pDoc->GetJCurves()->GetJCurve(2);
//	IwCurve* copyCurve;
//	curve2->Copy(pDoc->GetIwContext(), copyCurve);
//	IwBSplineCurve* ltCurve = (IwBSplineCurve*)copyCurve;
//	curve2->Copy(pDoc->GetIwContext(), copyCurve);
//	IwBSplineCurve* mtCurve = (IwBSplineCurve*)copyCurve;
//	IwExtent1d troDomain = ltCurve->GetNaturalInterval();
//	IwExtent1d trimDomain( troDomain.GetMin(), troDomain.GetMin() + 0.35*troDomain.GetLength() );
//	ltCurve->Trim(trimDomain);
//	mtCurve->Trim(trimDomain);
//	// Also move along z-axis 12.5mm to help the linear sweep ltSurface intersect with outer surface later on.
//	IwAxis2Placement lTrans(lOffsetDist*fAxes.GetXAxis()+12.5*fAxes.GetZAxis(), IwVector3d(1,0,0), IwVector3d(0,1,0));
//	ltCurve->Transform(lTrans);
//	IwAxis2Placement mTrans(mOffsetDist*fAxes.GetXAxis()+12.5*fAxes.GetZAxis(), IwVector3d(1,0,0), IwVector3d(0,1,0));
//	mtCurve->Transform(mTrans);
//if (0)
//{
//ShowCurve(pDoc, mtCurve, red);
//ShowCurve(pDoc, ltCurve, blue);
//}
//	// Create a linear sweep
//	IwBSplineSurface *ltSurface, *mtSurface;
//	IwBSplineSurface::CreateLinearSweep(pDoc->GetIwContext(), *ltCurve, -25*fAxes.GetZAxis(), ltSurface);
//	IwBSplineSurface::CreateLinearSweep(pDoc->GetIwContext(), *mtCurve, -25*fAxes.GetZAxis(), mtSurface);
//
//	// Determine intersection between femur surface and thicknessSurface
//	IwBSplineSurface* outerSurface = pDoc->GetFemur()->GetSinglePatchSurface();
//	IwBoolean useEdges[2];
//	useEdges[0] = FALSE;useEdges[1] = FALSE;
//	IwTArray<IwCurve*> fInterCurves;
//	thicknessSurface->GlobalSurfaceIntersect(pDoc->GetIwContext(), thicknessSurface->GetNaturalUVDomain(), *outerSurface, outerSurface->GetNaturalUVDomain(), useEdges, NULL, NULL, &fInterCurves, NULL, NULL, NULL, NULL);
//
//	// Determine intersection between ltSurface and thicknessSurface
//	useEdges[0] = TRUE;useEdges[1] = TRUE;
//	IwTArray<IwCurve*> ltInterCurves, mtInterCurves;
//	IntersectSurfaces(thicknessSurface, ltSurface, ltInterCurves);
//	IntersectSurfaces(thicknessSurface, mtSurface, mtInterCurves);
//	IwPoint3d farNYPoint = fAxes.GetOrigin() - 100*fAxes.GetYAxis();
//	lateralEnghteenCurve = MakeSplineFromCurves(ltInterCurves, farNYPoint);
//	medialEnghteenCurve = MakeSplineFromCurves(mtInterCurves, farNYPoint);
//
//	// Find the fInterCurve which has the shortest distance to white side line origin
//	IwAxis2Placement wslAxes;
//	pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
//	double minDist = 1000000;
//	IwCurve* fInterCurve = NULL;
//	IwPoint3d closestPnt;
//	double closestParam;
//	for (unsigned i=0; i<fInterCurves.GetSize(); i++)
//	{
//		IwPoint3d cPnt;
//		double cParam;
//		double dist = DistFromPointToCurve(wslAxes.GetOrigin(), (IwBSplineCurve*)fInterCurves.GetAt(i), cPnt, cParam);
//		if ( dist < minDist )
//		{
//			minDist = dist;
//			fInterCurve = fInterCurves.GetAt(i);
//			closestPnt = cPnt;
//			closestParam = cParam;
//		}
//	}
//
//	notchProfile = (IwBSplineCurve*)fInterCurve;
//
//if (0)
//{
//ShowCurve(pDoc, notchProfile, green);
//ShowCurve(pDoc, lateralEnghteenCurve, cyan);
//ShowCurve(pDoc, medialEnghteenCurve, magenta);
//}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// This formula here should be the same as DetermineNotchEighteenWidthPoints()
///////////////////////////////////////////////////////////////////////////////
bool CDefineOutlineProfile::EstimateNotchOutlineProfileFeaturePoints
(
	CTotalDoc* pDoc,								// I:
	IwBSplineSurface*& thicknessSurface,			// I:
	IwBSplineCurve*& notchProfile,					// I:
	IwBSplineCurve*& medialEnghteenCurve,			// I:
	IwBSplineCurve*& lateralEnghteenCurve,			// I:
	IwTArray<IwBSplineCurve*>& notchOutlineProfileCurves,	// O: 0: medial notch curve, 1: lateral notch curve
	IwTArray<IwPoint3d>& notchOutlineProfileFeaturePoints	// O: 0: notch tip point, 1: medial eighteen point, 2: lateral eighteen point
)
{
	//// Determine notch profile tip point
	//// Get trochlear Curve
	//IwCurve* trochlearCurve = pDoc->GetJCurves()->GetJCurve(2);
	//IwCurve* tCurve;
	//trochlearCurve->Copy(pDoc->GetIwContext(), tCurve);
	//IwBSplineCurve *ttCurve = (IwBSplineCurve *)tCurve;
	//ttCurve->ReparametrizeWithArcLength();

	//IwAxis2Placement wslAxes;
	//pDoc->GetFemoralAxes()->GetWhiteSideLineAxes(wslAxes);
	//IwPoint3d tPoint, closestPoint, downPoint, downDownPoint;
	//double tParam;
	//IwPoint2d param, eighteenWidthParam;
	//// get the closest point on trochlear curve first
	//DistFromPointToCurve(wslAxes.GetOrigin(), ttCurve, tPoint, tParam);

	//// Get femoral size
	//double femoralSize = pDoc->GetFemoralAxes()->GetEpiCondyleSize();
	//// Here we try to map femoralSize [60~90] to overhangDist [5.75~8.75], (hard coded value)
	//// The distance is along the free-form thicknessSurface, therefore distance [5.75~8.75] could be very close to 
	//// the distance [7~10] in distal view.
	//double overhangDist = 5.75 + 3.0*(femoralSize-60.0)/30.0;
	//// Here we try to map EighteenWidthDist, (hard coded value)  
	//double EighteenWidthChord = 3.25 + 4.5*(femoralSize-60.0)/30.0;

	//// Move down along trochlear curve with overhangDist distance
	//double downParam = tParam - overhangDist;
	//ttCurve->EvaluatePoint(downParam, downPoint);
	//// downPoint should distant to origin >=7.0mm along Y direction
	//// 0.125 as allowance
	//double desiredDist = 0.125 + pDoc->GetVarTableValue("OUTLINE PROFILE NOTCH TIP MIN DISTANCE TO ORIGIN");
	//IwVector3d Vec7 = downPoint - wslAxes.GetOrigin();
	//double extraParam = desiredDist - Vec7.Dot(-wslAxes.GetYAxis());
	//if ( extraParam > 0 )
	//{
	//	overhangDist = overhangDist + 1.5*extraParam;
	//	ttCurve->EvaluatePoint(tParam - overhangDist, downPoint);
	//}
	//// get the closest point on thicknessSurface
	//DistFromPointToSurface(downPoint, thicknessSurface, closestPoint, param);
	//// Move down along trochlear curve with overhangDist+EighteenWidthChord distance
	//double downDownParam = tParam - overhangDist - EighteenWidthChord;
	//ttCurve->EvaluatePoint(downDownParam, downDownPoint);
	//// get the closest point on thicknessSurface
	//DistFromPointToSurface(downDownPoint, thicknessSurface, closestPoint, eighteenWidthParam);

	//IwPoint2d notchTipParam;
	//// get side info 
	//// Get implant side information
	//QString sideInfo;
	//CImplantSide implantSide = pDoc->GetImplantSide(sideInfo);
	//bool positiveSideIsLateral;
	//// 
	//if (implantSide == IMPLANT_SIDE_RIGHT)
	//{
	//	positiveSideIsLateral = true;
	//	notchTipParam = param + IwPoint2d(1.0, 0.0);// move to medial side 1.0mm
	//}
	//else
	//{
	//	positiveSideIsLateral = false;
	//	notchTipParam = param + IwPoint2d(-1.0, 0.0);// move to medial side 1.0mm
	//}

	//IwPoint3d notchTipPoint;
	//thicknessSurface->EvaluatePoint(notchTipParam, notchTipPoint);

	//// Determine the end points of the eighteenWidth by the intersection
	//// points between the iso-curve at eighteenWidthParam and the medialEnghteenCurve & lateralEnghteenCurve;
	//IwBSplineCurve *eighteenWidthCurve, *Curve6, *Curve13;
	//thicknessSurface->CreateIsoParametricCurve(pDoc->GetIwContext(), IW_SP_V, eighteenWidthParam.y, 0.01, eighteenWidthCurve);
	//// Curve6 could be the cut edge between 1st & 2nd post chamfer cuts
	//thicknessSurface->CreateIsoParametricCurve(pDoc->GetIwContext(), IW_SP_V, eighteenWidthParam.y-6.5, 0.01, Curve6);
	//// Curve13 could be the cut edge between 2nd post cham and post cuts
	//thicknessSurface->CreateIsoParametricCurve(pDoc->GetIwContext(), IW_SP_V, eighteenWidthParam.y-13.0, 0.01, Curve13);
	//// Split Curve6 into medial/lateral curve
	//IwCurve *mCurve6, *lCurve6, *mCurve13, *lCurve13;
	//if ( positiveSideIsLateral )
	//{
	//	Curve6->CreateBySplitCurve(pDoc->GetIwContext(), Curve6->GetNaturalInterval().GetMid(), mCurve6);
	//	lCurve6 = (IwCurve*)Curve6;
	//	Curve13->CreateBySplitCurve(pDoc->GetIwContext(), Curve13->GetNaturalInterval().GetMid(), mCurve13);
	//	lCurve13 = (IwCurve*)Curve13;
	//}
	//else
	//{
	//	Curve6->CreateBySplitCurve(pDoc->GetIwContext(), Curve6->GetNaturalInterval().GetMid(), lCurve6);
	//	mCurve6 = (IwCurve*)Curve6;
	//	Curve13->CreateBySplitCurve(pDoc->GetIwContext(), Curve13->GetNaturalInterval().GetMid(), lCurve13);
	//	mCurve13 = (IwCurve*)Curve13;
	//}

	//// Determine the end points of the eighteenWidth by the intersection
	//IwPoint3d ePoint, mEighteenWidthPoint, lEighteenWidthPoint;
	//double eParam, mEighteenParam, lEighteenParam;
	//DistFromCurveToCurve(eighteenWidthCurve, medialEnghteenCurve, ePoint, eParam, mEighteenWidthPoint, mEighteenParam);
	//DistFromCurveToCurve(eighteenWidthCurve, lateralEnghteenCurve, ePoint, eParam, lEighteenWidthPoint, lEighteenParam);
	//// Determine the intersection between Curve6 and notchProfile
	//IwPoint3d mCurve6Point, lCurve6Point;
	//double mCurve6Param, lCurve6Param;
	//DistFromCurveToCurve(notchProfile, (IwBSplineCurve*)mCurve6, ePoint, eParam, mCurve6Point, mCurve6Param);
	//DistFromCurveToCurve(notchProfile, (IwBSplineCurve*)lCurve6, ePoint, eParam, lCurve6Point, lCurve6Param);
	//// Determine the intersection between Curve13 and notchProfile
	//IwPoint3d mCurve13Point, lCurve13Point;
	//double mCurve13Param, lCurve13Param;
	//DistFromCurveToCurve(notchProfile, (IwBSplineCurve*)mCurve13, ePoint, eParam, mCurve13Point, mCurve13Param);
	//DistFromCurveToCurve(notchProfile, (IwBSplineCurve*)lCurve13, ePoint, eParam, lCurve13Point, lCurve13Param);

	//// drop the points onto thicknessSurface to get UV parameters
	//IwTArray<IwPoint3d> IntUVPnts;
	//DistFromPointToSurface(lCurve13Point, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));
	//DistFromPointToSurface(lCurve6Point, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));
	//DistFromPointToSurface(lEighteenWidthPoint, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));
	//DistFromPointToSurface(notchTipPoint, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));
	//DistFromPointToSurface(mEighteenWidthPoint, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));
	//DistFromPointToSurface(mCurve6Point, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));
	//DistFromPointToSurface(mCurve13Point, thicknessSurface, closestPoint, param);
	//IntUVPnts.Add(IwPoint3d(param.x, param.y, 0));

	//// Interpolate IntUVPnts into a 2D (UV) curve
	//IwBSplineCurve* UVCurve;
	//IwBSplineCurve::InterpolatePoints(pDoc->GetIwContext(), IntUVPnts, NULL, 2, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, UVCurve);
	//// create a curveOnSurface
	//IwCrvOnSurf* crvOnSurf = new (pDoc->GetIwContext()) IwCrvOnSurf(*UVCurve, *thicknessSurface);
	//// Convert crvOnSurf into pure 3D curve
	//IwTArray<double> breakParams;
	//breakParams.Add(crvOnSurf->GetNaturalInterval().GetMin());
	//breakParams.Add(crvOnSurf->GetNaturalInterval().GetMax());
	//double aTol;
	//IwCurve *splitCurve;
	//IwBSplineCurve *notchOutlineProfileCurve, *mNotchOutlineProfileCurve, *lNotchOutlineProfileCurve;
	//crvOnSurf->ApproximateCurve(pDoc->GetIwContext(), IW_AA_HERMITE, breakParams, 0.01, aTol, notchOutlineProfileCurve, FALSE, FALSE);
	//// split notchOutlineProfileCurve into 2 curves
	//DistFromPointToCurve(wslAxes.GetOrigin(), notchOutlineProfileCurve, tPoint, tParam);
	//IwPoint3d plusPoint, minusPoint;
	//notchOutlineProfileCurve->EvaluatePoint(tParam+0.01, plusPoint);
	//notchOutlineProfileCurve->EvaluatePoint(tParam-0.01, minusPoint);
	//IwVector3d tempVec = plusPoint - minusPoint;
	//if ( tempVec.Dot(wslAxes.GetXAxis()) > 0)
	//{
	//	if ( positiveSideIsLateral )
	//	{
	//		notchOutlineProfileCurve->CreateBySplitCurve(pDoc->GetIwContext(), tParam, splitCurve);
	//		lNotchOutlineProfileCurve = (IwBSplineCurve*)splitCurve;
	//		mNotchOutlineProfileCurve = notchOutlineProfileCurve;
	//	}
	//	else
	//	{
	//		notchOutlineProfileCurve->CreateBySplitCurve(pDoc->GetIwContext(), tParam, splitCurve);
	//		mNotchOutlineProfileCurve = (IwBSplineCurve*)splitCurve;
	//		lNotchOutlineProfileCurve = notchOutlineProfileCurve;
	//	}
	//}
	//else
	//{
	//	if ( positiveSideIsLateral )
	//	{
	//		notchOutlineProfileCurve->CreateBySplitCurve(pDoc->GetIwContext(), tParam, splitCurve);
	//		mNotchOutlineProfileCurve = (IwBSplineCurve*)splitCurve;
	//		lNotchOutlineProfileCurve = notchOutlineProfileCurve;
	//	}
	//	else
	//	{
	//		notchOutlineProfileCurve->CreateBySplitCurve(pDoc->GetIwContext(), tParam, splitCurve);
	//		lNotchOutlineProfileCurve = (IwBSplineCurve*)splitCurve;
	//		mNotchOutlineProfileCurve = notchOutlineProfileCurve;
	//	}
	//}

	//notchOutlineProfileCurves.Add(mNotchOutlineProfileCurve);
	//notchOutlineProfileCurves.Add(lNotchOutlineProfileCurve);

	//notchOutlineProfileFeaturePoints.Add(notchTipPoint);
	//notchOutlineProfileFeaturePoints.Add(mEighteenWidthPoint);
	//notchOutlineProfileFeaturePoints.Add(lEighteenWidthPoint);

	//if ( ttCurve ) IwObjDelete(ttCurve);
	//if ( eighteenWidthCurve ) IwObjDelete(eighteenWidthCurve);
	//if ( Curve6 ) IwObjDelete(Curve6);
	//if ( Curve13 ) IwObjDelete(Curve13);

	return true;
}

IwBSplineCurve* CDefineOutlineProfile::DetermineSketchProfile(CTotalDoc* pDoc)
{
	IwContext& iwContext = pDoc->GetIwContext();
	IwBSplineCurve* sketchProfile=NULL;

	// Get femoral axes info
	CFemoralAxes* femoralAxes = pDoc->GetFemoralAxes();
	IwAxis2Placement wslAxes;
	femoralAxes->GetWhiteSideLineAxes(wslAxes);
	double refSize = femoralAxes->GetEpiCondyleSize();

	IwTArray<IwPoint3d>		origins;
	IwTArray<IwVector3d>	xAxes;
	IwTArray<IwVector3d>	yAxes;
	IwTArray<IwExtent2d>	domains;

	CFemoralCuts*		femoralCuts = pDoc->GetFemoralCuts();
	if (femoralCuts == NULL) return NULL;

	IwTArray<IwPoint3d> cutAntPostInfo;
	femoralCuts->GetAntPostInfo(cutAntPostInfo);

	IwTArray<IwPoint3d> points;
	IwTArray<IwVector3d> vectors;
	IwPoint3d antPoint, antTipPoint, postPoint, postTipPoint;
	IwVector3d antVector, postVector;

	if ( cutAntPostInfo.GetSize() == 0 )
	{
		// There is no AntPostInfo when initially initialized from OnInitializeJOCSteps().
		// We need to derive such info here.
		IwPoint3d antCutPoint;
		IwVector3d antCurOrientation;
		pDoc->GetFemoralAxes()->GetAntCutPlaneInfo(antCutPoint, antCurOrientation);
		IwTArray<IwPoint3d> cutEdgePoints;
		pDoc->GetFemoralCuts()->GetInitialCutEdgePoints(cutEdgePoints);

		// get the last fanning plane origin
		IwVector3d tempVec = origins.GetAt(19) - cutEdgePoints.GetAt(0);
		double zDist = fabs(tempVec.Dot(wslAxes.GetZAxis()));
		antPoint = cutEdgePoints.GetAt(0).ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		antVector = wslAxes.GetXAxis()*antCurOrientation;
		antTipPoint = antPoint - zDist*antVector;
		postTipPoint = cutEdgePoints.GetAt(6).ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		postPoint = cutEdgePoints.GetAt(5).ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		postVector = wslAxes.GetZAxis();
	}
	else
	{
		antTipPoint = cutAntPostInfo.GetAt(0);
		antPoint = cutAntPostInfo.GetAt(2);
		antVector = cutAntPostInfo.GetAt(3);
		postPoint = cutAntPostInfo.GetAt(4);
		postVector = cutAntPostInfo.GetAt(5);
		postTipPoint = cutAntPostInfo.GetAt(6);
	}


	// move to anterior and posterior a little bit.
	antPoint = antPoint - 2.5*antVector;
	postPoint = postPoint + 2.5*postVector;
	points.Add(antPoint);
	points.Add(postPoint);
	vectors.Add(antVector);
	vectors.Add(postVector);
	// Fair the vectors
	IwBSplineCurve::FairBlendCurveDerivatives(points, vectors); // This function only handles 2 points/vectors
	// Determine the angle between antVector and postVector
	double angle, angleDeg;
	antVector.AngleBetween(-postVector, angle);
	angleDeg = angle*180/IW_PI;
	// Determine the height difference
	double height = (antPoint - postPoint).Dot(wslAxes.GetZAxis());
	double heightRatio = height/refSize;
	// Determine the best parameters to fair sketch profile
	double scaleAnt = 0.65; // Scale 0.65 seems have best results for regular cases
	double scalePost = 0.55; // Scale 0.55 seems have best results for regular cases
	// Proportional adjust scaleAnt such that 
	// scaleAnt=0.65 when heightRatio=0.1
	// scaleAnt=0.85 when heightRatio=0.2
	if ( heightRatio > 0.1 )
		scaleAnt = 0.65 + 2.0*(heightRatio-0.1);

	IwTArray<IwVector3d> scaleVectors;
	scaleVectors.Add(scaleAnt*vectors.GetAt(0));//psv---
	scaleVectors.Add(scalePost*vectors.GetAt(1));//psv---

	IwBSplineCurve::CreateInterpolatingCurve(iwContext, IW_CP_CHORDLENGTH, 3, 3, points, scaleVectors, NULL, FALSE, sketchProfile);
	IwBSplineCurve *extendedCurveStart, *extendedCurveEnd;

	CAdvancedControl* advCtrl = pDoc->GetAdvancedControl();
	double additionalAntExt = advCtrl->GetOutlineProfileSketchSurfaceAntExtension();
	double additionalPostExt = advCtrl->GetOutlineProfileSketchSurfacePostExtension();
	double antDist = antTipPoint.DistanceBetween(antPoint) + 15.0;
	if (antDist < 40 ) antDist = 40.0;
	double postDist = postTipPoint.DistanceBetween(postPoint) + 5.0;
	if ( postDist < 30 ) postDist = 30.0;
	sketchProfile->CreateExtendedCurve(iwContext, antDist + additionalAntExt, 1, IW_CT_G1, extendedCurveStart);
	extendedCurveStart->CreateExtendedCurve(iwContext, postDist + additionalPostExt, 2, IW_CT_G1, extendedCurveEnd);


	return extendedCurveEnd;
}

bool CDefineOutlineProfile::DetermineSketchSurface
(
	CTotalDoc* pDoc,
	IwBSplineSurface*&	sketchSurface
)
{
	IwContext& iwContext = pDoc->GetIwContext();
	sketchSurface = NULL;

	// Get femoral axes information
	CFemoralAxes*		femoralAxes = pDoc->GetFemoralAxes();
	if (femoralAxes==NULL) return NULL;

	IwAxis2Placement axes;
	femoralAxes->GetSweepAxes(axes);
	double refSize = femoralAxes->GetEpiCondyleSize();

	IwBSplineCurve* sketchProfile = CDefineOutlineProfile::DetermineSketchProfile(pDoc);
	if (sketchProfile == NULL) return false;

	// Translate to negative xAxis with 0.6*refSize distance
	IwAxis2Placement trans(-0.6*refSize*axes.GetXAxis(), IwVector3d(1,0,0), IwVector3d(0,1,0));
	sketchProfile->Transform(trans);

	//ShowCurve(pDoc, sketchProfile, red);//PSV

	IwBSplineSurface* sweepSurface;
	IwBSplineSurface::CreateLinearSweep(iwContext, *sketchProfile, 1.2*refSize*axes.GetXAxis(), sweepSurface);

	if (sweepSurface == NULL) return false;

	sweepSurface->ReparametrizeWithArcLength();
	sketchSurface = sweepSurface;

	return true;
}
#pragma endregion
