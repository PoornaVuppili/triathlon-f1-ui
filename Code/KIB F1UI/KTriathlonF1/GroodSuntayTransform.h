#ifndef GROODSUNTAYTRANSFORM_H
#define GROODSUNTAYTRANSFORM_H

class IwAxis2Placement;

class GroodSuntayTransform
{
public:
	GroodSuntayTransform(double flexRadian, double abductionRadian, double rotationRadian);//femur wrt tibia (in col vector): I/E Rotate * (Abduct * Flex)^(-1).
	static void GetGroodSuntayParameters(IwAxis2Placement const& mechanicalAxes, IwAxis2Placement const& xform, double& flexRadian, double& abductionRadian, double& rotationRadian, double translate[3]);

private:
	double m_xVec[3];
	double m_yVec[3];
	double m_zVec[3];
	double m_origin[3];
};

#endif