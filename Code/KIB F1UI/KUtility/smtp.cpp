#include <qdatetime.h>
#include "KUtility.h"
#include "smtp.h"

Smtp::Smtp() 
{
    socket = new QTcpSocket( this );

    connect( socket, SIGNAL( readyRead() ), this, SLOT( readyRead() ) );
    connect( socket, SIGNAL( connected() ), this, SLOT( connected() ) );
    connect( socket, SIGNAL(error(QAbstractSocket::SocketError)), this, 
		SLOT(errorReceived(QAbstractSocket::SocketError)));   
    connect( socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, 
		SLOT(stateChanged(QAbstractSocket::SocketState)));       
    connect(socket, SIGNAL(disconnectFromHost()), this, 
		SLOT(disconnected()));
}
Smtp::~Smtp()
{
    delete t;
    delete socket;
}

void Smtp::send(const QString &from, const QString &to, const QString &onServer,
					const QString &subject, const QString &body)
{
	QString header;
	header = "Date: " + QDateTime::currentDateTime().toString("dd.MM.yyyy, hh:mm:ss \r\n");

	header += "From: " + CurrentUserName() +
				"<" + CurrentUserName() + "@Stryker.com>\r\n";
	header += "Reply-To: " + CurrentUserName() + "<" + CurrentUserName() + "@Stryker.com>\r\n";
	header += "To: " + to + "\r\n";
	header += "Subject: " + subject + "\r\n";
	header += "Message-ID: <SegSurfMessage." + QDateTime::currentDateTime().toString() + ">\r\n";
	header +=  "Content-Type: text/plain; charset=us-ascii\r\nContent-Transfer-Encoding: 7bit\r\n";
	header += "\r\n";
 	message.append(body);
	message.replace( QString::fromLatin1( "\n" ), QString::fromLatin1( "\r\n" ) );
	message.replace( QString::fromLatin1( "\r\n.\r\n" ), QString::fromLatin1( "\r\n..\r\n" ) );

	message = header + message;

    this->from = from;
    rcpt = to;

    state = Init;
    socket->connectToHost( onServer, 25);
    if(socket->waitForConnected ( 30000 ))
	{
		t = new QTextStream( socket );
	}
	else
	{
		QMessageBox::warning( 0, "SegSurf Mail", "Failed to connect to mail server.");
		deleteLater();
	}
}

void Smtp::stateChanged(QTcpSocket::SocketState socketState)
{
    qDebug() <<"stateChanged " << socketState;
}

void Smtp::errorReceived(QTcpSocket::SocketError socketError)
{
    qDebug() << "error " <<socketError;
}

void Smtp::disconnected()
{
    qDebug() <<"disconnected";
    qDebug() << "error "  << socket->errorString();
}

void Smtp::connected()
{
    qDebug() << "Connected ";
}

void Smtp::readyRead()
{
     qDebug() <<"readyRead";
    // SMTP is line-oriented

    QString responseLine;
    do
    {
        responseLine = socket->readLine();
        response += responseLine;
    }
    while ( socket->canReadLine() && responseLine[3] != ' ' );

	responseLine.truncate( 3 );

    if ( state == Init && responseLine[0] == '2' )
    {
        // banner was okay, let's go on
		*t << "HELO there\r\n";
		t->flush();
		
        state = Mail;
		//state = Auth;
    }
	else if(state == Auth && responseLine[0] == '2')
	{
		*t << "AUTH LOGIN\r\n";
        t->flush();

		state = AuthUser;
	}
	else if(state == AuthUser && responseLine[0] == '3')
	{
		*t << rcpt << "\r\n";
		t->flush();

		state = AuthPassword;
	}
	else if(state == AuthPassword && responseLine[0] == '3')
	{
		//*t << password << "\r\n";
		t->flush();

		state = Mail;
	}
    else if ( state == Mail && responseLine[0] == '2' )
    {
        *t << "MAIL FROM: " << from << "\r\n";
		t->flush();
        state = Rcpt;
    }
    else if ( state == Rcpt && responseLine[0] == '2' )
    {
        *t << "RCPT TO: " << rcpt << "\r\n"; //r
		t->flush();
        state = Data;
    }
    else if ( state == Data && responseLine[0] == '2' )
    {

        *t << "DATA\r\n";
		t->flush();
        state = Body;
    }
    else if ( state == Body && responseLine[0] == '3' )
    {
        *t << message << "\r\n.\r\n";
		t->flush();
        state = Quit;
    }
    else if ( state == Quit && responseLine[0] == '2' )
    {
        *t << "QUIT\r\n";
		t->flush();
        // here, we just close.
        state = Close;
        emit status( tr( "Message sent" ) );
    }
    else if ( state == Close )
    {
        deleteLater();
        return;
    }
    else
    {
        // something broke.
        QMessageBox::warning( 0, "SegSurf Mail", "Unexpected reply from SMTP server:\n\n" + response );
        state = Close;
    }
    response = "";
}