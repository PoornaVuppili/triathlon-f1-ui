#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Implant.h"
#include "EntityWidget.h"
#include "ViewWidget.h"
#include "ViewportControlDialog.h"
#include "OptionDialog.h"
#include "OptionGeneral.h"
#include "CloseImplantEvent.h"
#include "..\KUtility\KUtility.h"
#include "../KUtility/KApplicationMode.h"

#include <assert.h>
#include <QProgressBar>
#include <QDockWidget>
#include <QSplitter>
#include <QMessageBox>
#include <QUndoStack>
#include <HwIges.h>
#include <QFileDialog>
#include <QSettings>
#include <QDate>
#include <QColorDialog>
#include <qcombobox.h>
#include <qtoolbutton.h>
#include <qactiongroup.h>
#include <qtextedit.h>
#include "..\KUtility\CoordinateSystem.h"
#include "../KApp/SwitchBoard.h"

// global instance
MainWindow* mainWindow = NULL;

QString const strReviewMode(" : REVIEW MODE");

class QTextEditEx : public QTextEdit
{
public:
	QTextEditEx(QWidget* parent=0) : QTextEdit(parent) {}

	virtual QSize sizeHint() const {
		return QSize(100,20);
	}
};

///////////////////////////////////////////////////////////////////////
// Function name:    MainWindow
// Function purpose: Constructor
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
MainWindow::MainWindow(bool reviewmode, QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
    , ui(nullptr)
    , undoStack(nullptr)
    , menuFile(nullptr)
    , menuHelp(nullptr)
    , actOpenImplant(nullptr)
    , actSaveImplant(nullptr)
    , actCloseImplant(nullptr)
    , actScreenCapture(nullptr)
    , actReviewerFeedback(nullptr)
    , menuEdit(nullptr)
    , actUndo(nullptr)
    , actRedo(nullptr)
    , menuView(nullptr)
    , actViewport(nullptr)
    , actZoomIn(nullptr)
    , actZoomOut(nullptr)
    , actZoomFit(nullptr)
    , actRefresh(nullptr)
    , menuTool(nullptr)
    , menuMeasure(nullptr)
    , measureGroup(nullptr)
    , actOption(nullptr)
    , actMeasureEntityDistance(nullptr)
    , actMeasureEntityAngle(nullptr)
    , actMeasureEntityRadius(nullptr)
    , actMeasureScreenDistance(nullptr)
    , actMeasureScreenAngle(nullptr)
    , actMeasureScreenRadius(nullptr)
    , wndToolbar(nullptr)
    , editToolbar(nullptr)
    , toolMeasure(nullptr)
    , dockWidget(nullptr)
    , splitter(nullptr)
    , progress(nullptr)
    , implant(nullptr)
    , appReviewMode(reviewmode)
    , caseReviewMode(false)
{
    ui = new Ui::MainWindowClass();
    ui->setupUi(this);

    mainWindow = this;
    editToolbar = NULL;

	//Make sure the Qt's publish/subscribe mechanism handles properly this type.
	qRegisterMetaType<CoordSystem>("CoordSystem");

    splitter = new QSplitter(Qt::Vertical);
    entityWidget = new EntityWidget(splitter);
    splitter->addWidget(entityWidget);
    splitter->setStretchFactor(0,1);

    viewWidget = new ViewWidget(this);
	
    undoStack = NULL;
    implant = NULL;

    //setWindowIcon(QIcon(":/Resources/Stryker.png"));

    // set the default custom colors
    QColorDialog::setCustomColor(0, qRgb(192,64,64));   // brep iges surface
    QColorDialog::setCustomColor(1, qRgb(64,64,192));   // poly brep mesh color
    QColorDialog::setCustomColor(2, qRgb(64,192,64));   // subdiv mesh color
    QColorDialog::setCustomColor(3, qRgb(224,128,128)); // subdiv control color

    // set up the base menu bar, tool bar, central widget, dock widget & status bar
    SetupMenuBar();
    SetupToolBar();
    SetupCentralWidget();
    SetupDockWidget();
    SetupStatusBar();

    UpdateActions(false);
}

///////////////////////////////////////////////////////////////////////
// Function name:    ~MainWindow
// Function purpose: Destructor
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
MainWindow::~MainWindow()
{
    // delete the implant
    DeletePtr(implant);

    ClearContext();

    delete ui;
}

///////////////////////////////////////////////////////////////////////
// Function name:    SetupMenuBar
// Function purpose: Set up the menu bar
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void MainWindow::SetupMenuBar()
{
    AddFileMenu();
    AddEditMenu();
    AddViewMenu();
    AddToolMenu();
    AddHelpMenu();
}

void MainWindow::AddFileMenu()
{
    int s = 16;

    // File menu
    // - open
    QIcon iconOpen;
    iconOpen.addFile(":/Resources/Open.png", QSize(s,s), QIcon::Normal);
    iconOpen.addFile(":/Resources/Open_h.png", QSize(s,s), QIcon::Active);
    iconOpen.addFile(":/Resources/Open_d.png", QSize(s,s), QIcon::Disabled);
    iconOpen.addFile(":/Resources/Open_h.png", QSize(s,s), QIcon::Selected);

    actOpenImplant = new QAction(iconOpen, "&Open implant...", this);
    actOpenImplant->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_O) );
    actOpenImplant->setToolTip("Open an implant");
    actOpenImplant->setStatusTip("Open an existing implant");

    // - save
    QIcon iconSave;
    iconSave.addFile(":/Resources/Save.png", QSize(s,s), QIcon::Normal);
    iconSave.addFile(":/Resources/Save_h.png", QSize(s,s), QIcon::Active);
    iconSave.addFile(":/Resources/Save_d.png", QSize(s,s), QIcon::Disabled);
    iconSave.addFile(":/Resources/Save_h.png", QSize(s,s), QIcon::Selected);

    actSaveImplant = new QAction(iconSave, "&Save implant", this);
    actSaveImplant->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_S) );
    actSaveImplant->setToolTip("Save the implant");
    actSaveImplant->setStatusTip("Save the implant");

    // - close
    actCloseImplant = new QAction("&Close implant", this);
    actCloseImplant->setToolTip("Close the implant");
    actCloseImplant->setStatusTip("Close the implant");

    // - capture
    QIcon iconSnap;
    iconSnap.addFile(":/Resources/Snapshot.png", QSize(s,s), QIcon::Normal);
    iconSnap.addFile(":/Resources/Snapshot_h.png", QSize(s,s), QIcon::Active);
    iconSnap.addFile(":/Resources/Snapshot_d.png", QSize(s,s), QIcon::Disabled);
    iconSnap.addFile(":/Resources/Snapshot_h.png", QSize(s,s), QIcon::Selected);

    actScreenCapture  = new QAction(iconSnap, "Capture screen...", this);
    actScreenCapture->setToolTip("Screen capture");
    actScreenCapture->setStatusTip("Capture the screen as an image file");

    menuFile = menuBar()->addMenu("&File");
    menuFile->addAction(actOpenImplant);
    menuFile->addAction(actSaveImplant);
    menuFile->addAction(actCloseImplant);
    menuFile->addSeparator();
    menuFile->addAction(actScreenCapture);
    menuFile->addSeparator();
    menuFile->addAction("&Quit", this, SLOT(close()));

    connect(actOpenImplant, SIGNAL(triggered()), this, SLOT(OpenImplant()));
    connect(actSaveImplant, SIGNAL(triggered()), this, SLOT(SaveImplant()));
    connect(actCloseImplant, SIGNAL(triggered()), this, SLOT(CloseImplant()));
    connect(actScreenCapture, SIGNAL(triggered()), this, SLOT(CaptureScreen()));
}

void MainWindow::AddEditMenu()
{
    int s = 16;

    // - undo
    QIcon iconUndo;
    iconUndo.addFile(":/Resources/Undo.png", QSize(s,s), QIcon::Normal);
    iconUndo.addFile(":/Resources/Undo_h.png", QSize(s,s), QIcon::Active);
    iconUndo.addFile(":/Resources/Undo_d.png", QSize(s,s), QIcon::Disabled);
    iconUndo.addFile(":/Resources/Undo_h.png", QSize(s,s), QIcon::Selected);
    actUndo = new QAction(iconUndo, "Undo", this);
    actUndo->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_Z) );
    actUndo->setToolTip("Undo");

    // - redo
    QIcon iconRedo;
    iconRedo.addFile(":/Resources/Redo.png", QSize(s,s), QIcon::Normal);
    iconRedo.addFile(":/Resources/Redo_h.png", QSize(s,s), QIcon::Active);
    iconRedo.addFile(":/Resources/Redo_d.png", QSize(s,s), QIcon::Disabled);
    iconRedo.addFile(":/Resources/Redo_h.png", QSize(s,s), QIcon::Selected);
    actRedo = new QAction(iconRedo, "Redo", this);
    actRedo->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_Y) );
    actRedo->setToolTip("Redo");

    menuEdit = menuBar()->addMenu("&Edit");
    menuEdit->addAction(actUndo);
    menuEdit->addAction(actRedo);

    actUndo->setEnabled(false);
    actRedo->setEnabled(false);
    connect(actUndo, SIGNAL(triggered()), this, SLOT(Undo()));
    connect(actRedo, SIGNAL(triggered()), this, SLOT(Redo()));
}

void MainWindow::AddViewMenu()
{
    int s = 16;

    // -viewport
    QIcon iconViewport;
    iconViewport.addFile(":/Resources/viewport.png", QSize(s,s), QIcon::Normal);
    iconViewport.addFile(":/Resources/viewport.png", QSize(s,s), QIcon::Active);
    iconViewport.addFile(":/Resources/viewport_d.png", QSize(s,s), QIcon::Disabled);
    iconViewport.addFile(":/Resources/viewport.png", QSize(s,s), QIcon::Selected);

    actViewport = new QAction(iconViewport, "Viewport", this);
    actViewport->setToolTip("Viewport");

    // - zoom in
    QIcon iconZoomIn;
    iconZoomIn.addFile(":/Resources/Zoom_in.png", QSize(s,s), QIcon::Normal);
    iconZoomIn.addFile(":/Resources/Zoom_in_h.png", QSize(s,s), QIcon::Active);
    iconZoomIn.addFile(":/Resources/Zoom_in_d.png", QSize(s,s), QIcon::Disabled);
    iconZoomIn.addFile(":/Resources/Zoom_in_h.png", QSize(s,s), QIcon::Selected);

    actZoomIn = new QAction(iconZoomIn, "Zoom In", this);
    QString tipStr = "Zoom in";
    actZoomIn->setToolTip(tipStr);
    actZoomIn->setStatusTip(tipStr);
    
    // - zoom out
    QIcon iconZoomOut;
    iconZoomOut.addFile(":/Resources/Zoom_out.png", QSize(s,s), QIcon::Normal);
    iconZoomOut.addFile(":/Resources/Zoom_out_h.png", QSize(s,s), QIcon::Active);
    iconZoomOut.addFile(":/Resources/Zoom_out_d.png", QSize(s,s), QIcon::Disabled);
    iconZoomOut.addFile(":/Resources/Zoom_out_h.png", QSize(s,s), QIcon::Selected);

    actZoomOut = new QAction(iconZoomOut, "Zoom Out", this);
    tipStr = "Zoom out";
    actZoomOut->setToolTip(tipStr);
    actZoomOut->setStatusTip(tipStr);

    QIcon iconZoomFit;
    iconZoomFit.addFile(":/Resources/Zoom_Fit.png", QSize(s,s), QIcon::Normal);
    iconZoomFit.addFile(":/Resources/Zoom_Fit_h.png", QSize(s,s), QIcon::Active);
    iconZoomFit.addFile(":/Resources/Zoom_Fit_d.png", QSize(s,s), QIcon::Disabled);
    iconZoomFit.addFile(":/Resources/Zoom_Fit_h.png", QSize(s,s), QIcon::Selected);

    // - best fit
    actZoomFit = new QAction(iconZoomFit, "Best Fit", this);
    tipStr = "Best fit";
    actZoomFit->setToolTip(tipStr);
    actZoomFit->setStatusTip(tipStr);
    
    // - refresh
    QIcon iconRefresh;
    iconRefresh.addFile(":/Resources/Refresh.png", QSize(s,s), QIcon::Normal);
    iconRefresh.addFile(":/Resources/Refresh_h.png", QSize(s,s), QIcon::Active);
    iconRefresh.addFile(":/Resources/Refresh_d.png", QSize(s,s), QIcon::Disabled);
    iconRefresh.addFile(":/Resources/Refresh_h.png", QSize(s,s), QIcon::Selected);

    actRefresh = new QAction(iconRefresh, "Refresh", this);
    tipStr = "Refresh the display";
    actRefresh->setToolTip(tipStr);
    actRefresh->setStatusTip(tipStr);
    
    menuView = menuBar()->addMenu("&View");
    menuView->addAction(actViewport);
    menuView->addAction(actZoomIn);
    menuView->addAction(actZoomOut);
    menuView->addAction(actZoomFit);
    menuView->addAction(actRefresh);

    connect(actViewport, SIGNAL(triggered()), this, SLOT(ControlViewport()));
    connect(actZoomIn, SIGNAL(triggered()), viewWidget, SLOT(ZoomIn()));
    connect(actZoomOut, SIGNAL(triggered()), viewWidget, SLOT(ZoomOut()));
    connect(actZoomFit, SIGNAL(triggered()), viewWidget, SLOT(ZoomFit()));
    connect(actRefresh, SIGNAL(triggered()), viewWidget, SLOT(updateGL()));
}

void MainWindow::AddToolMenu()
{
    int s = 16;

    menuTool = menuBar()->addMenu("&Tools");

    // options
    QIcon iconOption;
    iconOption.addFile(":/Resources/Configurator.png", QSize(s,s), QIcon::Normal);
    iconOption.addFile(":/Resources/Configurator_h.png", QSize(s,s), QIcon::Active);
    iconOption.addFile(":/Resources/Configurator_d.png", QSize(s,s), QIcon::Disabled);
    iconOption.addFile(":/Resources/Configurator_h.png", QSize(s,s), QIcon::Selected);

    actOption  = menuTool->addAction(iconOption, "Options...");
    actOption->setToolTip("Option");
    actOption->setStatusTip("Set the software options");

    // measurement sub-menu
    menuMeasure = menuTool->addMenu("Measure");
	//menuMeasure->setDisabled(true); 

    measureGroup = new QActionGroup(menuMeasure);
    measureGroup->setExclusive(false);

    QIcon iconMeasureEntityDist;
    iconMeasureEntityDist.addFile(":/Resources/Ruler_Dist.png", QSize(s,s), QIcon::Normal);
    iconMeasureEntityDist.addFile(":/Resources/Ruler_Dist_h.png", QSize(s,s), QIcon::Active);
    iconMeasureEntityDist.addFile(":/Resources/Ruler_Dist_d.png", QSize(s,s), QIcon::Disabled);
    iconMeasureEntityDist.addFile(":/Resources/Ruler_Dist_h.png", QSize(s,s), QIcon::Selected);

    actMeasureEntityDistance = measureGroup->addAction(iconMeasureEntityDist, "Measure Entity Distance");
    actMeasureEntityDistance->setCheckable(true);
    actMeasureEntityDistance->setChecked(false);
    actMeasureEntityDistance->setToolTip("Entity distance");
	actMeasureEntityDistance->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_D) ); 

    QIcon iconMeasureEntityAngle;
    iconMeasureEntityAngle.addFile(":/Resources/Ruler_Ang.png", QSize(s,s), QIcon::Normal);
    iconMeasureEntityAngle.addFile(":/Resources/Ruler_Ang_h.png", QSize(s,s), QIcon::Active);
    iconMeasureEntityAngle.addFile(":/Resources/Ruler_Ang_d.png", QSize(s,s), QIcon::Disabled);
    iconMeasureEntityAngle.addFile(":/Resources/Ruler_Ang_h.png", QSize(s,s), QIcon::Selected);

	actMeasureEntityAngle		= measureGroup->addAction(iconMeasureEntityAngle, "Measure Entity Angle" );
    actMeasureEntityAngle->setCheckable(true);
    actMeasureEntityAngle->setChecked(false);
    actMeasureEntityAngle->setToolTip("Entity angle");
	actMeasureEntityAngle->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_A) ); 

    QIcon iconMeasureEntityRadius;
    iconMeasureEntityRadius.addFile(":/Resources/Ruler_Radius.png", QSize(s,s), QIcon::Normal);
    iconMeasureEntityRadius.addFile(":/Resources/Ruler_Radius_h.png", QSize(s,s), QIcon::Active);
    iconMeasureEntityRadius.addFile(":/Resources/Ruler_Radius_d.png", QSize(s,s), QIcon::Disabled);
    iconMeasureEntityRadius.addFile(":/Resources/Ruler_Radius_h.png", QSize(s,s), QIcon::Selected);

	actMeasureEntityRadius		= measureGroup->addAction(iconMeasureEntityRadius, "Measure Entity Radius" );
    actMeasureEntityRadius->setCheckable(true);
    actMeasureEntityRadius->setChecked(false);
    actMeasureEntityRadius->setToolTip("Entity radius");
	actMeasureEntityRadius->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_R) ); 

    measureGroup->addAction("")->setSeparator(true);

    QIcon iconMeasureScreenDist;
    iconMeasureScreenDist.addFile(":/Resources/RulerScreen_Dist.png", QSize(s,s), QIcon::Normal);
    iconMeasureScreenDist.addFile(":/Resources/RulerScreen_Dist.png", QSize(s,s), QIcon::Active);
    iconMeasureScreenDist.addFile(":/Resources/RulerScreen_Dist_d.png", QSize(s,s), QIcon::Disabled);
    iconMeasureScreenDist.addFile(":/Resources/RulerScreen_Dist.png", QSize(s,s), QIcon::Selected);

	actMeasureScreenDistance= measureGroup->addAction(iconMeasureScreenDist, "Measure Screen Distance");
    actMeasureScreenDistance->setCheckable(true);
    actMeasureScreenDistance->setChecked(false);
    actMeasureScreenDistance->setToolTip("Screen distance");
	actMeasureScreenDistance->setShortcut( QKeySequence(Qt::SHIFT + Qt::Key_D) ); 

    QIcon iconMeasureScreenAng;
    iconMeasureScreenAng.addFile(":/Resources/RulerScreen_Ang.png", QSize(s,s), QIcon::Normal);
    iconMeasureScreenAng.addFile(":/Resources/RulerScreen_Ang.png", QSize(s,s), QIcon::Active);
    iconMeasureScreenAng.addFile(":/Resources/RulerScreen_Ang_d.png", QSize(s,s), QIcon::Disabled);
    iconMeasureScreenAng.addFile(":/Resources/RulerScreen_Ang.png", QSize(s,s), QIcon::Selected);

	actMeasureScreenAngle	= measureGroup->addAction(iconMeasureScreenAng, "Measure Screen Angle" );
    actMeasureScreenAngle->setCheckable(true);
    actMeasureScreenAngle->setChecked(false);
    actMeasureScreenAngle->setToolTip("Screen angle");
	actMeasureScreenAngle->setShortcut( QKeySequence(Qt::SHIFT + Qt::Key_A) ); 

    QIcon iconMeasureScreenRadius;
    iconMeasureScreenRadius.addFile(":/Resources/RulerScreen_Radius.png", QSize(s,s), QIcon::Normal);
    iconMeasureScreenRadius.addFile(":/Resources/RulerScreen_Radius.png", QSize(s,s), QIcon::Active);
    iconMeasureScreenRadius.addFile(":/Resources/RulerScreen_Radius_d.png", QSize(s,s), QIcon::Disabled);
    iconMeasureScreenRadius.addFile(":/Resources/RulerScreen_Radius.png", QSize(s,s), QIcon::Selected);

	actMeasureScreenRadius	= measureGroup->addAction(iconMeasureScreenRadius, "Measure Screen Radius" );
    actMeasureScreenRadius->setCheckable(true);
    actMeasureScreenRadius->setChecked(false);
    actMeasureScreenRadius->setToolTip("Screen radius");
	actMeasureScreenRadius->setShortcut( QKeySequence(Qt::SHIFT + Qt::Key_R) ); 

    menuMeasure->addActions(measureGroup->actions());

    connect(actOption, SIGNAL(triggered()), this, SLOT(SetOption()));
    connect(measureGroup, SIGNAL(triggered(QAction*)), this, SLOT(TriggerMeasure(QAction*)));
}

void MainWindow::AddHelpMenu()
{
    menuHelp = menuBar()->addMenu("&Help");
    //menuHelp->addAction("&Context", this, SLOT(ContextHelp()), QKeySequence(Qt::CTRL + Qt::Key_F1));
    actReviewerFeedback = menuHelp->addAction("&Reviewer feedback...", this, SLOT(ReviewerFeedback()));
    actReviewerFeedback->setEnabled(ReviewerFeedbackAvailable());
    menuHelp->addSeparator();
    menuHelp->addAction("&About", this, SLOT(About()), Qt::Key_F1);
}

///////////////////////////////////////////////////////////////////////
// Function name:    SetupToolBar
// Function purpose: Set up the tool bar
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void MainWindow::SetupToolBar()
{
    ui->mainToolBar->setWindowTitle("File");
    ui->mainToolBar->addAction(actOpenImplant);
    ui->mainToolBar->addAction(actSaveImplant);
    ui->mainToolBar->addAction(actScreenCapture);
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addAction(actUndo);
    ui->mainToolBar->addAction(actRedo);

    wndToolbar = new QToolBar("Viewport",this);
    addToolBar( wndToolbar );

    wndToolbar->addAction(actViewport);
    //wndToolbar->addAction(actZoomIn);// Not need it. Mouse middle wheel is already the short-key
    //wndToolbar->addAction(actZoomOut);// Not need it. Mouse middle wheel is already the short-key
    wndToolbar->addAction(actZoomFit);
    wndToolbar->addAction(actRefresh);

    toolMeasure = new QToolButton();
    toolMeasure->setCheckable(true);
    toolMeasure->setDefaultAction(actMeasureEntityDistance);
    toolMeasure->setMenu( menuMeasure );
    wndToolbar->addWidget(toolMeasure);
}

///////////////////////////////////////////////////////////////////////
// Function name:    SetupCentralWidget
// Function purpose: Set up the centrol widget
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void MainWindow::SetupCentralWidget()
{
    setCentralWidget(viewWidget);
}

///////////////////////////////////////////////////////////////////////
// Function name:    SetupStatusBar
// Function purpose: Set up the status bar
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void MainWindow::SetupStatusBar()
{
    progress = new QProgressBar;
    progress->setHidden(true);
    statusBar()->addPermanentWidget(progress, 0);

    kStatus.SetStatusBar( statusBar() );
    kStatus.SetProgressBar( progress );
    kStatus.ShowMessage("Ready");
}

///////////////////////////////////////////////////////////////////////
// Function name:    SetupDockWidget
// Function purpose: Set up the dock widgets. Two docks are created, one
//   on the left, and on the right; and both are empty. They are updated
//   based on the content of the central widget.
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void MainWindow::SetupDockWidget()
{
    setDockOptions(QMainWindow::AnimatedDocks);
	setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
	setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    QSettings settings;

    dockWidget = new QDockWidget();
    dockWidget->setFeatures( QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable );
	addDockWidget( (Qt::DockWidgetArea) settings.value("DockWidgetLocation", Qt::LeftDockWidgetArea).toInt(), dockWidget);
    dockWidget->setWidget(splitter);

    connect(dockWidget, SIGNAL(dockLocationChanged(Qt::DockWidgetArea)), this, SLOT(OnDockLocationChanged(Qt::DockWidgetArea)));
}

// Called when the dock location is changed
void MainWindow::OnDockLocationChanged(Qt::DockWidgetArea area)
{
	QSettings settings;
	QObject* s = sender();
	if( s==dockWidget )
		settings.setValue("DockWidgetLocation", area);
}

void MainWindow::UpdateUndoStack(QUndoStack* stack)
{
    if( undoStack )
    {
        disconnect(undoStack, SIGNAL(canUndoChanged(bool)), actUndo, SLOT(setEnabled(bool)));
        disconnect(undoStack, SIGNAL(canRedoChanged(bool)), actRedo, SLOT(setEnabled(bool)));
    }
    undoStack = stack;

    if( undoStack==NULL )
    {
        if (actUndo) actUndo->setEnabled(false);
        if (actRedo) actRedo->setEnabled(false);
    }
    else
    {
        if (actUndo)
        {
           actUndo->setEnabled( undoStack->canUndo() );
           connect(undoStack, SIGNAL(canUndoChanged(bool)), actUndo, SLOT(setEnabled(bool)));
        }
        if (actRedo)
        {
           actRedo->setEnabled( undoStack->canRedo() );
           connect(undoStack, SIGNAL(canRedoChanged(bool)), actRedo, SLOT(setEnabled(bool)));
        }
    }
}

// Undo/Redo
void MainWindow::Undo()
{
    if( undoStack)
    {
        undoStack->undo();
        entityWidget->update();
        viewWidget->update();
    }
}

void MainWindow::Redo()
{
    if( undoStack)
    {
        undoStack->redo();
        entityWidget->update();
        viewWidget->update();
    }
}

// Enable the editing with either a widget or toolbar
void MainWindow::SetEditControl(QWidget* w, bool enable)
{
    if( enable )
    {
        assert(w);

        if( splitter->count()==2 )
            splitter->widget(1)->setParent(NULL);

        splitter->addWidget(w);
    }
    //else
    //    assert( w==splitter->widget(1) );
        
    SetEditMode(enable);
}

void MainWindow::SetEditControl(QToolBar* b, bool enable)
{
	if( enable )
	{
		assert(b);

		editToolbar = b;
		addToolBar( editToolbar );
	}

	SetEditMode(enable);
}

///////////////////////////////////////////////////////////////////////
// Function name:    About
// Function purpose: Display the program information
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void MainWindow::About()
{
    QString msg = (GetAppName() + " " + GetAppVersion() + "\n%1 2008-%2 ConforMIS , Inc.").arg(QChar(169)).arg(QDate::currentDate().year());

    msg += "\n\n";

    msg += "Built: " + QString(__DATE__) + ' ' + QString(__TIME__) + "\n\n";

    //msg += implant->GetModuleVersionInfo().join("\n") + "\n";
    msg += QString("Qt version: ") + qVersion() + " (compiled with " + QT_VERSION_STR + ")\n";

#ifdef SM_VERSION_STRING         // since 8.6.17 we should expect this
    msg += "SMLib version: " + QString(SM_VERSION_STRING) + "\n";
#elif defined(SM_VERSION_NUMBER) // slightly older version of NMTLib defines this
    msg += "SMLib version: " + QString(SM_VERSION_NUMBER) + "\n";
#endif
    QString iwVersion = QString::number(IW_VERSION_NUMBER);
    msg += "IW version: " + iwVersion.mid(0,2) + "." + iwVersion.mid(2,2) + "\n";
    msg += "HW IGES version: " + QString(HW_IGES_VERSION) + "\n";
    //msg += "HW STEP version: " + QString(HW_STEP_VERSION) + "\n\n";
    msg += "\n";

    QString ext = (char*) glGetString(GL_EXTENSIONS);
    msg += "OpenGL vendor: " + QString((char*) glGetString(GL_VENDOR)) + "\n";
    msg += "OpenGL renderer: " + QString((char*) glGetString(GL_RENDERER)) + "\n";
    msg += "OpenGL version: " + QString((char*) glGetString(GL_VERSION)) + "\n";
#ifndef NDEBUG
    msg += "OpenGL rectangle texture extension: ";
    msg += (ext.indexOf("GL_ARB_texture_rectangle") != -1) ? "Yes" : "No";
    msg += "\n";
    msg += "OpenGL 3D texture extension: ";
    msg += (ext.indexOf("GL_EXT_texture3D") != -1) ? "Yes" : "No";
#endif

    // CPU info
    /*int CPUInfo[4] = {-1};
    __cpuid(CPUInfo, 1);
    int CPUfeature = CPUInfo[3];
    msg += "\n\n";
    msg += "CPU features: ";
    if( CPUfeature & (0x01 << 23) )
        msg += "MMX ";
    if( CPUfeature & (0x01 << 25) )
        msg += "SSE ";
    if( CPUfeature & (0x01 << 26) )
        msg += "SSE2 ";*/

	QMessageBox::about(this, GetAppName(), msg);
}

// this is a slot, it is activated when the user requests context help
// if the dialog doesn't exist, it shall be created and updated with the content
// also, if created for the first time, the dialog will be shown
// if the dialog is currently visible, it will be hidden
void MainWindow::ContextHelp()
{

}

void MainWindow::ContextHelp(QString const& helpContent)
{

}

bool MainWindow::ReviewerFeedbackAvailable() const
{
    if (implant)
    {
	    QString fileName = implant->GetInfo("FeedbackLoc");
        return QFile::exists(fileName) && !IsReviewMode();
    }
    return false;
}

void MainWindow::ReviewerFeedback()
{
    //
    QString serNum = implant->GetInfo("ImplantName");
	QString fileName = implant->GetInfo("ImplantDir") + "\\" + serNum + "_ReviewerFeedback.txt";
	if (QFile::exists(fileName) && !IsReviewMode())
	{
		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly/* | QIODevice::Text*/))
		{
            QByteArray ba(file.readAll());
            file.close();
            QString textUtf8 = QString::fromUtf8(ba.constData());
//			QString text(file.readAll());

			QMessageBox::information(this, "Reviewer Feedback", textUtf8);
		}
	}
}

void MainWindow::customEvent(QEvent *evt)
{
	switch(evt->type())
	{
	case CLOSE_IMPLANT_EVENT:
		CloseImplant(static_cast<CloseImplantEvent*>(evt)->GetShouldConfirm());
		break;
	};
}

void MainWindow::closeEvent( QCloseEvent * event )
{
    if( implant->HasImplant() )
    {
        CloseImplant();

        if( implant->HasImplant() )
        {
            // not close
            event->ignore();
            return;
        }
    }

    QMainWindow::closeEvent(event);
}

// Save an implant
void MainWindow::SaveImplant()
{
   if (!IsReviewMode())
      implant->SaveImplant();
}

// Close an implant
void MainWindow::CloseImplant(bool confirm)
{
    if( confirm && !IsReviewMode())
    {
          if (QMessageBox::information(this, "Confirmation", "Close all implant data?", QMessageBox::Yes | QMessageBox::No)
                  == QMessageBox::No )
            return;
    }

    SetEditMode(false);

    if( !implant->CloseImplant() )
        return;

    lock.Release();

    ClearContext();
    UpdateActions(false);
}

// Viewport control
void MainWindow::ControlViewport()
{
    ViewportControlDialog dlg(this);

    if( dlg.Init(viewWidget) )
        dlg.exec();
}

// Capture the screen
void MainWindow::CaptureScreen()
{
    QString filter;
    QString name = QFileDialog::getSaveFileName(this, "Capture the screen", implant->HasImplant() ? implant->GetInfo("ImplantDir") : ".", 
        "24-bit Bitmap (*.bmp);;JPEG File Interchange Format (*.jpg *.jpeg);;"
        "Portable Network Graphics Format (*.png);;", &filter);
	
	QImage image = viewWidget->grabFrameBuffer();
	if (filter == "24-bit Bitmap (*.bmp)")
	{
		if (!name.endsWith(".bmp",Qt::CaseInsensitive))
			name.append(".bmp");
		image.save(name, "BMP");
	}
	else if (filter == "JPEG File Interchange Format (*.jpg *.jpeg)")
	{
        if (!name.endsWith(".jpg",Qt::CaseInsensitive) && !name.endsWith(".jpeg"))
			name.append(".jpg");
		image.save(name, "JPEG");
	}
	else if (filter == "Portable Network Graphics Format (*.png)")
	{
		if (!name.endsWith(".png",Qt::CaseInsensitive))
			name.append(".png");
		image.save(name, "PNG");
	}
}

// Put the application in the edit mode
void MainWindow::SetEditMode(bool edit)
{
    bool e = !edit;

    if( e )
    {
        // remove the widget from the splitter by reparenting
        if( splitter->count()==2 )
            splitter->widget(1)->setParent(NULL);

        // remove the edit toolbar
        if( editToolbar )
        {
            removeToolBar( editToolbar );
            editToolbar = NULL;
        }
    }

    UpdateActions(true, edit);
    entityWidget->AllowClose(e);
    implant->EnableActions(!edit);

    // cancel measure if active
    QList<QAction*> actions = measureGroup->actions();
    foreach(QAction* a, actions)
    {
        if( a->isChecked() )
            a->setChecked(false);
    }
    viewWidget->SetMeasureMode(None);
}

void MainWindow::SetReviewMode(bool rm)
{
   caseReviewMode = rm;

   RefreshTitle();
}

void MainWindow::SetSubTitle(QString const &middle)
{
   subTitle = middle;

   RefreshTitle();
}

void MainWindow::RefreshTitle()
{
	QString ttl = /*QString("iFit\xae ") +*/ subTitle + (IsReviewMode() ? strReviewMode : "");

	setWindowTitle(ttl);
}


// Enable/disable the actions depend on if there is an open implant and edit status
void MainWindow::UpdateActions(bool hasImplant, bool edit)
{
    bool enable = ( hasImplant && !edit );

    actOpenImplant->setEnabled(!edit);
#ifdef TESTING_MODE
    actSaveImplant->setEnabled(hasImplant && !IsReviewMode());
#else
    actSaveImplant->setEnabled(hasImplant && !IsReviewMode() && (( implant->isInProdFolder()) || !implant->isInProdFolder()));//PSV---
#endif
    actCloseImplant->setEnabled(enable);

    actViewport->setEnabled(hasImplant);
    actZoomIn->setEnabled(hasImplant);
    actZoomOut->setEnabled(hasImplant);
    actZoomFit->setEnabled(hasImplant);
    actRefresh->setEnabled(hasImplant);
    
    toolMeasure->setEnabled(hasImplant);
    toolMeasure->setChecked(false);
    measureGroup->setEnabled(hasImplant);

    actOption->setEnabled(!edit);
    actReviewerFeedback->setEnabled(ReviewerFeedbackAvailable());
}

// Set options
void MainWindow::SetOption()
{
    OptionDialog dlg(this);
    dlg.AddPage( new OptionGeneral() );

    dlg.exec();
}

// Cancel the measurement tool
void MainWindow::CancelMeasure()
{
	toolMeasure->defaultAction()->trigger();
}

// Trigger the measure action
void MainWindow::TriggerMeasure(QAction* act)
{
    toolMeasure->setDefaultAction(act);

    if( act->isChecked() )
    {
        // uncheck all other actions
        QList<QAction*> actions = measureGroup->actions();
        foreach(QAction* a, actions)
        {
            if( a != act )
                a->setChecked(false);
        }

        // set the measure mode
        if( actMeasureEntityDistance == act )
            viewWidget->SetMeasureMode( EntityDistance );
        else if( actMeasureEntityAngle == act )
            viewWidget->SetMeasureMode( EntityAngle );
        else if( actMeasureEntityRadius == act )
            viewWidget->SetMeasureMode( EntityRadius );
        else if( actMeasureScreenDistance == act )
            viewWidget->SetMeasureMode( ScreenDistance );
        else if( actMeasureScreenAngle == act )
            viewWidget->SetMeasureMode( ScreenAngle );
        else if( actMeasureScreenRadius == act )
            viewWidget->SetMeasureMode( ScreenRadius );
    }
    else
        viewWidget->SetMeasureMode(None);
}

// Close an implant immediately & exit application
void MainWindow::CloseImplantImmediately(QString& msg)
{
	if( msg.isEmpty() )
	{
		QMessageBox::critical(this, "Errors!", "Software encounters unexpected errors.\nSoftware will be terminated.", QMessageBox::Ok );
	}
	else
	{
		QMessageBox::critical(this, "Errors!", msg, QMessageBox::Ok );
	}

	CloseImplant(false);
	QApplication::closeAllWindows();
}
