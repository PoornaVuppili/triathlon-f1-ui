#include <QtGui>
#include <QtOpenGL>
#include "TotalView.h"
#include "TotalDoc.h"
#include "TotalMainWindow.h"
#include "..\KAppTotal\Globals.h"
#include "..\KAppTotal\ZoomView.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\Utilities.h"
#include "FemurSurfaceMgr.h"
#include "FemoralAxes.h"
#include "MakeFemoralAxes.h"
#include "MakeFemoralAxes.h"
#include "FemoralCuts.h"
#include "DefineOutlineProfile.h"
#include "ValidateOutlineProfile.h"
#include "OutlineProfile.h"
#include "DisplayAdvancedControl.h"
#include "AdvancedControl.h"
#include "MeasureScreenDistance.h"
#include "MeasureScreenAngle.h"
#include "MeasureScreenRadius.h"
#include "MeasureEntityDistance.h"
#include "MeasureEntityAngle.h"
#include "MeasureCurveRadius.h"
#include "DefineCartilage.h"
#include "ImportOsteophyteSurface.h"
#include "DefineOutlineProfileJigs.h"
#include "MakeInnerSurfaceJigs.h"
#include "MakeOuterSurfaceJigs.h"
#include "MakeSideSurfaceJigs.h"
#include "MakeSolidPositionJigs.h"
#include "SolidPositionJigs.h"
#include "DefineStylusJigs.h"
#include "MakeAnteriorSurfaceJigs.h"
#include "OutputResultsJigs.h"
#include "DisplayAdvancedControlJigs.h"
#include "AdvancedControlJigs.h"
#include "..\KApp\Viewport.h"
#include "..\KApp\CloseImplantEvent.h"
#include "..\KApp\SwitchBoard.h"
#include "IFemurImplant.h"

#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

CTotalView::CTotalView( QObject *parent )
    : COpenGLView( parent )
{
	m_validationReportDlg = NULL;
	m_validationReportTextEdit = NULL;
	m_validationReportJigsDlg = NULL;
	m_validationReportJigsTextEdit = NULL;
	m_chkNotableConditions = NULL;

	SwitchBoard::addSignalReceiver(SIGNAL(FemoralJigsNeedToChange()), this, SLOT(OnSetModifiedMarkerToFemoralJigs()));
}


CTotalView::~CTotalView()
{
    SwitchBoard::removeSignalReceiver(this);
	SwitchBoard::removeSignalReceiver(this);
}

void CTotalView::OnViewFront() 
{
	m_pDoc->AppendLog( QString("CTotalView::OnViewFront()") );

	SetPredefinedView( FRONT_VIEW );

	OnDetermineViewingNormalRegion();
}


void CTotalView::OnViewBack() 
{
	m_pDoc->AppendLog( QString("CTotalView::OnViewBack()") );

	SetPredefinedView( BACK_VIEW );

	OnDetermineViewingNormalRegion();
}


void CTotalView::OnViewTop() 
	{
	m_pDoc->AppendLog( QString("CTotalView::OnViewTop()") );

	SetPredefinedView( TOP_VIEW );	

	OnDetermineViewingNormalRegion();
}


void CTotalView::OnViewBottom() 
{
	m_pDoc->AppendLog( QString("CTotalView::OnViewBottom()") );

	SetPredefinedView( BOTTOM_VIEW );	

	OnDetermineViewingNormalRegion();
}


void CTotalView::OnViewLeft() 
	{
	m_pDoc->AppendLog( QString("CTotalView::OnViewLeft()") );

	SetPredefinedView( LEFT_VIEW );	

	OnDetermineViewingNormalRegion();
}


void CTotalView::OnViewRight() 
{
	m_pDoc->AppendLog( QString("CTotalView::OnViewRight()") );

	SetPredefinedView( RIGHT_VIEW );	

	OnDetermineViewingNormalRegion();
}


void CTotalView::OnViewFrontIso() 
	{
	m_pDoc->AppendLog( QString("CTotalView::OnViewFrontIso()") );

	SetPredefinedView( ISO1_VIEW );	

	OnDetermineViewingNormalRegion();

}


void CTotalView::OnViewBackIso() 
	{
	m_pDoc->AppendLog( QString("CTotalView::OnViewBackIso()") );

	SetPredefinedView( ISO2_VIEW );	

	OnDetermineViewingNormalRegion();
}

void CTotalView::OnImportFemurSurface(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnImportFemurSurface()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CFemurSurfaceMgr* femurSurfaceMgr = new CFemurSurfaceMgr( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT )
	{
        viewWidget->AddManager( femurSurfaceMgr );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}

		pEntity = m_pDoc->GetEntity( ENT_ROLE_HIP );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

void CTotalView::OnMakeFemoralAxes(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnMakeFemoralAxes()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CMakeFemoralAxes* makeFemAxes = new CMakeFemoralAxes( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( makeFemAxes );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		// display the femur
		if ( pEntity )
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
				 pEntity->GetDisplayMode() != DISP_MODE_DISPLAY_CT && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENT_CT)
			{
				pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();

	Qt::CursorShape shape = Qt::ArrowCursor;
	QApplication::setOverrideCursor(QCursor(shape));
}

void CTotalView::OnCutFemur(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnCutFemur()") );

	if(manActType == MAN_ACT_TYPE_RESET)
	{
		//PSV--- FOR 28R CASE
		CFemoralCuts * pFemoralCut = GetTotalDoc()->GetFemoralCuts();

		QString strImplantName = mainWindow->GetImplant()->GetInfo("ImplantName");
		QString strFileName = strImplantName + "_008_Femoral_Cuts";
		pFemoralCut = new CFemoralCuts(GetTotalDoc(), ENT_ROLE_FEMORAL_CUTS, "CutFemur", 8);

		QString strDir = mainWindow->GetImplant()->GetInfo("ImplantDir");
		strDir = strDir + "\\Surfaces\\FemCut";
		pFemoralCut->LoadInputs(strDir);
		GetTotalDoc()->AddPart( pFemoralCut );

		return;
	}
	else if(manActType == MAN_ACT_TYPE_EDIT)
	{

		return;
	}

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;
}

void CTotalView::OnDefineOutlineProfile(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnDefineOutlineProfile()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CDefineOutlineProfile* defOutlineProfile = new CDefineOutlineProfile( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager(defOutlineProfile);

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE );
		if ( pEntity ) // Display itself
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
		// display femoral cuts
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		
	}
	if ( manActType==MAN_ACT_TYPE_EDIT ) // Only Edit expects bottom view. Review has its in OnReviewPredefinedView()
	{
		Viewport* vp = viewWidget->GetViewport();
		vp->FrontView(false);
	}

	Redraw();
}


void CTotalView::OnHandleFemoralAxes(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnHandleFemoralAxes()") );

	CAdvancedControl* advCtrl = GetTotalDoc()->GetAdvancedControl();
	if ( GetTotalDoc()->GetFemoralAxes() ) 
	{
		// FemoralAxes is already existed.
		OnMakeFemoralAxes(manActType);
	}
	else if ( advCtrl->GetAutoAllSteps() )
	{
		OnAutoInitializeAllSteps(manActType);
	}
	else
	{
		// Just go to FemoralAxes
		OnMakeFemoralAxes(manActType);
	}
}

void CTotalView::OnAutoInitializeAllSteps(CManagerActivateType manActType)
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// First iteration, run Femoral Axes (Fanning Planes) and JCurves.
	// Second/Third iterations, evaluate the distal/posterior delta and flexion angle,
	// then modify the femoral axes based on the distal/posterior delta and flexion angle,
	// then run through to the end

	// First run 
	// Make femoral axes (and fanning planes).
	// If femoral axes not exist, RESET to create one.
	// OnAutoInitializeAllSteps() could be called from FemoralAxes() 
	// where users have manually refined the orientation.
	// Then do not Reset to modify their orientation.
	
	// Check valgus and varus
	if ( GetTotalDoc()->GetFemoralAxes() )
	{
		double maxDistalPostteriorDelta = 6.0;
		if (GetTotalDoc()->GetFemoralAxes()->GetEpiCondyleSize() > 85.0)
			maxDistalPostteriorDelta = 7.0;
		IwPoint3d lPnt, rPnt;
		bool bDev;
		double distalDelta, posteriorDelta;
		//CMakeFemoralAxes::RefineEpiCondylarLandMarks_PosteriorDelta(GetTotalDoc(), 0.1, lPnt, rPnt, bDev, &distalDelta, &posteriorDelta);
		if ( distalDelta < 0 /* varus*/ || (distalDelta-posteriorDelta) > 4.5 /*valgus*/ || 
			 (posteriorDelta-distalDelta) > maxDistalPostteriorDelta /*post delta too big*/|| distalDelta > maxDistalPostteriorDelta /* distal delta too big */) 
		{
			// Software cannot handle these 3 conditions. Switch to none auto
			if ( distalDelta < -2.0 /* severe varus*/ || distalDelta > 8.5 /* too too big */ || posteriorDelta > 8.5 /* too too big */)
			{
				GetTotalDoc()->GetAdvancedControl()->SetAutoAllSteps(false);
				GetTotalDoc()->GetAdvancedControl()->SetAutoMajorSteps(false);
				GetTotalDoc()->GetAdvancedControl()->SetAutoJOCSteps(false);
				// Also make reset available in toolbar
				GetTotalDoc()->GetAdvancedControl()->SetFemoralCutsEnableReset(true);
				GetTotalDoc()->GetAdvancedControl()->SetOutlineProfileEnableReset(true);
				//GetTotalDoc()->GetAdvancedControl()->SetJCurvesDisplayMoreOptions(true);
			}
			else // software still auto JOC steps
			{
				GetTotalDoc()->GetAdvancedControl()->SetAutoAllSteps(false);
				GetTotalDoc()->GetAdvancedControl()->SetAutoMajorSteps(false);
				GetTotalDoc()->GetAdvancedControl()->SetAutoJOCSteps(true);
			}
			//GetTotalDoc()->GetFemoralAxes()->DisplayAntCutPlane(); // Display ant cut preview plane for non-auto mode
			QApplication::restoreOverrideCursor();
		/*	int button = QMessageBox::information( NULL, 
											"Info", 
											"This could be irregular shaped condyles.\nFemoral Axis needs to be manually defined.\nNo JCurves Cuts Outline automation.", 
											QMessageBox::Ok );
			return;*/
		}
	}

	// Second iteration, refine MakeFemoralAxes() 
	// Re-initialize fanning plane translation distance and rotation angle,
	// in order to recalculate good values.
	//GetTotalDoc()->GetAdvancedControl()->SetAnteriorTranslationDistance(0.0);
	//GetTotalDoc()->GetAdvancedControl()->SetCondylarRotationAngle(0.0);
	//Redraw();
	//QApplication::processEvents();

	// Third iteration, refine MakeFemoralAxes 
	// Re-initialize fanning plane translation distance and rotation angle,
	// in order to recalculate good values.
	//GetTotalDoc()->GetAdvancedControl()->SetAnteriorTranslationDistance(0.0);
	//GetTotalDoc()->GetAdvancedControl()->SetCondylarRotationAngle(0.0);
	// Refine MakeFemoralAxes(), 

	// Create JOC steps
	bool JOCSucceed = OnAutoInitializeJCOSteps(true);
	Redraw();
	QApplication::processEvents();

	// only display 3 surfaces
	m_pDoc->GetEntity( ENT_ROLE_FEMUR )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMUR )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();
	m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();
	m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	if ( !JOCSucceed )
	{
		m_pDoc->GetEntity( ENT_ROLE_FEMUR )->SetDisplayMode(DISP_MODE_DISPLAY);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMUR )->GetEntId(), true);
		m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS )->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS )->GetEntId(), false);
		Redraw();
		QApplication::processEvents();
		QApplication::restoreOverrideCursor();
		return;
	}

	// if only create the major steps, return now.
	if ( 1 /*GetTotalDoc()->GetAdvancedControl()->GetAutoMajorSteps()*/ )
	{
		Qt::CursorShape shape = Qt::ArrowCursor;
		QApplication::setOverrideCursor(QCursor(shape));
		//QApplication::restoreOverrideCursor();
		return;
	}

	if ( m_pDoc->isPosteriorStabilized() )// For PS
	{
		m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS )->SetDisplayMode(DISP_MODE_TRANSPARENCY);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS )->GetEntId(), true);
		Redraw();
		QApplication::processEvents();
	}
	else // For CR
	{
		m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS )->SetDisplayMode(DISP_MODE_TRANSPARENCY);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMORAL_CUTS )->GetEntId(), true);
		Redraw();
		QApplication::processEvents();
		
	}
	
	QApplication::restoreOverrideCursor();
}

bool CTotalView::OnAutoInitializeJCOSteps
(
	bool calledFromInitializeAllSteps		// I: whether called from OnAutoInitializeAllSteps(). If yes, JCurves will been kept and 
											//    A) ant cut plane in femoral axes, B) ant cut in Cuts, C) ant profile in OutlinrProfile 
											//    will be adjusted accordingly.
)
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	bool succeed = true;

	// Delete and create objects from JCurves, outelineProfile, to FemoralCuts
	// NOTE sequence is JOC
	CEntity* pEnt;	
	// JCurves deletion could be optional. If OnAutoInitializeAllSteps() calls this function, keeps the JCurves.
	// Get the design time before delete them
	int JTime=0, CTime=0, OTime=0, STime=0;
	
	// Cuts
	pEnt = m_pDoc->GetEntity(ENT_ROLE_FEMORAL_CUTS);
	if ( pEnt )
	{
		CTime = pEnt->GetDesignTime();
		m_pDoc->DeleteEntityById(pEnt->GetEntId());
	}
	
	// Outline Profiles
	pEnt = m_pDoc->GetEntity(ENT_ROLE_OUTLINE_PROFILE);
	if ( pEnt )
	{
		OTime = pEnt->GetDesignTime();
		m_pDoc->DeleteEntityById(pEnt->GetEntId());
	}
	
	// Delete jigs entities
	GetTotalDoc()->DeleteEntitiesBelongToJigs();
	// Here also need to delete Femur PS features.
	//IFemurImplant::DeleteEntitiesBelongToPSFeatures();

	OnTessellationNormal();
	//OnTessellationCoarse();

	OnCutFemur(MAN_ACT_TYPE_RESET);			
	OnDefineOutlineProfile(MAN_ACT_TYPE_RESET);
	//OnDefineOutlineProfile(MAN_ACT_TYPE_REFINE);// Refine immediately//psv---
	Redraw();
	QApplication::processEvents();
	return true;//psv---

	OnCutFemur(MAN_ACT_TYPE_REFINE);//PSV--- TO TEST if it can stop crash
	Redraw();
	QApplication::processEvents();
	OnDefineOutlineProfile(MAN_ACT_TYPE_REFINE);
	Redraw();
	QApplication::processEvents();

	// Once JCO have been refined, the anterior profile should be updated if called from OnAutoInitializeAllSteps()
	if ( calledFromInitializeAllSteps ) 
	{
		RefineAnteriorProfileOnlyInAJCO();
	}


	CAdvancedControl* advCtrl = GetTotalDoc()->GetAdvancedControl();
	int addIter = advCtrl->GetAutoAdditionalIterations() + 2;
	
	for ( int iter=0; iter<addIter; iter++)
	{
		// if last iter
		if (iter == addIter-1)
		{
			// Last iteration
			OnTessellationFine();
			// Re-initialize fanning plane translation distance and rotation angle,
			// in order to make the fanning planes pass the medial j-curve most posterior point.
			//GetTotalDoc()->GetAdvancedControl()->SetAnteriorTranslationDistance(0.0);
			//GetTotalDoc()->GetAdvancedControl()->SetCondylarRotationAngle(0.0);
		}
		// The iteration, all should be all "REFINE".

		// optimize femoral cuts
		OnCutFemur(MAN_ACT_TYPE_REFINE);
		Redraw();
		QApplication::processEvents();
		// refine outline profile
		OnDefineOutlineProfile(MAN_ACT_TYPE_REFINE);
		Redraw();
		QApplication::processEvents();
	}

	QApplication::restoreOverrideCursor();

	// Toggle off minor design steps
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMUR )->GetEntId(), false);
	m_pDoc->GetEntity( ENT_ROLE_FEMUR )->SetDisplayMode(DISP_MODE_HIDDEN);

	return succeed;
}

void CTotalView::OnAutoRegenerateJCOSteps()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// regenerate femoral cuts
	if ( GetTotalDoc()->GetFemoralCuts() )
		OnCutFemur(MAN_ACT_TYPE_REGENERATE);
	// regenerate outline profile
	if ( GetTotalDoc()->GetOutlineProfile() )
		OnDefineOutlineProfile(MAN_ACT_TYPE_REGENERATE);

	QApplication::restoreOverrideCursor();

	return;
}

void CTotalView::OnAutoRefineJCOSteps()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	for (unsigned i=0; i<2; i++)
	{
		// refine femoral cuts
		if ( GetTotalDoc()->GetFemoralCuts() )
			OnCutFemur(MAN_ACT_TYPE_REFINE);
		// refine outline profile
		if ( GetTotalDoc()->GetOutlineProfile() )
			OnDefineOutlineProfile(MAN_ACT_TYPE_REFINE);
	}
	
	QApplication::restoreOverrideCursor();

	return;
}

void CTotalView::OnManualDefaultJCOSteps()
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Step# 2, determine femoral cuts
	OnCutFemur(MAN_ACT_TYPE_RESET);
	// Step# 3, determine outline profile
	OnDefineOutlineProfile(MAN_ACT_TYPE_RESET);

	QApplication::restoreOverrideCursor();

}

void CTotalView::OnHandleFillets(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnHandleFillets()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CAdvancedControl* advCtrl = GetTotalDoc()->GetAdvancedControl();

	bool autoSteps = advCtrl->GetAutoAllSteps() || advCtrl->GetAutoJOCSteps() || advCtrl->GetAutoMajorSteps();
	if ( !autoSteps  )// if fillet implant exists or none auto
	{
	}
	else
	{
		OnAutoInitializeFPCSteps( manActType );
	}

}

void CTotalView::OnAutoInitializeFPCSteps(CManagerActivateType manActType)
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// We need to check whether outer fillet exists or not
	// before proceeding the Pegs, Casting, and MFG entity creation
	bool bOuterFilletExisted = true;//CAddFillets::ValidateFilletingExistence(GetTotalDoc());
	if ( !bOuterFilletExisted )
	{
		QApplication::restoreOverrideCursor();
		return;
	}

	QApplication::restoreOverrideCursor();

	return;
}

void CTotalView::OnOutputJigs()
{
	m_pDoc->AppendLog( QString("CTotalView::OnOutputJigs()") );

 	if (m_pManager != NULL) return;

    viewWidget->AddManager( new COutputResultsJigs( this ) );
	Redraw();

}

void CTotalView::OnAdvancedControl()
{
	m_pDoc->AppendLog( QString("CTotalView::OnAdvancedControl()") );

	if (m_pManager != NULL) return;

	new CDisplayAdvancedControl( this );
	Redraw();

}

void CTotalView::OnAdvancedControlJigs()
{
	m_pDoc->AppendLog( QString("CTotalView::OnAdvancedControlJigs()") );

	if (m_pManager != NULL) return;

	new CDisplayAdvancedControlJigs( this );
	Redraw();

}

void CTotalView::OnTest1()
{
	//IwPolyBrep* pPolyBrep = NULL;
	//IwPolyBrep::ReadFromSTLFile(m_pDoc->GetIwContext(),"C:/Projects/TriathlonF1/0005166.STL",pPolyBrep,IW_BINARY, FALSE, 3);
	//pPolyBrep->SetTolerance(IW_EFF_ZERO);
	//pPolyBrep->CleanSolidMesh(TRUE);
	//pPolyBrep->WriteToSTLFile("C:/Projects/TriathlonF1/0005166_fixed.STL", IW_BINARY);

}


void CTotalView::OnMeasureScreenDistance()
{
	m_pDoc->AppendLog( QString("CTotalView::OnMeasureScreenDistance()") );

	viewWidget->AddManager( new CMeasureScreenDistance( this ) );
	Redraw();
}

void CTotalView::OnMeasureScreenAngle()
{
	m_pDoc->AppendLog( QString("CTotalView::OnMeasureScreenAngle()") );

	viewWidget->AddManager( new CMeasureScreenAngle( this ) );
	Redraw();
}

void CTotalView::OnMeasureScreenRadius()
{
	m_pDoc->AppendLog( QString("CTotalView::OnMeasureScreenRadius()") );

	viewWidget->AddManager( new CMeasureScreenRadius( this ) );
	Redraw();
}

void CTotalView::OnMeasureEntityDistance()
{
	m_pDoc->AppendLog( QString("CTotalView::OnMeasureEntityDistance()") );

	viewWidget->AddManager( new CMeasureEntityDistance( this ) );
	Redraw();
}

void CTotalView::OnMeasureEntityAngle()
{
	m_pDoc->AppendLog( QString("CTotalView::OnMeasureEntityAngle()") );

	viewWidget->AddManager( new CMeasureEntityAngle( this ) );
	Redraw();
}

void CTotalView::OnMeasureCurveRadius()
{
	m_pDoc->AppendLog( QString("CTotalView::OnMeasureCurveRadius()") );

	viewWidget->AddManager( new CMeasureCurveRadius( this ) );
	Redraw();
}

void CTotalView::OnAnalyzeImplant(CManagerActivateType manActType)
{

}

void CTotalView::OnValidateImplant(CManagerActivateType manActType, bool displayReportDlg /* displayReportDlg or not display for output */)
{
}

void CTotalView::OnAcceptValidationReport()
{
	// J-Curves restoration distance 
	double maxRestorationDistance=0;
	
	// Delete dlg and text
	if ( m_validationReportDlg )
		delete m_validationReportDlg;
	m_validationReportTextEdit = NULL;// when delete m_validationReportDlg, m_validationReportTextEdit should also be deteted.
}

void CTotalView::OnPrintValidationReport(QString outputFileName)
{
	bool allGoodResults=true;
	QString totalReportMessage ;
	totalReportMessage = m_pDoc->GetValidationReport(allGoodResults);

	QPrinter printer;
	printer.setPageSize(QPrinter::A4);
	printer.setFullPage (true);
	if ( outputFileName.isEmpty() )// print to a printer
	{
		QPrintDialog *dialog = new QPrintDialog (&printer, m_validationReportTextEdit);
		if (dialog->exec() == QDialog::Accepted)
		{
			m_validationReportTextEdit->print(&printer);
		}
	}
	else // print to a given file, default *.pdf format and *.txt format
	{
		printer.setOutputFileName( outputFileName );
		m_validationReportTextEdit->print(&printer);
		// Also write to a text file
		QString outputFileNameText = outputFileName;
		int sLength = outputFileNameText.length();
		if (outputFileNameText.endsWith(".pdf"))
			outputFileNameText = outputFileNameText.left(sLength-4); // remove ".pdf"
		outputFileNameText = outputFileNameText + QString(".txt");
		QFile			File( outputFileNameText );
		QTextStream		out( &File );
		bool bOK = File.open( QIODevice::WriteOnly | QIODevice::Text );
		if( !bOK )
			return;
		out << totalReportMessage;
		File.close();	
	}


}

void CTotalView::OnValidateJigs(CManagerActivateType manActType, bool displayReportDlg /* displayReportDlg or not display for output */)
{
	m_pDoc->AppendLog( QString("CTotalView::OnValidateJigs()") );

	QString message;
	m_validationReportJigsTextEdit = new QTextEdit();
	GetTotalDoc()->InitializeValidationReportJigs();

	CEntity *pEntity = NULL, *pEntity1 = NULL;
	CDisplayMode dispMode, dispMode1;
	
	// Outline profile jigs
	QApplication::setOverrideCursor( Qt::WaitCursor );
	if ( m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS ) )
	{
		if ( m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->GetUpToDateStatus() )
		{
			CDefineOutlineProfileJigs::Validate(GetTotalDoc(), message);
		}
		else
		{
			GetTotalDoc()->AppendValidationReportJigsEntry("**** Outline Profile Jig not up-to-date ****", false);
		}
	}
	else
	{
		GetTotalDoc()->AppendValidationReportJigsEntry("**** No Outline Profile Jig ****", false);
	}
	QApplication::restoreOverrideCursor();

	// Solid Position Jigs
	if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS ) )
	{
		if ( m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS )->GetUpToDateStatus() )
		{
			CMakeSolidPositionJigs::Validate(GetTotalDoc(), message);
		}
		else
		{
			GetTotalDoc()->AppendValidationReportJigsEntry("**** Solid Position Jig not up-to-date ****", false);
		}
	}
	else
	{
		GetTotalDoc()->AppendValidationReportJigsEntry("**** No Solid Position Jig ****", false);
	}
	QApplication::restoreOverrideCursor();


	// Stylus
	QApplication::setOverrideCursor( Qt::WaitCursor );
	if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS ) )
	{
		if ( m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS )->GetUpToDateStatus() )
		{
			CDefineStylusJigs::Validate(GetTotalDoc(), message);
		}
		else
		{
			GetTotalDoc()->AppendValidationReportJigsEntry("**** Stylus Jig not up-to-date ****", false);
		}
	}
	else
	{
		GetTotalDoc()->AppendValidationReportJigsEntry("**** No Stylus Jig ****", false);
	}
	QApplication::restoreOverrideCursor();


	// post-processing
	bool allGoodResults=true;
	QString totalReportMessage ;
	totalReportMessage = GetTotalDoc()->GetValidationReportJigs(allGoodResults);
	if ( allGoodResults )
	{
		bool found = GetValidationReportJigsTextEdit()->find(QString("**** NOT ALL PASS ****"), QTextDocument::FindBackward);
		if ( found )
			GetValidationReportJigsTextEdit()->cut();
	}
	GetValidationReportJigsTextEdit()->moveCursor(QTextCursor::Start);

	m_validationReportJigsDlg = NULL;
	if ( displayReportDlg )
	{
		// Report message
		m_validationReportJigsTextEdit->setReadOnly(true);

		// report dialog
		m_validationReportJigsDlg = new QDialog(viewWidget, 0);
		m_validationReportJigsDlg->setWindowTitle(QString("E DHR"));
		m_validationReportJigsDlg->resize(600, 500);
		m_validationReportJigsDlg->setModal(true);

		QGridLayout *mainLayout = new QGridLayout;

		QHBoxLayout *hlayoutText = new QHBoxLayout;
		hlayoutText->addWidget(m_validationReportJigsTextEdit);
		mainLayout->addLayout(hlayoutText, 0, 0, 1, 1);

		// For "OK" and "Print" buttons
		QHBoxLayout *hlayoutButtons = new QHBoxLayout;
		QPushButton *OKButton = new QPushButton(tr("OK"), NULL);
		connect(OKButton, SIGNAL(clicked(bool)), this, SLOT(OnAcceptValidationReportJigs()));
		hlayoutButtons->addWidget(OKButton);

		QPushButton *PrintButton = new QPushButton(tr("Print"), NULL);
		connect(PrintButton, SIGNAL(clicked(bool)), this, SLOT(OnPrintValidationReportJigs()));
		hlayoutButtons->addWidget(PrintButton);

		mainLayout->addLayout(hlayoutButtons, 1, 0, 1, 1);

		m_validationReportJigsDlg->setLayout(mainLayout);

		if ( displayReportDlg )
			m_validationReportJigsDlg->exec();
	}

}

void CTotalView::OnAcceptValidationReportJigs()
{
	if ( m_validationReportJigsDlg )
		delete m_validationReportJigsDlg;
	m_validationReportJigsTextEdit = NULL;// when delete m_validationReportJigsDlg, m_validationReportTextEdit should also be deteted.
}

void CTotalView::OnPrintValidationReportJigs(QString outputFileName)
{
	bool allGoodResults=true;
	QString totalReportMessage ;
	totalReportMessage = GetTotalDoc()->GetValidationReportJigs(allGoodResults);

	QPrinter printer;
	printer.setPageSize(QPrinter::A4);
	printer.setFullPage (true);
	if ( outputFileName.isEmpty() )// print to a printer
	{
		QPrintDialog *dialog = new QPrintDialog (&printer, m_validationReportJigsTextEdit);
		if (dialog->exec() == QDialog::Accepted)
		{
			m_validationReportJigsTextEdit->print(&printer);
		}
	}
	else // print to a given file, default *.pdf format and *.txt format
	{
		printer.setOutputFileName( outputFileName );
		m_validationReportJigsTextEdit->print(&printer);
		// delete m_validationReportJigsTextEdit
		delete m_validationReportJigsTextEdit;
		m_validationReportJigsTextEdit = NULL;
		// Also write to a text file
		QString outputFileNameText = outputFileName;
		int sLength = outputFileNameText.length();
		if (outputFileNameText.endsWith(".pdf"))
			outputFileNameText = outputFileNameText.left(sLength-4); // remove ".pdf"
		outputFileNameText = outputFileNameText + QString(".txt");
		QFile			File( outputFileNameText );
		QTextStream		out( &File );
		bool bOK = File.open( QIODevice::WriteOnly | QIODevice::Text );
		if( !bOK )
			return;
		out << totalReportMessage;
		File.close();	
		//
		OnAcceptValidationReportJigs();
	}


}

void CTotalView::OnFileInfo()
{
	m_pDoc->AppendLog( QString("CTotalView::OnFileInfo()") );

	QList<QString> prevUserInfo;
	int prevUserCount = m_pDoc->GetPreviousUserInfo(prevUserInfo);

	QString msgTitle;
	msgTitle = QString("TriathlonF1%1").arg(QChar(8482));

	QString msg = QString("File Name: %1\n").arg(m_pDoc->GetFileName());

	msg = msg + QString("File History:\n");
	for (int i=0; i<prevUserCount; i++)
	{
		msg = msg + QString("%1\n").arg(prevUserInfo.at(i));
	}
	msg = msg + QString("%1\n").arg(m_pDoc->GetCurrentUserInfo());

	QMessageBox::about(viewWidget, msgTitle, msg);
}

void CTotalView::OnPatientInfo()
{
	m_pDoc->AppendLog( QString("CTotalView::OnPatientInfo()") );

	CImplantSide			eImplantSide;
	CImplantRegulatoryZone	eImplantZone;
	QString					sPatientId, sSide, sZone, sZeros;

	if( m_pDoc->GetImplantInfo( sPatientId, eImplantSide, eImplantZone ) )
	{
		switch( eImplantSide )
		{
			case IMPLANT_SIDE_LEFT:		sSide = "Left Knee";	break;
			case IMPLANT_SIDE_RIGHT: 	sSide = "Right Knee";	break;
			default:					sSide = "Unknown Knee Side";
		}
		switch( eImplantZone )
		{
			case IMPLANT_ZONE_US_FDA:	sZone = "US FDA";	break;
			case IMPLANT_ZONE_CE_MARK: 	sZone = "CE MARK";	break;
			case IMPLANT_ZONE_NEW_ZONE: sZone = "New Zone";	break;
			default:					sZone = "Unknown Regulatory Zone";
		}
	} 

	QString msgTitle = QString("Patient's Information");
	QString msg = QString("Patient ID: %1, %2, %3").arg(sPatientId).arg(sSide).arg(sZone);
	QMessageBox::about(viewWidget, msgTitle, msg);

}

void CTotalView::OnDetermineViewingNormalRegion()
{
	if (m_pManager)
	{
		if (m_pManager->GetClassName() == "CDefineOutlineProfile")
		{
			((CDefineOutlineProfile*)m_pManager)->DetermineViewingNormalRegion();
			Redraw();
		}
		else if (m_pManager->GetClassName() == "CDefineOutlineProfileJigs")
		{
			((CDefineOutlineProfileJigs*)m_pManager)->DetermineViewingNormalRegion();
			Redraw();
		}
	}
}

void CTotalView::OnAcceptThisManager(CManager* thisManager)
{
	if ( thisManager == NULL )
		return;

	if (thisManager->GetClassName() == "CAnalyzeImplant")
	{
		//CAnalyzeImplant* analyzeImplantManager = (CAnalyzeImplant*)thisManager;
		//analyzeImplantManager->OnAccept();
	}
	else if (thisManager->GetClassName() == "CMeasureEntityDistance")
	{
		CMeasureEntityDistance* entityDistanceManager = (CMeasureEntityDistance*)thisManager;
		entityDistanceManager->OnAccept();
	}
	else if (thisManager->GetClassName() == "CMeasureEntityAngle")
	{
		CMeasureEntityAngle* entityAngleManager = (CMeasureEntityAngle*)thisManager;
		entityAngleManager->OnAccept();
	}
	else if (thisManager->GetClassName() == "CMeasureCurveRadius")
	{
		CMeasureCurveRadius* curveRadiusManager = (CMeasureCurveRadius*)thisManager;
		curveRadiusManager->OnAccept();
	}
	else if (thisManager->GetClassName() == "CMeasureScreenDistance")
	{
		CMeasureScreenDistance* screenDistanceManager = (CMeasureScreenDistance*)thisManager;
		screenDistanceManager->OnAccept();
	}
	else if (thisManager->GetClassName() == "CMeasureScreenAngle")
	{
		CMeasureScreenAngle* screenAngleManager = (CMeasureScreenAngle*)thisManager;
		screenAngleManager->OnAccept();
	}
	else if (thisManager->GetClassName() == "CMeasureScreenRadius")
	{
		CMeasureScreenRadius* screenRadiusManager = (CMeasureScreenRadius*)thisManager;
		screenRadiusManager->OnAccept();
	}

}

void CTotalView::OnAcceptAnalyzeImplantManager()
{
	// Accept the stack managers
	int mSize = m_pManagerStack.size(); 
	for ( int i=mSize; i>0; i-- )
	{
		CManager* stackManager = m_pManagerStack.at(i-1);
		if ( stackManager->GetClassName() == "CAnalyzeImplant" )
		{
			 OnAcceptThisManager(stackManager);
		}
	}

	// Accept this manager
	CManager* thisManager = this->GetManager();
	if ( thisManager )
	{
		if ( thisManager->GetClassName() == "CAnalyzeImplant" )
		{
			OnAcceptThisManager(thisManager);
		}
	}

}

//// Jigs ////////////////
void CTotalView::OnImportOsteophyteSurface(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnImportOsteophyteSurface()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && m_pManager!=NULL ) return;

	bool firstTime = false;
	CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
	if ( pEntity == NULL ) // the first time coming here
	{
		// hide all when first time import osteophyte
		mainWindow->GetImplant()->HideAll();

		firstTime = true;
	}

    CImportOsteophyteSurface* importOsteo = new CImportOsteophyteSurface( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT )
	{
        viewWidget->AddManager( importOsteo );

		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
				 pEntity->GetDisplayMode() != DISP_MODE_DISPLAY_NET && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY_NET &&
				 pEntity->GetDisplayMode() != DISP_MODE_WIREFRAME)
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}

			if ( firstTime )
			{
				Viewport* vp = viewWidget->GetViewport();
				vp->BottomView();
			}
		}


	}

    if (firstTime)
    {
        // TODO: make the module (which is of type CTotalMainWindow)
        //       report that the design stage 1 (not 0) has commenced
        GetMainWindow()->emitDesignStageCommenced(1);
    }

	Viewport* vp = viewWidget->GetViewport();
	vp->BestFit();

	Redraw();
}

void CTotalView::OnHandleCartilage(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnHandleCartilage()") );

	CAdvancedControlJigs* advCtrlJigs = GetTotalDoc()->GetAdvancedControlJigs();
	if ( GetTotalDoc()->GetCartilageSurface() ) 
	{
		// Cartilage Surface Jigs is already existed.
		OnDefineCartilage(manActType);
	}
	else if ( !advCtrlJigs->GetAutoAllSteps() )
	{
		// it is none auto or auto driven steps
		OnDefineCartilage(manActType);
	}
	else
	{
		OnAutoInitializeAllJigsSteps(manActType);
	}
}

void CTotalView::OnAutoInitializeAllJigsSteps(CManagerActivateType manActType)
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Display Femur 
	m_pDoc->GetEntity( ENT_ROLE_FEMUR )->SetDisplayMode(DISP_MODE_DISPLAY);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMUR )->GetEntId(), true);
	// Hide osteophyte 
	m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	// Define Cartilage
	// Need to turn off this flag
	GetTotalDoc()->GetAdvancedControlJigs()->SetCartilageInitializeEdgeOnly(false);
	OnDefineCartilage(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Hide Femur and display osteophyte
	m_pDoc->GetEntity( ENT_ROLE_FEMUR )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMUR )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->SetDisplayMode(DISP_MODE_DISPLAY);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->GetEntId(), true);
	Redraw();
	QApplication::processEvents();

	// Define Outline Profile Jigs
	OnDefineOutlineProfileJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Make Inner Surface Jigs
	OnMakeInnerSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Make Outer Surface Jigs
	OnMakeOuterSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Make Side Surface 
	OnMakeSideSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Create Solid Position Jigs 
	OnMakeSolidPositionJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Leave cartilage alone

	m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	// Check whether solid position jig Brep exists or not. If not, something wrong.
	if ( !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
	{
		QApplication::restoreOverrideCursor();
		int button = QMessageBox::critical( NULL, 
										tr("Error!"), 
										tr("Solid positioning jig fails."), 
										QMessageBox::Abort);	
		if( button == QMessageBox::Abort ) return;
	}

	// Create Stylus Jigs 
	OnDefineStylusJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	// Create Anterior Surface 
	OnMakeAnteriorSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	QApplication::restoreOverrideCursor();
}

void CTotalView::OnAutoInitializeFromFemoralAxes()
{
	if ( GetTotalDoc()->GetFemoralAxes()->GetDisplayAntCutPlane() ) // if display ant plane, then just re-create JOC
			QTimer::singleShot(250, this, SLOT(OnAutoInitializeJCOSteps()));
	else // If not display ant plane, refine AJOC (but RefineFemoralLandMarks() will not be called.)
			QTimer::singleShot(250, this, SLOT(OnAutoInitializeAllSteps())); ;

}

void CTotalView::OnDefineCartilage(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnDefineCartilage()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CEntity*	pEntity = NULL;
	// whether is initialization
	bool firstTime = (m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE ) == NULL);
	// Hide osteophyte
	pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}
	// Display femur
	pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
	}

    CDefineCartilage* defCart = new CDefineCartilage( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( defCart );

		pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
		if ( pEntity && manActType==MAN_ACT_TYPE_EDIT )
		{
			if (firstTime)
			{
				pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
			else if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
				      pEntity->GetDisplayMode() != DISP_MODE_DISPLAY_NET && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY_NET &&
				      pEntity->GetDisplayMode() != DISP_MODE_WIREFRAME )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

void CTotalView::OnDefineOutlineProfileJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnDefineOutlineProfileJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS );
	// Hide femur
	pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}
	// Display osteophyte 
	pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
	}
	// Hide Cartilage
	pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}

	CDefineOutlineProfileJigs* defOutProfJigs = new CDefineOutlineProfileJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( defOutProfJigs );

		pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

void CTotalView::OnMakeInnerSurfaceJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnMakeInnerSurfaceJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	bool firstTime = (m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS ) == NULL);

	CMakeInnerSurfaceJigs* makeInnerSurfJig = new CMakeInnerSurfaceJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( makeInnerSurfJig );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS );
		if ( pEntity )
		{
			if (firstTime)
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY_NET );
			else if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
				      pEntity->GetDisplayMode() != DISP_MODE_DISPLAY_NET && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY_NET &&
				      pEntity->GetDisplayMode() != DISP_MODE_WIREFRAME )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

void CTotalView::OnHandleOuterSurfaceJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnHandleOuterSurfaceJigs()") );

	CAdvancedControlJigs* advCtrlJigs = GetTotalDoc()->GetAdvancedControlJigs();
	if ( GetTotalDoc()->GetOuterSurfaceJigs() ) 
	{
		// Outer Surface Jigs is already existed.
		OnMakeOuterSurfaceJigs(manActType);
	}
	else if ( !advCtrlJigs->GetAutoAllSteps() && !advCtrlJigs->GetAutoDrivenSteps() )
	{
		// it is none auto
		OnMakeOuterSurfaceJigs(manActType);
	}
	else
	{
		OnAutoInitializeDrivenJigsSteps(manActType);
	}
}

void CTotalView::OnAutoInitializeDrivenJigsSteps(CManagerActivateType manActType)
{
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Update outline profile jig / inner surface jigs, if needed
	if ( !m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->GetUpToDateStatus() )
		OnDefineOutlineProfileJigs(MAN_ACT_TYPE_REGENERATE);
	if ( !m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS )->GetUpToDateStatus() )
		OnMakeInnerSurfaceJigs(MAN_ACT_TYPE_REGENERATE);

	// Make Outer Surface Jigs
	OnMakeOuterSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Make Side Surface 
	OnMakeSideSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	// Create Solid Position Jigs 
	OnMakeSolidPositionJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	//
	m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	// Check whether solid position jig Brep exists or not. If not, something wrong.
	if ( !GetTotalDoc()->GetSolidPositionJigs()->GetIwBrep() )
	{
		QApplication::restoreOverrideCursor();
		int button = QMessageBox::critical( NULL, 
										tr("Error!"), 
										tr("Solid positioning jig fails."), 
										QMessageBox::Abort);	
		if( button == QMessageBox::Abort ) return;
	}

	// Create Stylus Jigs 
	OnDefineStylusJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->SetDisplayMode(DISP_MODE_DISPLAY);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->GetEntId(), true);
	Redraw();
	QApplication::processEvents();

	// Create Anterior Surface 
	OnMakeAnteriorSurfaceJigs(MAN_ACT_TYPE_RESET);
	Redraw();
	QApplication::processEvents();

	m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS )->SetDisplayMode(DISP_MODE_HIDDEN);
	m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS )->GetEntId(), false);
	Redraw();
	QApplication::processEvents();

	QApplication::restoreOverrideCursor();
}

void CTotalView::OnMakeOuterSurfaceJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnMakeOuterSurfaceJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CMakeOuterSurfaceJigs* makeOutSurfJig = new CMakeOuterSurfaceJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( makeOutSurfJig );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
				 pEntity->GetDisplayMode() != DISP_MODE_DISPLAY_NET && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY_NET &&
				 pEntity->GetDisplayMode() != DISP_MODE_WIREFRAME )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

void CTotalView::OnMakeSideSurfaceJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnMakeSideSurfaceJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CMakeSideSurfaceJigs* makeSideSurfJig = new CMakeSideSurfaceJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT )
	{
        viewWidget->AddManager( makeSideSurfJig );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

void CTotalView::OnMakeSolidPositionJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnMakeSolidPositionJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CMakeSolidPositionJigs* makeSolidPosJig = new CMakeSolidPositionJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( makeSolidPosJig );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				if ( manActType==MAN_ACT_TYPE_EDIT )
				{
					pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
					m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
				}
				else if ( manActType==MAN_ACT_TYPE_REVIEW ) // Reviewers do not want to see solid positioning jig but inner/outer surfaces.
				{
					pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
					m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
				}
			}
		}
	}

	Redraw();
}

void CTotalView::OnDefineStylusJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnDefineStylusJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CEntity*	pEntity = NULL;
	// Display osteophyte
	pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
	}
	// Hide femur
	pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}
	// Hide Cartilage
	pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
	if ( pEntity )
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}

	CDefineStylusJigs* defStyJig = new CDefineStylusJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( defStyJig );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}


void CTotalView::OnMakeAnteriorSurfaceJigs(CManagerActivateType manActType)
{
	m_pDoc->AppendLog( QString("CTotalView::OnMakeAnteriorSurfaceJigs()") );

	if ( manActType==MAN_ACT_TYPE_EDIT && IsAMouseEventManager(m_pManager) ) return;

	CMakeAnteriorSurfaceJigs* makeAntSurfJig = new CMakeAnteriorSurfaceJigs( this, manActType );

	if ( manActType==MAN_ACT_TYPE_EDIT || manActType==MAN_ACT_TYPE_REVIEW )
	{
        viewWidget->AddManager( makeAntSurfJig );

		CEntity*	pEntity = m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS );
		if ( pEntity )
		{
			if ( pEntity->GetDisplayMode() != DISP_MODE_DISPLAY && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY &&
				 pEntity->GetDisplayMode() != DISP_MODE_DISPLAY_NET && pEntity->GetDisplayMode() != DISP_MODE_TRANSPARENCY_NET &&
				 pEntity->GetDisplayMode() != DISP_MODE_WIREFRAME )
			{
				pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
				m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
			}
		}
	}

	Redraw();
}

//////////////////////////////////////////////////////////////
// This function updates:
// 1) anterior cut plane in Femoral Axes
// 2) anterior cut in Cuts
// 3) anterior profile in Outline Profile
// 4) But NOT update anterior j-curve in JCurves 
void CTotalView::RefineAnteriorProfileOnlyInAJCO()
{
	if ( GetTotalDoc()->GetFemoralAxes() == NULL ||
		 GetTotalDoc()->GetFemoralCuts() == NULL ||
		 GetTotalDoc()->GetOutlineProfile() == NULL )
		 return;

	IwPoint3d antEdgePoint;
	double flangeRatio = GetTotalDoc()->GetFemoralCuts()->GetAnteriorFlangeRatio(antEdgePoint);
	if ( flangeRatio == 0  )/* Do nothing. */
		return; 
	if ( flangeRatio < 1.25 && flangeRatio > 1.0 )/* Do nothing. */
		return;

	bool extendAnteriorCoverage = true;
	// if flangeRatio > 1.25, shorten anterior coverage.
	// if flangeRatio < 1.0, enlarge anterior coverage.
	if ( flangeRatio > 1.25 )/* shorten anterior coverage */ 
	{ 
		extendAnteriorCoverage = false;
	}
	else if ( flangeRatio < 1.0 )/* enlarge anterior coverage */
	{
		extendAnteriorCoverage = true;
	}
	// Prepare for updating 
	IwTArray<IwPoint3d> antCutPlanePoints;
	bool getAntCutPlanePoints = GetTotalDoc()->GetFemoralAxes()->GetBackUpAntCutPlaneRotationCenters(antCutPlanePoints);
	if ( !getAntCutPlanePoints )
		return;
	IwPoint3d moreCoverageRotationCenter = antCutPlanePoints.GetAt(0);
	IwPoint3d moreCoverageCenter = antCutPlanePoints.GetAt(1);
	IwPoint3d lessCoverageRotationCenter = antCutPlanePoints.GetAt(2);
	IwPoint3d lessCoverageCenter = antCutPlanePoints.GetAt(3);

	// Only do RefineAnteriorProfileOnlyInAJCO() one time for each initialization.
	GetTotalDoc()->GetFemoralAxes()->EmptyBackUpAntCutPlaneRotationCenters();

	// Get Femoral Axes parameters
	IwPoint3d hipPoint, proximalPoint, leftEpiCondylarPoint, rightEpiCondylarPoint, antCutPlaneCenter, antCutPlaneRotationCenter;
	double antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, femoralAxesYRotAngle, femoralAxesZRotAngle;
	GetTotalDoc()->GetFemoralAxes()->GetParams(hipPoint, proximalPoint, leftEpiCondylarPoint, rightEpiCondylarPoint, 
		antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, 
		antCutPlaneCenter, antCutPlaneRotationCenter, femoralAxesYRotAngle, femoralAxesZRotAngle);

	if ( extendAnteriorCoverage )/* enlarge anterior coverage */ 
	{ 
		// Update Femoral Axes
		GetTotalDoc()->GetFemoralAxes()->SetParams(hipPoint, proximalPoint, leftEpiCondylarPoint, rightEpiCondylarPoint, 
			antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, 
			moreCoverageCenter, moreCoverageRotationCenter, femoralAxesYRotAngle, femoralAxesZRotAngle);
		OnMakeFemoralAxes(MAN_ACT_TYPE_REGENERATE);
		// Update Femoral Cuts
		GetTotalDoc()->GetFemoralCuts()->MoveAnteriorCut(moreCoverageCenter);
		OnCutFemur(MAN_ACT_TYPE_REGENERATE);
		// Update Outline Profile
		GetTotalDoc()->GetOutlineProfile()->MoveAnteriorProfile(moreCoverageCenter);
		OnDefineOutlineProfile(MAN_ACT_TYPE_REFINE);
	}
	else /* shorten anterior coverage */
	{
		// Update Femoral Axes
		GetTotalDoc()->GetFemoralAxes()->SetParams(hipPoint, proximalPoint, leftEpiCondylarPoint, rightEpiCondylarPoint, 
			antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, 
			lessCoverageCenter, lessCoverageRotationCenter, femoralAxesYRotAngle, femoralAxesZRotAngle);
		OnMakeFemoralAxes(MAN_ACT_TYPE_REGENERATE);
		// Update Femoral Cuts
		GetTotalDoc()->GetFemoralCuts()->MoveAnteriorCut(lessCoverageCenter);
		OnCutFemur(MAN_ACT_TYPE_REGENERATE);
		// Update Outline Profile
		GetTotalDoc()->GetOutlineProfile()->MoveAnteriorProfile(lessCoverageCenter);
		OnDefineOutlineProfile(MAN_ACT_TYPE_REFINE);
	}

	// update JCurves
	// No need to do anything since the function after this "RefineAnteriorProfileInAJOC()" is the updating JCurves.

}

//////////////////////////////////////////////////////////////////////////////////
// This function will delete the existing manager and activate the next manager.
void CTotalView::OnActivateNextReviewManager(CManager* pManagerToBeDeleted, CEntRole eRole, bool bNext)
{
	if (pManagerToBeDeleted)
	{
		pManagerToBeDeleted->SetDestroyMe(); // set the flag
		Draw(); // call Draw() to delete the manager immediately
	}

	// If the design not complete design yet, just return;
	if ( !GetTotalDoc()->IsFemoralImplantDesignComplete() )
	{
		QMessageBox::information( NULL, 
								"Info", 
								"Femoral Implant is not complete or not up-to-date.\nCan not proceed review.", 
								QMessageBox::Ok );
		return;	
	}
	// Hide all the un-necessary entities
	GetTotalDoc()->HideAllFemurImplantEntities();
	GetTotalDoc()->HideAllFemurJigsEntities();

	if ( eRole == ENT_ROLE_NONE )
	{
        // Make sure that the 3d view is maximized
        viewWidget->GetViewport()->SetMaximized(true);
		OnReviewFemoralAxes();
	}
	else if ( eRole == ENT_ROLE_FEMORAL_AXES )
	{
	}
	
	else if ( eRole == ENT_ROLE_FEMORAL_CUTS )
	{
		if ( bNext )
			OnReviewOutlineProfile();
	}
	else if ( eRole == ENT_ROLE_OUTLINE_PROFILE )
	{
		if ( !bNext )
			OnReviewFemoralCuts();
	}
	
}

//////////////////////////////////////////////////////////////////////////////////
// This function will delete the existing manager and activate the next manager.
void CTotalView::OnActivateNextJigsReviewManager(CManager* pManagerToBeDeleted, CEntRole eRole, bool bNext)
{
	if (pManagerToBeDeleted)
	{
		pManagerToBeDeleted->SetDestroyMe(); // set the flag
		Draw(); // call Draw() to delete the manager immediately
	}

	// If the design not complete design yet, just return;
	if ( !GetTotalDoc()->IsFemoralJigsDesignComplete() )
	{
		QMessageBox::information( NULL, 
								"Info", 
								"Femoral Jigs are not complete or not up-to-date.\nCan not proceed review.", 
								QMessageBox::Ok );
		return;	
	}

	// Hide all the un-necessary entities
	if ( m_pDoc->GetEntity(ENT_ROLE_FEMUR) )
	{
		m_pDoc->GetEntity(ENT_ROLE_FEMUR)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_FEMUR )->GetEntId(), false);
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_OSTEOPHYTE_SURFACE) )
	{
		m_pDoc->GetEntity(ENT_ROLE_OSTEOPHYTE_SURFACE)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE )->GetEntId(), false);
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_CARTILAGE_SURFACE) )
	{
		m_pDoc->GetEntity(ENT_ROLE_CARTILAGE_SURFACE)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_OUTLINE_PROFILE_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_OUTLINE_PROFILE_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTLINE_PROFILE_JIGS )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_INNER_SURFACE_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_INNER_SURFACE_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_INNER_SURFACE_JIGS )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_OUTER_SURFACE_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_OUTER_SURFACE_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_OUTER_SURFACE_JIGS )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_SIDE_SURFACE_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_SIDE_SURFACE_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_SIDE_SURFACE_JIGS )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_SOLID_POSITION_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_SOLID_POSITION_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_SOLID_POSITION_JIGS )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_STYLUS_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_STYLUS_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_STYLUS_JIGS )->GetEntId(), false);	
	}
	if ( m_pDoc->GetEntity(ENT_ROLE_ANTERIOR_SURFACE_JIGS) )
	{
		m_pDoc->GetEntity(ENT_ROLE_ANTERIOR_SURFACE_JIGS)->SetDisplayMode(DISP_MODE_HIDDEN);
		m_pDoc->ToggleEntPanelItemByID(m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS )->GetEntId(), false);	
	}

	if ( eRole == ENT_ROLE_JIGS_NONE )
	{
		OnReviewCartilage();
	}
	else if ( eRole == ENT_ROLE_CARTILAGE_SURFACE )
	{
		if ( bNext )
			OnReviewOutlineProfileJigs();
		else
		{
			if (m_pDoc->isPosteriorStabilized())
				// Jump to PS Feature Review
				OnGoToPSFeaturesReview();
		}
	}
	else if ( eRole == ENT_ROLE_OUTLINE_PROFILE_JIGS )
	{
		if ( bNext )
			OnReviewInnerSurfaceJigs();
		else
			OnReviewCartilage();
	}
	else if ( eRole == ENT_ROLE_INNER_SURFACE_JIGS )
	{
		if ( bNext )
			OnReviewSolidPositionJigs();
		else
			OnReviewOutlineProfileJigs();
	}
	else if ( eRole == ENT_ROLE_SOLID_POSITION_JIGS )
	{
		if ( bNext )
			OnReviewStylusJigs();
		else
			OnReviewInnerSurfaceJigs();
	}
	else if ( eRole == ENT_ROLE_STYLUS_JIGS )
	{
		if ( bNext )
			OnReviewAnteriorSurfaceJigs();
		else
			OnReviewSolidPositionJigs();
	}
	else if ( eRole == ENT_ROLE_ANTERIOR_SURFACE_JIGS )
	{
		if ( bNext )
		{
		}
		else
			OnReviewStylusJigs();
	}
}

void CTotalView::OnGoToPSFeaturesReview()
{

}

/////////////////////////////////////////////////////////////////////
void CTotalView::OnCancelThisManager(CManager* thisManager)
{
	if ( thisManager == NULL )
		return;
}

//////////////////////////////////////////////////////////////////////
// iTW v6 only exists CAnalyzeImplant as a measurement manager.
void CTotalView::OnCancelAllMeasurementManagers()
{
	// cancel all the stack managers
	int mSize = m_pManagerStack.size(); 
	for ( int i=mSize; i>0; i-- )
	{
		CManager* stackManager = m_pManagerStack.at(i-1);
		if ( stackManager->GetClassName() == "CAnalyzeImplant" )
		{
			 OnCancelThisManager(stackManager);
		}
	}

	// cancel this manager
	CManager* thisManager = this->GetManager();
	if ( thisManager->GetClassName() == "CAnalyzeImplant" )
	{
		OnCancelThisManager(thisManager);
	}
}

bool CTotalView::DisplayReviewCommentDialog(CEntRole eRole)
{
	return true;
}

void CTotalView::OnSetModifiedMarkerToFemoralJigs()
{
	GetTotalDoc()->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_ADVANCED_CONTROL_JIGS, false, false);
}