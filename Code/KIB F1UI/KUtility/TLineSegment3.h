///////////////////////////////////////////////////////////////////////
//	File:    TLineSegment3.h
//	Purpose: A template class for 3D line segment.
///////////////////////////////////////////////////////////////////////

#ifndef TLINESEGMENT3_H
#define TLINESEGMENT3_H

#include "TPoint3.h"
#include "TLine3.h"

template <typename T>
class TLineSegment3
{
public:
    ///////////////////////////////////////////////////////////////////////
    // Function name:    TLineSegment3
    // Function purpose: Constructor
    // Input: the coordinates of two points
    // Output: None
    ///////////////////////////////////////////////////////////////////////
    TLineSegment3() {}

    TLineSegment3(T x1, T y1, T z1, T x2, T y2, T z2)
    {
        p1.SetXYZ(x1,y1,z1);
        p2.SetXYZ(x2,y2,z2);
    }

    TLineSegment3(const TPoint3<T>& _p1, const TPoint3<T>& _p2)
    { 
        p1 = _p1;
        p2 = _p2;
    }

    // Get values
    TPoint3<T> GetP1() const { return p1; }
    TPoint3<T> GetP2() const { return p2; }
    T GetX1() const { return p1.GetX(); }
    T GetY1() const { return p1.GetY(); }
    T GetZ1() const { return p1.GetZ(); }
    T GetX2() const { return p2.GetX(); }
    T GetY2() const { return p2.GetY(); }
    T GetZ2() const { return p2.GetZ(); }

    // Set values
    void SetP1(const TPoint3<T>& p) { p1 = p; }
    void SetP2(const TPoint3<T>& p) { p2 = p; }
    void SetX1(T xv) { p1.SetX(xv); }
    void SetY1(T yv) { p1.SetY(yv); }
    void SetZ1(T zv) { p1.SetZ(zv); }
    void SetX1Y1Z1(T xv, T yv, T zv) { p1.SetXYZ(xv,yv,zv); }
    void SetX2(T xv) { p2.SetX(xv); }
    void SetY2(T yv) { p2.SetY(yv); }
    void SetZ2(T zv) { p2.SetZ(zv); }
    void SetX2Y2Z2(T xv, T yv, T zv) { p2.SetXYZ(xv,yv,zv); }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Distance
    // Function purpose: Find the distance from a point to a line segment
    // Input: the point coordinate
    //        (optional) the closest point on the line segment
    // Return: the distance
    ///////////////////////////////////////////////////////////////////////
    T Distance(T x, T y, T z, TPoint3<T>* closest = NULL) const
    {
        // use double for computation internally, and convet to type T at return
        double x1 = p1.GetX();
        double y1 = p1.GetY();
        double z1 = p1.GetZ();
        double x2 = p2.GetX();
        double y2 = p2.GetY();
        double z2 = p2.GetZ();

        double a = x2 - x1;
        double b = y2 - y1;
        double c = z2 - z1;
        double v = 1 / (a*a + b*b + c*c);
        double k = - v * ( a*(x1 - x) + b*(y1 - y) + c*(z1 - z) );

        // the point with the smallest distance is on the line segment
        // - clamp k between 0 and 1
        if( k<0 )
            k = 0;
        else if( k>1 )
            k = 1;
        double tx = x1 + k * a;
        double ty = y1 + k * b;
        double tz = z1 + k * c;
        if( closest )
            closest->SetXYZ(tx,ty,tz);
        tx -= x;
        ty -= y;
        tz -= z;
        return (T)sqrt(tx*tx + ty*ty + tz*tz);
    }

    T Distance(const TPoint3<T>& p, TPoint3<T>* closest = NULL) const
    {
        return Distance(p.x(), p.y(), p.z(), closest);
    }

    // Test if two line segments are parallel
    bool IsParallel(const TLineSegment3<T>& ls, T tol = NumTraits<T>::dummy_precision()) const
    {
        TLine3<T> l1(p1, p2);
        TLine3<T> l2(ls.GetP1(), ls.GetP2());
        return l1.IsParallel(l2,tol);
    }

    // Test if the line segment is parallel to a line
    bool IsParallel(const TLine3<T>& ln, T tol = NumTraits<T>::dummy_precision()) const
    {
        TLine3<T> l1(p1, p2);
        return l1.IsParallel(ln, tol);
    }

    // Test if two line segments are colinear
    bool IsColinear(const TLineSegment3<T>& ls, T tol = NumTraits<T>::dummy_precision()) const
    {
        TLine3<T> l1(p1, p2);
        TLine3<T> l2(ls.GetP1(), ls.GetP2());
        return l1.IsColinear(l2, tol);
    }

    // Test if a line segment is colinear with a line
    bool IsColinear(const TLine3<T>& ln, T tol = NumTraits<T>::dummy_precision()) const
    {
        TLine3<T> l1(p1, p2);
        return l1.IsColinear(ln, tol);
    }

    ///////////////////////////////////////////////////////////////////////
    // Function name:    Intersect
    // Function purpose: Find the point of intersection with another line segment
    // Input:
    //   lineSeg: the other line segment
    // Output:
    //   intersectPoint: the point of intersection if exists
    // Return: an intersect flag
    //   IntersectFlag::Coincident, intersectPoint is undefined
    //   IntersectFlag::Parallel, intersectPoint is undefined
    //   IntersectFlag::Intersect, intersectPoint is the point of intersection
    //   IntersectFlag::NonIntersect, intersectPoint is the point that is
    //                                the closest to the other line segment
    ///////////////////////////////////////////////////////////////////////
    int Intersect(const TLineSegment3<T>& lineSeg, TPoint3<T>& intersectPoint, T tol=NumTraits<T>::dummy_precision()) const
    {
        if( IsParallel(lineSeg) )
        {
            TLine3<T> ln(p1, p2);
            return ln.Distance(lineSeg.GetP1())<tol ?
                IntersectFlag::Coincident : IntersectFlag::Parallel;
        }

        // Implementation is based on
        // http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/
        TPoint3<T> p3 = lineSeg.GetP1();
        TPoint3<T> p4 = lineSeg.GetP2();

        TPoint3<T> p13 = p1 - p3;
        TPoint3<T> p43 = p4 - p3;
        TPoint3<T> p21 = p2 - p1;

        double d1343 = p13.dot(p43);
        double d4321 = p43.dot(p21);
        double d1321 = p13.dot(p21);
        double d4343 = p43.dot(p43);
        double d2121 = p21.dot(p21);

        double denom = d2121 * d4343 - d4321 * d4321;
        double numer = d1343 * d4321 - d1321 * d4343;

        double mua = numer / denom;
        double mub = (d1343 + d4321 * mua) / d4343;

        const double mtol = min(tol/p1.Distance(p2), tol/p3.Distance(p4));

        if( mua>=-mtol && mua<=1+mtol && 
            mub>=-mtol && mub<=1+mtol )
        {
            Point3Double pmua(p1.GetX()+mua*p21.GetX(), p1.GetY()+mua*p21.GetY(), p1.GetZ()+mua*p21.GetZ());
            Point3Double pmub(p3.GetX()+mub*p43.GetX(), p3.GetY()+mub*p43.GetY(), p3.GetZ()+mub*p43.GetZ());

            if( pmua.Distance(pmub) < tol )
            {
                intersectPoint.SetXYZ( (pmua.GetX()+pmub.GetX())/2, (pmua.GetY()+pmub.GetY())/2, (pmua.GetZ()+pmub.GetZ())/2 );
                return IntersectFlag::Intersect;
            }
            else
                intersectPoint.SetXYZ( pmua.GetX(), pmua.GetY(), pmua.GetZ() );
        }
        else if( mua>=-mtol && mua<=1+mtol )
            intersectPoint.SetXYZ( p1.GetX()+mua*p21.GetX(), p1.GetY()+mua*p21.GetY(), p1.GetZ()+mua*p21.GetZ() );
        else if( mua<0 )
            intersectPoint.SetXYZ( p1.GetX(), p1.GetY(), p1.GetZ() );
        else
            intersectPoint.SetXYZ( p2.GetX(), p2.GetY(), p2.GetZ() );
        return IntersectFlag::NonIntersect;
    }

protected:
    TPoint3<T> p1, p2;
};

//////////////////////////////////////////
// typedef's 
//////////////////////////////////////////

typedef TLineSegment3<int> LineSegment3Int;
typedef TLineSegment3<float> LineSegment3Float;
typedef TLineSegment3<double> LineSegment3Double;

#endif //TLINESEGMENT3_H
