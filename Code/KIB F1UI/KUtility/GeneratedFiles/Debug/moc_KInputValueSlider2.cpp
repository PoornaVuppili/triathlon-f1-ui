/****************************************************************************
** Meta object code from reading C++ file 'KInputValueSlider2.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../KInputValueSlider2.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KInputValueSlider2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KInputValueSlider2_t {
    QByteArrayData data[11];
    char stringdata0[168];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KInputValueSlider2_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KInputValueSlider2_t qt_meta_stringdata_KInputValueSlider2 = {
    {
QT_MOC_LITERAL(0, 0, 18), // "KInputValueSlider2"
QT_MOC_LITERAL(1, 19, 13), // "value1Changed"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 13), // "value2Changed"
QT_MOC_LITERAL(4, 48, 15), // "onSlider1Change"
QT_MOC_LITERAL(5, 64, 1), // "v"
QT_MOC_LITERAL(6, 66, 16), // "onSpinBox1Change"
QT_MOC_LITERAL(7, 83, 15), // "onSlider2Change"
QT_MOC_LITERAL(8, 99, 16), // "onSpinBox2Change"
QT_MOC_LITERAL(9, 116, 27), // "on_pushButtonCancel_clicked"
QT_MOC_LITERAL(10, 144, 23) // "on_pushButtonOK_clicked"

    },
    "KInputValueSlider2\0value1Changed\0\0"
    "value2Changed\0onSlider1Change\0v\0"
    "onSpinBox1Change\0onSlider2Change\0"
    "onSpinBox2Change\0on_pushButtonCancel_clicked\0"
    "on_pushButtonOK_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KInputValueSlider2[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       3,    1,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   60,    2, 0x09 /* Protected */,
       6,    1,   63,    2, 0x09 /* Protected */,
       7,    1,   66,    2, 0x09 /* Protected */,
       8,    1,   69,    2, 0x09 /* Protected */,
       9,    0,   72,    2, 0x08 /* Private */,
      10,    0,   73,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void KInputValueSlider2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KInputValueSlider2 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->value1Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->value2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->onSlider1Change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->onSpinBox1Change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->onSlider2Change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->onSpinBox2Change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_pushButtonCancel_clicked(); break;
        case 7: _t->on_pushButtonOK_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (KInputValueSlider2::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KInputValueSlider2::value1Changed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (KInputValueSlider2::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KInputValueSlider2::value2Changed)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KInputValueSlider2::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_KInputValueSlider2.data,
    qt_meta_data_KInputValueSlider2,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KInputValueSlider2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KInputValueSlider2::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KInputValueSlider2.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int KInputValueSlider2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void KInputValueSlider2::value1Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KInputValueSlider2::value2Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
