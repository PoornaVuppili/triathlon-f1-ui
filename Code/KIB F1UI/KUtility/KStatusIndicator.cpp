#include "KStatusIndicator.h"
#include <qcoreapplication.h>

// global instance
KStatusIndicator kStatus;

KStatusIndicator::KStatusIndicator(QObject *parent)
    : QObject(parent)
{
    progress = NULL;
    status = NULL;
}

KStatusIndicator::~KStatusIndicator()
{
}

void KStatusIndicator::ShowMessage(QString msg, bool bForceUpdate)
{
    if( !status )
        return;
    status->showMessage(msg);
	if( bForceUpdate )
		QCoreApplication::processEvents();
}

void KStatusIndicator::ProgressInit(int maxv)
{
    if( !progress )
        return;
    progress->setHidden(false);
    progress->setRange(0, maxv);
}

void KStatusIndicator::ProgressInit(int minv, int maxv)
{
    if( !progress )
        return;
    progress->setHidden(false);
    progress->setRange(minv, maxv);
}

void KStatusIndicator::ProgressSet(int val)
{
    if( !progress )
        return;
    progress->setValue(val);
}

void KStatusIndicator::ProgressReset()
{
    if( !progress )
        return;
    progress->reset();
    progress->setHidden(true);
}

