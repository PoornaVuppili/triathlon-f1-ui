#include "Viewport.h"
#include "EntityView.h"
#include "..\KUtility\KUtility.h"
#include "..\KUtility\KXmlHelper.h"
#include "Manager.h"
#include <qbitmap.h>
#include <IwBrep.h>
#include <IwFace.h>
#include <IwExtent3d.h>
#include <IwPoly.h>
#include <assert.h>
#include <algorithm>
#include "..\KUtility\TIntersect.h"
#include "..\KUtility\TCircle3.h"
#include "MainWindow.h"
#include "..\KApp\Implant.h"
// For debugging-------
//Point3Float raycen;
//Point3Float raydir;
//bool drawray = false;


#define SIDE_SCROLL_OFFSET 50

Viewport::Viewport()
    : QObject(), defaultManager(NULL)
{
    widget = NULL;
    viewportMinX = viewportMinY = 0;
    viewportMaxX = viewportMaxY = 0;
    contextMenu = NULL;

    id = "3D View";
    timerID = 0;
    showAxis = false;
	showAxisRotationRegion = false;
	animateTransform = true;

    ClearContent();

    maximized = false;
    contextMenu = NULL;
    quad = NULL;

	predefinedViews = NULL;

    measureCursor = QCursor(QBitmap(":/Resources/ArrowRuler.png"), 
            QBitmap(":/Resources/ArrowRulerMask.png"), 0,0);

    actFront = new QAction("Front view", this);
    actFront->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_1) );
    actFront->setIcon( QIcon( ":/Resources/frontView.png" ) );
    connect(actFront, SIGNAL(triggered()), this, SLOT(FrontView()));

    actBack = new QAction("Back view", this);
    actBack->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_2) );
    actBack->setIcon( QIcon(":/Resources/backView.png") );
    connect(actBack, SIGNAL(triggered()), this, SLOT(BackView()));

    actLeft = new QAction("Left view", this);
    actLeft->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_3) );
    actLeft->setIcon( QIcon(":/Resources/leftView.png") );
    connect(actLeft, SIGNAL(triggered()), this, SLOT(LeftView()));

    actRight = new QAction("Right view", this);
    actRight->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_4) );
    actRight->setIcon( QIcon(":/Resources/rightView.png") );
    connect(actRight, SIGNAL(triggered()), this, SLOT(RightView()));

    actTop = new QAction("Top view", this);
    actTop->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_5) );
    actTop->setIcon( QIcon(":/Resources/topView.png") );
    connect(actTop, SIGNAL(triggered()), this, SLOT(TopView()));

    actBottom = new QAction("Bottom view", this);
    actBottom->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_6) );
    actBottom->setIcon( QIcon(":/Resources/bottomView.png") );
    connect(actBottom, SIGNAL(triggered()), this, SLOT(BottomView()));
}

QToolBar* Viewport::SetupPredefinedViewsToolBar()
{
	if(predefinedViews)
		return predefinedViews;

	predefinedViews = new PredefinedViewToolBar();
	predefinedViews->addAction(actFront);
    predefinedViews->addAction(actBack);
    predefinedViews->addAction(actLeft);
    predefinedViews->addAction(actRight);
    predefinedViews->addAction(actTop);
    predefinedViews->addAction(actBottom);
    connect(predefinedViews, SIGNAL(BroadcastCurrentCoordSystem(const CoordSystem&)), this, SLOT(SetCurrentCoordSystem(const CoordSystem&)));

	return predefinedViews;
}

Viewport::~Viewport()
{
	if(Is3D())
		RemoveDefaultManager();

	delete predefinedViews;
    ClearContent();

    delete actFront;
    delete actBack;
    delete actLeft;
    delete actRight;
    delete actTop;
    delete actBottom;
}

void Viewport::SetCurrentCoordSystem(const CoordSystem& axes)
{
    QStringList iconFiles = axes.GetIconFiles();
    if( iconFiles.size()>=6 )
    {
        actFront->  setIcon( QIcon(iconFiles[0]) );
	    actBack->   setIcon( QIcon(iconFiles[1]) );
	    actLeft->   setIcon( QIcon(iconFiles[2]) );
	    actRight->  setIcon( QIcon(iconFiles[3]) );
        actTop->    setIcon( QIcon(iconFiles[4]) );
	    actBottom-> setIcon( QIcon(iconFiles[5]) );
    }
    else
    {
        actFront  ->setIcon( QIcon(":/Resources/frontView.png" ) );
        actBack   ->setIcon( QIcon(":/Resources/backView.png") );
        actLeft   ->setIcon( QIcon(":/Resources/leftView.png") );
        actRight  ->setIcon( QIcon(":/Resources/rightView.png") );
        actTop    ->setIcon( QIcon(":/Resources/topView.png") );
        actBottom ->setIcon( QIcon(":/Resources/bottomView.png") );
    }

	// If previously was aligned to x/y/z axes predefined views, re-align to the new x/y/z axes
	// such that it behaves the same as 2D image viewports.
	bool wasAlignedView = false;
	if ( predefinedViews->GetFrontView(&wasAlignedView) || wasAlignedView )
	{
		FrontView(false);
	}
	else if ( predefinedViews->GetBackView(&wasAlignedView) || wasAlignedView )
	{
		BackView(false);
	}
	else if ( predefinedViews->GetLeftView(&wasAlignedView) || wasAlignedView )
	{
		LeftView(false);
	}
	else if ( predefinedViews->GetRightView(&wasAlignedView) || wasAlignedView )
	{
		RightView(false);
	}
	else if ( predefinedViews->GetTopView(&wasAlignedView) || wasAlignedView )
	{
		TopView(false);
	}
	else if ( predefinedViews->GetBottomView(&wasAlignedView) || wasAlignedView )
	{
		BottomView(false);
	}
}

int Viewport::GetNumberOfCoordSystems() const
{
    return predefinedViews ? predefinedViews->GetNumberOfSystems() : 0;
}

QString Viewport::GetCurrentCoordSystemName()
{
    return predefinedViews ? predefinedViews->GetCurrentSystemName() : "";
}

void Viewport::SetCurCoordSystemName(const QString& name)
{
    if( predefinedViews )
        predefinedViews->SetCurrentSystemName(name);
}

void Viewport::SetDefaultManager(Manager *mgr)
{
	defaultManager = mgr;
}

void Viewport::RemoveDefaultManager()
{
	if(defaultManager)
	{
		delete defaultManager;
		defaultManager = NULL;
	}
}

void Viewport::SetupContextMenu()
{
    if( contextMenu!=NULL )
    {
        delete contextMenu;
        contextMenu = NULL;
    }

    contextMenu = new QMenu(widget);

    QAction* actFit = contextMenu->addAction("Best fit");
    actFit->setShortcut( QKeySequence(Qt::Key_F) );
    connect(actFit, SIGNAL(triggered()), this, SLOT(BestFitView()));

    QAction* actMaxRestore = contextMenu->addAction(maximized ? "Restore" : "Maximize");
    connect(actMaxRestore, SIGNAL(triggered()), this, SLOT(ToggleMaximize()));

    contextMenu->addSeparator();

	if(Is3D())
	{
		contextMenu->addAction(actFront);
		contextMenu->addAction(actBack);
		contextMenu->addAction(actLeft);
		contextMenu->addAction(actRight);
		contextMenu->addAction(actTop);
		contextMenu->addAction(actBottom);

		QAction* actAxisRotationRegion = contextMenu->addAction("Show Axes Rotation Region");
		actAxisRotationRegion->setCheckable(true);
		actAxisRotationRegion->setChecked( showAxisRotationRegion );
		connect(actAxisRotationRegion, SIGNAL(triggered()), this, SLOT(ToggleShowAxisRotationRegion()));
	}

    QAction* actAxis = contextMenu->addAction("Show orientation");
    actAxis->setCheckable(true);
    actAxis->setChecked( showAxis );
    connect(actAxis, SIGNAL(triggered()), this, SLOT(ToggleShowAxis()));
}

///////////////////////////////////////////////////////////////////////
// Function name:    ClearContent
// Function purpose: Clear everything.
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::ClearContent()
{
    // reset the bounding box to a default value
    boundBox.SetCenter(Point3Float(0,0,0));
    boundBox.SetAxis(Point3Float(1,0,0), Point3Float(0,1,0), Point3Float(0,0,1));
    boundBox.SetSpan(0,0,0);
    emptyBBox = true;

    // reset the center
    centerRot.SetXYZ( 0, 0, 0 );

    // reset the transformation
    IsoTransform(transform);

    // reset the entity buffer
    entityViews.clear();

    BestFit();

    if( contextMenu!=NULL )
    {
        delete contextMenu;
        contextMenu = NULL;
    }

    measureMode = None;

    managerStack.clear();
}

void Viewport::IsoTransform(Rigid3Float& t)
{
    t.SetIdentity();
    t.Translate( -centerRot );
    t.RotateZ(-135);
    t.RotateX(-70);
}

void Viewport::IsoTransformBack(Rigid3Float& t)
{
    t.SetIdentity();
    t.Translate( -centerRot );
    t.RotateZ(135);
    t.RotateX(70);
}

void Viewport::GetViewport(int& minX, int& maxX, int& minY, int& maxY)
{
    minX = viewportMinX;
    minY = viewportMinY;
    maxX = viewportMaxX;
    maxY = viewportMaxY;
}

void Viewport::SetViewport(int minX, int maxX, int minY, int maxY)
{
    viewportMinX = minX;
    viewportMinY = minY;
    viewportMaxX = maxX;
    viewportMaxY = maxY;

    UpdateViewVolume();
}

void Viewport::GetViewRange(float& left, float& right, float& bottom, float& top, float& ne, float& fa)
{
    left = viewLeft;
    right = viewRight;
    bottom = viewBottom;
    top = viewTop;
    ne = viewNear;
    fa = viewFar;
}

void Viewport::SetViewRange(float left, float right, float bottom, float top, float ne, float fa)
{
    viewLeft = left;
    viewRight = right;
    viewBottom = bottom;
    viewTop = top;
    viewNear = ne;
    viewFar = fa;
}

void Viewport::SetBoundingBox(const Box3Float& box)
{
    boundBox = box;
    centerRot = boundBox.GetCenter();
    emptyBBox = false;

    UpdateTransform();
    UpdateViewVolume();
}

void Viewport::UpdateTransform()
{
    Rotate3Float r = transform.GetRotationMatrix();

    transform.SetIdentity();
    transform.Translate( -centerRot );
    transform.Rotate( r );
}

void Viewport::SetTransform(Rigid3Float& tran)
{
    transform = tran;
    UpdateViewVolume();
}

void Viewport::SetOrientation(Rotate3Float& rot)
{
    transform.SetIdentity();
    transform.Translate( -centerRot );
    transform.Rotate(rot);
}

bool Viewport::InsideViewport(int x, int y)
{
    return ( x>=viewportMinX && x<viewportMaxX &&
             y>=viewportMinY && y<viewportMaxY );
}

void Viewport::SetMaximized(bool b)
{
    if( maximized == b )
        return;

    maximized = b;
    emit maximizeChanged();
    BestFitView();
}

///////////////////////////////////////////////////////////////////////
// Function name:    BestFit
// Function purpose: Adjust the viewing to best fit the data
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::BestFit(float expandRatio)
{
    UpdateBoundingBox();
    UpdateTransform();
    UpdateViewVolume(expandRatio);
}

void Viewport::UpdateViewVolume(float expandRatio)
{
    // the bounding box after the transformation
    Point3Float boundMinT, boundMaxT;
    GetBoundTransform( boundMinT, boundMaxT );

    // expand the bounding box for viewing (by expandRatio on each side)
    float xs = boundMaxT.GetX() - boundMinT.GetX();
    float ys = boundMaxT.GetY() - boundMinT.GetY();
    float zs = boundMaxT.GetZ() - boundMinT.GetZ();

    if( xs<=0 )
    {
        viewLeft  = -1;
        viewRight =  1;
    }
    else
    {
        xs *= expandRatio;
        viewLeft   = boundMinT.GetX() - xs;
        viewRight  = boundMaxT.GetX() + xs;
    }

    if( ys<=0 )
    {
        viewBottom = -1;
        viewTop    =  1;
    }
    else
    {
        ys *= expandRatio;
        viewBottom = boundMinT.GetY() - ys;
        viewTop    = boundMaxT.GetY() + ys;
    }

    if( zs<=0 )
    {
        viewFar  = 1;
        viewNear = -1;
    }
    else
    {
        viewFar   = -(boundMinT.GetZ() - zs);
        viewNear  = -(boundMaxT.GetZ() + zs);
    }

    // adjust the view depending on the window aspect ratio
    AdjustViewAspectRatio();
}

///////////////////////////////////////////////////////////////////////
// Function name:    AdjustViewAspectRatio
// Function purpose: Adjust the viewing depending on the window aspect ratio
// Input: None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::AdjustViewAspectRatio()
{
    float viewWid = viewRight - viewLeft;
    float viewHei = viewTop - viewBottom;
    float viewAspectRatio = viewWid / viewHei;

    float winWid = viewportMaxX - viewportMinX;
    float winHei = viewportMaxY - viewportMinY;
    if( winWid <=0 || winHei <=0 )
        return;

    float winAspectRatio = winWid / winHei;

    if( viewAspectRatio > winAspectRatio )
    {
        float h1 = viewWid / winAspectRatio;
        float h2 = (h1 - viewHei) * .5;
        viewTop += h2;
        viewBottom -= h2;
    }
    else if( viewAspectRatio < winAspectRatio )
    {
        float w1 = viewHei * winAspectRatio;
        float w2 = (w1 - viewWid) * .5;
        viewLeft -= w2;
        viewRight += w2;
    }
}

///////////////////////////////////////////////////////////////////////
// Function name:    AdjustViewDepth
// Function purpose: Adjust the viewing depth 
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::AdjustViewDepth()
{
    // the bounding box after the transformation
    Point3Float boundMinT, boundMaxT;
    GetBoundTransform( boundMinT, boundMaxT );

    float zs = boundMaxT.GetZ() - boundMinT.GetZ();
    if( zs <= 0 )
    {
        viewFar   =  1;
        viewNear  = -1;
    }
    else
    {
        // expand 100% on z
        viewFar   = -(boundMinT.GetZ() - zs);
        viewNear  = -(boundMaxT.GetZ() + zs);
    }
}

///////////////////////////////////////////////////////////////////////
// Function name:    GetBoundTransform
// Function purpose: Get the axis-aligned bounding box after the transformation
// Input:  None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::GetBoundTransform(Point3Float& boundTransformMin, Point3Float& boundTransformMax)
{
    Point3Float cen = boundBox.GetCenter();
    Point3Float ax[] = { boundBox.GetAxis(0)*boundBox.GetSpan(0), 
                         boundBox.GetAxis(1)*boundBox.GetSpan(1),
                         boundBox.GetAxis(2)*boundBox.GetSpan(2) };

    Point3Float ptT = transform * cen;
    boundTransformMin = ptT;
    boundTransformMax = ptT;

    for(int i=-1; i<=1; i+=2)
    {
        Point3Float pz = cen + ax[2]*i;
        for(int j=-1; j<=1; j+=2)
        {
            Point3Float py = pz + ax[1]*j;
            for(int k=-1; k<=1; k+=2)
            {
                ptT = transform * (py + ax[0]*k);

                if( ptT.GetX() < boundTransformMin.GetX() )
                    boundTransformMin.SetX( ptT.GetX() );
                if( ptT.GetY() < boundTransformMin.GetY() )
                    boundTransformMin.SetY( ptT.GetY() );
                if( ptT.GetZ() < boundTransformMin.GetZ() )
                    boundTransformMin.SetZ( ptT.GetZ() );

                if( ptT.GetX() > boundTransformMax.GetX() )
                    boundTransformMax.SetX( ptT.GetX() );
                if( ptT.GetY() > boundTransformMax.GetY() )
                    boundTransformMax.SetY( ptT.GetY() );
                if( ptT.GetZ() > boundTransformMax.GetZ() )
                    boundTransformMax.SetZ( ptT.GetZ() );
            }
        }
    }
}

// Process the mouse press event
// Left mouse button click
// Middle ................
//  - pass onto the manager if exists. Otherwise, nothing.
// Right mouse button click
//  - pass onto the manager if exists. Otherwise, create the default context menu.
void Viewport::ProcessMousePressEvent( QMouseEvent * event )
{
    keyModifier = event->modifiers();
	bool executeDefaultManager = managerStack.empty();
    switch( event->button() )
    {
    case Qt::LeftButton:
        if( measureMode != None )
        {
			// alt key during screen measurement -> assistant lines
			if((measureMode == ScreenDistance || measureMode == EntityDistance && !Is3D() ||
				measureMode == ScreenRadius || measureMode == EntityRadius && !Is3D() ||
				measureMode == ScreenAngle || measureMode == EntityAngle && !Is3D() ) && keyModifier==Qt::AltModifier )
				UpdateAssistLine(event->pos());
			else
				DoMeasure(event->pos());
            break;
        }
        else if( !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseLeft(event->pos(), keyModifier, this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseLeft(event->pos(), keyModifier, this);
		break;

    case Qt::MiddleButton:
        // Middle mouse click
        if( !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseMiddle(event->pos(), keyModifier, this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseMiddle(event->pos(), keyModifier, this);

        break;

    case Qt::RightButton:
        // Right mouse click
        if( !managerStack.empty() )
			if( !(executeDefaultManager = managerStack.top()->MouseRight(event->pos(), keyModifier, this)) )
				break;

		if(executeDefaultManager && defaultManager)
			if(!defaultManager->MouseRight(event->pos(), keyModifier, this))
				break;

        // Default context menu if the manager doesn't process the event
        SetupContextMenu();
        contextMenu->exec(QCursor::pos());
        break;
    }
	
    mouseBegin = mouseLast = event->pos();
}

// Process the mouse move event
// Mouse middle button down 
//   - no modifier: rotate
//   - shift : zoom in /out
//   - ctrl : pan
// Mouse left button
//   - pass onto the manager if exists. Otherwise, nothing.
// No mouse button
//   - pass onto the manager if exists. Otherwise, set the default arrow cursor.
void Viewport::ProcessMouseMoveEvent( QMouseEvent * event )
{
	keyModifier = event->modifiers();
	QPoint pt = event->pos() - mouseLast;
	bool executeDefaultManager = managerStack.empty();
	float sx,sy;

    switch(event->buttons())
    {
    case Qt::MidButton:
        if( !managerStack.empty() && ! (executeDefaultManager = managerStack.top()->MouseMiddleMove(event->pos(), event->modifiers(), this)))
			break;

		if(executeDefaultManager && defaultManager && !defaultManager->MouseMiddleMove(event->pos(), event->modifiers(), this))
			break;

        switch( keyModifier )
        {
        case Qt::NoModifier:
            // rotate
            {
                int xp = mouseBegin.x();
                int yp = widget->height()-1 - mouseBegin.y();
                bool rotXonly = ( xp < viewportMinX + SIDE_SCROLL_OFFSET || xp > viewportMaxX - SIDE_SCROLL_OFFSET );
                bool rotYonly = ( yp < viewportMinY + SIDE_SCROLL_OFFSET || yp > viewportMaxY - SIDE_SCROLL_OFFSET );
                sx = 360.0 * pt.x() / viewportWidth();
                sy = 360.0 * pt.y() / viewportHeight();
                if( rotXonly )
                {
					txtRotation.setNum(360.0 * (event->pos().y() - mouseBegin.y()) / viewportHeight(), 'f', 1);
					RotateX(sy);
                }
                else if( rotYonly )
                {
					txtRotation.setNum(360.0 * (event->pos().x() - mouseBegin.x()) / viewportWidth(), 'f', 1);
					RotateY(sx);
                }
                else
					RotateYX(sx, sy);
            }
            break;

        case Qt::ShiftModifier:
            Zoom(pt.y()<0, mouseBegin.x(), widget->height()-1 - mouseBegin.y());
            break;

        case Qt::ControlModifier:
            // pan
			Shift(-pt.x() * (viewRight - viewLeft) / viewportWidth(), pt.y() * (viewTop - viewBottom) / viewportHeight());
            break;

        case Qt::AltModifier:
			RotateZ(360.0f * pt.x() / viewportWidth());
            break;
        }
        widget->setCursor( Qt::ArrowCursor );
        break;

    case Qt::LeftButton:
        if( measureMode==None && !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseLeftMove(event->pos(), event->modifiers(), this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseLeftMove(event->pos(), event->modifiers(), this);

        break;

	case Qt::RightButton:
        if( measureMode==None && !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseRightMove(event->pos(), event->modifiers(), this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseRightMove(event->pos(), event->modifiers(), this);

        break;

    case Qt::NoButton:
        if( measureMode != None )
		{
			// alt key during screen measurement -> assistant lines
			if((measureMode == ScreenDistance || measureMode == EntityDistance && !Is3D() ||
				measureMode == ScreenRadius || measureMode == EntityRadius && !Is3D() ||
				measureMode == ScreenAngle || measureMode == EntityAngle && !Is3D() ) && 
				((lineAssist.size()>>1)<<1) != lineAssist.size() )
			{
				if( keyModifier==Qt::AltModifier )
				{
					UpdateAssistLine(event->pos());
					lineAssist.pop_back();
				}
				else
				{
					lineAssist.pop_back();
					widget->updateGL();
				}
			}
			else
			{
				widget->setCursor( measureCursor );
				const int n = (int) entyMeasure.size();
				if( measureMode == ScreenDistance || measureMode == EntityDistance && !Is3D() )
				{
					if( n==1 )
						DoMeasure(event->pos());
				}
				else if( measureMode == ScreenRadius || measureMode == EntityRadius && !Is3D() ) 
				{
					if( n==1 || n==2 )
						DoMeasure(event->pos());
				}
				else if( measureMode == ScreenAngle || measureMode == EntityAngle && !Is3D() )
				{
					if( n==1 || n==3 )
						DoMeasure(event->pos());
				}
				if( entyMeasure.size()==n+1 )
				{
					entyMeasure.pop_back();
					resMeasure.Reset();
				}
			}
		}
        else
        {
            if( !managerStack.empty() && !(executeDefaultManager = managerStack.top()->MouseNoneMove(event->pos(), event->modifiers(), this)) )
                break;

			if(executeDefaultManager && defaultManager && !defaultManager->MouseNoneMove(event->pos(), event->modifiers(), this))
				break;

            widget->setCursor( Qt::ArrowCursor );
        }
        break;
    }

    mouseLast = event->pos();
}

// Process the mouse release event
void Viewport::ProcessMouseReleaseEvent( QMouseEvent * event )
{
	bool executeDefaultManager = managerStack.empty();
    switch( event->button() )
    {
    case Qt::LeftButton:
        if( measureMode==None && !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseLeftUp(event->pos(), this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseLeftUp(event->pos(), this);

        break;

    case Qt::MiddleButton:
        if( measureMode==None && !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseMiddleUp(event->pos(), this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseMiddleUp(event->pos(), this);

        break;

    case Qt::RightButton:
        if( measureMode==None && !managerStack.empty() )
            executeDefaultManager = managerStack.top()->MouseRightUp(event->pos(), this);

		if(executeDefaultManager && defaultManager)
			defaultManager->MouseRightUp(event->pos(), this);

        break;
    }

    if( !txtRotation.isEmpty() )
    {
        txtRotation.clear();
        widget->update();
    }
}

void Viewport::ProcessDoubleClickEvent( QMouseEvent * event )
{
	if(event->button()!=Qt::LeftButton)
		return;

	bool executeDefaultManager = managerStack.empty();
    if( !managerStack.empty() && 
        !(executeDefaultManager = managerStack.top()->MouseDoubleClickLeft(event->pos(), this)) )
        return;

	if(executeDefaultManager && defaultManager && !defaultManager->MouseDoubleClickLeft(event->pos(), this))
		return;

    SetMaximized( !maximized );
}

// Process the mouse wheel event
// - pass onto the manager if exists. Otherwise, zoom in/out the viewport
void Viewport::ProcessWheelEvent ( QWheelEvent * ev )
{
	//first try to execute the default manager
	if(defaultManager && !defaultManager->MouseWheel(ev, this))
		return;

    // let the manager process the event
    if( !managerStack.empty() && !managerStack.top()->MouseWheel(ev, this) )
        return;

	if(ev->modifiers() == Qt::ControlModifier)
		return;

    // otherwise, zoom in/out
    if( ev->delta() > 0 )
	{
		const float factorOut = 1.1;
        Zoom(factorOut, ev->x(), widget->height()-1 - ev->y());
	}
    else if( ev->delta() < 0 )
	{
		const float factorIn  = 0.9;
        Zoom(factorIn, ev->x(), widget->height()-1 - ev->y());
	}
}

void Viewport::ProcessKeyReleaseEvent( QKeyEvent* ev )
{
	bool executeDefaultManager = managerStack.empty();
    if (!managerStack.empty() && !(executeDefaultManager = managerStack.top()->keyReleaseEvent(ev, this)))
        return;

	if(executeDefaultManager && defaultManager && !defaultManager->keyReleaseEvent(ev, this))
		return;
}


void Viewport::ProcessFocusInEvent(QFocusEvent* ev)
{
	bool executeDefaultManager = managerStack.empty();
    if (!managerStack.empty() && !(executeDefaultManager = managerStack.top()->focusInEvent(ev, this)))
        return;

	if(executeDefaultManager && defaultManager && !defaultManager->focusInEvent(ev, this))
		return;
}

void Viewport::ProcessFocusOutEvent(QFocusEvent* ev)
{
	bool executeDefaultManager = managerStack.empty();
    if (!managerStack.empty() && !(executeDefaultManager = managerStack.top()->focusOutEvent(ev, this)))
        return;

	if(executeDefaultManager && defaultManager && !defaultManager->focusOutEvent(ev, this))
		return;
}

///////////////////////////////////////////////////////////////////////
// Function name:    ProcessKeyPressEvent
// Function purpose: Called when a key is pressed. It currently handles:
//   Arrow left/right   : rotate around the screen y axis
//   Arrow up/down      : rotate around the screen x axis
//   Shift + Arrow left/right : rotate around the screen y axis by 90 degrees
//   Shift + Arrow up/down    : rotate around the screen x axis by 90 degrees
//   Alt + Arrow left/right : rotate around the z axis, which is perpendicular to the screen
//   Ctrl + Arrow keys      : pan 
//   Ctrl+R: redraw
//   F  : zoom to fit
//   Z/-  : zoom out
//   Shift + Z/+ : zoom in
//   Ctrl + 1: Font view
//   Ctrl + 2: Back view
//   Ctrl + 3: Left view
//   Ctrl + 4: Right view
//   Ctrl + 5: Top view
//   Ctrl + 6: Bottom view
//   Ctrl + 7: Iso view
// Input:
//   event : the key event
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::ProcessKeyPressEvent( QKeyEvent* event )
{
	bool executeDefaultManager = managerStack.empty();
    // Escape when measuring
	// - if the measurement buffer is not empty, clear it;
	// - otherwise, cancel the measurement tool
    if( measureMode != None && event->key()==Qt::Key_Escape )
    {
		if( entyMeasure.empty() )
		{
			mainWindow->CancelMeasure();
			widget->setCursor( Qt::ArrowCursor );
		}
		else
		{
			entyMeasure.clear();
			resMeasure.Reset();
			lineAssist.clear();
			widget->updateGL();
		}
		return;
    }

    if( !managerStack.empty() && !(executeDefaultManager = managerStack.top()->keyPressEvent(event, this)) )
        return;

	if(executeDefaultManager && defaultManager && !defaultManager->keyPressEvent(event, this))
		return;

    const float step = 5;

    switch( event->key() )
    {
    case Qt::Key_Left:
        if( event->modifiers() == Qt::NoModifier )
			RotateY(-step);
        else if( event->modifiers() == Qt::AltModifier )
			RotateZ(-step);
        else if( event->modifiers() == Qt::ShiftModifier )
			RotateY(-90);
        else if( event->modifiers() == Qt::ControlModifier )
			Shift((viewRight - viewLeft)*.01, 0);
        break;

    case Qt::Key_Right:
        if( event->modifiers() == Qt::NoModifier )
			RotateY(step);
        else if( event->modifiers() == Qt::AltModifier )
			RotateZ(step);
        else if( event->modifiers() == Qt::ShiftModifier )
			RotateY(90);
        else if( event->modifiers() == Qt::ControlModifier )
			Shift((viewLeft - viewRight)*.01, 0);
        break;

    case Qt::Key_Down:
        if( event->modifiers() == Qt::NoModifier )
			RotateX(step);
        else if( event->modifiers() == Qt::ShiftModifier )
			RotateX(90);
        else if( event->modifiers() == Qt::ControlModifier )
			Shift(0, (viewTop - viewBottom)*.01);
        break;

    case Qt::Key_Up:
        if( event->modifiers() == Qt::NoModifier )
			RotateX(-step);
        else if( event->modifiers() == Qt::ShiftModifier )
			RotateX(-90);
        else if( event->modifiers() == Qt::ControlModifier )
			Shift(0, (viewBottom - viewTop)*.01);
        break;

    // Refresh
    case Qt::Key_R:
        if( event->modifiers() == Qt::ControlModifier )
            widget->updateGL();
        break;

    // Best fit
    case Qt::Key_F:
        BestFitView();
        break;

    // Zoom out
    case Qt::Key_Minus:
    case Qt::Key_Underscore:
        Zoom(false);
        break;

    // Zoom in
    case Qt::Key_Plus:
    case Qt::Key_Equal:
        Zoom(true);
        break;

    // Zoom
    case Qt::Key_Z:
        if( event->modifiers() == Qt::NoModifier )
            // zoom out 
            Zoom(false);
        else if( event->modifiers() == Qt::ShiftModifier )
            // zoom in
            Zoom(true);
        break;

    // front view
    case Qt::Key_1:
        if( event->modifiers() == Qt::ControlModifier )
            FrontView();
        break;
    // back view
    case Qt::Key_2:
        if( event->modifiers() == Qt::ControlModifier )
            BackView();
        break;
    // left view
    case Qt::Key_3:
        if( event->modifiers() == Qt::ControlModifier )
            LeftView();
        break;
    // right view
    case Qt::Key_4:
        if( event->modifiers() == Qt::ControlModifier )
            RightView();
        break;
    // top view
    case Qt::Key_5:
        if( event->modifiers() == Qt::ControlModifier )
            TopView();
        break;
    // bottom view
    case Qt::Key_6:
        if( event->modifiers() == Qt::ControlModifier )
            BottomView();
        break;
    // iso view
    case Qt::Key_7:
        if( event->modifiers() == Qt::ControlModifier )
            IsoView();
        break;
	case Qt::Key_8:
		if( event->modifiers() == Qt::ControlModifier )
			IsoBackView();
		break;
    }
}

void Viewport::RotateX(float degree)
{
    transform.RotateX(degree);
    AdjustViewDepth();
    widget->updateGL();
	if(predefinedViews)	predefinedViews->ResetToggles();
}

void Viewport::RotateY(float degree)
{
    transform.RotateY(degree);
    AdjustViewDepth();
    widget->updateGL();
	if(predefinedViews)	predefinedViews->ResetToggles();
}

void Viewport::RotateYX(float ydegree, float xdegree)
{
    transform.RotateY(ydegree);
	transform.RotateX(xdegree);
    AdjustViewDepth();
    widget->updateGL();
	if(predefinedViews)	predefinedViews->ResetToggles();
}

void Viewport::RotateZ(float degree)
{
	transform.RotateZ(degree);
    AdjustViewDepth();
    widget->updateGL();
	if(predefinedViews)	predefinedViews->ResetToggles();
}

void Viewport::Shift(float h, float v)
{
	viewLeft += h;
    viewRight += h;
	viewBottom += v;
    viewTop += v;
    widget->updateGL();
	if(predefinedViews)	predefinedViews->ResetToggles(true);
}

void Viewport::BestFitView()
{
    BestFit( Is3D() ? 0 : 0 );
    widget->updateGL();
}

void Viewport::ToggleMaximize()
{
    SetMaximized(!maximized);
}

void Viewport::SetMeasureMode(MeasureMode m)
{
    measureMode = m;
    entyMeasure.clear();
    resMeasure.Reset();
	lineAssist.clear();
}

void Viewport::ToggleShowAxis()
{
    showAxis = !showAxis;
    widget->updateGL();
}

void Viewport::ToggleShowAxisRotationRegion()
{
    showAxisRotationRegion = !showAxisRotationRegion;
    widget->updateGL();
}

void Viewport::InitPredefinedViewTransform()
{
    IwAxis2Placement cur = predefinedViews->GetCurrentSystem();
    Vector3Float tx = cur.GetXAxis();
    Vector3Float ty = cur.GetYAxis();
    Vector3Float tz = cur.GetZAxis();

    Vector3Float ax,ay,az;

    if( fabs(tx.x()) > fabs(ty.x()) )
    {
        if( fabs(tx.x()) > fabs(tz.x()) )
        {
            ax = tx;
            ay = ( fabs(ty.y()) > fabs(tz.y()) ) ? ty : tz;
        }
        else
        {
            ax = tz;
            ay = ( fabs(tx.y()) > fabs(ty.y()) ) ? tx : ty;
        }
    }
    else if( fabs(ty.x()) > fabs(tz.x()) )
    {
        ax = ty;
        ay = ( fabs(tx.y()) > fabs(tz.y()) ) ? tx : tz;
    }
    else
    {
        ax = tz;
        ay = ( fabs(tx.y()) > fabs(ty.y()) ) ? tx : ty;
    }
    if( ax.x() < 0 ) ax = -ax;
    if( ay.y() < 0 ) ay = -ay;
    az = ax.UnitCross(ay);

    transformTo.SetIdentity();
	transformTo.Translate( -centerRot );
    transformTo.Rotate( Rotate3Float(ax.x(), ax.y(), ax.z(), ay.x(), ay.y(), ay.z(), az.x(), az.y(), az.z()) );
}

// If autoRotZ180=true, it will automatically rotate along z-axis 180 degrees if needed.
void Viewport::FrontView(bool autoRotZ180)
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

    InitPredefinedViewTransform();

	transformTo.RotateX(-90.0);
    transformTo.RotateY(180.0);
    if( predefinedViews->GetFrontView() && autoRotZ180 )
        transformTo.RotateZ(180.0);

	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleFrontView(autoRotZ180);
}

// If autoRotZ180=true, it will automatically rotate along z-axis 180 degrees if needed.
void Viewport::BackView(bool autoRotZ180)
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

    InitPredefinedViewTransform();

	// The backview for femur coordinate systems is different from other coord systems.
	if (predefinedViews->GetCurrentSystemName() == "Fem Mech Axis" || predefinedViews->GetCurrentSystemName() == "Fem Implant Axis" )
	{
		transformTo.RotateZ(180.0);
		transformTo.RotateX(90.0);
	}
	else
	{
		transformTo.RotateX(-90.0);
	}
    if( predefinedViews->GetBackView() && autoRotZ180 )
        transformTo.RotateZ(180.0);

	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleBackView(autoRotZ180);
}

// If autoRotZ180=true, it will automatically rotate along z-axis 180 degrees if needed.
void Viewport::TopView(bool autoRotZ180)
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

    InitPredefinedViewTransform();

	if( predefinedViews->GetTopView() && autoRotZ180 )
        transformTo.RotateZ(180.0);

	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleTopView(autoRotZ180);
}

// If autoRotZ180=true, it will automatically rotate along z-axis 180 degrees if needed.
void Viewport::BottomView(bool autoRotZ180)
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

    InitPredefinedViewTransform();
    transformTo.RotateY( 180.0 );

	if( predefinedViews->GetBottomView() && autoRotZ180 )
		transformTo.RotateZ( 180.0 );

	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleBottomView(autoRotZ180);
}

void Viewport::ReAlignAlongXAxis(bool viewingFromLeftToRight /*which is along the positive x-axis*/)
{
	if(viewingFromLeftToRight)
	{
		LeftView();
	}
	else
	{
		RightView();
	}
}

void Viewport::ReAlignAlongYAxis(bool viewingFromBackToFront/*which is along the positive y-axis*/)
{
	if(viewingFromBackToFront)
	{
		BackView();
	}
	else
	{
		FrontView();
	}
}

void Viewport::ReAlignAlongZAxis(bool viewingFromBottomToTop/*which is along the positive z-axis*/)
{
	if(viewingFromBottomToTop)
	{
		BottomView();
	}
	else
	{
		TopView();
	}
}

// If autoAdjust=true, it will automatically rotate along z-axis 180 degrees if needed
// and automatically adjust to best fit into the view volume (done by AnimateTransform() ).
void Viewport::LeftView(bool autoAdjust)
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

    InitPredefinedViewTransform();

    transformTo.RotateX(-90);
    transformTo.RotateY(90);

	if( predefinedViews->GetLeftView() && autoAdjust )
		transformTo.RotateZ( 180.0 );

	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleLeftView(autoAdjust);
}

// If autoAdjust=true, it will automatically rotate along z-axis 180 degrees if needed
// and automatically adjust to best fit into the view volume (done by AnimateTransform() ).
void Viewport::RightView(bool autoAdjust)
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

    InitPredefinedViewTransform();
    transformTo.RotateX(-90);
    transformTo.RotateY(-90);

	if( predefinedViews->GetRightView() && autoAdjust )
		transformTo.RotateZ(  180.0 );
	
	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleRightView(autoAdjust);
}

void Viewport::LateralView(bool autoAdjust)
{
	if ( mainWindow->GetImplant()->GetInfo("KneeType") == "Left" )
	{
		LeftView(autoAdjust);
	}
	else
	{
		RightView(autoAdjust);
	}
}

void Viewport::MedialView(bool autoAdjust)
{
	if ( mainWindow->GetImplant()->GetInfo("KneeType") == "Left" )
	{
		RightView(autoAdjust);
	}
	else
	{
		LeftView(autoAdjust);
	}
}

void Viewport::IsoView()
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

	IwAxis2Placement systemPlacement = predefinedViews->GetCurrentSystem();

	double xAng, yAng, zAng;
	systemPlacement.DecomposeToAngles(xAng, yAng, zAng);

	transformTo.SetIdentity();
	transformTo.Translate( -centerRot );

	transformTo.RotateZ(-zAng*180/IW_PI);
	transformTo.RotateY(-yAng*180/IW_PI);
	transformTo.RotateX(-xAng*180/IW_PI);    
	
	if ( predefinedViews->GetIsoView() )
	{
		transformTo.RotateZ( 180 );
		transformTo.RotateX( -175 );
		transformTo.RotateY( -50 );
		transformTo.RotateZ( -9.1f );
	}
	else
	{
		transformTo.RotateZ( 180 );
		transformTo.RotateX( -175 );
		transformTo.RotateY(  40 );
		transformTo.RotateZ(  16.1f );
	}	
	
	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleIsoView();
}

void Viewport::IsoBackView()
{
	if(timerID != 0)
	{
        killTimer(timerID);
		timerID = 0;
	}

	IwAxis2Placement systemPlacement = predefinedViews->GetCurrentSystem();

	double xAng, yAng, zAng;
	systemPlacement.DecomposeToAngles(xAng, yAng, zAng);

	transformTo.SetIdentity();
	transformTo.Translate( -centerRot );

	transformTo.RotateZ(-zAng*180/IW_PI);
	transformTo.RotateY(-yAng*180/IW_PI);
	transformTo.RotateX(-xAng*180/IW_PI);    
	
	if ( predefinedViews->GetIsoBackView() )
	{
		transformTo.RotateX( 30 );
		transformTo.RotateY( -30 );
		transformTo.RotateZ( -16.1f );
	}
	else
	{
		transformTo.RotateX( 30 );
		transformTo.RotateY(  30 );
		transformTo.RotateZ(  16.1f );
	}	

	if(animateTransform)
		AnimateTransform();
	else
		transform = transformTo;

	predefinedViews->ToggleIsoBackView();
}

///////////////////////////////////////////////////////////////////////
// Function name:    Zoom In/Out
// Function purpose: Zoom in/out at the specified viewport coordinate
// Input: None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::Zoom(float zoomRatio, int viewX, int viewY)
{
	if( viewX<0 || viewY<0 )
	{
		// default to viewport center 
		viewX = viewportCenX();
		viewY = viewportCenY();
	}

    float rx = ((float)viewX - viewportMinX) / viewportWidth();
    float ry = ((float)viewY - viewportMinY) / viewportHeight();

    float xs = viewRight - viewLeft;
    float ys = viewTop - viewBottom;

    float cx = viewLeft + xs * rx;
    float cy = viewBottom + ys * ry;

    float xsr = xs * zoomRatio;
    float ysr = ys * zoomRatio;

    viewLeft  = cx - xsr * rx;
    viewRight = cx + xsr * (1-rx);
    viewBottom= cy - ysr * ry;
    viewTop   = cy + ysr * (1-ry);

	widget->updateGL();
	if(predefinedViews)	predefinedViews->ResetToggles(true);
}

void Viewport::Zoom(bool in, int viewX, int viewY)
{
	Zoom(in ? .98f : 1.02f, viewX, viewY);
}

///////////////////////////////////////////////////////////////////////
// Function name:    AnimateTransform
// Function purpose: Animate the transformation
// Input: None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::AnimateTransform()
{
    oldQ = transform.GetRotationQuaternion();
    newQ = transformTo.GetRotationQuaternion();
    oldT = transform.GetTranslationVector();
    newT = transformTo.GetTranslationVector();

    theta = acos(oldQ.dot(newQ));

    if( fabs(theta)<SMALL_VALUE && (oldT-newT).norm()<SMALL_VALUE )
    {
        SetTransform( transformTo );
        widget->updateGL();
    }
    else
    {
        timerLimit = 1;
        timerCount = 0;
        timerID = startTimer(1);
    }
}

void Viewport::timerEvent( QTimerEvent * event )
{
    ++timerCount;

    if( timerCount != timerLimit )
    {
        float ratio = ((float)timerCount) / timerLimit;

        transform.SetTranslationVector( oldT * (1-ratio) + newT * ratio );
        transform.SetRotationQuaternion( oldQ.slerp(ratio, newQ) );
    }
    else
    {
        killTimer(timerID);
		timerID = 0;
        transform = transformTo;
    }

    UpdateViewVolume();
    widget->updateGL();
}

// Add an entity
void Viewport::AddView(EntityView* v)
{
    assert( find(entityViews.begin(), entityViews.end(), v)==entityViews.end() );

    entityViews.push_back(v);

    connect(v, SIGNAL(showChanged(bool)), this, SLOT(AdjustBBoxDepth()));
}

// Remove an entity
void Viewport::RemoveView(EntityView* v)
{
    auto it = find(entityViews.begin(), entityViews.end(), v);

    if( it != entityViews.end() )
        entityViews.erase( it, it+1 );
}

// Check view exists or not
bool Viewport::IsViewExist(EntityView* ev)
{
    for(int i=0; i<entityViews.size(); i++)
    {
        if( entityViews[i] == ev )
        {
            return true;
        }
    }
	return false;
}

void Viewport::AdjustBBoxDepth()
{
    bool lastemptyBBox = emptyBBox;
    UpdateBoundingBox();

    if( lastemptyBBox && !emptyBBox ) // empty -> not empty : best fit
    {
        UpdateTransform();
        UpdateViewVolume();
    }
    else    // otherwise, just adjust the depth
        AdjustViewDepth();
}

// Add the manager to the top of the manager stack as the active manager.
void Viewport::AddManager(Manager* mgr)
{
    if( !managerStack.empty() && managerStack.top() == mgr )
        return;

    managerStack.push( mgr );
}

// Remove the manager if it is the current manager. If the manager stack isn't empty,
// replace the current manage with the one on the top of the stack.
void Viewport::RemoveManager(Manager* mgr)
{
    if( !managerStack.empty() && managerStack.top() == mgr )
        managerStack.pop();
}

void Viewport::Draw()
{
    glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho( viewLeft, viewRight, viewBottom, viewTop, viewNear, viewFar );
    glViewport(viewportMinX, viewportMinY, viewportWidth(), viewportHeight());

    // Model view
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    float f[16];
    transform.GetGLMatrix(f);
    glMultMatrixf( f );

    glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
    glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);

    //glEnable(GL_LINE_SMOOTH);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);

    // draw all entities
    DrawEntities();

    if( measureMode != None )
        DrawMeasure();

    // draw the axis
    if( showAxis )
        DrawAxis();

	if(showAxisRotationRegion)
	{
		if(viewportWidth() !=0 && viewportHeight() != 0 && this->Is3D())
		{
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			glOrtho(-1, 1, -1, 1, 1, -1);

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			float x = 1 - (float)(2*SIDE_SCROLL_OFFSET)/(float)viewportWidth();
			float y = 1 - (float)(2*SIDE_SCROLL_OFFSET)/(float)viewportHeight();

			glColor3f(0.0f, 1.0f, 0.0f);
			glBegin(GL_LINE_LOOP);
				glVertex3f(-x, -y, -1);
				glVertex3f( x, -y, -1);
				glVertex3f( x,  y, -1);
				glVertex3f(-x,  y, -1);
			glEnd();
		}
	}

    // draw the rotation angle text
    if( !txtRotation.isEmpty() )
    {
        glColor3ub( 128, 128, 128 );
		QFont font( "Tahoma", 7);
        QPoint pos = widget->mapFromGlobal( QCursor::pos() );
        widget->renderText( pos.x(), pos.y(), txtRotation, font );
    }

    // For debugging-------
    /*if( drawray )
    {
        glBegin(GL_LINES);
        glVertex3f(raycen.GetX()+1000*raydir.GetX(), raycen.GetY()+1000*raydir.GetY(), raycen.GetZ()+1000*raydir.GetZ());
        glVertex3f(raycen.GetX()-1000*raydir.GetX(), raycen.GetY()-1000*raydir.GetY(), raycen.GetZ()-1000*raydir.GetZ());
        glEnd();
    }*/


   if (mainWindow->IsReviewMode())
   {
      glPushAttrib( GL_ENABLE_BIT | GL_VIEWPORT_BIT );

      // backup the projection matrix and set up
      glMatrixMode( GL_PROJECTION );
      glPushMatrix();
      glLoadIdentity();
      glOrtho( 0, 1, 0, 1, -1, 1 );

      int wid = GetGLWidget()->width(), hei = GetGLWidget()->height();
      glViewport(0, 0, wid, hei);

      // backup the model view matrix, attrib.. and set up
      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();
      glLoadIdentity();
      glDisable( GL_LIGHTING );
      glDisable( GL_DEPTH_TEST );

	  float lineWidth;
	  glGetFloatv(GL_LINE_WIDTH, &lineWidth);

      glLineWidth(9);
      glColor3ub(163, 73, 164);
      glBegin( GL_LINE_LOOP );
         glVertex2d( 0, 0 );
         glVertex2d( 1, 0 );
         glVertex2d( 1, 1 );
         glVertex2d( 0, 1 );
      glEnd();

	  glLineWidth(lineWidth);

      // recover the matrices etc.
      glMatrixMode( GL_PROJECTION );
      glPopMatrix();
      glMatrixMode( GL_MODELVIEW );
      glPopMatrix();
      glPopAttrib();
   }

}

void Viewport::DrawEntities()
{
    // draw the opaque objects
    for(int i=0; i<entityViews.size(); i++)
    {
        EntityView* ev = entityViews[i];

        if( ev->IsShow() )
            ev->Draw();
    }

    // draw the manger if available
    if( !managerStack.empty() )
	{
        // draw the manager at the top
        managerStack.top()->Display(this);

        // also draws the next in line if not empty
        if ( managerStack.size()>1 )
            managerStack[1]->Display(this);
	}

	if(defaultManager)
		defaultManager->Display(this);

    // draw the transparent objects
    for(int i=0; i<entityViews.size(); i++)
    {
        EntityView* ev = entityViews[i];

        if( ev->IsShow() )
            ev->DrawTransparent();
    }

    // draw the manager transparently (if it wants to)
    if( !managerStack.empty() )
	{
        // draw the manager at the top
        managerStack.top()->DisplayTransparent(this);

        // also draws the next in line if not empty
        if ( managerStack.size()>1 )
            managerStack[1]->DisplayTransparent(this);
	}

	if(defaultManager)
		defaultManager->DisplayTransparent(this);
}


void Viewport::DrawAfterBuffersSwap()
{
    if( !managerStack.empty() )
	{
        // draw the manager at the top
        managerStack.top()->DisplayAfterBuffersSwap( this );
	}
}

///////////////////////////////////////////////////////////////////////
// Function name:    DrawAxis
// Function purpose: Draw the axis at the bottom left corner
// Input: None
// Output: None
///////////////////////////////////////////////////////////////////////
void Viewport::DrawAxis()
{
    QColor	xColor(192, 0, 0);
    QColor	yColor(0, 192, 0);
    QColor	zColor(0, 0, 192);

    QFont font;

    const int wid = 90;
    const int hei = 90;

    glPushAttrib( GL_ENABLE_BIT | GL_VIEWPORT_BIT );
    
    // backup the projection matrix and set up
    glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glOrtho( -1, 1, -1, 1, -2, 2 );
    glViewport(viewportMinX+2, viewportMinY+2, wid, hei);

    // backup the model view matrix, attrib.. and set up
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();

    // clear the depth buffer within the axis drawing area
    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_ALWAYS);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ZERO, GL_ONE);
    glBegin(GL_QUADS);
    glVertex3f(-1,-1,-2);
    glVertex3f( 1,-1,-2);
    glVertex3f( 1, 1,-2);
    glVertex3f(-1, 1,-2);
    glEnd();
    glPopAttrib();

    // apply rotation
    float f[16];
    transform.GetGLRotMatrix(f);
    glMultMatrixf( f );

    double radCyld = .05;
    double radCone = .1;
    double hCyld = .5;  // cylinder height
    double hCone = .25;  // cone height

    if( quad )
    {
        // draw x axis
        glPushMatrix();
        glColor3f( xColor.redF(), xColor.greenF(), xColor.blueF() );
        glRotated(90, 0, 1, 0);
        widget->renderText(0, 0, hCyld + hCone + .02, "X", font);
        gluCylinder(quad, radCyld, radCyld, hCyld, 10, 1);
        glTranslated(0, 0, hCyld);
        gluCylinder(quad, radCone, 0, hCone, 10, 1);
        glPopMatrix();

        // draw y axis
        glPushMatrix();
        glColor3f( yColor.redF(), yColor.greenF(), yColor.blueF() );
        glRotated(90, -1, 0, 0);
        widget->renderText(0, 0, hCyld + hCone + .02, "Y", font);
        gluCylinder(quad, radCyld, radCyld, hCyld, 10, 1);
        glTranslated(0, 0, hCyld);
        gluCylinder(quad, radCone, 0, hCone, 10, 1);
        glPopMatrix();

        // draw z axis
        glPushMatrix();
        glColor3f( zColor.redF(), zColor.greenF(), zColor.blueF() );
        widget->renderText(0, 0, hCyld + hCone + .02, "Z", font);
        gluCylinder(quad, radCyld, radCyld, hCyld, 10, 1);
        glTranslated(0, 0, hCyld);
        gluCylinder(quad, radCone, 0, hCone, 10, 1);
        glPopMatrix();
    }

    // recover the matrices etc.
    glMatrixMode( GL_PROJECTION );
	glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	glPopAttrib();
}

// Build the ray for picking at the cursor point
PickRayFloat Viewport::BuildPickRay(const QPoint& cursor)
{
	const float px = cursor.x();
    const float py = widget->height()-1-cursor.y();

    // get the view point and direction of viewing
    float x = viewLeft   + (px-viewportMinX) * viewWidth()  / viewportWidth();
    float y = viewBottom + (py-viewportMinY) * viewHeight() / viewportHeight();
    float z = -viewNear;

    Point3Float rayCen(x,y,z);
    Vector3Float rayDir(0, 0, -1);
	Rotate3Float irot = transform.GetRotationMatrix().Inverse();
    Vector3Float mov = transform.GetTranslationVector();

    rayCen = irot * (rayCen - mov);
    rayDir = irot * rayDir;

    return PickRayFloat(rayCen, rayDir);
}

// Build the ray for picking at the cursor point
PickRectFloat Viewport::BuildPickRect(const QPoint& c1, const QPoint& c2) const 
{
   // the idea is to calculate the transform such that when you take a 3d point
   // in the global coordinates and multiply (tform * point), you get the screen
   // coordinates and can compare them with the corners of the pick rectangle

   Rotate3Float rot = transform.GetRotationMatrix(); // existing 3x3 rotation matrix in this viewport
   Vector3Float mov = transform.GetTranslationVector(); // existing 3x1 translation vector

   // ------------- invert the following sequence in order to get 'rayCen' back into 'cursor'
    //const int px = cursor.x();
    //const int py = widget->height()-1-cursor.y();

    //// get the view point and direction of viewing
    //float x = viewLeft   + (px-viewportMinX) * viewWidth()  / viewportWidth();
    //float y = viewBottom + (py-viewportMinY) * viewHeight() / viewportHeight();
    //float z = -viewNear;

    //Point3Float rC(x,y,z);
    //Rotate3Float rot = transform.GetRotationMatrix();
    //Vector3Float mov = transform.GetTranslationVector();
    //Rotate3Float irot = rot.Inverse();

    //rayCen = irot * (rC - mov);

   //================================= comes down to
   //   rC = rot * rayCen + mov
   //   rC2 = rC - {viewLeft, viewBottom,0}', which means we could subtract 'viewLeft, viewBottom' from 'mov'
   mov.x() -= viewLeft;
   mov.y() -= viewBottom;

   //   rC2 = rotate * rayCen + trn1
   Rigid3Float tform_1(rot, mov);

   //  cursorX = viewportMinX                        + rC2[0] * viewportWidth() / viewWidth()
   //  cursorY = widget->height() - 1 + viewportMinY - rC2[1] * viewportHeight() / viewHeight()
   rot = Rotate3Float(viewportWidth() / viewWidth(), 0, 0, 0, -viewportHeight() / viewHeight(), 0, 0, 0, 1);
   mov = Vector3Float(viewportMinX, widget->height() - 1 + viewportMinY, 0);
   Rigid3Float tform_2(rot, mov);

    Point3Float lc1(c1.x(), c1.y(), -viewNear), lc2(c2.x(), c2.y(), -viewNear);

    return PickRectFloat(lc1, lc2, tform_1, tform_2);
}

QPoint Viewport::XYZtoUV( const Point3Float& pt )
{
    Point3Float pf = transform * pt;
    return QPoint( viewportMinX   + (pf.GetX() - viewLeft) * viewportWidth() / viewWidth(),
                   widget->height()-1 - (viewportMinY + (pf.GetY() - viewBottom ) * viewportHeight() / viewHeight()) );
}

Point3Float Viewport::UVtoXYZ( const QPoint& point )
{
    float x = viewLeft   + (point.x()-viewportMinX) * viewWidth()  / viewportWidth();
    float y = viewBottom + (widget->height()-1-point.y()-viewportMinY) * viewHeight() / viewportHeight();
    return transform.Inverse() * Point3Float(x,y,0);
}

void Viewport::XYZtoUVW( Point3Float& pt, int uvw[3] )
{
    Point3Float pf = transform * pt;
    uvw[0] = viewportMinX   + (pf.GetX() - viewLeft) * viewportWidth() / viewWidth();
    uvw[1] = widget->height()-1 - ( viewportMinY + (pf.GetY() - viewBottom ) * viewportHeight() / viewHeight());
    uvw[2] = pf.GetZ() * viewportWidth() / viewWidth();
}

void Viewport::UVWtoXYZ( int uvw[3], Point3Float& pt )
{
    float x = viewLeft   + (uvw[0]-viewportMinX) * viewWidth()  / viewportWidth();
    float y = viewBottom + (widget->height()-1-uvw[1]-viewportMinY) * viewHeight() / viewportHeight();
    pt = transform.Inverse() * Point3Float(x, y, uvw[2] * viewWidth() / viewportWidth());
}

void Viewport::UpdateBoundingBox()
{
    bool update = false;
    Point3Float boundMin( LARGE_VALUE, LARGE_VALUE, LARGE_VALUE);
    Point3Float boundMax(-LARGE_VALUE,-LARGE_VALUE,-LARGE_VALUE);

    for(int i=0; i<entityViews.size(); i++)
    {
        EntityView* ev = entityViews[i];
        if( !ev->IsShow() )
            continue;

        IwExtent3d box = ev->GetShowRegion();
        if( box.IsInit() )
            continue;

        IwPoint3d mn = box.GetMin();
        if( mn.x < boundMin.GetX() )
            boundMin.SetX( mn.x );
        if( mn.y < boundMin.GetY() )
            boundMin.SetY( mn.y );
        if( mn.z < boundMin.GetZ() )
            boundMin.SetZ( mn.z );

        IwPoint3d mx = box.GetMax();
        if( mx.x > boundMax.GetX() )
            boundMax.SetX( mx.x );
        if( mx.y > boundMax.GetY() )
            boundMax.SetY( mx.y );
        if( mx.z > boundMax.GetZ() )
            boundMax.SetZ( mx.z );

        update = true;
    }

    if( !managerStack.empty() )
    {
        // query the manager at the top for additional extents
        IwExtent3d box = managerStack.top()->GetShowRegion();
        if( !box.IsInit() )
        {
           IwPoint3d mn = box.GetMin();
           if( mn.x < boundMin.GetX() )
               boundMin.SetX( mn.x );
           if( mn.y < boundMin.GetY() )
               boundMin.SetY( mn.y );
           if( mn.z < boundMin.GetZ() )
               boundMin.SetZ( mn.z );

           IwPoint3d mx = box.GetMax();
           if( mx.x > boundMax.GetX() )
               boundMax.SetX( mx.x );
           if( mx.y > boundMax.GetY() )
               boundMax.SetY( mx.y );
           if( mx.z > boundMax.GetZ() )
               boundMax.SetZ( mx.z );

           update = true;
        }
    }

    emptyBBox = !update;
    boundBox.SetAxis(Point3Float(1,0,0), Point3Float(0,1,0), Point3Float(0,0,1));
    if( emptyBBox )
    {
        boundBox.SetCenter(0,0,0);
        boundBox.SetSpan(1,1,1);
        centerRot.SetXYZ(0,0,0);
    }
    else
    {
        centerRot = (boundMin+boundMax)*.5;
        boundBox.SetCenter( centerRot );
        boundBox.SetSpan( max((boundMax.GetX()-boundMin.GetX())*.5, 1.0),
                          max((boundMax.GetY()-boundMin.GetY())*.5, 1.0),
                          max((boundMax.GetZ()-boundMin.GetZ())*.5, 1.0) );
    }
}

// Measure based on the object at (px,py)
void Viewport::DoMeasure(const QPoint& cursor)
{
    const float tolerance = GetResolution_MMPerPixel()*5;
    bool find = false;
    MeasureEntity minEnt, ent;
    float minDist = 10000000;// A huge number

    switch( measureMode )
    {
    // entity measurement
    case EntityDistance:
    case EntityAngle:
    case EntityRadius:
        {
			const PickRayFloat ray = BuildPickRay(cursor);
			const Point3Float cen = ray.GetRayCen();
            MeasureSelectType mask = SelectAll;
            int entNeeded = 2;     // 2 entities for distance
            bool needUpdate = false;

			if ( measureMode==EntityDistance )
			{
				mask = SelectAll;
				entNeeded = 2;     // 2 entities for distance
				needUpdate = false;

                bool findPoint = false, findCurveEdge = false, findSurfaceFaceOpaque = false, findSurfaceFaceTransparent = false;
                MeasureEntity minEntPoint, minEntCurveEdge, minEntSurfaceFaceOpaque, minEntSurfaceFaceTransparent;
            
			    float minDistPoint = 10000000, minDistCurveEdge = 10000000, minDistSurfaceFaceOpaque = 10000000, minDistSurfaceFaceTransparent = 10000000;
			    // Previous entity
			    MeasureEntity* previousEntity=NULL;
			    if (entyMeasure.size() > 0 )
				    previousEntity = &(entyMeasure.at(entyMeasure.size()-1));

                // Find point, curveEdge, surfaceFace individually for EntityDistance
                int i = managerStack.empty() ? 0 : -1;
                while(i<(int) entityViews.size())
                {
                    EntityView* ev = (i>=0) ? entityViews[i] : nullptr;
                    Manager*    mg = (i< 0) ? managerStack.top() : nullptr;
                    
                    // Select Point
				    if( ev && ev->Measure(ray, tolerance, ent, SelectPoint, previousEntity) ||
                        mg && mg->Measure(ray, tolerance, ent, SelectPoint, previousEntity) )
				    {
					    float d = ent.GetHitPoint().Distance(cen);
					    if( d < minDistPoint )
					    {
						    findPoint = true;
						    minDistPoint = d;
						    minEntPoint=ent;
					    }
				    }
				    // Select CurveEdge
				    if( ev && ev->Measure(ray, tolerance, ent, ( SelectCircle | SelectLine | SelectCurve | SelectEdge), previousEntity ) ||
                        mg && mg->Measure(ray, tolerance, ent, ( SelectCircle | SelectLine | SelectCurve | SelectEdge), previousEntity ) )
				    {
					    float d = ent.GetHitPoint().Distance(cen);
					    if( d < minDistCurveEdge )
					    {
						    findCurveEdge = true;
						    minDistCurveEdge = d;
						    minEntCurveEdge=ent;
					    }
				    }
				    // Select SurfaceFace opaque
				    if( ev && ev->Measure(ray, tolerance, ent, (SelectPlane | SelectSurface | SelectFace), previousEntity ) ||
                        mg && mg->Measure(ray, tolerance, ent, (SelectPlane | SelectSurface | SelectFace), previousEntity ) )
				    {
					    float d = ent.GetHitPoint().Distance(cen);
					    if( d < minDistSurfaceFaceOpaque )
					    {
						    findSurfaceFaceOpaque = true;
						    minDistSurfaceFaceOpaque = d;
						    minEntSurfaceFaceOpaque=ent;
					    }
				    }
				    // Select SurfaceFace transparent
				    if( ev && ev->Measure(ray, tolerance, ent, (SelectFaceTransparent), previousEntity ) ||
                        mg && mg->Measure(ray, tolerance, ent, (SelectFaceTransparent), previousEntity ) )
				    {
					    float d = ent.GetHitPoint().Distance(cen);
					    if( d < minDistSurfaceFaceTransparent )
					    {
						    findSurfaceFaceTransparent = true;
						    minDistSurfaceFaceTransparent = d;
						    minEntSurfaceFaceTransparent=ent;
					    }
				    }
                    ++i;
					// skip rest if manager finds something
					if( (i<0) && (findPoint || findCurveEdge || findSurfaceFaceOpaque || findSurfaceFaceTransparent) )
						break;
                }

                // For entity distance measurement, the priority order is point, curveEdge, surfaceFace (opaque), surfaceFace(Transparent) 
				if ( findPoint )
				{
					minEnt = minEntPoint;
					if ( findCurveEdge && (minDistPoint-tolerance > minDistCurveEdge) )
						minEnt = minEntCurveEdge;
					else if ( findSurfaceFaceOpaque && (minDistPoint-tolerance > minDistSurfaceFaceOpaque) )
						minEnt = minEntSurfaceFaceOpaque;
				}
				else if ( findCurveEdge )
				{
					minEnt = minEntCurveEdge;
					if ( findSurfaceFaceOpaque && (minDistCurveEdge-tolerance > minDistSurfaceFaceOpaque) )
						minEnt = minEntSurfaceFaceOpaque;
				}
				else if ( findSurfaceFaceOpaque )
				{
					minEnt = minEntSurfaceFaceOpaque;
				}
				find = findPoint || findCurveEdge || findSurfaceFaceOpaque;
				if ( !find && findSurfaceFaceTransparent )
				{
					minEnt = minEntSurfaceFaceTransparent;
					find = true;
				}
			}
            else
            {
                if( measureMode==EntityAngle )
                {
                    // 2/3/4 entities for angle
                    mask = SelectPoint | SelectLinearCurveEdge | SelectPlanarSurfaceFace;
                    size_t n = entyMeasure.size();
                
                    if(n==1)
                    {
                        // if the first selection is a point, the next needs to be a point (for form a line)
                        if( entyMeasure[0].GetSelectType() == SelectPoint )
                        {
                            mask = SelectPoint;
                            entNeeded = 3;
                        }
                        else
                            needUpdate = true;
                    }
                    else if( n==2 )
                    {
                        if( entyMeasure[0].GetSelectType() == SelectPoint &&
                            entyMeasure[1].GetSelectType() == SelectPoint )
                        {
                            entNeeded = 3;
                            needUpdate = true;
                        }
                        else if( entyMeasure[1].GetSelectType() == SelectPoint )
                        {
                            mask = SelectPoint;
                            entNeeded = 3;
                        }
                    }
                    else if( n==3 )
                    {
                        if( entyMeasure[0].GetSelectType() == SelectPoint &&
                            entyMeasure[1].GetSelectType() == SelectPoint &&
                            entyMeasure[2].GetSelectType() == SelectPoint )
                        {
							mask = SelectPoint;
                            entNeeded = 4;
                        }
                    }
                }
                else if( measureMode==EntityRadius )
                {
                    entNeeded = 1;      // 1 entity for radius
                    mask = SelectNonLinearCurveEdge;
                }

                // measure using the views & top manager
                int i = managerStack.empty() ? 0 : -1;
                while(i<(int) entityViews.size())
                {
					if( (i>=0 && entityViews[i]->Measure(ray, tolerance, ent, mask)) ||
                        (i< 0 && managerStack.top()->Measure(ray, tolerance, ent, mask)) )
					{
						float d = ent.GetHitPoint().Distance(cen);
						if( !find || d < minDist )
						{
							find = true;
							minDist = d;
							minEnt=ent;
							// skip rest if manager finds something
							if( i<0 )
								break;
						}
					}
                    ++i;
				}
            }

            if( !find )
			{
				// if the buffer is full, reset it to clear up the selected entities.
				if ( entyMeasure.size()>=entNeeded )
				{
					entyMeasure.clear();
					resMeasure.Reset();
					widget->update();
				}
                return;
			}
			// Whether use "Shift" to pierce the entity for distance
			if ( QApplication::keyboardModifiers() == Qt::ShiftModifier && measureMode==EntityDistance )
				minEnt.UseHitPointToMeasure(true);
			else
				minEnt.UseHitPointToMeasure(false);

            kStatus.ShowMessage(QString("Pick point (%1, %2, %3)").arg(minEnt.GetHitPoint().x())
                .arg(minEnt.GetHitPoint().y()).arg(minEnt.GetHitPoint().z()));

            AddReplaceMeasure(minEnt, entNeeded);

            if( needUpdate )
            {
                size_t n = entyMeasure.size();
                if( entyMeasure[n-1].GetSelectType() == SelectPoint )
                    entNeeded = (int) n+1;
            }
    
            if( entyMeasure.size()==entNeeded )
                resMeasure = MeasureResult::Compute(measureMode, entyMeasure);
        }
        break;

    // screen measurement
    case ScreenDistance:
    case ScreenAngle:
    case ScreenRadius:
        {
			const PickRayFloat ray = AdjustPickRay(cursor);
			const Point3Float cen = ray.GetRayCen();

            // select an entity point first if possible
            int i = managerStack.empty() ? 0 : -1;
            while(i<(int) entityViews.size())
            {
                EntityView* ev = (i>=0) ? entityViews[i] : nullptr;
                Manager*    mg = (i< 0) ? managerStack.top() : nullptr;
                    
                if( ev && ev->Measure(ray, tolerance, ent, SelectPoint) ||
                    mg && mg->Measure(ray, tolerance, ent, SelectPoint) )
				{
					float d = ent.GetHitPoint().Distance(cen);
					if( d < minDist )
					{
						find = true;
						minDist = d;
                        minEnt = ent;
						// skip rest if manager finds something
						if( i<0 )
							break;
					}
				}
                ++i;
            }
            // if so, map it on the mid-bounding box plane
            Plane3Float plane(boundBox.GetCenter(), ray.GetRayDir());
			Point3Float pt = plane.Project( find ? minEnt.GetHitPoint() : plane.Project(cen) );
            kStatus.ShowMessage(QString("Pick point (%1, %2, %3)").arg(pt.x()).arg(pt.y()).arg(pt.z()));

            int entNeeded = 2;  // 2 points for distance
            if( measureMode==ScreenAngle )
                entNeeded = 4;  // 4 points (2 line segments) for angle
            else if( measureMode==ScreenRadius )
                entNeeded = 3;  // 3 points to fit a circle for radius
            AddReplaceMeasure(pt, entNeeded);

            if( entyMeasure.size()==entNeeded )
			{
				// re-project before measurement calculation in case the scene is rotated
				for(int j=0; j<entNeeded-1; j++)
					entyMeasure[j] = plane.Project( entyMeasure[j].GetHitPoint() );

				resMeasure = MeasureResult::Compute(measureMode, entyMeasure);
			}
			else if( measureMode==ScreenAngle && entyMeasure.size()==2 )
			{
				vector<MeasureEntity> em;
				em.push_back( plane.Project( entyMeasure[0].GetHitPoint() ) );
				em.push_back( plane.Project( entyMeasure[1].GetHitPoint() ) );
				Point3Float df = em[1].GetHitPoint() - em[0].GetHitPoint();
				float len = df.norm();
				if( len < SMALL_VALUE )
					len = 50;
				Point3Float ax = transform.GetRotationMatrix().Inverse() * Point3Float(1,0,0);
				if( df.dot(ax)<0 )
					ax = -ax;
				em.push_back(em[0]);
				Point3Float hp = em[0].GetHitPoint()+len*ax;
				em.push_back(hp);
				resMeasure = MeasureResult::Compute(ScreenAngle, em);
			}
        }
        break;
    }

    widget->updateGL();
}

void Viewport::UpdateAssistLine(const QPoint& cursor)
{
	const PickRayFloat ray = BuildPickRay(cursor);
	const Point3Float cen = ray.GetRayCen();
	Plane3Float plane(boundBox.GetCenter(), ray.GetRayDir());
	lineAssist.push_back( plane.Project( cen ) );
	widget->updateGL();
}

// Adjust the pick ray if 'Shift' is pressed during screen measurement
PickRayFloat Viewport::AdjustPickRay(const QPoint& cursor)
{
	PickRayFloat ray = BuildPickRay(cursor);

	// if shift is pressed, adjust cursor to align with horizontal/vertical/45 degree direction
	if( keyModifier==Qt::ShiftModifier && (
		((measureMode==EntityDistance || measureMode==ScreenDistance) && entyMeasure.size()==1) ||
		((measureMode==EntityAngle    || measureMode==ScreenAngle   ) && (entyMeasure.size()%2)==1) ||
		((measureMode==EntityRadius   || measureMode==ScreenRadius  ) && entyMeasure.size()>0) ) )
	{
		Rotate3Float irot = transform.GetRotationMatrix().Inverse();
		Vector3Float ax = irot * Vector3Float(1, 0, 0);
		Vector3Float ay = irot * Vector3Float(0, 1, 0);

		Plane3Float plane(boundBox.GetCenter(), ray.GetRayDir());
		Point3Float pre = plane.Project(entyMeasure.back().GetHitPoint());
		Point3Float cur = plane.Project(ray.GetRayCen());
		Vector3Float df = cur - pre;
		float dx = df.dot(ax);
		float dy = df.dot(ay);
		float angle = atan2(dy, dx) * 180 / PI;
		Point3Float cen = pre;
		if( angle>=-22.5 && angle<=22.5 || angle>157.5 || angle<-157.5 )
			cen += ax*dx;
		else if( angle>=67.5 && angle<=112.5 || angle<=-67.5 && angle>=-112.5 )
			cen += ay*dy;
		else
		{
			float v = (fabs(dx) + fabs(dy))*.5;
			cen += ax*(Sign(dx)*v) + ay*(Sign(dy)*v);
		}
		ray.SetRayCen(cen); // ray center isn't on the near plane, but enough for screen measure
	}
	return ray;
}

// add or replace a measure entity in the measurement buffer
void Viewport::AddReplaceMeasure(const MeasureEntity& ent, int bufferSizeBound)
{
	MeasureCloseTolerance = std::min(0.005f, GetResolution_MMPerPixel());

    // if the buffer is full, reset it first.
	int size = (int) entyMeasure.size();
    if( size>=bufferSizeBound )
    {
        entyMeasure.clear();
        resMeasure.Reset();
		size = 0;
    }
	
	// selectively skip if already in the buffer
	if( bufferSizeBound==4 )
	{
		if( size==1 || size==3 )
		{
			if( entyMeasure[size-1] == ent )
				return;
		}
	}
	else if( bufferSizeBound==3 && measureMode==EntityAngle && size==2 &&
			 entyMeasure[0].GetSelectType() == SelectPoint &&
             entyMeasure[1].GetSelectType() == SelectPoint )
	{
		; // skip the check
	}
	else
	{
		for(int i=0; i<size; i++)
		{
			if( entyMeasure[i] == ent )
				return;
		}
	}

	// add to the measurement buffer. 
    entyMeasure.push_back( ent );
}

void Viewport::DrawMeasure()
{
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glColor3ub(255,255,0);
    glPointSize(5);
    glLineWidth(2);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    // apply a small shift to highlight selected entities
    Point3Float shift = transform.GetRotationMatrix().Inverse() * Point3Float(0,0,0.1f);
    glTranslatef(shift.x(), shift.y(), shift.z());

    if( measureMode == ScreenDistance || measureMode == ScreenAngle || measureMode == ScreenRadius )
        glDisable(GL_DEPTH_TEST);

    // draw the measurement entities
	double res = GetResolution_MMPerPixel();
	const size_t n = entyMeasure.size();
    for(size_t i=0; i<n; i++)
        entyMeasure[i].Draw(res);

	// draw lines between points
	if( (measureMode == ScreenAngle || measureMode == EntityAngle) && n>=2 )
	{
		glBegin(GL_LINES);

		if( entyMeasure[0].GetSelectType()==SelectPoint )
		{
			for(size_t i=0; i<2; i++)
			{
				const Point3Float& pt = entyMeasure[i].GetHitPoint();
				glVertex3f(pt.x(), pt.y(), pt.z());
			}
			if( n>=4 )
			{
				for(size_t i=2; i<4; i++)
				{
					const Point3Float& pt = entyMeasure[i].GetHitPoint();
					glVertex3f(pt.x(), pt.y(), pt.z());
				}
			}
		}
		else if( entyMeasure[1].GetSelectType()==SelectPoint && n>=3 )
		{
			for(size_t i=1; i<3; i++)
			{
				const Point3Float& pt = entyMeasure[i].GetHitPoint();
				glVertex3f(pt.x(), pt.y(), pt.z());
			}
		}

		glEnd();
	}
	else if( measureMode == ScreenRadius || measureMode == EntityRadius && !Is3D() )
	{
		glBegin(GL_LINE_STRIP);

		for(size_t i=0; i<n; i++)
		{
			const Point3Float& pt = entyMeasure[i].GetHitPoint();
			glVertex3f(pt.x(), pt.y(), pt.z());
		}

		glEnd();
	}

	glColor3ub(255,0,0);
	glPointSize(7);
    // draw the measurement result
    resMeasure.Draw(res);

	// draw the assistant lines
	if( !lineAssist.empty() )
	{
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0x0f0f);
		glColor3ub(0,255,0);
		glBegin(GL_LINES);
		size_t n = (lineAssist.size()>>1)<<1;
		for(size_t i=0; i<n; i++)
			glVertex3fv(lineAssist[i].data());
		glEnd();
	}

    glPopMatrix();
    glPopAttrib();
}

// Get the status by filling in 'elem'
void Viewport::GetStatus(QDomElement& elem)
{
	elem.setTagName("View3D");

	AddParams(elem);

	if( GetNumberOfCoordSystems()>1 )
        AppendNode(elem, "CoordSystem", GetCurrentCoordSystemName());
}

// Set the status based on the content of 'elem'
void Viewport::SetStatus(QDomElement elem)
{
	if( elem.tagName() != "View3D" )
		throw exception("Incorrect content");

	QDomNodeList nodes = elem.childNodes();
	const int size = nodes.size();

	// set the coordinate system
	for(int j=0; j<size; j++)
	{
		QDomElement e = nodes.item(j).toElement();
		if( e.tagName() == "CoordSystem" )
		{
			QString coordName;
            ParseNode(e, coordName);
			SetCurCoordSystemName(coordName);
			break;
		}
	}

	ParseParams(elem);
}

void Viewport::AddParams(QDomElement& elem)
{
	AppendNode(elem, "Rotate", transform.GetRotationMatrix());
    AppendNode(elem, "Translate", transform.GetTranslationVector());

    AppendNode(elem, "BoundingBox", boundBox);

	vector<float> range;
	range.reserve(6);
	range.push_back(viewLeft);
	range.push_back(viewRight);
	range.push_back(viewBottom);
	range.push_back(viewTop);
	range.push_back(viewNear);
	range.push_back(viewFar);
    AppendNode(elem, "Range", range);

    AppendNode(elem, "Maximize", maximized);
}

void Viewport::ParseParams(QDomElement elem)
{
	QDomNodeList nodes = elem.childNodes();
	const int size = nodes.size();
	Rigid3Float tr;

	for(int j=0; j<size; j++)
    {
        QDomElement e = nodes.item(j).toElement();
        if( e.tagName() == "Rotate" )
        {
            Rotate3Float r;
            if( !ParseNode(e,r) )
                throw exception("Incorrect content");
            tr.SetRotationMatrix( r );
        }
        else if( e.tagName() == "Translate" )
        {
            Vector3Float t;
            if( !ParseNode(e,t) )
                throw exception("Incorrect content");
            tr.SetTranslationVector( t );
        }
        else if( e.tagName() == "BoundingBox" )
        {
            Box3Float box;
            if( !ParseNode(e, box) )
                throw exception("Incorrect content");
            SetBoundingBox(box);
        }
        else if( e.tagName() == "Range" )
        {
            vector<float> vs;
            if( !ParseNode(e, 6, vs) )
                throw exception("Incorrect content");
            SetViewRange(vs[0], vs[1], vs[2], vs[3], vs[4], vs[5]);
        }
        else if( e.tagName() == "Maximize" )
        {
            int v;
            if( !ParseNode(e, v)  )
                throw exception("Incorrect content");
            SetMaximized(v);
        }
    }
    SetTransform( tr );
}
