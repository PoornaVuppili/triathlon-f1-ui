#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "AnteriorSurfaceJigs.h"
#include "MakeAnteriorSurfaceJigs.h"
#include "MakeInnerSurfaceJigs.h"
#include "FemoralPart.h"
#include "InnerSurfaceJigs.h"
#include "CartilageSurface.h"
#include "OutlineProfileJigs.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "AdvancedControlJigs.h"
#include "UndoRedoCmdJigs.h"
#include "..\KApp\SwitchBoard.h"

CMakeAnteriorSurfaceJigs::CMakeAnteriorSurfaceJigs( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
	bool activateUI = (manActType == MAN_ACT_TYPE_EDIT || m_manActType == MAN_ACT_TYPE_REVIEW);

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CMakeAnteriorSurfaceJigs";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actCancel = NULL;
	m_predefinedViewIndex = 0;

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Ant Surface Jigs");
		m_actMakeAnteriorSurfaceJigs = m_toolbar->addAction("AntSurf");
		SetActionAsTitle( m_actMakeAnteriorSurfaceJigs );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );
	}
	else if (manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Ant Surface Jigs");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("Back");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Done");
		EnableAction( m_actReviewNext, true );
		m_actReviewValidateJigs = m_toolbar->addAction("E Report");
		EnableAction( m_actReviewValidateJigs, true );

		m_actMakeAnteriorSurfaceJigs = m_toolbar->addAction("AntSurf");
		SetActionAsTitle( m_actMakeAnteriorSurfaceJigs );
		m_pMainWindow->connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		m_pMainWindow->connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		m_pMainWindow->connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
		m_pMainWindow->connect( m_actReviewValidateJigs, SIGNAL( triggered() ), this, SLOT( OnReviewValidateJigs() ) );
	}

	CAnteriorSurfaceJigs* anteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();
	if ( anteriorSurfaceJigs == NULL  ) // if no object
	{
		if ( anteriorSurfaceJigs == NULL )
		{
			anteriorSurfaceJigs = new CAnteriorSurfaceJigs( m_pDoc, ENT_ROLE_ANTERIOR_SURFACE_JIGS );
			m_pDoc->AddEntity( anteriorSurfaceJigs, true );
		}

		// Reset is the way to make inner surface jigs.
		Reset( activateUI );
	}
	else
	{
		// Call reset to update inner surface jigs
		if ( !anteriorSurfaceJigs->GetUpToDateStatus() )
			Reset( activateUI );
		else
			DetermineLateralCutPeakPointAndF3Profile();
	}

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

	m_pView->Redraw();
}

CMakeAnteriorSurfaceJigs::~CMakeAnteriorSurfaceJigs()
{
	CAnteriorSurfaceJigs* anteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();
	if ( anteriorSurfaceJigs != NULL )
	{
		int previoisDesignTime = anteriorSurfaceJigs->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		anteriorSurfaceJigs->SetDesignTime(previoisDesignTime+thisDesignTime);
	}
	SwitchBoard::removeSignalSender(this);
}

void CMakeAnteriorSurfaceJigs::CreateAnteriorSurfaceJigsObject(CTotalDoc* pDoc)
{

	if ( pDoc->GetPart(ENT_ROLE_ANTERIOR_SURFACE_JIGS) == NULL )
	{
		CAnteriorSurfaceJigs* pAnteriorSurfaceJigs = new CAnteriorSurfaceJigs(pDoc, ENT_ROLE_ANTERIOR_SURFACE_JIGS);
		pDoc->AddEntity(pAnteriorSurfaceJigs, true);
	}

	return;
}

void CMakeAnteriorSurfaceJigs::OnReset()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////
void CMakeAnteriorSurfaceJigs::Reset(bool activateUI)
{
	m_pDoc->AppendLog( QString("CMakeAnteriorSurfaceJigs:Reset()") );

	CAnteriorSurfaceJigs* pAnteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();
	if ( pAnteriorSurfaceJigs == NULL )
		return;

	CInnerSurfaceJigs* pInnerSurfaceJigs = m_pDoc->GetInnerSurfaceJigs();
	if ( pInnerSurfaceJigs == NULL )
		return;

	CFemoralPart* pOsteoSurf = m_pDoc->GetOsteophyteSurface();
	if ( pOsteoSurf == NULL )
		return;

	CCartilageSurface* pCartiSurf = m_pDoc->GetCartilageSurface();
	if ( pCartiSurf == NULL )
		return;

	// femoral axes
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();
	double refSize = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize();

	// Get osteophyte 3 surfaces
	IwTArray<IwSurface*> totalOffsetSurfaces;

	// Get osteophyte offset surfaces
	IwBrep* osteoOffsetBrep;
	IwTArray<IwFace*> faces;
	bool gotOsteoOffset = pInnerSurfaceJigs->GetOffsetOsteophyteBrep( osteoOffsetBrep );
	if ( gotOsteoOffset )
	{
		osteoOffsetBrep->GetFaces(faces);
		totalOffsetSurfaces.Add(faces.GetAt(0)->GetSurface());
		totalOffsetSurfaces.Add(faces.GetAt(1)->GetSurface());
		totalOffsetSurfaces.Add(faces.GetAt(2)->GetSurface());
	}
	else // The only reason for this "else" to to prevent software crash
	{
		IwBSplineSurface* osteoMainSurf = pOsteoSurf->GetSinglePatchSurface();
		IwBSplineSurface* osteoLeftSurf, * osteoRightSurf;
		pOsteoSurf->GetSideSurfaces(osteoLeftSurf, osteoRightSurf);
		//
		totalOffsetSurfaces.Add(osteoMainSurf);
		totalOffsetSurfaces.Add(osteoLeftSurf);
		totalOffsetSurfaces.Add(osteoRightSurf);
	}

	// Get cartilage 3 surfaces
	bool bSmooth = !m_pDoc->GetAdvancedControlJigs()->GetCartilageUseWaterTightSurface();
	IwBSplineSurface* cartiMainSurf = pCartiSurf->GetSinglePatchSurface(bSmooth);
	IwBSplineSurface *cartiLeftSurf, *cartiRightSurf;
	pCartiSurf->GetSideSurfaces(cartiLeftSurf, cartiRightSurf);
	totalOffsetSurfaces.Add(cartiMainSurf);
	totalOffsetSurfaces.Add(cartiLeftSurf);
	totalOffsetSurfaces.Add(cartiRightSurf);

	//////////////////////////////////////////////////////
	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Create anterior surface
	IwBrep* antSurfBrep=NULL;
	double dropOffset = 0.0;
	CMakeInnerSurfaceJigs::DetermineContactSurface(m_pDoc, totalOffsetSurfaces, dropOffset, true, true, NULL, NULL, NULL, antSurfBrep); // 6 surfaces as input

	// Set it to pInnerSurfaceJigs
	IwBrep* oldBrep = pAnteriorSurfaceJigs->GetIwBrep();
	if ( oldBrep )
		IwObjDelete(oldBrep);
	pAnteriorSurfaceJigs->SetIwBrep(antSurfBrep);
	pAnteriorSurfaceJigs->MakeTess();
	pAnteriorSurfaceJigs->SetModifiedFlag(true);

	// Determine the lateral cut plane peak point after antSurfBrep is set to pAnteriorSurfaceJigs
	DetermineLateralCutPeakPointAndF3Profile();
	pAnteriorSurfaceJigs->SetLateralPeakPoint(m_currLateralPeakPoint);

	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_ANTERIOR_SURFACE_JIGS, false, false);

	QApplication::restoreOverrideCursor();

	m_pView->Redraw();

}

void CMakeAnteriorSurfaceJigs::OnAccept()
{
	m_pDoc->AppendLog( QString("CMakeAnteriorSurfaceJigs::OnAccept()") );

	// automactically save the file, if need
	if ( m_pDoc->IsFileSaveAuto() )
		m_pMainWindow->OnFileSave();

	CAnteriorSurfaceJigs* pAnteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();

	if ( pAnteriorSurfaceJigs && !pAnteriorSurfaceJigs->GetUpToDateStatus() )
		m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_ANTERIOR_SURFACE_JIGS, true, true);

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CMakeAnteriorSurfaceJigs::OnCancel()
{
	m_pDoc->AppendLog( QString("CMakeAnteriorSurfaceJigs::OnCancel()") );

	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	m_bDestroyMe = true;
	m_pView->Redraw();

}

void CMakeAnteriorSurfaceJigs::Display(Viewport* vp)
{		
	glDisable( GL_LIGHTING );
	glDrawBuffer( GL_BACK );
	glLineWidth( 1.0 );

	IwVector3d viewVec = m_pView->GetViewingVector();
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();

	// Display F3 profile
	if ( viewVec.IsParallelTo(sweepAxes.GetYAxis(), 1.0) && m_currF3ProfilePoints.GetSize() > 0 )
	{
		IwPoint3d pnt =m_currF3ProfilePoints.GetAt(0);
		IwVector3d layerOffset = m_pView->GetLayerOffsetVector(1, pnt);
		glColor3ub( darkGreen[0], darkGreen[1], darkGreen[2]);
		glBegin( GL_LINE_STRIP );
			for (unsigned i=0; i<m_currF3ProfilePoints.GetSize(); i++)
			{
				pnt = m_currF3ProfilePoints.GetAt(i);
				if ( pnt == IwPoint3d(-999,-999,-999) )
				{
					glEnd();
					glLineWidth( 2.0 );
					glBegin( GL_LINE_LOOP );
				}
				else
				{
					pnt = pnt + layerOffset;
					glVertex3d( pnt.x, pnt.y, pnt.z );					
				}
			}
		glEnd();
	}

	glLineWidth( 1.0 );
	glEnable( GL_LIGHTING );

}

bool CMakeAnteriorSurfaceJigs::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	m_posStart = cursor;

	ReDraw();

	return true;
}

bool CMakeAnteriorSurfaceJigs::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // 
	{
		// if the clicked-down is the same as the button-up point, change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}

	ReDraw();

	return true;
}

bool CMakeAnteriorSurfaceJigs::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{

	ReDraw();

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// This function determines the anterior peak point on lateral distal cut plane,
// and F3 profile.
void CMakeAnteriorSurfaceJigs::DetermineLateralCutPeakPointAndF3Profile()
{
	m_currLateralPeakPoint = IwPoint3d(); // make it uninitialized.
	m_currF3ProfilePoints.RemoveAll();

	CAnteriorSurfaceJigs *pAnteriorSurfaceJigs = m_pDoc->GetAnteriorSurfaceJigs();
	if ( pAnteriorSurfaceJigs == NULL )
		return;
	IwBrep* anteriorSurfaceBrep = pAnteriorSurfaceJigs->GetIwBrep();
	if ( anteriorSurfaceBrep == NULL )
		return;
	IwTArray<IwFace*> faces;
	IwFace* face;
	anteriorSurfaceBrep->GetFaces(faces);
	if ( faces.GetSize() != 1 )
		return;

	// Get femoral axes info
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	double refSize = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize();

	// Get cut info
	IwTArray<IwVector3d> cutPoints, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);
	IwPoint3d lateralDistalCutPoint, lateralDistalCutOrientation;
	IwPoint3d medialDistalCutPoint, medialDistalCutOrientation;
	IwPoint3d stepCutPoint, stepCutOrientation;

	// Get cut feature points
	IwTArray<IwVector3d> cutFeaturePoints, antOuterPoints;
	IFemurImplant::FemoralCuts_GetCutsFeaturePoints(cutFeaturePoints, antOuterPoints);
	IwPoint3d lateralAntAntChamCutPoint; // The vertex between ant cut and ant chamfer cut in lateral side

	// Get side info
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		lateralDistalCutPoint = cutPoints.GetAt(6);
		lateralDistalCutOrientation = cutOrientations.GetAt(6);
		medialDistalCutPoint = cutPoints.GetAt(7);
		medialDistalCutOrientation = cutOrientations.GetAt(7);
		lateralAntAntChamCutPoint = cutFeaturePoints.GetAt(18);
	}
	else
	{
		positiveSideIsLateral = false;
		lateralDistalCutPoint = cutPoints.GetAt(7);
		lateralDistalCutOrientation = cutOrientations.GetAt(7);
		medialDistalCutPoint = cutPoints.GetAt(6);
		medialDistalCutOrientation = cutOrientations.GetAt(6);
		lateralAntAntChamCutPoint = cutFeaturePoints.GetAt(19);
	}
	stepCutPoint = cutPoints.GetAt(10);
	stepCutOrientation = cutOrientations.GetAt(10);


	//// For Lateral Peak point /////////////////////////////////////////////
	IwPoint3d originOnDistalCutPlane = wslAxes.GetOrigin();
	// Project onto lateral distal cut plane
	originOnDistalCutPlane = originOnDistalCutPlane.ProjectPointToPlane(lateralDistalCutPoint, lateralDistalCutOrientation);
	//
	face = faces.GetAt(0);
	IwBSplineSurface* antSurf = (IwBSplineSurface*)face->GetSurface();
	IwTArray<IwCurve*> intCurves;
	IntersectSurfaceByPlane(antSurf, lateralDistalCutPoint, lateralDistalCutOrientation, intCurves);
	if ( intCurves.GetSize() == 0 )
		return;
	// convert to sampling points
	IwTArray<IwPoint3d> totalIntPoints, points;
	IwBSplineCurve* crv;
	IwExtent1d crvDom;
	double length;
	int nSize;
	for (unsigned i=0; i<intCurves.GetSize(); i++)
	{
		crv = (IwBSplineCurve*)intCurves.GetAt(i);
		crvDom = crv->GetNaturalInterval();
		crv->Length(crvDom, 0.01, length);
		nSize = (int)length + 1;
		crv->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), nSize, 0.01, &points, NULL);
		totalIntPoints.Append(points);
	}
	// Search for the most anterior point in lateral size
	IwPoint3d farAntPoint = lateralDistalCutPoint + 3*refSize*wslAxes.GetYAxis();
	IwVector3d pnt, minPnt, tempVec;
	double dotValue, dist, minDist = HUGE_DOUBLE;
	for (unsigned i=0; i<totalIntPoints.GetSize(); i++)
	{
		// Ensure the point is in the lateral side
		pnt = totalIntPoints.GetAt(i);
		tempVec = pnt - originOnDistalCutPlane;
		dotValue = tempVec.Dot(wslAxes.GetXAxis());
		if ( positiveSideIsLateral )
		{
			if ( dotValue < 0 )
				continue;
		}
		else
		{
			if ( dotValue > 0 )
				continue;
		}
		// search for the most anterior point
		dist = pnt.DistanceBetween(farAntPoint);
if (0)
ShowPoint(m_pDoc, pnt, blue);
		if ( dist < minDist )
		{
			minDist = dist;
			minPnt = pnt;
		}
	}
	// Do we get the point?
	if ( minDist < 10*refSize )
	{
		m_currLateralPeakPoint = minPnt;
	}

	//// For F3 profile points /////////////////////////////////////////////
	// Distal cut profiles
	IwVector3d inwardVec;
	IwPoint3d tempPnt, tempPnt2, projPnt;
	// for positive side distal cut profile
	IwPoint3d posAntChamDistalCutFeaturePoint = cutFeaturePoints.GetAt(14);
	IwPoint3d posAntChamDistalCutFeaturePointProjected = posAntChamDistalCutFeaturePoint.ProjectPointToPlane(stepCutPoint, stepCutOrientation);
	IwPoint3d posAntChamDistalCutFeaturePointOutside = posAntChamDistalCutFeaturePoint + 10*wslAxes.GetXAxis(); // move it outside
	// for negative side distal cut profile
	IwPoint3d negAntChamDistalCutFeaturePoint = cutFeaturePoints.GetAt(17);
	IwPoint3d negAntChamDistalCutFeaturePointProjected = negAntChamDistalCutFeaturePoint.ProjectPointToPlane(stepCutPoint, stepCutOrientation);
	IwPoint3d negAntChamDistalCutFeaturePointOutside = negAntChamDistalCutFeaturePoint - 10*wslAxes.GetXAxis(); // move it outside

	// F3 anterior side profile
	double F3APWidth = m_pDoc->GetVarTableValue("JIGS F3 SKETCH AP WIDTH");
	// For lateral side profile
	IwPoint3d sagittalPlanePoint = wslAxes.GetOrigin();
	IwPoint3d sagittalPlaneNormal = wslAxes.GetXAxis();
	// Determine the point of F3 anterior side profile on sagittal plane 
	// by project lateralAntAntChamCutPoint onto distal cut, then project onto sagittal plane, then move F3APWidth anteriorly.
	tempPnt = lateralAntAntChamCutPoint.ProjectPointToPlane(lateralDistalCutPoint, lateralDistalCutOrientation);
	double distToDistalCutPlane = lateralAntAntChamCutPoint.DistanceBetween(tempPnt);
	tempPnt2 = tempPnt.ProjectPointToPlane(sagittalPlanePoint, sagittalPlaneNormal);
	double distToSagittalPlane = tempPnt2.DistanceBetween(tempPnt);
	inwardVec = tempPnt2 - tempPnt;
	inwardVec.Unitize();
	IwPoint3d onSagittalPlaneAPWidthPoint = tempPnt2 + F3APWidth*wslAxes.GetZAxis();
	// Determine F3 anterior profile 1st corner point
	double sixtyDeg = 60.0/180.0*IW_PI;
	double distToF3AntProfile = F3APWidth - distToDistalCutPlane;
	IwPoint3d lateralF3AntProfileFirstCornerPoint = onSagittalPlaneAPWidthPoint - (distToSagittalPlane-distToF3AntProfile/tan(sixtyDeg))*inwardVec;
	// Determine F3 anterior profile 2nd corner point
	double F3SideWidth = 9.0; // fixed width
	IwPoint3d lateralF3AntProfileSecondCornerPoint = lateralF3AntProfileFirstCornerPoint - (F3APWidth-F3SideWidth)*wslAxes.GetZAxis() -(F3APWidth-F3SideWidth)/tan(sixtyDeg)*inwardVec;
	IwPoint3d lateralF3AntProfileSecondCornerPointOutside;
	if ( positiveSideIsLateral )
		lateralF3AntProfileSecondCornerPointOutside = lateralF3AntProfileSecondCornerPoint.ProjectPointToPlane(posAntChamDistalCutFeaturePointOutside, inwardVec);
	else
		lateralF3AntProfileSecondCornerPointOutside = lateralF3AntProfileSecondCornerPoint.ProjectPointToPlane(negAntChamDistalCutFeaturePointOutside, inwardVec);
	// However, the F3 anterior profile taper side (defined by 1st corner and 2nd corner) can be offset/inset 5mm (or 5mm/sin(60) along inwardVec)
	double inOutwardDist = 5.0/sin(sixtyDeg);
	// the inset/offset points
	IwPoint3d lateralF3AntProfileFirstCornerPointInset = lateralF3AntProfileFirstCornerPoint + inOutwardDist*inwardVec;
	IwPoint3d lateralF3AntProfileFirstCornerPointOffset = lateralF3AntProfileFirstCornerPoint - inOutwardDist*inwardVec;
	IwPoint3d lateralF3AntProfileSecondCornerPointInset = lateralF3AntProfileSecondCornerPoint + inOutwardDist*inwardVec;
	IwPoint3d lateralF3AntProfileSecondCornerPointOffset = lateralF3AntProfileSecondCornerPoint - inOutwardDist*inwardVec;
	// Let's determine the medial side
	projPnt = lateralF3AntProfileFirstCornerPointInset.ProjectPointToPlane(sagittalPlanePoint, sagittalPlaneNormal);
	IwPoint3d medialF3AntProfileFirstCornerPointInset = 2*projPnt - lateralF3AntProfileFirstCornerPointInset;
	projPnt = lateralF3AntProfileFirstCornerPointOffset.ProjectPointToPlane(sagittalPlanePoint, sagittalPlaneNormal);
	IwPoint3d medialF3AntProfileFirstCornerPointOffset = 2*projPnt - lateralF3AntProfileFirstCornerPointOffset;
	projPnt = lateralF3AntProfileSecondCornerPointInset.ProjectPointToPlane(sagittalPlanePoint, sagittalPlaneNormal);
	IwPoint3d medialF3AntProfileSecondCornerPointInset = 2*projPnt - lateralF3AntProfileSecondCornerPointInset;
	projPnt = lateralF3AntProfileSecondCornerPointOffset.ProjectPointToPlane(sagittalPlanePoint, sagittalPlaneNormal);
	IwPoint3d medialF3AntProfileSecondCornerPointOffset = 2*projPnt - lateralF3AntProfileSecondCornerPointOffset;
	//
	IwPoint3d medialF3AntProfileSecondCornerPointOutside;
	if ( positiveSideIsLateral )
		medialF3AntProfileSecondCornerPointOutside = medialF3AntProfileSecondCornerPointInset.ProjectPointToPlane(negAntChamDistalCutFeaturePointOutside, inwardVec);
	else
		medialF3AntProfileSecondCornerPointOutside = medialF3AntProfileSecondCornerPointInset.ProjectPointToPlane(posAntChamDistalCutFeaturePointOutside, inwardVec);

	// Add to m_currF3ProfilePoints
	// F3 anterior profile - offset
	m_currF3ProfilePoints.Add(lateralF3AntProfileSecondCornerPointOutside);
	m_currF3ProfilePoints.Add(lateralF3AntProfileSecondCornerPointOffset);
	m_currF3ProfilePoints.Add(lateralF3AntProfileFirstCornerPointOffset);
	m_currF3ProfilePoints.Add(medialF3AntProfileFirstCornerPointOffset);
	m_currF3ProfilePoints.Add(medialF3AntProfileSecondCornerPointOffset);
	m_currF3ProfilePoints.Add(medialF3AntProfileSecondCornerPointOutside);
	m_currF3ProfilePoints.Add(IwPoint3d(-999,-999,-999));// break point
	// F3 anterior profile - inset
	m_currF3ProfilePoints.Add(lateralF3AntProfileSecondCornerPointOutside);
	m_currF3ProfilePoints.Add(lateralF3AntProfileSecondCornerPointInset);
	m_currF3ProfilePoints.Add(lateralF3AntProfileFirstCornerPointInset);
	m_currF3ProfilePoints.Add(medialF3AntProfileFirstCornerPointInset);
	m_currF3ProfilePoints.Add(medialF3AntProfileSecondCornerPointInset);
	m_currF3ProfilePoints.Add(medialF3AntProfileSecondCornerPointOutside);
	// Dista cut profile
	if ( positiveSideIsLateral )
	{
		// start from medial
		m_currF3ProfilePoints.Add(negAntChamDistalCutFeaturePointOutside);
		m_currF3ProfilePoints.Add(negAntChamDistalCutFeaturePointProjected);
		m_currF3ProfilePoints.Add(posAntChamDistalCutFeaturePointProjected);
		m_currF3ProfilePoints.Add(posAntChamDistalCutFeaturePointOutside);
	}
	else
	{
		// start from medial
		m_currF3ProfilePoints.Add(posAntChamDistalCutFeaturePointOutside);
		m_currF3ProfilePoints.Add(posAntChamDistalCutFeaturePointProjected);
		m_currF3ProfilePoints.Add(negAntChamDistalCutFeaturePointProjected);
		m_currF3ProfilePoints.Add(negAntChamDistalCutFeaturePointOutside);
	}
}

void CMakeAnteriorSurfaceJigs::OnReviewValidateJigs()
{
	m_actReviewNext->setEnabled(true);
	GetView()->OnValidateJigs();
}

bool CMakeAnteriorSurfaceJigs::OnReviewNext()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_ANTERIOR_SURFACE_JIGS, true);
	return true;
}

bool CMakeAnteriorSurfaceJigs::OnReviewBack()
{
	// Clean up temporary points, if any;
	m_pDoc->CleanUpTempPoints();

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_ANTERIOR_SURFACE_JIGS, false);
	return true;
}

bool CMakeAnteriorSurfaceJigs::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;


	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_ANTERIOR_SURFACE_JIGS))
    	OnAccept();
	return true;
}

void CMakeAnteriorSurfaceJigs::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 1;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 ) 
	{
		// Display anterior surface
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_ANTERIOR_SURFACE_JIGS );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY_NET );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}

		vp->FrontView(false);
	}


	m_predefinedViewIndex++;
}
