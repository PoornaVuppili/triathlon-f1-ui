#include "SMLibInterface.h"
#include "KStatusIndicator.h"

// Context object for Solid++ 
IwContext* iwContext = NULL;

void ClearContext()
{
    if( iwContext )
    {
        delete iwContext;
        iwContext = NULL;
    }
}

void ResetContext()
{
    ClearContext();
    iwContext = new IwContext();
}

// Class with the interface of HwProgressTrackerInterface
IwStatus HwProgress::Heartbeat()
{
    return IW_SUCCESS;
}

IwStatus HwProgress::StartCountdown( unsigned int count )
{
    kStatus.ProgressInit(count);
    counter = 0;
    return IW_SUCCESS;
}

IwStatus HwProgress::Tick()
{
    kStatus.ProgressSet(counter++);
    return IW_SUCCESS;
}

IwStatus HwProgress::EndCountdown()
{
    kStatus.ProgressReset();
    return IW_SUCCESS;
}

