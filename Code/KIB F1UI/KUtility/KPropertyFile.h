///////////////////////////////////////////////////////////////////////////////////////////
//  Filename:	KPropertyFile.h
//	Purpose:	definition of the PropertyFile class
//	Author:		Travis Odegaard
//				Stryker
//	Date:		12/01/04
//	Version:	$Id: Propertyfile.h,v 1.2 2007/09/14 00:57:55 costast Exp $
///////////////////////////////////////////////////////////////////////////////////////////

#ifndef KPROPERTYFILE_H
#define KPROPERTYFILE_H

#include <QHash>
#include <QString>
#include <QFile>

class KPropertyFile 
{
public:
	//contructor/destructor
	KPropertyFile(QString);
	~KPropertyFile(){};

	//methods
	QString getValue(const QString&);
	void insertValue(const QString&, const QString&);
    void removeValue(const QString&);
	bool loadFile();
	void saveFile();
	int size();

private:
	QHash<QString,QString> dict;
	QString filePath;
};

//////////////////////////////////////////
// Convenience functions
//////////////////////////////////////////

// Update the ###_info.txt file
// Parameters:
//   path  (I) : full path to the ###_info.txt file
//   info  (I) : pairs of strings to be saved. Each pair is saved as one line: 'pair_first=pair_second'
// Return:
//   true: succcessful; false : failed
// Note:
//   The order of entries in the file might NOT be the same as that in 'info'
bool KUpdateImplantInfoTxt(QString path, const QList<QPair<QString, QString> >& info);

#endif KPROPERTYFILE_H

