#pragma once

#include <QtGui>
#include "..\KAppTotal\Entity.h"
#include "..\KAppTotal\DocReader.h"
#include "TotalDoc.h"

class CTotalDocReader : public CDocReader
	{
public:
	CTotalDocReader( CTotalDoc* pDoc, const QString& sDocFileName );
	~CTotalDocReader();
	CTotalDoc*      GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	bool			LoadEntities(CEntRole startEntRole=ENT_ROLE_NONE, CEntRole endEntRole=ENT_ROLE_JIGS_END);


private:

private:
	
	};