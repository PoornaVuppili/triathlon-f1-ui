#ifndef ENTITYITEM_H
#define ENTITYITEM_H

///////////////////////////////////////////////////////////////////////////////////
// The base class for all items displayed in a QTreeView-derived class
//  - each item has a QStandardItem as its root
//  - the root item can contain serveral rows of items under its root
///////////////////////////////////////////////////////////////////////////////////

#include <QObject>
#include <QStandardItem>
#include <QDomDocument>
#include <QList>
#include <vector>

#include <algorithm>
using namespace std;

class QTreeView;

#define ICONDIM 12  // icon dimension used by entity item

class TEST_EXPORT EntityItem : public QObject
{
    Q_OBJECT

public:
    EntityItem(QTreeView* view);
    virtual ~EntityItem();

    // Get the list of standard items
    QList<QStandardItem*> GetItems() { return items; }

    // Get the item id
    virtual QString GetID() const = 0;

    // Called when an item is changed
    virtual void OnModelItemChanged(QStandardItem* si) {}

    // Called when an item is clicked
    virtual void OnItemClicked(const QModelIndex& index) {}

    // Called when an item is double-clicked
    virtual void OnItemDoubleClicked(const QModelIndex& index) {}

    // Called when an item is right-clicked
    virtual void OnItemRightClicked(const QModelIndex& index) {}

    // Get the status by filling in 'elem'
    virtual void GetStatus(QDomElement& elem) = 0;

    // Set the status based on the content of 'elem'
    virtual void SetStatus(QDomElement elem) = 0;

    // Whether the item has been modified
    virtual bool IsModified() { return modified; }

    // Set the modified. 
    // - Current implementation adds/removes '*' at the front of item text.
    virtual void SetModified(bool b);

    virtual bool IsUserEditable() const { return ueditable; }
    virtual void SetUserEditable(bool uedtbl) { ueditable = uedtbl; }

    enum ValidatedStatus { eNeedsValidation = -1, eValidatedOK, eValidatedWithWarnings, eNotValid };
 
    virtual void SetValidateStatus(ValidatedStatus);
    virtual ValidatedStatus GetValidated() const { return validated; }

    enum UpToDateStatus { eUpToDateUnkown = -1, eUpToDate, eNeedsChecking, eNeedsEditing };

    void SetNeedsChecking() { UpToDateStatus was = uptodate; uptodate = max(uptodate, eNeedsChecking); SetModified(uptodate != was); }
    void SetNeedsEditing()  { UpToDateStatus was = uptodate; uptodate = max(uptodate, eNeedsEditing); SetModified(uptodate != was); }
    void SetUpToDateStatusNoLowerThan(UpToDateStatus status);

    UpToDateStatus GetUpToDateStatus() const { return uptodate; }
    bool IsUpToDate() const { return uptodate == eUpToDate; }
    // for Set/GetStatus (XML writing)
    static UpToDateStatus UpToDateStatusFromString(QString const & str);
    static QString UpToDateStatusString(UpToDateStatus);

    void UpdateUI();

    // Set the 'allowClose' flag of the item
    virtual void AllowClose(bool b) { allowClose = b; }

    // The mouse button when the item is pressed
    // (Since QTreeView::OnItemClicked(const QModelIndex&) is called after the mouse button
    //  is released, an item that handles 'OnItemClicked()' notification cannot use
    //  QApplication::mouseButtons() to decide whether the left-button or middle-button
    //  caused the event. This is used for compatibility with EntPanel class in TriathlonF1)
    static Qt::MouseButtons ClickButton;
	
	// find a row for sub-item a few levels below this one
	int GetDescendantRow(QStandardItem* si);
	int GetDescendantRow(const QModelIndex& idx);

    QString GetText() const;
    QModelIndex GetIndex() const { return items.empty() ? QModelIndex() : items[0]->index(); }

signals:
    void close();   // emit this signal when the item should be closed

protected:
    void SetUpToDate() { UpToDateStatus was = uptodate; uptodate = max(eUpToDate, was); SetModified(uptodate != was); }
    //  A helper function to create a standard item
    //  Parameters:
    //    txt        (I) : text of the item
    //    selectable (I) : whether the item is selectable, default to false
    //    editable   (I) : whether the item is editable, default to false
    //    checkable  (I) : whether the item is checkable, default to false
    //    state      (I) : the check state, default to unchecked
    QStandardItem* NewItem(const QString& txt, bool selectable=false, bool editable=false, bool checkable=false, Qt::CheckState state=Qt::Unchecked);

    // Common icons
    QIcon& GetIcon_ShowIntersect();
    QIcon& GetIcon_HideIntersect();

    QTreeView* treeView;                // the tree view where the item is displayed
    QList<QStandardItem*> items;        // the root standard item list
    bool allowClose;                    // .................can be closed
                                        // (default to false. If true, the pop-up menu by right-clicking the item constains
                                        //  a 'Close' menu item. A 'close' signal should be emitted if the menu is selected.)

private:
    bool            modified;           // whether the item is modified 
    bool            ueditable;          // whether the item is user-editable, which means text is bold, otherwise text is normal
    ValidatedStatus validated;
    UpToDateStatus  uptodate;

    static char modifiedCharacter();
    static char outOfDateCharacter();

    static QFont uNOTeditableFont; // regular
    static QFont ueditableFont;    // same as uNOTeditable, but bold
    static int fontsCreatedFlag;
};

#endif // ENTITYITEM_H
