#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;

class TEST_EXPORT_TW CStylusJigs : public CPart
{
public:
	CStylusJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_STYLUS_JIGS, const QString& sFileName="", int nEntId=-1 );
	~CStylusJigs();

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );

	bool			IsMemberDataChanged(IwPoint3d& stylusControlPosition, IwPoint3d& stylusDelta, double& gapBetweenStylusToOuterSurface, IwTArray<double>& filletRadii);
	void			SetStylusPosition(IwPoint3d& stylusControlPosition, IwPoint3d& stylusTipPosition, IwPoint3d& stylusDelta, double& gapBetweenStylusToOuterSurface);
	void			GetStylusPosition(IwPoint3d& stylusControlPosition, IwPoint3d& stylusTipPosition, IwPoint3d& stylusDelta, double& gapBetweenStylusToOuterSurface);
	void			SetFilletPointsRadii(IwTArray<IwPoint3d>& filletPoints, IwTArray<double>& filletRadii);
	void			GetFilletPointsRadii(IwTArray<IwPoint3d>& filletPoints, IwTArray<double>& filletRadii);
	void			SetVirtualStylusNFillets(IwTArray<IwCurve*>& virtualStylus, IwTArray<IwCurve*>& virtualFillets);
	void			GetVirtualStylusNFillets(IwTArray<IwCurve*>& virtualStylus, IwTArray<IwCurve*>& virtualFillets);
	void			SetStylusContactSurface(IwBSplineSurface* contactSurface);
	IwBSplineSurface*	GetStylusContactSurface(){return m_stylusContactSurface;};

private:
	void			SetStylusJigsModifiedFlag( bool bModified );
	void			DisplayVirtualFeatures();
	void			DisplayStylusContactRegion();

private:
	long			m_glStylusJigsVirtualFeaturesList;// take care of virtual features if any.
	long			m_glStylusJigsStylusContactRegionList;// display stylus contact surface
	bool			m_bStylusJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool			m_bStylusJigsDataModified;// Geometric entities have not been saved yet to the file. 

	IwPoint3d			m_stylusControlPosition;
	IwPoint3d			m_stylusTipPosition;
	IwPoint3d			m_stylusDelta;
	IwTArray<double>	m_filletRadii;		// fillet radii at tab concave corners
	IwTArray<IwPoint3d>	m_filletPoints;		// tab concave corners on outer surface to display fillet radii
	IwTArray<IwCurve*>	m_virtualStylus;	// virtual stylus if exist
	IwTArray<IwCurve*>	m_virtualFillets;	// virtual fillets if exist
	IwBSplineSurface*	m_stylusContactSurface;
	double				m_gapBetweenStylusToOuterSurface;
};