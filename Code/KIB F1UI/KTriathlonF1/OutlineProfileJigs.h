#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include "..\KAppTotal\Part.h"
#include "..\KAppTotal\valgebra.h"

class CTotalDoc;
class CInnerSurfaceJigs;

class TEST_EXPORT_TW COutlineProfileJigs : public CPart
{
public:
	COutlineProfileJigs( CTotalDoc* pDoc, CEntRole eEntRole=ENT_ROLE_OUTLINE_PROFILE_JIGS, const QString& sFileName="", int nEntId=-1 );
	~COutlineProfileJigs();
	CTotalDoc*		GetTotalDoc() {return ((CTotalDoc*)m_pDoc);};

	virtual void	Display();
	virtual bool	Save( const QString& sDir, bool incrementalSave=true );
	virtual bool	Load( const QString& sDir );
	virtual void	GetSelectableEntities(IwTArray<IwBrep*>&, IwTArray<IwCurve*>&, IwTArray<IwPoint3d>&);

	bool			GetSketchSurface(IwBSplineSurface*& sketchSurface);
	void			SetSketchSurface(IwBSplineSurface* sketchSurface);
	void			SetOutlineProfileFeaturePoints(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo);
	void			GetOutlineProfileFeaturePoints(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo);
	void			UpdateOutlineProfileArcEndingPoints(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo);
	void			SetOutlineProfileTabPoints(IwTArray<IwPoint3d>& tabPoints);
	void			GetOutlineProfileTabPoints(IwTArray<IwPoint3d>& tabPoints);
	void			SetOutlineProfileTabEndPoints(IwTArray<IwPoint3d>& tabEndPoints);
	void			GetOutlineProfileTabEndPoints(IwTArray<IwPoint3d>& tabEndPoints);
	bool			GetJumpCurves(IwTArray<IwBSplineCurve*>& jumpCurves);
	void			SetOutlineProfileReferencePoints(IwTArray<IwPoint3d>& refPoints);
	void			GetOutlineProfileReferencePoints(IwTArray<IwPoint3d>& refPoints);
	void			ConvertUVToXYZ(	IwTArray<IwPoint3d>& uvPoints, IwTArray<IwPoint3d>& xyzPoints);
	void			ConvertUVToXYZ(	IwPoint3d& uvPoint, IwPoint3d& xyzPoint, IwVector3d& normal);
	void			DetermineOutlineProfileJigsProjectedCurvePoints(CInnerSurfaceJigs* innerSurfaceJigs=NULL);
	void			DetermineOutlineProfileJigsTabProfilePoints(CInnerSurfaceJigs* innerSurfaceJigs=NULL);
	void			DetermineCoreHolesProfiles();
	void			GetOutlineProfileJigsProjectedCurvePoints(IwTArray<IwPoint3d>& projectedPoints) {projectedPoints.RemoveAll(); projectedPoints.Append(m_outlineProfileProjectedCurve);};
	void			GetDroppingCenters(IwPoint3d& posDroppingCenter, IwPoint3d& negDroppingCenter) {posDroppingCenter=m_posDroppingCenter;negDroppingCenter=m_negDroppingCenter;};
	void			SetDroppingCenters(IwPoint3d& posDroppingCenter, IwPoint3d& negDroppingCenter) {m_posDroppingCenter=posDroppingCenter;m_negDroppingCenter=negDroppingCenter;};
	void			GetBoundingProfiles(double addOffset, IwBSplineCurve* &posUVProfile, IwBSplineCurve* &negUVProfile, IwBSplineCurve* &notchUVProfile);
	void			GetAnteriorBoundingProfiles(double extendDistance, IwBSplineCurve* &posAntUVProfile, IwBSplineCurve* &negAntUVProfile,  IwBSplineCurve* &notchUVProfile);
	bool			IsMemberDataChanged(IwTArray<IwPoint3d>& featurePoints, IwTArray<int>& featurePointsInfo, IwTArray<IwPoint3d>& tabPoints, IwTArray<IwPoint3d>& tabEndPoints, 
										IwPoint3d& posDroppingCenter, IwPoint3d& negDroppingCenter, IwTArray<double> &windowCutAngles, bool keyDataOnly=false);
	void			GetWindowCutAngles(IwTArray<double>& windowCutAngles, IwTArray<IwPoint3d>* windowCutTips=NULL, IwPoint3d* posCenterPoint=NULL, IwPoint3d* negCenterPoint=NULL);
	void			SetWindowCutAngles(IwTArray<double> windowCutAngles);
	IwPoint3d		GetJigsNotchPoint();
	void			SetDisplaySketchSurface(bool val) {m_outlineProfileDisplaySketchSurface=val;};
	IwBSplineCurve* GetUVCurveOnSketchSurface(bool bSquareOffPosteriorTips=false, CInnerSurfaceJigs* innerSurfaceJigs=NULL, IwTArray<IwPoint3d>* squaredOffCornerArcs=NULL);	
	IwPoint3d		GetLateralPinHoleCenter() {return m_lateralPinHoleCenter;};
	// Temporary function
	void			EstimatePegPositions(IwPoint3d& posPegPosition, IwPoint3d& negPegPosition);

	void            SetRefLinePoints(IwPoint3d line1Pnt1, IwPoint3d line1Pnt2, IwPoint3d line2Pnt1, IwPoint3d line2Pnt2);
	void            GetRefLinePoints(IwPoint3d& line1Pnt1, IwPoint3d& line1Pnt2, IwPoint3d& line2Pnt1, IwPoint3d& line2Pnt2);

private:
	void			SetOPJigsModifiedFlag( bool bModified );
	void			DisplayOutlineProfile();
	void			DisplayCoreHoleProfiles();
	void			DisplayOutlineFeaturePoints();
	double			GetVLengthRatio();
	void			SquareOffPosteriorTips(CInnerSurfaceJigs* innerSurfaceJigs, IwTArray<IwPoint3d>& outlineProfileFeaturePoints, IwTArray<int>& outlineProfileFeaturePointsInfo, IwTArray<int>* squaredOffCornerCenterIndexes=NULL);

private:
	bool					m_outlineProfileJigsModified;// Geometric entities have not been updated yet to the screen. 
	bool					m_bOPJigsDataModified;// Geometric entities have not been saved yet to the file. 

	bool					m_outlineProfileDisplaySketchSurface;
	long					m_glOutlineProfileCurveList;

	IwBSplineSurface*		m_sketchSurface;
	IwTArray<IwPoint3d>		m_outlineProfileProjectedCurve;		// for display purpose
	IwTArray<IwPoint3d>		m_coreHoleProfile;					// for display purpose

	IwTArray<IwPoint3d>		m_outlineProfileFeaturePoints;		// Although they are IwPoint3d points, only (x,y) are used to represent (U,V) on sketch surface
	IwTArray<int>			m_outlineProfileFeaturePointsInfo;	// 0: interpolation point
																// 1: arc center point to define an CCW arc
																// 2: arc center point to define an CW arc
																// 3: arc start point
																// 4: arc end point
																// 11: collapsible arc center point to define an CCW arc
																// 12: collapsible arc center point to define an CW arc
																// 21: squared-off corner arc center point to define an CCW arc
																// 22: squared-off corner arc center point to define an CW arc
																// the sequence in outline is 3-(1/2)-4 to describe an arc.
	IwPoint3d				m_posDroppingCenter;				// The approximation center of the positive-side projected outline profile.
																// the point location is actually in the negative side of coordinate origin.
	IwPoint3d				m_negDroppingCenter;				// The approximation center of the negative-side projected outline profile.
																// the point location is actually in the positive side of coordinate origin.
	IwTArray<IwPoint3d>		m_outlineProfileTabPoints;			// The sequence is: [0~1]:medial anterior tab, [2~3]:lateral middle tab, [4~5]:medial peg tab
	IwTArray<IwPoint3d>		m_outlineProfileTabEndPoints;		// The sequence is: [0~1]:medial anterior tab, [2~3]:lateral middle tab, [4~5]:medial peg tab
	IwTArray<double>		m_windowCutAngles;					// [0]: positive F1 window cut angle, [1]: negative F1, [2]: positive F2, [3]: negative F2
	IwTArray<IwPoint3d>		m_windowCutTips;					// [0]: positive F1 window cut angle, [1]: negative F1, [2]: positive F2, [3]: negative F2
	IwPoint3d				m_posCoreCenterPoint;				// Core hole center = peg position
	IwPoint3d				m_negCoreCenterPoint;				// Core hole center = peg position
	IwTArray<IwPoint3d>		m_tabProfiles;						// 3 tab profile, for display purpose
	IwTArray<IwPoint3d>		m_referencePoints;					// [0~1]: posterior positive/negative sides reference points, 
																// [2~3]: anterior positive/negative sides reference points
	IwBSplineCurve*			m_medialJumpCurve;					// It actually is the extended tab end profile
	IwBSplineCurve*			m_lateralJumpCurve;					// It actually is the extended tab end profile
	IwPoint3d				m_lateralPinHoleCenter;				// For validation purpose, no need to save.
	bool					m_displayedMsgForEstimatedPegPosition;		// Temporary variable.

	IwPoint3d				m_posPegPosition;				
	IwPoint3d				m_negPegPosition;

	IwPoint3d					m_posReflinePnt1;
	IwPoint3d					m_posReflinePnt2;
	IwPoint3d					m_negReflinePnt1;
	IwPoint3d					m_negReflinePnt2;
};