/****************************************************************************
** Meta object code from reading C++ file 'OpenGLView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../OpenGLView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OpenGLView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_COpenGLView_t {
    QByteArrayData data[18];
    char stringdata0[370];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_COpenGLView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_COpenGLView_t qt_meta_stringdata_COpenGLView = {
    {
QT_MOC_LITERAL(0, 0, 11), // "COpenGLView"
QT_MOC_LITERAL(1, 12, 18), // "OnTessellationHigh"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 18), // "OnTessellationFine"
QT_MOC_LITERAL(4, 51, 20), // "OnTessellationNormal"
QT_MOC_LITERAL(5, 72, 20), // "OnTessellationCoarse"
QT_MOC_LITERAL(6, 93, 30), // "OnTransparencyTransparentTotal"
QT_MOC_LITERAL(7, 124, 29), // "OnTransparencyTransparentPlus"
QT_MOC_LITERAL(8, 154, 25), // "OnTransparencyTransparent"
QT_MOC_LITERAL(9, 180, 20), // "OnTransparencyNormal"
QT_MOC_LITERAL(10, 201, 20), // "OnTransparencyOpaque"
QT_MOC_LITERAL(11, 222, 24), // "OnTransparencyOpaquePlus"
QT_MOC_LITERAL(12, 247, 20), // "OnArrowKeyRotationPP"
QT_MOC_LITERAL(13, 268, 19), // "OnArrowKeyRotationP"
QT_MOC_LITERAL(14, 288, 18), // "OnArrowKeyRotation"
QT_MOC_LITERAL(15, 307, 19), // "OnArrowKeyRotationM"
QT_MOC_LITERAL(16, 327, 20), // "OnArrowKeyRotationMM"
QT_MOC_LITERAL(17, 348, 21) // "OnArrowKeyRotationMMM"

    },
    "COpenGLView\0OnTessellationHigh\0\0"
    "OnTessellationFine\0OnTessellationNormal\0"
    "OnTessellationCoarse\0"
    "OnTransparencyTransparentTotal\0"
    "OnTransparencyTransparentPlus\0"
    "OnTransparencyTransparent\0"
    "OnTransparencyNormal\0OnTransparencyOpaque\0"
    "OnTransparencyOpaquePlus\0OnArrowKeyRotationPP\0"
    "OnArrowKeyRotationP\0OnArrowKeyRotation\0"
    "OnArrowKeyRotationM\0OnArrowKeyRotationMM\0"
    "OnArrowKeyRotationMMM"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_COpenGLView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x0a /* Public */,
       3,    0,   95,    2, 0x0a /* Public */,
       4,    0,   96,    2, 0x0a /* Public */,
       5,    0,   97,    2, 0x0a /* Public */,
       6,    0,   98,    2, 0x0a /* Public */,
       7,    0,   99,    2, 0x0a /* Public */,
       8,    0,  100,    2, 0x0a /* Public */,
       9,    0,  101,    2, 0x0a /* Public */,
      10,    0,  102,    2, 0x0a /* Public */,
      11,    0,  103,    2, 0x0a /* Public */,
      12,    0,  104,    2, 0x0a /* Public */,
      13,    0,  105,    2, 0x0a /* Public */,
      14,    0,  106,    2, 0x0a /* Public */,
      15,    0,  107,    2, 0x0a /* Public */,
      16,    0,  108,    2, 0x0a /* Public */,
      17,    0,  109,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void COpenGLView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<COpenGLView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnTessellationHigh(); break;
        case 1: _t->OnTessellationFine(); break;
        case 2: _t->OnTessellationNormal(); break;
        case 3: _t->OnTessellationCoarse(); break;
        case 4: _t->OnTransparencyTransparentTotal(); break;
        case 5: _t->OnTransparencyTransparentPlus(); break;
        case 6: _t->OnTransparencyTransparent(); break;
        case 7: _t->OnTransparencyNormal(); break;
        case 8: _t->OnTransparencyOpaque(); break;
        case 9: _t->OnTransparencyOpaquePlus(); break;
        case 10: _t->OnArrowKeyRotationPP(); break;
        case 11: _t->OnArrowKeyRotationP(); break;
        case 12: _t->OnArrowKeyRotation(); break;
        case 13: _t->OnArrowKeyRotationM(); break;
        case 14: _t->OnArrowKeyRotationMM(); break;
        case 15: _t->OnArrowKeyRotationMMM(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject COpenGLView::staticMetaObject = { {
    QMetaObject::SuperData::link<EntityView::staticMetaObject>(),
    qt_meta_stringdata_COpenGLView.data,
    qt_meta_data_COpenGLView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *COpenGLView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *COpenGLView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_COpenGLView.stringdata0))
        return static_cast<void*>(this);
    return EntityView::qt_metacast(_clname);
}

int COpenGLView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = EntityView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
