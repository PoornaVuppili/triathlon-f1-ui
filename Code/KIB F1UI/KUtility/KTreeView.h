#pragma once
#include "QTreeView"

class KTreeView : public QTreeView
{
	Q_OBJECT

public:
	KTreeView(QWidget *parent = 0);
	~KTreeView(void);

protected:
	void selectionChanged(const QItemSelection & selected,const QItemSelection & deselected);

signals:
	void selectionChanged(const QModelIndex& index);
};

