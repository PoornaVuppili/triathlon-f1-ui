#pragma once

#include "..\KAppTotal\TriathlonF1Definitions.h"
#include <QtGui>
#include "..\KAppTotal\Manager.h"

class CTotalView;
class CTotalDoc;
class CTotalMainWindow;
class CAdvancedControlDlg;
class CAdvancedControl;

class TEST_EXPORT_TW CDisplayAdvancedControl : public CManager
{
    Q_OBJECT

public:
					CDisplayAdvancedControl( CTotalView* pView );
	virtual			~CDisplayAdvancedControl();
	CTotalView*		GetView() {return ((CTotalView*)m_pView);};

private:

private slots:
	void			OnDispDlg(CAdvancedControl* advCtrl);
	void			OnAccept();

public:
	//static CDisplayAdvancedControl *pCurDispAdvCtrl;


private:
	CTotalMainWindow*			m_pMainWindow;
	CTotalDoc*					m_pDoc;
	QAction*					m_actAdvancedControl;
	QAction*					m_actDispDlg;
	CAdvancedControlDlg*		pDlg;
};
