#include "Measure.h"
#include "..\KUtility\SMLibInterface.h"
#include "..\KUtility\KStatusIndicator.h"
#include <IwFace.h>

MeasureResult MeasureResult::ComputeEntityAngle(MeasureEntity& e0, MeasureEntity& e1)
{
    if( IsLinear(e0) )
    {
        LineSegment3Double ls0 = GetLineSegment(e0);

        if( IsLinear(e1) )
		{
			LineSegment3Double ls1 = GetLineSegment(e1);

			if( ls0.GetP1()==ls1.GetP2() )
				return Angle(ls0, LineSegment3Double(ls1.GetP2(), ls1.GetP1()));
			else if( ls0.GetP2()==ls1.GetP1() )
				return Angle(LineSegment3Double(ls0.GetP2(), ls0.GetP1()), ls1);
			else if( ls0.GetP2()==ls1.GetP2() )
				return Angle(LineSegment3Double(ls0.GetP2(), ls0.GetP1()), LineSegment3Double(ls1.GetP2(), ls1.GetP1()));
			else
				return Angle(ls0, ls1);
		}
        else if( IsPlanar(e1) )
            return Angle(ls0, GetPlane(e1));
    }
    else if( IsPlanar(e0) )
    {
        Plane3Double pn0 = GetPlane(e0);

        if( IsLinear(e1) )
            return Angle(GetLineSegment(e1), pn0);
        else if( IsPlanar(e1) )
            return Angle(pn0, e0.GetHitPoint().cast<double>(), GetPlane(e1), e1.GetHitPoint().cast<double>());
    }
    
    kStatus.ShowMessage("Error: invalid entities for angle measurement");
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityAngle(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2)
{
    if( e0.selectType==SelectPoint && e1.selectType==SelectPoint )
    {
        LineSegment3Double ls(e0.selectValue.point.x, e0.selectValue.point.y, e0.selectValue.point.z,
                              e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z);

        if( IsLinear(e2) )
            return Angle(ls, GetLineSegment(e2));
        else if( IsPlanar(e2) )
            return Angle(ls, GetPlane(e2));
    }
    else if( e1.selectType==SelectPoint && e2.selectType==SelectPoint )
    {
        LineSegment3Double ls(e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z,
                              e2.selectValue.point.x, e2.selectValue.point.y, e2.selectValue.point.z);

        if( IsLinear(e0) )
            return Angle(GetLineSegment(e0), ls);
        else if( IsPlanar(e0) )
            return Angle(ls, GetPlane(e0));
    }
    kStatus.ShowMessage("Error: invalid entities for angle measurement");
    return MeasureResult();
}

MeasureResult MeasureResult::ComputeEntityAngle(MeasureEntity& e0, MeasureEntity& e1, MeasureEntity& e2, MeasureEntity& e3)
{
    if( e0.selectType==SelectPoint && e1.selectType==SelectPoint && e2.selectType==SelectPoint && e3.selectType==SelectPoint )
	{
		if( e0==e3 )
			return Angle(LineSegment3Double(e0.selectValue.point.x, e0.selectValue.point.y, e0.selectValue.point.z,
											e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z),
						 LineSegment3Double(e3.selectValue.point.x, e3.selectValue.point.y, e3.selectValue.point.z,
											e2.selectValue.point.x, e2.selectValue.point.y, e2.selectValue.point.z));
		else if( e1==e2 )
			return Angle(LineSegment3Double(e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z,
											e0.selectValue.point.x, e0.selectValue.point.y, e0.selectValue.point.z),
						 LineSegment3Double(e2.selectValue.point.x, e2.selectValue.point.y, e2.selectValue.point.z,
											e3.selectValue.point.x, e3.selectValue.point.y, e3.selectValue.point.z));
		else if( e1==e3 )
			return Angle(LineSegment3Double(e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z,
											e0.selectValue.point.x, e0.selectValue.point.y, e0.selectValue.point.z),
						 LineSegment3Double(e3.selectValue.point.x, e3.selectValue.point.y, e3.selectValue.point.z,
											e2.selectValue.point.x, e2.selectValue.point.y, e2.selectValue.point.z));
		else
			return Angle(LineSegment3Double(e0.selectValue.point.x, e0.selectValue.point.y, e0.selectValue.point.z,
											e1.selectValue.point.x, e1.selectValue.point.y, e1.selectValue.point.z),
						 LineSegment3Double(e2.selectValue.point.x, e2.selectValue.point.y, e2.selectValue.point.z,
											e3.selectValue.point.x, e3.selectValue.point.y, e3.selectValue.point.z));
	}
    kStatus.ShowMessage("Error: invalid entities for angle measurement");
    return MeasureResult();
}

bool MeasureResult::IsLinear(MeasureEntity& e)
{
    switch( e.selectType )
    {
    case SelectLine:    return true;
    case SelectCurve:   return e.selectValue.curve.curve->IsLinear(0.01);
    case SelectEdge:    return e.selectValue.edge.edge->GetCurve()->IsLinear(0.01);
    }
    return false;
}

bool MeasureResult::IsPlanar(MeasureEntity& e)
{
    switch( e.selectType )
    {
    case SelectCircle:  return true;
    case SelectPlane:   return true;
    case SelectSurface: return e.selectValue.surface.surface->IsPlanar(0.01);
    case SelectFace:    return e.selectValue.face.face->GetSurface()->IsPlanar(0.01);
    }
    return false;
}

LineSegment3Double MeasureResult::GetLineSegment(MeasureEntity& e)
{
    switch( e.selectType )
    {
    case SelectLine:
            return LineSegment3Double(e.selectValue.line.xs, e.selectValue.line.ys, e.selectValue.line.zs,
                                      e.selectValue.line.xe, e.selectValue.line.ye, e.selectValue.line.ze);

    case SelectCurve:
        {
            IwCurve* cv = e.selectValue.curve.curve;
            if( cv->IsLinear(0.01) )
            {
                IwPoint3d ps,pe;
                cv->GetEnds(ps,pe);
                return LineSegment3Double(ps, pe);
            }
        }
        break;

    case SelectEdge:
        {
            IwEdge* eg = e.selectValue.edge.edge;
            IwCurve* cv = eg->GetCurve();
            if( cv->IsLinear(0.01) )
            {
                IwExtent1d ext = eg->GetInterval();
                IwPoint3d ps,pe;
                cv->EvaluatePoint(ext.GetMin(), ps);
                cv->EvaluatePoint(ext.GetMax(), pe);
                return LineSegment3Double(ps, pe);
            }
        }
        break;
    }
    kStatus.ShowMessage("Error: non-linear curve isn't supported");
    return LineSegment3Double(0,0,0,1,1,1);
}

Plane3Double MeasureResult::GetPlane(MeasureEntity& e)
{
    switch( e.selectType )
    {
    case SelectCircle:
        return Plane3Double(Point3Double(e.selectValue.circle.x, e.selectValue.circle.y, e.selectValue.circle.z),
                            Vector3Double(e.selectValue.circle.nx, e.selectValue.circle.ny, e.selectValue.circle.nz));

    case SelectPlane:
        {
            Point3Double p1(e.selectValue.plane.cx, e.selectValue.plane.cy, e.selectValue.plane.cz);
            Point3Double t1 = Point3Double(e.selectValue.plane.ux, e.selectValue.plane.uy, e.selectValue.plane.uz) - p1;
            Point3Double t2 = Point3Double(e.selectValue.plane.vx, e.selectValue.plane.vy, e.selectValue.plane.vz) - p1;
            return Plane3Double(p1, t1.UnitCross(t2));
        }
            
    case SelectSurface:
        {
            IwSurface* surf = e.selectValue.surface.surface;
            IwPoint3d pt,nor;
            if( surf->IsPlanar(0.01, &pt, &nor) )
                return Plane3Double(pt,nor);
        }
        break;

    case SelectFace:
        {
            IwSurface* surf = e.selectValue.face.face->GetSurface();
            IwPoint3d pt,nor;
            if( surf->IsPlanar(0.01, &pt, &nor) )
                return Plane3Double(pt,nor);
        }
        break;
    }
    kStatus.ShowMessage("Error: non-planar surface isn't supported");
    return Plane3Double(Point3Double::Zero(), Point3Double::UnitZ());
}

// get one vector (among many) that's normal to 'v'
Vector3Double MeasureResult::NormalTo(const Vector3Double& v)
{
    return v.norm() * ( fabs(v.x())>fabs(v.y()) ?
            v.UnitCross( fabs(v.x())>fabs(v.z()) ? Vector3Double(v.y(), v.x(), v.z()) : Vector3Double(v.z(), v.y(), v.x()) ) :
            v.UnitCross( fabs(v.y())>fabs(v.z()) ? Vector3Double(v.x(), v.z(), v.y()) : Vector3Double(v.z(), v.y(), v.x()) ));
}

MeasureResult MeasureResult::Angle(const LineSegment3Double& L1, const LineSegment3Double& L2)
{
    MeasureResult r;

	Point3Double pc = ( L1.GetP1().Distance(L2.GetP1()) < L1.GetP2().Distance(L2.GetP2()) ) ?
		(L1.GetP1() + L2.GetP1())*.5 : (L1.GetP2() + L2.GetP2())*.5;
	Point3Double p1 = L1.GetP2() - L1.GetP1();
	Point3Double p2 = L2.GetP2() - L2.GetP1();

    r.mode = EntityAngle;
    r.value.angle.degree = acos(std::min(1.0, std::max(p1.normalized().dot(p2.normalized()), -1.0)))*180/PI;
    r.value.angle.xc = pc.x();
    r.value.angle.yc = pc.y();
    r.value.angle.zc = pc.z();
    p1 += pc;
    r.value.angle.x1 = p1.x();
    r.value.angle.y1 = p1.y();
    r.value.angle.z1 = p1.z();
    p2 += pc;
    r.value.angle.x2 = p2.x();
    r.value.angle.y2 = p2.y();
    r.value.angle.z2 = p2.z();
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::Angle(const LineSegment3Double& ls, const Plane3Double& pn)
{
    MeasureResult r;
    r.mode = EntityAngle;

    Point3Double p1 = ls.GetP1();
    Point3Double p2 = ls.GetP2();
    if( pn.IsPerpendicular(Line3Double(p1,p2)) )
    {
        r.value.angle.degree = 90;
        Point3Double p1j = pn.Project(p1);
        Point3Double pc = (p1 + p1j)*.5;
        r.value.angle.xc = pc.x();
        r.value.angle.yc = pc.y();
        r.value.angle.zc = pc.z();

        Vector3Double v = p2 - p1;
        Point3Double ps = pc + v;
        r.value.angle.x1 = ps.x();
        r.value.angle.y1 = ps.y();
        r.value.angle.z1 = ps.z();

        Point3Double pt = pc + NormalTo(v);
        r.value.angle.x2 = pt.x();
        r.value.angle.y2 = pt.y();
        r.value.angle.z2 = pt.z();
    }
    else
    {
        Point3Double p1j = pn.Project(p1);
        Point3Double p2j = pn.Project(p2);

		Point3Double pc;
		Vector3Double v,vj;

		if( p1j.Distance(p1) < p2j.Distance(p2) )
		{
			pc = (p1 + p1j)*.5;
			v = p2 - p1;
			vj = (p2j - p1j).normalized();
		}
		else
		{
			pc = (p2 + p2j)*.5;
			v = p1 - p2;
			vj = (p1j - p2j).normalized();
		}

        r.value.angle.xc = pc.x();
        r.value.angle.yc = pc.y();
        r.value.angle.zc = pc.z();

        Point3Double ps = pc + v;
        r.value.angle.x1 = ps.x();
        r.value.angle.y1 = ps.y();
        r.value.angle.z1 = ps.z();

        Point3Double pt = pc + vj*v.norm();
        r.value.angle.x2 = pt.x();
        r.value.angle.y2 = pt.y();
        r.value.angle.z2 = pt.z();

        r.value.angle.degree = acos(std::min(1.0 ,std::max(v.normalized().dot(vj), -1.0)))*180/PI;
    }
    r.valid = true;
    return r;
}

MeasureResult MeasureResult::Angle(const Plane3Double& pn1, const Point3Double& hit1, const Plane3Double& pn2, const Point3Double& hit2)
{
    MeasureResult r;
    r.mode = EntityAngle;
    Point3Double pc,p1,p2;
    if( pn1.IsParallel(pn2) )
    {
        pc = (hit1 + hit2)*.5;
        p1 = pc + NormalTo(pn1.GetPlaneNormal()) * 10;
        p2 = p1;
        r.value.angle.degree = 0;
    }
    else if( pn1.IsPerpendicular(pn2, 0.001) )
    {
        Point3Double pj1 = pn2.Project(hit1);
        Point3Double pj2 = pn1.Project(hit2);
        pc = (pj1 + pj2)*.5;
        double len = std::max(pj1.Distance(hit1), pj2.Distance(hit2));
        p1 = pc + pn1.GetPlaneNormal()*len;
        p2 = pc + pn2.GetPlaneNormal()*len;
        r.value.angle.degree = 90;
    }
    else
    {
        const Vector3Double& n1 = pn1.GetPlaneNormal();
        const Vector3Double& n2 = pn2.GetPlaneNormal();
        pc = (hit1 + hit2)*.5;
        double len = hit1.Distance(hit2);
        p1 = pc + n1*len;
        p2 = pc + n2*len;
        r.value.angle.degree = acos(std::min(1.0, std::max(n1.dot(n2), -1.0)))*180/PI;
    }
    r.value.angle.xc = pc.x();  r.value.angle.yc = pc.y();  r.value.angle.zc = pc.z();
    r.value.angle.x1 = p1.x();  r.value.angle.y1 = p1.y();  r.value.angle.z1 = p1.z();
    r.value.angle.x2 = p2.x();  r.value.angle.y2 = p2.y();  r.value.angle.z2 = p2.z();
    r.valid = true;
    return r;
}
