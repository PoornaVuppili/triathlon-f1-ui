#pragma warning( disable : 4996 4805 )
#include <IwBrep.h>
#pragma warning( default : 4996 4805 )
#include "FemoralAxes.h"
#include "AdvancedControl.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "..\KApp\SwitchBoard.h"
#include "..\KUtility\CoordinateSystem.h"

#include "..\KApp\MainWindow.h"
#include "..\KApp\Implant.h"
#include "IFemurImplant.h"

CFemoralAxes::CFemoralAxes( CTotalDoc* pDoc, CEntRole eEntRole, const QString& sFileName, int nEntId ) : 
						CEntity( pDoc, eEntRole, sFileName, nEntId )
{
	SetEntType( ENT_TYPE_FEMORAL_AXES );

	m_eDisplayMode = DISP_MODE_TRANSPARENCY;

	m_color = darkGreen;
	m_size = 0.0;
	m_antCutFlexionAngle = 85.0;
	m_antCutObliqueAngle = 0.0;
	m_femoralAxesFlexionAngle = 5.0;
	m_femoralAxesYRotAngle = 0.0;
	m_femoralAxesZRotAngle = 0.0;

	m_displayAntCutPlane = false;
	m_bDisplayCTScanOnTroclearPlane = false;

	m_bModified = false;
	m_bDataModified = false;

	//m_2ndItem = new QStandardItem("CT");
	//m_2ndItem->setCheckable(true);
	//m_2ndItem->setCheckState(Qt::Unchecked);
	//m_2ndItem->setSelectable(false);

	SwitchBoard::addSignalSender(SIGNAL(BroadcastCoordSystem(const CoordSystem&, bool)), this);
}

CFemoralAxes::~CFemoralAxes()
{
	SwitchBoard::removeSignalSender(this);
}

void CFemoralAxes::EnableCTScanDisplayOnTroclearPlane(bool enable)
{
	m_bDisplayCTScanOnTroclearPlane = enable;
}

void CFemoralAxes::Display()
{
	if ( m_size <= 0.0 )
		return;/// ">" indicate the feature points have been set.

	IwVector3d viewingVector = m_pDoc->GetView()->GetViewingVector();

	glDisable( GL_LIGHTING );

	// Display implant (flexed) axes
	glLineWidth( 3.0 );
	glBegin( GL_LINES );
		glColor3d( 1.0, 0.0, 0.0 );
		glVertex3d( m_axesPoints.GetAt(0).x, m_axesPoints.GetAt(0).y, m_axesPoints.GetAt(0).z );
		glVertex3d( m_axesPoints.GetAt(1).x, m_axesPoints.GetAt(1).y, m_axesPoints.GetAt(1).z );

		glColor3d( 0.0, 1.0, 0.0 );
		glVertex3d( m_axesPoints.GetAt(0).x, m_axesPoints.GetAt(0).y, m_axesPoints.GetAt(0).z );
		glVertex3d( m_axesPoints.GetAt(2).x, m_axesPoints.GetAt(2).y, m_axesPoints.GetAt(2).z );

		glColor3d( 0.0, 0.0, 1.0 );
		glVertex3d( m_axesPoints.GetAt(0).x, m_axesPoints.GetAt(0).y, m_axesPoints.GetAt(0).z );
		glVertex3d( m_axesPoints.GetAt(3).x, m_axesPoints.GetAt(3).y, m_axesPoints.GetAt(3).z );
	glEnd();

	// Display mechanical axes
	if ( ((CTotalDoc*)m_pDoc)->GetAdvancedControl()->GetFemoralAxesDisplayMechAxes() )
	{
		glLineWidth( 4.0 );
		glBegin( GL_LINES );
			glColor3d( 0.5, 0.0, 0.0 );
			glVertex3d( m_mechAxesPoints.GetAt(0).x, m_mechAxesPoints.GetAt(0).y, m_mechAxesPoints.GetAt(0).z );
			glVertex3d( m_mechAxesPoints.GetAt(1).x, m_mechAxesPoints.GetAt(1).y, m_mechAxesPoints.GetAt(1).z );

			glColor3d( 0.0, 0.5, 0.0 );
			glVertex3d( m_mechAxesPoints.GetAt(0).x, m_mechAxesPoints.GetAt(0).y, m_mechAxesPoints.GetAt(0).z );
			glVertex3d( m_mechAxesPoints.GetAt(2).x, m_mechAxesPoints.GetAt(2).y, m_mechAxesPoints.GetAt(2).z );

			glColor3d( 0.0, 0.0, 0.5 );
			glVertex3d( m_mechAxesPoints.GetAt(0).x, m_mechAxesPoints.GetAt(0).y, m_mechAxesPoints.GetAt(0).z );
			glVertex3d( m_mechAxesPoints.GetAt(3).x, m_mechAxesPoints.GetAt(3).y, m_mechAxesPoints.GetAt(3).z );
		glEnd();
	}
	glLineWidth( 1.0 );

	glPointSize(7);
	glBegin( GL_POINTS );
		glColor3d( 1.0, 0.0, 0.0 );
		glVertex3d( m_hipPoint.x, m_hipPoint.y, m_hipPoint.z );
		glVertex3d( m_proximalPoint.x, m_proximalPoint.y, m_proximalPoint.z );
		glVertex3d( m_leftEpiCondylarPoint.x, m_leftEpiCondylarPoint.y, m_leftEpiCondylarPoint.z );
		glVertex3d( m_rightEpiCondylarPoint.x, m_rightEpiCondylarPoint.y, m_rightEpiCondylarPoint.z );

		if ( m_antCutPlaneRotationCenter.IsInitialized() )
		{
			IwPoint3d displayPnt = m_antCutPlaneRotationCenter - 10*viewingVector;
			glVertex3d( displayPnt.x, displayPnt.y, displayPnt.z );
		}

		if ( m_antCutPlaneCenter.IsInitialized() )
		{
			glColor3d( 0.0, 0.0, 1.0 );

			IwPoint3d displayPnt = m_antCutPlaneCenter - 10*viewingVector;
			glVertex3d( displayPnt.x, displayPnt.y, displayPnt.z );
		}
	glEnd();

	glPointSize(2);
	glBegin( GL_POINTS );
		glColor3d( 1.0, 1.0, 1.0 );
		glVertex3d( m_sweepAxes.GetOrigin().x, m_sweepAxes.GetOrigin().y, m_sweepAxes.GetOrigin().z );
	glEnd();

	glPointSize(1);

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_POLYGON_OFFSET_FILL );
	glPolygonOffset( 3.0, 100.0 );

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY || m_eDisplayMode == DISP_MODE_TRANSPARENT_CT)	
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
		glColor4ub( m_color[0], m_color[1], m_color[2], m_pDoc->GetTransparencyFactor());
	}

	//// DISPLAY ANTERIOR CUT PLANE //////////////////////////////////////////
	if ( m_displayAntCutPlane )
	{
		if( m_eDisplayMode == DISP_MODE_DISPLAY ||
			m_eDisplayMode == DISP_MODE_TRANSPARENCY ||
			m_eDisplayMode == DISP_MODE_TRANSPARENT_CT ||
			m_eDisplayMode == DISP_MODE_DISPLAY_CT)
		{
			if ( m_cutPlaneCorners.GetSize() > 0 )
			{
			/*	if(m_eDisplayMode == DISP_MODE_DISPLAY_CT || m_eDisplayMode == DISP_MODE_TRANSPARENT_CT)
					DisplayCTScanOnAnteriorPlane();
				else
					DisplayAnteriorPlane();*/

				glLineWidth( 1.0 );
				glBegin( GL_LINE_LOOP );
					glColor3d( 0.0, 0.0, 0.0 );
					glVertex3d( m_cutPlaneCorners.GetAt(0).x, m_cutPlaneCorners.GetAt(0).y, m_cutPlaneCorners.GetAt(0).z );
					glVertex3d( m_cutPlaneCorners.GetAt(1).x, m_cutPlaneCorners.GetAt(1).y, m_cutPlaneCorners.GetAt(1).z );
					glVertex3d( m_cutPlaneCorners.GetAt(2).x, m_cutPlaneCorners.GetAt(2).y, m_cutPlaneCorners.GetAt(2).z );
					glVertex3d( m_cutPlaneCorners.GetAt(3).x, m_cutPlaneCorners.GetAt(3).y, m_cutPlaneCorners.GetAt(3).z );
				glEnd();
			}
		}
	}

	if ( m_eDisplayMode == DISP_MODE_TRANSPARENCY || m_eDisplayMode == DISP_MODE_TRANSPARENT_CT )	
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	glDisable( GL_POLYGON_OFFSET_FILL );
	glEnable( GL_LIGHTING );

	m_bModified = true;
	
	glFlush();
}

void CFemoralAxes::DisplayAnteriorPlane()
{
	glBegin( GL_QUADS );
		glNormal3d( m_cutPlaneCorners.GetAt(4).x, m_cutPlaneCorners.GetAt(4).y, m_cutPlaneCorners.GetAt(4).z );

		glVertex3d( m_cutPlaneCorners.GetAt(0).x, m_cutPlaneCorners.GetAt(0).y, m_cutPlaneCorners.GetAt(0).z );
		glVertex3d( m_cutPlaneCorners.GetAt(1).x, m_cutPlaneCorners.GetAt(1).y, m_cutPlaneCorners.GetAt(1).z );
		glVertex3d( m_cutPlaneCorners.GetAt(2).x, m_cutPlaneCorners.GetAt(2).y, m_cutPlaneCorners.GetAt(2).z );
		glVertex3d( m_cutPlaneCorners.GetAt(3).x, m_cutPlaneCorners.GetAt(3).y, m_cutPlaneCorners.GetAt(3).z );
	glEnd();
}

bool CFemoralAxes::Save( const QString& sDir, bool incrementalSave )
{
	QString				sFileName;
	bool				bOK=true;

	// "Save As" is not incrementalSave
	if ( incrementalSave && !m_bDataModified ) 
		return bOK;

	sFileName = sDir + "\\" + m_sFileName + "_fa.dat~";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;


	File.write( (char*) &m_hipPoint, sizeof( IwPoint3d ) );
	File.write( (char*) &m_proximalPoint, sizeof( IwPoint3d ) );
	File.write( (char*) &m_leftEpiCondylarPoint, sizeof( IwPoint3d ) );
	File.write( (char*) &m_rightEpiCondylarPoint, sizeof( IwPoint3d ) );//

	File.write( (char*) &m_proximalRefPoint, sizeof( IwPoint3d ) );


	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v2.1.a
	/////////////////////////////////////////////////////////////////////////////////////
	// Write m_antCutFlexionAngle
	File.write( (char*) &m_antCutFlexionAngle, sizeof( double ) );
	// Write m_femoralAxesFlexionAngle
	File.write( (char*) &m_femoralAxesFlexionAngle, sizeof( double ) );
	// Write m_antCutPlaneCenter
	File.write( (char*) &m_antCutPlaneCenter, sizeof( IwPoint3d ) );

	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v5.0.0.a
	/////////////////////////////////////////////////////////////////////////////////////
	// Write m_femoralAxesYRotAngle
	File.write( (char*) &m_femoralAxesYRotAngle, sizeof( double ) );
	// Write m_femoralAxesZRotAngle
	File.write( (char*) &m_femoralAxesZRotAngle, sizeof( double ) );

	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v5.0.4.a
	/////////////////////////////////////////////////////////////////////////////////////
	// Write m_antCutObliqueAngle
	File.write( (char*) &m_antCutObliqueAngle, sizeof( double ) );

	/////////////////////////////////////////////////////////////////////////////////////
	// New parameters for v6.0.7
	/////////////////////////////////////////////////////////////////////////////////////
	// Write m_antCutPlaneRotationCenter
	File.write( (char*) &m_antCutPlaneRotationCenter, sizeof( IwPoint3d ) );

	File.close();

	////////////////////////////////////////////////////////////////////////////////////
	//// Save Validation Parameters ////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_fa_validation.dat~";

	QFile				FileValidation( sFileName );

	bOK = FileValidation.open( QIODevice::WriteOnly );

	if( !bOK )
		return false;

	FileValidation.write( (char*) &m_hipPointValidation, sizeof( IwPoint3d ) );
	FileValidation.write( (char*) &m_proximalPointValidation, sizeof( IwPoint3d ) );
	FileValidation.write( (char*) &m_leftEpiCondylarPointValidation, sizeof( IwPoint3d ) );
	FileValidation.write( (char*) &m_rightEpiCondylarPointValidation, sizeof( IwPoint3d ) );
	FileValidation.write( (char*) &m_antCutFlexionAngleValidation, sizeof( double ) );
	FileValidation.write( (char*) &m_femoralAxesFlexionAngleValidation, sizeof( double ) );
	FileValidation.write( (char*) &m_antCutPlaneCenterValidation, sizeof( IwPoint3d ) );

	FileValidation.close();
	//////////////////////////////////////////////////////////////////////////////////

	m_bDataModified = false;

	return true;
}


bool CFemoralAxes::Load( const QString& sDir )
{
	QString				sFileName;
	bool				bOK;

	sFileName = sDir + "\\" + m_sFileName + "_fa.dat";

	QFile				File( sFileName );

	bOK = File.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	// Variables for TriathlonF1 version 0.0
	IwPoint3d hipPoint, proximalPoint, leftEpiCondylarPoint, rightEpiCondylarPoint, proximalRefPoint;//m_proximalRefPoint
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{

		File.read( (char*) &hipPoint, sizeof( IwPoint3d ) );
		File.read( (char*) &proximalPoint, sizeof( IwPoint3d ) );
		File.read( (char*) &leftEpiCondylarPoint, sizeof( IwPoint3d ) );
		File.read( (char*) &rightEpiCondylarPoint, sizeof( IwPoint3d ) );
		File.read( (char*) &proximalRefPoint, sizeof( IwPoint3d ) );
		m_proximalRefPoint = proximalRefPoint;
	}

	// Variables for TriathlonF1 version 2.1 
	double antCutFlexionAngle, femoralAxesFlexionAngle;
	IwPoint3d antCutPlaneCenter;
	if ( m_pDoc->IsOpeningFileRightVersion(2,1,0) )
	{
		// Read m_antCutFlexionAngle
		File.read( (char*) &antCutFlexionAngle, sizeof( double ) );
		// Read m_femoralAxesFlexionAngle
		File.read( (char*) &femoralAxesFlexionAngle, sizeof( double ) );
		// Read m_antCutPlaneCenter
		File.read( (char*) &antCutPlaneCenter, sizeof( IwPoint3d ) );
	}

	// Variables for TriathlonF1 version 5.0.0 
	double femoralAxesYRotAngle = 0.0, femoralAxesZRotAngle = 0.0;
	if ( m_pDoc->IsOpeningFileRightVersion(5,0,0) )
	{
		// Read m_femoralAxesYRotAngle
		File.read( (char*) &femoralAxesYRotAngle, sizeof( double ) );
		// Read m_femoralAxesZRotAngle
		File.read( (char*) &femoralAxesZRotAngle, sizeof( double ) );
	}

	// Variables for TriathlonF1 version 5.0.4 
	double antCutObliqueAngle = 0.0;
	if ( m_pDoc->IsOpeningFileRightVersion(5,0,4) )
	{
		// Read m_antCutObliqueAngle
		File.read( (char*) &antCutObliqueAngle, sizeof( double ) );
	}

	// Variables for TriathlonF1 version 6.0.7 
	IwPoint3d antCutPlaneRotationCenter;
	if ( m_pDoc->IsOpeningFileRightVersion(6,0,7) )
	{
		// Read m_antCutPlaneRotationCenter
		File.read( (char*) &antCutPlaneRotationCenter, sizeof( IwPoint3d ) );
	}
	else
	{
		antCutPlaneRotationCenter = antCutPlaneCenter;
	}


	File.close();

	// Actions required by TriathlonF1 version 0.0
	if (m_pDoc->IsOpeningFileRightVersion(0,0,0))
	{
		// message and vStatus will be reset after call SetParams().
		// However we are not really set new params, but initialize params.
		QString message;
		CEntValidateStatus vStatus = GetValidateStatus(message);
		SetParams(hipPoint, proximalPoint,leftEpiCondylarPoint, rightEpiCondylarPoint, antCutFlexionAngle, antCutObliqueAngle, femoralAxesFlexionAngle, antCutPlaneCenter, antCutPlaneRotationCenter, femoralAxesYRotAngle, femoralAxesZRotAngle);
		// set back the original vStatus and message
		SetValidateStatus(vStatus, message);
		// notice m_pView to correct the predefined view angles to align with femoral axes
		IwAxis2Placement femoralAxes;
		GetSweepAxes(femoralAxes);
		double xAng, yAng, zAng;
		femoralAxes.DecomposeToAngles(xAng, yAng, zAng);
		m_pDoc->GetView()->SetAdditionViewingRotations(xAng*180.0/IW_PI, yAng*180.0/IW_PI, zAng*180.0/IW_PI);
	}


	//////////////////////////////////////////////////////////////////////////////
	//// Load Validation Parameters
	//////////////////////////////////////////////////////////////////////////////
	sFileName = sDir + "\\" + m_sFileName + "_fa_validation.dat";

	QFile				FileValidation( sFileName );

	bOK = FileValidation.open( QIODevice::ReadOnly );

	if( !bOK )
		return false;

	FileValidation.read( (char*) &m_hipPointValidation, sizeof( IwPoint3d ) );
	FileValidation.read( (char*) &m_proximalPointValidation, sizeof( IwPoint3d ) );
	FileValidation.read( (char*) &m_leftEpiCondylarPointValidation, sizeof( IwPoint3d ) );
	FileValidation.read( (char*) &m_rightEpiCondylarPointValidation, sizeof( IwPoint3d ) );
	FileValidation.read( (char*) &m_antCutFlexionAngleValidation, sizeof( double ) );
	FileValidation.read( (char*) &m_femoralAxesFlexionAngleValidation, sizeof( double ) );
	FileValidation.read( (char*) &m_antCutPlaneCenterValidation, sizeof( IwPoint3d ) );

	FileValidation.close();
	///////////////////////////////////////////////////////////////////////////////

	m_bDataModified = false;

	return true;
}

void CFemoralAxes::SetProximalRefPoint(IwPoint3d &proximalRefPoint)
{
	m_proximalRefPoint = proximalRefPoint;
}

IwPoint3d CFemoralAxes::GetRefProximalPoint()
{
	return m_proximalRefPoint;
}


void CFemoralAxes::SetParams
(
		IwPoint3d& hipPoint, 
		IwPoint3d& proximalPoint,
		IwPoint3d& leftEpiCondylarPoint,
		IwPoint3d& rightEpiCondylarPoint,
		double antCutFlexionAngle,
		double antCutObliqueAngle,
		double femoralAxesFlexionAngle,
		IwPoint3d& antCutPlaneCenter,
		IwPoint3d& antCutPlaneRotationCenter,
		double femoralAxesYRotAngle,
		double femoralAxesZRotAngle
)
{
	// If it only changes antCutPlaneCenter or antCutPlaneRotationCenter, we do not need to emit cood system
	bool emitCoordSystem = true;
	if (m_hipPoint==hipPoint && m_proximalPoint==proximalPoint && 
		m_leftEpiCondylarPoint==leftEpiCondylarPoint && m_rightEpiCondylarPoint==rightEpiCondylarPoint &&
		m_antCutFlexionAngle==antCutFlexionAngle && m_femoralAxesFlexionAngle==femoralAxesFlexionAngle)
		emitCoordSystem = false;

	m_hipPoint = hipPoint;
	m_proximalPoint = proximalPoint;
	m_leftEpiCondylarPoint = leftEpiCondylarPoint;
	m_rightEpiCondylarPoint = rightEpiCondylarPoint;
	m_antCutFlexionAngle = antCutFlexionAngle;
	m_antCutObliqueAngle = antCutObliqueAngle;
	m_femoralAxesFlexionAngle = femoralAxesFlexionAngle;
	m_femoralAxesYRotAngle = femoralAxesYRotAngle;
	m_femoralAxesZRotAngle = femoralAxesZRotAngle;
	m_antCutPlaneRotationCenter = antCutPlaneRotationCenter;
	m_antCutPlaneCenter = antCutPlaneCenter;

	// become not validate
	SetValidateStatus(VALIDATE_STATUS_NOT_VALIDATE, QString("Not validate."));

	// Need to update axes
	DetermineAxes();

	// m_antCutPlaneCenter needs to be updated once axes are determined.
	if ( m_antCutPlaneCenter.IsInitialized() && m_antCutPlaneRotationCenter.IsInitialized() )
	{
		IwPoint3d cutCenter, cutOrientation;
		GetAntCutPlaneInfo(cutCenter, cutOrientation);
		m_antCutPlaneCenter = m_antCutPlaneCenter.ProjectPointToPlane(m_antCutPlaneRotationCenter, cutOrientation);
	}

	// need to update m_cutPlaneCorners
	m_cutPlaneCorners.RemoveAll();
	if ( m_antCutPlaneCenter.IsInitialized() )
	{
		double planeHSize = 0.5*m_size;
		IwAxis2Placement antFAxes, antFOAxes;
		// rotate with anterior cut angle
		double antCutFlexionAngleRadian = m_antCutFlexionAngle/180.0*IW_PI;//85 degrees
		double antCutObliqueAngleRadian = m_antCutObliqueAngle/180.0*IW_PI;
		IwAxis2Placement rotOMat, rotFMat;
		rotFMat.RotateAboutAxis(antCutFlexionAngleRadian, IwVector3d(1,0,0));
		rotOMat.RotateAboutAxis(antCutObliqueAngleRadian, IwVector3d(0,1,0));
		rotFMat.TransformAxis2Placement(m_sweepAxes, antFAxes);//antFAxes = rotFMat * m_sweepAxes;
		rotOMat.TransformAxis2Placement(antFAxes, antFOAxes);//antFOAxes = rotOMat * antFAxes;

		// now the antFOAxes z-axis is the anterior cut normal (toward femur)
		IwPoint3d pnt0, pnt1, pnt2, pnt3;
		pnt0 = m_antCutPlaneCenter + planeHSize*antFOAxes.GetXAxis() + 0.5*planeHSize*antFOAxes.GetYAxis();
		pnt1 = m_antCutPlaneCenter + planeHSize*antFOAxes.GetXAxis() - 1.5*planeHSize*antFOAxes.GetYAxis();
		pnt2 = m_antCutPlaneCenter - planeHSize*antFOAxes.GetXAxis() - 1.5*planeHSize*antFOAxes.GetYAxis();
		pnt3 = m_antCutPlaneCenter - planeHSize*antFOAxes.GetXAxis() + 0.5*planeHSize*antFOAxes.GetYAxis();

		m_cutPlaneCorners.Add(pnt0);
		m_cutPlaneCorners.Add(pnt1);
		m_cutPlaneCorners.Add(pnt2);
		m_cutPlaneCorners.Add(pnt3);

		m_cutPlaneCorners.Add(antFOAxes.GetZAxis());// 4th element is normal
	}

	// notice m_pView to correct the predefined view angles to align with femoral axes
	CoordSystem femoralAxes, mechAxes;
	GetSweepAxes(femoralAxes);
	GetMechanicalAxes(mechAxes);
	double xAng, yAng, zAng;
	femoralAxes.DecomposeToAngles(xAng, yAng, zAng);
	m_pDoc->GetView()->SetAdditionViewingRotations(xAng*180.0/IW_PI, yAng*180.0/IW_PI, zAng*180.0/IW_PI);

	SetModifiedFlag(true);

	if ( emitCoordSystem )
	{
		QStringList icons;
		icons << ":/KTriathlonF1/Resources/ViewFront.png" << ":/KTriathlonF1/Resources/ViewBack.png"
			  << ":/KTriathlonF1/Resources/ViewLeft.png"  << ":/KTriathlonF1/Resources/ViewRight.png"
			  << ":/KTriathlonF1/Resources/ViewTop.png"   << ":/KTriathlonF1/Resources/ViewBottom.png"
			  << ":/KTriathlonF1/Resources/ViewFrontIso.png" << ":/KTriathlonF1/Resources/ViewBackIso.png";
		femoralAxes.SetIconFiles( icons );
		mechAxes.SetIconFiles( icons );

		mechAxes.SetName(m_pDoc->GetFemurMechCoordSysName());
		femoralAxes.SetName(m_pDoc->GetFemurImplantCoordSysName());

		emit BroadcastCoordSystem(mechAxes, false); // Mech Axis first
		emit BroadcastCoordSystem(femoralAxes, false); // Implant Axis 2nd
	}
}

void CFemoralAxes::SetParamsValidation(
		IwPoint3d& hipPoint, 
		IwPoint3d& proximalPoint,
		IwPoint3d& leftEpiCondylarPoint,
		IwPoint3d& rightEpiCondylarPoint,
		double antCutFlexionAngle,
		double antCutObliqueAngle,
		double femoralAxesFlexionAngle,
		IwPoint3d& antCutPlaneCenter)
{
	m_hipPointValidation = hipPoint;
	m_proximalPointValidation = proximalPoint;
	m_leftEpiCondylarPointValidation = leftEpiCondylarPoint;
	m_rightEpiCondylarPointValidation = rightEpiCondylarPoint;
	m_antCutFlexionAngleValidation = antCutFlexionAngle;
	m_antCutObliqueAngleValidation = antCutObliqueAngle;
	m_femoralAxesFlexionAngleValidation = femoralAxesFlexionAngle;
	m_antCutPlaneCenterValidation = antCutPlaneCenter;

	SetModifiedFlag(true);// To trigger "Save" function
}

void CFemoralAxes::GetParams
(
		IwPoint3d& hipPoint, 
		IwPoint3d& proximalPoint,
		IwPoint3d& leftEpiCondylarPoint,
		IwPoint3d& rightEpiCondylarPoint,
		double& antCutFlexionAngle,
		double& antCutObliqueAngle,
		double& femoralAxesFlexionAngle,
		IwPoint3d& antCutPlaneCenter,
		IwPoint3d& antCutPlaneRotationCenter,
		double& femoralAxesYRotAngle,
		double& femoralAxesZRotAngle
)
{
	hipPoint = m_hipPoint;
	proximalPoint = m_proximalPoint;
	leftEpiCondylarPoint = m_leftEpiCondylarPoint;
	rightEpiCondylarPoint = m_rightEpiCondylarPoint;
	antCutFlexionAngle = m_antCutFlexionAngle;
	antCutObliqueAngle = m_antCutObliqueAngle;
	femoralAxesFlexionAngle = m_femoralAxesFlexionAngle;
	femoralAxesYRotAngle = m_femoralAxesYRotAngle;
	femoralAxesZRotAngle = m_femoralAxesZRotAngle;
	antCutPlaneCenter = m_antCutPlaneCenter;
	antCutPlaneRotationCenter = m_antCutPlaneRotationCenter;
}

void CFemoralAxes::GetParamsValidation(
		IwPoint3d& hipPoint, 
		IwPoint3d& proximalPoint,
		IwPoint3d& leftEpiCondylarPoint,
		IwPoint3d& rightEpiCondylarPoint,
		double& antCutFlexionAngle,
		double& antCutObliqueAngle,
		double& femoralAxesFlexionAngle,
		IwPoint3d& antCutPlaneCenter)
{
	hipPoint = m_hipPointValidation;
	proximalPoint = m_proximalPointValidation;
	leftEpiCondylarPoint = m_leftEpiCondylarPointValidation;
	rightEpiCondylarPoint = m_rightEpiCondylarPointValidation;
	antCutFlexionAngle = m_antCutFlexionAngleValidation;
	antCutObliqueAngle = m_antCutObliqueAngleValidation;
	femoralAxesFlexionAngle = m_femoralAxesFlexionAngleValidation;
	antCutPlaneCenter = m_antCutPlaneCenterValidation;
}

bool CFemoralAxes::DetermineAxes()
{
	//m_rightEpiCondylarPoint = IwPoint3d(-67.17, -17.11, -520.30);
	//m_leftEpiCondylarPoint = IwPoint3d(-139.11, -8.80, -514.28);
	//m_proximalPoint = IwPoint3d(-104.30, -2.48, -534.40);

	IwPoint3d centerPoint = 0.5*(m_leftEpiCondylarPoint + m_rightEpiCondylarPoint);
	IwPoint3d origin = m_proximalPoint;
	IwVector3d zAxis = m_hipPoint - origin;
	zAxis.Unitize();
	IwVector3d hVector = m_rightEpiCondylarPoint - m_leftEpiCondylarPoint;
	m_size = hVector.Length();
	hVector.Unitize();
	IwVector3d yAxis = zAxis*hVector;
	IwVector3d xAxis = yAxis*zAxis;
	// additional flexion angle
	double additionalFlexionAngleRadian = m_femoralAxesFlexionAngle/180.0*IW_PI;//m_pDoc->GetAdvancedControl()->GetFemoralAxesFlexionAngle()/180.0*IW_PI;// radian
	// additional Y rotational angle
	double additionalYRotAngleRadian = m_femoralAxesYRotAngle/180.0*IW_PI;
	// additional Z rotational angle
	double additionalZRotAngleRadian = m_femoralAxesZRotAngle/180.0*IW_PI;

	// define m_mechanicalAxes
	m_mechanicalAxes.SetCanonical(origin, xAxis, yAxis);

	// matrix for rotating along x, y, z axes
	IwAxis2Placement rotMatX, rotMatY, rotMatZ, axesRotZ, axesRotZX, axesRotZXY;//, axesRotX, axesRotXY, axesRotXYZ;
	rotMatX.RotateAboutAxis(additionalFlexionAngleRadian, IwVector3d(1,0,0));
	rotMatY.RotateAboutAxis(additionalYRotAngleRadian, IwVector3d(0,1,0));
	rotMatZ.RotateAboutAxis(additionalZRotAngleRadian, IwVector3d(0,0,1));
	// rotate m_whiteSideLineAxes with additional***AngleRadian based on m_mechanicalAxes
	rotMatZ.TransformAxis2Placement(m_mechanicalAxes, axesRotZ);// axesRotZ=rotMatZ*m_mechanicalAxes
	rotMatX.TransformAxis2Placement(axesRotZ, axesRotZX);//axesRotZX=rotMatX*axesRotZ
	rotMatY.TransformAxis2Placement(axesRotZX, axesRotZXY);//axesRotZXY=axesRotZX*rotMatY

	m_whiteSideLineAxes = axesRotZXY;
	// define m_sweepAxes at centerPoint and parallel to m_whiteSideLineAxes X/Y axes
	m_sweepAxes = IwAxis2Placement(centerPoint, m_whiteSideLineAxes.GetXAxis(), m_whiteSideLineAxes.GetYAxis());

	m_axesPoints.RemoveAll();
	m_axesPoints.Add(origin);
	m_axesPoints.Add(origin + 0.65*m_size*m_whiteSideLineAxes.GetXAxis());
	m_axesPoints.Add(origin + 0.65*m_size*m_whiteSideLineAxes.GetYAxis());
	m_axesPoints.Add(origin + 0.75*m_size*m_whiteSideLineAxes.GetZAxis());
	m_mechAxesPoints.RemoveAll();
	m_mechAxesPoints.Add(origin);
	m_mechAxesPoints.Add(origin + 0.325*m_size*m_mechanicalAxes.GetXAxis());
	m_mechAxesPoints.Add(origin + 0.325*m_size*m_mechanicalAxes.GetYAxis());
	m_mechAxesPoints.Add(origin + 0.375*m_size*m_mechanicalAxes.GetZAxis());

	SetModifiedFlag(true);

	return true;
}

void CFemoralAxes::GetMechanicalAxes(IwAxis2Placement& axes)
{
	axes = m_mechanicalAxes;
}

void CFemoralAxes::GetImplantAxes(IwAxis2Placement& axes)
{
	axes = m_whiteSideLineAxes; // the same as the WhiteSideLineAxes
}

void CFemoralAxes::GetWhiteSideLineAxes(IwAxis2Placement& axes)
{
	axes = m_whiteSideLineAxes;
}

void CFemoralAxes::GetSweepAxes(IwAxis2Placement& axes)
{
	axes = m_sweepAxes;
}

void CFemoralAxes::GetAxesPoints
(
	IwTArray<IwPoint3d>& axesPoints	// O:
)
{
	axesPoints.RemoveAll();
	axesPoints.Append(m_axesPoints);
}

void CFemoralAxes::SetModifiedFlag( bool bModified )
{
	m_bModified = bModified;
	if (m_bModified)
	{
		m_bDataModified = true;
		m_pDoc->SetModified(m_bModified);
	}
}

void CFemoralAxes::GetSelectableEntities
(
	IwTArray<IwBrep*>&breps,		// O:
	IwTArray<IwCurve*>& curves,		// O:
	IwTArray<IwPoint3d>&points		// O:
)
{
	points.Add(m_sweepAxes.GetOrigin());
	points.Append(m_axesPoints);
	points.Append(m_mechAxesPoints);

}

CBounds3d CFemoralAxes::ComputeBounds( const CTransform& Trf, CBounds3d* pClipBounds )
{

	// Expand the Bounding box for curves
	CBounds3d	Bounds;
	IwExtent3d	crvBounds;
	CPoint3d   pnt;
	for (unsigned i=0; i<m_axesPoints.GetSize(); i++)
	{
		pnt = m_axesPoints.GetAt(i);
		CPoint3d pt = Trf * pnt;
		Bounds.Expand( pt );
	}

	m_Bounds = Bounds;

	return Bounds;
}

void CFemoralAxes::GetAntCutPlaneInfo
(
	IwPoint3d& antCutPlaneCenter,		// O:
	IwVector3d& antCutPlaneOrientation,	// O:
	IwVector3d* antCutPlaneRotationCenter// O:
)
{
	antCutPlaneCenter = m_antCutPlaneCenter;
	double flexionAngle = m_antCutFlexionAngle;
	double obliqueAngle = m_antCutObliqueAngle;
	double antCutFlexionAngleRadian = flexionAngle/180.0*IW_PI; // 85.0
	double antCutObliqueAngleRadian = obliqueAngle/180.0*IW_PI; 
	IwAxis2Placement femAxes;
	GetSweepAxes(femAxes);
	IwAxis2Placement rotFAxes, rotFOAxes;
	IwAxis2Placement rotFMat, rotOMat;
	rotFMat.RotateAboutAxis(antCutFlexionAngleRadian, IwVector3d(1,0,0));
	rotOMat.RotateAboutAxis(antCutObliqueAngleRadian, IwVector3d(0,1,0));
	rotFMat.TransformAxis2Placement(femAxes, rotFAxes);//rotFAxes=rotFMat*femAxes
	rotOMat.TransformAxis2Placement(rotFAxes, rotFOAxes);//rotFOAxes=rotOMat*rotFAxes

	antCutPlaneOrientation = rotFOAxes.GetZAxis();

	if ( antCutPlaneRotationCenter )
		*antCutPlaneRotationCenter = m_antCutPlaneRotationCenter;

	if(1)
	{
		IwPoint3d antPlaneCenter, antPlaneRotCenter;
		IFemurImplant::FemoralAxes_AntCenterPointAndAntRotCenterPoint(antPlaneCenter, antPlaneRotCenter);

		antCutPlaneCenter = antPlaneCenter;

		if ( antCutPlaneRotationCenter )
			*antCutPlaneRotationCenter = antPlaneRotCenter;
	}

	return;
}

/////////////////////////////////////////////////////////
// Run time data, only available during initialization
// 0/1 elements are for more coverage
// 2/3 elements are for less corveage
/////////////////////////////////////////////////////////
void CFemoralAxes::SetBackUpAntCutPlaneRotationCenters(IwTArray<IwPoint3d> centerPoints)
{
	m_backUpAntCutPlaneRotationCenters.RemoveAll();
	m_backUpAntCutPlaneRotationCenters.Append(centerPoints);
}

/////////////////////////////////////////////////////////
// Run time data, only available during initialization
// 0/1 elements are for more coverage
// 2/3 elements are for less corveage
/////////////////////////////////////////////////////////
bool CFemoralAxes::GetBackUpAntCutPlaneRotationCenters(IwTArray<IwPoint3d>& centerPoints)
{
	centerPoints.RemoveAll();

	if ( m_backUpAntCutPlaneRotationCenters.GetSize() != 4 )
		return false;

	IwPoint3d antCutPlaneCenetr, antCutPlaneOrientation;
	this->GetAntCutPlaneInfo(antCutPlaneCenetr, antCutPlaneOrientation);

	IwPoint3d moreCoverageRotationCenter = m_backUpAntCutPlaneRotationCenters.GetAt(0);
	IwPoint3d moreCoverageCenter = m_backUpAntCutPlaneRotationCenters.GetAt(1);
	moreCoverageCenter = moreCoverageCenter.ProjectPointToPlane(moreCoverageRotationCenter, antCutPlaneOrientation);

	IwPoint3d lessCoverageRotationCenter = m_backUpAntCutPlaneRotationCenters.GetAt(2);
	IwPoint3d lessCoverageCenter = m_backUpAntCutPlaneRotationCenters.GetAt(3);
	lessCoverageCenter = lessCoverageCenter.ProjectPointToPlane(lessCoverageRotationCenter, antCutPlaneOrientation);


	centerPoints.Add(moreCoverageRotationCenter);
	centerPoints.Add(moreCoverageCenter);
	centerPoints.Add(lessCoverageRotationCenter);
	centerPoints.Add(lessCoverageCenter);

	return true;
}

//////////////////////////////////////////////////////////////////////////
// The bearing boundaries are silhouettes of femur
bool CFemoralAxes::GetBearingRegionBoundaries
(
	IwTArray<IwCurve*>& posSilhouettes, // O:
	IwTArray<IwCurve*>& negSilhouettes	// O:
)
{
	posSilhouettes.RemoveAll();
	negSilhouettes.RemoveAll();

	if ( m_posSilhouetteCurves.GetSize() == 0 || m_negSilhouetteCurves.GetSize() == 0 )
		return false;

	posSilhouettes.Append(m_posSilhouetteCurves);
	negSilhouettes.Append(m_negSilhouetteCurves);

	return true;
}

void CFemoralAxes::SetBearingRegionBoundaries
(
	IwTArray<IwCurve*>& posSilhouettes, // I:
	IwTArray<IwCurve*>& negSilhouettes	// I:
)
{
	m_posSilhouetteCurves.RemoveAll();
	m_negSilhouetteCurves.RemoveAll();

	m_posSilhouetteCurves.Append(posSilhouettes);
	m_negSilhouetteCurves.Append(negSilhouettes);
}