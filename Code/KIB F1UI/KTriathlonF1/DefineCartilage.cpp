#include <QtOpenGL>
#pragma warning( disable : 4996 4805 )
#include <iwtslib_all.h>
#include <IwPoly.h>
#pragma warning( default : 4996 4805 )
#include "..\KAppTotal\Manager.h"
#include "DefineCartilage.h"
#include "CartilageSurface.h"
#include "FemoralPart.h"
#include "IFemurImplant.h"
#include "TotalMainWindow.h"
#include "TotalDoc.h"
#include "TotalView.h"
#include "..\KAppTotal\Utilities.h"
#include "UndoRedoCmdJigs.h"
#include "AdvancedControlJigs.h"
#include "..\KApp\SwitchBoard.h"

#include "FemoralAxes.h"


///////////////////////////////////////////////////////////////////////
// There are significant changes in iTW6. There exists cartilage boundary
// to define the cartilage region near weight-bearing area. In iTW6,
// there is no such cartilage boundary. The whole femur surface will
// be offset 3mm, plus the extra offset near the trochlear groove. 
///////////////////////////////////////////////////////////////////////

CDefineCartilage::CDefineCartilage( CTotalView* pView, CManagerActivateType manActType ) : CManager( pView, manActType )
{
    EnableUndoStack(true);

	bool activateUI = ( manActType == MAN_ACT_TYPE_EDIT || manActType == MAN_ACT_TYPE_REVIEW );

	m_pMainWindow = pView->GetMainWindow();

	m_pDoc = pView->GetTotalDoc();

	m_sClassName = "CDefineCartilage";

	m_toolbar = NULL;
	m_actReset = NULL;
	m_actAccept = NULL;
	m_actSmooth = NULL;
	m_actUpdate = NULL;
	m_actCancel = NULL;
	m_actValidate = NULL;
	m_cartilageSurfaceUpToDate = true;
	m_leftButtonDown = false;
	m_addDeleteMode = 0;
	m_actReviewBack = NULL;
	m_actReviewNext = NULL;
	m_actReviewRework = NULL;
	m_predefinedViewIndex = 0;
	// run time data to speed up performance
	double sizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;
	m_troCurveMaxDeviation = sizeRatio*m_pDoc->GetVarTableValue("JIGS CARTILAGE TROCHLEAR CURVE MAX DEVIATION");

	CAdvancedControlJigs *advCtrl = m_pDoc->GetAdvancedControlJigs();

	if ( manActType == MAN_ACT_TYPE_EDIT )
	{
		m_toolbar = new QToolBar("Define Cartilage");
		m_actDefineCartilage = m_toolbar->addAction("Cartilage");
		SetActionAsTitle( m_actDefineCartilage );
		m_actReset = m_toolbar->addAction("Reset");
		EnableAction( m_actReset, true );
		if ( advCtrl->GetCartilageDisplaySmooth() )
		{
			m_actSmooth = m_toolbar->addAction("Smooth");
			m_actSmooth->setToolTip("Select a region to smooth");
			EnableAction( m_actSmooth, true );
			m_pMainWindow->connect( m_actSmooth, SIGNAL( triggered() ), this, SLOT( OnSmooth() ) );
		}
		m_actUpdate = m_toolbar->addAction("Update");
		EnableAction( m_actUpdate, false );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		m_actAccept = m_toolbar->addAction("Accept");
		EnableAction( m_actAccept, true );
		//m_actCancel = m_toolbar->addAction("Cancel");// No cancel 
		//EnableAction( m_actCancel, true );

		m_pMainWindow->connect( m_actReset, SIGNAL( triggered() ), this, SLOT( OnReset() ) );
		m_pMainWindow->connect( m_actAccept, SIGNAL( triggered() ), this, SLOT( OnAccept() ) );
		m_pMainWindow->connect( m_actUpdate, SIGNAL( triggered() ), this, SLOT( OnUpdate() ) );
		m_pMainWindow->connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		//m_pMainWindow->connect( m_actCancel, SIGNAL( triggered() ), this, SLOT( OnCancel() ) );

	}
	else if (manActType == MAN_ACT_TYPE_REVIEW)
	{
		m_toolbar = new QToolBar("Define Cartilage");
		m_actReviewRework = m_toolbar->addAction("Rework");
		EnableAction( m_actReviewRework, true );
		m_actReviewBack = m_toolbar->addAction("BackPS");
		m_actReviewBack->setToolTip("Back to PS Features");
		EnableAction( m_actReviewBack, true );
		m_actReviewNext = m_toolbar->addAction("Next");
		EnableAction( m_actReviewNext, true );

		m_actDefineCartilage = m_toolbar->addAction("Cartilage");
		SetActionAsTitle( m_actDefineCartilage );
		m_actValidate = m_toolbar->addAction( QIcon( ":/KTriathlonF1/Resources/Validate.png" ), QString("Not validate.") );
		EnableAction( m_actValidate, true );
		connect( m_actValidate, SIGNAL( triggered() ), this, SLOT( OnValidate() ) );
		connect( m_actReviewBack, SIGNAL( triggered() ), this, SLOT( OnReviewBack() ) );
		connect( m_actReviewNext, SIGNAL( triggered() ), this, SLOT( OnReviewNext() ) );
		connect( m_actReviewRework, SIGNAL( triggered() ), this, SLOT( OnReviewRework() ) );
	}

	// Get the femur surface for easy referencing
	bool bCopy = false;
	IFemurImplant::FemoralPart_GetSurfaces(bCopy, m_femurMainSurfRef, m_femurLeftSurfRef, m_femurRightSurfRef);

	CCartilageSurface* cartilageSurface = m_pDoc->GetCartilageSurface();
	if ( cartilageSurface == NULL ) // if no object 
	{
		// We want the advanced control jig be the first jig entity
		m_pDoc->GetAdvancedControlJigs(); // This function will create advCtrl if not exist;
		//
		cartilageSurface = new CCartilageSurface( m_pDoc );
		m_pDoc->AddEntity( cartilageSurface, true );

		// Reset is the way to define the cartilage surface in default situation.
		bool constructSurf = !advCtrl->GetCartilageInitializeEdgeOnly();
		Reset( activateUI, constructSurf );
		if ( m_actUpdate && !constructSurf )
			EnableAction( m_actUpdate, true );
		if ( m_actAccept && !constructSurf )
			EnableAction( m_actAccept, false );
	}
	else
	{
		// Load the parameters
		cartilageSurface->GetTrochlearGroovePoints(m_currTroGroovePoints);
		cartilageSurface->GetTrochlearGroovePoints(m_oldTroGroovePoints);
		cartilageSurface->GetTrochlearGrooveRefPoints(m_troGrooveRefPoints);
		// Determine trochlear groove anterior reference points
		DetermineTroGrooveAnteriorReferencePoints(m_currTroGroovePoints);
		// Convert to XYZ space
		cartilageSurface->ConvertUVToXYZ(m_currTroGroovePoints, m_currTroGrooveXYZPoints, NULL);
		//
		IwTArray<IwPoint3d> totalPoints;
		totalPoints.Append(m_currTroGrooveXYZPoints);
		SetMouseMoveSearchingList(totalPoints);

		SetCartilageSurfaceUpToDate(true);

		if ( !cartilageSurface->GetUpToDateStatus() )
			OnUpdate();
		OnValidate();
	}

	SwitchBoard::addSignalSender(SIGNAL(SwitchToCoordSystem(QString)), this);
	emit SwitchToCoordSystem(m_pDoc->GetFemurImplantCoordSysName());

	if ( !activateUI )
		OnAccept();

	if ( manActType == MAN_ACT_TYPE_REVIEW )
	{
		OnReviewPredefinedView();
	}

	m_pView->Redraw();
}

CDefineCartilage::~CDefineCartilage()
{
	CCartilageSurface* cartilageSurface = m_pDoc->GetCartilageSurface();
	if ( cartilageSurface != NULL )
	{
		int previoisDesignTime = cartilageSurface->GetDesignTime();
		int thisDesignTime = GetElapseTime();
		cartilageSurface->SetDesignTime(previoisDesignTime+thisDesignTime);
	}
	SwitchBoard::removeSignalSender(this);
}

void CDefineCartilage::Display(Viewport* vp)
{		
	IwVector3d viewVec = m_pView->GetViewingVector();

	glDrawBuffer( GL_BACK );

	if (m_leftButtonDown)
	{
		IwPoint3d caughtPnt;
		int caughtIndex;
		if ( GetMouseMoveCaughtPoint(caughtPnt, caughtIndex) )
		{
			glDisable( GL_LIGHTING );
			glPointSize(7);
			glColor3ub( 255, 255, 255 );

			DisplayCaughtPoint(-50*viewVec);

			glPointSize(1);
			glEnable( GL_LIGHTING );
		}
		else if ( m_smoothRegionLoop3d.GetSize() > 0 )
		{
			glDisable( GL_LIGHTING );
			glColor3ub( 0, 0, 0 );
			glLineWidth(1.0);

			DisplaySmoothRegionLoop(-50*viewVec);

			glEnable( GL_LIGHTING );
		}

	}
	else
	{
		glDisable( GL_LIGHTING );
		// Display the control points
		DisplayTrochlearControlPoints(-25*viewVec);
		// Display reference points
		DisplayTrochlearGrooveReferencePoints(-25*viewVec);
		// Display the caught point
		glPointSize(6);
		glColor3ub( 255, 255, 0 );
		DisplayCaughtPoint(-50*viewVec);

		glPointSize(1);
		glEnable( GL_LIGHTING );
	}

}

void CDefineCartilage::DisplayTrochlearControlPoints(IwVector3d offset)
{
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	IwPoint3d pnt;
	glColor3ub( 0, 64, 0 );
	glPointSize(5);
	IwVector3d tempVec;
	bool displayRefPoint = false;
	for (unsigned i=0; i<m_currTroGrooveXYZPoints.GetSize(); i++)
	{
		displayRefPoint = false;
		tempVec = m_currTroGrooveXYZPoints.GetAt(i) - wslAxes.GetOrigin();
		if ( fabs(tempVec.Dot(wslAxes.GetXAxis())) > m_troCurveMaxDeviation )
			glColor3ub( 255, 100, 0 );
		else
			glColor3ub( 0, 64, 0 );
		// 
		if ( i > 0 && m_troGrooveRefPoints.GetSize() > 0 )// skip 1st one (ant ctrl pnt)
		{
			tempVec = m_currTroGrooveXYZPoints.GetAt(i) - m_troGrooveRefPoints.GetAt(i);
			double dist = tempVec.Dot(wslAxes.GetXAxis());
			if ( fabs(tempVec.Dot(wslAxes.GetXAxis())) > m_troCurveMaxDeviation ) // ML deviation > m_troCurveMaxDeviation
			{
				glColor3ub( 255, 100, 0 );
				displayRefPoint = true;
			}
		}

		glBegin( GL_POINTS );
			pnt = m_currTroGrooveXYZPoints.GetAt(i) + offset; // move closer to the viewer.
			glVertex3d( pnt.x, pnt.y, pnt.z );
		glEnd();
		if (displayRefPoint)
		{
			glColor3ub( 255, 0, 255 );
			glPointSize(7);
			glBegin( GL_POINTS );
				pnt = m_troGrooveRefPoints.GetAt(i) + offset; // move closer to the viewer.
				glVertex3d( pnt.x, pnt.y, pnt.z );
			glEnd();
			glPointSize(5);
		}
	}

}

void CDefineCartilage::DisplayTrochlearGrooveReferencePoints(IwVector3d offset)
{
	IwPoint3d pnt;
	glColor3ub(212, 0, 212);
	glLineWidth(3.0);
	glBegin( GL_LINE_STRIP );
		for (unsigned i=0; i<m_troGrooveAntRefPoints.GetSize(); i++)
		{
			pnt = m_troGrooveAntRefPoints.GetAt(i) + offset; // move closer to the viewer.
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}
	glEnd();
}

void CDefineCartilage::DisplaySmoothRegionLoop(IwVector3d offset)
{
	if ( m_smoothRegionLoop3d.GetSize() == 0 )
		return;

	IwPoint3d pnt;

	glBegin( GL_LINE_STRIP );
		for (unsigned i=0; i<m_smoothRegionLoop3d.GetSize(); i++)
		{
			pnt = m_smoothRegionLoop3d.GetAt(i) + offset;
			glVertex3d( pnt.x, pnt.y, pnt.z );
		}
	glEnd();
}

void CDefineCartilage::OnReset()
{
	int button = QMessageBox::warning( NULL, 
									QString("Cartilage Reset!"), 
									QString("Cartilage edge will be reset."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return;

	bool constructSurf = !m_pDoc->GetAdvancedControlJigs()->GetCartilageInitializeEdgeOnly();
	Reset(true, constructSurf);
}

void CDefineCartilage::Reset(bool activateUI, bool constructSurf)
{
	m_pDoc->AppendLog( QString("CDefineCartilage::Reset(), constructSurf=%1").arg(constructSurf) );

	CCartilageSurface* pCartilageSurf = m_pDoc->GetCartilageSurface();
	if ( pCartilageSurf == NULL )
		return;

	QApplication::setOverrideCursor( Qt::WaitCursor );

	// iTW v6 does not define cartilage edge, but only trochlear groove
	DetermineTrochlearGroove(m_currTroGroovePoints, m_troGrooveRefPoints);
	pCartilageSurf->SetTrochlearGroovePoints(m_currTroGroovePoints);
	pCartilageSurf->SetTrochlearGrooveRefPoints(m_troGrooveRefPoints);

	pCartilageSurf->ConvertUVToXYZ(m_currTroGroovePoints, m_currTroGrooveXYZPoints, NULL);
	IwTArray<IwPoint3d> totalPnts;
	totalPnts.Append(m_currTroGrooveXYZPoints);

	SetMouseMoveSearchingList(totalPnts);

	// Why not to construct the cartilage surface?
	// In case, the cartilage edge is not right when initialize.
	if ( constructSurf )
	{
		ConstructCartilageSurface();

		SetCartilageSurfaceUpToDate(true);
	}

	// Change the status sign of itself
	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_CARTILAGE_SURFACE, m_validateStatus, m_validateMessage);
	m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_CARTILAGE_SURFACE, false, false); // therefore, cartilage become out of date

	m_pView->Redraw();

	QApplication::restoreOverrideCursor();
}

void CDefineCartilage::OnAccept()
{
	m_pDoc->AppendLog( QString("CDefineCartilage::OnAccept()") );

	if ( !m_cartilageSurfaceUpToDate )
	{
		int		button;
		button = QMessageBox::warning( NULL, 
										tr("Warning Message!"), 
										tr("Cartilage surface has to be updated before accepting.\n"), 
										QMessageBox::Ok );	

		if( button == QMessageBox::Ok ) return;
	}

	CCartilageSurface* pCartilageSurface = m_pDoc->GetCartilageSurface();

	if ( pCartilageSurface )
	{
		if ( !pCartilageSurface->GetUpToDateStatus() || pCartilageSurface->IsMemberDataChanged(m_oldTroGroovePoints) )
			m_pDoc->UpdateEntPanelStatusMarkerJigs(ENT_ROLE_CARTILAGE_SURFACE, true, true);
		if ( m_validateStatus == VALIDATE_STATUS_NOT_VALIDATE ) // use runtime m_validateStatus, rather than outlineProfileJigs->GetValidateStatus()
			OnValidate();
	}

	// automactically save the file, if need
	m_pDoc->FileSaveAuto();

	m_bDestroyMe = true;
	m_pView->Redraw();
}


void CDefineCartilage::OnUpdate()
{
	m_pDoc->AppendLog( QString("CDefineCartilage::OnUpdate()") );

	ConstructCartilageSurface();

	EnableAction( m_actAccept, true );

	SetCartilageSurfaceUpToDate(true);

	m_pView->Redraw();

}

void CDefineCartilage::OnCancel()
{
	m_pDoc->AppendLog( QString("CDefineCartilage::OnCancel()") );

	m_bDestroyMe = true;
	m_pView->Redraw();
}

void CDefineCartilage::OnSmooth()
{
	m_pDoc->AppendLog( QString("CDefineCartilage::OnSmooth()") );

	QApplication::setOverrideCursor( Qt::CrossCursor );
	m_addDeleteMode = 3; // 0: none, 1:add, 2:delete, 3:smooth
	EnableAction( m_actSmooth, false );

	m_pView->Redraw();

}

///////////////////////////////////////////////////////////////
// This function determines the default trochlear groove curve.
///////////////////////////////////////////////////////////////
bool CDefineCartilage::DetermineTrochlearGroove
(
	IwTArray<IwPoint3d> &troPoints,		// O: (u,v,0) in femur surface UV domain
	IwTArray<IwPoint3d> &troRefPoints	// O: (x,y,z) in xyz space
)
{
	m_pDoc->AppendLog( QString("CDefineCartilage::DetermineTrochlearGroove()") );

	troPoints.RemoveAll();
	troRefPoints.RemoveAll();

	double troCurveMaxDeviationSafety = m_troCurveMaxDeviation - 0.5;

	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
	}
	else
	{
		positiveSideIsLateral = false;
	}

	// Get femoral axes & size & ant cut plane
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();
	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	double sizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;
	IwPoint3d antPlaneCtr;
	IwVector3d antPlaneNormal;
	IFemurImplant::FemoralAxes_GetAntCutPlaneInfo(antPlaneCtr, antPlaneNormal);

	// Get solid implant feature points
	IwTArray<IwPoint3d> SIFeatPoints;
	
	// Get cuts info
	IwTArray<IwPoint3d> cutPoints, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);

	// Get J curves
	bool bCopy = false;
	IwTArray<IwCurve*> jcurves;
	//IFemurImplant::JCurves_GetJCurves(bCopy, jcurves);

	IFemurImplant::JCurvesInp_GetJCurves(jcurves);//PSV --- for removal of J curve

	// Get outline profile
	bCopy = false;

	// Get solid implant outer edge curve
	bCopy = false;

	IwExtent2d surfDom = m_femurMainSurfRef->GetNaturalUVDomain();
	//////////////////////////////////////////////////////////////////////
	// The trochlear groove at femoral origin point
	IwPoint3d originPnt = m_pDoc->GetFemoralAxes()->GetRefProximalPoint();//wslAxes.GetOrigin();
	IwPoint3d cPnt;
	IwPoint2d paramOrigin;
	DistFromPointToSurface(originPnt, m_femurMainSurfRef, cPnt, paramOrigin);
	// Move a little posterior to get a better result.
	if ( surfDom.GetUMax() > surfDom.GetVMax() )
		paramOrigin += IwPoint2d(3.0, 0);
	else
		paramOrigin += IwPoint2d(0, 3.0);
	// update originPnt
	m_femurMainSurfRef->EvaluatePoint(paramOrigin, originPnt);
if (0)
ShowPoint(m_pDoc, originPnt, green);

	///////////////////////////////////////////////////////////////
	// Determine the trochlear groove between the cut edge (18, 19)
	IwVector3d tempVec;
	// pnt18 & pnt19
	IwPoint3d pnt18, pnt19;

	if(1)
	{
		IwTArray<IwPoint3d> cutFeaturePoints, antOuterPoints;
		IFemurImplant::FemoralCuts_GetCutsFeaturePoints(cutFeaturePoints, antOuterPoints);

		pnt18 = cutFeaturePoints.GetAt(18);
		pnt19 = cutFeaturePoints.GetAt(19);
	}

	// Determine the trochlear groove bottom point between 18 & 19
	double param, dist, minDist = HUGE_DOUBLE;
	IwPoint3d mid189Pnt;
	if ( m_pDoc->IsPositiveSideLateral() )
		mid189Pnt = 0.45*pnt18+0.55*pnt19;
	else
		mid189Pnt = 0.55*pnt18+0.45*pnt19;

	if(0)
	{
		mid189Pnt = IwPoint3d(-4.69, 30.33, 17.57) + IwPoint3d(-98.76, 194.27, 272.24);//PSV---
	}

	ShowPoint(m_pDoc, pnt18, blue);
	ShowPoint(m_pDoc, pnt19, green);

	ShowPoint(m_pDoc, mid189Pnt, red);//PSV---

	// Determine the closest point on trochlear j curve and the tangect vector there.
	DistFromPointToCurve(mid189Pnt, (IwBSplineCurve*)jcurves.GetAt(2), cPnt, param, NULL);
	IwVector3d evals[2];
	jcurves.GetAt(2)->Evaluate(param, 1, FALSE, evals);
	IwVector3d tangVec = evals[1];
	tangVec.Unitize();
	IwVector3d mYaxis = sweepAxes.GetXAxis()*tangVec;// inward femur
	// determine cross section
	IwTArray<IwCurve*> intCurves;
	IntersectSurfaceByPlane(m_femurMainSurfRef, mid189Pnt, tangVec, intCurves);
	IwPoint3d farMid189Pnt = mid189Pnt + 10*sizeRatio*mYaxis;
	IwPoint3d closestPnt;
	IwPoint3d closestMid189Point = mid189Pnt;// initialize
	for (unsigned i=0; i<intCurves.GetSize(); i++)
	{
		dist = DistFromPointToCurve(farMid189Pnt, (IwBSplineCurve*)intCurves.GetAt(i), cPnt, param, NULL);
		if ( dist < minDist )
		{
			closestPnt = cPnt;
			minDist = dist;
		}
	}
	tempVec = closestPnt - mid189Pnt;
	if ( fabs(tempVec.Dot(sweepAxes.GetXAxis())) < 15.0*sizeRatio )// in case wierd result
	{
		closestMid189Point = closestPnt;
	}
	// Check whether it deviates from sagittal plane too much 4.5mm
	tempVec = closestMid189Point - wslAxes.GetOrigin();
	if ( fabs(tempVec.Dot(wslAxes.GetXAxis())) > troCurveMaxDeviationSafety ) 
	{
		IwPoint3d projSagPnt = closestMid189Point.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		tempVec = closestMid189Point - projSagPnt;
		tempVec.Unitize();
		closestMid189Point = projSagPnt + troCurveMaxDeviationSafety*tempVec;
	}
	// map to uv parametric domain
	IwPoint2d paramMid189;
	DistFromPointToSurface(closestMid189Point, m_femurMainSurfRef, closestMid189Point, paramMid189);
if (0)
ShowPoint(m_pDoc, closestMid189Point, green);

	///////////////////////////////////////////////////////////////////
	// the point in between the closestOriginPoint and closestMidPoint
	minDist = HUGE_DOUBLE;
	IwPoint2d paramMid = 0.5*paramOrigin + 0.5*paramMid189;
	IwPoint3d midPnt;
	m_femurMainSurfRef->EvaluatePoint(paramMid, midPnt);
	// Determine the closest point on trochlear j curve and the tangect vector there.
	DistFromPointToCurve(midPnt, (IwBSplineCurve*)jcurves.GetAt(2), cPnt, param, NULL);
	jcurves.GetAt(2)->Evaluate(param, 1, FALSE, evals);
	tangVec = evals[1];
	tangVec.Unitize();
	mYaxis = sweepAxes.GetXAxis()*tangVec;// inward femur
	// determine cross section
	intCurves.RemoveAll();
	IntersectSurfaceByPlane(m_femurMainSurfRef, midPnt, tangVec, intCurves);
	IwPoint3d farMidPnt = midPnt + 10*sizeRatio*mYaxis;
	IwPoint3d closestMidPoint = midPnt;// initialize
	for (unsigned i=0; i<intCurves.GetSize(); i++)
	{
		dist = DistFromPointToCurve(farMidPnt, (IwBSplineCurve*)intCurves.GetAt(i), cPnt, param, NULL);
		if ( dist < minDist )
		{
			closestPnt = cPnt;
			minDist = dist;
		}
	}
	tempVec = closestPnt - midPnt;
	if ( fabs(tempVec.Dot(sweepAxes.GetXAxis())) < 15.0*sizeRatio )// in case wierd result
	{
		closestMidPoint = closestPnt;
	}
	// Check whether it deviates from sagittal plane too much 4.5mm
	tempVec = closestMidPoint - wslAxes.GetOrigin();
	if ( fabs(tempVec.Dot(wslAxes.GetXAxis())) > troCurveMaxDeviationSafety )
	{
		IwPoint3d projSagPnt = closestMidPoint.ProjectPointToPlane(wslAxes.GetOrigin(), wslAxes.GetXAxis());
		tempVec = closestMidPoint - projSagPnt;
		tempVec.Unitize();
		closestMidPoint = projSagPnt + troCurveMaxDeviationSafety*tempVec;
	}
	// map to uv parametric domain
	DistFromPointToSurface(closestMidPoint, m_femurMainSurfRef, closestMidPoint, paramMid);
if (0)
ShowPoint(m_pDoc, closestMidPoint, green);

	/////////////////////////////////////////////////////////////////////////
	// The point close to the anterior tip, which location = 2*paramMid189 - paramMid
	// The trochlear groove near the anterior may not really have a lowest "groove".
	// Therefore, it is better to estimate its location, rather than determine the lowest point.
	IwPoint2d paramAnt = 2*paramMid189 - paramMid;
	// Check not to exceed the anterior boundary. It could be u or v.
	if ( paramAnt.x < 0 )
		paramAnt = IwPoint2d(0, paramAnt.y);
	if ( paramAnt.y < 0 )
		paramAnt = IwPoint2d(paramAnt.x, 0);
	IwPoint3d closestAntPoint;
	m_femurMainSurfRef->EvaluatePoint(paramAnt, closestAntPoint);
if (0)
ShowPoint(m_pDoc, closestAntPoint, green);
	// However, the parametric length near anterior shaft is not necessarily equal (or close) to the main area.
	// Need to adjust if the closestAntPoint is too anterior 
	if ( closestMid189Point.DistanceBetween(closestAntPoint) > 1.25*closestMid189Point.DistanceBetween(closestMidPoint) )
	{
		tempVec = closestAntPoint - closestMid189Point;
		tempVec.Unitize();
		closestAntPoint = closestMid189Point + 1.25*closestMid189Point.DistanceBetween(closestMidPoint)*tempVec;
		// map to uv parametric domain
		DistFromPointToSurface(closestAntPoint, m_femurMainSurfRef, closestAntPoint, paramAnt);
if (0)
ShowPoint(m_pDoc, closestAntPoint, green);
	}
	// Make sure paramAnt is in the anterior side of F3 profile
	double F3APThickness = m_pDoc->GetVarTableValue("JIGS F3 SKETCH AP WIDTH") + 4.0 + 1.0; // 4.0 + 1.0 as extra allowance
	IwPoint3d lateralDistalCutPnt;
	if ( positiveSideIsLateral )
	{
		lateralDistalCutPnt = cutPoints.GetAt(6);
	}
	else
	{
		lateralDistalCutPnt = cutPoints.GetAt(7);
	}
	tempVec = closestAntPoint - lateralDistalCutPnt;
	double distToDistalCut = tempVec.Dot(sweepAxes.GetZAxis());
	if ( distToDistalCut < F3APThickness )
	{
		closestAntPoint = closestAntPoint + (F3APThickness-distToDistalCut)*sweepAxes.GetZAxis();// Let it pass.
		DistFromPointToSurface(closestAntPoint, m_femurMainSurfRef, closestAntPoint, paramAnt);
	}
if (0)
ShowPoint(m_pDoc, closestAntPoint, darkGreen);

	////Note the sequence ////////////////////////////////////////////////
	troPoints.Add(IwPoint3d(paramAnt.x, paramAnt.y, 0));
	troPoints.Add(IwPoint3d(paramMid189.x, paramMid189.y, 0));
	troPoints.Add(IwPoint3d(paramMid.x, paramMid.y, 0));
	troPoints.Add(IwPoint3d(paramOrigin.x, paramOrigin.y, 0));
	//
	troRefPoints.Add(closestAntPoint);
	troRefPoints.Add(closestMid189Point);
	troRefPoints.Add(closestMidPoint);
	troRefPoints.Add(originPnt);

	// Determine trochlear groove reference points
	DetermineTroGrooveAnteriorReferencePoints(troPoints);

	return true;
}

////////////////////////////////////////////////////////////////////////////
// This function uses the cartilage edge curve and the other thickness curves 
// to offset the femur surface.
////////////////////////////////////////////////////////////////////////////
void CDefineCartilage::ConstructCartilageSurface()
{
	m_pDoc->AppendLog( QString("CDefineCartilage::ConstructCartilageSurface()") );

	QApplication::setOverrideCursor( Qt::WaitCursor );

	// Get a set of femur (3) surfaces to create cartilage Brep (see below)
	IwBSplineSurface *mainSurf3, *leftSurf3, *rightSurf3;
	bool bCopy = true;
	IFemurImplant::FemoralPart_GetSurfaces(bCopy, mainSurf3, leftSurf3, rightSurf3);

	// Get cartilage thickness and transition width
	CAdvancedControlJigs* advCtrl = m_pDoc->GetAdvancedControlJigs();
	double cartiThickness = advCtrl->GetCartilageThickness() + 0.05;
	double troGroTransWidth = advCtrl->GetTrochlearGrooveCartilageTransitionWidth();

	////////////////////////////////////////////////////////////////////////////////
	// Create a trochlear groove cartilage surface, which represents the additional 
	// cartilage thickness around trochlear groove.
	CCartilageSurface* pCartilageSurface = m_pDoc->GetCartilageSurface();
	IwBSplineCurve* troGroUVCurve = pCartilageSurface->GetTrochlearGrooveUVCurve();
	IwBSplineSurface* troGroCartiSurface = CreateTrochlearGrooveSurface(troGroUVCurve);


//ShowSurface(m_pDoc, troGroCartiSurface, "Surface_1", black);

	IwExtent2d troGroCartiDom = troGroCartiSurface->GetNaturalUVDomain();

	////////////////////////////////////////////////////////////////////
	// Use m_femurMainSurfRef and m_femurLeftSurfRef and m_femurRightSurfRef 
	// to determine the surface normal info only. Then offset the control 
	// points of mainSurf3 and leftSurf3 and rightSurf3. 
	// Note, mainSurf3 and leftSurf3 and rightSurf3 are modified,
	// but mainSurf2 and leftSurf2 and rightSurf2 are intact.

#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	mainSurf3->Notify(IW_NO_PRE_EDIT, mainSurf3, nullptr, nullptr);
	leftSurf3->Notify(IW_NO_PRE_EDIT, leftSurf3, nullptr, nullptr);
	rightSurf3->Notify(IW_NO_PRE_EDIT, rightSurf3, nullptr, nullptr);
#else
	mainSurf3->Notify(IW_NO_PRE_EDIT);
	leftSurf3->Notify(IW_NO_PRE_EDIT);
	rightSurf3->Notify(IW_NO_PRE_EDIT);
#endif
	// Get the knot info 
	ULONG ukCount, vkCount;
	double *uKnots, *vKnots;
	m_femurMainSurfRef->GetKnotsPointers(ukCount, vkCount, uKnots, vKnots);
	// Get control points
	ULONG ucCount, vcCount;
	double* cPointers;
	m_femurMainSurfRef->GetControlPointsPointer(ucCount, vcCount, cPointers);
	// Get the knot info of left surface
	ULONG ukCountLeft, vkCountLeft;
	double *uKnotsLeft, *vKnotsLeft;
	m_femurLeftSurfRef->GetKnotsPointers(ukCountLeft, vkCountLeft, uKnotsLeft, vKnotsLeft);
	// Get control points of left surface
	ULONG ucCountLeft, vcCountLeft;
	double* cPointersLeft;
	m_femurLeftSurfRef->GetControlPointsPointer(ucCountLeft, vcCountLeft, cPointersLeft);
	// Get the knot info of right surface
	ULONG ukCountRight, vkCountRight;
	double *uKnotsRight, *vKnotsRight;
	m_femurRightSurfRef->GetKnotsPointers(ukCountRight, vkCountRight, uKnotsRight, vKnotsRight);
	// Get control points of right surface
	ULONG ucCountRight, vcCountRight;
	double* cPointersRight;
	m_femurRightSurfRef->GetControlPointsPointer(ucCountRight, vcCountRight, cPointersRight);

	//// for mainSurf3 and leftSurf3 and rightSurf3 ////////////////////////////////////
	// Get control points
	ULONG ucCount3, vcCount3;
	double* cPointers3;
	mainSurf3->GetControlPointsPointer(ucCount3, vcCount3, cPointers3);
	// Get control points of left surface
	ULONG ucCountLeft3, vcCountLeft3;
	double* cPointersLeft3;
	leftSurf3->GetControlPointsPointer(ucCountLeft3, vcCountLeft3, cPointersLeft3);
	// Get control points of right surface
	ULONG ucCountRight3, vcCountRight3;
	double* cPointersRight3;
	rightSurf3->GetControlPointsPointer(ucCountRight3, vcCountRight3, cPointersRight3);

	double uMin = uKnots[3];
	double uMax = uKnots[ukCount-1];
	double uDelta = (uMax - uMin)/(ucCount-1);// only (ucCount-1) spans
	double vMin = vKnots[3];
	double vMax = vKnots[vkCount-1];
	double vDelta = (vMax - vMin)/(vcCount-1);// only (vcCount-1) spans
	double uParam, vParam, param;
	double effectCartiThickness, distToTroGro, dist, distToEdge;
	int cpIndex;
	IwPoint2d param2d, guessParam;
	IwPoint3d cPoint, cUVPnt, param3d;
	IwPoint3d closestPoint, closestPointOnCurve, offsetPoint;
	IwVector3d normal, troGroNormal, offsetVector, tempVec;
	IwPointClassification pClass;
	int bLeftRight;
	bool bCorner;
	ULONG uIndexOut, vIndexOut, uIndexOutNext, vIndexOutNext;

	////////////////////////////////////////////////////////////////////////////////
	// Modify along the 2 u boundaries first
	for (ULONG ui=0; ui<ucCount; ui+=(ucCount-1))// first one and last one
	{
		uParam = uMin + ui*uDelta;
		for (ULONG vi=0; vi<vcCount; vi++)// every one 
		{
			vParam = vMin + vi*vDelta;
			guessParam = IwPoint2d(uParam, vParam);
			cpIndex = 4*(vi+ui*vcCount);
			cPoint = IwPoint3d(cPointers[cpIndex], cPointers[cpIndex+1], cPointers[cpIndex+2]);
			// Call the local optimization DistFromPointToSurface() to speed up calculation.
			dist = DistFromPointToSurface(cPoint, m_femurMainSurfRef, guessParam, closestPoint, param2d);
			m_femurMainSurfRef->EvaluateNormal(param2d, FALSE, FALSE, normal);
			// offset with 100% cartiThickness
			if (1)
			{
				offsetVector = cartiThickness*normal;
				offsetPoint = cPoint + offsetVector;
				// move control point of mainSurf3 with cartiThickness distance
				cPointers3[cpIndex] = offsetPoint.x;
				cPointers3[cpIndex+1] = offsetPoint.y;
				cPointers3[cpIndex+2] = offsetPoint.z;
				CFemoralPart::FindCorrespondentIndexOnSideSurface(ui, vi, ucCount, vcCount, 
											ucCountLeft, vcCountLeft, ucCountRight, vcCountRight, 
											bLeftRight, uIndexOut, vIndexOut, uIndexOutNext, vIndexOutNext, bCorner);
				if ( bLeftRight == 0 )
				{
					// Find corresponding control point on leftSurf3 and move it
					CFemoralPart::MoveBoundaryControlPoint(cPoint, offsetVector, uIndexOut, vIndexOut, ucCountLeft3, vcCountLeft3, cPointersLeft3);
				}
				else if ( bLeftRight == 1 )
				{
					// Find corresponding control point on rightSurf3 and move it
					CFemoralPart::MoveBoundaryControlPoint(cPoint, offsetVector, uIndexOut, vIndexOut, ucCountRight3, vcCountRight3, cPointersRight3);
				}
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////
	// Modify along the 2 v boundaries
	for (ULONG ui=1; ui<(ucCount-1); ui++)// every one except first one and last one
	{
		uParam = uMin + ui*uDelta;
		for (ULONG vi=0; vi<vcCount; vi+=(vcCount-1)) // first one and last one
		{
			vParam = vMin + vi*vDelta;
			guessParam = IwPoint2d(uParam, vParam);
			cpIndex = 4*(vi+ui*vcCount);
			cPoint = IwPoint3d(cPointers[cpIndex], cPointers[cpIndex+1], cPointers[cpIndex+2]);
			dist = DistFromPointToSurface(cPoint, m_femurMainSurfRef, guessParam, closestPoint, param2d);
			m_femurMainSurfRef->EvaluateNormal(param2d, FALSE, FALSE, normal);
			if (1)// offset with cartiThickness distance
			{
				offsetVector = cartiThickness*normal;
				offsetPoint = cPoint + offsetVector;
				// move control point on mainSurf3
				cPointers3[cpIndex] = offsetPoint.x;
				cPointers3[cpIndex+1] = offsetPoint.y;
				cPointers3[cpIndex+2] = offsetPoint.z;
				CFemoralPart::FindCorrespondentIndexOnSideSurface(ui, vi, ucCount, vcCount, 
											ucCountLeft, vcCountLeft, ucCountRight, vcCountRight, 
											bLeftRight, uIndexOut, vIndexOut, uIndexOutNext, vIndexOutNext, bCorner);
				if ( bLeftRight == 0 )
				{
					// Find corresponding control point on leftSurf3 and move it
					CFemoralPart::MoveBoundaryControlPoint(cPoint, offsetVector, uIndexOut, vIndexOut, ucCountLeft3, vcCountLeft3, cPointersLeft3);
				}
				else if ( bLeftRight == 1 )
				{
					// Find corresponding control point on rightSurf3 and move it
					CFemoralPart::MoveBoundaryControlPoint(cPoint, offsetVector, uIndexOut, vIndexOut, ucCountRight3, vcCountRight3, cPointersRight3);
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// Modify the non-boundary control points on mainSurf3
	for (ULONG ui=1; ui<(ucCount-1); ui++)// skip the first one and the last one
	{
		uParam = uMin + ui*uDelta;
		for (ULONG vi=1; vi<(vcCount-1); vi++)// skip the first one and the last one
		{
			vParam = vMin + vi*vDelta;
			guessParam = IwPoint2d(uParam, vParam);
			cpIndex = 4*(vi+ui*vcCount);
			cPoint = IwPoint3d(cPointers[cpIndex], cPointers[cpIndex+1], cPointers[cpIndex+2]);
			// Determine the cPoint corresponding parameter on m_femurMainSurfRef and normal vector
			dist = DistFromPointToSurface(cPoint, m_femurMainSurfRef, guessParam, closestPoint, param2d);
			m_femurMainSurfRef->EvaluateNormal(param2d, FALSE, FALSE, normal);
			if (1) // offset with cartiThickness
			{
				// distance to trochlear groove UV curve
				param3d = IwPoint3d(param2d.x, param2d.y, 0); 
				double distToTroGroCurve = DistFromPointToCurve(param3d, troGroUVCurve, closestPoint, param, NULL);
				effectCartiThickness = cartiThickness;
				// if close to the troGroUVCurve enough
				if ( distToTroGroCurve < 1.75*troGroTransWidth ) 
				{
					distToTroGro = DistFromPointToSurface(cPoint, troGroCartiSurface, closestPoint, param2d);
					troGroCartiSurface->EvaluateNormal(param2d, FALSE, FALSE, troGroNormal);
					double factor1, factor2;
					// if the closestPoint is on the trochlear groove surface boundary,
					// the cPoint could be outside of the troGroCartiSurface
					if ( param2d.x < troGroCartiDom.Evaluate(0.001,0.001).x ||
							param2d.y < troGroCartiDom.Evaluate(0.001,0.001).y ||
							param2d.x > troGroCartiDom.Evaluate(0.999,0.999).x ||
							param2d.y > troGroCartiDom.Evaluate(0.999,0.999).y ) 
					{
						offsetVector = effectCartiThickness*normal;
if (0) 
ShowPoint(m_pDoc, closestPoint, black);
					}
					// if the closestPoint is close to the trochlear groove surface boundary within 0.25*troGroTransWidth
					else if ( param2d.x < (troGroCartiDom.GetMin().x+0.25*troGroTransWidth) )
					{
						factor1 = ((troGroCartiDom.GetMin().x+0.25*troGroTransWidth) - param2d.x)/(0.25*troGroTransWidth);
						factor2 = 1.0 - factor1;
						offsetVector = factor1*effectCartiThickness*normal+factor2*(effectCartiThickness+distToTroGro)*troGroNormal;
if (0) 
ShowPoint(m_pDoc, closestPoint, red);
					}
					// if the closestPoint is close to the trochlear groove surface boundary within 0.25*troGroTransWidth
					else if ( param2d.x > (troGroCartiDom.GetMax().x-0.25*troGroTransWidth) )
					{
						factor1 = (param2d.x - (troGroCartiDom.GetMax().x-0.25*troGroTransWidth))/(0.25*troGroTransWidth);
						factor2 = 1.0 - factor1;
						offsetVector = factor1*effectCartiThickness*normal+factor2*(effectCartiThickness+distToTroGro)*troGroNormal;
if (0) 
ShowPoint(m_pDoc, closestPoint, brown);
					}
					// if the closestPoint is close to the trochlear groove surface boundary within 0.5*troGroTransWidth
					else if ( param2d.y < (troGroCartiDom.GetMin().y+0.5*troGroTransWidth) )
					{
						factor1 = ((troGroCartiDom.GetMin().y+0.5*troGroTransWidth) - param2d.y)/(0.5*troGroTransWidth);
						factor2 = 1.0 - factor1;
						offsetVector = factor1*effectCartiThickness*normal+factor2*(effectCartiThickness+distToTroGro)*troGroNormal;
if (0) 
ShowPoint(m_pDoc, closestPoint, green);
					}
					// if the closestPoint is close to the trochlear groove surface boundary within 0.5*troGroTransWidth
					else if ( param2d.y > (troGroCartiDom.GetMax().y-0.5*troGroTransWidth) )
					{
						factor1 = (param2d.y - (troGroCartiDom.GetMax().y-0.5*troGroTransWidth))/(0.5*troGroTransWidth);
						factor2 = 1.0 - factor1;
						offsetVector = factor1*effectCartiThickness*normal+factor2*(effectCartiThickness+distToTroGro)*troGroNormal;
if (0) 
ShowPoint(m_pDoc, closestPoint, darkGreen);
					}
					// if the closestPoint is fully inside the trochlear groove surface boundary 
					else
					{
						offsetVector = (effectCartiThickness+distToTroGro)*troGroNormal;
if (0) 
ShowPoint(m_pDoc, closestPoint, white);
					}
				}
				else
				{
					offsetVector = effectCartiThickness*normal;
if (0) 
ShowPoint(m_pDoc, cPoint, cyan);
				}

				offsetPoint = cPoint + offsetVector;
if (0)
ShowPoint(m_pDoc, offsetPoint, green);

				// move control point on mainSurf3
				cPointers3[cpIndex] = offsetPoint.x;
				cPointers3[cpIndex+1] = offsetPoint.y;
				cPointers3[cpIndex+2] = offsetPoint.z;
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// Modify the non-boundary control points on leftSurf3
	double uMinLeft = uKnotsLeft[3];
	double uMaxLeft = uKnotsLeft[ukCountLeft-1];
	double uDeltaLeft = (uMaxLeft - uMinLeft)/(ucCountLeft-1);// only (ucCountLeft-1) spans
	double vMinLeft = vKnotsLeft[3];
	double vMaxLeft = vKnotsLeft[vkCountLeft-1];
	double vDeltaLeft = (vMaxLeft - vMinLeft)/(vcCountLeft-1);// only (vcCountLeft-1) spans
	for (ULONG ui=0; ui<(ucCountLeft-1); ui++)// skip the first one and the last one
	{
		uParam = uMinLeft + ui*uDeltaLeft;
		for (ULONG vi=0; vi<(vcCountLeft-1); vi++)// skip the last one
		{
			vParam = vMinLeft + vi*vDeltaLeft;
			guessParam = IwPoint2d(uParam, vParam);
			cpIndex = 4*(vi+ui*vcCountLeft);
			cPoint = IwPoint3d(cPointersLeft[cpIndex], cPointersLeft[cpIndex+1], cPointersLeft[cpIndex+2]);
			dist = DistFromPointToSurface(cPoint, m_femurLeftSurfRef, guessParam, closestPoint, param2d);
			m_femurLeftSurfRef->EvaluateNormal(param2d, FALSE, FALSE, normal);
			if (1)
			{
				effectCartiThickness = cartiThickness;//*(1.0 - (distToEdge/transWidth));
				offsetVector = effectCartiThickness*normal;
				offsetPoint = cPoint + offsetVector;
if (0)
ShowPoint(m_pDoc, offsetPoint, darkGreen);
				// move control point on mainSurf3
				cPointersLeft3[cpIndex] = offsetPoint.x;
				cPointersLeft3[cpIndex+1] = offsetPoint.y;
				cPointersLeft3[cpIndex+2] = offsetPoint.z;
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////
	// Modify the non-boundary control points on rightSurf3
	double uMinRight = uKnotsRight[3];
	double uMaxRight = uKnotsRight[ukCountRight-1];
	double uDeltaRight = (uMaxRight - uMinRight)/(ucCountRight-1);// only (ucCountRight-1) spans
	double vMinRight = vKnotsRight[3];
	double vMaxRight = vKnotsRight[vkCountRight-1];
	double vDeltaRight = (vMaxRight - vMinRight)/(vcCountRight-1);// only (vcCountRight-1) spans
	for (ULONG ui=0; ui<(ucCountRight-1); ui++)// skip the first one and the last one
	{
		uParam = uMinRight + ui*uDeltaRight;
		for (ULONG vi=0; vi<(vcCountRight-1); vi++)// skip the last one
		{
			vParam = vMinRight + vi*vDeltaRight;
			guessParam = IwPoint2d(uParam, vParam);
			cpIndex = 4*(vi+ui*vcCountRight);
			cPoint = IwPoint3d(cPointersRight[cpIndex], cPointersRight[cpIndex+1], cPointersRight[cpIndex+2]);
			dist = DistFromPointToSurface(cPoint, m_femurRightSurfRef, guessParam, closestPoint, param2d);
			m_femurRightSurfRef->EvaluateNormal(param2d, FALSE, FALSE, normal);
			if (1)
			{
				effectCartiThickness = cartiThickness;//*(1.0 - (distToEdge/transWidth));
				offsetVector = effectCartiThickness*normal;
				offsetPoint = cPoint + offsetVector;
if (0)
ShowPoint(m_pDoc, offsetPoint, darkGreen);
				// move control point on mainSurf3
				cPointersRight3[cpIndex] = offsetPoint.x;
				cPointersRight3[cpIndex+1] = offsetPoint.y;
				cPointersRight3[cpIndex+2] = offsetPoint.z;
			}
		}
	}

#ifdef SM_VERSION_STRING // starting with SMLib 8.6.17, we define this macro
	mainSurf3->Notify(IW_NO_POST_EDIT, mainSurf3, nullptr, nullptr);
	leftSurf3->Notify(IW_NO_POST_EDIT, leftSurf3, nullptr, nullptr);
	rightSurf3->Notify(IW_NO_POST_EDIT, rightSurf3, nullptr, nullptr);
#else
	mainSurf3->Notify(IW_NO_POST_EDIT);
	leftSurf3->Notify(IW_NO_POST_EDIT);
	rightSurf3->Notify(IW_NO_POST_EDIT);
#endif

	// FemoralPart
	CFemoralPart::SmoothMainSurfaceAtCrisps(mainSurf3);

	// create a Brep by mainSurf3, leftSurf3, rightSurf3
	IwBrep* cartiBrep = new (m_pDoc->GetIwContext()) IwBrep(); 
	IwFace *face;
	cartiBrep->CreateFaceFromSurface(mainSurf3, mainSurf3->GetNaturalUVDomain(), face);
	cartiBrep->CreateFaceFromSurface(leftSurf3, leftSurf3->GetNaturalUVDomain(), face);
	cartiBrep->CreateFaceFromSurface(rightSurf3, rightSurf3->GetNaturalUVDomain(), face);

	// update Brep
	IwBrep* prevBrep = pCartilageSurface->GetIwBrep();
	if (prevBrep) 
	{
		IwObjDelete(prevBrep);
	}

	pCartilageSurface->SetIwBrep(cartiBrep);
	pCartilageSurface->MakeTess();
	pCartilageSurface->SetModifiedFlag(true);

	// Set time stamp
	QDateTime cartiTime = QDateTime::currentDateTime();
	uint cartiTimeStamp = cartiTime.toTime_t();
	m_pDoc->GetAdvancedControlJigs()->SetCartilageTimeStamp(cartiTimeStamp);

	if ( troGroCartiSurface )
		IwObjDelete(troGroCartiSurface);

	QApplication::restoreOverrideCursor();

	return;
}

bool CDefineCartilage::MouseLeft( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp )
{
	bool continuousResponse = true;
    m_posStart = cursor;
	m_leftButtonDown = true;

	m_smoothRegionLoop.RemoveAll();
	m_smoothRegionLoop3d.RemoveAll();

	IwPoint3d caughtPoint;
	int caughtIndex;
	bool gotCaughtPoint = GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);
	{
		continuousResponse = false;
	}

	if (m_manActType == MAN_ACT_TYPE_REVIEW) 
	{
		m_leftButtonDown = false;// no response to MouseUp if under review
	}

	ReDraw();

	return continuousResponse;
}

bool CDefineCartilage::MouseUp( const QPoint& cursor, Viewport* vp )
{
	if (m_manActType == MAN_ACT_TYPE_REVIEW) // Remember if under review, m_leftButtonDown is always false.
	{
		// if the clicked-down is the same as the button-up point, change to the next review predefined view
		if (m_posStart == cursor)
			OnReviewPredefinedView();
	}
	else if ( m_leftButtonDown ) 
	{
		IwPoint3d caughtPoint;
		int caughtIndex;
		bool gotCaughtPoint = GetMouseMoveCaughtPoint(caughtPoint, caughtIndex);

		//// Update the mouse move caught point /////////////////
		if ( m_addDeleteMode == 0 && gotCaughtPoint)
		{
			m_pDoc->AppendLog( QString("CDefineCartilage::MouseUp(), m_addDeleteMode = move, caughtIndex: %1, caughtPoint: %2 %3 %4").arg(caughtIndex).arg(caughtPoint.x).arg(caughtPoint.y).arg(caughtPoint.z) );

			// if the clicked-down is the same as the button-up point, do nothing
			if (m_posStart == cursor)
			{
				m_leftButtonDown = false;
				return false;
			}

			CDefineCartilageUndoData prevUndoData, postUndoData;
			// Get the undo data before any changes
			GetDefineCartilageUndoData(prevUndoData);
			// Update the change
			if (m_femurMainSurfRef)
			{
				IwVector3d viewVec = m_pView->GetViewingVector();
				IwPoint3d backWardPnt = caughtPoint - 200*viewVec; // move the caught point closer to the viewer, in case no intersection with the sketch surface
				IwPoint3d interPnt;
				double uvPnt[2];
				bool gotIntersectPnt = IntersectSurfaceByLine((IwSurface*)m_femurMainSurfRef, backWardPnt, viewVec, interPnt, uvPnt);
				if (gotIntersectPnt)
				{
					// update the UVPnt
					IwPoint3d interUVPnt = IwPoint3d(uvPnt[0], uvPnt[1], 0);
					m_currTroGroovePoints.SetAt(caughtIndex, interUVPnt);

					// Get the undo data after changes
					GetDefineCartilageUndoData(postUndoData);

					QUndoCommand *defineCartilageCmd = new DefineCartilageCmd( this, prevUndoData, postUndoData);
					undoStack->push( defineCartilageCmd );
				}
			}
		}
		//// Update the smooth points /////////////////
		else if (m_addDeleteMode == 3)
		{
			m_pDoc->AppendLog( QString("CDefineCartilage::MouseUp(), m_addDeleteMode = smooth"));

			QApplication::restoreOverrideCursor();
			m_addDeleteMode = 0; // none
			EnableAction( m_actSmooth, true );

			if ( m_smoothRegionLoop.GetSize() > 2 )// at least 3 points to define a region
			{
				CCartilageSurface* pCartilageSurface = m_pDoc->GetCartilageSurface();
				IwBrep* cartilageBrep = pCartilageSurface->GetIwBrep();
				IwTArray<IwFace*> cFaces;
				cartilageBrep->GetFaces(cFaces);
				if ( cFaces.GetSize() != 3 )
					return true;
				IwTArray<ULONG> ctrlPntIndexes;
				IwTArray<IwPoint3d> prevCtrlPnts, postCtrlPnts;
				IwBSplineSurface* mainSurf = (IwBSplineSurface*)cFaces.GetAt(0)->GetSurface();
				SmoothARegion(m_pDoc, m_smoothRegionLoop, m_pView->GetViewingVector(), mainSurf, ctrlPntIndexes, prevCtrlPnts, postCtrlPnts);
				if ( postCtrlPnts.GetSize() > 0 ) // if got any modified ctrl points
				{
					CDefineCartilageUndoData prevUndoData, postUndoData;
					// Get the undo data before any changes
					GetDefineCartilageUndoData(prevUndoData, &ctrlPntIndexes, &prevCtrlPnts);

					// update the change
					// No curr data for smooth

					// Get the undo data after changes
					GetDefineCartilageUndoData(postUndoData, &ctrlPntIndexes, &postCtrlPnts);
					QUndoCommand *defineOutlineProfileCmd = new DefineCartilageCmd( this, prevUndoData, postUndoData);
					undoStack->push( defineOutlineProfileCmd );

				}
			}
			m_smoothRegionLoop.RemoveAll();
			m_smoothRegionLoop3d.RemoveAll();
		}
	}

	m_leftButtonDown = false;

	ReDraw();

	return true;
}

bool CDefineCartilage::MouseMove( const QPoint& cursor, Qt::KeyboardModifiers keyModifier, Viewport* vp)
{
	bool continuousResponse = true;
	if (m_leftButtonDown)
	{
		if ( m_addDeleteMode == 0 ) // Grab and move the control point
		{
			MoveCaughtPoint(cursor);
			continuousResponse = false;
		}
		else if ( m_addDeleteMode == 3 ) // select a region to smooth
		{
			m_smoothRegionLoop.AddUnique(cursor);// collect all cursor locations in screen space
			CPoint3d cpt;
			m_pView->UVtoXYZ(cursor, cpt);
			m_smoothRegionLoop3d.Add(cpt);
			continuousResponse = false;
		}
        m_posLast = cursor;
	}
	else
	{
		// call the parent's MouseMove();
		CManager::MouseMove(cursor, keyModifier);
	}

	ReDraw();

	return continuousResponse;
}

void CDefineCartilage::GetDefineCartilageUndoData
(
	CDefineCartilageUndoData& undoData,		// O:
	IwTArray<ULONG>* ctrlPntIndexes,		// I: 
	IwTArray<IwPoint3d>* ctrlPnts			// I:
)
{
	undoData.troGroovePoints.RemoveAll();
	undoData.troGroovePoints.Append(m_currTroGroovePoints);

	undoData.ctrlPntIndexes.RemoveAll();
	undoData.ctrlPnts.RemoveAll();
	if ( ctrlPnts && ctrlPntIndexes && ctrlPntIndexes->GetSize()>0 )
	{
		for (unsigned i=0; i<ctrlPntIndexes->GetSize(); i++)
		{
			undoData.ctrlPntIndexes.Add(ctrlPntIndexes->GetAt(i));
			undoData.ctrlPnts.Add(ctrlPnts->GetAt(i));
		}
	}
}


void CDefineCartilage::SetDefineCartilageUndoData
(
	CDefineCartilageUndoData& undoData	// O:
)
{
	m_currTroGroovePoints.RemoveAll();
	m_currTroGroovePoints.Append(undoData.troGroovePoints);

	// Also set the m_currEdgePoints to the SetMouseMoveSearchingList() for mouse searching;
	// convert feature points from UV domain to xyz space
	CCartilageSurface*		cartilageSurface = m_pDoc->GetCartilageSurface();
	if (cartilageSurface)
	{
		IwTArray<IwPoint3d> totalPoints;
		cartilageSurface->SetTrochlearGroovePoints(m_currTroGroovePoints);
		cartilageSurface->ConvertUVToXYZ(m_currTroGroovePoints, m_currTroGrooveXYZPoints, NULL);
		totalPoints.Append(m_currTroGrooveXYZPoints);

		SetMouseMoveSearchingList(totalPoints);
	}

	// Use the control points stored in undoData.ctrlPntIndexes and undoData.ctrlPnts
	// to update the main surface in cartilageSurface
	if ( undoData.ctrlPntIndexes.GetSize() > 0 )
	{
		IwBSplineSurface* mainSurf = cartilageSurface->GetSinglePatchSurface();
		// Very time consuming, need to let users know still in processing.
		QApplication::setOverrideCursor( Qt::WaitCursor );
		CCartilageSurface::UpdateSurfacebyCtrlPoints(m_pDoc, mainSurf, undoData.ctrlPntIndexes, undoData.ctrlPnts);
		cartilageSurface->MakeTess();
		cartilageSurface->SetModifiedFlag(true);
		QApplication::restoreOverrideCursor();
		// if the only change is to smooth the main surface area,
		// then UpdateMainSurfacebyCtrlPoints() has done the update.
		if ( m_cartilageSurfaceUpToDate )
			SetCartilageSurfaceUpToDate(true);
		else
			SetCartilageSurfaceUpToDate(false);
		// However, time stamp needs to be updated.
		QDateTime cartiTime = QDateTime::currentDateTime();
		uint cartiTimeStamp = cartiTime.toTime_t();
		m_pDoc->GetAdvancedControlJigs()->SetCartilageTimeStamp(cartiTimeStamp);
	}
	else
		SetCartilageSurfaceUpToDate(false);

	// Whenever parameters are changed, the validation status is invalid.
	m_validateStatus = VALIDATE_STATUS_NOT_VALIDATE;
	m_validateMessage = QString("Not validate.");
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_CARTILAGE_SURFACE, m_validateStatus, m_validateMessage);

	m_pView->Redraw();
}

void CDefineCartilage::SetCartilageSurfaceUpToDate( bool flag )
{
	m_cartilageSurfaceUpToDate = flag;

	if ( m_cartilageSurfaceUpToDate )
	{
		EnableAction( m_actUpdate, false );
	}
	else
	{
		EnableAction( m_actUpdate, true );
	}

}

///////////////////////////////////////////////////////////////////
// This function uses the troGroUVCurve to offset along both sides,
// then sweep the offset curves as the trochlear groove surface.
// The distance between the surface and troGroUVCurve is checked to 
// ensure the native trochlear groove is smoothed out enough.
///////////////////////////////////////////////////////////////////
IwBSplineSurface* CDefineCartilage::CreateTrochlearGrooveSurface
(
	IwBSplineCurve*& troGroUVCurve	// I: the trochlear groove uv curve 
)
{
	//IwTArray<IwCurve*> arrOfCurves;
	//arrOfCurves.Add(troGroUVCurve);
	//WriteCurves(arrOfCurves, "C:\\PS\\TroGroCrv\\TG.crv");

	CAdvancedControlJigs* advCtrl = m_pDoc->GetAdvancedControlJigs();
	double troGroAddCartiThickness = advCtrl->GetTrochlearGrooveAdditionalCartilageThickness();
	double troGroTransWidth = advCtrl->GetTrochlearGrooveCartilageTransitionWidth();

	// offset curve along surface on left/right/normal directions
	IwBSplineCurve *troGroXYZCurvePlus, *troGroXYZCurvePlusPlus;
	IwBSplineCurve *troGroXYZCurveMinus, *troGroXYZCurveMinusMinus;

	double posEndOffsetFactorPP, negEndOffsetFactorMM;
	double posEndOffsetFactorP, negEndOffsetFactorM;
	double posEndExtension, negEndExtension;
	if (m_pDoc->IsPositiveSideLateral())
	{
		posEndOffsetFactorPP = 1.5;// lateral
		posEndOffsetFactorP = 1.35;// lateral
		negEndOffsetFactorMM = -1.65;
		negEndOffsetFactorM = -1.5;
		posEndExtension = 3.5; // lateral end extension 2.5mm
		negEndExtension = 0.0; // medial 0
	}
	else
	{
		posEndOffsetFactorPP = 1.65;
		posEndOffsetFactorP = 1.5;
		negEndOffsetFactorMM = -1.5;// lateral
		negEndOffsetFactorM = -1.35;// lateral
		posEndExtension = 0.0; // medial 0
		negEndExtension = 3.5; // lateral end extension 2.5mm
	}

	troGroXYZCurvePlusPlus = OffsetCurveAlongSurfaceAndNormal(m_pDoc->GetIwContext(), m_femurMainSurfRef, troGroUVCurve, 1.15*troGroTransWidth, posEndOffsetFactorPP*troGroTransWidth, posEndExtension, 0.0, 10); 
	troGroXYZCurvePlus = OffsetCurveAlongSurfaceAndNormal(m_pDoc->GetIwContext(), m_femurMainSurfRef, troGroUVCurve, troGroTransWidth, posEndOffsetFactorP*troGroTransWidth, posEndExtension, 0.0, 10); 
	troGroXYZCurveMinus = OffsetCurveAlongSurfaceAndNormal(m_pDoc->GetIwContext(), m_femurMainSurfRef, troGroUVCurve, -troGroTransWidth, negEndOffsetFactorM*troGroTransWidth, negEndExtension, 0.0, 10); 
	troGroXYZCurveMinusMinus = OffsetCurveAlongSurfaceAndNormal(m_pDoc->GetIwContext(), m_femurMainSurfRef, troGroUVCurve, -1.15*troGroTransWidth, negEndOffsetFactorMM*troGroTransWidth, negEndExtension, 0.0, 10); 
if (0)//psv
{
ShowCurve(m_pDoc, troGroUVCurve, white);

ShowCurve(m_pDoc, troGroXYZCurvePlusPlus, red);
ShowCurve(m_pDoc, troGroXYZCurvePlus, brown);
ShowCurve(m_pDoc, troGroXYZCurveMinus, cyan);
ShowCurve(m_pDoc, troGroXYZCurveMinusMinus, blue);
}
	// create skin surface by these 4 curves
	IwTArray<IwBSplineCurve*> skinCurves;
	skinCurves.Add(troGroXYZCurveMinusMinus);
	skinCurves.Add(troGroXYZCurveMinus);
	skinCurves.Add(troGroXYZCurvePlus);
	skinCurves.Add(troGroXYZCurvePlusPlus);
	IwBSplineSurface* troGroCartiSurface = NULL;
	IwBSplineSurface::CreateSkinnedSurface(m_pDoc->GetIwContext(), skinCurves, FALSE, IW_SP_U, 0.05, NULL, NULL, FALSE, NULL, NULL, troGroCartiSurface);
	troGroCartiSurface->ReparametrizeWithArcLength();
if (0)//PSV
{
IwBrep* troBrep = new (m_pDoc->GetIwContext()) IwBrep(); 
IwFace* troFace;
troBrep->CreateFaceFromSurface(troGroCartiSurface, troGroCartiSurface->GetNaturalUVDomain(), troFace);
ShowBrep(m_pDoc, troBrep, "troBrep", blue);
}

	// Check the distance to m_femurMainSurfRef. Ensure they are no less than troGroAddCartiThickness
	// But also ensure the curve is smooth.
	// Get center curve
	IwBSplineCurve *centralCurve=NULL, *smoothCurve=NULL;
	troGroCartiSurface->CreateIsoParametricCurve(m_pDoc->GetIwContext(), IW_SP_U, troGroCartiSurface->GetNaturalUVDomain().Evaluate(0.5,0).x, 0.01, centralCurve, NULL);
	bool bAllGood = true;
	double dist, param;
	double modifiedTroGroAddCartiThickness;
	IwPoint3d cPnt, pnt;
	IwVector3d normal, vec;
	IwPoint2d param2d;
	IwTArray<IwPoint3d> samplingPnts, centralPoints;
	int samplingNo = 25;
	int iters = 4;
	IwExtent1d crvDom;
	for (int iter=0; iter<iters; iter++)
	{
		samplingPnts.RemoveAll();
		centralPoints.RemoveAll();
		// sampling points
		crvDom = centralCurve->GetNaturalInterval();
		centralCurve->EquallySpacedPoints(crvDom.GetMin(), crvDom.GetMax(), samplingNo, 0.01, &samplingPnts, NULL);
		// approximation curve
		IwBSplineCurve::CreateApproximatingCurve(m_pDoc->GetIwContext(), 8, IW_IT_CHORDLENGTH, 3, 3, samplingPnts, NULL, NULL, smoothCurve);
		for ( unsigned i=0; i<samplingPnts.GetSize(); i++)
		{
			// First ensure they are no less than troGroAddCartiThickness
			dist = DistFromPointToSurface(samplingPnts.GetAt(i), m_femurMainSurfRef, cPnt, param2d);
			// We want the anterior portion of extra thickness to be bigger.
			modifiedTroGroAddCartiThickness = troGroAddCartiThickness + 0.5*(1.0 - (double)i/(double)samplingNo);
			if ( dist < modifiedTroGroAddCartiThickness )
			{
				bAllGood = false;
				DistFromPointToSurface(samplingPnts.GetAt(i), troGroCartiSurface, cPnt, param2d);// determine parameter of samplingPnts on troGroCartiSurface
				troGroCartiSurface->EvaluateNormal(param2d, FALSE, FALSE, normal);
				pnt = samplingPnts.GetAt(i) + (modifiedTroGroAddCartiThickness-dist)*normal;// pnt will have enough distance to the m_femurMainSurfRef
			}
			else
			{
				pnt = samplingPnts.GetAt(i);
			}
			// Second ensure thet are no less than smoothCurve
			dist = DistFromPointToCurve(pnt, smoothCurve, cPnt, param);
			vec = pnt - cPnt;
			if ( vec.Dot(normal) < 0 ) // below smoothCurve
			{
				pnt = pnt - vec.Dot(normal)*normal;
				bAllGood = false;
			}
			else // vec.Dot(normal) > 0 -> above smoothCurve
			{
				// Special handle for first points
				if (i==1) // if the second point is above the smoothCurve
				{
					IwPoint3d pnt0 = centralPoints.GetAt(0);
					pnt0 = pnt0 + vec.Dot(normal)*normal; // lift up fisrt point the same distance as second point
					centralPoints.SetAt(0, pnt0);
					bAllGood = false;
				}
			}

			centralPoints.Add(pnt);
		}
		if (centralCurve)
			IwObjDelete(centralCurve);
		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), centralPoints, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, centralCurve);
	}

	if ( 1 )
	{
		if ( troGroCartiSurface )
			IwObjDelete(troGroCartiSurface);
		troGroCartiSurface = NULL;
		if ( centralCurve )
			IwObjDelete (centralCurve );

		IwBSplineCurve::InterpolatePoints(m_pDoc->GetIwContext(), centralPoints, NULL, 3, NULL, NULL, FALSE, IW_IT_CHORDLENGTH, centralCurve);
		// create skin surface by these curves to relax the trochlear groove surface a little bit.
		skinCurves.RemoveAll();
		skinCurves.Add(troGroXYZCurveMinusMinus);
		skinCurves.Add(troGroXYZCurveMinus);
		skinCurves.Add(centralCurve);
		skinCurves.Add(troGroXYZCurvePlus);
		skinCurves.Add(troGroXYZCurvePlusPlus);
		IwBSplineSurface::CreateSkinnedSurface(m_pDoc->GetIwContext(), skinCurves, FALSE, IW_SP_U, 0.05, NULL, NULL, FALSE, NULL, NULL, troGroCartiSurface);
		troGroCartiSurface->ReparametrizeWithArcLength();
if (0)//PSV
{
IwBrep* troBrep = new (m_pDoc->GetIwContext()) IwBrep(); 
IwFace* troFace;
troBrep->CreateFaceFromSurface(troGroCartiSurface, troGroCartiSurface->GetNaturalUVDomain(), troFace);
ShowBrep(m_pDoc, troBrep, "troBrepSmooth", cyan);
}	


	}


	if ( centralCurve )
		IwObjDelete (centralCurve );

	return troGroCartiSurface;
}

////////////////////////////////////////////////////////////////
// This function smoothes a selected region on the main surface.
////////////////////////////////////////////////////////////////
void CDefineCartilage::SmoothARegion
(
	CTotalDoc*& pDoc,					// I: TriathlonF1Doc
	IwTArray<QPoint>& screenPoints,		// I: they are the cursor (x,y) points on screen coordinates 
	IwVector3d& viewingVector,			// I: toward the screen
	IwBSplineSurface*& mainSurf,		// I:
	IwTArray<ULONG>& ctrlPntIndexes,	// O: the index of the control point to be moved
	IwTArray<IwPoint3d>& prevCtrlPnts,	// O: the control point locations before moved (for redo/undo purpose)
	IwTArray<IwPoint3d>& postCtrlPnts	// O: the control point locations to be moved
)
{
	// initialize data
	ctrlPntIndexes.RemoveAll();
	prevCtrlPnts.RemoveAll();
	postCtrlPnts.RemoveAll();

	IwContext& iwContect = pDoc->GetIwContext();
	// create a polygon defined by screenPoints
	IwTArray<IwPoint3d> screenPoints3d;
	for (unsigned i=0; i<screenPoints.GetSize(); i++)
	{
		screenPoints3d.Add(IwPoint3d(screenPoints.GetAt(i).x(), screenPoints.GetAt(i).y(), 0));
	}
	IwPolyBrep * screenPolyBrep = new (iwContect) IwPolyBrep(0.0001);
    IwPolyShell *pNewShell = NULL; 
	IwPolyFace* screenPolyFace = NULL;
	screenPolyBrep->CreatePoly(pNewShell, screenPoints3d, screenPolyFace);
	//screen Bounding Box
	IwExtent3d screenBoundingBox;
	screenPolyFace->CalculateBoundingBox(screenBoundingBox);
	//Project the screenPoints onto the mainSurf, then 
	// calculate the 3D Bounding Box of the projected points
	IwExtent3d boundingBox3D;
	int ppNo = (int)ceil(screenPoints.GetSize()/20.0);
	int uv[2];
	IwPoint3d cpt;
	IwPoint3d intStartPnt, pnt;
	for ( unsigned i=0; i<screenPoints.GetSize(); i+=ppNo )// therefore, we only sample at most 20 points
	{
		pDoc->GetView()->UVtoXYZ(screenPoints.GetAt(i), cpt);
		intStartPnt = cpt - 100*viewingVector;// move closer to the viewers
		IntersectSurfaceByLine(mainSurf, intStartPnt, viewingVector, cpt, NULL);
		boundingBox3D.AddPoint3d(cpt);
	}
	// expand a little bit
	boundingBox3D = boundingBox3D.ExpandRelative(0.1);
	
	//////////////////////////////////////////////////////////////////////////////
	// cycle through all control points and get the control points on the boundary 
	// Get the knot info 
	ULONG ukCount, vkCount;
	double *uKnots, *vKnots;
	mainSurf->GetKnotsPointers(ukCount, vkCount, uKnots, vKnots);
	// Get control points
	ULONG ucCount, vcCount;
	double* cPointers;
	mainSurf->GetControlPointsPointer(ucCount, vcCount, cPointers);
	ULONG cpIndex;
	IwPoint3d cPoint;
	IwBoolean bInside, bPreInside = FALSE;
	IwTArray<ULONG> startArray, endArray;// When we scan along each iso-u control points, the startArray and endArray will keep all the control point indexes at the entering and leaving of the selelcted region
	for (ULONG ui=1; ui<(ucCount-1); ui++)// skip the first one and last one
	{
		bPreInside = FALSE;
		for (ULONG vi=2; vi<(vcCount-2); vi++)// skip the first two and last two. We do not want to modify the ctrl point on boundary.
		{
			cpIndex = 4*(vi+ui*vcCount);
			cPoint = IwPoint3d(cPointers[cpIndex], cPointers[cpIndex+1], cPointers[cpIndex+2]);
			if ( boundingBox3D.ContainsPoint3d(cPoint) == FALSE )
				continue;
			cpt = (IwPoint3d)cPoint;
			pDoc->GetView()->XYZtoUV(cpt, uv);
			pnt = IwPoint3d(uv[0], uv[1], 0);
			if ( screenBoundingBox.ContainsPoint3d(pnt, 1.0) == FALSE )
				continue;
			screenPolyFace->PointInPolygon(pnt, bInside, FALSE);

			// entering the selected region
			if ( !bPreInside && bInside )
			{
				startArray.Add(cpIndex-4);
				endArray.Add(cpIndex+4);// Immediately set the next one in the endArray
			}
			// Still in the region
			else if ( bPreInside && bInside )
			{
				endArray.SetAt(endArray.GetSize()-1, cpIndex+4);// replace the last one in endArray
			}

			bPreInside = bInside;
		}
	}

	if ( screenPolyBrep )
		IwObjDelete(screenPolyBrep);

	if ( startArray.GetSize() == 0 )
		return;
	if ( startArray.GetSize() != endArray.GetSize() )// Both should have the same size.
		return;

	///////////////////////////////////////////////////////////////////
	// Smooth the selected region. The approach is as follows:
	// 1. A line is defined by each pair of startArray and endArray. 
	// 2. The line is divided into equally spaced based on how many control points in between.
	// 3. Project each equally spaced point from outside toward the femur.
	// 4. The intersection point becomes the new control point location.
	IwPoint3d startPoint, endPoint, oldCtrlPoint, newCtrlPoint;
	IwVector3d deltaVec;
	ULONG cpIndexStart, cpIndexEnd, cpIndexNew;
	int cpNo;
	for (unsigned i=0; i<startArray.GetSize(); i++)
	{
		cpIndexStart = startArray.GetAt(i);
		startPoint = IwPoint3d(cPointers[cpIndexStart], cPointers[cpIndexStart+1], cPointers[cpIndexStart+2]);
		cpIndexEnd = endArray.GetAt(i);
		endPoint = IwPoint3d(cPointers[cpIndexEnd], cPointers[cpIndexEnd+1], cPointers[cpIndexEnd+2]);
		cpNo = (int)((cpIndexEnd - cpIndexStart)/4) + 1;// how may control points in between
		deltaVec = (endPoint - startPoint)/(cpNo-1);// only have (cpNo-1) spans
		for (int j=0; j<cpNo; j++)
		{
			pnt = startPoint + j*deltaVec - 50.0*viewingVector;
			IntersectSurfaceByLine(mainSurf, pnt, viewingVector, cpt, NULL);
			cpIndexNew = cpIndexStart + j*4;
			oldCtrlPoint = IwPoint3d(cPointers[cpIndexNew], cPointers[cpIndexNew+1], cPointers[cpIndexNew+2]);
			if ( j == 0 || j == (cpNo-1))// first point or last point
			{
				// average out
				newCtrlPoint = 0.5*oldCtrlPoint + 0.5*(IwPoint3d)cpt;
			}
			else
			{
				newCtrlPoint = cpt;
			}
			//
			ctrlPntIndexes.Add(cpIndexNew);
			prevCtrlPnts.Add(oldCtrlPoint);
			postCtrlPnts.Add(newCtrlPoint);
		}
	}

	return;
}

void CDefineCartilage::DetermineTroGrooveAnteriorReferencePoints(IwTArray<IwPoint3d> troUVPoints)
{
	m_troGrooveAntRefPoints.RemoveAll();

	if ( troUVPoints.GetSize() == 0 )
		return;

	// Get cuts info
	IwTArray<IwPoint3d> cutPoints, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);
	// Get implant side information
	QString sideInfo;
	CImplantSide implantSide = m_pDoc->GetImplantSide(sideInfo);
	bool positiveSideIsLateral;
	IwPoint3d lateralDistalCutPnt;
	if (implantSide == IMPLANT_SIDE_RIGHT)
	{
		positiveSideIsLateral = true;
		lateralDistalCutPnt = cutPoints.GetAt(6);
	}
	else
	{
		positiveSideIsLateral = false;
		lateralDistalCutPnt = cutPoints.GetAt(7);
	}
	// Get femoral axes 
	IwAxis2Placement sweepAxes = IFemurImplant::FemoralAxes_GetSweepAxes();

	//////////////////////////////////////////////////////////////////////////////
	// We always use troUVPoints.GetAt(0) as reference point ML position
	double F3APThickness = m_pDoc->GetVarTableValue("JIGS F3 SKETCH AP WIDTH") + 4.0;// Let it pass 4mm.
	IwPoint3d refUVPnt = troUVPoints.GetAt(0);
	IwPoint2d refUV = IwPoint2d(refUVPnt.x, refUVPnt.y);
	IwPoint3d refPnt;
	m_femurMainSurfRef->EvaluatePoint(refUV, refPnt);
	IwVector3d tempVec = refPnt - lateralDistalCutPnt;
	double distToMove = tempVec.Dot(sweepAxes.GetZAxis()) - F3APThickness;
	refPnt = refPnt - distToMove*sweepAxes.GetZAxis();
if(0)
ShowPoint(m_pDoc, refPnt, red);
	// 
	IwPoint3d cPnt;
	for (double d=-10; d<10.1; d++)
	{
		IntersectSurfaceByLine(m_femurMainSurfRef, refPnt+d*sweepAxes.GetXAxis(), sweepAxes.GetYAxis(), cPnt);
		m_troGrooveAntRefPoints.Add(cPnt);
	}
}

bool CDefineCartilage::OnReviewBack()
{
	// set back to normal
	m_pView->OnTransparencyTransparent();
	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_CARTILAGE_SURFACE, false);
	return true;
}

bool CDefineCartilage::OnReviewNext()
{
	// set back to normal
	m_pView->OnTransparencyTransparent();
	// hide femoral axes
	CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES );
	if ( pEntity ) 
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}

	GetView()->OnActivateNextJigsReviewManager(this, ENT_ROLE_CARTILAGE_SURFACE);
	return true;
}

bool CDefineCartilage::OnReviewRework()
{
	// Give reviewer a warning
	int button = QMessageBox::warning( NULL, 
									QString("Rework!"), 
									QString("Rework will exit the review process."), 
									QMessageBox::Ok, QMessageBox::Cancel);	
	if ( button == QMessageBox::Cancel )
		return true;

	// set back to normal
	m_pView->OnTransparencyTransparent();
	// hide femoral axes
	CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMORAL_AXES );
	if ( pEntity ) 
	{
		pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
		m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
	}

	if (GetView()->DisplayReviewCommentDialog(ENT_ROLE_CARTILAGE_SURFACE))
    	OnAccept();
	return true;
}

void CDefineCartilage::OnReviewPredefinedView()
{
	Viewport* vp = m_pView->GetViewWidget()->GetViewport();
	if ( vp == NULL )
		return;

	int totalReviews = 4;
	int viewIndex = m_predefinedViewIndex%totalReviews;
	if ( viewIndex == 0 ) 
	{
		// Display osteophyte
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Hide femur
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Hide cartilage
		pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		vp->BottomView(false);
	}
	else if ( viewIndex == 1 ) 
	{
		// Hide osteophyte
		CEntity* pEntity = m_pDoc->GetEntity( ENT_ROLE_OSTEOPHYTE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_HIDDEN );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), false);
		}
		// Display femur
		pEntity = m_pDoc->GetEntity( ENT_ROLE_FEMUR );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_DISPLAY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}
		// Transparent cartilage
		pEntity = m_pDoc->GetEntity( ENT_ROLE_CARTILAGE_SURFACE );
		if ( pEntity ) 
		{
			pEntity->SetDisplayMode( DISP_MODE_TRANSPARENCY );
			m_pDoc->ToggleEntPanelItemByID(pEntity->GetEntId(), true);
		}

		vp->BackView(false);
	}
	else if ( viewIndex == 2 ) 
	{
		vp->BottomView(false);
	}
	else if ( viewIndex == 3 ) 
	{
		vp->FrontView(false);
	}

	m_predefinedViewIndex++;
}

void CDefineCartilage::OnValidate()
{
	// No validation report for Cartilage
	//m_pDoc->AppendValidationReportJigsEntry("\n");
	//m_pDoc->AppendValidationReportJigsEntry("**** Cartilage Surface ****");

	m_validateStatus = Validate(m_pDoc, m_validateMessage);
	SetValidateIcon(m_validateStatus, m_validateMessage);
	m_pDoc->SetEntPanelValidateStatus(ENT_ROLE_CARTILAGE_SURFACE, m_validateStatus, m_validateMessage);
	m_pDoc->GetCartilageSurface()->SetValidateStatus(m_validateStatus, m_validateMessage);
}

///////////////////////////////////////////////////////////////////////
// This function validates users settings.
///////////////////////////////////////////////////////////////////////
CEntValidateStatus CDefineCartilage::Validate
(
	CTotalDoc* pDoc,						// I:
	QString &message						// O: error messages
)
{
	pDoc->AppendLog( QString("CDefineCartilage::Validate()") );

	// initialize data
	message.clear();
	int vStatus = 0; 

	if ( pDoc->GetValidateTableStatus("CARTILAGE TROCHLEAR CURVE MAX DEVIATION STATUS") != -1 )
	{
		double sizeRatio = IFemurImplant::FemoralAxes_GetFemurEpiCondylarSize()/76.0;
		double maxDeviationAllowed = sizeRatio*pDoc->GetVarTableValue("JIGS CARTILAGE TROCHLEAR CURVE MAX DEVIATION");
		double maxDist;
		bool pastAntRefCurve;
		bool reliable = ValidateTrochlearCurveDeviation(pDoc, maxDist, pastAntRefCurve);
		if ( reliable )
		{
			if ( maxDist > maxDeviationAllowed || !pastAntRefCurve )
			{
				pDoc->AppendValidateMessage( QString("CARTILAGE TROCHLEAR CURVE MAX DEVIATION"), vStatus, message );
			}
		}
		else
		{
			if ( !message.isEmpty() )
				message += QString("\n");
			message += QString("Software validation is not reliable. Please proceed manual checking for trochlear curve deviation.");		
			vStatus = max(vStatus, 2);
		}
	}

	//
	if ( vStatus == 0 )
	{
		if ( message.isEmpty() )
			message = QString("All pass.");
		else
			message += QString("\nAll pass.");
	}

	return (CEntValidateStatus)vStatus;
}

void CDefineCartilage::SetValidateIcon(CEntValidateStatus vStatus, QString message)
{
	if ( m_actValidate )
	{
		m_actValidate->setToolTip( message );
		if ( vStatus == VALIDATE_STATUS_RED )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Red.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_YELLOW )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Yellow.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_GREEN )
		{
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate_Green.png" ) );
		}
		else if ( vStatus == VALIDATE_STATUS_NOT_VALIDATE )
		{
			m_actValidate->setToolTip( QString("Not validate.") );
			m_actValidate->setIcon( QIcon( ":/KTriathlonF1/Resources/Validate.png" ) );
		}
	}
}

bool CDefineCartilage::ValidateTrochlearCurveDeviation
(
	CTotalDoc* pDoc,						// I:
	double& maxDeviation,					// O:
	bool &pastAntRefCurve					// O:
)
{
	CCartilageSurface* cartilageSurface = pDoc->GetCartilageSurface();
	if (cartilageSurface==NULL)
		return false;

	// Get control points
	IwTArray<IwPoint3d> troGrooveUVPoints, troGrooveXYZPoints;
	cartilageSurface->GetTrochlearGroovePoints(troGrooveUVPoints);
	cartilageSurface->ConvertUVToXYZ(troGrooveUVPoints, troGrooveXYZPoints, NULL);
	// Get reference points
	IwTArray<IwPoint3d> troGrooveRefXYZPoints;
	cartilageSurface->GetTrochlearGrooveRefPoints(troGrooveRefXYZPoints);


	IwAxis2Placement wslAxes = IFemurImplant::FemoralAxes_GetWhiteSideLineAxes();
	double dist, maxDist = 0;
	IwVector3d tempVec;

	// Check for the deviation from the sagittal plane
	for (unsigned i=0; i<troGrooveXYZPoints.GetSize(); i++)
	{
		tempVec = troGrooveXYZPoints.GetAt(i) - wslAxes.GetOrigin();
		dist = fabs(tempVec.Dot(wslAxes.GetXAxis()));
		if (dist > maxDist)
			maxDist = dist;
	}

	// Check for the deviation from the reference points. Only consider ML deviation.
	for (unsigned i=1; i<troGrooveRefXYZPoints.GetSize(); i++) // Skip the first one (anterior ctrl pnt)
	{
		tempVec = troGrooveRefXYZPoints.GetAt(i) - troGrooveXYZPoints.GetAt(i);
		dist = fabs(tempVec.Dot(wslAxes.GetXAxis()));
		if (dist > maxDist)
			maxDist = dist;
	}

	maxDeviation = maxDist;

	//////////////////////////////////////////////////////////////////////////////////
	// Now check whether the trochlear passes the anterior reference curve (F3 depth)
	double F3APThickness = pDoc->GetVarTableValue("JIGS F3 SKETCH AP WIDTH") + 4.0; // 4.0 as allowance
	// Get cuts info
	IwTArray<IwPoint3d> cutPoints, cutOrientations;
	IFemurImplant::FemoralCuts_GetCutsInfo(cutPoints, cutOrientations);
	IwPoint3d lateralDistalCutPnt;
	if ( pDoc->IsPositiveSideLateral() )
	{
		lateralDistalCutPnt = cutPoints.GetAt(6);
	}
	else
	{
		lateralDistalCutPnt = cutPoints.GetAt(7);
	}

	// 
	IwPoint3d troCurveAnteriorPoint = troGrooveXYZPoints.GetAt(0);

	tempVec = troCurveAnteriorPoint - lateralDistalCutPnt;
	double troDist = tempVec.Dot(wslAxes.GetZAxis());

	if ( troDist > F3APThickness )
		pastAntRefCurve = true;
	else
		pastAntRefCurve = false;

	return true;
}