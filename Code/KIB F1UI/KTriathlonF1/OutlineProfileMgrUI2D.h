#pragma once

#include "OutlineProfileMgrUIBase.h"

class CTotalDoc;
///////////////////////////////////////////////////////////////
// This class implements the action of a manager
// It contains all the action the OutlineProfile manager should take
// in case of the 2D (flat) Outline Profile editing
// See DefineOutlineProfile.h for more information
///////////////////////////////////////////////////////////////
class OutlineProfileMgrUI2D : public OutlineProfileMgrUIBase
{
	Q_OBJECT

public:
					OutlineProfileMgrUI2D(CTotalDoc* doc, CManagerActivateType manActType, CManager *owner, OutlineProfileMgrModel *model );
	virtual			~OutlineProfileMgrUI2D();

	virtual void	Display(Viewport* vp=NULL);

    virtual bool	MouseLeft  ( const QPoint& cursor, Qt::KeyboardModifiers keyModifier=0, Viewport* vp=NULL );
	virtual bool	MouseUp    ( const QPoint& cursor, Viewport* vp=NULL );
    virtual bool    MouseDoubleClickLeft( const QPoint& cursor, Viewport* vp=NULL );
	virtual bool	keyPressEvent( QKeyEvent* event, Viewport* vp=NULL ){return true;};

	virtual void	SetDefineOutlineProfileUndoData(CDefineOutlineProfileUndoData& undoData);

	virtual void	SetMouseMoveSearchingList();

protected:
	virtual void	UpdateSketchSurface();
	virtual void	Reset(bool activateUI=true);
	virtual void	DetermineNotchEighteenWidthPoints();

protected slots:
	virtual void	OnDispRuler();
	virtual void	OnValidate();
private:
	void ConvertSketchSurfaceTo2D(IwTArray<IwPoint3d> &arr);
	void ConvertSketchSurfaceTo2D(IwPoint3d &pt);
	void Convert2DToSketchSurface(IwTArray<IwPoint3d> &arr);
	void Convert2DToSketchSurface(IwPoint3d &pt, IwPoint2d &uv);
	void Convert2DIntersectionPtToSketchSurface(const IwPoint3d &anchor, const IwVector3d &vec, IwPoint2d &uv);
	void DisplayOutlineFeaturePoints();
	void Display2DOutline();
	void DeleteGLlist(long &list);
private:
	long	m_glOutlineProfileFlatList;
	long	m_glRulerGLlist;
};
