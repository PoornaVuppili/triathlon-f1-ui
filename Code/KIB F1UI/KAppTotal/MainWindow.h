#pragma once
#include "TriathlonF1Definitions.h"
#include "..\KApp\Module.h"
#include "Part.h"

class CTotalView;
class CEntPanel;
class CTotalDoc;
class CSquishWrapper;
class QActionGroup;

class CMainWindow : public Module
{
    Q_OBJECT

public:
    CMainWindow();
	~CMainWindow();

	void				SetDoc( CDataDoc* pDoc ) {m_pDoc=pDoc;};
	CDataDoc*			GetDoc() {return m_pDoc;};
	void				SetEntPanel( CEntPanel* pEntPanel ) {m_pEntPanel=pEntPanel;};
	CEntPanel*			GetEntPanelc() {return m_pEntPanel;};
	void				SetView( COpenGLView* pView ) {m_pView=pView;};
	COpenGLView*		GetView() {return m_pView;};

    virtual bool    Initialize()=0;
    virtual void	EnableActions( bool bEnable )=0;
	virtual void	EnableMakingActions( bool bEnable)=0;

    bool            HasImplant();
    bool            IsImplantModified();

    // Get the version info: module name & version
    virtual QString GetVersionInfo() { return ""; }

    // Get the data file version info
    virtual QString GetDataFileVersionInfo()=0;
	void			SetAppVersion(QString &str) {m_appVersion=str;};
	QString			GetAppVersion() {return m_appVersion;};

	virtual QString	WindowTitle()=0;
	virtual void	LoadingFileFinished()=0;
	void			ResetViewDefaultSettings();

public slots:
	virtual void	OnFileNew()=0;
	virtual void	OnFileOpen(QString &elem=QString(""))=0;
	virtual void	OnFileClose()=0;
	virtual void	OnFileSave(bool bWarning=true)=0;
	virtual void	OnFileOpenAsReadOnly()=0;
	virtual void	OnFileSaveACopy()=0;
	virtual void	OnEnableMakingActions()=0;
	void			OnFileSaveAutoOn(bool);
	virtual void	OnImportCADEntity()=0;
	void			OnCleanUpTempDisplay();


signals:

protected:
	void	ImportCADEntity(CEntRole entRole=ENT_ROLE_NONE, IwAxis2Placement* additionalTransformation=NULL);

private:

public:

protected:

	QMenu*			m_menuFile;
	QAction*		m_actFileNew;
	QAction*		m_actFileSaveACopy;
	QAction*		m_actFileImportCADEntity;

	QActionGroup*	m_groupTessellation;
	QMenu*			m_menuViewTessellation;
	QAction*		m_actTessellationHigh;	
	QAction*		m_actTessellationFine;	
	QAction*		m_actTessellationNormal;	
	QAction*		m_actTessellationCoarse;	

	QActionGroup*	m_groupTransparency;
	QMenu*			m_menuViewTransparency;
	QAction*		m_actTransparencyTransparentTotal;	
	QAction*		m_actTransparencyTransparentPlus;	
	QAction*		m_actTransparencyTransparent;	
	QAction*		m_actTransparencyNormal;	
	QAction*		m_actTransparencyOpaque;	
	QAction*		m_actTransparencyOpaquePlus;

	QAction*		m_actCleanTempDisplay;	

	QMenu*			m_menuTools;

	QMenu*			m_menuInfo;
	QAction*		m_actPatientInfo;
	QAction*		m_actUserHistory;

	QMenu*			m_menuImplantDesignStatus;
	QAction*		m_actImplantDesignProcess;
	QAction*		m_actImplantDesignLocked;
	QAction*		m_actJigsDesignProcess;
	QAction*		m_actJigsDesignLocked;
	QActionGroup*	m_groupImplantDesignStatus;

	COpenGLView*	m_pView;
	CEntPanel*		m_pEntPanel;
	CDataDoc*		m_pDoc;

	QString			m_sRecordDir;

	bool			m_bModified;

	QString			m_appVersion;
};